# Open Finance Conformance Suite Test Automation Script

This script automates the test plan execution process for Open Finance Conformance Suite. It allows you to choose a specific test plan and a configuration file. Additionally, you can specify an optional overrides file for customizing the test behavior.

## Prerequisites

- The Conformance Suite and its dependencies should be set up and configured. [Running the conformance suite locally](https://gitlab.com/raidiam-conformance/open-finance/certification/-/wikis/Running-the-conformance-suite-locally)
- Python 3.x must be installed on your system.
- Install httpx: `pip3 install httpx`

## Usage

The script `scripts/run-automation.sh` automates the test plan execution process. It accepts the following parameters:

1. `-t <testplan_name>`: Specify the desired test plan name.
2. `-c <config_file_path>`: Specify the path to the configuration JSON file.
3. `[-o <overrides_file_path>]`: (Optional) Specify the path to the overrides JSON file. If not provided, the script will use the default overrides file located in `./scripts/configs/overrides.json`.

## How to Run

To execute the script, navigate to the project directory and run the following command:

``` ./scripts/run-automation.sh -t <testplan_name> -c <config_file_path> [-o <overrides_file_path>] ```

Replace `<testplan_name>` and `<config_file_path>` with the appropriate values for your test plan and configuration file, respectively. The overrides file is optional.

## Examples

1. Run a test plan with a configuration file:

```bash
./scripts/run-automation.sh -t payments_test-plan -c /Users/marinamanso/Documents/payment-config.json
```

2. Run a test plan with a configuration file and custom overrides:

```bash
./scripts/run-automation.sh -t payments_test-plan -c /Users/marinamanso/Documents/payment-config.json -o ./scripts/configs/overrides.json
```

## Overrides File Format

The overrides file should be in the following JSON format:

```json
{
    "model-name": {
        "pixPaymentAmount": "100"
    },
    "payments_api_email-proxy_test-module": {
        "pixPaymentAmount": "9999.99"
    },
    "payments_api_qres-mismatched-proxy_test-module": {
        "pixPaymentAmount": "10422.01"
    }
}
```

The values in the overrides file correspond to different responses to Mock Bank according to the [Conformance Suite Phase 3 Payments V2 documentation.](https://gitlab.com/raidiam-conformance/open-finance/certification/-/wikis/Phase-3-Payments-V2)

## Notes

- Make sure to provide valid paths for the configuration and overrides files.
- Make sure to provide valid test plan names.
- If you encounter any issues or errors during the test execution process, the script will log the errors appropriately.

Feel free to modify the README file with additional information or customize it to suit your project requirements.