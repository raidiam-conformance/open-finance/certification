#!/usr/bin/env python3

import json

paths = [
    'phase3/v4',
    'phase3/automatic-payments/v2/payments',
    'phase3/automatic-payments/v2/pix-payments',
    'phase3/no-redirect-payments/v1',
    'phase3/no-redirect-payments/v2',
    'phase4b/v1',
    'phase2/v3',
    'phase2/v2'
]


def load_override_template(path):
    with open(f'./scripts/configs/{path}/override-template.json') as json_file:
        return json.load(json_file)

def add_overrides(config, overrides, path):
    for module_name, override_info in overrides.items():
        override_data = load_override_template(path)
        if 'pixPaymentAmount' in override_info:
            payment_amount = override_info['pixPaymentAmount']
            print('Overriding pix payment amount in module {0} to {1}'.format(module_name, payment_amount))
            override_data['resource']['paymentAmount'] = payment_amount
            override_data['resource']['brazilPaymentConsent']['data']['payment']['amount'] = payment_amount
            override_data['resource']['brazilQrdnPaymentConsent']['data']['payment']['amount'] = payment_amount

        if 'qrdnAmount' in override_info:
            payment_amount = override_info['qrdnAmount']
            print('Overriding qrdn payment amount in module {0} to {1}'.format(module_name, payment_amount))
            override_data['resource']['brazilQrdnRemittance'] = "$$EDITPAYMENT$$:" + payment_amount

        if 'date' in override_info:
            date_value = override_info['date']
            print('Overriding date in module {0} to {1}'.format(module_name, date_value))
            override_data['resource']['payment']['date'] = date_value
            override_data['resource']['brazilPaymentConsent']['data']['payment']['date'] = date_value
            override_data['resource']['brazilQrdnPaymentConsent']['data']['payment']['date'] = date_value

        if 'loggedUserIdentification' in override_info:
            cpf_value = override_info['loggedUserIdentification']
            print('Overriding loggedUserIdentification in module {0} to {1}'.format(module_name, cpf_value))
            override_data['resource']['loggedUserIdentification'] = cpf_value

        if 'businessEntityIdentification' in override_info:
            cnpj_value = override_info['businessEntityIdentification']
            print('Overriding businessEntityIdentification in module {0} to {1}'.format(module_name, cnpj_value))
            override_data['resource']['businessEntityIdentification'] = cnpj_value

        if 'browser' in override_info:
            override_data['browser'] = override_info['browser']

            # if 'schedule' in override_data['resource']:
            #     override_data['resource']['schedule']['single']['date'] = date_value
            # else:
            #     override_data['resource']['schedule'] = {
            #         "single": {
            #             "date": date_value
            #         }
            #     }
            #
        config['override'][module_name] = override_data

def add_aliases(config, aliases, path):
    for alias in aliases:
        config['alias'] = alias
        with open(f'./scripts/configs/{path}/generated/generated-{alias}-config.json', 'w+', encoding='utf-8') as f:
            json.dump(config, f, ensure_ascii=False, indent=4)


if __name__ == '__main__':
    for path in paths:
        with open(f'./scripts/configs/{path}/base-config.json') as json_file:
            data = json.load(json_file)

        with open(f'./scripts/configs/{path}/overrides.json') as json_file:
            overrides = json.load(json_file)

        with open(f'./scripts/configs/{path}/aliases.json') as json_file:
            aliases = json.load(json_file)

        data['override'] = {}

        add_overrides(data, overrides, path)
        add_aliases(data, aliases, path)
