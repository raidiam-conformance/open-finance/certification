package net.openid.conformance.extensions;

import net.openid.conformance.extensions.oneshot.OneShot;
import net.openid.conformance.extensions.oneshot.OneShotJob;

import java.util.concurrent.CountDownLatch;

public class TestOneShotJobOne implements OneShotJob {

	private CountDownLatch latch;

	public TestOneShotJobOne(CountDownLatch latch) {
		this.latch = latch;
	}

	@Override
	public void execute() {
		latch.countDown();
	}
}
