package net.openid.conformance.extensions;

import net.openid.conformance.extensions.oneshot.AssureJobCondition;
import net.openid.conformance.extensions.oneshot.AssureJobRun;
import org.junit.Test;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.lang.annotation.Annotation;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AssureJobConditionTests {

	@Test
	public void disabledIfBrandRequestedButNotPresent() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation("opf", null, null);
		ConditionContext ctx = createMockContext(null, null);
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertFalse(matches);

	}

	@Test
	public void disabledIfBrandRequestedButWrong() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation("opf", null, null);
		ConditionContext ctx = createMockContext("opin", null);
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertFalse(matches);

	}

	@Test
	public void disabledIfUrlRequestedButNotPresent() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation(null, "https://conformance.yeah", null);
		ConditionContext ctx = createMockContext(null, null);
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertFalse(matches);

	}

	@Test
	public void disabledIfUrlRequestedButWrong() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation(null, "https://conformance.yeah", null);
		ConditionContext ctx = createMockContext(null, "https://conformance.nope");
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertFalse(matches);

	}

	@Test
	public void disabledIfJobRequestedButNotConfigured() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation(null, null, "job1");
		ConditionContext ctx = createMockContext(null, null);
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertFalse(matches);

	}

	@Test
	public void disabledIfJobRequestedButNotPresent() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation(null, null, "job1");
		ConditionContext ctx = createMockContext(null, null, "job2", "job3");
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertFalse(matches);

	}

	@Test
	public void disabledIfNotEveryParameterCorrect() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation("opf", "https://conformance.yeah", "job1");
		ConditionContext ctx = createMockContext("opf", "https://conformance.yeah", "job2", "job3");
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertFalse(matches);

	}

	@Test
	public void enabledIfBrandRequestedAndConfigured() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation("opf", null, "job1");
		ConditionContext ctx = createMockContext("opf", null, "job1");
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertTrue(matches);

	}

	@Test
	public void enabledIfUrlRequestedAndConfigured() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation(null, "https://conformance.yeah", "job1");
		ConditionContext ctx = createMockContext("opf", "https://conformance.yeah", "job1");
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertTrue(matches);

	}

	@Test
	public void enabledIfJobRequestedAndConfigured() {

		Condition condition = new AssureJobCondition();
		AssureJobRun annotation = createAnnotation(null, "https://conformance.yeah", "job2");
		ConditionContext ctx = createMockContext("opf", "https://conformance.yeah", "job1", "job2", "job3");
		AnnotatedTypeMetadata metadata = createMockMetadata(annotation);
		boolean matches = condition.matches(ctx, metadata);

		assertTrue(matches);

	}


	private AnnotatedTypeMetadata createMockMetadata(AssureJobRun annotation) {
		AnnotatedTypeMetadata metadata = mock(AnnotatedTypeMetadata.class);
		MergedAnnotations annotations = mock(MergedAnnotations.class);
		MergedAnnotation mergedAnnotation = mock(MergedAnnotation.class);
		when(mergedAnnotation.getString("brand")).thenReturn(annotation.brand());
		when(mergedAnnotation.getString("conformanceUrl")).thenReturn(annotation.conformanceUrl());
		when(mergedAnnotation.getString("jobName")).thenReturn(annotation.jobName());
		when(annotations.get(AssureJobRun.class)).thenReturn(mergedAnnotation);
		when(metadata.getAnnotations()).thenReturn(annotations);
		when(metadata.isAnnotated(AssureJobRun.class.getName())).thenReturn(true);
		return metadata;
	}

	private ConditionContext createMockContext(String brand, String conformanceUrl, String... jobNames) {
		ConditionContext ctx = mock(ConditionContext.class);
		Environment env = mock(Environment.class);
		when(env.getProperty("fintechlabs.brand")).thenReturn(brand);
		when(env.getProperty("fintechlabs.base_url")).thenReturn(conformanceUrl);
		when(env.getProperty("FINTECHLABS_EXTENSIONS_ONESHOT_JOBS")).thenReturn(String.join(",", jobNames));
		when(ctx.getEnvironment()).thenReturn(env);
		return ctx;
	}

	private AssureJobRun createAnnotation(String brand, String conformanceUrl, String jobName) {
		return new AssureJobRun() {
			@Override
			public Class<? extends Annotation> annotationType() {
				return AssureJobRun.class;
			}

			@Override
			public String brand() {
				return Optional.ofNullable(brand).orElse("");
			}

			@Override
			public String conformanceUrl() {
				return Optional.ofNullable(conformanceUrl).orElse("");
			}

			@Override
			public String jobName() {
				return Optional.ofNullable(jobName).orElse("");
			}
		};
	}

}
