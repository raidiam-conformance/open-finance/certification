package net.openid.conformance.extensions.yacs;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class AliasHandlerTests {

	@Test
	public void aliasNotProvided() throws IOException {

		String expectedOrgId = "fb4801a2-dbaa-443f-a442-8445c6d4e172";

		ConfigAliasHandler handler = new ConfigAliasHandler();
		JsonObject config = new JsonObject();
		String wellKnown = "https://kc-fap.mvp.openligia.com/realms/master/.well-known/openid-configuration";
		Set<Map> orgs = theOrgs();

		handler.handle(config, orgs, wellKnown);

		assertEquals(expectedOrgId, OIDFJSON.getString(config.get("alias")));
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		String orgId = OIDFJSON.getString(resourceConfig.get("brazilOrganizationId"));
		assertEquals(expectedOrgId, orgId);

	}

	@Test
	public void aliasProvidedOnConfig() throws IOException {

		String expectedOrgId = "f8993e33-8f0c-5530-9207-2d9cf8454ee3";
		ConfigAliasHandler handler = new ConfigAliasHandler();
		JsonObject config = new JsonObject();
		config.addProperty("alias", expectedOrgId);
		String wellKnown = "https://acp.dev.bancofibra.com.br/default/openbanking_brasil/.well-known/openid-configuration";
		Set<Map> orgs = theOrgs();

		handler.handle(config, orgs, wellKnown);

		assertEquals(expectedOrgId, OIDFJSON.getString(config.get("alias")));
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		String orgId = OIDFJSON.getString(resourceConfig.get("brazilOrganizationId"));
		assertEquals(expectedOrgId, orgId);

	}

	@Test
	public void unknownAliasProvidedOnConfig() throws IOException {

		String expectedOrgId = "f8993e33-8f0c-5530-9207-2d9cf8454ee5";
		ConfigAliasHandler handler = new ConfigAliasHandler();
		JsonObject config = new JsonObject();
		config.addProperty("alias", expectedOrgId);
		String wellKnown = "https://acp.dev.bancofibra.com.br/default/openbanking_brasil/.well-known/openid-configuration";
		Set<Map> orgs = theOrgs();

		try {
			handler.handle(config, orgs, wellKnown);
			fail();
		} catch (RuntimeException e) {

		}

	}

	@Test
	public void illegalAliasProvidedOnConfig() throws IOException {

		String expectedOrgId = "f8993e33-8f0c-5530-9207-2d9cf8454ee3";
		ConfigAliasHandler handler = new ConfigAliasHandler();
		JsonObject config = new JsonObject();
		config.addProperty("alias", expectedOrgId);
		String wellKnown = "https://auth-dev1-crystalamc-ofb.cmsw.com/auth/realms/test/.well-known/openid-configuration";
		Set<Map> orgs = theOrgs();

		try {
			handler.handle(config, orgs, wellKnown);
			fail();
		} catch (RuntimeException e) {

		}

	}

	public static Set<Map> theOrgs() throws IOException {
		Set<String> orgIds = Set.of("fb4801a2_dbaa_443f_a442_8445c6d4e172",
			"fb1ec91f_8228_4c89_b895_d1fd0fd10e77", "f8993e33_8f0c_5530_9207_2d9cf8454ee3" );
		InputStream stream = AliasHandlerTests.class.getClassLoader().getResourceAsStream("participants.json");
		String resource = new String(stream.readAllBytes());
		Map[] all =  new Gson().fromJson(resource, Map[].class);
		Map<String, Map> lookup = new HashMap<>();
		for(Map m: all) {
			String orgId = (String) m.get("OrganisationId");
			orgId = orgId.replaceAll("-", "_");
			lookup.put(orgId, m);
		}
		return lookup.entrySet()
			.stream()
			.filter(kv -> orgIds.contains(kv.getKey()))
			.map(Map.Entry::getValue)
			.collect(Collectors.toSet());
	}

	public static Optional<Map<String, Object>> theOrgsMap(String authorisationServerId) throws IOException {
		InputStream stream = AliasHandlerTests.class.getClassLoader().getResourceAsStream("mockParticipants.json");
		String resource = new String(stream.readAllBytes());
		Map[] all =  new Gson().fromJson(resource, Map[].class);
		Map<String, Map> lookup = new HashMap<>();
		for(Map m: all) {
			String orgId = (String) m.get("OrganisationId");
			orgId = orgId.replaceAll("-", "_");
			lookup.put(orgId, m);
		}
		return lookup.values()
			.stream()
			.map(org -> org.get("AuthorisationServers"))
			.filter(authorisationServers -> authorisationServers instanceof List)
			.flatMap(authorisationServers -> ((List<?>) authorisationServers).stream())
			.filter(server -> server instanceof Map)
			.map(server -> (Map<String, Object>) server)
			.filter(server -> authorisationServerId.equals(server.get("AuthorisationServerId")))
			.findFirst();
	}

}
