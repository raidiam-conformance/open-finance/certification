package net.openid.conformance.extensions.yacs;

import net.openid.conformance.extensions.AuthenticatedUserRepository;
import net.openid.conformance.extensions.CachedAuthenticatedUser;
import net.openid.conformance.extensions.EndpointLimits;
import net.openid.conformance.extensions.InMemoryAuthenticatedUserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


import java.time.Instant;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticatedUserLimitsTests {

	private AuthenticatedUserRepository repository;

	@Before
	public void setUp() {
		repository = new InMemoryAuthenticatedUserRepository();
	}

	@Test
	public void testIncrementEndpointLimitsReached() {
		CachedAuthenticatedUser user = new CachedAuthenticatedUser("user1", "sub1");
		for (int i = 0; i < 10; i++) {
			repository.incrementEndpointCalled(user, EndpointLimits.CREATE_TEST_PLAN);
		}
		assertTrue(repository.getLimitReached(user, EndpointLimits.CREATE_TEST_PLAN));
	}

	@Test
	public void verifyUsersAreUnique() {
		CachedAuthenticatedUser user = new CachedAuthenticatedUser("user1", "sub1");
		for (int i = 0; i < 10; i++) {
			repository.incrementEndpointCalled(user, EndpointLimits.CREATE_TEST_PLAN);
		}
		user = new CachedAuthenticatedUser("user1", "sub1");
		repository.incrementEndpointCalled(user, EndpointLimits.CREATE_TEST_PLAN);
		assertTrue(repository.getLimitReached(user, EndpointLimits.CREATE_TEST_PLAN));
	}

	@Test
	public void testIncrementEndpointLimitsNotReached() {
		CachedAuthenticatedUser user = new CachedAuthenticatedUser("user2", "sub1");
		for (int i = 0; i < 5; i++) {
			repository.incrementEndpointCalled(user, EndpointLimits.CREATE_TEST_PLAN);
		}
		assertFalse(repository.getLimitReached(user, EndpointLimits.CREATE_TEST_PLAN));
	}


	@Test
	public void testRemoveAuthenticatedUserOlderThan() {
		CachedAuthenticatedUser user1 = new CachedAuthenticatedUser("newUser1", "sub1");
		repository.incrementEndpointCalled(user1, EndpointLimits.CREATE_TEST_PLAN);
		repository.removeAuthenticatedUserOlderThan(Instant.now().plusSeconds(120));
		assertNull(repository.getAuthenticatedUser(user1));
	}
}
