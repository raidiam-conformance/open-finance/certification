package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import org.mockito.Mockito;
import org.springframework.mock.web.DelegatingServletInputStream;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ServletInputStreamFactory {

	public static ServletInputStream forJson(JsonObject object) {
		return new DelegatingServletInputStream(new ByteArrayInputStream(object.toString().getBytes(StandardCharsets.UTF_8)));
	}

	public static void mockRequestBody(HttpServletRequest request, JsonObject body) throws IOException {
		Mockito.when(request.getInputStream()).thenReturn(forJson(body));
	}

	public static BodyRepeatableHttpServletRequest withBody(JsonObject jsonObject) throws IOException {
		HttpServletRequest mockDelegate = Mockito.mock(HttpServletRequest.class);
		mockRequestBody(mockDelegate, jsonObject);
		return new BodyRepeatableHttpServletRequest(mockDelegate);
	}

}
