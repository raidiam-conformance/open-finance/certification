package net.openid.conformance.extensions.yacs;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

class TinyWebServer implements Runnable {

	private final TinyPki.Bundle serverCert;
	private final boolean useTls;
	private int port;
	private CountDownLatch latch = new CountDownLatch(1);
	private boolean keepRunning = true;

	public TinyWebServer(TinyPki.Bundle serverCert) {
		this.serverCert = serverCert;
		this.useTls = true;
	}

	void start() {
		if (useTls) {
			startTls();
		} else {
			startPlain();
		}
	}

	private void startPlain() {
		try (ServerSocket serverSocket = new ServerSocket(0)) {
			port = serverSocket.getLocalPort();
			latch.countDown();

			while (keepRunning) {
				try (Socket clientSocket = serverSocket.accept();
					 BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					 PrintWriter out = new PrintWriter(clientSocket.getOutputStream())) {

					String requestLine = in.readLine();

					String httpResponse = "HTTP/1.1 200 OK\r\n" +
						"Content-Type: text/plain\r\n" +
						"Content-Length: 24\r\n" +
						"\r\n" +
						"{\"text\":\"Hello, World!\"}";

					out.write(httpResponse);
					out.flush();

				} catch (IOException e) {
					System.err.println("Error handling client connection: " + e.getMessage());
				}
			}
			serverSocket.close();
		} catch (IOException e) {
			System.err.println("Server error: " + e.getMessage());
		}
	}

	void startTls() {
		try {
			startTlsInt();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	void startTlsInt() throws Exception {
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(serverCert.keyManagers(), serverCert.getIssuer().trustStore(), new SecureRandom());

		SSLServerSocketFactory serverSocketFactory = sslContext.getServerSocketFactory();
		SSLServerSocket serverSocket = (SSLServerSocket) serverSocketFactory.createServerSocket(0);
		serverSocket.setNeedClientAuth(true);
		port = serverSocket.getLocalPort();
		latch.countDown();
		while (keepRunning) {
			try (SSLSocket clientSocket = (SSLSocket) serverSocket.accept();
				 BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				 PrintWriter out = new PrintWriter(clientSocket.getOutputStream())) {
				System.out.println("Client connected");
				String requestLine = in.readLine();

				String httpResponse = "HTTP/1.1 200 OK\r\n" +
					"Content-Type: text/plain\r\n" +
					"Content-Length: 24\r\n" +
					"\r\n" +
					"{\"text\":\"Hello, World!\"}";

				out.write(httpResponse);
				out.flush();
			} catch (IOException e) {
				System.err.println("Client connection error: " + e.getMessage());
			}

		}
		serverSocket.close();
	}

	@Override
	public void run() {
		start();
	}

	public int port() throws InterruptedException {
		latch.await(1000L, TimeUnit.SECONDS);
		return port;
	}

	public void stop() {
		keepRunning = false;
	}
}
