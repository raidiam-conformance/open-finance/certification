package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import net.openid.conformance.extensions.yacs.dcr.DirectoryDetails;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConfigFilterTests {

    @Test
    public void blocksCreationByUsersWithNoPfpvc() throws ServletException, IOException {

        AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
        when(authenticationFacade.isUser()).thenReturn(false);
        when(authenticationFacade.isAdmin()).thenReturn(false);
        BodyRepeatableHttpServletRequest request = mock(BodyRepeatableHttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        ConfigFilter configFilter = new ConfigFilter(null, authenticationFacade, null, null, null);

        boolean shouldProceed = configFilter.processRequest(request, response);

        assertFalse(shouldProceed);
        Mockito.verify(response).sendError(Mockito.intThat(i -> i == 403), Mockito.anyString());

    }

    @Test
    public void allowsCreationByUsersWithPfpvc() throws ServletException, IOException {

        ParticipantsService participantsService = mock(ParticipantsService.class);
        DirectoryDetails directoryDetails = mock(DirectoryDetails.class);
        when(directoryDetails.getKeystoreUri()).thenReturn("https://example.com");
        when(directoryDetails.getDirectoryUri()).thenReturn("https://example.com");
        when(directoryDetails.getDiscoveryUri()).thenReturn("https://example.com");
        AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
        when(authenticationFacade.isUser()).thenReturn(true);
        when(authenticationFacade.isAdmin()).thenReturn(false);
        BodyRepeatableHttpServletRequest request = mock(BodyRepeatableHttpServletRequest.class);
        when(request.body()).thenReturn(new JsonObject());
        when(request.getParameterValues("planName")).thenReturn(new String[] {"fakePlan"});
        HttpServletResponse response = mock(HttpServletResponse.class);

        ConfigFilter configFilter = new ConfigFilter(participantsService, authenticationFacade, directoryDetails, "https://example.com", "https://example.com");

        boolean shouldProceed = configFilter.processRequest(request, response);

        assertTrue(shouldProceed);
        Mockito.verify(response, Mockito.never()).sendError(Mockito.anyInt(), Mockito.anyString());

    }

	@Test
	public void allowsCreationByUsersWithPfpvcAndAuthorisationServerId() throws ServletException, IOException {

		ParticipantsService participantsService = mock(ParticipantsService.class);
		DirectoryDetails directoryDetails = mock(DirectoryDetails.class);
		when(directoryDetails.getKeystoreUri()).thenReturn("https://example.com");
		when(directoryDetails.getDirectoryUri()).thenReturn("https://example.com");
		when(directoryDetails.getDiscoveryUri()).thenReturn("https://example.com");
		String authorisationServerId = "896e82c7-fcfd-4bf3-ad11-e45e36009921";

		AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
		when(authenticationFacade.isUser()).thenReturn(true);
		when(authenticationFacade.isAdmin()).thenReturn(false);
		when(participantsService.authorisationServerForId(any())).thenReturn(AliasHandlerTests.theOrgsMap(authorisationServerId));

		JsonObject config = new JsonObject();
		JsonObject serverConfig = new JsonObject();
		serverConfig.addProperty("authorisationServerId", authorisationServerId);
		config.add("server", serverConfig);

		BodyRepeatableHttpServletRequest request = mock(BodyRepeatableHttpServletRequest.class);
		when(request.body()).thenReturn(config);
		when(request.getParameterValues("planName")).thenReturn(new String[] {"fakePlan"});
		HttpServletResponse response = mock(HttpServletResponse.class);

		ConfigFilter configFilter = new ConfigFilter(participantsService, authenticationFacade, directoryDetails, "https://example.com", "https://example.com");

		boolean shouldProceed = configFilter.processRequest(request, response);

		assertTrue(shouldProceed);
		assertEquals("fb4801a2-dbaa-443f-a442-8445c6d4e172", OIDFJSON.getString(config.get("alias")));
		Mockito.verify(response, Mockito.never()).sendError(Mockito.anyInt(), Mockito.anyString());
	}

}
