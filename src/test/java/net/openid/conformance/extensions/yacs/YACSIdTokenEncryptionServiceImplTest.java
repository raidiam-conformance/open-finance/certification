package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.util.JsonObjectBuilder;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;

public class YACSIdTokenEncryptionServiceImplTest {

	@Test
	public void weCanAddKeys() throws IOException {
		String key = IOUtils.resourceToString("tokenIdEncryptionKeys/key.json", Charset.defaultCharset(), getClass().getClassLoader());
		YACSIdTokenEncryptionServiceImpl service = new YACSIdTokenEncryptionServiceImpl(IOUtils.resourceToURL("tokenIdEncryptionKeys/key.json", getClass().getClassLoader()).getPath());

		JsonObject config = new JsonObjectBuilder()
			.addField("client.jwks.keys", new JsonArray())
			.addField("client.org_jwks.keys", new JsonArray())
			.build();

		service.addKeyToConfig(config);

		JsonArray jwks = config.getAsJsonObject("client")
			.getAsJsonObject("jwks")
			.getAsJsonArray("keys");

		JsonArray orgJwks = config.getAsJsonObject("client")
			.getAsJsonObject("org_jwks")
			.getAsJsonArray("keys");

		JsonElement keyJsonObject = JsonParser.parseString(key);
		Assert.assertEquals(1, jwks.size());
		Assert.assertEquals(keyJsonObject.getAsJsonObject(), jwks.get(0).getAsJsonObject());

		Assert.assertEquals(1, orgJwks.size());
		Assert.assertEquals(keyJsonObject.getAsJsonObject(), orgJwks.get(0).getAsJsonObject());
	}

	@Test
	public void weCanAddKeysIfSomeObjectsAreMissing() throws IOException {
		String key = IOUtils.resourceToString("tokenIdEncryptionKeys/key.json", Charset.defaultCharset(), getClass().getClassLoader());
		YACSIdTokenEncryptionServiceImpl service = new YACSIdTokenEncryptionServiceImpl(IOUtils.resourceToURL("tokenIdEncryptionKeys/key.json", getClass().getClassLoader()).getPath());

		JsonObject config = new JsonObject();

		service.addKeyToConfig(config);

		JsonArray jwks = config.getAsJsonObject("client")
			.getAsJsonObject("jwks")
			.getAsJsonArray("keys");

		JsonArray orgJwks = config.getAsJsonObject("client")
			.getAsJsonObject("org_jwks")
			.getAsJsonArray("keys");

		JsonElement keyJsonObject = JsonParser.parseString(key);
		Assert.assertEquals(1, jwks.size());
		Assert.assertEquals(keyJsonObject.getAsJsonObject(), jwks.get(0).getAsJsonObject());

		Assert.assertEquals(1, orgJwks.size());
		Assert.assertEquals(keyJsonObject.getAsJsonObject(), orgJwks.get(0).getAsJsonObject());
	}

	@Test
	public void weCanAddKeysWithoutOrgJwks() throws IOException {
		String key = IOUtils.resourceToString("tokenIdEncryptionKeys/key.json", Charset.defaultCharset(), getClass().getClassLoader());
		YACSIdTokenEncryptionServiceImpl service = new YACSIdTokenEncryptionServiceImpl(IOUtils.resourceToURL("tokenIdEncryptionKeys/key.json", getClass().getClassLoader()).getPath());

		JsonObject config = new JsonObjectBuilder()
			.addField("client.jwks.keys", new JsonArray())
			.build();

		service.addKeyToConfig(config);

		JsonArray jwks = config.getAsJsonObject("client")
			.getAsJsonObject("jwks")
			.getAsJsonArray("keys");

		JsonArray orgJwks = config.getAsJsonObject("client")
			.getAsJsonObject("org_jwks")
			.getAsJsonArray("keys");

		JsonElement keyJsonObject = JsonParser.parseString(key);
		Assert.assertEquals(1, jwks.size());
		Assert.assertEquals(keyJsonObject.getAsJsonObject(), jwks.get(0).getAsJsonObject());

		Assert.assertEquals(1, orgJwks.size());
		Assert.assertEquals(keyJsonObject.getAsJsonObject(), orgJwks.get(0).getAsJsonObject());
	}

	@Test
	public void ifEncKeyIsPresentItWillBeReplaced() throws IOException {
		String key = IOUtils.resourceToString("tokenIdEncryptionKeys/key.json", Charset.defaultCharset(), getClass().getClassLoader());
		String keyWithDifferentKid = IOUtils.resourceToString("tokenIdEncryptionKeys/keyWithDifferentKid.json", Charset.defaultCharset(), getClass().getClassLoader());
		YACSIdTokenEncryptionServiceImpl service = new YACSIdTokenEncryptionServiceImpl(IOUtils.resourceToURL("tokenIdEncryptionKeys/key.json", getClass().getClassLoader()).getPath());


		JsonElement keyWithDifferentKidJsonObject = JsonParser.parseString(keyWithDifferentKid);
		JsonArray jwksToAdd = new JsonArray();
		jwksToAdd.add(keyWithDifferentKidJsonObject);

		JsonObject config = new JsonObjectBuilder()
			.addField("client.jwks.keys", jwksToAdd)
			.build();

		service.addKeyToConfig(config);

		JsonArray jwks = config.getAsJsonObject("client")
			.getAsJsonObject("jwks")
			.getAsJsonArray("keys");

		JsonElement keyJsonObject = JsonParser.parseString(key);

		Assert.assertEquals(1, jwks.size());
		Assert.assertEquals(keyJsonObject.getAsJsonObject(), jwks.get(0).getAsJsonObject());
	}

	@Test
	public void ifMultipleEncKeysArePresentTheyWillBeReplaced() throws IOException {
		String key = IOUtils.resourceToString("tokenIdEncryptionKeys/key.json", Charset.defaultCharset(), getClass().getClassLoader());
		String keyWithDifferentKid = IOUtils.resourceToString("tokenIdEncryptionKeys/keyWithDifferentKid.json", Charset.defaultCharset(), getClass().getClassLoader());
		String keyWithDifferentKid2 = IOUtils.resourceToString("tokenIdEncryptionKeys/keyWithDifferentKid2.json", Charset.defaultCharset(), getClass().getClassLoader());
		YACSIdTokenEncryptionServiceImpl service = new YACSIdTokenEncryptionServiceImpl(IOUtils.resourceToURL("tokenIdEncryptionKeys/key.json", getClass().getClassLoader()).getPath());


		JsonElement keyWithDifferentKidJsonObject = JsonParser.parseString(keyWithDifferentKid);
		JsonElement keyWithDifferentKidJsonObject2 = JsonParser.parseString(keyWithDifferentKid2);
		JsonArray jwksToAdd = new JsonArray();
		jwksToAdd.add(keyWithDifferentKidJsonObject);
		jwksToAdd.add(keyWithDifferentKidJsonObject2);

		JsonObject config = new JsonObjectBuilder()
			.addField("client.jwks.keys", jwksToAdd)
			.build();

		service.addKeyToConfig(config);

		JsonArray jwks = config.getAsJsonObject("client")
			.getAsJsonObject("jwks")
			.getAsJsonArray("keys");

		JsonElement keyJsonObject = JsonParser.parseString(key);

		Assert.assertEquals(1, jwks.size());
		Assert.assertEquals(keyJsonObject.getAsJsonObject(), jwks.get(0).getAsJsonObject());
	}

	@Test
	public void ifSignKeyIsPresentItWillNotBeReplaced() throws IOException {

		String keyPath = IOUtils.resourceToURL("tokenIdEncryptionKeys/key.json", getClass().getClassLoader()).getPath();
		YACSIdTokenEncryptionServiceImpl service = new YACSIdTokenEncryptionServiceImpl(keyPath);

		String signKey = IOUtils.resourceToString("tokenIdEncryptionKeys/signKey.json", Charset.defaultCharset(), getClass().getClassLoader());
		JsonElement signKeyJsonObject = JsonParser.parseString(signKey);
		JsonArray jwksToAdd = new JsonArray();
		jwksToAdd.add(signKeyJsonObject);

		JsonObject config = new JsonObjectBuilder()
			.addField("client.jwks.keys", jwksToAdd)
			.build();

		service.addKeyToConfig(config);

		JsonArray jwks = config.getAsJsonObject("client")
			.getAsJsonObject("jwks")
			.getAsJsonArray("keys");

		Assert.assertEquals(2, jwks.size());
	}

	@Test
	public void weCantUseSignKey() throws IOException {
		String keyPath = IOUtils.resourceToURL("tokenIdEncryptionKeys/signKey.json", getClass().getClassLoader()).getPath();
		try {
			new YACSIdTokenEncryptionServiceImpl(keyPath);
			throw new AssertionError("Service did not fail");
		} catch (RuntimeException e) {
			Assert.assertEquals("Provided key is not for encryption", e.getMessage());
		}

	}

	@Test
	public void weCantUsePublicKey() throws IOException {
		String keyPath = IOUtils.resourceToURL("tokenIdEncryptionKeys/publicKey.json", getClass().getClassLoader()).getPath();
		try {
			new YACSIdTokenEncryptionServiceImpl(keyPath);
			throw new AssertionError("Service did not fail");
		} catch (RuntimeException e) {
			Assert.assertEquals("Provided key is not private", e.getMessage());
		}

	}
}
