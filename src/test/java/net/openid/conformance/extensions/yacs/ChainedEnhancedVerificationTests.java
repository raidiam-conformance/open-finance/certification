package net.openid.conformance.extensions.yacs;

import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

public class ChainedEnhancedVerificationTests {

	@Test
	public void chainedVerificationPassesIfAllPass() throws IOException {

		EnhancedVerification first = mock(EnhancedVerification.class);
		EnhancedVerification second = mock(EnhancedVerification.class);
		when(first.allowed(any())).thenReturn(VerificationResult.passed());
		when(second.allowed(any())).thenReturn(VerificationResult.passed());
		EnhancedVerification chain = new ChainedEnhancedVerification(List.of(first, second));
		VerificationResult allowed = chain.allowed(new BodyRepeatableHttpServletRequest(mock(HttpServletRequest.class)));

		assertEquals(VerificationResult.Decision.SOFT_PASS, allowed.getDecision());

		verify(first).allowed(any());
		verify(second).allowed(any());

	}

	@Test
	public void chainedVerificationFailsIfOneFails() throws IOException {

		EnhancedVerification first = mock(EnhancedVerification.class);
		EnhancedVerification second = mock(EnhancedVerification.class);
		EnhancedVerification third = mock(EnhancedVerification.class);
		when(first.allowed(any())).thenReturn(VerificationResult.passed());
		when(second.allowed(any())).thenReturn(VerificationResult.generalRulesViolation());
		when(third.allowed(any())).thenReturn(VerificationResult.passed());
		EnhancedVerification chain = new ChainedEnhancedVerification(List.of(first, second, third));
		VerificationResult allowed = chain.allowed(new BodyRepeatableHttpServletRequest(mock(HttpServletRequest.class)));

		assertEquals(VerificationResult.Decision.SOFT_FAIL, allowed.getDecision());

	}

	@Test
	public void hardPassWins() throws IOException {

		EnhancedVerification first = mock(EnhancedVerification.class);
		EnhancedVerification second = mock(EnhancedVerification.class);
		EnhancedVerification third = mock(EnhancedVerification.class);
		when(first.allowed(any())).thenReturn(VerificationResult.generic());
		when(second.allowed(any())).thenReturn(new VerificationResult(VerificationResult.Decision.HARD_PASS));
		when(third.allowed(any())).thenReturn(VerificationResult.generalRulesViolation());
		EnhancedVerification chain = new ChainedEnhancedVerification(List.of(first, second, third));
		VerificationResult allowed = chain.allowed(new BodyRepeatableHttpServletRequest(mock(HttpServletRequest.class)));

		assertEquals(VerificationResult.Decision.HARD_PASS, allowed.getDecision());

	}

}
