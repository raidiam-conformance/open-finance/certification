package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import net.openid.conformance.extensions.yacs.pcm.ConsentTracker;
import net.openid.conformance.extensions.yacs.pcm.PcmEvent;
import net.openid.conformance.extensions.yacs.pcm.PcmEventEmitter;
import net.openid.conformance.extensions.yacs.pcm.PcmRepository;
import net.openid.conformance.extensions.yacs.pcm.TrackedConsent;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bson.Document;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;

import javax.net.ssl.SSLContext;
import java.nio.charset.StandardCharsets;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PcmEventEmitterTests {

	private static TinyPki pki;
	private static String CLIENT_ORG = "clientorg";
	private static String CLIENT_SSID = "clientssid";
	private static String SERVER_ORG = "serverorg";
	private static TinyPki.Bundle clientCert;
	private static TinyPki.Bundle serverCert;
	private TinyWebServer tws;

	private EndpointDecorationService endpointDecorationService = new EndpointDecorationService();
	private ServerInfoTemplate serverInfoTemplate = new ServerInfoTemplate();





	@Test
	public void extractsEventDataCorrectly() throws Exception {

		String fapiInteractionId = UUID.randomUUID().toString();
		CountDownLatch latch = new CountDownLatch(1);

		Environment env = new Environment();
		env.putString("config", "alias", SERVER_ORG);
		String tid = "abcde";
		PcmRepository repository = mock(PcmRepository.class);
		ConsentTracker tracker = new ConsentTracker();

		tws = new TinyWebServer(serverCert);
		new Thread(tws).start();
		int port = tws.port();
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(clientCert.keyManagers(), pki.trustStore(), new SecureRandom());

		HttpClientBuilder builder = HttpClientBuilder.create()
			.setSSLContext(sslContext)
			.useSystemProperties();
		builder.addInterceptorLast(wrapWithLatch(latch, new PcmEventEmitter(repository, endpointDecorationService, tracker, env, tid, serverInfoTemplate)));
		CloseableHttpClient client = builder.build();

		HttpPost request = new HttpPost(String.format("https://localhost:%d/test/endpoint", port));
		request.addHeader("x-fapi-interaction-id", fapiInteractionId);

		String jsonRequestBody = "{ \"key\": \"value\" }";
		StringEntity entity = new StringEntity(jsonRequestBody, StandardCharsets.UTF_8);
		entity.setContentType("application/json");
		request.setEntity(entity);

		CloseableHttpResponse response = client.execute(request);
		latch.await(1, TimeUnit.SECONDS);
		assertEquals(200, response.getStatusLine().getStatusCode());

		ArgumentCaptor<PcmEvent> captor = ArgumentCaptor.forClass(PcmEvent.class);
		verify(repository).save(captor.capture());

		PcmEvent event = captor.getValue();

		assertNotNull(event);
		assertEquals("/test/endpoint", event.getEndpoint());
		assertEquals("POST", event.getHttpMethod());
		assertEquals(CLIENT_ORG, event.getClientOrgId());
		assertEquals(CLIENT_SSID, event.getClientSSId());
		assertEquals(SERVER_ORG, event.getServerOrgId());
		assertEquals("localhost", event.getEndpointUriPrefix());
		assertEquals(fapiInteractionId, event.getFapiInteractionId());
		assertEquals("CLIENT", event.getRole());
		assertEquals(200, event.getStatusCode());
	}

	@Test
	public void extractsJwtConsentCorrectly() throws Exception {

		String fapiInteractionId = UUID.randomUUID().toString();
		CountDownLatch latch = new CountDownLatch(1);
		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
		.claim("data", Map.of("consentId", "12345"))
			.build();
		JWSHeader header = new JWSHeader(JWSAlgorithm.RS256);
		SignedJWT jwt = new SignedJWT(header, claimsSet);
		clientCert.sign(jwt); // obviously this wouldn't be signed by the client cert, but meh it just needs a signature
		String jwtString = jwt.serialize();
		JsonObject consentResponse = new JsonObject();
		consentResponse.addProperty("body", jwtString);
		consentResponse.addProperty("status", 200);

		Environment env = new Environment();
		env.putObject("consent_endpoint_response_full", consentResponse);

		String tid = "abcde";
		PcmRepository repository = mock(PcmRepository.class);
		ConsentTracker tracker = new ConsentTracker();

		var tws = new TinyWebServer(serverCert);
		new Thread(tws).start();
		int port = tws.port();
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(clientCert.keyManagers(), pki.trustStore(), new SecureRandom());

		HttpClientBuilder builder = HttpClientBuilder.create()
			.setSSLContext(sslContext)
			.useSystemProperties();
		builder.addInterceptorLast(wrapWithLatch(latch, new PcmEventEmitter(repository, endpointDecorationService, tracker, env, tid,serverInfoTemplate)));
		CloseableHttpClient client = builder.build();
		HttpUriRequest request = new HttpPost(String.format("https://localhost:%d/reg",port));
		request.addHeader("x-fapi-interaction-id", fapiInteractionId);
		CloseableHttpResponse response = client.execute(request);
		latch.await(1, TimeUnit.SECONDS);
		assertEquals(200, response.getStatusLine().getStatusCode());

		List<TrackedConsent> consents = tracker.getConsentsForTest(tid);
		assertNotNull(consents);
		assertEquals(1, consents.size());
		TrackedConsent consent = consents.get(0);
		assertEquals("12345", consent.getConsentId());

	}

	@Test
	public void extractsJsonConsentCorrectly() throws Exception {

		String fapiInteractionId = UUID.randomUUID().toString();
		CountDownLatch latch = new CountDownLatch(1);
		JsonObject consentObject = new JsonObject();
		JsonObject data = new JsonObject();
		data.addProperty("consentId", "12345");
		consentObject.add("data", data);
		JsonObject consentResponse = new JsonObject();
		consentResponse.addProperty("body", consentObject.toString());
		consentResponse.addProperty("status", 200);

		Environment env = new Environment();
		env.putObject("consent_endpoint_response_full", consentResponse);

		String tid = "abcde";
		PcmRepository repository = mock(PcmRepository.class);
		ConsentTracker tracker = new ConsentTracker();

		var tws = new TinyWebServer(serverCert);
		new Thread(tws).start();
		int port = tws.port();
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(clientCert.keyManagers(), pki.trustStore(), new SecureRandom());

		HttpClientBuilder builder = HttpClientBuilder.create()
			.setSSLContext(sslContext)
			.useSystemProperties();
		builder.addInterceptorLast(wrapWithLatch(latch, new PcmEventEmitter(repository, endpointDecorationService, tracker, env, tid, serverInfoTemplate)));
		CloseableHttpClient client = builder.build();
		HttpUriRequest request = new HttpPost(String.format("https://localhost:%d/reg",port));
		request.addHeader("x-fapi-interaction-id", fapiInteractionId);
		CloseableHttpResponse response = client.execute(request);
		latch.await(1, TimeUnit.SECONDS);
		assertEquals(200, response.getStatusLine().getStatusCode());

		List<TrackedConsent> consents = tracker.getConsentsForTest(tid);
		assertNotNull(consents);
		assertEquals(1, consents.size());
		TrackedConsent consent = consents.get(0);
		assertEquals("12345", consent.getConsentId());

	}

	@Test
	public void extractsJwtRecurringConsentCorrectly() throws Exception {

		String fapiInteractionId = UUID.randomUUID().toString();
		CountDownLatch latch = new CountDownLatch(1);
		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
			.claim("data", Map.of("recurringConsentId", "12345"))
			.build();
		JWSHeader header = new JWSHeader(JWSAlgorithm.RS256);
		SignedJWT jwt = new SignedJWT(header, claimsSet);
		clientCert.sign(jwt); // obviously this wouldn't be signed by the client cert, but meh it just needs a signature
		String jwtString = jwt.serialize();
		JsonObject consentResponse = new JsonObject();
		consentResponse.addProperty("body", jwtString);
		consentResponse.addProperty("status", 200);

		Environment env = new Environment();
		env.putObject("consent_endpoint_response_full", consentResponse);

		String tid = "abcde";
		PcmRepository repository = mock(PcmRepository.class);
		ConsentTracker tracker = new ConsentTracker();

		var tws = new TinyWebServer(serverCert);
		new Thread(tws).start();
		int port = tws.port();
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(clientCert.keyManagers(), pki.trustStore(), new SecureRandom());

		HttpClientBuilder builder = HttpClientBuilder.create()
			.setSSLContext(sslContext)
			.useSystemProperties();
		builder.addInterceptorLast(wrapWithLatch(latch, new PcmEventEmitter(repository, endpointDecorationService, tracker, env, tid,serverInfoTemplate)));
		CloseableHttpClient client = builder.build();
		HttpUriRequest request = new HttpPost(String.format("https://localhost:%d/reg",port));
		request.addHeader("x-fapi-interaction-id", fapiInteractionId);
		CloseableHttpResponse response = client.execute(request);
		latch.await(1, TimeUnit.SECONDS);
		assertEquals(200, response.getStatusLine().getStatusCode());

		List<TrackedConsent> consents = tracker.getConsentsForTest(tid);
		assertNotNull(consents);
		assertEquals(1, consents.size());
		TrackedConsent consent = consents.get(0);
		assertEquals("12345", consent.getConsentId());

	}

	@Test
	public void extractsConsentFromUrlWhenMissingFromJsonCorrectly() throws Exception {

		String fapiInteractionId = UUID.randomUUID().toString();
		CountDownLatch latch = new CountDownLatch(1);
		JsonObject consentObject = new JsonObject();
		JsonObject consentResponse = new JsonObject();
		consentResponse.addProperty("body", consentObject.toString());
		consentResponse.addProperty("status", 200);

		Environment env = new Environment();
		env.putObject("consent_endpoint_response_full", consentResponse);
		env.putString("protected_resource_url", "https://auth.fapithings.com/consent/12345");
		String tid = "abcde";
		PcmRepository repository = mock(PcmRepository.class);
		ConsentTracker tracker = new ConsentTracker();

		var tws = new TinyWebServer(serverCert);
		new Thread(tws).start();
		int port = tws.port();
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(clientCert.keyManagers(), pki.trustStore(), new SecureRandom());

		HttpClientBuilder builder = HttpClientBuilder.create()
			.setSSLContext(sslContext)
			.useSystemProperties();
		builder.addInterceptorLast(wrapWithLatch(latch, new PcmEventEmitter(repository, endpointDecorationService, tracker, env, tid, serverInfoTemplate)));
		CloseableHttpClient client = builder.build();
		HttpUriRequest request = new HttpPost(String.format("https://localhost:%d/reg",port));
		request.addHeader("x-fapi-interaction-id", fapiInteractionId);
		CloseableHttpResponse response = client.execute(request);
		latch.await(1, TimeUnit.SECONDS);
		assertEquals(200, response.getStatusLine().getStatusCode());

		List<TrackedConsent> consents = tracker.getConsentsForTest(tid);
		assertNotNull(consents);
		assertEquals(1, consents.size());
		TrackedConsent consent = consents.get(0);
		assertEquals("12345", consent.getConsentId());

	}

	@Test
	public void extractsConsentFromUrlWhenMissingFromJwtCorrectly() throws Exception {

		String fapiInteractionId = UUID.randomUUID().toString();
		CountDownLatch latch = new CountDownLatch(1);
		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
			.build();
		JWSHeader header = new JWSHeader(JWSAlgorithm.RS256);
		SignedJWT jwt = new SignedJWT(header, claimsSet);
		clientCert.sign(jwt);
		String jwtString = jwt.serialize();
		JsonObject consentResponse = new JsonObject();
		consentResponse.addProperty("body", jwtString);
		consentResponse.addProperty("status", 200);

		Environment env = new Environment();
		env.putObject("consent_endpoint_response_full", consentResponse);
		env.putString("protected_resource_url", "https://auth.fapithings.com/consent/12345");

		String tid = "abcde";
		PcmRepository repository = mock(PcmRepository.class);
		ConsentTracker tracker = new ConsentTracker();

		var tws = new TinyWebServer(serverCert);
		new Thread(tws).start();
		int port = tws.port();
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(clientCert.keyManagers(), pki.trustStore(), new SecureRandom());

		HttpClientBuilder builder = HttpClientBuilder.create()
			.setSSLContext(sslContext)
			.useSystemProperties();
		builder.addInterceptorLast(wrapWithLatch(latch, new PcmEventEmitter(repository, endpointDecorationService, tracker, env, tid, serverInfoTemplate)));
		CloseableHttpClient client = builder.build();
		HttpUriRequest request = new HttpPost(String.format("https://localhost:%d/reg",port));
		request.addHeader("x-fapi-interaction-id", fapiInteractionId);
		CloseableHttpResponse response = client.execute(request);
		latch.await(1, TimeUnit.SECONDS);
		assertEquals(200, response.getStatusLine().getStatusCode());

		List<TrackedConsent> consents = tracker.getConsentsForTest(tid);
		assertNotNull(consents);
		assertEquals(1, consents.size());
		TrackedConsent consent = consents.get(0);
		assertEquals("12345", consent.getConsentId());

	}



	@ParameterizedTest
	@ValueSource(strings = {
		"/open-banking/enrollments/v1/enrollments/urn:c6bank:01JCJWQ769NEXT3QE54E5ATSY7/fido-sign-options",
		"/open-banking/enrollments/v1/enrollments",
		"/open-banking/enrollments/v1/consents/urn:c6bank:01JCJWQ769NEXT3QE54E5ATSY7/authorise",
		"/open-banking/enrollments/v2/enrollments/urn:c6bank:01JCJWQ769NEXT3QE54E5ATSY7/fido-sign-options",
		"/open-banking/enrollments/v2/enrollments",
		"/open-banking/enrollments/v2/consents/urn:c6bank:01JCJWQ769NEXT3QE54E5ATSY7/authorise",
	})
	public void additionalInfoIsGenerated(String endpoint) throws Exception {


		String fapiInteractionId = UUID.randomUUID().toString();
		CountDownLatch latch = new CountDownLatch(1);

		Environment env = new Environment();
		env.putString("config", "alias", SERVER_ORG);
		String tid = "abcde";
		PcmRepository repository = mock(PcmRepository.class);
		ConsentTracker tracker = new ConsentTracker();

		tws = new TinyWebServer(serverCert);
		new Thread(tws).start();
		int port = tws.port();
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(clientCert.keyManagers(), pki.trustStore(), new SecureRandom());

		HttpClientBuilder builder = HttpClientBuilder.create()
			.setSSLContext(sslContext)
			.useSystemProperties();
		builder.addInterceptorLast(wrapWithLatch(latch, new PcmEventEmitter(repository, endpointDecorationService, tracker, env, tid, serverInfoTemplate)));
		CloseableHttpClient client = builder.build();

		HttpUriRequest request = new HttpPost(String.format("https://localhost:%d/%s", port, endpoint));
		request.addHeader("x-fapi-interaction-id", fapiInteractionId);
		CloseableHttpResponse response = client.execute(request);
		latch.await(1, TimeUnit.SECONDS);
		assertEquals(200, response.getStatusLine().getStatusCode());
		String string = EntityUtils.toString(response.getEntity());

		ArgumentCaptor<PcmEvent> captor = ArgumentCaptor.forClass(PcmEvent.class);
		verify(repository).save(captor.capture());

		PcmEvent event = captor.getValue();

		assertNotNull(event);
		assertEquals("POST", event.getHttpMethod());
		assertNotEquals(new Document(), event.getAdditionalInfo());
	}

	@Test
	public void pkiTest() throws Exception {
		Provider provider = new BouncyCastleProvider();
		TinyPki pki = new TinyPki("iss", provider);

		TinyPki.Bundle issuer = pki.getIssuer();
		X509Certificate issuerIssuer = issuer.getIssuer().getCertificate();

		assertIssuedBy(issuerIssuer, issuer.getCertificate());

		TinyPki.Bundle bundle = pki.issueCert("client", "client", "ssid");

		assertIssuedBy(bundle.getCertificate(), issuerIssuer);

	}

	void assertIssuedBy(X509Certificate cert, X509Certificate issuer) {
		try {
			cert.verify(issuer.getPublicKey());
		} catch (Exception e) {
			fail("Certificate not issued by issuer");
		}
	}

	@BeforeAll
	public static void setupPki() throws Exception {
		pki = new TinyPki("issuer one", new BouncyCastleProvider());
		clientCert = pki.issueCert("client", CLIENT_ORG, CLIENT_SSID);
		serverCert = pki.issueCert("localhost", SERVER_ORG, null);
	}

	@AfterEach
	public void tearDown() {
		if(tws != null) {
			tws.stop();
		}
	}


	private HttpResponseInterceptor wrapWithLatch(CountDownLatch latch, PcmEventEmitter pcmEventEmitter) {
		return (httpResponse, httpContext) -> {
			pcmEventEmitter.process(httpResponse, httpContext);
			latch.countDown();
		};
	}

}
