package net.openid.conformance.extensions;

import net.openid.conformance.token.TokenService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.web.client.RestTemplate;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.Map;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

public class CertificationOffloadFilterTests {

	private RestTemplate restTemplate;
	private MockRestServiceServer mockServer;
	private TokenService tokenService = Mockito.mock(TokenService.class);

	@Test
	public void ignoresIrrelevantUrls() throws Exception {

		String uri = "http://localhost:8080/api/certoffload";

		mockServer.expect(ExpectedCount.never(),
			requestTo(uri));

		HttpServletRequest request = new MockHttpServletRequest("/api/plan/info", "GET");
		HttpServletResponse response = new MockHttpServletResponse();
		FilterChain filterChain = new MockFilterChain();
		CertificationOffloadHelper helper = new CertificationOffloadHelper(restTemplate, uri, "credentials", tokenService);
		CertificationOffloadFilter filter = new CertificationOffloadFilter(helper);
		filter.doFilter(request, response, filterChain);

		mockServer.verify();

	}

	@Test
	public void successfullyNotifiesOfACertification() throws Exception {

		String uri = "http://localhost:8080/api/certoffload";
		String tokenToReturn = "abcde12345";

		when(tokenService.createToken(false)).thenReturn(Map.of("token", tokenToReturn));

		when(tokenService.createToken(false)).thenReturn(Map.of("token", "abcde12345"));

		mockServer.expect(ExpectedCount.once(),
				requestTo(new URI(uri)))
			.andExpect(method(HttpMethod.POST))
			.andExpect(request -> {
				String body = request.getBody().toString();
				if(!body.contains("plan=an7HDk78j")) {
					fail("Expected body to contain plan=an7HDk78j but got " + body);
				}
				if(!body.contains("apiKey=" + tokenToReturn)) {
					fail("Expected body to contain apiKey=" + tokenToReturn + " but got " + body);
				}
			})
			.andExpect(noAuthHeaderPresent())
			.andRespond(withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.body("")
			);

		MockHttpServletRequest request = new MockHttpServletRequest("POST","http://localhost/api/plan/an7HDk78j/certificationpackage");
		request.setPathInfo("/api/plan/an7HDk78j/certificationpackage");
		HttpServletResponse response = new MockHttpServletResponse();
		FilterChain filterChain = new MockFilterChain();
		CertificationOffloadHelper helper = new CertificationOffloadHelper(restTemplate, uri, null, tokenService);
		CertificationOffloadFilter filter = new CertificationOffloadFilter(helper);
		filter.doFilter(request, response, filterChain);

		mockServer.verify();

	}

	@Test
	public void successfullyNotifiesOfACertificationWithCredentials() throws Exception {

		String uri = "http://localhost:8080/api/certoffload";
		String credentials = "abcde12345";
		String userApiKey = "wiruhgfoeruh";

		when(tokenService.createToken(false)).thenReturn(Map.of("token", userApiKey));

		mockServer.expect(ExpectedCount.once(),
				requestTo(new URI(uri)))
			.andExpect(method(HttpMethod.POST))
			.andExpect(request -> {
				String body = request.getBody().toString();
				if(!body.contains("plan=an7HDk78j")) {
					fail("Expected body to contain plan=an7HDk78j but got " + body);
				}
				if(!body.contains("apiKey=" + userApiKey)) {
					fail("Expected body to contain apiKey=" + userApiKey + " but got " + body);
				}
			})
			.andExpect(header("Authorization", "Bearer " + credentials))
			.andRespond(withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.body("")
			);

		MockHttpServletRequest request = new MockHttpServletRequest("POST","http://localhost/api/plan/an7HDk78j/certificationpackage");
		request.setPathInfo("/api/plan/an7HDk78j/certificationpackage");
		HttpServletResponse response = new MockHttpServletResponse();
		FilterChain filterChain = new MockFilterChain();
		CertificationOffloadHelper helper = new CertificationOffloadHelper(restTemplate, uri, credentials, tokenService);
		CertificationOffloadFilter filter = new CertificationOffloadFilter(helper);
		filter.doFilter(request, response, filterChain);

		mockServer.verify();

	}

	@Test
	public void stillProceedsIfNotOK() throws Exception {

		String uri = "http://localhost:8080/api/certoffload";
		String credentials = "abcde12345";
		String apiKeyForUser = "LFEIHgreoigergrw";

		when(tokenService.createToken(false)).thenReturn(Map.of("token", apiKeyForUser));

		mockServer.expect(ExpectedCount.once(),
				requestTo(new URI(uri)))
			.andExpect(method(HttpMethod.POST))
			.andExpect(request -> {
				String body = request.getBody().toString();
				if(!body.contains("plan=an7HDk78j")) {
					fail("Expected body to contain plan=an7HDk78j but got " + body);
				}
				if(!body.contains("apiKey=" + apiKeyForUser)) {
					fail("Expected body to contain apiKey=" + apiKeyForUser + " but got " + body);
				}
			})
			.andExpect(header("Authorization", "Bearer " + credentials))
			.andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
			);

		MockHttpServletRequest request = new MockHttpServletRequest("POST","http://localhost/api/plan/an7HDk78j/certificationpackage");
		request.setPathInfo("/api/plan/an7HDk78j/certificationpackage");
		HttpServletResponse response = new MockHttpServletResponse();
		FilterChain filterChain = new MockFilterChain();
		CertificationOffloadHelper helper = new CertificationOffloadHelper(restTemplate, uri, credentials, tokenService);
		CertificationOffloadFilter filter = new CertificationOffloadFilter(helper);
		filter.doFilter(request, response, filterChain);

		mockServer.verify();

	}

	private RequestMatcher noAuthHeaderPresent() {
		return request -> {
			if(request.getHeaders().containsKey("Authorization")) {
				throw new AssertionError("Expected no Authorization header but got " + request.getHeaders().get("Authorization"));
			}
		};
	}

	@Before
	public void init() {
		restTemplate = new RestTemplate();
		mockServer = MockRestServiceServer.createServer(restTemplate);
	}

}
