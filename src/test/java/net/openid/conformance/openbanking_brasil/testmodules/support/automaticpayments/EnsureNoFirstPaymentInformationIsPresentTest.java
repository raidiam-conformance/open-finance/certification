package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureNoFirstPaymentInformationIsPresent;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureNoFirstPaymentInformationIsPresentTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/GetRecurringConsents";

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingFirstPayment200Response.json")
	public void happyPathTest() {
		run(new EnsureNoFirstPaymentInformationIsPresent());
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(EnsureNoFirstPaymentInformationIsPresent.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(EnsureNoFirstPaymentInformationIsPresent.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(EnsureNoFirstPaymentInformationIsPresent.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build());
		unhappyPathTest("Could not find data.recurringConfiguration.automatic in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRecurringConfiguration200Response.json")
	public void unhappyPathTestMissingRecurringConfiguration() {
		unhappyPathTest("Could not find data.recurringConfiguration.automatic in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "Sweeping200Response.json")
	public void unhappyPathTestNotAutomatic() {
		unhappyPathTest("Could not find data.recurringConfiguration.automatic in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void unhappyPathTestHasFirstPaymentInformation() {
		unhappyPathTest("There is firstPayment information present in the response");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureNoFirstPaymentInformationIsPresent());
		assertTrue(error.getMessage().contains(message));
	}
}
