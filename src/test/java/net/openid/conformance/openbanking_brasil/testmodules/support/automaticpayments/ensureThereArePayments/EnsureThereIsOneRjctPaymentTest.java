package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureThereArePayments;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereIsOneRjctPayment;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureThereIsOneRjctPaymentTest extends AbstractEnsureThereArePaymentsTest {

	@Override
	protected AbstractEnsureThereArePayments condition() {
		return new EnsureThereIsOneRjctPayment();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "OneRjct.json")
	public void happyPathTest() {
		super.happyPathTest();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "MissingStatus.json")
	public void unhappyPathTestMissingStatus() {
		super.unhappyPathTestMissingStatus();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "NonExistingStatus.json")
	public void unhappyPathTestInvalidStatus() {
		super.unhappyPathTestInvalidStatus();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + ".json")
	public void unhappyPathTestStatusNotMatching() {
		super.unhappyPathTestStatusNotMatching();
	}
}
