package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@UseResurce("jsonResponses/resourcesAPI/v2/resourcesAPIResponseEmptyData.json")
public class AbstractPageSizeTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructureSimpleUrl()
	{
		environment.putString("protected_resource_url", "https://raidiamopin.com.br/open-insurance/insurance-patrimonial/resources/v1");
		run(new SetProtectedResourceUrlPageSize1000());
	}

	@Test
	public void validateStructurePageSizeAlreadyPresent()
	{
		String url = "https://raidiamopin.com.br/open-insurance/insurance-patrimonial/resources/v1";
		environment.putString("protected_resource_url", url + "?page-size=25");
		run(new SetProtectedResourceUrlPageSize1000());
		assertThat(environment.getString("protected_resource_url"), containsString(url + "?page-size=1000"));
	}

	@Test
	public void validateStructurePageSizeInvertedOrder() {

		String url = "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page-size=1&page=CAPITALIZATION_TITLES";
		environment.putString("protected_resource_url", url);
		run(new SetProtectedResourceUrlPageSize1000());
		assertThat(environment.getString("protected_resource_url"), containsString(url.replaceFirst("page-size=1", "page-size=1000")));
	}

	@Test
	public void validateStructurePageSizeMultiplePageSize() {

		String url = "https://mtls-btgseguros.openinsurance-dev.btgpactual.com/open-insurance/resources/v1?page-size=1&page=CAPITALIZATION_TITLES&page-size=10";
		environment.putString("protected_resource_url", url);
		ConditionError conditionError = runAndFail(new SetProtectedResourceUrlPageSize1000());
		assertThat(conditionError.getMessage(), containsString("resourceUrl provided contains multiple page-size parameters, please review the endpoint response."));
	}

	@Test
	public void validateStructurePage1()
	{
		String url ="https://raidiamopin.com.br/open-banking/credit-cards-accounts/v2/accounts?page=1";
		environment.putString("protected_resource_url", url);
		run(new SetProtectedResourceUrlPageSize1000());
		assertThat(environment.getString("protected_resource_url"), containsString(url + "&page-size=1000"));
	}
}
