package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ValidateEnrollmentWebhooks;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class ValidateEnrollmentWebhooksTest {

	private final int EXPECTED_WEBHOOKS_RECEIVED_AMOUNTS = 1;
	private final String ENROLLMENT_ID = "urn:bancoex:C1DD33123";

	protected Environment prepareEnvironment() {
		Environment env = new Environment();
		env.putString("time_of_enrollment_request", Instant.now().toString());
		env.putString("enrollment_id", ENROLLMENT_ID);
		JsonObject webhooksObj = new JsonObject();

		JsonArray webhooks = new JsonArray();

		JsonObject webhook = new JsonObject();

		JsonObject webhookHeaders = new JsonObject();
		webhookHeaders.addProperty("x-webhook-interaction-id", "8e4f97e6-7769-11ee-b962-0242ac120002");
		webhookHeaders.addProperty("content-type", "application/json");
		webhook.add("headers", webhookHeaders);

		JsonObject body = new JsonObject();
		JsonObject data = new JsonObject();
		data.addProperty("timestamp", Instant.now().toString());
		body.add("data", data);
		webhook.addProperty("body", body.toString());

		webhook.addProperty("path", "open-banking/webhook/v1/enrollments/v1/enrollments/" + ENROLLMENT_ID);
		webhook.addProperty("type", "enrollment");

		webhooks.add(webhook);
		webhooksObj.add("webhooks", webhooks);

		env.putObject("webhooks_received_enrollment", webhooksObj);
		return env;
	}

	protected ValidateEnrollmentWebhooks createCondition() {
		ValidateEnrollmentWebhooks condition = new ValidateEnrollmentWebhooks();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		return condition;
	}

	@Test
	public void testHappyPath() {
		Environment env = prepareEnvironment();
		ValidateEnrollmentWebhooks condition = createCondition();
		condition.evaluate(env);
	}

	@Test
	public void testMissingEnrollmentId() {
		Environment env = prepareEnvironment();
		env.removeNativeValue("enrollment_id");
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "enrollment_id not found";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testMissingWebhooks() {
		Environment env = prepareEnvironment();
		env.removeObject("webhooks_received_enrollment");
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "Webhooks not found";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testWebhookObjectIsNotJsonArray() {
		Environment env = prepareEnvironment();
		JsonObject webhook = env.getObject("webhooks_received_enrollment");
		webhook.addProperty("webhooks", "Wrong type");
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ClassCastException.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "cannot be cast to class com.google.gson.JsonArray";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testMissingBody() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		webhook.remove("body");
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "No webhook body found in the webhook request";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testMissingData() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		String webhookBody = OIDFJSON.getString(webhook.get("body"));
		JsonObject webhookBodyJson = JsonParser.parseString(webhookBody).getAsJsonObject();
		webhookBodyJson.remove("data");
		webhook.addProperty("body", webhookBodyJson.toString());
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "Webhook response body does not contain data field";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testMissingPath() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		webhook.remove("path");
		ValidateEnrollmentWebhooks condition = createCondition();

		assertThrows(NullPointerException.class, () -> {
			condition.evaluate(env);
		});
	}

	@Test
	public void testWebhookMissingHeader() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		webhook.remove("headers");
		ValidateEnrollmentWebhooks condition = createCondition();

		assertThrows(NullPointerException.class, () -> {
			condition.evaluate(env);
		});
	}

	@Test
	public void testWebhookHeaderMissingXWebhookInteractionId() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		JsonObject headers = webhook.get("headers").getAsJsonObject();
		headers.remove("x-webhook-interaction-id");
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "Headers returned in webhook response do not contain x-webhook-interaction-id";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testWebhookMissingContentType() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		JsonObject headers = webhook.get("headers").getAsJsonObject();
		headers.remove("content-type");
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "Headers returned in webhook response do not contain content-type";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testWebhookWrongContentType() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		JsonObject headers = webhook.get("headers").getAsJsonObject();
		headers.addProperty("content-type", "application/jwt");
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "content-type not found or not equal to application/json";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testDifferentAmountOfWebhooksThanExpected() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		webhooks.remove(0);
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = String.format("Expected %d webhooks, but received %d",
			EXPECTED_WEBHOOKS_RECEIVED_AMOUNTS, EXPECTED_WEBHOOKS_RECEIVED_AMOUNTS-1);
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testMissingTimestamp() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		String webhookBody = OIDFJSON.getString(webhook.get("body"));
		JsonObject webhookBodyJson = JsonParser.parseString(webhookBody).getAsJsonObject();
		JsonObject data = webhookBodyJson.getAsJsonObject("data");
		data.remove("timestamp");
		webhook.addProperty("body", webhookBodyJson.toString());
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "Webhook response body does not contain timestamp field";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testTimestampBeforeRequestTime() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		String webhookBody = OIDFJSON.getString(webhook.get("body"));
		JsonObject webhookBodyJson = JsonParser.parseString(webhookBody).getAsJsonObject();
		JsonObject data = webhookBodyJson.getAsJsonObject("data");
		data.addProperty("timestamp", Instant.now().minusSeconds(60).toString());
		webhook.addProperty("body", webhookBodyJson.toString());
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "Webhook timestamp is not within the time of the POST request and current time";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testTimestampAfterCurrentTime() {
		Environment env = prepareEnvironment();
		JsonObject webhooksObj = env.getObject("webhooks_received_enrollment");
		JsonArray webhooks = webhooksObj.get("webhooks").getAsJsonArray();
		JsonObject webhook = webhooks.get(0).getAsJsonObject();
		String webhookBody = OIDFJSON.getString(webhook.get("body"));
		JsonObject webhookBodyJson = JsonParser.parseString(webhookBody).getAsJsonObject();
		JsonObject data = webhookBodyJson.getAsJsonObject("data");
		data.addProperty("timestamp", Instant.now().plusSeconds(60).toString());
		webhook.addProperty("body", webhookBodyJson.toString());
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "Webhook timestamp is not within the time of the POST request and current time";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testEnrollmentIdNotMatching() {
		Environment env = prepareEnvironment();
		env.putString("enrollment_id", ENROLLMENT_ID.substring(1));
		ValidateEnrollmentWebhooks condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});

		String expectedErrorMessage = "Enrollment ID in webhook path does not match the enrollment ID in the request";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

}
