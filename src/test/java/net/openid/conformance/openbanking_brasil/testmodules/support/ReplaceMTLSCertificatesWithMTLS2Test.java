package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class ReplaceMTLSCertificatesWithMTLS2Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init(){
		JsonObject config = new JsonObject();
		config.add("mtls2",new JsonObject());
		environment.putObject("config",config);
	}

	@Test
	public void happyPath() {
		JsonObject obj = environment.getObject("config").get("mtls2").getAsJsonObject();
		obj.addProperty("cert","a1b2c3");
		obj.addProperty("key","a1b2c3");
		obj.addProperty("ca","a1b2c3");
		JsonObject alias = new JsonObject();
		alias.addProperty("key", "some_key");
		obj.add("mtls_alias",alias);
		ReplaceMTLSCertificatesWithMTLS2 condition = new ReplaceMTLSCertificatesWithMTLS2();
		run(condition);
	}

	@Test
	public void happyPathNoAlias() {
		JsonObject obj = environment.getObject("config").get("mtls2").getAsJsonObject();
		obj.addProperty("cert","a1b2c3");
		obj.addProperty("key","a1b2c3");
		obj.addProperty("ca","a1b2c3");
		ReplaceMTLSCertificatesWithMTLS2 condition = new ReplaceMTLSCertificatesWithMTLS2();
		run(condition);
	}

	@Test
	public void unhappyPathNoCerts() {
		ReplaceMTLSCertificatesWithMTLS2 condition = new ReplaceMTLSCertificatesWithMTLS2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("mtls2: one or more of the following are missing: cert, key, ca"));
	}
	@Test
	public void unhappyPathNoCert() {
		JsonObject obj = environment.getObject("config").get("mtls2").getAsJsonObject();
		obj.addProperty("key","a1b2c3");
		obj.addProperty("ca","a1b2c3");
		ReplaceMTLSCertificatesWithMTLS2 condition = new ReplaceMTLSCertificatesWithMTLS2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("mtls2: one or more of the following are missing: cert, key, ca"));
	}

	@Test
	public void unhappyPathNoKey() {
		JsonObject obj = environment.getObject("config").get("mtls2").getAsJsonObject();
		obj.addProperty("cert","a1b2c3");
		obj.addProperty("ca","a1b2c3");
		ReplaceMTLSCertificatesWithMTLS2 condition = new ReplaceMTLSCertificatesWithMTLS2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("mtls2: one or more of the following are missing: cert, key, ca"));
	}

	@Test
	public void unhappyPathNoCa() {
		JsonObject obj = environment.getObject("config").get("mtls2").getAsJsonObject();
		obj.addProperty("cert","a1b2c3");
		obj.addProperty("key","a1b2c3");
		ReplaceMTLSCertificatesWithMTLS2 condition = new ReplaceMTLSCertificatesWithMTLS2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("mtls2: one or more of the following are missing: cert, key, ca"));
	}

	@Test
	public void unhappyPathInvalidCert() {
		JsonObject obj = environment.getObject("config").get("mtls2").getAsJsonObject();
		obj.addProperty("cert","=");
		obj.addProperty("key","=");
		obj.addProperty("ca","=");
		ReplaceMTLSCertificatesWithMTLS2 condition = new ReplaceMTLSCertificatesWithMTLS2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Couldn't decode certificate, key, or CA chain from Base64"));
	}
}
