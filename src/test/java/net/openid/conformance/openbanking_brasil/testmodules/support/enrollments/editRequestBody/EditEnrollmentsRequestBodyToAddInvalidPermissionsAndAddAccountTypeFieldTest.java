package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditEnrollmentsRequestBodyToAddInvalidPermissionsAndAddAccountTypeFieldTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="jsonRequests/payments/enrollments/postEnrollments.json", key="resource_request_entity_claims")
	public void happyPathTest() {
		run(new EditEnrollmentsRequestBodyToAddInvalidPermissionsAndAddAccountTypeField());
		JsonArray permissions = environment
			.getElementFromObject("resource_request_entity_claims", "data.permissions")
			.getAsJsonArray();

		String[] permissionStrings = {"ACCOUNTS_READ", "ACCOUNTS_BALANCES_READ", "ACCOUNTS_BALANCES_READ",
			"ACCOUNTS_BALANCES_READ", "RESOURCES_READ"};
		JsonArray expectedPermissions = new JsonArray();
		for (String permissionString : permissionStrings) {
			expectedPermissions.add(permissionString);
		}

		JsonObject debtorAccount = environment
			.getElementFromObject("resource_request_entity_claims", "data.debtorAccount")
			.getAsJsonObject();

		assertEquals(expectedPermissions, permissions);
		assertEquals("CACC", OIDFJSON.getString(debtorAccount.get("accountType")));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource_request_entity_claims", new JsonObject());
		ConditionError error = runAndFail(new EditEnrollmentsRequestBodyToAddInvalidPermissionsAndAddAccountTypeField());
		assertTrue(error.getMessage().contains("Could not find data inside the body"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResourceRequestEntityClaims() {
		run(new EditEnrollmentsRequestBodyToAddInvalidPermissionsAndAddAccountTypeField());
	}
}
