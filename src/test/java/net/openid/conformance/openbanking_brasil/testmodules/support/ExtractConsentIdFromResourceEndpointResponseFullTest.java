package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtractConsentIdFromResourceEndpointResponseFullTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/account/readConsentResponse.json")
	public void testHappyPath() {
		ExtractConsentIdFromResourceEndpointResponseFull condition = new ExtractConsentIdFromResourceEndpointResponseFull();
		run(condition);
		String consentId = environment.getString("consent_id");
		assertTrue(!Strings.isNullOrEmpty(consentId));
	}

	@Test
	@UseResurce("jsonResponses/account/accountV2/balancesV2/accountBalancesResponse.json")
	public void testUnhappyPathNoConsentId() {
		ExtractConsentIdFromResourceEndpointResponseFull condition = new ExtractConsentIdFromResourceEndpointResponseFull();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Couldn't find data.consentId in the response"));
	}

	@Test
	public void testUnhappyPathNoBody() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		ExtractConsentIdFromResourceEndpointResponseFull condition = new ExtractConsentIdFromResourceEndpointResponseFull();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}
}
