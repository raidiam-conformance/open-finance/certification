package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractEnsureEnrollmentVariableSetToValue;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureDailyLimitSetTo1BRL;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureTransactionLimitSetTo1BRL;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.AbstractExtractFromEnrollmentResponse;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AbstractEnsureEnrollmentVariableSetToValueTest extends AbstractJsonResponseConditionUnitTest {
	private class ExtractExampleLimitFromEnrollmentResponse extends AbstractEnsureEnrollmentVariableSetToValue {

		@Override
		protected String fieldName() {
			return "example";
		}

		@Override
		protected String envVarName() {
			return "example";
		}

		@Override
		protected String value() {
			return "example";
		}
	}

	@Test
	public void happyPathTestDailyLimit() {
		JsonObject data = new JsonObject();
		data.addProperty("dailyLimit", "1.00");
		JsonObject body = new JsonObject();
		body.add("data", data);
		environment.putObject(
			AbstractExtractFromEnrollmentResponse.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", body.toString()).build()
		);
		EnsureDailyLimitSetTo1BRL condition = new EnsureDailyLimitSetTo1BRL();
		run(condition);
		assertEquals("1.00", environment.getString("daily_limit"));
	}

	@Test
	public void happyPathTestTransactionLimit() {
		JsonObject data = new JsonObject();
		data.addProperty("transactionLimit", "1.00");
		JsonObject body = new JsonObject();
		body.add("data", data);
		environment.putObject(
			AbstractExtractFromEnrollmentResponse.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", body.toString()).build()
		);
		EnsureTransactionLimitSetTo1BRL condition = new EnsureTransactionLimitSetTo1BRL();
		run(condition);
		assertEquals("1.00", environment.getString("transaction_limit"));
	}

	@Test
	public void unhappyPathTestDifferentLimit() {
		JsonObject data = new JsonObject();
		data.addProperty("example", "incorrect example");
		JsonObject body = new JsonObject();
		body.add("data", data);
		environment.putObject(
			AbstractExtractFromEnrollmentResponse.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", body.toString()).build()
		);
		ExtractExampleLimitFromEnrollmentResponse condition = new ExtractExampleLimitFromEnrollmentResponse();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Value is incorrect"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResponse() {
		ExtractExampleLimitFromEnrollmentResponse condition = new ExtractExampleLimitFromEnrollmentResponse();
		run(condition);
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(AbstractExtractFromEnrollmentResponse.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestInvalidJwt() {
		environment.putObject(
			AbstractExtractFromEnrollmentResponse.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya1.b2.c3").build()
		);
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(
			AbstractExtractFromEnrollmentResponse.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build()
		);
		unhappyPathTest("Could not find data.example in enrollments response");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getEnrollments.json")
	public void unhappyPathTestMissingField() {
		unhappyPathTest("Could not find data.example in enrollments response");
	}

	private void unhappyPathTest(String errorMessage) {
		ExtractExampleLimitFromEnrollmentResponse condition = new ExtractExampleLimitFromEnrollmentResponse();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(errorMessage));
	}
}
