package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;


public class GetRecurringPixListOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/paymentListResponse/getRecurringPaymentResponseWith3Payments.json")
	public void happyPath() {
		run(new GetRecurringPixListOASValidatorV1());
	}

}
