package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentEndpoint;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SetProtectedResourceUrlToRecurringPaymentEndpointTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "config";
	private static final String PATH = "resource.resourceUrl";
	private static final String BASE_URL = "https://www.example.com/";
	private static final String PAYMENT_ID = "123456";

	@Before
	public void init() {
		environment.putString("payment_id", PAYMENT_ID);
	}

	@Test
	public void happyPathTest() {
		String originalUrl = BASE_URL + "automatic-payments/v2/pix/recurring-payments";
		environment.putString(KEY, PATH, originalUrl);
		run(new SetProtectedResourceUrlToRecurringPaymentEndpoint());
		String resourceUrl = environment.getString("protected_resource_url");
		String expectedUrl = String.format("%s/%s", originalUrl, PAYMENT_ID);
		assertEquals(expectedUrl, resourceUrl);
	}

	@Test
	public void unhappyPathTestDifferentBaseApi() {
		environment.putString(KEY, PATH, BASE_URL + "payments/v2/payments");
		ConditionError error = runAndFail(new SetProtectedResourceUrlToRecurringPaymentEndpoint());
		assertTrue(error.getMessage().contains("Base url path has not been correctly provided. It must match the regex"));
	}
}
