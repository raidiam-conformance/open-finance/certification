package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;

import static org.junit.Assert.assertTrue;

public abstract class AbstractEnrollmentsOASV2ValidatorsTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String JSON_PATH = "jsonResponses/enrollments/v2/";

	protected abstract AbstractEnrollmentsOASValidatorV2 validator();

	protected void happyPathTest(int status) {
		setStatus(status);
		run(validator());
	}

	protected void unhappyPathTest(int status, String message) {
		setStatus(status);
		ConditionError error = runAndFail(validator());
		assertTrue(error.getMessage().contains(message));
	}
}
