package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureBusinessEntityIdentificationIsPresent;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureBusinessEntityIdentificationIsPresentTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		environment.putBoolean("continue_test", true);
	}

	@Test
	public void happyPathTest() {
		environment.putString("config", "resource.businessEntityIdentification", "123456");
		EnsureBusinessEntityIdentificationIsPresent condition = new EnsureBusinessEntityIdentificationIsPresent();
		run(condition);
		assertTrue(environment.getBoolean("continue_test"));
	}

	@Test
	public void unhappyPathTestMissingField() {
		environment.putObject("config", new JsonObject());
		EnsureBusinessEntityIdentificationIsPresent condition = new EnsureBusinessEntityIdentificationIsPresent();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Business Entity CNPJ field is empty. Institution is assumed to not have this functionality and the test will be skipped."));
		assertFalse(environment.getBoolean("continue_test"));
	}
}
