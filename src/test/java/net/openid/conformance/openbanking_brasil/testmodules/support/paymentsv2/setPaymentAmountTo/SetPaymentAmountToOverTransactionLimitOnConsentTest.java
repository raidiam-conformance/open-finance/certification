package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo;


import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SetPaymentAmountToOverTransactionLimitOnConsentTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		JsonObject payment = new JsonObject();
		payment.addProperty("amount","100.00");
		JsonObject data = new JsonObject();
		data.add("payment",payment);
		JsonObject brazilPaymentConsent = new JsonObject();
		brazilPaymentConsent.add("data",data);
		JsonObject resource = new JsonObject();
		resource.add("brazilPaymentConsent",brazilPaymentConsent);
		environment.putObject("resource",resource);

		environment.putString("transaction_limit","10000.00");

	}

	@Test
	public void happyPathTest() {
		SetPaymentAmountToOverTransactionLimitOnConsent condition = new SetPaymentAmountToOverTransactionLimitOnConsent();
		run(condition);
	}

	@Test
	public void happyPathTestAmountWithComa() {
		environment.putString("resource","brazilPaymentConsent.data.payment.amount", "100,00");
		SetPaymentAmountToOverTransactionLimitOnConsent condition = new SetPaymentAmountToOverTransactionLimitOnConsent();
		run(condition);
	}

	@Test
	public void unhappyPathTestLimitWithComa() {
		environment.putString("transaction_limit","10000,00");
		try{
			SetPaymentAmountToOverTransactionLimitOnConsent condition = new SetPaymentAmountToOverTransactionLimitOnConsent();
			run(condition);
			assertThat("", containsString("For input string: \"10000,00\""));
		} catch (NumberFormatException e) {
			assertThat(e.getMessage(), containsString("For input string: \"10000,00\""));
		}
	}

	@Test
	public void unhappyPathTestAmountAndLimitWithComa() {
		environment.putString("resource","brazilPaymentConsent.data.payment.amount", "100,00");
		environment.putString("transaction_limit","10000,00");
		try{
			SetPaymentAmountToOverTransactionLimitOnConsent condition = new SetPaymentAmountToOverTransactionLimitOnConsent();
			run(condition);
			assertThat("", containsString("For input string: \"10000,00\""));
		} catch (NumberFormatException e) {
			assertThat(e.getMessage(), containsString("For input string: \"10000,00\""));
		}
	}

	@Test
	public void happyPathTestAmountAndLimitEqual() {
		environment.putString("transaction_limit","100.00");
		SetPaymentAmountToOverTransactionLimitOnConsent condition = new SetPaymentAmountToOverTransactionLimitOnConsent();
		run(condition);
	}

	@Test
	public void unhappyPathTestNoAmount() {
		environment.putObject("resource","brazilPaymentConsent.data.payment", new JsonObject());

		SetPaymentAmountToOverTransactionLimitOnConsent condition = new SetPaymentAmountToOverTransactionLimitOnConsent();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not parse amount"));
	}

	@Test
	public void unhappyPathTestNoBrazilPaymentConsent() {
		environment.putObject("resource","brazilPaymentConsent", new JsonObject());

		SetPaymentAmountToOverTransactionLimitOnConsent condition = new SetPaymentAmountToOverTransactionLimitOnConsent();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract"));
	}

}
