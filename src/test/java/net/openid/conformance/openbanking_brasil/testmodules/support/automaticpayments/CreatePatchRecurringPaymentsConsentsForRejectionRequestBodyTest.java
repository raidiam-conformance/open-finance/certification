package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRejectionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectedByEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectedFromEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectionReasonEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsStatusEnum;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreatePatchRecurringPaymentsConsentsForRejectionRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		CreatePatchRecurringPaymentsConsentsForRejectionRequestBody condition = new CreatePatchRecurringPaymentsConsentsForRejectionRequestBody();
		run(condition);

		JsonObject consentEndpointRequest = environment.getObject("consent_endpoint_request");

		JsonObject data = assertObjectFieldIsPresent(consentEndpointRequest, "data");

		String status = assertStringFieldIsPresent(data, "status");
		assertTrue(RecurringPaymentsConsentsStatusEnum.toSet().contains(status));

		JsonObject rejection = assertObjectFieldIsPresent(data, "rejection");

		String rejectedBy = assertStringFieldIsPresent(rejection, "rejectedBy");
		assertTrue(RecurringPaymentsConsentsRejectedByEnum.toSet().contains(rejectedBy));

		String rejectedFrom = assertStringFieldIsPresent(rejection, "rejectedFrom");
		assertTrue(RecurringPaymentsConsentsRejectedFromEnum.toSet().contains(rejectedFrom));

		JsonObject reason = assertObjectFieldIsPresent(rejection, "reason");

		String code = assertStringFieldIsPresent(reason, "code");
		assertTrue(RecurringPaymentsConsentsRejectionReasonEnum.toSet().contains(code));

		assertStringFieldIsPresent(reason, "detail");
	}

	private String assertStringFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonPrimitive() && obj.get(field).getAsJsonPrimitive().isString());
		return OIDFJSON.getString(obj.get(field));
	}

	private JsonObject assertObjectFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonObject());
		return obj.getAsJsonObject(field);
	}
}
