package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddAutomaticPaymentToTheResource;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddAutomaticPaymentToTheResourceTest extends AbstractJsonResponseConditionUnitTest {

	private static final String AMOUNT = "100.00";
	private static final String KEY = "resource";
	private static final String CONSENT_PATH = "brazilPaymentConsent";
	private static final String FILE_BASE_PATH = "jsonRequests/automaticPayments/consents/brazilPaymentConsent";
	private static final String CPF = "11111111111";
	private static final String CNPJ = "11111111111111";

	@Test
	@UseResurce(value = FILE_BASE_PATH + "WithSweepingField.json", key = KEY, path = CONSENT_PATH)
	public void happyPathTestWithBusinessEntity() {
		happyPathTest(true);
	}

	@Test
	@UseResurce(value = FILE_BASE_PATH + "WithCreditorWithCpf.json", key = "config", path = KEY+"."+CONSENT_PATH)
	public void happyPathTestWithoutBusinessEntity() {
		happyPathTest(false);
	}

	@Test
	@UseResurce(value = FILE_BASE_PATH + "WithSweepingField.json", key = KEY, path = CONSENT_PATH)
	public void happyPathTestWithSetLocalInstrument() {
		environment.putString("local_instrument", "MANU");
		happyPathTest(true);
	}

	@Test
	public void unhappyPathTestMissingResourceAndConfig() {
		unhappyPathTest("Could not find resource object");
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Could not extract the consent data from the resource");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, CONSENT_PATH, new JsonObject());
		unhappyPathTest("Could not extract the consent data from the resource");
	}

	@Test
	@UseResurce(value = FILE_BASE_PATH + "WithBusinessEntityMissingIdentification.json", key = KEY, path = CONSENT_PATH)
	public void unhappyPathTestWithBusinessEntityMissingIdentification() {
		unhappyPathTest("Could not find identification field inside businessEntity");
	}

	@Test
	@UseResurce(value = FILE_BASE_PATH + "WithLoggedUserMissingIdentification.json", key = KEY, path = CONSENT_PATH)
	public void unhappyPathTestWithoutBusinessEntityMissingIdentification() {
		unhappyPathTest("Could not find identification field inside loggedUser");
	}

	private void happyPathTest(boolean withBusinessEntity) {
		AddAutomaticPaymentToTheResource condition = new AddAutomaticPaymentToTheResource();
		run(condition);

		JsonObject payment = environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject();
		JsonObject data = assertObjectFieldIsPresent(payment, "data");

		String expectedEndToEndId = environment.getString("endToEndId");

		String endToEndId = assertStringFieldIsPresent(data, "endToEndId");
		assertEquals(expectedEndToEndId, endToEndId);

		String date = assertStringFieldIsPresent(data, "date");
		assertTrue(date.matches("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$"));

		String cnpjInitiator = assertStringFieldIsPresent(data, "cnpjInitiator");
		assertEquals(14, cnpjInitiator.length());

		String ibgeTownCode = assertStringFieldIsPresent(data, "ibgeTownCode");
		assertTrue(ibgeTownCode.matches("^\\d{7}$"));

		String localInstrument = assertStringFieldIsPresent(data, "localInstrument");
		assertEquals(4, localInstrument.length());
		if (environment.getString("local_instrument") != null) {
			assertEquals(environment.getString("local_instrument"), localInstrument);
		}

		JsonObject paymentField = assertObjectFieldIsPresent(data, "payment");

		String amount = assertStringFieldIsPresent(paymentField, "amount");
		assertEquals(AMOUNT, amount);

		String currency = assertStringFieldIsPresent(paymentField, "currency");
		assertEquals("BRL", currency);

		JsonObject document = assertObjectFieldIsPresent(data, "document");

		String expectedIdentification = withBusinessEntity ? CNPJ : CPF;
		String expectedRel = withBusinessEntity ? "CNPJ" : "CPF";

		String identification = assertStringFieldIsPresent(document, "identification");
		assertEquals(expectedIdentification, identification);

		String proxy = assertStringFieldIsPresent(data, "proxy");
		assertEquals(expectedIdentification, proxy);

		String rel = assertStringFieldIsPresent(document, "rel");
		assertEquals(expectedRel, rel);
	}

	private String assertStringFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonPrimitive() && obj.get(field).getAsJsonPrimitive().isString());
		return OIDFJSON.getString(obj.get(field));
	}

	private JsonObject assertObjectFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonObject());
		return obj.getAsJsonObject(field);
	}

	private void unhappyPathTest(String expectedMessage) {
		AddAutomaticPaymentToTheResource condition = new AddAutomaticPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
