package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.webhook;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.WebhookWithEnrollmentIdV1;
import net.openid.conformance.testmodule.Environment;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class WebhookWithEnrollmentIdV1Test {

	protected final String BASE_URL = "www.example.com";

	protected Environment prepareEnvironment(boolean includeBaseUrl) {
		Environment env = new Environment();
		if (includeBaseUrl) {
			env.putString("base_url", BASE_URL);
		}
		return env;
	}

	protected WebhookWithEnrollmentIdV1 createCondition() {
		WebhookWithEnrollmentIdV1 condition = new WebhookWithEnrollmentIdV1();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		return condition;
	}

	@Test
	public void testHappyPath() {
		Environment env = prepareEnvironment(true);
		WebhookWithEnrollmentIdV1 condition = createCondition();
		condition.evaluate(env);

		String webhookUri = env.getString("webhook_uri_enrollment_id");
		assertNotNull(webhookUri);
		assertEquals(BASE_URL + "/open-banking/enrollments/v1/enrollments/", webhookUri);
	}

	@Test
	public void testMissingBaseUrl() {
		Environment env = prepareEnvironment(false);
		WebhookWithEnrollmentIdV1 condition = createCondition();

		assertThrows(NullPointerException.class, () -> {
			condition.evaluate(env);
		});

		String webhookUri = env.getString("webhook_uri_enrollment_id");
		assertNull(webhookUri);
	}

	@Test
	public void testEmptyBaseUrl() {
		Environment env = prepareEnvironment(false);
		env.putString("base_url", "");
		WebhookWithEnrollmentIdV1 condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(env);
		});
		String expectedErrorMessage = "Base URL is empty";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));

		String webhookUri = env.getString("webhook_uri_enrollment_id");
		assertNull(webhookUri);
	}
}
