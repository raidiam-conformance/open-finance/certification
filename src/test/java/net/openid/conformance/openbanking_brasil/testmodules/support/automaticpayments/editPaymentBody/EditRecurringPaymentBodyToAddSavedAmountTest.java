package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToAddSavedAmount;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentBodyToAddSavedAmountTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBody";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";

	private static final String EXPECTED_AMOUNT = "100.00";

	@Before
	public void init() {
		environment.putString("payment_amount", EXPECTED_AMOUNT);
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "V2.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(new EditRecurringPaymentBodyToAddSavedAmount());

		String amount = OIDFJSON.getString(environment.getElementFromObject(KEY, PATH + ".data.payment.amount"));
		assertEquals(EXPECTED_AMOUNT, amount);
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "MissingPaymentV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingPayment() {
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new EditRecurringPaymentBodyToAddSavedAmount());
		assertTrue(error.getMessage().contains("Unable to find data.payment in payments payload"));
	}
}
