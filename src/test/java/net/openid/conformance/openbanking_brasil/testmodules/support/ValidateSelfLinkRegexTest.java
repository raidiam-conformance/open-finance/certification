package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ValidateSelfLinkRegexTest extends AbstractJsonResponseConditionUnitTest {




	@After
	public void cleanup(){
		environment.unmapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY);
	}

	@Test
	@UseResurce(value = "jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseOK.json")
	public void weCanValidatePostAutomaticPaymentsResponse(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}

	@Test
	@UseResurce(value = "jsonResponses/automaticpayments/GetRecurringPixPaymentsListByConsentIdResponseOK.json")
	public void weCanValidateGetAutomaticPaymentsByConsentIDResponse(){
		environment.putString("protected_resource_url", "https://api.banco.com.br/open-banking/automatic-payments/v1/pix/recurring-payments?recurringConsentId=urn:bancoex:C1DD33123");
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/consent/v4/PatchPaymentsConsentV4OK.json")
	public void weCanValidateV4PatchPaymentConsentResponse(){
		environment.putString("consent_id", "urn:raidiambank:a105524d-6e83-4818-85b8-e5644172e52b");
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/consent/v4/PatchPaymentsConsentV4OK.json")
	public void weCantValidateV4PatchPaymentConsentResponseWithoutConsentId(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		assertTrue(e.getMessage().contains("consent ID"));
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4Ok.json")
	public void weCanValidateV4PostPaymentConsentResponse(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}



	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixValidatorV4.json")
	public void weCanValidateV4PostPaymentResponse(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}


	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/consent/v3/PostPaymentsConsentValidatorV3ResponseOK.json")
	public void weCanValidateV3PostPaymentConsentResponse(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}



	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v3/PostPaymentsPixValidatorV3.json")
	public void weCanValidateV3PostPaymentResponse(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}


	@Test
	@UseResurce(value = "jsonResponses/automaticpayments/PostRecurringConsentsOneOfSweepingResponseOK.json")
	public void weCanValidateV1PostAutomaticPaymentConsentResponse() {
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}


	@Test
	@UseResurce(value = "jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseOK.json")
	public void weCanValidateV1PostAutomaticPaymentResponse(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}

	@Test
	@UseResurce(value = "jsonRequests/payments/enrollments/getEnrollments.json")
	public void weCanValidateV1PostEnrollmentsResponse(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		run(cond);
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/enrollments/postEnrollmentsEmptyData.json")
	public void errorIsThrownIfPostEnrollmentsDataIsEmptyV1(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Data array is empty", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/enrollments/postEnrollmentsMissingData.json")
	public void errorIsThrownIfPostEnrollmentsDataIsMissingV1(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Could not extract data from body", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/enrollments/postEnrollmentsMissingEnrollmentId.json")
	public void errorIsThrownIfPostEnrollmentIdIsMissingV1(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Could not find enrollmentId in the data JSON object", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/enrollments/postEnrollmentsArrayMissingEnrollmentId.json")
	public void errorIsThrownIfPostEnrollmentIdIsMissingV1FromArray() {
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Could not find enrollmentId in the first JSON object in the data array", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4WrongSelfLink.json")
	public void errorIsThrownIfSelfLinksDoesNotMatchPostPaymentsConsentsV4(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Self link does not match the regex", e.getMessage());
	}



	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixV4WrongSelfLink.json")
	public void errorIsThrownIfSelfLinksDoesNotMatchPostPaymentsV4(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Self link does not match the regex", e.getMessage());
	}


	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/consent/v3/PostPaymentsConsentVaV3WrongSelfLink.json")
	public void errorIsThrownIfSelfLinksDoesNotMatchPostPaymentsConsentsV3(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Self link does not match the regex", e.getMessage());
	}



	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v3/PostPaymentsPixV3WrongSelfLink.json")
	public void errorIsThrownIfSelfLinksDoesNotMatchPostPaymentsV3(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Self link does not match the regex", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixV4NoLinks.json")
	public void errorIsThrownIfNoLinksObjectIsPresent(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Could not extract links.self from response body", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixV4NoSelfLink.json")
	public void errorIsThrownIfNoSelfLinksIsPresent(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Could not extract links.self from response body", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixV4EmptyData.json")
	public void errorIsThrownIfPostPaymentsDataIsEmptyV4(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Data array is empty", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixV4NoPaymentId.json")
	public void errorIsThrownIfPostPaymentsIdIsMissingV4(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Could not find paymentId in the data JSON object", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v3/PostPaymentsPixV3NoPaymentId.json")
	public void errorIsThrownIfPostPaymentsIdIsMissingV3(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Could not find paymentId in the data JSON object", e.getMessage());
	}

	@Test
	@UseResurce(value = "jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixV4NoData.json")
	public void errorIsThrownIfPostPaymentsDataIsMissingV4(){
		environment.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		ValidateSelfLinkRegex cond = new ValidateSelfLinkRegex();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("ValidateSelfLinkRegex: Could not extract data from payment body", e.getMessage());
	}

}
