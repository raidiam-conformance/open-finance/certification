package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

	public class GetTreasureTitleIdentificationV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
		@Before
		public void setUp() {
			setStatus(200);
		}

		@Test
		@UseResurce("jsonResponses/treasureTitles/identification.json")
		public void happyPath() {
			run(new GetTreasureTitleIdentificationV1OASValidator());
		}


		@Test
		@UseResurce("jsonResponses/treasureTitles/identificationNoIndexerAdditionalInfo.json")
		public void unhappyPathNoIndexerAdditionalInfo() {
			ConditionError e = runAndFail(new GetTreasureTitleIdentificationV1OASValidator());
			assertThat(e.getMessage(), containsString("indexerAdditionalInfo is required when indexer is OUTROS"));
		}


		@Test
		@UseResurce("jsonResponses/treasureTitles/identificationNoPostFixedIndexerPercentage.json")
		public void unhappyPathNoPostFixedIndexerPercentage() {
			ConditionError e = runAndFail(new GetTreasureTitleIdentificationV1OASValidator());
			assertThat(e.getMessage(), containsString("postFixedIndexerPercentage is required when indexer is PRE_FIXADO"));
		}

		@Test
		@UseResurce("jsonResponses/treasureTitles/identificationNoPreFixedRate.json")
		public void unhappyPathNoPreFixedRate() {
			ConditionError e = runAndFail(new GetTreasureTitleIdentificationV1OASValidator());
			assertThat(e.getMessage(), containsString("preFixedRate is required when indexer is PRE_FIXADO"));
		}

		@Test
		@UseResurce("jsonResponses/treasureTitles/identificationNoVaucherPaymentPeriodicity.json")
		public void unhappyPathNoVaucherPaymentPeriodicity() {
			ConditionError e = runAndFail(new GetTreasureTitleIdentificationV1OASValidator());
			assertThat(e.getMessage(), containsString("voucherPaymentPeriodicity is required when voucherPaymentIndicator is SIM"));
		}

		@Test
		@UseResurce("jsonResponses/treasureTitles/identificationNoVaucherPaymentPeriodicityAdditionalInfo.json")
		public void unhappyPathNoVaucherPaymentPeriodicityAdditionalInfo() {
			ConditionError e = runAndFail(new GetTreasureTitleIdentificationV1OASValidator());
			assertThat(e.getMessage(), containsString("voucherPaymentPeriodicityAdditionalInfo is required when voucherPaymentPeriodicity is OUTROS"));
		}

}
