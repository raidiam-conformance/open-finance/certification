package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.investments.bankFixedIncomes.v1;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetInvestmentsBankFixedIncomesOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFixedIncomeBankResponseV1.json")
	public void happyPath() {
		GetInvestmentsBankFixedIncomesOASValidatorV1 cond = new GetInvestmentsBankFixedIncomesOASValidatorV1();
		run(cond);

		//IndexerAdditionalInfo
		JsonObject index = new JsonObject();
		index.addProperty("indexer", "OUTROS");
		index.addProperty("indexerAdditionalInfo", "OUTROS");
		JsonObject data = new JsonObject();
		data.add("index", index);
		cond.assertIndexerAdditionalInfo(data);
	}

}
