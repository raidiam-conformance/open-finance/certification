package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LogRetryAuxTestResults;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

public class LogRetryAuxTestResultsTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		environment.putString("consent_id", "urn:bancoex:C1DD33123");
		environment.putString("refresh_token", "refresh_token");

		JsonArray recurringPaymentIdsArray = new JsonArray();
		recurringPaymentIdsArray.add("id 1");
		recurringPaymentIdsArray.add("id 2");
		recurringPaymentIdsArray.add("id 3");
		environment.putObject("recurringPaymentIds", new JsonObjectBuilder().addField("recurringPaymentIdsArray", recurringPaymentIdsArray).build());

		run(new LogRetryAuxTestResults());
	}
}
