package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditEnrollmentsRequestBodyToSetAccountTypeToSLRYTest extends AbstractJsonResponseConditionUnitTest {

	private static final String JSON_PATH_BASE = "jsonRequests/payments/enrollments/v2/postEnrollmentsRequest";
	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";

	@Test
	@UseResurce(value = JSON_PATH_BASE + "V2.json", key = REQUEST_CLAIMS_KEY)
	public void happyPathTest() {
		run(new EditEnrollmentsRequestBodyToSetAccountTypeToSLRY());
		String accountType = OIDFJSON.getString(
			environment.getElementFromObject(REQUEST_CLAIMS_KEY, "data.debtorAccount.accountType")
		);
		assertEquals("SLRY", accountType);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResourceRequestEntityClaims() {
		run(new EditEnrollmentsRequestBodyToSetAccountTypeToSLRY());
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(REQUEST_CLAIMS_KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_PATH_BASE + "WithoutDebtorAccountV2.json", key = REQUEST_CLAIMS_KEY)
	public void unhappyPathTestMissingDebtorAccount() {
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new EditEnrollmentsRequestBodyToSetAccountTypeToSLRY());
		assertTrue(error.getMessage().contains("Could not find debtorAccount inside the body"));
	}
}
