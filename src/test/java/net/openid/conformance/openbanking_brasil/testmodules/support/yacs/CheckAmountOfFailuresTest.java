package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CheckAmountOfFailuresTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		JsonObject apiFailures = new JsonObjectBuilder()
			.addField("api 1", 0)
			.addField("api 2", 0)
			.addField("api 3", 0)
			.build();
		environment.putObject(CheckAmountOfFailures.LIST_OF_API_FAILURES_ENV_KEY, apiFailures);
		run(new CheckAmountOfFailures());
	}

	@Test
	public void unhappyPathTest() {
		JsonObject apiFailures = new JsonObjectBuilder()
			.addField("api 1", 3)
			.addField("api 2", 2)
			.addField("api 3", 0)
			.build();
		environment.putObject(CheckAmountOfFailures.LIST_OF_API_FAILURES_ENV_KEY, apiFailures);
		assertTrue(runAndFail(new CheckAmountOfFailures()).getMessage().contains("At least one of the APIs tested has presented errors"));
	}
}
