package net.openid.conformance.openbanking_brasil.testmodules.support.assertLinksLogic;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AbstractAssertLinksLogicTest extends AbstractJsonResponseConditionUnitTest {

	private static class CustomAssertLinksLogic extends AbstractAssertLinksLogic {

		@Override
		protected void validateLinksLogic(JsonObject links) {}
	}

	@Test
	@UseResurce("jsonResponses/links/SimpleSelfLinkResponse.json")
	public void happyPathTest() {
		run(new CustomAssertLinksLogic());
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingBody() {
		run(new CustomAssertLinksLogic());
	}

	@Test
	public void unhappyPathTestInvalidBody() {
		environment.putObject(CustomAssertLinksLogic.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(
			CustomAssertLinksLogic.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya1.b2.c3").build()
		);
		unhappyPathTest("Could not parse the body");
	}

	@Test
	@UseResurce("jsonResponses/links/MissingLinksResponse.json")
	public void unhappyPathTestMissingLinks() {
		unhappyPathTest("Could not extract links from body");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new CustomAssertLinksLogic());
		assertTrue(error.getMessage().contains(message));
	}
}
