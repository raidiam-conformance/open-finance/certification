package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostFidoRegistrationOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/enrollments/GenericEnrollment422Response.json")
	public void happyPathTest422() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 422);
		run(new PostFidoRegistrationOASValidatorV1());
	}
}
