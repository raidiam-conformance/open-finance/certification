package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GetEnrollmentStatusTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/enrollments/GetEnrollmentsAuthorisedWithoutDailyLimit.json")
	public void happyPathTest() {
		run(new GetEnrollmentStatus());
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(GetEnrollmentStatus.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(GetEnrollmentStatus.RESPONSE_ENV_KEY,
			new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestMissingStatus() {
		environment.putObject(GetEnrollmentStatus.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build());
		unhappyPathTest("Body does not have data.status field");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new GetEnrollmentStatus());
		assertTrue(error.getMessage().contains(message));
	}
}
