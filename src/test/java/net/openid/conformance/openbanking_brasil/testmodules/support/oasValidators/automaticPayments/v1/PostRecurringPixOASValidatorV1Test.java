package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class PostRecurringPixOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(201);
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseOK.json")
	public void happyPath() {
		run(new PostRecurringPixOASValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseMissingConsentId.json")
	public void unhappyPathRecurringConsentIdMissing() {
		ConditionError e = runAndFail(new PostRecurringPixOASValidatorV1());
		assertThat(e.getMessage(), containsString("recurringConsentId is required when authorisationFlow is FIDO_FLOW"));
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseMissingProxy.json")
	public void unhappyPathProxyMissing() {
		ConditionError e = runAndFail(new PostRecurringPixOASValidatorV1());
		assertThat(e.getMessage(), containsString("proxy is required when localInstrument is [INIC, DICT, QRDN, QRES]"));
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseProxyWithManu.json")
	public void unhappyPathProxyWithManu() {
		ConditionError e = runAndFail(new PostRecurringPixOASValidatorV1());
		assertThat(e.getMessage(), containsString("proxy cannot be present when localInstrument has the value MANU"));
	}
}
