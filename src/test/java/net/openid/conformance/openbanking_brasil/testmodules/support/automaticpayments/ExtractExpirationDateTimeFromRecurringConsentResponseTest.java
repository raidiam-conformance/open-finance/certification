package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractExpirationDateTimeFromRecurringConsentResponse;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExtractExpirationDateTimeFromRecurringConsentResponseTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String EXPIRATION_DATE_TIME = "2023-05-21T08:30:00Z";
	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/GetRecurringConsents";

	@Test
	@UseResurce(BASE_JSON_PATH + "Sweeping200Response.json")
	public void happyPathTest() {
		run(new ExtractExpirationDateTimeFromRecurringConsentResponse());
		String expirationDateTime = environment.getString("expiration_date_time");
		assertEquals(EXPIRATION_DATE_TIME, expirationDateTime);
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(ExtractExpirationDateTimeFromRecurringConsentResponse.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject(ExtractExpirationDateTimeFromRecurringConsentResponse.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "SweepingMissingExpirationDateTime200Response.json")
	public void unhappyPathTestMissingExpirationDateTime() {
		unhappyPathTest("Unable to find expirationDateTime field in the response data");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new ExtractExpirationDateTimeFromRecurringConsentResponse());
		assertTrue(error.getMessage().contains(message));
	}
}
