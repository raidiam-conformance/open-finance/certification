package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureResponseCodeWas401or403or422;
import net.openid.conformance.testmodule.Environment;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class EnsureResourceResponseCodeWas401Or403Or422Test {

	protected Environment setStatus(Integer status) {
		Environment environment = new Environment();
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		environment.getObject("resource_endpoint_response_full").addProperty("status", status);
		return environment;
	}

	protected EnsureResponseCodeWas401or403or422 createCondition() {
		EnsureResponseCodeWas401or403or422 condition = new EnsureResponseCodeWas401or403or422();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		return condition;
	}

	@Test
	public void testResponseCodeIs401() {
		Environment environment = setStatus(HttpStatus.UNAUTHORIZED.value());

		EnsureResponseCodeWas401or403or422 condition = createCondition();
		condition.evaluate(environment);

		assertNull(environment.getString("422_status"));
	}

	@Test
	public void testResponseCodeIs403() {
		Environment environment = setStatus(HttpStatus.FORBIDDEN.value());

		EnsureResponseCodeWas401or403or422 condition = createCondition();
		condition.evaluate(environment);

		assertNull(environment.getString("422_status"));
	}

	@Test
	public void testResponseCodeIs422() {
		Environment environment = setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());

		EnsureResponseCodeWas401or403or422 condition = createCondition();
		condition.evaluate(environment);

		String is422Status = environment.getString("422_status");
		assertNotNull(is422Status);
		assertEquals("ok", is422Status);
	}

	@Test
	public void testResponseCodeIsNot401Or403Or422() {
		Environment environment = setStatus(HttpStatus.OK.value());

		EnsureResponseCodeWas401or403or422 condition = createCondition();

		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(environment);
		});

		String expectedErrorMessage = "Was expecting a 401, 403 or 422 response";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
		assertNull(environment.getString("422_status"));
	}

	@Test
	public void testResponseCodeIsMissingInTheResponse() {
		Environment environment = setStatus(null);

		EnsureResponseCodeWas401or403or422 condition = createCondition();

		Exception exception = assertThrows(Environment.UnexpectedTypeException.class, () -> {
			condition.evaluate(environment);
		});

		assertNull(environment.getString("422_status"));
	}

	@Test
	public void testResourceEndpointResponseFullMissingInTheEnvironment() {
		Environment environment = new Environment();

		EnsureResponseCodeWas401or403or422 condition = createCondition();

		Exception exception = assertThrows(NullPointerException.class, () -> {
			condition.evaluate(environment);
		});

		assertNull(environment.getString("422_status"));
	}
}
