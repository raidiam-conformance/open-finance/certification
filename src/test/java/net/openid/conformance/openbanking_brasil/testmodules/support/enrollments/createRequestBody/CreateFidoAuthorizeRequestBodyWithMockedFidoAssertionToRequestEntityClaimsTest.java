package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.Base64;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class CreateFidoAuthorizeRequestBodyWithMockedFidoAssertionToRequestEntityClaimsTest {

	private final Environment environment = new Environment();
	private final static String ENROLLMENT_ID = "enrollment_id";
	private final static String CLIENT_DATA = "clientData";
	private final static String AUTHENTICATOR_DATA = "authenticatorData";

	private static final String USER_ID = "userId";
	private static final String SIGNATURE = "signature";
	private static final String ID = "id";


	@Before
	public void init() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
		environment.putString("enrollment_id", ENROLLMENT_ID);
	}

	@Test
	public void happyPath() {
		try {
			CreateFidoAuthorizeRequestBodyWithMockedFidoAssertionToRequestEntityClaims condition
				= new CreateFidoAuthorizeRequestBodyWithMockedFidoAssertionToRequestEntityClaims();


			condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
			condition.execute(environment);
			JsonObject request = environment.getObject("resource_request_entity_claims");
			JsonObject expectedObject = getExpectedObject();
			assertEquals(expectedObject, request);
		} catch (NoSuchAlgorithmException | SignatureException | InvalidKeyException e) {
			throw new AssertionError("Test Failed", e);
		}

	}

	private JsonObject getExpectedObject() throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		JsonObject response = new JsonObjectBuilder()
			.addField("clientDataJSON", Base64.getEncoder().withoutPadding().encodeToString(CLIENT_DATA.getBytes(StandardCharsets.UTF_8)))
			.addField("authenticatorData", Base64.getEncoder().withoutPadding().encodeToString(AUTHENTICATOR_DATA.getBytes()))
			.addField("signature", Base64.getEncoder().withoutPadding().encodeToString(SIGNATURE.getBytes()))
			.addField("userHandle", USER_ID)
			.build();

		JsonObject fidoAssertion = new JsonObjectBuilder()
			.addField("id", Base64.getEncoder().withoutPadding().encodeToString(ID.getBytes(StandardCharsets.UTF_8)))
			.addField("rawId", Base64.getEncoder().withoutPadding().encodeToString(ID.getBytes(StandardCharsets.UTF_8)))
			.addField("type", "public-key")
			.addField("response", response)
			.build();

		JsonObject riskSignals = new JsonObjectBuilder().addFields("data", Map.of(
			"deviceId", "5ad82a8f-37e5-4369-a1a3-be4b1fb9c034",
			"isRootedDevice", false,
			"screenBrightness", 90,
			"elapsedTimeSinceBoot", 28800000,
			"osVersion", "16.6",
			"userTimeZoneOffset", "-03",
			"language", "pt",
			"accountTenure", "2023-01-01"
		)).addFields("data.screenDimensions", Map.of(
			"height", 1080,
			"width", 1920
		)).build().getAsJsonObject("data");

		return new JsonObjectBuilder()
			.addFields("data", Map.of(
				"enrollmentId", ENROLLMENT_ID,
				"riskSignals", riskSignals,
				"fidoAssertion", fidoAssertion
			))
			.build();
	}

}
