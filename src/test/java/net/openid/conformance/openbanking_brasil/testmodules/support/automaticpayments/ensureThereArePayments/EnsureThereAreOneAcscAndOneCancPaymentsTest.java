package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureThereArePayments;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreOneAcscAndOneCancPayments;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureThereAreOneAcscAndOneCancPaymentsTest extends AbstractEnsureThereArePaymentsTest {

	@Override
	protected AbstractEnsureThereArePayments condition() {
		return new EnsureThereAreOneAcscAndOneCancPayments();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "OneAcscOneCanc.json")
	public void happyPathTest() {
		super.happyPathTest();
	}
}
