package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;


public class GenerateFidoClientDataTest extends AbstractJsonResponseConditionUnitTest {

	public static final String FIDO_CHALLENGE = "fido_challenge";
	public static final String ORIGIN = "https://test.com";

	@Before
	public void init() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
		setJwt(true);
		environment.putObject("config", new JsonObjectBuilder().addField("resource.enrollmentsUrl", ORIGIN + "/test").build());
		environment.putString("fido_challenge", FIDO_CHALLENGE);

	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void evaluateClientDataCreationForRegistration() {
		run(new GenerateFidoClientData());
		JsonObject clientData = environment.getObject("client_data");
		validateClientData(clientData, "webauthn.create");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void evaluateClientDataCreationForAuthorization() {
		environment.putBoolean("is_client_data_registration", false);
		run(new GenerateFidoClientData());
		JsonObject clientData = environment.getObject("client_data");
		validateClientData(clientData, "webauthn.get");
	}

	private void validateClientData(JsonObject clientData, String expectedType){
		assertThat(clientData, notNullValue());
		JsonElement type = clientData.get("type");
		JsonElement challenge = clientData.get("challenge");
		JsonElement origin = clientData.get("origin");
		JsonElement crossOrigin = clientData.get("crossOrigin");

		assertThat(type, notNullValue());
		assertThat(challenge, notNullValue());
		assertThat(origin, notNullValue());
		assertThat(crossOrigin, notNullValue());

		assertThat(OIDFJSON.getString(type), equalTo(expectedType));
		assertThat(OIDFJSON.getString(challenge), equalTo(FIDO_CHALLENGE));
		assertThat(OIDFJSON.getString(origin), equalTo(ORIGIN));
		assertThat(OIDFJSON.getBoolean(crossOrigin), equalTo(false));

	}
}
