package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetExchangesEventsV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/exchanges/events.json")
	public void happyPath() {
		run(new GetExchangesEventsV1OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/exchanges/eventsNoForeignParite.json")
	public void noForeignPartie1() {
		run(new GetExchangesEventsV1OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/exchanges/eventsNoForeignParite2.json")
	public void noForeignPartie2() {
		run(new GetExchangesEventsV1OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/exchanges/eventsNoForeignParite3.json")
	public void noForeignPartie3() {
		run(new GetExchangesEventsV1OASValidator());
	}
}
