package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetAccountsBalancesOASValidatorV2n4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/account/v2n/balances.json")
	public void happyPath() {
		run(new GetAccountsBalancesOASValidatorV2n4());
	}

}
