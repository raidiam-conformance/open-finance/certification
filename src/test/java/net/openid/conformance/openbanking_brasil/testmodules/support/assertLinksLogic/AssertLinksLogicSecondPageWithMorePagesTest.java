package net.openid.conformance.openbanking_brasil.testmodules.support.assertLinksLogic;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AssertLinksLogicSecondPageWithMorePagesTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/links/CompleteSecondPageResponse.json")
	public void happyPathTest() {
		run(new AssertLinksLogicSecondPageWithMorePages());
	}

	@Test
	@UseResurce("jsonResponses/links/CompleteSecondPageOmittingPage1Response.json")
	public void happyPathTestOmittingPageParamForPrev() {
		run(new AssertLinksLogicSecondPageWithMorePages());
	}

	@Test
	@UseResurce("jsonResponses/links/GenericMissingPrevResponse.json")
	public void unhappyPathTestMissingPrev() {
		unhappyPathTest("The prev link is mandatory if it is not the first page");
	}

	@Test
	@UseResurce("jsonResponses/links/GenericMissingSelfResponse.json")
	public void unhappyPathTestMissingSelf() {
		unhappyPathTest("The self link is mandatory for any response");
	}

	@Test
	@UseResurce("jsonResponses/links/GenericMissingNextResponse.json")
	public void unhappyPathTestMissingNext() {
		unhappyPathTest("The next link is mandatory if it is not the last page");
	}

	@Test
	@UseResurce("jsonResponses/links/SecondPagePrevAndSelfNotSuccessiveResponse.json")
	public void unhappyPathTestPrevAndSelfNotSuccessive() {
		unhappyPathTest("The \"page\" parameter from self should be equals to the one from prev + 1");
	}

	@Test
	@UseResurce("jsonResponses/links/SecondPageSelfAndNextNotSuccessiveResponse.json")
	public void unhappyPathTestSelfAndNextNotSuccessive() {
		unhappyPathTest("The \"page\" parameter from next should be equals to the one from self + 1");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new AssertLinksLogicSecondPageWithMorePages());
		assertTrue(error.getMessage().contains(message));
	}
}
