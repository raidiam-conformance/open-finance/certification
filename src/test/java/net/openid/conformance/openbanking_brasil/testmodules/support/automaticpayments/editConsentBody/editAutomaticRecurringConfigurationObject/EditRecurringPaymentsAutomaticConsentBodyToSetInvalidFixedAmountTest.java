package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject.AbstractEditRecurringPaymentsAutomaticConsentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject.EditRecurringPaymentsAutomaticConsentBodyToSetInvalidFixedAmount;
import net.openid.conformance.testmodule.OIDFJSON;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentsAutomaticConsentBodyToSetInvalidFixedAmountTest extends AbstractEditRecurringPaymentsAutomaticConsentBodyTest {

	@Override
	protected AbstractEditRecurringPaymentsAutomaticConsentBody condition() {
		return new EditRecurringPaymentsAutomaticConsentBodyToSetInvalidFixedAmount();
	}

	@Override
	protected void assertAutomaticObject(JsonObject automatic) {
		assertFalse(automatic.has("maximumVariableAmount"));
		assertFalse(automatic.has("minimumVariableAmount"));
		assertTrue(automatic.has("fixedAmount"));

		assertEquals("1", OIDFJSON.getString(automatic.get("fixedAmount")));
	}
}
