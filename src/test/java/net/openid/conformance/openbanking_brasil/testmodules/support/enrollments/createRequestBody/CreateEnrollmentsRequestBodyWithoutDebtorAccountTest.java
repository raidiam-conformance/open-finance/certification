package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CreateEnrollmentsRequestBodyWithoutDebtorAccountTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void  happyPath(){
		environment.putObject("config", new JsonObjectBuilder()
			.addField("resource.loggedUserIdentification", "user")
			.addField("resource.businessEntityIdentification", "business")
			.build());
		environment.putString("enrollment_permissions", "permission1 permission2");

		CreateEnrollmentsRequestBodyWithoutDebtorAccount condition = new CreateEnrollmentsRequestBodyWithoutDebtorAccount();
		run(condition);

		JsonObject requestBody = environment.getObject("resource_request_entity_claims");
		JsonArray permissions = new JsonArray();
		permissions.add("permission1");
		permissions.add("permission2");
		JsonObject expectedObject = new JsonObjectBuilder().addFields("data", Map.of(
			"loggedUser.document.rel", "CPF",
			"loggedUser.document.identification", "user",
			"businessEntity.document.identification", "business",
			"businessEntity.document.rel", "CNPJ",
			"permissions", permissions
		)).build();
		assertEquals(expectedObject, requestBody);
	}

}
