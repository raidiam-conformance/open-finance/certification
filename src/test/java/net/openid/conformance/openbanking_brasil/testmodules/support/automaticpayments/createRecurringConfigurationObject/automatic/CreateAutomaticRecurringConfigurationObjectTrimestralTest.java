package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createRecurringConfigurationObject.automatic;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.AbstractCreateAutomaticRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectTrimestral;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;

public class CreateAutomaticRecurringConfigurationObjectTrimestralTest extends AbstractCreateAutomaticRecurringConfigurationObjectTest {

	@Override
	protected AbstractCreateAutomaticRecurringConfigurationObject condition() {
		return new CreateAutomaticRecurringConfigurationObjectTrimestral();
	}

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getExpectedInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.TRIMESTRAL;
	}
}
