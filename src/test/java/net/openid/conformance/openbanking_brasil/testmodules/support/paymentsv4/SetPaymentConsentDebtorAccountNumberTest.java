package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

public class SetPaymentConsentDebtorAccountNumberTest extends AbstractJsonResponseConditionUnitTest {

	private static final String JSON_BASE_PATH = "jsonRequests/payments/enrollments/v2/postEnrollmentsRequest";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";
	private static final String DEBTOR_ACCOUNT_NUMBER = "1234";

	@Before
	public void init() {
		environment.putString("debtor_account_number", DEBTOR_ACCOUNT_NUMBER);
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "V2.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(new SetPaymentConsentDebtorAccountNumber());
		JsonObject debtorAccount = environment.getElementFromObject(KEY, PATH + ".data.debtorAccount").getAsJsonObject();
		assertEquals(DEBTOR_ACCOUNT_NUMBER, OIDFJSON.getString(debtorAccount.get("number")));
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "V2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingNumber() {
		environment.removeNativeValue("debtor_account_number");
		assertThrowsExactly(AssertionError.class, () -> {
			run(new SetPaymentConsentDebtorAccountNumber());
		});
	}

	@Test
	public void unhappyPathTestMissingResource() {
		assertThrowsExactly(AssertionError.class, () -> {
			run(new SetPaymentConsentDebtorAccountNumber());
		});
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithoutDebtorAccountV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingDebtorAccount() {
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new SetPaymentConsentDebtorAccountNumber());
		assertTrue(error.getMessage().contains("Could not find debtorAccount in payment consent request body"));
	}
}
