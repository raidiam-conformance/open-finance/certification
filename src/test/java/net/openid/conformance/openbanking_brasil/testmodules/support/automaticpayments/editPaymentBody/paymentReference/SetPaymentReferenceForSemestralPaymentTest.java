package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.paymentReference;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForSemestralPayment;
import org.junit.Test;

public class SetPaymentReferenceForSemestralPaymentTest extends AbstractSetPaymentReferenceTest {

	@Override
	protected AbstractSetPaymentReference condition() {
		return new SetPaymentReferenceForSemestralPayment();
	}

	@Test
	public void happyPathTestS1() {
		happyPathTest("2024-03-01", "S1-2024");
	}

	@Test
	public void happyPathTestS2() {
		happyPathTest("2024-07-01", "S2-2024");
	}
}
