package net.openid.conformance.openbanking_brasil.testmodules.support.selectlocalinstrument;

import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractPaymentLocalInstrumentCondtion;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRDNCodeLocalInstrument;

public class SelectQRDNCodeLocalInstrumentTest extends AbstractPaymentLocalInstrumentConditionTest{

	@Override
	protected AbstractPaymentLocalInstrumentCondtion localInstrumentCondition() {
		return new SelectQRDNCodeLocalInstrument();
	}

	@Override
	protected String localInstrumentString() {
		return "QRDN";
	}
}
