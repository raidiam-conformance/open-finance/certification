package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RemoveLastElementFromCustomScheduleTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_PAYMENTS_PATH = "jsonRequests/payments/payments/paymentWithDataArrayWith";
	private static final String KEY = "resource";
	private static final String PAYMENTS_PATH = "brazilPixPayment";


	@Test
	@UseResurce(value = BASE_PAYMENTS_PATH + "5Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void happyPathTest() {
		RemoveLastElementFromCustomSchedule condition = new RemoveLastElementFromCustomSchedule();
		run(condition);

		JsonArray data = environment.getElementFromObject(KEY, PAYMENTS_PATH)
			.getAsJsonObject()
			.getAsJsonArray("data");

		assertEquals(4, data.size());
	}
}
