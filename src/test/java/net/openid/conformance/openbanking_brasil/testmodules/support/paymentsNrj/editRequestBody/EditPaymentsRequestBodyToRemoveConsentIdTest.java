package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj.editRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EditPaymentsRequestBodyToRemoveConsentIdTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value = "jsonRequests/payments/payments/paymentWithDataObjectConsentId.json", key = "resource_request_entity_claims")
	public void happyPathTestJsonObject() {
		run(new EditPaymentsRequestBodyToRemoveConsentId());
		JsonObject data = environment.getObject("resource_request_entity_claims").getAsJsonObject("data");
		assertFalse(data.has("consentId"));
	}

	@Test
	@UseResurce(value = "jsonRequests/payments/payments/paymentWithDataArrayWith5Payments.json", key = "resource_request_entity_claims")
	public void happyPathTestJsonArray() {
		run(new EditPaymentsRequestBodyToRemoveConsentId());
		JsonArray data = environment.getObject("resource_request_entity_claims").getAsJsonArray("data");
		for (JsonElement paymentElement : data) {
			JsonObject payment = paymentElement.getAsJsonObject();
			assertFalse(payment.has("consentId"));
		}
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResourceRequestEntityClaims() {
		run(new EditPaymentsRequestBodyToRemoveConsentId());
	}

	@Test
	public void unhappyPathTestMissingData() {
		unhappyPathTest(new JsonObject(), "Could not find data inside the body");
	}

	@Test
	public void unhappyPathTestInvalidObject() {
		JsonObject resourceRequestEntityClaims = new JsonObjectBuilder().addField("data", "data").build();
		unhappyPathTest(resourceRequestEntityClaims, "Payment is not of a valid type");
	}

	private void unhappyPathTest(JsonObject resourceRequestEntityClaims, String message) {
		environment.putObject("resource_request_entity_claims", resourceRequestEntityClaims);
		ConditionError error = runAndFail(new EditPaymentsRequestBodyToRemoveConsentId());
		assertTrue(error.getMessage().contains(message));
	}
}
