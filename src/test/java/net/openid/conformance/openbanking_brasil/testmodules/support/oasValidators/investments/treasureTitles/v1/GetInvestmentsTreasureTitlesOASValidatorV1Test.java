package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.investments.treasureTitles.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetInvestmentsTreasureTitlesOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetTreasureResponseV1.json")
	public void happyPath() {
		run(new GetInvestmentsTreasureTitlesOASValidatorV1());
	}

}
