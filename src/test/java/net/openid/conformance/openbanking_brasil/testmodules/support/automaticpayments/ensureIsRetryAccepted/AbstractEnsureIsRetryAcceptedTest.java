package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureIsRetryAccepted;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted.AbstractEnsureIsRetryAccepted;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public abstract class AbstractEnsureIsRetryAcceptedTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/GetRecurringConsents";

	protected abstract AbstractEnsureIsRetryAccepted condition();



	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(AbstractEnsureIsRetryAccepted.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject(AbstractEnsureIsRetryAccepted.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(AbstractEnsureIsRetryAccepted.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build());
		unhappyPathTest("Could not find data.recurringConfiguration.automatic in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRecurringConfiguration200Response.json")
	public void unhappyPathTestMissingRecurringConfiguration() {
		unhappyPathTest("Could not find data.recurringConfiguration.automatic in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "Sweeping200Response.json")
	public void unhappyPathTestNotAutomatic() {
		unhappyPathTest("Could not find data.recurringConfiguration.automatic in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingIsRetryAccepted200Response.json")
	public void unhappyPathTestMissingIsRetryAccepted() {
		unhappyPathTest("Could not find isRetryAccepted field inside automatic recurringConfiguration object");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}
}
