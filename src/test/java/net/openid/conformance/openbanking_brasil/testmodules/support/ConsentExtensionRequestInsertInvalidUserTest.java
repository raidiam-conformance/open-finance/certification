package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ConsentExtensionRequestInsertInvalidUser;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class ConsentExtensionRequestInsertInvalidUserTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value = "jsonRequests/consents/v3/consentExtensionRequest.json", key = "consent_endpoint_request")
	public void happyPath() {
		ConsentExtensionRequestInsertInvalidUser cond = new ConsentExtensionRequestInsertInvalidUser();
		run(cond);
		Assert.assertEquals(
			"82139497066",
			OIDFJSON.getString(environment.getObject("consent_endpoint_request").get("data").getAsJsonObject()
				.get("loggedUser").getAsJsonObject()
				.get("document").getAsJsonObject()
				.get("identification"))
		);
		Assert.assertEquals(
			"CPF",
			OIDFJSON.getString(environment.getObject("consent_endpoint_request").get("data").getAsJsonObject()
				.get("loggedUser").getAsJsonObject()
				.get("document").getAsJsonObject()
				.get("rel"))
		);

	}

	@Test
	@UseResurce(value = "jsonRequests/consents/v3/businessConsentExtensionRequest.json", key = "consent_endpoint_request")
	public void businessHappyPath() {
		ConsentExtensionRequestInsertInvalidUser cond = new ConsentExtensionRequestInsertInvalidUser();
		run(cond);

		// Logged user
		Assert.assertEquals(
			"88639174342",
			OIDFJSON.getString(environment.getObject("consent_endpoint_request").get("data").getAsJsonObject()
				.get("loggedUser").getAsJsonObject()
				.get("document").getAsJsonObject()
				.get("identification"))
		);
		Assert.assertEquals(
			"CPF",
			OIDFJSON.getString(environment.getObject("consent_endpoint_request").get("data").getAsJsonObject()
				.get("loggedUser").getAsJsonObject()
				.get("document").getAsJsonObject()
				.get("rel"))
		);

		// Business entity
		Assert.assertEquals(
			"38404035000169",
			OIDFJSON.getString(environment.getObject("consent_endpoint_request").get("data").getAsJsonObject()
				.get("businessEntity").getAsJsonObject()
				.get("document").getAsJsonObject()
				.get("identification"))
		);
		Assert.assertEquals(
			"CNPJ",
			OIDFJSON.getString(environment.getObject("consent_endpoint_request").get("data").getAsJsonObject()
				.get("businessEntity").getAsJsonObject()
				.get("document").getAsJsonObject()
				.get("rel"))
		);

	}




}
