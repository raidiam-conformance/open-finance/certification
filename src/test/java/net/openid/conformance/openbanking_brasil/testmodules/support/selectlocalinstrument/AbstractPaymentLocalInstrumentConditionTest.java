package net.openid.conformance.openbanking_brasil.testmodules.support.selectlocalinstrument;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractPaymentLocalInstrumentCondtion;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public abstract class AbstractPaymentLocalInstrumentConditionTest extends AbstractJsonResponseConditionUnitTest {

	@UseResurce(value="test_resource_payments_v4_single.json", key="resource")
	@Test
	public void happyPath(){
		AbstractPaymentLocalInstrumentCondtion condition = localInstrumentCondition();
		run(condition);
		Assert.assertEquals(localInstrumentString(), OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.localInstrument")));
	}

	@UseResurce(value="brazil_payments_consent_v4_no_details.json", key="resource")
	@Test
	public void unhappyPath(){
		AbstractPaymentLocalInstrumentCondtion condition = localInstrumentCondition();
		ConditionError error = runAndFail(condition);
		Assert.assertTrue(error.getMessage().contains("Configuration is malformed, missing data.payment.details"));
	}

	protected abstract AbstractPaymentLocalInstrumentCondtion localInstrumentCondition();
	protected abstract String localInstrumentString();
}
