package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class EditEnrollmentsRequestBodyToRemoveDebtorAccountAccountTypeFieldTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="jsonRequests/payments/enrollments/postEnrollments.json", key="resource_request_entity_claims")
	public void happyPathTest() {
		run(new EditEnrollmentsRequestBodyToRemoveDebtorAccountAccountTypeField());
		JsonObject debtorAccount = environment
			.getElementFromObject("resource_request_entity_claims", "data.debtorAccount")
			.getAsJsonObject();
		assertFalse(debtorAccount.has("accountType"));
	}

	@Test
	@UseResurce(value="jsonRequests/payments/enrollments/postEnrollmentsNoDebtorAccount.json", key="resource_request_entity_claims")
	public void happyPathTestMissingDebtorAccount() {
		run(new EditEnrollmentsRequestBodyToRemoveDebtorAccountAccountTypeField());
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResourceRequestEntityClaims() {
		run(new EditEnrollmentsRequestBodyToRemoveDebtorAccountAccountTypeField());
	}
}
