package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentAspspRevoked;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentRejectAspspMaxDateReached;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentRejectUserManuallyRejected;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentRevokedByUser;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AbstractConsentRejectionValidationTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedOkMaxDate.json")
	public void happyPathTestAspspMaxDateReached() {
		run(new EnsureConsentRejectAspspMaxDateReached());
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedOkManuallyRejected.json")
	public void happyPathTestUserManuallyRejected() {
		run(new EnsureConsentRejectUserManuallyRejected());
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedOk.json")
	public void happyPathTestRevokedByUser() {
		run(new EnsureConsentRevokedByUser());
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingConsentEndpointResponseFull() {
		run(new EnsureConsentRevokedByUser());
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("consent_endpoint_response_full", new JsonObject());
		unhappyPathTest("Could not extract data from response body");
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject("consent_endpoint_response_full",
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedMissingRejection.json")
	public void unhappyPathTestNoRejectionStatusRejected() {
		unhappyPathTest("consent status is REJECTED but no rejection object found");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponse.json")
	public void unhappyPathTestNoRejectionStatusNotRejected() {
		unhappyPathTest("Rejection object did not match expected results.");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedMissingReason.json")
	public void unhappyPathTestMissingReason() {
		unhappyPathTest("Rejection object did not contain reason object");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedInvalidRejectionReason.json")
	public void unhappyPathTestReasonIsNotObject() {
		unhappyPathTest("Reason object is not a json object, it should have the mandatory field code");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedMissingCode.json")
	public void unhappyPathTestMissingCode() {
		unhappyPathTest("Unable to find rejection code in rejection object");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedMissingRejectedBy.json")
	public void unhappyPathTestMissingRejectedBy() {
		unhappyPathTest("Unable to find rejectedBy inside rejection object");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedEmptyRejectedBy.json")
	public void unhappyPathTestRejectedByIsEmpty() {
		unhappyPathTest("RejectedBy is not a string inside rejection object");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedOkManuallyRejected.json")
	public void unhappyPathTestRejectionReasonCodeNotMatching() {
		unhappyPathTest("Rejection object did not match expected results.");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedOkAspsp.json")
	public void unhappyPathTestRejectedByNotMatching() {
		unhappyPathTest("Rejection object did not match expected results.");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedOk.json")
	public void happyPathTestAspspRevokedWithRejected() {
		environment.putBoolean("code_returned", false);
		run(new EnsureConsentAspspRevoked());
		assertTrue(environment.getBoolean("code_returned"));
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponse.json")
	public void happyPathTestAspspRevokedWithoutRejected() {
		environment.putBoolean("code_returned", false);
		run(new EnsureConsentAspspRevoked());
		assertFalse(environment.getBoolean("code_returned"));
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedOkManuallyRejected.json")
	public void unhappyPathTestAspspRevokedRejectionReasonCodeNotMatching() {
		unhappyPathTestAspspRevoked("Rejection object did not match expected results.");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseRejectedOkAspsp.json")
	public void unhappyPathTestAspspRevokedRejectedByNotMatching() {
		unhappyPathTestAspspRevoked("Rejection object did not match expected results.");
	}

	@Test
	public void unhappyPathTestAspspRevokedUnparseableBody() {
		environment.putObject("consent_endpoint_response_full",
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTestAspspRevoked("Could not parse the body");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureConsentRevokedByUser());
		assertTrue(error.getMessage().contains(message));
	}

	protected void unhappyPathTestAspspRevoked(String message) {
		ConditionError error = runAndFail(new EnsureConsentAspspRevoked());
		assertTrue(error.getMessage().contains(message));
	}
}

