package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateDummyPaymentConsentRequest;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class CreateDummyPaymentConsentRequestTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		run(new CreateDummyPaymentConsentRequest());
		assertNotNull(environment.getObject("consent_endpoint_request"));
	}
}
