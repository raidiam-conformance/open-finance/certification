package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddBrazilPixPaymentToTheResourceTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_PATH_UNHAPPY = "jsonRequests/payments/consents/paymentConsentRequest";
	private static final String BASE_PATH_HAPPY = "jsonRequests/payments/consents/correctPaymentConsentRequest";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	private static final SimpleDateFormat DATETIME_FORMATTER = new SimpleDateFormat("yyyyMMdd");
	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
	private static final String END_TO_END_ID_REGEX = "E(.{8})(\\d{12})(.{11})";

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "WithoutSchedule.json", key = KEY, path = PATH)
	public void happyWithoutSchedule() {
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "WithoutScheduleWithoutOptionalFields.json", key = KEY, path = PATH)
	public void happyWithoutScheduleWithoutOptionalFields() {
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "WithSchedule.json", key = KEY, path = PATH)
	public void happyWithSchedule() {
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "NoPayment.json", key = KEY, path = PATH)
	public void unhappyNoPayment() {
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not extract the payment resource";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "NoAmount.json", key = KEY, path = PATH)
	public void unhappyNoAmount() {
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "amount field is missing in the payment object";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "NoCurrency.json", key = KEY, path = PATH)
	public void unhappyNoCurrency() {
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "currency field is missing in the payment object";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "NoDetails.json", key = KEY, path = PATH)
	public void unhappyNoDetails() {
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "details object is missing in the payment";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "NoLocalInstrument.json", key = KEY, path = PATH)
	public void unhappyNoLocalInstrument() {
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "localInstrument field is missing in the details";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "NoCreditorAccount.json", key = KEY, path = PATH)
	public void unhappyNoCreditorAccount() {
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "creditorAccount object is missing in the details";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "ArraySingleSchedule.json", key = KEY, path = PATH)
	public void happyArraySingleSchedule() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(1, data.size());
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "ArrayDailyScheduleWith5Payments.json", key = KEY, path = PATH)
	public void happyArrayDailyScheduleWith5Payments() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-08-23", "2023-08-24", "2023-08-25", "2023-08-26", "2023-08-27"));
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "ArrayWeeklyScheduleWith5Payments.json", key = KEY, path = PATH)
	public void happyArrayWeeklyScheduleWith5Payments() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-08-28", "2023-09-04", "2023-09-11", "2023-09-18", "2023-09-25"));
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "ArrayMonthlyScheduleWith5Payments1st.json", key = KEY, path = PATH)
	public void happyArrayMonthlyScheduleWith5Payments1st() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-09-01", "2023-10-01", "2023-11-01", "2023-12-01", "2024-01-01"));
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "ArrayMonthlyScheduleWith5Payments29th.json", key = KEY, path = PATH)
	public void happyArrayMonthlyScheduleWith5Payments29th() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-03-01", "2023-03-29", "2023-04-29", "2023-05-29", "2023-06-29"));
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "ArrayMonthlyScheduleWith12Payments31st.json", key = KEY, path = PATH)
	public void happyArrayMonthlyScheduleWith12Payments31st() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(12, data.size());
		validateDates(data,List.of(
			"2023-01-31", "2023-03-01", "2023-03-31", "2023-05-01",
			"2023-05-31", "2023-07-01", "2023-07-31", "2023-08-31",
			"2023-10-01", "2023-10-31", "2023-12-01", "2023-12-31"
		));
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "ArrayMonthlyScheduleWith5Payments30thStartingInFebruary.json", key = KEY, path = PATH)
	public void happyArrayMonthlyScheduleWith2Payments30thStartingInFebruary() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-03-01", "2023-03-30", "2023-04-30", "2023-05-30", "2023-06-30"));
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "ArrayCustomScheduleWith5Payments.json", key = KEY, path = PATH)
	public void happyArrayCustomScheduleWith5Payments() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-08-23", "2023-09-26", "2023-12-04", "2024-01-30", "2024-03-15"));
	}

	@Test
	@UseResurce(value = BASE_PATH_HAPPY + "ArraySingleSchedule.json", key = "config", path = "resource." + PATH)
	public void happyArraySingleScheduleInConfigResource() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(1, data.size());
	}

	@Test
	public void unhappyMissingResourceObject() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "Could not find resource object";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArraySingleInvalidScheduleFormat.json", key = KEY, path = PATH)
	public void unhappyInvalidScheduleFormat() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArraySingleScheduleMissingDate.json", key = KEY, path = PATH)
	public void unhappyArraySingleMissingDate() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayDailyScheduleMissingStartDate.json", key = KEY, path = PATH)
	public void unhappyArrayDailyMissingStartDate() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayDailyScheduleMissingQuantity.json", key = KEY, path = PATH)
	public void unhappyArrayDailyMissingQuantity() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayWeeklyScheduleMissingStartDate.json", key = KEY, path = PATH)
	public void unhappyArrayWeeklyMissingStartDate() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayWeeklyScheduleMissingQuantity.json", key = KEY, path = PATH)
	public void unhappyArrayWeeklyMissingQuantity() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayWeeklyScheduleMissingDayOfWeek.json", key = KEY, path = PATH)
	public void unhappyArrayWeeklyMissingDayOfWeek() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayMonthlyScheduleMissingStartDate.json", key = KEY, path = PATH)
	public void unhappyArrayMonthlyMissingStartDate() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayMonthlyScheduleMissingQuantity.json", key = KEY, path = PATH)
	public void unhappyArrayMonthlyMissingQuantity() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayMonthlyScheduleMissingDayOfMonth.json", key = KEY, path = PATH)
	public void unhappyArrayMonthlyMissingDayOfMonth() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayCustomScheduleMissingDates.json", key = KEY, path = PATH)
	public void unhappyArrayCustomMissingDates() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_PATH_UNHAPPY + "ArrayCustomScheduleMissingAdditionalInformation.json", key = KEY, path = PATH)
	public void unhappyArrayCustomMissingAdditionalInformation() {
		setPaymentsToBeArray();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "data.payment.schedule is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	protected void setPaymentsToBeArray() {
		environment.putString("payment_is_array", "ok");
	}

	protected JsonArray getDataArrayFromPaymentObject() {
		return environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonArray();
	}

	protected void validateEndToEndId(String endToEndId, String date) {
		Pattern pattern = Pattern.compile(END_TO_END_ID_REGEX);
		Matcher matcher = pattern.matcher(endToEndId);
		if (matcher.matches()) {
			String dateTimeString = matcher.group(2).substring(0, 8);
			try {
				Date endToEndIdDate = DATETIME_FORMATTER.parse(dateTimeString);
				Date expectedDate = DATE_FORMATTER.parse(date);
				assertEquals(endToEndIdDate, expectedDate);
			} catch (ParseException e) {
				fail("Could not parse dates.");
			}
		} else {
			fail("Invalid endToEndId format.");
		}
	}

	protected void validateDates(JsonArray data, List<String> dates) {
		for (int i = 0; i < data.size(); i++) {
			String endToEndId = OIDFJSON.getString(data.get(i).getAsJsonObject().get("endToEndId"));
			validateEndToEndId(endToEndId, dates.get(i));
		}
	}
}
