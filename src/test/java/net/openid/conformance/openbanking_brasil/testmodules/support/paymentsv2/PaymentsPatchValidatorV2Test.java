package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentsPatchValidatorV2;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class PaymentsPatchValidatorV2Test extends AbstractJsonResponseConditionUnitTest {
	private static final String END_TO_END_ID = "E9040088820210128000800123873170";

	@Before
	public void init() {
		environment.putString("endToEndId", END_TO_END_ID);

		JsonObject request = new JsonObject();
		environment.putObject("resource_request_entity_claims", request);
		JsonObject data = new JsonObject();
		request.add("data", data);
		data.addProperty("endToEndId", END_TO_END_ID);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/patchPaymentsV2/patchPaymentWithCancellation.json")
	public void validatePatchPaymentWithCancellation() {
		PaymentsPatchValidatorV2 condition = new PaymentsPatchValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/patchPaymentsV2/patchPaymentNoCancellation.json")
	public void validatePatchPaymentWithoutCancellation() {
		PaymentsPatchValidatorV2 condition = new PaymentsPatchValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("cancellation", condition.getApiName())));


	}

}
