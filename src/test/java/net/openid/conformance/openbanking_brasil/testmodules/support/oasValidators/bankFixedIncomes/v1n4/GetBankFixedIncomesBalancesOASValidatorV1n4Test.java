package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetBankFixedIncomesBalancesOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/balances.json")
	public void validateStructure() {
		run(new GetBankFixedIncomesBalancesOASValidatorV1n4());
	}
}
