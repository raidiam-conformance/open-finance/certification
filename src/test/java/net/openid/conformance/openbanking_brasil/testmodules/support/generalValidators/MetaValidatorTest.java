package net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators;

import com.google.gson.JsonElement;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class MetaValidatorTest extends AbstractJsonResponseConditionUnitTest {

    private final static String RESPONSE_ENV_KEY = "resource_endpoint_response_full";
    private final static String JSON_RESPONSES_BASE_PATH = "jsonResponses/generalValidators/metaValidator/";

    class CustomValidator extends AbstractJsonAssertingCondition {
        private final MetaValidator metaValidator;

		private boolean requestDateTimePresent = true;
		private boolean requestDateTimeWrong = false;
		CustomValidator(boolean isTotalRecordsMandatory, boolean isTotalPagesMandatory, boolean isRequestDateTimeMandatory) {
            metaValidator = new MetaValidator(
                this,
                isTotalRecordsMandatory,
                isTotalPagesMandatory,
                isRequestDateTimeMandatory
            );
        }
		public void setRequestDateTimePresent(boolean requestDateTimePresent){
			this.requestDateTimePresent = requestDateTimePresent;
		}

		public void setRequestDateTimeWrong(boolean requestDateTimeWrong){
			this.requestDateTimeWrong = requestDateTimeWrong;
		}
        CustomValidator() {
            metaValidator = new MetaValidator(this);
        }

        @Override
        @PreEnvironment(required = RESPONSE_ENV_KEY)
        public Environment evaluate(Environment env) {
            JsonElement body = bodyFrom(env,RESPONSE_ENV_KEY);
			if(requestDateTimePresent){body=generateRequestDateTime(body);}
            metaValidator.assertMetaObject(body);
            String errorMessage = metaValidator.getErrorMessage();
            if (!Objects.equals(errorMessage, "")) {
                throw error(errorMessage, metaValidator.getArgs());
            }
            return env;
        }

		protected JsonElement generateRequestDateTime(JsonElement  body){
			Instant requestDateTimeInstant = Instant.now();
			if(requestDateTimeWrong)
			{
				requestDateTimeInstant = requestDateTimeInstant.minusSeconds(400);
			}
			requestDateTimeInstant=requestDateTimeInstant.truncatedTo(ChronoUnit.SECONDS);
			body.getAsJsonObject().getAsJsonObject("meta").addProperty("requestDateTime",requestDateTimeInstant.toString());
			return body;
		}

    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseStandard.json")
    public void validateMetaValidator() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }


	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseBalloonPayments.json")
	public void validateMetaValidatorBalloonPayments() {
		CustomValidator condition = new CustomValidator();
		run(condition);
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseErrors.json")
	public void validateMetaWithErrorsValidator() {
		CustomValidator condition = new CustomValidator();
		run(condition);
	}

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseMissingMeta.json")
    public void validateStructureMissingMeta() {
        CustomValidator condition = new CustomValidator();
		condition.setRequestDateTimePresent(false);
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("meta", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingTotalRecords.json")
    public void validateStructureMissingTotalRecordsNotAllowed() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("totalRecords", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingTotalRecords.json")
    public void validateStructureMissingTotalRecordsAllowed() {
        CustomValidator condition = new CustomValidator(false, true, true);
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingTotalPages.json")
    public void validateStructureMissingTotalPagesNotAllowed() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("totalPages", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingTotalPages.json")
    public void validateStructureMissingTotalPagesAllowed() {
        CustomValidator condition = new CustomValidator(true, false, true);
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingRequestDateTime.json")
    public void validateStructureMissingRequestDateTimeNotAllowed() {
        CustomValidator condition = new CustomValidator();
		condition.setRequestDateTimePresent(false);
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("requestDateTime", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "responseMissingRequestDateTime.json")
    public void validateStructureMissingRequestDateTimeAllowed() {
        CustomValidator condition = new CustomValidator(true, true, false);
		condition.setRequestDateTimePresent(false);
		run(condition);
    }

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseStandard.json")
	public void validateStructureOverdueRequestDateTime() {
		CustomValidator condition = new CustomValidator(true, true, false);
		condition.setRequestDateTimeWrong(true);
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(
			"Value of requestDateTime is not in expected range"
		));
	}
    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalRecordsGreaterThanPageSizeTotalPagesLessOrEqualToOne.json")
    public void validateStructureTotalRecordsGreaterThanPageSizeTotalPagesLessOrEqualToOne() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "Number of records exceeds page size, but totalPages is not greater than 1"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalRecordsGreaterThanPageSizeTotalPagesNotEqualExpectedAmount.json")
    public void validateStructureTotalRecordsGreaterThanPageSizeTotalPagesNotEqualExpectedAmount() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "The totalPages value is not equal to the amount of pages expected"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseTotalRecordsGreaterThanPageSize.json")
    public void validateStructureCorrectTotalRecordsGreaterThanPageSize() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalRecordsSmallerThanPageSizeTotalPagesNotEqualOne.json")
    public void validateStructureTotalRecordsSmallerThanPageSizeTotalPagesNotEqualOne() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "Number of records does not exceed page size, but totalPages is not equal to 1"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalRecordsSmallerThanPageSizeNumberOfRecordsNotEqualTotalRecords.json")
    public void validateStructureTotalRecordsSmallerThanPageSizeNumberOfRecordsNotEqualTotalRecords() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "Number of records does not match totalRecords"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseTotalRecordsSmallerThanPageSize.json")
    public void validateStructureCorrectTotalRecordsSmallerThanPageSize() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseZeroRecordsTotalPagesGreaterThanZero.json")
    public void validateStructureZeroRecordsTotalPagesGreaterThanZero() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "When there are no resources, both totalRecords and totalPages should be 0"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseZeroRecordsTotalRecordsGreaterThanZero.json")
    public void validateStructureZeroRecordsTotalRecordsGreaterThanZero() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "When there are no resources, both totalRecords and totalPages should be 0"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseZeroRecords.json")
    public void validateStructureCorrectZeroRecords() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }
}
