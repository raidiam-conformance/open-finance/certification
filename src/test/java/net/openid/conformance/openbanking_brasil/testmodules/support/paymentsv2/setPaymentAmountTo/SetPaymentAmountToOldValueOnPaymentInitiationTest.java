package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

public class SetPaymentAmountToOldValueOnPaymentInitiationTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathArrayTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();

		String rawJsonClaim = IOUtils.resourceToString("resource_request_entity_claims_array.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject resourceRequestEntityClaims = new JsonParser().parse(rawJsonClaim).getAsJsonObject();
		environment.putObject("resource_request_entity_claims",resourceRequestEntityClaims);

		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putString("old_amount", "12345.67");
		environment.putObject("resource", resourceConfig);
		environment.putString("payment_is_array", "ok");


		SetPaymentAmountToOldValueOnPaymentInitiation condition = new SetPaymentAmountToOldValueOnPaymentInitiation();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);
		JsonObject paymentConfig = environment.getObject("resource_request_entity_claims").getAsJsonArray("data").get(0).getAsJsonObject();
		String amount = OIDFJSON.getString(paymentConfig.get("payment").getAsJsonObject().get("amount"));
		Assert.assertEquals(environment.getString("old_amount"), amount);
	}

	@Test
	public void happyPathSingleTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();

		String rawJsonClaim = IOUtils.resourceToString("resource_request_entity_claims.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject resourceRequestEntityClaims = new JsonParser().parse(rawJsonClaim).getAsJsonObject();
		environment.putObject("resource_request_entity_claims",resourceRequestEntityClaims);

		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putString("old_amount", "12345.67");
		environment.putObject("resource", resourceConfig);


		SetPaymentAmountToOldValueOnPaymentInitiation condition = new SetPaymentAmountToOldValueOnPaymentInitiation();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);
		JsonObject paymentConfig = environment.getElementFromObject("resource_request_entity_claims","data.payment").getAsJsonObject();
		String amount = OIDFJSON.getString(paymentConfig.get("amount"));
		Assert.assertEquals(environment.getString("old_amount"), amount);
	}

	@Test
	public void unhappyPathArrayTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();

		String rawJsonClaim = IOUtils.resourceToString("resource_request_entity_claims_array_error.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject resourceRequestEntityClaims = new JsonParser().parse(rawJsonClaim).getAsJsonObject();
		environment.putObject("resource_request_entity_claims",resourceRequestEntityClaims);

		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putString("old_amount", "12345.67");
		environment.putObject("resource", resourceConfig);
		environment.putString("payment_is_array", "ok");


		SetPaymentAmountToOldValueOnPaymentInitiation condition = new SetPaymentAmountToOldValueOnPaymentInitiation();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		try {
			condition.execute(environment);
			fail("Should have thrown an error");
		} catch(ConditionError e) {
			assertThat(e.getMessage(), containsString("There is no data.payment object on resource_request_entity_claims"));
		}
	}

	@Test
	public void unhappyPathSingleTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();

		String rawJsonClaim = IOUtils.resourceToString("resource_request_entity_claims_error.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject resourceRequestEntityClaims = new JsonParser().parse(rawJsonClaim).getAsJsonObject();
		environment.putObject("resource_request_entity_claims",resourceRequestEntityClaims);

		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putString("old_amount", "12345.67");
		environment.putObject("resource", resourceConfig);


		SetPaymentAmountToOldValueOnPaymentInitiation condition = new SetPaymentAmountToOldValueOnPaymentInitiation();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		try {
			condition.execute(environment);
			fail("Should have thrown an error");
		} catch(ConditionError e) {
			assertThat(e.getMessage(), containsString("There is no data.payment object on resource_request_entity_claims"));
		}
	}
}
