package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class InjectRealCreditorAccountToPaymentPhoneTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathSingleTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4_single.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);
		InjectRealCreditorAccountToPaymentPhone condition = new InjectRealCreditorAccountToPaymentPhone();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);

		JsonObject data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonObject("data");
		assertEquals(DictHomologKeys.PROXY_PHONE_NUMBER_BRANCH_NUMBER, OIDFJSON.getString(data.getAsJsonObject("creditorAccount").get("issuer")));
		assertEquals(DictHomologKeys.PROXY_PHONE_NUMBER_ACCOUNT_NUMBER, OIDFJSON.getString(data.getAsJsonObject("creditorAccount").get("number")));
		assertEquals(DictHomologKeys.PROXY_PHONE_NUMBER_ACCOUNT_TYPE, OIDFJSON.getString(data.getAsJsonObject("creditorAccount").get("accountType")));
		assertEquals(DictHomologKeys.PROXY_PHONE_NUMBER_ISPB, OIDFJSON.getString(data.getAsJsonObject("creditorAccount").get("ispb")));
	}

	@Test
	public void happyPathArrayTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);

		InjectRealCreditorAccountToPaymentPhone condition = new InjectRealCreditorAccountToPaymentPhone();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);

		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");

		for (int i = 0 ; i < data.size(); i++) {
			assertEquals(DictHomologKeys.PROXY_PHONE_NUMBER_BRANCH_NUMBER, OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("creditorAccount").get("issuer")));
			assertEquals(DictHomologKeys.PROXY_PHONE_NUMBER_ACCOUNT_NUMBER, OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("creditorAccount").get("number")));
			assertEquals(DictHomologKeys.PROXY_PHONE_NUMBER_ACCOUNT_TYPE, OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("creditorAccount").get("accountType")));
			assertEquals(DictHomologKeys.PROXY_PHONE_NUMBER_ISPB, OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("creditorAccount").get("ispb")));
		}
	}
}
