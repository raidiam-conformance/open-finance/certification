package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import org.junit.Assert;

public class GetPaymentConsentsV4EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/payments/v4/consents";
	}

	@Override
	protected String getApiFamilyType() {
		return "payments-consents";
	}

	@Override
	protected String getApiVersion() {
		return "4.0.0";
	}

	@Override
	protected boolean isResource() {
		return false;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetPaymentConsentsV4Endpoint();
	}

	@Override
	protected void assertUrlNotFound() {
		run(getCondition());
		Assert.assertNotNull(environment.getString("warning_message"));
	}
}
