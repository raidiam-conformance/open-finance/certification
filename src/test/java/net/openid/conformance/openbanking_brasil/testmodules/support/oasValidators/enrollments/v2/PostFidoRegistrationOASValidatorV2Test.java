package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostFidoRegistrationOASValidatorV2Test extends AbstractEnrollmentsOASV2ValidatorsTest {

	@Override
	protected AbstractEnrollmentsOASValidatorV2 validator() {
		return new PostFidoRegistrationOASValidatorV2();
	}

	@Test
	@UseResurce(JSON_PATH + "GenericErrorResponse.json")
	public void happyPathTest401() {
		happyPathTest(401);
	}
}
