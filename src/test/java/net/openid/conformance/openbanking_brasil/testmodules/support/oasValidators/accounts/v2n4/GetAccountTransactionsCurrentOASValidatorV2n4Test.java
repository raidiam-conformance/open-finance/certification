package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class GetAccountTransactionsCurrentOASValidatorV2n4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/account/v2n/getAccountTransactionsCurrentResponseOAS.json")
	public void testHappyPath() {
		GetAccountTransactionsCurrentOASValidatorV2n4 cond = new GetAccountTransactionsCurrentOASValidatorV2n4();
		run(cond);

		// Partie Cnpj Cpf constraint.
		JsonObject folhaPagamentoData = new JsonObject();
		folhaPagamentoData.addProperty("type", "FOLHA_PAGAMENTO");
		assertThrows(ConditionError.class, () -> cond.assertTypeAndPartieCnpjCpfConstraint(folhaPagamentoData));
	}

	@Test
	@UseResurce("jsonResponses/errors/goodErrorResponseNoLinksNoMeta.json")
	public void testHappyPathCode403() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 403);
		GetAccountTransactionsCurrentOASValidatorV2n4 cond = new GetAccountTransactionsCurrentOASValidatorV2n4();
		run(cond);
	}
}
