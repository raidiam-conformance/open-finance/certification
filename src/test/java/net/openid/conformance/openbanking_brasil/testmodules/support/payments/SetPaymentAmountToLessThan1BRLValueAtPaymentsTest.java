package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetPaymentAmountToLessThan1BRLValueAtPaymentsTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";
	private static final String expectedAmount = "0.75";

	@Test
	@UseResurce(value="jsonRequests/payments/payments/paymentWithDataArrayWith5Payments.json", key=KEY, path=PATH)
	public void happyPathArrayTest() {
		SetPaymentAmountToLessThan1BRLValueAtPayments condition = new SetPaymentAmountToLessThan1BRLValueAtPayments();
		run(condition);

		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");

		for (int i = 0 ; i < data.size(); i++) {

			String amount = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("payment").get("amount"));
			assertEquals(expectedAmount, amount);
		}
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentWithCpf.json", key=KEY, path=PATH)
	public void happyPathSingleTest() {
		SetPaymentAmountToLessThan1BRLValueAtPayments condition = new SetPaymentAmountToLessThan1BRLValueAtPayments();
		run(condition);

		String amount = OIDFJSON.getString(environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals(expectedAmount, amount);
	}

}
