package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GetTreasureTitlesTransactionsV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/treasureTitles/transactions.json")
	public void happyPath() {
		run(new GetTreasureTitlesTransactionsV1OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/treasureTitles/transactionsNoIncomeTax.json")
	public void noIncomeTax() {
		ConditionError e = runAndFail(new GetTreasureTitlesTransactionsV1OASValidator());
		assertTrue(e.getMessage().contains("incomeTax is required when type is SAIDA"));
	}

	@Test
	@UseResurce("jsonResponses/treasureTitles/transactionsNoTransactionalTax.json")
	public void noTransactionalTax() {
		ConditionError e = runAndFail(new GetTreasureTitlesTransactionsV1OASValidator());
		assertTrue(e.getMessage().contains("financialTransactionTax is required when type is SAIDA"));
	}

	@Test
	@UseResurce("jsonResponses/treasureTitles/transactionsNoTransactionRate.json")
	public void noTransactionRate() {
		ConditionError e = runAndFail(new GetTreasureTitlesTransactionsV1OASValidator());
		assertTrue(e.getMessage().contains("remunerationTransactionRate is required when type is ENTRADA"));
	}

	@Test
	@UseResurce("jsonResponses/treasureTitles/transactionsNoTransactionTypeAdditionalInfo.json")
	public void noTransactionTypeAdditionalInfo() {
		ConditionError e = runAndFail(new GetTreasureTitlesTransactionsV1OASValidator());
		assertTrue(e.getMessage().contains("transactionTypeAdditionalInfo is required when transactionType is OUTROS"));
	}

}
