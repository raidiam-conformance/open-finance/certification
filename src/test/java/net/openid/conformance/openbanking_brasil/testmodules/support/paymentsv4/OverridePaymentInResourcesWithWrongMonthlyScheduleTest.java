package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.overridePaymentsInResources.OverridePaymentInResourcesWithWrongMonthlySchedule;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OverridePaymentInResourcesWithWrongMonthlyScheduleTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_CONSENTS_PATH_HAPPY = "jsonRequests/payments/consents/correctPaymentConsentRequest";
	private static final String BASE_PAYMENTS_PATH = "jsonRequests/payments/payments/paymentWithDataArrayWith";
	private static final String KEY = "resource";
	private static final String CONSENTS_PATH = "brazilPaymentConsent";
	private static final String PAYMENTS_PATH = "brazilPixPayment";

	private static final SimpleDateFormat DATETIME_FORMATTER = new SimpleDateFormat("yyyyMMdd");
	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
	private static final String END_TO_END_ID_REGEX = "E(.{8})(\\d{12})(.{11})";

	@Before
	public void init() {
		setPaymentsToBeArray();
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayDailyScheduleWith5Payments.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "5Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void unhappyPaymentsMissingMonthlyFieldTest() {
		OverridePaymentInResourcesWithWrongMonthlySchedule condition = new OverridePaymentInResourcesWithWrongMonthlySchedule();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not extract the monthly from the payment in resource";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayMonthlyScheduleWith5Payments1st.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "12Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void unhappyPaymentsDataArrayWithWrongSizeTest() {
		OverridePaymentInResourcesWithWrongMonthlySchedule condition = new OverridePaymentInResourcesWithWrongMonthlySchedule();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Payments data array does not have the correct amount of payments in it";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayMonthlyScheduleWith5Payments1st.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "InvalidEndToEndId.json", key = KEY, path = PAYMENTS_PATH)
	public void unhappyPaymentsWithInvalidEndToEndIdTest() {
		OverridePaymentInResourcesWithWrongMonthlySchedule condition = new OverridePaymentInResourcesWithWrongMonthlySchedule();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "endToEndId is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayMonthlyScheduleWith5Payments1st.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "5Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void happy5Payments1st() {
		OverridePaymentInResourcesWithWrongMonthlySchedule condition = new OverridePaymentInResourcesWithWrongMonthlySchedule();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-09-01", "2023-10-01", "2023-11-01", "2023-12-01", "2024-01-01"));
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayMonthlyScheduleWith5Payments29th.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "5Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void happy5Payments29th() {
		OverridePaymentInResourcesWithWrongMonthlySchedule condition = new OverridePaymentInResourcesWithWrongMonthlySchedule();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-02-28", "2023-03-29", "2023-04-29", "2023-05-29", "2023-06-29"));
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayMonthlyScheduleWith12Payments31st.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "12Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void happy12Payments31st() {
		OverridePaymentInResourcesWithWrongMonthlySchedule condition = new OverridePaymentInResourcesWithWrongMonthlySchedule();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(12, data.size());
		validateDates(data,List.of(
			"2023-01-31", "2023-02-28", "2023-03-31", "2023-04-30",
			"2023-05-31", "2023-06-30", "2023-07-31", "2023-08-31",
			"2023-09-30", "2023-10-31", "2023-11-30", "2023-12-31"
		));
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayMonthlyScheduleWith5Payments30thStartingInFebruary.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "5Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void happy2Payments30thStartingInFebruary() {
		OverridePaymentInResourcesWithWrongMonthlySchedule condition = new OverridePaymentInResourcesWithWrongMonthlySchedule();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-02-28", "2023-03-30", "2023-04-30", "2023-05-30", "2023-06-30"));
	}

	protected JsonArray getDataArrayFromPaymentObject() {
		return environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonArray();
	}

	protected void validateEndToEndId(String endToEndId, String date) {
		Pattern pattern = Pattern.compile(END_TO_END_ID_REGEX);
		Matcher matcher = pattern.matcher(endToEndId);
		if (matcher.matches()) {
			String dateTimeString = matcher.group(2).substring(0, 8);
			try {
				Date endToEndIdDate = DATETIME_FORMATTER.parse(dateTimeString);
				Date expectedDate = DATE_FORMATTER.parse(date);
				assertEquals(endToEndIdDate, expectedDate);
			} catch (ParseException e) {
				fail("Could not parse dates.");
			}
		} else {
			fail("Invalid endToEndId format.");
		}
	}

	protected void setPaymentsToBeArray() {
		environment.putString("payment_is_array", "ok");
	}

	protected void validateDates(JsonArray data, List<String> dates) {
		for (int i = 0; i < data.size(); i++) {
			String endToEndId = OIDFJSON.getString(data.get(i).getAsJsonObject().get("endToEndId"));
			validateEndToEndId(endToEndId, dates.get(i));
		}
	}
}
