package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v4.enums.PaymentConsentStatusEnumV4;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class EnsureExpirationDateTimeTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentValidatorV4ResponsePartiallyAccepted.json")
	public void happyPathSingleTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4PartiallyAcceptedManyOK.json")
	public void happyPathArrayTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4PartiallyAcceptedSingleBadExpirationDateTime.json")
	public void unhappyPathSingleTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("expirationDateTime is after date"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4PartiallyAcceptedDailyBadExpirationDateTime.json")
	public void unhappyPathDailyTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("expirationDateTime is after startDate"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4PartiallyAcceptedWeeklyBadExpirationDateTime.json")
	public void unhappyPathWeeklyTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("expirationDateTime is after startDate"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4PartiallyAcceptedMonthlyBadExpirationDateTime.json")
	public void unhappyPathMonthlyTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("expirationDateTime is after startDate"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4PartiallyAcceptedCustomBadExpirationDateTime.json")
	public void unhappyPathCustomTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("expirationDateTime is after startDate"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4BadNoStatus.json")
	public void unhappyPathNoStatusTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Body does not have data.status field"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4EmptySchedule.json")
	public void unhappyPathEmptyOrIncorrectScheduleTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Payment schedule is empty or incorrect"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentValidatorV4ResponseOK.json")
	public void happyPathDifferentStatusTest() {
		EnsureExpirationDateTime condition = new EnsureExpirationDateTime();
		run(condition);
		String status = environment.getString("resource_endpoint_response_full","body_json.data.status");
		assertThat(status, !status.equals(PaymentConsentStatusEnumV4.PARTIALLY_ACCEPTED.toString()));
	}

}
