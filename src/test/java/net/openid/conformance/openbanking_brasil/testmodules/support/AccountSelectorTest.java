package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class AccountSelectorTest extends AbstractJsonResponseConditionUnitTest {
	@Test
	@UseResurce("jsonResponses/account/v2n/accountListResponseV2n.json")
	public void testHappyPath(){
		run(new AccountSelector());
		String expectedId = OIDFJSON.getString(environment.getElementFromObject("resource_endpoint_response_full","body_json.data").getAsJsonArray().get(0).getAsJsonObject().get("accountId"));
		String actualId = environment.getString("accountId");
		Assert.assertEquals(actualId,expectedId);
	}
	@Test
	@UseResurce("jsonResponses/account/v2n/accountListEmptyDataResponseV2n.json")
	public void testUnhappyPathNoData() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		AccountSelector condition = new AccountSelector();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Data field is empty, no further processing required."));
	}

	@Test
	public void testUnhappyPathNoBody() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		AccountSelector condition = new AccountSelector();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}
}
