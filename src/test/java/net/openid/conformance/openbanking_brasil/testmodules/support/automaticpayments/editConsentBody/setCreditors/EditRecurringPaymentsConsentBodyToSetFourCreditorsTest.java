package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.setCreditors;

import com.google.gson.JsonArray;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.AbstractEditRecurringPaymentsConsentBodyToSetCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.EditRecurringPaymentsConsentBodyToSetFourCreditors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EditRecurringPaymentsConsentBodyToSetFourCreditorsTest extends AbstractEditRecurringPaymentsConsentBodyToSetCreditorTest {

	@Override
	protected void additionalValidation(JsonArray creditors) {
		assertEquals(environment.getObject("first_creditor"), creditors.get(0).getAsJsonObject());
		assertEquals(environment.getObject("second_creditor"), creditors.get(1).getAsJsonObject());
	}

	@Override
	protected AbstractEditRecurringPaymentsConsentBodyToSetCreditor condition() {
		return new EditRecurringPaymentsConsentBodyToSetFourCreditors();
	}

	@Override
	protected int expectedAmountOfCreditors() {
		return 4;
	}

	@Override
	protected String expectedPersonType() {
		return "PESSOA_JURIDICA";
	}
}
