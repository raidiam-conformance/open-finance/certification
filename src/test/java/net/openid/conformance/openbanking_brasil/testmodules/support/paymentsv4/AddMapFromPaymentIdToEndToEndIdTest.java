package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddMapFromPaymentIdToEndToEndIdTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_PATH = "jsonResponses/paymentInitiation/pixByPayments/v4/";

	@Test
	@UseResurce("jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseOK.json")
	public void testDataAsObjectHappyPath() {

		AddMapFromPaymentIdToEndToEndId cond = new AddMapFromPaymentIdToEndToEndId();
		run(cond);

		assertEquals(
			"E9040088820210128000800123873170",
			environment.getString("end_to_end_id_map", "TXpRMU9UQTROMWhZV2xSU1FUazJSMDl")
		);
	}

	@Test
	@UseResurce(BASE_PATH + "standardPostPaymentsV4ResponseWith2Payments.json")
	public void testDataAsArrayHappyPath() {

		AddMapFromPaymentIdToEndToEndId cond = new AddMapFromPaymentIdToEndToEndId();
		run(cond);

		assertEquals(
			"E9040088820210128000800123873170",
			environment.getString("end_to_end_id_map", "TXpRMU9UQTROMWhZV2xSU1FUazJSMDl")
		);
		assertEquals(
			"E9040088820210128000800123873170",
			environment.getString("end_to_end_id_map", "TXpRMU9UQTROMWhZV2xSU1FUazJSMDf")
		);
	}

	@Test
	public void testNoBodyProvided() {
		AddMapFromPaymentIdToEndToEndId cond = new AddMapFromPaymentIdToEndToEndId();
		ConditionError error = runAndFail(cond);
		assertTrue(error.getMessage().contains("body"));
	}

	@Test
	@UseResurce(BASE_PATH + "postPaymentsV4ResponseMissingPaymentId.json")
	public void testMissingPaymentId() {
		AddMapFromPaymentIdToEndToEndId cond = new AddMapFromPaymentIdToEndToEndId();
		ConditionError error = runAndFail(cond);
		assertTrue(error.getMessage().contains("paymentId"));
	}

	@Test
	@UseResurce(BASE_PATH + "postPaymentsV4ResponseMissingEndToEndId.json")
	public void testMissingEndToEndId() {
		AddMapFromPaymentIdToEndToEndId cond = new AddMapFromPaymentIdToEndToEndId();
		ConditionError error = runAndFail(cond);
		assertTrue(error.getMessage().contains("endToEndId"));
	}
}
