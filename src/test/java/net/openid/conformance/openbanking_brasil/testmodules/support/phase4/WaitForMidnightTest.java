package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractWaitUntilMidnight;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.Supplier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WaitForMidnightTest extends AbstractJsonResponseConditionUnitTest  {

	private AbstractWaitUntilMidnight waitUntilMidnight;

	@Test
	public void test900SecondsUntilMidnight() {

		Supplier<ZonedDateTime> mockNowSupplier = () -> ZonedDateTime.of(LocalDate.now(), LocalTime.of(23, 45, 0), ZoneId.of("America/Sao_Paulo"));
		WaitForMidnight condition = new WaitForMidnight(mockNowSupplier);

		long secondsUntilMidnight = condition.getSecondsUntilMidnight();
		assertEquals(900, secondsUntilMidnight);
	}

	@Test
	public void testSecondsUntilMidnightShouldBeAdjustedWhenCurrentTimeIsAfterMidnight() {
		Supplier<ZonedDateTime> mockNowSupplier = () -> ZonedDateTime.of(LocalDate.now().plusDays(2), LocalTime.of(0, 0, 1), ZoneId.of("America/Sao_Paulo"));
		WaitForMidnight condition = new WaitForMidnight(mockNowSupplier);

		long secondsUntilMidnight = condition.getSecondsUntilMidnight();
		assertTrue(secondsUntilMidnight > 0);
		assertTrue(secondsUntilMidnight < 24 * 60 * 60);
	}

	@Test
	public void testSecondsUntilMidnightWhenCurrentTimeIsMidnight() {
		Supplier<ZonedDateTime> mockNowSupplier = () -> ZonedDateTime.of(LocalDate.now(), LocalTime.of(0, 0, 0), ZoneId.of("America/Sao_Paulo"));
		WaitForMidnight condition = new WaitForMidnight(mockNowSupplier);

		long secondsUntilMidnight = condition.getSecondsUntilMidnight();
		assertEquals( 24 * 60 * 60, secondsUntilMidnight);
	}

	@Test
	public void testTargetMidnightWhenCurrentTimeIsBeforeMidnightButNotSameDay() {
		Supplier<ZonedDateTime> mockNowSupplier = () -> ZonedDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(23, 59, 59), ZoneId.of("America/Sao_Paulo"));
		WaitForMidnight condition = new WaitForMidnight(mockNowSupplier);

		long secondsUntilMidnight = condition.getSecondsUntilMidnight();
		assertTrue(secondsUntilMidnight > 0);
	}

	@Test
	public void testTargetMidnightWhenCurrentTimeIsAfterMidnightButNotSameDay() {
		Supplier<ZonedDateTime> mockNowSupplier = () -> ZonedDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(0, 0, 1), ZoneId.of("America/Sao_Paulo"));
		WaitForMidnight condition = new WaitForMidnight(mockNowSupplier);

		long secondsUntilMidnight = condition.getSecondsUntilMidnight();
		assertTrue(secondsUntilMidnight > 0);
		assertTrue(secondsUntilMidnight < 24 * 60 * 60);
	}

	@Test
	public void testGetSecondsUntilMidnight() {
		WaitForMidnight condition = new WaitForMidnight();
		long secondsUntilMidnight = condition.getSecondsUntilMidnight();
		assertTrue(secondsUntilMidnight >= 0);
		assertTrue(secondsUntilMidnight < 24 * 60 * 60);
	}

	@Test
	public void testEvaluate() {
		waitUntilMidnight = new AbstractWaitUntilMidnight() {
			@Override
			public long getSecondsUntilMidnight() {
				return 0;
			}
		};
		run(waitUntilMidnight);
	}
}
