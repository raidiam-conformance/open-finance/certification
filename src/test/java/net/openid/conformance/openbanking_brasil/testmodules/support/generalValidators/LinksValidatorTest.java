package net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators;

import com.google.gson.JsonElement;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.Objects;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class LinksValidatorTest extends AbstractJsonResponseConditionUnitTest {

    class CustomValidator extends AbstractJsonAssertingCondition {
        private final LinksValidator linksValidator;
        private final String requestUri;
        private final int expectedVersion;

        CustomValidator() {
            linksValidator = new LinksValidator(this);
            requestUri = "https://api.banco.com.br/open-banking/api/v1/resource";
            expectedVersion = 1;
        }

        CustomValidator(String requestUri) {
            linksValidator = new LinksValidator(this);
            this.requestUri = requestUri;
            expectedVersion = 1;
        }

        CustomValidator(int expectedVersion) {
            linksValidator = new LinksValidator(this);
            requestUri = "https://api.banco.com.br/open-banking/api/v1/resource";
            this.expectedVersion = expectedVersion;
        }

        @Override
        @PreEnvironment(required = RESPONSE_ENV_KEY)
        public Environment evaluate(Environment env) {
            JsonElement body = bodyFrom(env,RESPONSE_ENV_KEY);
            linksValidator.assertLinksObject(body, requestUri, expectedVersion);
            String errorMessage = linksValidator.getErrorMessage();
            if (!Objects.equals(errorMessage, "")) {
                throw error(errorMessage, linksValidator.getArgs());
            }
            return env;
        }
    }

    private final static String RESPONSE_ENV_KEY = "resource_endpoint_response_full";
    private final static String JSON_RESPONSES_BASE_PATH = "jsonResponses/generalValidators/linksValidator/";

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseMissingLinks.json")
    public void validateStructureMissingLinks() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("links", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseMissingSelfLink.json")
    public void validateStructureMissingSelfLink() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("self", condition.getApiName())
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalPagesGreaterThanOneNoPage.json")
    public void validateStructureTotalPagesGreaterThanOneNoPage() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "totalPages parameter is greater than 1, but page parameter is not valid"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalPagesGreaterThanOnePageGreaterThanTotalPages.json")
    public void validateStructureTotalPagesGreaterThanOnePageGreaterThanTotalPages() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "totalPages parameter is greater than 1, but page parameter is not valid"
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalPagesGreaterThanOnePageEqualsOneNextAndLastMissing.json")
    public void validateStructureTotalPagesGreaterThanOnePageEqualsOneNextAndLastMissing() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
                ErrorMessagesUtils.createElementNotFoundMessage("next", condition.getApiName()))
        );
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalPagesGreaterThanOnePageEqualsTotalPagesFirstAndPrevMissing.json")
    public void validateStructureTotalPagesGreaterThanOnePageEqualsTotalPagesFirstAndPrevMissing() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("first", condition.getApiName()))
        );
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseTotalPagesGreaterThanOnePageBetweenOneAndTotalPagesAllMissing.json")
    public void validateStructureTotalPagesGreaterThanOnePageBetweenOneAndTotalPagesAllMissing() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            ErrorMessagesUtils.createElementNotFoundMessage("first", condition.getApiName()))
        );
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseWithoutTotalPages.json")
    public void validateStructureWithoutTotalPages() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponsePageParameterDifferentInRequestAndSelf.json")
    public void validateStructurePageParameterDifferentInRequestAndSelf() {
        CustomValidator condition = new CustomValidator(
            "https://api.banco.com.br/open-banking/api/v1/resource?page=1");
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "page parameter on 'self' link does not match the one on the request URI."
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseWithoutTotalPages.json")
    public void validateStructureVersionDifferentFromExpected() {
        CustomValidator condition = new CustomValidator(2);
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "version on 'self' link does not match expected value."
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseWrongOrderFirst.json")
    public void validateStructureWrongOrderFirst() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "'first' link is present and has a page parameter, but it is not equal to 1."
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseWrongOrderPrev.json")
    public void validateStructureWrongOrderPrev() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "'prev' link is present and has a page parameter, but it is not equal to 'self' - 1."
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseWrongOrderNext.json")
    public void validateStructureWrongOrderNext() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "'next' link is present and has a page parameter, but it is not equal to 'self' + 1."
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "badResponseWrongOrderLast.json")
    public void validateStructureWrongOrderLast() {
        CustomValidator condition = new CustomValidator();
        ConditionError error = runAndFail(condition);
        assertThat(error.getMessage(), containsString(
            "'last' link is present and has a page parameter, but it is not equal to totalPages."
        ));
    }

    @Test
    @UseResurce(JSON_RESPONSES_BASE_PATH + "goodResponseWithEverything.json")
    public void validateStructureWithEverything() {
        CustomValidator condition = new CustomValidator();
        run(condition);
    }
}
