package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.PrepareToGetConsentExtensions;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrepareToGetConsentExtensionsTests extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void init() {
		environment.putString("consent_id", "123456789");
		JsonObject config = new JsonObject();
		JsonObject resource = new JsonObject();
		resource.addProperty("consentUrl", "https://consent-api.com.br");
		config.add("resource", resource);
		environment.putObject("config", config);
	}

	protected void assertExtensionUrl(String expectedUrl) {
		String extensionUrl = environment.getString("consent_extension_url");
		assertEquals(expectedUrl, extensionUrl);
	}

	protected void assertHttpMethod(String expectedHttpMethod) {
		String httpMethod = environment.getString("http_method");
		assertEquals(expectedHttpMethod, httpMethod);
	}
	@Test
	public void prepareToGetConsentExtensionsHappy(){
		PrepareToGetConsentExtensions condition = new PrepareToGetConsentExtensions();
		run(condition);
		assertExtensionUrl("https://consent-api.com.br/123456789/extensions");
		assertHttpMethod("GET");
	}

	@Test
	public void prepareToGetConsentExtensionsNoConsentId(){
		environment.removeNativeValue("consent_id");
		PrepareToGetConsentExtensions condition = new PrepareToGetConsentExtensions();
		ConditionError error = runAndFail(condition);
		assertEquals(error.getMessage(), "PrepareToGetConsentExtensions: [pre] Something unexpected happened (this could be caused by something you did wrong, or it may be an issue in the test suite - please review the instructions and your configuration, if you still see a problem please contact certification@oidf.org with the full details) - couldn't find string in environment: consent_id");
	}

	@Test
	public void prepareToGetConsentExtensionsNoConfig(){
		environment.removeObject("config");
		PrepareToGetConsentExtensions condition = new PrepareToGetConsentExtensions();
		ConditionError error = runAndFail(condition);
		assertEquals(error.getMessage(), "PrepareToGetConsentExtensions: [pre] Something unexpected happened (this could be caused by something you did wrong, or it may be an issue in the test suite - please review the instructions and your configuration, if you still see a problem please contact certification@oidf.org with the full details) - couldn't find object in environment: config");
	}

	@Test
	public void prepareToGetConsentExtensionsNoConsentUrl(){
		JsonObject config = new JsonObject();
		environment.putObject("config", config);
		PrepareToGetConsentExtensions condition = new PrepareToGetConsentExtensions();
		ConditionError error = runAndFail(condition);
		assertEquals(error.getMessage(), "PrepareToGetConsentExtensions: Couldn't find consentUrl in configuration");
	}

}
