package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class LoadPaymentIdsTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value = "environmentVariables/payments/v4/paymentIdsWithMoreThan1Element.json", key = "saved_payment_ids")
	public void happyPathTest() {
		LoadPaymentIds condition = new LoadPaymentIds();
		run(condition);
		JsonObject paymentIds = environment.getObject("payment_ids");
		JsonObject savedPaymentIds = environment.getObject("saved_payment_ids");
		assertEquals(paymentIds, savedPaymentIds);

		JsonArray paymentIdsArray = paymentIds.getAsJsonArray("paymentIds");
		if (!paymentIdsArray.isEmpty()) {
			paymentIdsArray.remove(0);
		}
		assertNotEquals(paymentIds, savedPaymentIds);
	}
}
