package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.GetConsentOASValidatorV3;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class AbstractGetConsentOASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullConsentResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/consent/v3/GetConsentResponseV3.json")
	public void testAdditionalConstraints() {
		// Set up the environment.
		GetConsentOASValidatorV3 cond = new GetConsentOASValidatorV3();
		run(cond);

		JsonObject rejectedConsentData = new JsonObject();
		rejectedConsentData.addProperty("status", "REJECTED");
		assertThrows(ConditionError.class, () -> cond.assertRejectionConstraint(rejectedConsentData));

		rejectedConsentData.add("rejection", new JsonObject());
		cond.assertRejectionConstraint(rejectedConsentData);
	}


	@Test
	@UseResurce("jsonResponses/consent/v3/400ErrorResponse.json")
	public void testBadRequestResponse() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullConsentResponseEnvKey.getKey());
		response.addProperty("status", 403);
		// Set up the environment.
		GetConsentOASValidatorV3 cond = new GetConsentOASValidatorV3();
		run(cond);
	}
}
