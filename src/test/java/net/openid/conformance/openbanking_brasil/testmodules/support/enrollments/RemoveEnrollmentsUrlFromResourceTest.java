package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RemoveEnrollmentsUrlFromResourceTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		environment.putString("config", "resource.enrollmentsUrl", "https://matls-api.mockbank.poc.raidiam.io/open-banking/enrollments/v2/enrollments");
		run(new RemoveEnrollmentsUrlFromResource());
		assertTrue(environment.getObject("config").getAsJsonObject("resource").get("enrollmentsUrl")==null);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingConfig() {
		run(new RemoveEnrollmentsUrlFromResource());
	}

	@Test
	public void unhappyPathTestMissingResource() {
		environment.putObject("config", new JsonObject());
		unhappyPathTest();
	}

	protected void unhappyPathTest () {
		ConditionError error = runAndFail(new RemoveEnrollmentsUrlFromResource());
		assertTrue(error.getMessage().contains("No resource in config file"));
	}
}
