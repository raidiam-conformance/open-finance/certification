package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;


public class AbstractEnsurePaymentConsentStatusWasTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationAuthorizedConsentResponse.json")
	public void expectedConsentCodeStatusTest() {
		setJwt(true);
		setStatus(HttpStatus.OK.value());
		AbstractEnsurePaymentConsentStatusWas condition = new EnsurePaymentConsentStatusWasAuthorised();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsurePaymentConsentStatusWas.RESPONSE_ENV_KEY);
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationAuthorizedConsentResponse.json")
	public void notExpectedConsentCodeStatusTest() {
		setJwt(true);
		setStatus(HttpStatus.OK.value());
		AbstractEnsurePaymentConsentStatusWas condition = new EnsurePaymentConsentStatusWasConsumed();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsurePaymentConsentStatusWas.RESPONSE_ENV_KEY);
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Consent not in the expected state"));
	}

}
