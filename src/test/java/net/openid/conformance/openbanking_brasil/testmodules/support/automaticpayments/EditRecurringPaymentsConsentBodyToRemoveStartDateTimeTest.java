package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToRemoveStartDateTime;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentsConsentBodyToRemoveStartDateTimeTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithSweepingField.json", key=KEY, path=PATH)
	public void happyPathTest() {
		EditRecurringPaymentsConsentBodyToRemoveStartDateTime condition = new EditRecurringPaymentsConsentBodyToRemoveStartDateTime();
		run(condition);

		JsonObject data = environment
			.getElementFromObject("resource", "brazilPaymentConsent.data")
			.getAsJsonObject();
		assertFalse(data.has("startDateTime"));
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putObject("resource", new JsonObject());
		EditRecurringPaymentsConsentBodyToRemoveStartDateTime condition = new EditRecurringPaymentsConsentBodyToRemoveStartDateTime();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data field in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPaymentConsent", new JsonObject());
		EditRecurringPaymentsConsentBodyToRemoveStartDateTime condition = new EditRecurringPaymentsConsentBodyToRemoveStartDateTime();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data field in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
