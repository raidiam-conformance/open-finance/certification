package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedFrom.EnsureRecurringPaymentsConsentsRevokedFromDetentora;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedFrom.EnsureRecurringPaymentsConsentsRevokedFromIniciadora;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureRecurringPaymentsConsentsRevokedFromTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonResponses/automaticpayments/PostRecurringConsents";
	private static final String MISSING_FIELD_MESSAGE = "Unable to find element data.revocation.revokedFrom in the response payload";

	@Test
	@UseResurce(BASE_FILE_PATH + "RevokedFromIniciadoraRevokedByUsuario.json")
	public void happyPathTestIniciadora() {
		EnsureRecurringPaymentsConsentsRevokedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRevokedFromIniciadora();
		run(condition);
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "RevokedFromDetentoraRevokedByDetentora.json")
	public void happyPathTestDetentora() {
		EnsureRecurringPaymentsConsentsRevokedFromDetentora condition = new EnsureRecurringPaymentsConsentsRevokedFromDetentora();
		run(condition);
	}

	@Test
	public void unhappyPathMissingResponse() {
		EnsureRecurringPaymentsConsentsRevokedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRevokedFromIniciadora();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	public void unhappyPathMissingData() {
		EnsureRecurringPaymentsConsentsRevokedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRevokedFromIniciadora();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRevocationField.json")
	public void unhappyPathMissingRevocation() {
		EnsureRecurringPaymentsConsentsRevokedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRevokedFromIniciadora();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRevocationRevokedFromField.json")
	public void unhappyPathMissingRevokedFrom() {
		EnsureRecurringPaymentsConsentsRevokedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRevokedFromIniciadora();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "RevokedFromDetentoraRevokedByDetentora.json")
	public void unhappyPathRejectedByNotMatchingExpected() {
		EnsureRecurringPaymentsConsentsRevokedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRevokedFromIniciadora();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "revokedFrom returned in the response does not match the expected revokedFrom";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
