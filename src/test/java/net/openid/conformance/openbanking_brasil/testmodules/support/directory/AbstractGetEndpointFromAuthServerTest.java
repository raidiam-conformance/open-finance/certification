package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.List;


public abstract class AbstractGetEndpointFromAuthServerTest extends AbstractJsonResponseConditionUnitTest {

	protected JsonObject createApiResource(String familyType, String version, List<String> edpointsToAdd) {
		JsonObject apiResource = new JsonObjectBuilder()
			.addField("ApiFamilyType", familyType)
			.addField("ApiVersion", version)
			.build();

		JsonArray endpoints =  new JsonArray();

		edpointsToAdd.forEach(endpoint -> endpoints.add(new JsonObjectBuilder().addField("ApiEndpoint", endpoint).build()));

		apiResource.add("ApiDiscoveryEndpoints", endpoints);
		return apiResource;
	}

	protected void addApiResourcesToServer(List<JsonObject> resources){
		JsonArray apiResources = new JsonArray();
		resources.forEach(apiResources::add);
		JsonObject server = new JsonObject();
		server.add("ApiResources", apiResources);
		environment.putObject("authorisation_server", server);
	}

	protected String getErrorMessage(String message){
		return String.format("%s: %s", getCondition().getClass().getSimpleName(), message);
	}

	protected abstract AbstractGetEndpointFromAuthServer getCondition();


}
