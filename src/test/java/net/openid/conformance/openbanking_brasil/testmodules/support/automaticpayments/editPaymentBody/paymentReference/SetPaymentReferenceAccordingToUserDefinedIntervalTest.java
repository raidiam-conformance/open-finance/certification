package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.paymentReference;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceAccordingToUserDefinedInterval;
import org.junit.Test;

public class SetPaymentReferenceAccordingToUserDefinedIntervalTest extends AbstractSetPaymentReferenceTest {

	@Override
	protected AbstractSetPaymentReference condition() {
		return new SetPaymentReferenceAccordingToUserDefinedInterval();
	}

	@Test
	public void happyPathTestSemanal() {
		addIntervalToTheEnvironment("SEMANAL");
		happyPathTest("2024-12-11", "W50-2024");
	}

	@Test
	public void happyPathTestMensal() {
		addIntervalToTheEnvironment("MENSAL");
		for (int i = 1; i <= 12; i++) {
			happyPathTest(String.format("2024-%02d-01", i), String.format("M%02d-2024", i));
		}
	}

	@Test
	public void happyPathTestTrimestral() {
		addIntervalToTheEnvironment("TRIMESTRAL");
		happyPathTest("2024-03-01", "Q1-2024");
		happyPathTest("2024-06-01", "Q2-2024");
		happyPathTest("2024-09-01", "Q3-2024");
		happyPathTest("2024-12-01", "Q4-2024");
	}

	@Test
	public void happyPathTestSemestral() {
		addIntervalToTheEnvironment("SEMESTRAL");
		happyPathTest("2024-06-01", "S1-2024");
		happyPathTest("2024-12-01", "S2-2024");
	}

	@Test
	public void happyPathTestAnual() {
		addIntervalToTheEnvironment("ANUAL");
		happyPathTest("2024-01-01", "Y2024");
	}

	@Test
	public void unhappyPathTestInvalidInterval() {
		insertPaymentWithDateInTheEnvironment("2024-01-01");

		addIntervalToTheEnvironment("BIMESTRAL");
		unhappyPathTest("The value defined for resource.interval is not valid");

		addIntervalToTheEnvironment("mensal");
		unhappyPathTest("The value defined for resource.interval is not valid");

		addIntervalToTheEnvironment("Mensal");
		unhappyPathTest("The value defined for resource.interval is not valid");
	}

	private void addIntervalToTheEnvironment(String interval) {
		environment.putString("resource", "interval", interval);
	}
}
