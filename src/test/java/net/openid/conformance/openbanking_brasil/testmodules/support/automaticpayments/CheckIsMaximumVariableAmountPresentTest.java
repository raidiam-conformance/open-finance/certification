package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CheckIsMaximumVariableAmountPresent;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CheckIsMaximumVariableAmountPresentTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/GetRecurringConsents";

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void happyPathTestPresent() {
		run(new CheckIsMaximumVariableAmountPresent());
		assertTrue(environment.getBoolean("max_var_amount_present"));
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingMaximumVariableAmount200Response.json")
	public void happyPathTestNotPresent() {
		run(new CheckIsMaximumVariableAmountPresent());
		assertFalse(environment.getBoolean("max_var_amount_present"));
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(CheckIsMaximumVariableAmountPresent.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(CheckIsMaximumVariableAmountPresent.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRecurringConfiguration200Response.json")
	public void unhappyPathTestMissingRecurringConfiguration() {
		unhappyPathTest("Could not extract automatic field from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "Sweeping200Response.json")
	public void unhappyPathTestNotAutomatic() {
		unhappyPathTest("Could not extract automatic field from response data");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new CheckIsMaximumVariableAmountPresent());
		assertTrue(error.getMessage().contains(message));
	}
}
