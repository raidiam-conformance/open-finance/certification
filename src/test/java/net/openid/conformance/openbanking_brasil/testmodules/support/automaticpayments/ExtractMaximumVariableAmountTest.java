package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractMaximumVariableAmount;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExtractMaximumVariableAmountTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/GetRecurringConsents";
	protected static final String EXPECTED_MAXIMUM_VARIABLE_AMOUNT = "1000000.12";

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void happyPathTest() {
		run(new ExtractMaximumVariableAmount());
		String maximumVariableAmount = environment.getString("maximumVariableAmount");
		assertEquals(EXPECTED_MAXIMUM_VARIABLE_AMOUNT, maximumVariableAmount);
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(ExtractMaximumVariableAmount.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(ExtractMaximumVariableAmount.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRecurringConfiguration200Response.json")
	public void unhappyPathTestMissingRecurringConfiguration() {
		unhappyPathTest("Could not extract maximumVariableAmount field from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "Sweeping200Response.json")
	public void unhappyPathTestNotAutomatic() {
		unhappyPathTest("Could not extract maximumVariableAmount field from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingMaximumVariableAmount200Response.json")
	public void unhappyPathTestMissingMaximumVariableAmount() {
		unhappyPathTest("Could not extract maximumVariableAmount field from response data");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new ExtractMaximumVariableAmount());
		assertTrue(error.getMessage().contains(message));
	}
}
