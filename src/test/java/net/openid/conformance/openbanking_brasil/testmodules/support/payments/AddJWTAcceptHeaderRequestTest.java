package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddJWTAcceptHeaderRequestTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathEmptyHeadersTest() {
		happyPathTest(1);
	}

	@Test
	public void happyPathNonEmptyHeadersTest() {
		JsonObject oldHeaders = new JsonObject();
		oldHeaders.addProperty("header 1", "example");
		environment.putObject("resource_endpoint_request_headers", oldHeaders);
		happyPathTest(2);
	}

	@Test
	public void happyPathReplaceAcceptHeaderTest() {
		JsonObject oldHeaders = new JsonObject();
		oldHeaders.addProperty("Accept", "application/json");
		oldHeaders.addProperty("header 1", "example 1");
		oldHeaders.addProperty("header 2", "example 2");
		environment.putObject("resource_endpoint_request_headers", oldHeaders);
		happyPathTest(3);
	}

	protected void happyPathTest(int expectedAmountHeaders) {
		AddJWTAcceptHeaderRequest condition = new AddJWTAcceptHeaderRequest();
		run(condition);
		JsonObject headers = environment.getObject("resource_endpoint_request_headers");
		assertEquals("application/jwt", OIDFJSON.getString(headers.get("Accept")));
		assertEquals(expectedAmountHeaders, headers.size());
	}
}
