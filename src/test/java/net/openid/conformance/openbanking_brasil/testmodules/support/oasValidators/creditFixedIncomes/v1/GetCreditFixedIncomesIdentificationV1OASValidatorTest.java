package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetCreditFixedIncomesIdentificationV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/identification.json")
	public void happyPath() {
		run(new GetCreditFixedIncomesIdentificationV1OASValidator());
	}


	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/identificationNoIndexerAdditionalInfo.json")
	public void unhappyPathNoIndexerAdditionalInfo() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesIdentificationV1OASValidator());
		assertThat(e.getMessage(), containsString("indexerAdditionalInfo is required when indexer is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/identificationNoIsinCode.json")
	public void unhappyPathNoIsinCode() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesIdentificationV1OASValidator());
		assertThat(e.getMessage(), containsString("isinCode' is required when field 'clearingCode' is not present"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/identificationNoPostFixedIndexerPercentage.json")
	public void unhappyPathNoPostFixedIndexerPercentage() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesIdentificationV1OASValidator());
		assertThat(e.getMessage(), containsString("postFixedIndexerPercentage is required when indexer is PRE_FIXADO"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/identificationNoPreFixedRate.json")
	public void unhappyPathNoPreFixedRate() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesIdentificationV1OASValidator());
		assertThat(e.getMessage(), containsString("preFixedRate is required when indexer is PRE_FIXADO"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/identificationVoucherPaymentPeriodicity.json")
	public void happyPathVoucherPaymentIndicatorNAO() {
		run(new GetCreditFixedIncomesIdentificationV1OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/identificationNoVaucherPaymentPeriodicity.json")
	public void unhappyPathNoVaucherPaymentPeriodicity() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesIdentificationV1OASValidator());
		assertThat(e.getMessage(), containsString("voucherPaymentPeriodicity is required when voucherPaymentIndicator is SIM"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/identificationNoVaucherPaymentPeriodicityAdditionalInfo.json")
	public void unhappyPathNoVaucherPaymentPeriodicityAdditionalInfo() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesIdentificationV1OASValidator());
		assertThat(e.getMessage(), containsString("voucherPaymentPeriodicityAdditionalInfo is required when voucherPaymentPeriodicity is OUTROS"));
	}
}
