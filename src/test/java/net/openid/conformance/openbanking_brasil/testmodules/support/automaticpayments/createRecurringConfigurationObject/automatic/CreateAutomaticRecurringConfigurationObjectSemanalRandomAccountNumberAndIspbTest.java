package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createRecurringConfigurationObject.automatic;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.AbstractCreateAutomaticRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalRandomAccountNumberAndIspb;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;
import net.openid.conformance.testmodule.OIDFJSON;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class CreateAutomaticRecurringConfigurationObjectSemanalRandomAccountNumberAndIspbTest extends AbstractCreateAutomaticRecurringConfigurationObjectTest {

	@Override
	protected AbstractCreateAutomaticRecurringConfigurationObject condition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalRandomAccountNumberAndIspb();
	}

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getExpectedInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.SEMANAL;
	}

	@Override
	protected void assertFirstPayment(JsonObject automatic) {
		if (hasFirstPayment()) {
			JsonObject firstPayment = automatic.getAsJsonObject("firstPayment");
			assertStringField(firstPayment, "type", "PIX");
			assertStringField(firstPayment, "date");
			assertStringField(firstPayment, "currency", "BRL");
			assertStringField(firstPayment, "amount");

			JsonObject creditorAccount = assertObjectField(firstPayment, "creditorAccount");
			assertStringField(creditorAccount, "ispb");
			assertStringField(creditorAccount, "issuer", CREDITOR_ACCOUNT_ISSUER);
			assertStringField(creditorAccount, "number");
			assertStringField(creditorAccount, "accountType", CREDITOR_ACCOUNT_ACCOUNT_TYPE);

			String number = OIDFJSON.getString(creditorAccount.get("number"));
			assertNotEquals(CREDITOR_ACCOUNT_NUMBER, number);
			assertTrue(!number.isEmpty() && number.length() <= 20);

			String ispb = OIDFJSON.getString(creditorAccount.get("ispb"));
			assertNotEquals(CREDITOR_ACCOUNT_ISPB, ispb);
			assertEquals(8, ispb.length());
		}
	}
}
