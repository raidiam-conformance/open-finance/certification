package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.setCreditors;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.AbstractEditRecurringPaymentsConsentBodyToSetCreditor;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class AbstractEditRecurringPaymentsConsentBodyToSetCreditorTest extends AbstractJsonResponseConditionUnitTest {

	protected abstract AbstractEditRecurringPaymentsConsentBodyToSetCreditor condition();
	protected abstract int expectedAmountOfCreditors();
	protected abstract String expectedPersonType();

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/consents/brazilPaymentConsentWithSweepingField.json", key="resource", path="brazilPaymentConsent")
	public void happyPathTest() {
		run(condition());

		JsonArray creditors = environment.getElementFromObject("resource", "brazilPaymentConsent")
			.getAsJsonObject()
			.getAsJsonObject("data")
			.getAsJsonArray("creditors");

		assertEquals(expectedAmountOfCreditors(), creditors.size());
		for (JsonElement creditorElement : creditors) {
			JsonObject creditor = creditorElement.getAsJsonObject();

			String personType = OIDFJSON.getString(creditor.get("personType"));
			String cpfCnpj = OIDFJSON.getString(creditor.get("cpfCnpj"));

			int expectedCpfCnpjLength = personType.equals("PESSOA_JURIDICA") ? 14 : 11;
			assertEquals(expectedCpfCnpjLength, cpfCnpj.length());
			assertEquals(expectedPersonType(), personType);
			assertTrue(creditor.has("name"));
		}

		additionalValidation(creditors);
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		environment.putObject("resource", new JsonObject());
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPaymentConsent", new JsonObject());
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains("Unable to find data in consents payload"));
	}

	protected void additionalValidation(JsonArray creditors) {}
}
