package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevocationReasonEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedByEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedFromEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsStatusEnum;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody condition = new CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody();
		run(condition);

		JsonObject consentEndpointRequest = environment.getObject("consent_endpoint_request");

		JsonObject data = assertObjectFieldIsPresent(consentEndpointRequest, "data");

		String status = assertStringFieldIsPresent(data, "status");
		assertTrue(RecurringPaymentsConsentsStatusEnum.toSet().contains(status));

		JsonObject revocation = assertObjectFieldIsPresent(data, "revocation");

		String revokedBy = assertStringFieldIsPresent(revocation, "revokedBy");
		assertTrue(RecurringPaymentsConsentsRevokedByEnum.toSet().contains(revokedBy));

		String revokedFrom = assertStringFieldIsPresent(revocation, "revokedFrom");
		assertTrue(RecurringPaymentsConsentsRevokedFromEnum.toSet().contains(revokedFrom));

		JsonObject reason = assertObjectFieldIsPresent(revocation, "reason");

		String code = assertStringFieldIsPresent(reason, "code");
		assertTrue(RecurringPaymentsConsentsRevocationReasonEnum.toSet().contains(code));

		assertStringFieldIsPresent(reason, "detail");
	}

	private String assertStringFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonPrimitive() && obj.get(field).getAsJsonPrimitive().isString());
		return OIDFJSON.getString(obj.get(field));
	}

	private JsonObject assertObjectFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonObject());
		return obj.getAsJsonObject(field);
	}
}
