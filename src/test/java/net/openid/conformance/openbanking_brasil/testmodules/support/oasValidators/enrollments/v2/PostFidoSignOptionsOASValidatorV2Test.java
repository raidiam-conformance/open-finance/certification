package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostFidoSignOptionsOASValidatorV2Test extends AbstractEnrollmentsOASV2ValidatorsTest {

	@Override
	protected AbstractEnrollmentsOASValidatorV2 validator() {
		return new PostFidoSignOptionsOASValidatorV2();
	}

	@Test
	@UseResurce(JSON_PATH + "PostFidoSignOptions201Response.json")
	public void happyPathTest201() {
		happyPathTest(201);
	}

	@Test
	@UseResurce(JSON_PATH + "PostFidoSignOptions422Response.json")
	public void happyPathTest422() {
		happyPathTest(422);
	}
}
