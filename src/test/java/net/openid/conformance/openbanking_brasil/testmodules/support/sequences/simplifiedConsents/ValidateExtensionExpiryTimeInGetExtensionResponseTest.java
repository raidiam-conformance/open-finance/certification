package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidateExtensionExpiryTimeInGetExtensionResponseTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_RESOURCE_PATH = "jsonResponses/consent/v3/GetConsentExtensionsResponse";
	private static final List<String> ONE_EXTENSION_LIST = List.of("2021-05-21T08:30:00Z");
	private static final List<String> THREE_EXTENSIONS_LIST = List.of("2021-05-21T08:30:00Z", "2021-04-21T08:30:00Z", "");
	private static final List<String> WRONG_EXTENSION_LIST = List.of("2021-03-21T08:30:00Z");

	@Test
	@UseResurce(BASE_RESOURCE_PATH + "V3.json")
	public void happyPathTestExpectingOne() {
		happyPathTest(new ValidateExtensionExpiryTimeInGetSizeOne(), ONE_EXTENSION_LIST);
	}

	@Test
	@UseResurce(BASE_RESOURCE_PATH + "WithThreeObjectsV3.json")
	public void happyPathTestExpectingThree() {
		happyPathTest(new ValidateExtensionExpiryTimeInGetSizeThree(), THREE_EXTENSIONS_LIST);
	}

	@Test
	@UseResurce(BASE_RESOURCE_PATH + "V3.json")
	public void unhappyPathTestMissingExtensionTimesArray() {
		unhappyPathTest(new ValidateExtensionExpiryTimeInGetSizeOne(), "Expected extension_times array to be present, this is a bug in the test module", List.of());
	}

	@Test
	@UseResurce(BASE_RESOURCE_PATH + "WithThreeObjectsV3.json")
	public void unhappyPathTestDifferentArraySizes() {
		unhappyPathTest(new ValidateExtensionExpiryTimeInGetSizeOne(), "Expected extension response array size to be: ", ONE_EXTENSION_LIST);
	}

	@Test
	@UseResurce(BASE_RESOURCE_PATH + "V3.json")
	public void unhappyPathTestExpirationTimeNotPresentInArray() {
		unhappyPathTest(new ValidateExtensionExpiryTimeInGetSizeOne(), "Expected consent to match one of the saved extension time", WRONG_EXTENSION_LIST);
	}

	@Test
	@UseResurce(BASE_RESOURCE_PATH + "WithThreeObjectsOutOfOrderV3.json")
	public void unhappyPathTestNotInDescendingOrder() {
		unhappyPathTest(new ValidateExtensionExpiryTimeInGetSizeThree(), "Expected extension response array to be in descending order", THREE_EXTENSIONS_LIST);
	}

	public void setupTest(List<String> extensionTimesList) {
		JsonObject extensionExpiryTimes = new JsonObject();
		if (!extensionTimesList.isEmpty()) {
			JsonArray extensionTimes = new JsonArray();
			for (String extensionTime : extensionTimesList) {
				extensionTimes.add(extensionTime);
			}
			extensionExpiryTimes.add("extension_times", extensionTimes);
		}
		environment.putObject("extension_expiry_times", extensionExpiryTimes);
	}

	public void happyPathTest(ValidateExtensionExpiryTimeInGetExtensionResponse condition, List<String> extensionTimesList) {
		setupTest(extensionTimesList);
		run(condition);
	}

	public void unhappyPathTest(ValidateExtensionExpiryTimeInGetExtensionResponse condition, String errorMessage, List<String> extensionTimesList) {
		setupTest(extensionTimesList);
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(errorMessage));
	}
}
