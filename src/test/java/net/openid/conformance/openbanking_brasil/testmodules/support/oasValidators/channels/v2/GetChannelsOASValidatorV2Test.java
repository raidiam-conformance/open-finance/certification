package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.channels.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetChannelsOASValidatorV2Test  extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/channels/v2/GetBankingAgentsChannelsResponseOK.json")
	public void testHappyPathBankingAgents() {
		GetChannelsBankingAgentsOASValidatorV2 cond = new GetChannelsBankingAgentsOASValidatorV2();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/channels/v2/GetBranchesChannelsResponseOK.json")
	public void testHappyPathBranches() {
		GetChannelsBranchesOASValidatorV2 cond = new GetChannelsBranchesOASValidatorV2();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/channels/v2/GetElectronicChannelsResponseOK.json")
	public void testHappyPathElectronicChannels() {
		GetChannelsElectronicChannelsOASValidatorV2 cond = new GetChannelsElectronicChannelsOASValidatorV2();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/channels/v2/GetElectronicChannelsResponseAdditionalMissingOUTROS.json")
	public void testUnhappyPathElectronicChannelsAdditionalMissingOUTRO() {
		GetChannelsElectronicChannelsOASValidatorV2 cond = new GetChannelsElectronicChannelsOASValidatorV2();
		ConditionError e = runAndFail(cond);
		assertThat(e.getMessage(), containsString("additionalInfo is required when type is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/channels/v2/GetPhoneChannelsResponseOK.json")
	public void testHappyPathPhoneChannels() {
		GetChannelsPhoneChannelsOASValidatorV2 cond = new GetChannelsPhoneChannelsOASValidatorV2();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/channels/v2/GetTellerMachinesChannelsResponseOK.json")
	public void testHappyPathSharedAutomatedTellerMachines() {
		GetChannelsSharedAutomatedTellerMachinesOASValidatorV2 cond = new GetChannelsSharedAutomatedTellerMachinesOASValidatorV2();
		run(cond);
	}

}
