package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.AbstractEnrollmentsOASValidatorV2;

import static org.junit.Assert.assertTrue;

public abstract class AbstractEnrollmentsOASV2n1ValidatorsTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String JSON_PATH = "jsonResponses/enrollments/v2n1/";

	protected abstract AbstractEnrollmentsOASValidatorV2n1 validator();

	protected void happyPathTest(int status) {
		setStatus(status);
		run(validator());
	}

	protected void unhappyPathTest(int status, String message) {
		setStatus(status);
		ConditionError error = runAndFail(validator());
		assertTrue(error.getMessage().contains(message));
	}
}
