package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class EnsureExpirationWasNotReturnedTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/account/readConsentResponse_missing_expirationDateTime.json")
	public void testHappyPath() {
		EnsureExpirationWasNotReturned condition = new EnsureExpirationWasNotReturned();
		run(condition);
	}

	@Test
	@UseResurce("massadedados/F2_Consentimento/post-consents-201-2.json")
	public void testUnhappyExpirationReturned() {
		EnsureExpirationWasNotReturned condition = new EnsureExpirationWasNotReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("expirationDateTime field should not be present in the response"));
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseWithoutData.json")
	public void testUnhappyPathNoData() {
		EnsureExpirationWasNotReturned condition = new EnsureExpirationWasNotReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Data element is missing in the body"));
	}

	@Test
	public void testUnhappyPathNoBody() {
		EnsureExpirationWasNotReturned condition = new EnsureExpirationWasNotReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}
}
