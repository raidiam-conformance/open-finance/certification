package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetFinancingsV2EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/financings/v2/contracts";
	}

	@Override
	protected String getApiFamilyType() {
		return "financings";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetFinancingsV2Endpoint();
	}
}
