package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.webauthn4j.converter.util.CborConverter;
import com.webauthn4j.converter.util.ObjectConverter;
import com.webauthn4j.data.attestation.AttestationObject;
import com.webauthn4j.data.attestation.authenticator.AAGUID;
import com.webauthn4j.data.attestation.authenticator.AttestedCredentialData;
import com.webauthn4j.data.attestation.authenticator.AuthenticatorData;
import com.webauthn4j.data.attestation.authenticator.COSEKey;
import com.webauthn4j.data.attestation.authenticator.EC2COSEKey;
import com.webauthn4j.data.attestation.authenticator.RSACOSEKey;
import com.webauthn4j.data.attestation.statement.AttestationStatement;
import com.webauthn4j.data.attestation.statement.COSEAlgorithmIdentifier;
import com.webauthn4j.data.attestation.statement.NoneAttestationStatement;
import com.webauthn4j.data.extension.authenticator.RegistrationExtensionAuthenticatorOutput;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonUtils;
import net.openid.conformance.util.UseResurce;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.htmlunit.csp.value.Hash;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.text.ParseException;
import java.util.Base64;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GenerateFidoAttestationObjectTest extends AbstractJsonResponseConditionUnitTest {

	public static final String RP_ID = "rp_id";
	public static final String EC_KID = "ec_kid";
	public static final String RSA_KID = "rsa_kid";

	@Before
	public void init() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
		setJwt(true);
		Security.addProvider(new BouncyCastleProvider());

		environment.putString("rp_id", RP_ID);
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC", "BC");
		keyPairGenerator.initialize(new ECGenParameterSpec("prime256v1"));
		KeyPair ecKeyPair = keyPairGenerator.generateKeyPair();
		JWK ecJwk = new ECKey.Builder(Curve.P_256, (ECPublicKey) ecKeyPair.getPublic()).privateKey(ecKeyPair.getPrivate()).keyID(EC_KID).build();


		keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);
		KeyPair rsaKeyPair = keyPairGenerator.generateKeyPair();
		JWK rsaKwk = new RSAKey.Builder((RSAPublicKey) rsaKeyPair.getPublic()).privateKey(rsaKeyPair.getPrivate()).keyID(RSA_KID).build();

		environment.putObject("rsa_fido_keys_jwk", JsonUtils.createBigDecimalAwareGson().fromJson(rsaKwk.toJSONString(), JsonObject.class));
		environment.putObject("ec_fido_keys_jwk", JsonUtils.createBigDecimalAwareGson().fromJson(ecJwk.toJSONString(), JsonObject.class));

	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void evaluateAttestationObjectCreationWithRsaKey() throws NoSuchAlgorithmException, ParseException, JOSEException {
		environment.mapKey("fido_keys_jwk", "rsa_fido_keys_jwk");
		run(new GenerateFidoAttestationObject());
		String encodedAttestationObject = environment.getString("encoded_attestation_object");
		assertThat(encodedAttestationObject, notNullValue());
		byte[] attestationObjectData = Base64.getUrlDecoder().decode(encodedAttestationObject);
		CborConverter converter = new ObjectConverter().getCborConverter();
		AttestationObject attestationObject = converter.readValue(attestationObjectData, AttestationObject.class);

		KeyPair keyPair = RSAKey.parse(environment.getObject("rsa_fido_keys_jwk").toString()).toKeyPair();
		RSACOSEKey rsaCOSEKey = RSACOSEKey.create((RSAPublicKey) keyPair.getPublic(), COSEAlgorithmIdentifier.RS256);

		validateAttestationObject(attestationObject, RSA_KID,  rsaCOSEKey);

	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void evaluateAttestationObjectCreationWithEcKey() throws NoSuchAlgorithmException, ParseException, JOSEException {
		environment.mapKey("fido_keys_jwk", "ec_fido_keys_jwk");
		run(new GenerateFidoAttestationObject());
		String encodedAttestationObject = environment.getString("encoded_attestation_object");
		assertThat(encodedAttestationObject, notNullValue());
		byte[] attestationObjectData = Base64.getUrlDecoder().decode(encodedAttestationObject);
		CborConverter converter = new ObjectConverter().getCborConverter();
		AttestationObject attestationObject = converter.readValue(attestationObjectData, AttestationObject.class);

		KeyPair keyPair = ECKey.parse(environment.getObject("ec_fido_keys_jwk").toString()).toKeyPair();
		EC2COSEKey ec2COSEKey = EC2COSEKey.create((ECPublicKey) keyPair.getPublic(), COSEAlgorithmIdentifier.ES256);

		validateAttestationObject(attestationObject, EC_KID,  ec2COSEKey);

	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingRpId() {
		environment.removeNativeValue("rp_id");
		run(new GenerateFidoAttestationObject());
	}

	@Test
	public void unhappyPathTestMissingKid() {
		JsonObject fidoKeysJwk = environment.getObject("ec_fido_keys_jwk").deepCopy();
		fidoKeysJwk.remove("kid");
		environment.putObject("fido_keys_jwk", fidoKeysJwk);
		ConditionError error = runAndFail(new GenerateFidoAttestationObject());
		assertTrue(error.getMessage().contains("Could not find kid in the fido_keys_jwk.kid"));
	}


	private void validateAttestationObject(AttestationObject attestationObject, String expectedCredentialId, COSEKey expectedCOSEKey) throws NoSuchAlgorithmException {
		assertThat(attestationObject, notNullValue());


		AttestationStatement attestationStatement = attestationObject.getAttestationStatement();
		AuthenticatorData<RegistrationExtensionAuthenticatorOutput> authenticatorData = attestationObject.getAuthenticatorData();
		byte[] rpIdHash = authenticatorData.getRpIdHash();
		byte flags = authenticatorData.getFlags();
		long signCount = authenticatorData.getSignCount();
		AttestedCredentialData attestedCredentialData = authenticatorData.getAttestedCredentialData();

		assertThat(attestedCredentialData, notNullValue());

		byte[] credentialId = attestedCredentialData.getCredentialId();
		AAGUID aaguid = attestedCredentialData.getAaguid();
		COSEKey coseKey = attestedCredentialData.getCOSEKey();


		assertThat(attestationStatement.getClass(), equalTo(NoneAttestationStatement.class));
		assertThat(flags, equalTo(Byte.parseByte("01000101", 2)));
		assertThat(signCount, equalTo(0L));
		assertThat(rpIdHash, equalTo(MessageDigest.getInstance(Hash.Algorithm.SHA256.name()).digest(RP_ID.getBytes(StandardCharsets.UTF_8))));
		assertThat(credentialId, equalTo(expectedCredentialId.getBytes(StandardCharsets.UTF_8)));
		assertThat(aaguid, equalTo(AAGUID.ZERO));
		assertThat(coseKey, equalTo(expectedCOSEKey));



	}

}
