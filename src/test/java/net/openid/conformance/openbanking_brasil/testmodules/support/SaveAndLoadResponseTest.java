package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class SaveAndLoadResponseTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		JsonObject response = new JsonObject();
		response.addProperty("foo", "bar");
		environment.putObject(SaveResponse.RESPONSE_ENV_KEY, response);
	}

	@Test
	public void happyPathTest() {
		// save the response and check if it worked
		run(new SaveResponse());
		JsonObject savedResponse = environment.getObject("saved_response");
		JsonObject originalResponse = environment.getObject(SaveResponse.RESPONSE_ENV_KEY);
		assertEquals(originalResponse, savedResponse);

		// update the original response and check that the saved response does not change
		originalResponse.addProperty("foo", "bar1");
		assertNotEquals(originalResponse, savedResponse);

		// load the old response and check if it overwrites the updated one
		run(new LoadOldResponse());
		JsonObject overwrittenResponse = environment.getObject(SaveResponse.RESPONSE_ENV_KEY);
		assertEquals(savedResponse, overwrittenResponse);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestLoadBeforeSave() {
		run(new LoadOldResponse());
	}
}
