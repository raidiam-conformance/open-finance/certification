package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class PostConsentExtendsOASValidatorV3N2Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullConsentResponseEnvKey.getKey());
		response.addProperty("status", 201);
	}

	@Test
	@UseResurce("jsonResponses/consent/v3/PostConsentExtendsResponseV3.json")
	public void testHappyPath() {
		PostConsentExtendsOASValidatorV3n2 cond = new PostConsentExtendsOASValidatorV3n2();
		run(cond);
	}
}
