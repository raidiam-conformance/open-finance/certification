package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.setConfigurationFieldsForRetryTest;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.AbstractSetConfigurationFieldsForRetryTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public abstract class AbstractSetConfigurationFieldsForRetryTestTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String CONSENT_ID = "consent_id";
	protected static final String REFRESH_TOKEN = "refresh_token";

	protected abstract AbstractSetConfigurationFieldsForRetryTest condition();
	protected abstract String expectedPrefix();

	@Test
	public void happyPathTest() {
		String consentIdEnvPath = expectedPrefix() + "ConsentId";
		environment.putString("resource", consentIdEnvPath, CONSENT_ID);

		String refreshTokenEnvPath = expectedPrefix() + "RefreshToken";
		environment.putString("resource", refreshTokenEnvPath, REFRESH_TOKEN);

		run(condition());

		String consentId = environment.getString("consent_id");
		assertEquals(CONSENT_ID, consentId);

		String refreshToken = environment.getString("refresh_token");
		assertEquals(REFRESH_TOKEN, refreshToken);
	}
}
