package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateCompleteRiskSignalsRequestBodyToRequestEntityClaims;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

public class EditConsentsAuthoriseRequestBodyToAddCompleteRiskSignalsTest extends AbstractJsonResponseConditionUnitTest {

	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";

	@Test
	@UseResurce(value = "jsonRequests/payments/enrollments/postConsentsAuthorise.json", key = REQUEST_CLAIMS_KEY)
	public void happyPathTest() {
		run(new EditConsentsAuthoriseRequestBodyToAddCompleteRiskSignals());
		JsonObject riskSignals = environment.getObject(REQUEST_CLAIMS_KEY)
			.getAsJsonObject("data").getAsJsonObject("riskSignals");
		assertEquals(CreateCompleteRiskSignalsRequestBodyToRequestEntityClaims.getRiskSignalsObject().getAsJsonObject("data"), riskSignals);
	}

	@Test
	public void unhappyPathTestMissingRequest() {
		assertThrowsExactly(AssertionError.class, () -> {
			run(new EditConsentsAuthoriseRequestBodyToAddCompleteRiskSignals());
		});
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(REQUEST_CLAIMS_KEY, new JsonObject());
		ConditionError error = runAndFail(new EditConsentsAuthoriseRequestBodyToAddCompleteRiskSignals());
		assertTrue(error.getMessage().contains("Could not find data inside consents authorise request body"));
	}

	protected void unhappyPathTest() {

	}
}
