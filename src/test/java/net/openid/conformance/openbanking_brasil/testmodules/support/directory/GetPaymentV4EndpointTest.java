package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetPaymentV4EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/payments/v4/pix/payments";
	}

	@Override
	protected String getApiFamilyType() {
		return "payments-pix";
	}

	@Override
	protected String getApiVersion() {
		return "4.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetPaymentV4Endpoint();
	}
}
