package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GenerateNewE2EIDBasedOnPaymentDateTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBody";
	protected static final String KEY = "resource";
	protected static final String PATH = "brazilPixPayment";

	protected static final String DATE_TIME = "202408121500";
	protected static final String ISPB = "12345678";

	@Before
	public void init() {
		environment.putString("ispb", ISPB);
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "V2.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(new GenerateNewE2EIDBasedOnPaymentDate());

		String expectedEndToEndIdFormat = String.format("^E%s%s[a-zA-Z0-9]{11}$", ISPB, DATE_TIME);
		String endToEndIdEnv = environment.getString("endToEndId");
		assertTrue(endToEndIdEnv.matches(expectedEndToEndIdFormat));

		JsonObject data = environment.getElementFromObject(KEY, PATH + ".data").getAsJsonObject();
		String endToEndId = OIDFJSON.getString(data.get("endToEndId"));
		assertEquals(endToEndIdEnv, endToEndId);
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "MissingDateV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingDate() {
		unhappyPathTest("Unable to find date field in payments payload");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "InvalidDateV2.json", key = KEY, path = PATH)
	public void unhappyPathTestInvalidDate() {
		unhappyPathTest("The date field from payments payload is not in a valid format");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new GenerateNewE2EIDBasedOnPaymentDate());
		assertTrue(error.getMessage().contains(message));
	}
}
