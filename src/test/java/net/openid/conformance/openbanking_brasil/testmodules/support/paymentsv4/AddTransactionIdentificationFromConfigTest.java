package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AddTransactionIdentificationFromConfig;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import java.io.IOException;
import java.nio.charset.Charset;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class AddTransactionIdentificationFromConfigTest extends AbstractJsonResponseConditionUnitTest {

	public static String setTransactionIdentificaton = "12345678";
	@Test
	public void happyPathObjectTest() throws IOException {

		prepareTransactionIdenticatonWith("test_config.json");
		AddTransactionIdentificationFromConfig condition = new AddTransactionIdentificationFromConfig();
		run(condition);

		String currentTransactionIdentification = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.transactionIdentification"));

		Assert.assertEquals(setTransactionIdentificaton,currentTransactionIdentification);
	}

	@Test
	public void happyPathArrayTest() throws IOException {

		prepareTransactionIdenticatonWith("test_config_payments_v4.json");
		AddTransactionIdentificationFromConfig condition = new AddTransactionIdentificationFromConfig();
		run(condition);

		JsonElement data = environment.getElementFromObject("resource", "brazilPixPayment.data");
		String currentTransactionIdentification = OIDFJSON.getString(data.getAsJsonArray().get(0).getAsJsonObject().get("transactionIdentification"));

		Assert.assertEquals(setTransactionIdentificaton,currentTransactionIdentification);

	}

	@Test
	public void noDataObjectTest() throws IOException {

		prepareTransactionIdenticatonWith("test_config_no_data.json");
		AddTransactionIdentificationFromConfig condition = new AddTransactionIdentificationFromConfig();
		ConditionError error = runAndFail(condition);

		String expected = "Could not find data inside brazilPixPayment";
		assertThat(error.getMessage(), containsString(expected));
	}


	public void prepareTransactionIdenticatonWith(String fileName) throws IOException {
		String rawJson = IOUtils.resourceToString(fileName, Charset.defaultCharset(), getClass().getClassLoader());
		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		JsonObject testConfig = new JsonObjectBuilder()
			.addField("resource.transactionIdentifier", setTransactionIdentificaton)
			.build();

		environment.putObject("resource", resourceConfig);
		environment.putObject("config", testConfig);

	}

}
