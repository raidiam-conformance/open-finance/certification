package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsWebhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AddWebhookUrisToClientConfigurationRequest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddWebhookUrisToClientConfigurationRequestTest extends AbstractJsonResponseConditionUnitTest {


	@Test
	public void happyPath() {
		environment.putObject("registration_client_endpoint_request_body", new JsonObject());
		environment.putString("software_api_webhook_uri", "https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/alias");
		AddWebhookUrisToClientConfigurationRequest condition = new AddWebhookUrisToClientConfigurationRequest();
		run(condition);

		JsonArray webhookUris = environment.getObject("registration_client_endpoint_request_body").getAsJsonArray("webhook_uris");
		assertEquals(1, webhookUris.size());
	}

	@Test
	public void missingRegistrationClientEndpointRequestBody() {
		environment.removeObject("registration_client_endpoint_request_body");
		environment.putString("software_api_webhook_uri", "https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/alias");
		AddWebhookUrisToClientConfigurationRequest condition = new AddWebhookUrisToClientConfigurationRequest();
		ConditionError error = runAndFail(condition);
		assertEquals("AddWebhookUrisToClientConfigurationRequest: [pre] Something unexpected happened (this could be caused by something you did wrong, or it may be an issue in the test suite - please review the instructions and your configuration, if you still see a problem please contact certification@oidf.org with the full details) - couldn't find object in environment: registration_client_endpoint_request_body", error.getMessage());
	}

	@Test
	public void missingSoftwareApiWebhookUri() {
		environment.putObject("registration_client_endpoint_request_body", new JsonObject());
		AddWebhookUrisToClientConfigurationRequest condition = new AddWebhookUrisToClientConfigurationRequest();
		ConditionError error = runAndFail(condition);
		assertEquals("AddWebhookUrisToClientConfigurationRequest: Unable to find webhook_uri to add to the client configuration request", error.getMessage());
		}
	}


