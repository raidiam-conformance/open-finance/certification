package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureThereArePayments;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreOneAcscAndOneRjctPayments;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureThereAreOneAcscAndOneRjctPaymentsTest extends AbstractEnsureThereArePaymentsTest {

	@Override
	protected AbstractEnsureThereArePayments condition() {
		return new EnsureThereAreOneAcscAndOneRjctPayments();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "OneAcscOneRjct.json")
	public void happyPathTest() {
		super.happyPathTest();
	}
}
