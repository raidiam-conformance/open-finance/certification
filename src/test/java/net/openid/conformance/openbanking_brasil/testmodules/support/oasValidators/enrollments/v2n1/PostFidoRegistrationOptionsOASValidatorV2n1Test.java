package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.AbstractEnrollmentsOASV2ValidatorsTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.AbstractEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostFidoRegistrationOptionsOASValidatorV2;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostFidoRegistrationOptionsOASValidatorV2n1Test extends AbstractEnrollmentsOASV2n1ValidatorsTest {

	@Override
	protected AbstractEnrollmentsOASValidatorV2n1 validator() {
		return new PostFidoRegistrationOptionsOASValidatorV2n1();
	}

	@Test
	@UseResurce(JSON_PATH + "PostFidoRegistrationOptions201Response.json")
	public void happyPathTest201() {
		happyPathTest(201);
	}

	@Test
	@UseResurce(JSON_PATH + "PostFidoRegistrationOptions422Response.json")
	public void happyPathTest422() {
		happyPathTest(422);
	}
}
