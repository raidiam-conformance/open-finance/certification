package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureRejectionReasonsFromRecurringPaymentsListResponseDemandNewE2EID;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureRejectionReasonsFromRecurringPaymentsListResponseDemandNewE2EIDTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/payments/GetRecurringPaymentsList200Response";

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "RejectionReasonNaoInformado.json")
	public void happyPathTest() {
		run(new EnsureRejectionReasonsFromRecurringPaymentsListResponseDemandNewE2EID());
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(EnsureRejectionReasonsFromRecurringPaymentsListResponseDemandNewE2EID.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(EnsureRejectionReasonsFromRecurringPaymentsListResponseDemandNewE2EID.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRejectionReason.json")
	public void unhappyPathTestMissingRejectionReason() {
		unhappyPathTest("Could not find rejectionReason code inside of payment");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRejectionReasonCode.json")
	public void unhappyPathTestMissingRejectionReasonCode() {
		unhappyPathTest("Could not find rejectionReason code inside of payment");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "NonExistingRejectionReasonCode.json")
	public void unhappyPathTestNonExistingRejectionReasonCode() {
		unhappyPathTest("Invalid rejectionReason code");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "RejectionReasonValorAcimaLimite.json")
	public void unhappyPathTestRejectionReasonCodeThatDoesNotDemandE2EID() {
		unhappyPathTest("The rejectionReason code inside of payment does not demand a new E2EID");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureRejectionReasonsFromRecurringPaymentsListResponseDemandNewE2EID());
		assertTrue(error.getMessage().contains(message));
	}
}
