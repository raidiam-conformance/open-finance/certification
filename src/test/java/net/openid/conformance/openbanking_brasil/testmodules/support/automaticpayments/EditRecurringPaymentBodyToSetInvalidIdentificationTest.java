package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetInvalidIdentification;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentBodyToSetInvalidIdentificationTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/payments/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";
	private static final String OLD_CPF = "11111111111";
	private static final String OLD_CNPJ = "11111111111111";

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentWithCpf.json", key=KEY, path=PATH)
	public void happyPathTestCPF() {
		EditRecurringPaymentBodyToSetInvalidIdentification condition = new EditRecurringPaymentBodyToSetInvalidIdentification();
		run(condition);

		String newIdentification = OIDFJSON.getString(
			environment.getElementFromObject(KEY, PATH+".data.document")
				.getAsJsonObject()
				.get("identification")
		);
		assertNotEquals(OLD_CPF, newIdentification);
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentWithCnpj.json", key=KEY, path=PATH)
	public void happyPathTestCNPJ() {
		EditRecurringPaymentBodyToSetInvalidIdentification condition = new EditRecurringPaymentBodyToSetInvalidIdentification();
		run(condition);

		String newIdentification = OIDFJSON.getString(
			environment.getElementFromObject(KEY, PATH+".data.document")
				.getAsJsonObject()
				.get("identification")
		);
		assertNotEquals(OLD_CNPJ, newIdentification);
	}

	@Test
	public void unhappyPathTestMissingBrazilPayment() {
		environment.putObject("resource", new JsonObject());
		EditRecurringPaymentBodyToSetInvalidIdentification condition = new EditRecurringPaymentBodyToSetInvalidIdentification();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data.document in payments payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		EditRecurringPaymentBodyToSetInvalidIdentification condition = new EditRecurringPaymentBodyToSetInvalidIdentification();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data.document in payments payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentMissingDocument.json", key=KEY, path=PATH)
	public void unhappyPathTestMissingDocument() {
		EditRecurringPaymentBodyToSetInvalidIdentification condition = new EditRecurringPaymentBodyToSetInvalidIdentification();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data.document in payments payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentWithDocumentMissingIdentification.json", key=KEY, path=PATH)
	public void unhappyPathTestMissingIdentification() {
		EditRecurringPaymentBodyToSetInvalidIdentification condition = new EditRecurringPaymentBodyToSetInvalidIdentification();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find identification field for the document";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentWithDocumentWithInvalidIdentification.json", key=KEY, path=PATH)
	public void unhappyPathTestWithInvalidCpfCnpj() {
		EditRecurringPaymentBodyToSetInvalidIdentification condition = new EditRecurringPaymentBodyToSetInvalidIdentification();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "The value inside the identification field is neither a CPF nor a CNPJ";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
