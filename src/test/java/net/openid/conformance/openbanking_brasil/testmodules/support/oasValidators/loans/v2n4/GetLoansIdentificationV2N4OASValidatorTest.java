package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetLoansIdentificationV2N4OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identification.json")
	public void happyPath() {
		run(new GetLoansIdentificationV2n4OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identificationIndexerInfoIsOptional.json")
	public void happyPathIndexerInfoIsOptional1() {
		run(new GetLoansIdentificationV2n4OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identificationIndexerInfoIsOptional2.json")
	public void happyPathIndexerInfoIsOptional2() {
		run(new GetLoansIdentificationV2n4OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identificationIndexerInfoIsOptional3.json")
	public void happyPathIndexerInfoIsOptional3() {
		run(new GetLoansIdentificationV2n4OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identificationNoIndexerAdditionalInfo.json")
	public void unhappyPathNoIndexerAdditionalInfo() {
		ConditionError e = runAndFail(new GetLoansIdentificationV2n4OASValidator());
		assertThat(e.getMessage(), containsString("referentialRateIndexerAdditionalInfo is required when referentialRateIndexerType and referentialRateIndexerSubType equals to OUTROS_INDEXADORES"));
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identificationNoChargeAdditionalInfo.json")
	public void unhappyPathNoChargeAdditionalInfo() {
		ConditionError e = runAndFail(new GetLoansIdentificationV2n4OASValidator());
		assertThat(e.getMessage(), containsString("chargeAdditionalInfo is required when chargeType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identificationNoCnpjConsignee.json")
	public void unhappyPathNoCnpjConsignee() {
		ConditionError e = runAndFail(new GetLoansIdentificationV2n4OASValidator());
		assertThat(e.getMessage(), containsString("cnpjConsignee is required when productSubType is CREDITO_PESSOAL_COM_CONSIGNACAO"));
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identificationNoFeeAmount.json")
	public void unhappyPathNoFeeAmount() {
		ConditionError e = runAndFail(new GetLoansIdentificationV2n4OASValidator());
		assertThat(e.getMessage(), containsString("feeAmount is required when feeCharge is not PERCENTUA"));
	}


	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identificationNoFeeRate.json")
	public void unhappyPathNoFeeRate() {
		ConditionError e = runAndFail(new GetLoansIdentificationV2n4OASValidator());
		assertThat(e.getMessage(), containsString("feeRate is required when feeCharge is PERCENTUAL"));
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/identificationNoScheduledAdditionalInfo.json")
	public void unhappyPathNoScheduledAdditionalInfo() {
		ConditionError e = runAndFail(new GetLoansIdentificationV2n4OASValidator());
		assertThat(e.getMessage(), containsString("amortizationScheduledAdditionalInfo is required when amortizationScheduled is OUTROS"));
	}

}
