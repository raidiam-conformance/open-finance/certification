package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.rejectionReason;

import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentRejectionReasonCodeWasForaPrazoPermitido;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureRecurringPaymentRejectionReasonCodeWasForaPrazoPermitidoTest extends AbstractEnsureRecurringPaymentsRejectionReasonTest {

	@Override
	protected AbstractEnsureRecurringPaymentsRejectionReason condition() {
		return new EnsureRecurringPaymentRejectionReasonCodeWasForaPrazoPermitido();
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "RejectionReasonForaPrazoPermitido.json")
	public void happyPathTest() {
		run(condition());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + ".json")
	public void unhappyPathTestRejectionReasonCodeNotMatching() {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains("Rejection reason returned in the response does not match the expected rejection reason"));
	}
}
