package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class EnsureSpecificPermissionsWereReturnedTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("massadedados/F2_Consentimento/post-consents-201-2.json")
	public void testHappyPath() {
		environment.putString("consent_permissions","CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ CUSTOMERS_PERSONAL_ADITTIONALINFO_READ CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ CUSTOMERS_BUSINESS_ADITTIONALINFO_READ RESOURCES_READ");
		EnsureSpecificPermissionsWereReturned condition = new EnsureSpecificPermissionsWereReturned();
		run(condition);
	}

	@Test
	@UseResurce("massadedados/F2_Consentimento/post-consents-201-2.json")
	public void testUnhappyPermissionsMismatch() {
		environment.putString("consent_permissions","RESOURCES_READ ACCOUNTS_READ");
		EnsureSpecificPermissionsWereReturned condition = new EnsureSpecificPermissionsWereReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Consent endpoint response does not contain expected permissions"));
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseWithoutData.json")
	public void testUnhappyPathNoData() {
		EnsureSpecificPermissionsWereReturned condition = new EnsureSpecificPermissionsWereReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Data element is missing in the body"));
	}

	@Test
	public void testUnhappyPathNoBody() {
		EnsureSpecificPermissionsWereReturned condition = new EnsureSpecificPermissionsWereReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}
}
