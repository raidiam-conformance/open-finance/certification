package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasConsentimentoEmStatusRejeitado;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasDataPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasPermissoesPfPjEmConjunto;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasPermissoesPjIncorretas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasSemPermissoesFuncionaisRestantes;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasStatusResourcePendingAuthorisation;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;

public class AbstractEnsureErrorResponseCodeFieldWasTest extends AbstractJsonResponseConditionUnitTest {


	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseV2.json")
	public void processFindCodeField() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeFieldWasParametroNaoInformado();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseV2.json")
	public void processFindCodeField2() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeWasDataPagamentoInvalida();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/errors/422/bad422WrongCode.json")
	public void processFindCodeField3() {
		setJwt(true);
		setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeWasDataPagamentoInvalida();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		ConditionError conditionError = runAndFail(condition);
		Assert.assertTrue(conditionError.getMessage().contains("Could not find error with expected code in the errors JSON array"));
	}

	@Test
	@UseResurce("jsonResponses/errors/422/good422StatusResourcePendingAuthorisationErrorResponse.json")
	public void testHappyPathRightCode() {
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeFieldWasStatusResourcePendingAuthorisation();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/errors/422/bad422ErrorNotFoundResponse.json")
	public void testUnhappyPathMissingError() {
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeFieldWasStatusResourcePendingAuthorisation();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		ConditionError conditionError = runAndFail(condition);
		Assert.assertTrue(conditionError.getMessage().contains("Could not find error with expected code in the errors JSON array"));
	}

	@Test
	@UseResurce("jsonResponses/errors/422/bad422ErrorNotJsonArrayResponse.json")
	public void UnhappyPathNotJsonArray() {
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeFieldWasStatusResourcePendingAuthorisation();
		environment.mapKey("resource_endpoint_response_full", AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		ConditionError conditionError = runAndFail(condition);
		Assert.assertTrue(conditionError.getMessage().contains("Errors is not JSON array"));
	}

	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseV3.json")
	@Test
	public void happyResponsePermissoesPfPjEmConjunto() {
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeWasPermissoesPfPjEmConjunto();
		environment.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		run(condition);
	}

	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseV3.json")
	@Test
	public void happyResponsePermissoesPjIncorretas() {
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeWasPermissoesPjIncorretas();
		environment.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		run(condition);
	}

	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseConsentimentoEmStatusRejeitadoV3.json")
	@Test
	public void happyResponseConsentimentoEmStatusRejeitado() {
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeWasConsentimentoEmStatusRejeitado();
		environment.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		run(condition);
	}

	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseCombinacaoPermissoesIncorretaV3.json")
	@Test
	public void happyResponseCombinacaoPermissoesIncorreta() {
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta();
		environment.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		run(condition);
	}

	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseSemPermissoesFuncionaisRestantesV3.json")
	@Test
	public void happyResponseSemPermissoesFuncionaisRestantes() {
		AbstractEnsureErrorResponseCodeFieldWas condition = new EnsureErrorResponseCodeWasSemPermissoesFuncionaisRestantes();
		environment.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		run(condition);
	}
}
