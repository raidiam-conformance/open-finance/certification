package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnsureTokenResponseWas400Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPath() {
		setStatus(400);
		EnsureTokenResponseWas400 cond = new EnsureTokenResponseWas400();
		run(cond);
	}

	@Test
	public void wrongStatus(){
		setStatus(201);
		EnsureTokenResponseWas400 cond = new EnsureTokenResponseWas400();
		ConditionError e = runAndFail(cond);
		assertEquals(String.format("%s: Was expecting a 400 in the token response", cond.getClass().getSimpleName()), e.getMessage());
	}

	@Test
	public void noStatus(){
		EnsureTokenResponseWas400 cond = new EnsureTokenResponseWas400();
		ConditionError e = runAndFail(cond);
		assertEquals(String.format("%s: token_endpoint_response_http_status was not found", cond.getClass().getSimpleName()), e.getMessage());
	}

}
