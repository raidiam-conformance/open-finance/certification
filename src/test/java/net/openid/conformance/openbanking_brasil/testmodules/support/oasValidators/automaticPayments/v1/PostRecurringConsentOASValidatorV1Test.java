package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class PostRecurringConsentOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(201);
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/patchRecurringConsents/PatchRecurringConsentsResponseOK.json")
	public void happyPath() {
		run(new PostRecurringConsentOASValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/GetRecurringConsentsResponseOKSweeping.json")
	public void happyPathSweeping() {
		run(new PostRecurringConsentOASValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/GetRecurringConsentsResponseOKVrp.json")
	public void happyPathVrp() {
		run(new PostRecurringConsentOASValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/GetRecurringConsentsResponseNoDayOfMonth.json")
	public void unhappyPathAutomaticDayOfMonthMissing() {
		ConditionError e = runAndFail(new PostRecurringConsentOASValidatorV1());
		assertThat(e.getMessage(), containsString("dayOfMonth is required when period is [MENSAL, ANUAL]"));
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/GetRecurringConsentsResponseNoDayOfWeek.json")
	public void unhappyPathAutomaticDayOfWeekMissing() {
		ConditionError e = runAndFail(new PostRecurringConsentOASValidatorV1());
		assertThat(e.getMessage(), containsString("dayOfWeek is required when period is SEMANAL"));
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/GetRecurringConsentsResponseNoMonth.json")
	public void unhappyPathAutomaticMonthMissing() {
		ConditionError e = runAndFail(new PostRecurringConsentOASValidatorV1());
		assertThat(e.getMessage(), containsString("month is required when period is ANUAL"));
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/GetRecurringConsentsResponseSweepingLimitsMissing.json")
	public void unhappyPathSweepingPeriodicLimitsMissing() {
		ConditionError e = runAndFail(new PostRecurringConsentOASValidatorV1());
		assertThat(e.getMessage(), containsString("must have at least one of quantityLimit or transactionLimit."));
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/GetRecurringConsentsResponseVrpLimitsMissing.json")
	public void unhappyPathVrpPeriodicLimitsMissing() {
		ConditionError e = runAndFail(new PostRecurringConsentOASValidatorV1());
		assertThat(e.getMessage(), containsString("must have at least one of quantityLimit or transactionLimit."));
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/patchRecurringConsents/PatchRecurringConsentsResponseMissingIssuer.json")
	public void unhappyPathDebtorAccountIssuerMissing() {
		ConditionError e = runAndFail(new PostRecurringConsentOASValidatorV1());
		assertThat(e.getMessage(), containsString("debtorAccount.issuer is required when debtorAccount.accountType is [CACC, SVGS]"));
	}
}
