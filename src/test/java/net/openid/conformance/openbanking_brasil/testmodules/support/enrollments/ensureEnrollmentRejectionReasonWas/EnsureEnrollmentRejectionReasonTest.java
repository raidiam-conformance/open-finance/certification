package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentRejectionReasonEnum;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureEnrollmentRejectionReasonTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTests() {
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoDispositivoIncompativel(), EnrollmentRejectionReasonEnum.REJEITADO_DISPOSITIVO_INCOMPATIVEL);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoFalhaFido(), EnrollmentRejectionReasonEnum.REJEITADO_FALHA_FIDO);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoFalhaInfraestrutura(), EnrollmentRejectionReasonEnum.REJEITADO_FALHA_INFRAESTRUTURA);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoHybridFlow(), EnrollmentRejectionReasonEnum.REJEITADO_FALHA_HYBRID_FLOW);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoManualmente(), EnrollmentRejectionReasonEnum.REJEITADO_MANUALMENTE);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoMaximoChallengesAtingido(), EnrollmentRejectionReasonEnum.REJEITADO_MAXIMO_CHALLENGES_ATINGIDO);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoOutro(), EnrollmentRejectionReasonEnum.REJEITADO_OUTRO);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoSegurancaInterna(), EnrollmentRejectionReasonEnum.REJEITADO_SEGURANCA_INTERNA);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoAccountHolderValidation(), EnrollmentRejectionReasonEnum.REJEITADO_TEMPO_EXPIRADO_ACCOUNT_HOLDER_VALIDATION);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoAuthorisation(), EnrollmentRejectionReasonEnum.REJEITADO_TEMPO_EXPIRADO_AUTHORISATION);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoRiskSignals(), EnrollmentRejectionReasonEnum.REJEITADO_TEMPO_EXPIRADO_RISK_SIGNALS);
		happyPathTest(new EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoEnrollment(), EnrollmentRejectionReasonEnum.REJEITADO_TEMPO_EXPIRADO_ENROLLMENT);
	}

	@Test
	public void unhappyPathTestMissingBody() {
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putString(AbstractEnsureEnrollmentRejectionReasonWas.RESPONSE_ENV_KEY, "body", "{}");
		unhappyPathTest("Could not extract data from body");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getEnrollmentsNoCancellation.json")
	public void unhappyPathTestMissingCancellation() {
		unhappyPathTest("Could not extract cancellation from data");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getEnrollmentsNoReason.json")
	public void unhappyPathTestMissingReason() {
		unhappyPathTest("Could not extract reason from cancellation");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getEnrollmentsNoRejectionReason.json")
	public void unhappyPathTestMissingRejectionReason() {
		unhappyPathTest("Could not extract rejectionReason from reason");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getEnrollments.json")
	public void unhappyPathTestWrongRejectionReason() {
		unhappyPathTest("Enrollments rejection reason is not what was expected");
	}

	@Test
	public void unhappyPathTestInvalidJwt() {
		environment.putString(AbstractEnsureEnrollmentRejectionReasonWas.RESPONSE_ENV_KEY, "body", "ey1.b.c");
		unhappyPathTest("Could not parse the body");
	}

	protected void happyPathTest(AbstractEnsureEnrollmentRejectionReasonWas condition, EnrollmentRejectionReasonEnum rejectionReason) {
		JsonArray permissions = new JsonArray();
		permissions.add("PAYMENTS_INITIATE");
		JsonObject body = new JsonObjectBuilder()
			.addFields("data", Map.of(
				"enrollmentId", "urn:bancoex:C1DD33123",
				"creationDateTime", "2023-07-12T08:30:00Z",
				"status", "REJECTED",
				"expirationDateTime", "2023-07-12T08:30:00Z",
				"permissions", permissions,
				"transactionLimit", "100000.12",
				"dailyLimit", "100000.12"
			))
			.addFields("data.loggedUser.document", Map.of(
				"identification", "11111111111",
				"rel", "CPF"
			))
			.addFields("data.businessEntity.document", Map.of(
				"identification", "11111111111111",
				"rel", "CNPJ"
			))
			.addFields("data.debtorAccount", Map.of(
				"ispb", "12345678",
				"issuer", "1774",
				"number", "1234567890",
				"accountType", "TRAN"
			))
			.addFields("data.cancellation.cancelledBy.document", Map.of(
				"identification", "11111111111",
				"rel", "CPF"
			))
			.addFields("data.cancellation", Map.of(
				"additionalInformation", "Contrato entre iniciadora e detentora interrompido",
				"cancelledFrom", "INICIADORA",
				"rejectedAt", "2023-07-12T08:30:00Z"
			))
			.addField("data.cancellation.reason.rejectionReason", rejectionReason.toString())
			.addField("links.self", "https://api.banco.com.br/open-banking/enrollments/v1/enrollments/urn:bancoex:C1DD33123")
			.addField("meta.requestDateTime", "2023-07-12T08:30:00Z")
			.build();
		environment.putString(AbstractEnsureEnrollmentRejectionReasonWas.RESPONSE_ENV_KEY, "body", body.toString());
		run(condition);
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureEnrollmentRejectionReasonWasRejeitadoOutro());
		assertTrue(error.getMessage().contains(message));
	}
}
