package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetFirstPaymentCreditorAccount;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentBodyToSetFirstPaymentCreditorAccountTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBodyMissingCreditorAccountV2.json";
	protected static final String BASE_REC_CONFIG_JSON_PATH = "jsonEnvironmentObjects/recurringConfigurationObject/automaticRecurringConfiguration";
	protected static final String KEY = "resource";
	protected static final String PATH = "brazilPixPayment";
	protected static final String REC_CONFIG_KEY = "recurring_configuration_object";

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	@UseResurce(value = BASE_REC_CONFIG_JSON_PATH + ".json", key = REC_CONFIG_KEY)
	public void happyPathTest() {
		run(new EditRecurringPaymentBodyToSetFirstPaymentCreditorAccount());

		JsonObject recConfigCreditorAccount = environment
			.getElementFromObject(REC_CONFIG_KEY, "automatic.firstPayment.creditorAccount")
			.getAsJsonObject();

		JsonObject paymentCreditorAccount = environment
			.getElementFromObject(KEY, PATH + ".data.creditorAccount")
			.getAsJsonObject();

		assertEquals(recConfigCreditorAccount, paymentCreditorAccount);
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	@UseResurce(value = BASE_REC_CONFIG_JSON_PATH + "MissingFirstPayment.json", key = REC_CONFIG_KEY)
	public void unhappyPathTestRecurringConfigurationMissingFirstPayment() {
		unhappyPathTest("Unable to find firstPayment creditorAccount inside automatic recurring configuration object");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	@UseResurce(value = BASE_REC_CONFIG_JSON_PATH + "MissingCreditorAccount.json", key = REC_CONFIG_KEY)
	public void unhappyPathTestRecurringConfigurationMissingCreditorAccount() {
		unhappyPathTest("Unable to find firstPayment creditorAccount inside automatic recurring configuration object");
	}

	@Test
	@UseResurce(value = BASE_REC_CONFIG_JSON_PATH + ".json", key = REC_CONFIG_KEY)
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	@UseResurce(value = BASE_REC_CONFIG_JSON_PATH + ".json", key = REC_CONFIG_KEY)
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentBodyToSetFirstPaymentCreditorAccount());
		assertTrue(error.getMessage().contains(message));
	}
}
