package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrepareToPatchConsentRequestTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_URL = "https://www.example.com/payments/v4";
	private static final String CONSENT_URL = BASE_URL + "/consents";
	private static final String CONSENT_ID = "urn:bancoex:C1DD33123";

	@Test
	public void happyPathTest() {
		environment.putString("config", "resource.consentUrl", CONSENT_URL);
		environment.putString("consent_id", CONSENT_ID);

		PrepareToPatchConsentRequest condition = new PrepareToPatchConsentRequest();
		run(condition);

		assertEquals(BASE_URL + "/pix/payments/consents/" + CONSENT_ID, environment.getString("consent_url"));
		assertEquals("PATCH", environment.getString("http_method"));
	}
}
