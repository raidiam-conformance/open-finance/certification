package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureResponseHasNextLinkTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/common/GetStatusResponse.json")
	public void happyPathTest() {
		run(new EnsureResponseHasNextLink());
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResourceEndpointResponseFull() {
		run(new EnsureResponseHasNextLink());
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(EnsureResponseHasNextLink.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestInvalidJwt() {
		JsonObject body = new JsonObject();
		body.addProperty("body", "ey1.b.c");
		environment.putObject(EnsureResponseHasNextLink.RESPONSE_ENV_KEY, body);
		unhappyPathTest("Could not parse body");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsNoExtensions.json")
	public void unhappyPathTestHasNoLinks() {
		unhappyPathTest("'Next' link not found in the response.");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureResponseHasNextLink());
		assertTrue(error.getMessage().contains(message));
	}
}
