package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public abstract class AbstractRecurringPaymentPixOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/payments/";

	protected abstract int status();
	protected abstract AbstractRecurringPaymentPixOASValidatorV2 validator();

	@Before
	public void setUp() {
		setStatus(status());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201Response.json")
	public void happyPathTest() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseMissingOptionalFields.json")
	public void happyPathTestMissingOptionalFields() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseFidoFlowMissingRecurringConsentId.json")
	public void unhappyPathTestFidoFlowMissingRecurringConsentId() {
		unhappyPathTest("recurringConsentId is required when authorisationFlow is FIDO_FLOW");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseRJCTMissingRejectionReason.json")
	public void unhappyPathTestRJCTMissingRejectionReason() {
		unhappyPathTest("rejectionReason is required when status is RJCT");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseCANCMissingCancellation.json")
	public void unhappyPathTestCANCMissingCancellation() {
		unhappyPathTest("cancellation is required when status is CANC");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseDICTMissingProxy.json")
	public void unhappyPathTestDICTMissingProxy() {
		unhappyPathTest("proxy is required when localInstrument is [DICT, INIC]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseINICMissingProxy.json")
	public void unhappyPathTestINICMissingProxy() {
		unhappyPathTest("proxy is required when localInstrument is [DICT, INIC]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseMANUWithProxy.json")
	public void unhappyPathTestMANUWithProxy() {
		unhappyPathTest("proxy cannot be present when localInstrument has the value MANU");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseCreditorAccountCACCMissingIssuer.json")
	public void unhappyPathTestCreditorAccountCACCMissingIssuer() {
		unhappyPathTest("issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseCreditorAccountSVGSMissingIssuer.json")
	public void unhappyPathTestCreditorAccountSVGSMissingIssuer() {
		unhappyPathTest("issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseINICMissingTransactionIdentification.json")
	public void unhappyPathTestINICMissingTransactionIdentification() {
		unhappyPathTest("transactionIdentification is required when localInstrument is INIC");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseMANUWithTransactionIdentification.json")
	public void unhappyPathTestMANUWithTransactionIdentification() {
		unhappyPathTest("transactionIdentification cannot be present when localInstrument has the value MANU");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseDICTWithTransactionIdentification.json")
	public void unhappyPathTestDICTWithTransactionIdentification() {
		unhappyPathTest("transactionIdentification cannot be present when localInstrument has the value DICT");
	}


	// TODO: uncomment the lines below when swagger is fixed
//	@Test
//	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseDebtorAccountCACCMissingIssuer.json")
//	public void unhappyPathTestDebtorAccountCACCMissingIssuer() {
//		run(validator());
//		unhappyPathTest("issuer is required when accountType is [CACC, SVGS]");
//	}
//
//	@Test
//	@UseResurce(BASE_JSON_PATH + "PostRecurringPayments201ResponseDebtorAccountSVGSMissingIssuer.json")
//	public void unhappyPathTestDebtorAccountSVGSMissingIssuer() {
//		unhappyPathTest("issuer is required when accountType is [CACC, SVGS]");
//	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(validator());
		assertTrue(error.getMessage().contains(message));
	}
}
