package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.consentsRejectionReason;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsConsentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentsConsentsRejectionReasonWasRejeitadoUsuario;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class AbstractEnsureRecurringPaymentsConsentsRejectionReasonTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_FILE_PATH = "jsonResponses/automaticpayments/PostRecurringConsents";
	protected static final String MISSING_FIELD_MESSAGE = "Unable to find element data.rejection.reason.code in the response payload";

	protected abstract AbstractEnsureRecurringPaymentsConsentsRejectionReason condition();

	public void happyPathTest() {
		run(condition());
	}

	@Test
	public void unhappyPathMissingResponse() {
		unhappyPathTest(MISSING_FIELD_MESSAGE);
	}

	@Test
	public void unhappyPathMissingData() {
		environment.putObject(EnsureRecurringPaymentsConsentsRejectionReasonWasRejeitadoUsuario.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest(MISSING_FIELD_MESSAGE);
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRejectionField.json")
	public void unhappyPathMissingRejection() {
		unhappyPathTest(MISSING_FIELD_MESSAGE);
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingReasonField.json")
	public void unhappyPathMissingReason() {
		unhappyPathTest(MISSING_FIELD_MESSAGE);
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingCodeField.json")
	public void unhappyPathMissingCode() {
		unhappyPathTest(MISSING_FIELD_MESSAGE);
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "RejectionReasonNaoInformado.json")
	public void unhappyPathRejectedByNotMatchingExpected() {
		unhappyPathTest("Rejection reason returned in the response does not match the expected rejection reason");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}
}
