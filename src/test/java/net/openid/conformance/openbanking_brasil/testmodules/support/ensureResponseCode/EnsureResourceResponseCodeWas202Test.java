package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureResourceResponseCodeWas202Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureResourceResponseCodeWas202();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.ACCEPTED.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.BAD_REQUEST.value();
	}

}
