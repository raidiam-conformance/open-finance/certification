package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.FAPIBrazilAddRecurringConsentIdToClientScope;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FAPIBrazilAddRecurringConsentIdToClientScopeTest extends AbstractJsonResponseConditionUnitTest {

	private static final String RECURRING_CONSENT_ID = "urn:bank:123456";

	@Before
	public void init() {
		environment.putString("consent_id", RECURRING_CONSENT_ID);
	}

	@Test
	public void happyPathTest() {
		environment.putObject("client", new JsonObjectBuilder().addField("scope", "openid recurring-payments").build());
		FAPIBrazilAddRecurringConsentIdToClientScope condition = new FAPIBrazilAddRecurringConsentIdToClientScope();
		run(condition);
		String scope = OIDFJSON.getString(environment.getElementFromObject("client", "scope"));
		assertTrue(scope.endsWith("recurring-consent:"+RECURRING_CONSENT_ID));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathMissingClient() {
		FAPIBrazilAddRecurringConsentIdToClientScope condition = new FAPIBrazilAddRecurringConsentIdToClientScope();
		run(condition);
	}

	@Test
	public void unhappyPathMissingScope() {
		environment.putObject("client", new JsonObject());
		FAPIBrazilAddRecurringConsentIdToClientScope condition = new FAPIBrazilAddRecurringConsentIdToClientScope();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("scope missing in client object"));
	}

	@Test
	public void unhappyPathScopeEmpty() {
		environment.putObject("client", new JsonObjectBuilder().addField("scope", "").build());
		FAPIBrazilAddRecurringConsentIdToClientScope condition = new FAPIBrazilAddRecurringConsentIdToClientScope();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("scope empty in client object"));
	}
}
