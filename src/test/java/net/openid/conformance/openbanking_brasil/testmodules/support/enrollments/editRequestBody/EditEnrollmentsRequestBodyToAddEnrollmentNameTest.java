package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EditEnrollmentsRequestBodyToAddEnrollmentNameTest extends AbstractJsonResponseConditionUnitTest {

	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";
	private static final String ENROLLMENT_NAME_KEY = "enrollment_name";

	@Test
	@UseResurce(value="jsonRequests/payments/enrollments/v2/postEnrollmentsWithoutEnrollmentNameV2.json", key=REQUEST_CLAIMS_KEY)
	public void happyPathTest() {
		run(new EditEnrollmentsRequestBodyToAddEnrollmentName());
		JsonElement enrollmentNameElement = environment.getElementFromObject(REQUEST_CLAIMS_KEY, "data.enrollmentName");
		assertTrue(enrollmentNameElement.isJsonPrimitive() && enrollmentNameElement.getAsJsonPrimitive().isString());

		String enrollmentName = environment.getString(ENROLLMENT_NAME_KEY);
		assertEquals(OIDFJSON.getString(enrollmentNameElement), enrollmentName);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingRequestClaims() {
		run(new EditEnrollmentsRequestBodyToAddEnrollmentName());
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(REQUEST_CLAIMS_KEY, new JsonObject());
		ConditionError error = runAndFail(new EditEnrollmentsRequestBodyToAddEnrollmentName());
		assertTrue(error.getMessage().contains("Could not find data inside the body"));
	}
}
