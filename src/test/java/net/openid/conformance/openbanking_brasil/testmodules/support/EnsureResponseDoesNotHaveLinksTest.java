package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureResponseDoesNotHaveLinksTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsNoExtensions.json")
	public void happyPathTest() {
		run(new EnsureResponseDoesNotHaveLinks());
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResourceEndpointResponseFull() {
		run(new EnsureResponseDoesNotHaveLinks());
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(EnsureResponseDoesNotHaveLinks.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestInvalidJwt() {
		JsonObject body = new JsonObject();
		body.addProperty("body", "ey1.b.c");
		environment.putObject(EnsureResponseDoesNotHaveLinks.RESPONSE_ENV_KEY, body);
		unhappyPathTest("Could not parse body");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsNoExtensionsWithLinks.json")
	public void unhappyPathTestHasLinks() {
		unhappyPathTest("Response body has \"links\" field");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureResponseDoesNotHaveLinks());
		assertTrue(error.getMessage().contains(message));
	}
}
