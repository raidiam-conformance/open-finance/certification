package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeThat;

public abstract class AbstractEnsureNumberOfTotalRecordsTest extends AbstractJsonResponseConditionUnitTest {


	// Equal Tests
	@Test
	public void equalHappytest() {
		assumeThat("Operation is EQUAL", getComparisonMethod(), is(TotalRecordsComparisonOperator.EQUAL));
		addTestItems(getTotalRecordsComparisonAmount());
		run(getCondition());
	}


	@Test
	public void equalTestLessItems() {
		assumeThat("Operation is EQUAL", getComparisonMethod(), is(TotalRecordsComparisonOperator.EQUAL));
		int testItems = getTotalRecordsComparisonAmount() - 1;
		addTestItems(testItems);
		ConditionError e = runAndFail(getCondition());
		assertTrue(e.getMessage()
			.contains(String.format("Total records amount was not the expected %d, but %d", getTotalRecordsComparisonAmount(), testItems)));
	}

	@Test
	public void equalTestMoreItems() {
		assumeThat("Operation is EQUAL", getComparisonMethod(), is(TotalRecordsComparisonOperator.EQUAL));
		int testItems = getTotalRecordsComparisonAmount() + 1;
		addTestItems(testItems);
		ConditionError e = runAndFail(getCondition());
		assertTrue(e.getMessage()
			.contains(String.format("Total records amount was not the expected %d, but %d", getTotalRecordsComparisonAmount(), testItems)));
	}


	// At least tests
	@Test
	public void atLeastHappyTest() {
		assumeThat("Operation is AT_LEAST", getComparisonMethod(), is(TotalRecordsComparisonOperator.AT_LEAST));
		addTestItems(getTotalRecordsComparisonAmount());
		run(getCondition());
	}

	@Test
	public void atLeastHappyTestMoreItems() {
		assumeThat("Operation is AT_LEAST", getComparisonMethod(), is(TotalRecordsComparisonOperator.AT_LEAST));
		addTestItems(getTotalRecordsComparisonAmount() + 1);
		run(getCondition());
	}

	@Test
	public void atLeastTestLessItems() {
		assumeThat("Operation is AT_LEAST", getComparisonMethod(), is(TotalRecordsComparisonOperator.AT_LEAST));
		int testItems = getTotalRecordsComparisonAmount() - 1;
		addTestItems(testItems);
		ConditionError e = runAndFail(getCondition());
		assertTrue(e.getMessage()
			.contains(String.format("Total records amount was not more the expected %d, but less: %d", getTotalRecordsComparisonAmount(), testItems)));
	}


	protected abstract int getTotalRecordsComparisonAmount();

	protected abstract TotalRecordsComparisonOperator getComparisonMethod();

	protected abstract void addTestItems(int items);

	protected abstract AbstractEnsureNumberOfTotalRecords getCondition();


	public void addMetaToEnv(int numberOfElements) {
		environment.putString("resource_endpoint_response_full", "body", new JsonObjectBuilder()
			.addField("meta.totalRecords", numberOfElements)
			.build()
			.toString());
	}


	public void addArrayToEnv(int numberOfElements) {
		JsonArray array = new JsonArray();
		IntStream.range(0, numberOfElements).forEach(array::add);
		JsonObject body = new JsonObject();
		body.add("data", array);
		environment.putString("resource_endpoint_response_full", "body", body.toString());
	}


}
