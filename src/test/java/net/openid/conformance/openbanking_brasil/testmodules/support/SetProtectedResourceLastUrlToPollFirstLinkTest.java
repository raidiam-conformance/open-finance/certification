package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SetProtectedResourceLastUrlToPollFirstLinkTest extends AbstractJsonResponseConditionUnitTest {

	String firstUrl;
	String selfUrl;
	String lastUrl;

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3MultipleResourcesLastPageAvailable.json")
	public void happyPathTestLastPage() {
		environment.putBoolean("is_last_page", true);
		run(new SetProtectedResourceLastUrlToPollFirstLink());
		setLinks();
		assertEquals(firstUrl, environment.getString("protected_resource_url"));
		assertNotEquals(selfUrl, environment.getString("protected_resource_url"));
		assertNotEquals(lastUrl, environment.getString("protected_resource_url"));

		assertFalse(environment.getBoolean("is_last_page"));
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3MultipleResourcesFirstPageAvailable.json")
	public void happyPathTestFirstPage() {

		environment.putBoolean("is_last_page", false);
		run(new SetProtectedResourceLastUrlToPollFirstLink());
		setLinks();
		//protected_resource_url is not set by this method if criteria is not met
		assertNull(environment.getString("protected_resource_url"));

		assertFalse(environment.getBoolean("is_last_page"));
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3OnlySelfLink.json")
	public void happyPathTestSelfOnly() {
		environment.putBoolean("is_last_page", true);
		run(new SetProtectedResourceLastUrlToPollFirstLink());
		setLinks();
		assertEquals(selfUrl, environment.getString("protected_resource_url"));

		assertFalse(environment.getBoolean("is_last_page"));
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3MissingLinks.json")
	public void unhappyPathTestMissingLinks() {
		environment.putBoolean("is_last_page", true);
		ConditionError error = runAndFail(new SetProtectedResourceLastUrlToPollFirstLink());
		assertTrue(error.getMessage().contains("No links found in the response"));
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3EmptyLinks.json")
	public void unhappyPathTestEmptyLinks() {
		environment.putBoolean("is_last_page", true);
		ConditionError error = runAndFail(new SetProtectedResourceLastUrlToPollFirstLink());
		assertTrue(error.getMessage().contains("No valid URL found in the response links"));
	}

	private JsonObject parseResponseBody(Environment env) {
		return JsonParser.parseString(env.getString("resource_endpoint_response_full", "body"))
			.getAsJsonObject();
	}

	private void setLinks() {
		JsonObject body = parseResponseBody(environment);
		JsonObject links = body.getAsJsonObject("links");
		if (links.get("first") != null) {
			firstUrl = OIDFJSON.getString(links.get("first"));
		}
		selfUrl = OIDFJSON.getString(links.get("self"));
		if (links.get("last") != null) {
			lastUrl = OIDFJSON.getString(links.get("last"));
		}
	}
}
