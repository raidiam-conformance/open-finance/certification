package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentConsentIdExtractorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4Ok.json")
	public void happyPathTest() {
		PaymentConsentIdExtractor condition = new PaymentConsentIdExtractor();
		run(condition);
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		PaymentConsentIdExtractor condition = new PaymentConsentIdExtractor();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Could not extract body from response"));
	}

	@Test
	public void unhappyPathTestInvalidJWT() {
		JsonObject consent = new JsonObject();
		consent.addProperty("body", "ey1.b.c");
		environment.putObject("consent_endpoint_response_full", consent);
		PaymentConsentIdExtractor condition = new PaymentConsentIdExtractor();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Could not parse the body"));
	}
}
