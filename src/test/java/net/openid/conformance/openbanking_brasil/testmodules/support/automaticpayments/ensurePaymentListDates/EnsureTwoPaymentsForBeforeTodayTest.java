package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensurePaymentListDates;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.AbstractEnsurePaymentListDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.EnsureTwoPaymentsForBeforeToday;

import java.time.LocalDate;
import java.time.ZoneId;

public class EnsureTwoPaymentsForBeforeTodayTest extends AbstractEnsurePaymentListDatesTest {

	@Override
	protected AbstractEnsurePaymentListDates condition() {
		return new EnsureTwoPaymentsForBeforeToday();
	}

	@Override
	protected LocalDate expectedDate() {
		return LocalDate.now(ZoneId.of("UTC-3")).minusDays(1);
	}

	@Override
	protected int expectedAmount() {
		return 2;
	}
}
