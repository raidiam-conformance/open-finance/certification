package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.specto.hoverfly.junit.core.model.RequestFieldMatcher;
import io.specto.hoverfly.junit.dsl.HoverflyDsl;
import io.specto.hoverfly.junit.rule.HoverflyRule;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static io.specto.hoverfly.junit.core.SimulationSource.dsl;
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.noContent;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CallEnrollmentsEndpointWithBearerTokenTest {

	@Spy
	private Environment env = new Environment();

	@Mock
	private TestInstanceEventLog eventLog;

	private static final JsonObject bearerToken = JsonParser.parseString("{"
		+ "\"value\":\"mF_9.B5f-4.1JqM\","
		+ "\"type\":\"Bearer\""
		+ "}").getAsJsonObject();

	@ClassRule
	public static final HoverflyRule hoverfly = HoverflyRule.inSimulationMode(dsl(
		service("example.com")
			.get("/resource")
			.anyBody()
			.willReturn(success("OK", "text/plain;charset=UTF-8")),
		service("example.com")
			.post("/resource")
			.body("{\"example\": \"example\"}")
			.header("content-type", "application/jwt")
			.willReturn(success("OK", "application/jwt")),//success only if body != null and headers changed to app/jwt from what is originally in test
		service("example.com")
			.delete("/resource")
			.body(RequestFieldMatcher.newJsonMatcher(null))
			.willReturn(noContent()),//success only if body == null and headers not changed to app/jwt from what is originally in test
		service("example.com")
			.patch("/resource")
			.body("{\"example\": \"example\"}")
			.header("content-type", "application/jwt")
			.willReturn(success("OK", "application/jwt")),//success only if body != null and headers changed to app/jwt from what is originally in test
		service("example.com")
			.post("/resource")
			.anyBody()
			.header("content-type", "application/jwt")
			.willReturn(HoverflyDsl.response().status(422)),
		service("example.com")
			.post("/resource")
			.anyBody()
			.header("content-type", "text/plain;charset=UTF-8")
			.willReturn(HoverflyDsl.response().status(422)),//422 only when header not changed to app/jwt from text/plain (any body)
		service("example.com")
			.patch("/resource")
			.anyBody()
			.header("content-type", "text/plain;charset=UTF-8")
			.willReturn(HoverflyDsl.response().status(422)),
		service("example.com")
			.patch("/resource")
			.anyBody()
			.header("content-type", "application/jwt")
			.willReturn(HoverflyDsl.response().status(422)),//422 only when header not changed to app/jwt from text/plain (any body)
		service("example.com")
			.get("/resource")
			.body("{\"example\": \"example\"}")
			.willReturn(HoverflyDsl.response().status(422)),//422 only when body other than null present (any header)
		service("example.com")
			.delete("/resource")
			.anyBody()
			.willReturn(HoverflyDsl.response().status(422))//422 only when body other than null present (any header)
	));

	private CallEnrollmentsEndpointWithBearerToken condition;

	@Before
	public void setUp() throws Exception {
		hoverfly.resetJournal();
		env.putObject("access_token", bearerToken);
		env.putString("enrollments_url", "http://example.com/resource");
		env.putObject("resource_endpoint_request_headers", new JsonObject());
		env.putObject("resource", new JsonObject());
		condition = new CallEnrollmentsEndpointWithBearerToken();
		condition.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		env.putString("resource_request_entity","{\"example\": \"example\"}");
	}

	@Test
	public void happyPathTestGet() {
		env.putString("http_method", "GET");
		condition.execute(env);

		hoverfly.verify(service("example.com")
			.get("/resource")
			.header("Authorization", "Bearer mF_9.B5f-4.1JqM"));

		verify(env, atLeastOnce()).getString("access_token", "value");
		verify(env, atLeastOnce()).getString("access_token", "type");
		verify(env, atLeastOnce()).getString("enrollments_url");

		assertThat(env.getElementFromObject("resource_endpoint_response_full","body").isJsonNull());
	}

	@Test
	public void happyPathTestPost() {
		env.putString("http_method", "POST");
		env.putString("resource_endpoint_request_headers", "content-type","text/plain;charset=UTF-8");
		env.putString("resource_request_entity", "{\"example\": \"example\"}");
		condition.execute(env);

		hoverfly.verify(service("example.com")
			.post("/resource")
			.anyBody()
			.header("Authorization", "Bearer mF_9.B5f-4.1JqM"));

		verify(env, atLeastOnce()).getString("access_token", "value");
		verify(env, atLeastOnce()).getString("access_token", "type");
		verify(env, atLeastOnce()).getString("enrollments_url");

		assertThat(env.getString("resource_endpoint_response_full","body")).isEqualTo("OK");
		assertThat(env.getString("resource_endpoint_response_headers", "content-type")).isEqualTo("application/jwt");
	}

	@Test
	public void happyPathTestDelete() {
		env.putString("http_method", "DELETE");
		condition.execute(env);

		hoverfly.verify(service("example.com")
			.delete("/resource")
			.header("Authorization", "Bearer mF_9.B5f-4.1JqM"));

		verify(env, atLeastOnce()).getString("access_token", "value");
		verify(env, atLeastOnce()).getString("access_token", "type");
		verify(env, atLeastOnce()).getString("enrollments_url");
	}

	@Test
	public void happyPathTestPatch() {
		env.putString("http_method", "PATCH");
		env.putString("resource_endpoint_request_headers", "content-type","text/plain;charset=UTF-8");
		condition.execute(env);

		hoverfly.verify(service("example.com")
			.patch("/resource")
			.anyBody()
			.header("content-type", "application/jwt"));

		verify(env, atLeastOnce()).getString("access_token", "value");
		verify(env, atLeastOnce()).getString("access_token", "type");
		verify(env, atLeastOnce()).getString("enrollments_url");

		assertThat(env.getString("resource_endpoint_response_full","body")).isEqualTo("OK");
		assertThat(env.getString("resource_endpoint_response_headers", "content-type")).isEqualTo("application/jwt");
	}

	@Test
	public void unhappyPathTestMissingEnrollmentsUrl() {
		env.putString("http_method", "GET");
		env.removeNativeValue("enrollments_url");
		try {
			condition.execute(env);
		} catch (ConditionError error) {
			assertThat(error.getMessage()).contains("enrollments url missing");
		}
	}

	@Test
	public void unhappyPathTestMissingMethod() {
		try {
			condition.execute(env);
		} catch (ConditionError error) {
			assertThat(error.getMessage()).contains("HTTP method not found");
		}
	}
}
