package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.junit.Test;
import org.springframework.http.HttpStatus;

public class EnsureResourceResponseCodeWas401or403Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureResourceResponseCodeWas401or403();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.UNAUTHORIZED.value();
	}

	protected int happyPathResponseCodeOtherPossibility() {
		return HttpStatus.FORBIDDEN.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.OK.value();
	}

	@Test
	public void happyPathOtherPossibility(){
		AbstractEnsureResponseCodeWas condition = condition();
		setStatus(happyPathResponseCodeOtherPossibility());
		run(condition);
	}
}
