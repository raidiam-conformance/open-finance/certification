package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureNoContractClientIdIsPresentInConfigTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		environment.putString("config", "resource.noContractClientId", "123");
		run(new EnsureNoContractClientIdIsPresentInConfig());
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingConfig() {
		run(new EnsureNoContractClientIdIsPresentInConfig());
	}

	@Test
	public void unhappyPathTestMissingResource() {
		environment.putObject("config", new JsonObject());
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingClientId() {
		environment.putObject("config", "resource", new JsonObject());
		unhappyPathTest();
	}

	protected void unhappyPathTest () {
		ConditionError error = runAndFail(new EnsureNoContractClientIdIsPresentInConfig());
		assertTrue(error.getMessage().contains("The client_id for no contract test is not present in the config"));
	}
}
