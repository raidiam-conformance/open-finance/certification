package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureThereArePayments;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereIsAtLeastOneSchdPayment;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureThereIsAtLeastOneSchdPaymentTest extends AbstractEnsureThereArePaymentsTest {

	@Override
	protected AbstractEnsureThereArePayments condition() {
		return new EnsureThereIsAtLeastOneSchdPayment();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "OneRjctOneSchd.json")
	public void happyPathTest() {
		super.happyPathTest();
	}

	@Override
	@Test
	public void unhappyPathTestAmountNotMatching() {
		environment.putObject(AbstractEnsureThereArePayments.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{\"data\": []}").build());
		unhappyPathTest("The amount of payments returned is not what was expected");
	}

	@Override
	public void unhappyPathTestStatusNotMatching() {
		unhappyPathTest("Not all of the expected statuses were found in the response");
	}
}
