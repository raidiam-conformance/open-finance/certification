package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createPatchConsentEdition;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyExpirationTomorrowNoAmountNoRiskSignals;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class CreatePatchRecurringConsentsForEditionRequestBodyExpirationTomorrowNoAmountNoRiskSignalsTest extends AbstractCreatePatchRecurringConsentsForEditionRequestBodyTest {

	@Override
	protected AbstractCreatePatchRecurringConsentsForEditionRequestBody condition() {
		return new CreatePatchRecurringConsentsForEditionRequestBodyExpirationTomorrowNoAmountNoRiskSignals();
	}

	@Override
	protected boolean hasRiskSignals() {
		return false;
	}

	@Override
	protected String getExpectedExpirationDateTime() {
		LocalDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		LocalDateTime dateTime = currentDateTime.plusDays(1);
		return dateTime.format(DATE_TIME_FORMATTER);
	}

	@Override
	protected String getExpectedMaxVariableAmount() {
		return null;
	}
}
