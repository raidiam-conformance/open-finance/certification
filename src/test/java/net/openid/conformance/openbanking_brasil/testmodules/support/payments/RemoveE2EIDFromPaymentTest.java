package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Optional;

import static org.mockito.Mockito.mock;

public class RemoveE2EIDFromPaymentTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathObjectTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);


		RemoveE2EIDFromPayment condition = new RemoveE2EIDFromPayment();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);
		JsonObject paymentConfig = environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonObject();

		Assert.assertNull(environment.getObject("endToEndId"));
		Assert.assertNull(paymentConfig.get("endToEndId"));
	}

	@Test
	public void happyPathArrayTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);
		environment.putString("payment_is_array", "ok");

		RemoveE2EIDFromPayment condition = new RemoveE2EIDFromPayment();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);
		JsonArray paymentConfig = environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonArray();

		Assert.assertNull(environment.getObject("endToEndId"));
		Assert.assertNull(paymentConfig.get(0).getAsJsonObject().get("endToEndId"));
	}
}
