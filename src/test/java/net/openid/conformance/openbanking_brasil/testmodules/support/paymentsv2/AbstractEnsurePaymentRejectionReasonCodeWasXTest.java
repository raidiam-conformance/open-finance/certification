package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasLimitePeriodoQuantidadeExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasLimitePeriodoValorExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasValorAcimaLimite;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasValorInvalido;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class AbstractEnsurePaymentRejectionReasonCodeWasXTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_PATH = "jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseRJCTWithRejectionReason";

	@Before
	public void init() {
		setJwt(true);
	}


	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseRjctWithRejectionReason2.json")
	public void validateGoodRejectionReason() {
		EnsurePaymentRejectionReasonCodeWasValorInvalido condition = new EnsurePaymentRejectionReasonCodeWasValorInvalido();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v2/paymentInitiationResponseRjctWithRejectionReason.json")
	public void validateBadRejectionReason() {
		EnsurePaymentRejectionReasonCodeWasValorInvalido condition = new EnsurePaymentRejectionReasonCodeWasValorInvalido();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Payment rejection code is not what was expected"));
	}

	@Test
	@UseResurce(BASE_PATH + "LimitePeriodoQuantidadeExcedido.json")
	public void happyPathTestLimitePeriodoQuantidadeExcedido() {
		EnsurePaymentRejectionReasonCodeWasLimitePeriodoQuantidadeExcedido condition = new EnsurePaymentRejectionReasonCodeWasLimitePeriodoQuantidadeExcedido();
		run(condition);
	}

	@Test
	@UseResurce(BASE_PATH + "LimitePeriodoValorExcedido.json")
	public void happyPathTestLimitePeriodoValorExcedido() {
		EnsurePaymentRejectionReasonCodeWasLimitePeriodoValorExcedido condition = new EnsurePaymentRejectionReasonCodeWasLimitePeriodoValorExcedido();
		run(condition);
	}

	@Test
	@UseResurce(BASE_PATH + "ValorAcimaLimite.json")
	public void happyPathTestValorAcimaLimite() {
		EnsurePaymentRejectionReasonCodeWasValorAcimaLimite condition = new EnsurePaymentRejectionReasonCodeWasValorAcimaLimite();
		run(condition);
	}
}
