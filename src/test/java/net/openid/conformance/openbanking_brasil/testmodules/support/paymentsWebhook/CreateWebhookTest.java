package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsWebhook;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentWebhook.CreatePaymentConsentWebhookv4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentWebhook.CreatePaymentWebhookV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePaymentConsentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePaymentWebhookv2;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CreateWebhookTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void init() {
		environment.putString("base_url", "https://localhost:8443/test/a/alias");
	}
	protected void assertWebhookUriPath(String expectedUrlPath, String valueFromEnv) {
		String webhookUri = environment.getString("webhook_uri_" + valueFromEnv);
		assertEquals("https://localhost:8443/test-mtls/a/alias" + expectedUrlPath, webhookUri);
	}

	protected void assertWebhookUriExists(String valueFromEnv) {
		String webhookUri = environment.getString("webhook_uri_" + valueFromEnv);
		assertNotNull(webhookUri);
	}
	@Test
	public void validatePaymentWebhookHappyPath(){
		CreatePaymentWebhookv2 createPaymentWebhookv2 = new CreatePaymentWebhookv2();
		run(createPaymentWebhookv2);
		assertWebhookUriExists("payment_id");
		assertWebhookUriPath("/open-banking/webhook/v1/payments/v2/pix/payments/", "payment_id");
	}

	@Test
	public void validatePaymentWebhookBaseUrlMissing(){
		CreatePaymentWebhookv2 createPaymentWebhookv2 = new CreatePaymentWebhookv2();
		environment.removeNativeValue("base_url");
		ConditionError error = runAndFail(createPaymentWebhookv2);
		assertEquals("CreatePaymentWebhookv2: [pre] Something unexpected happened (this could be caused by something you did wrong, or it may be an issue in the test suite - please review the instructions and your configuration, if you still see a problem please contact certification@oidf.org with the full details) - couldn't find string in environment: base_url", error.getMessage());
	}

	@Test
	public void validatePaymentWebhookBaseUrlEmpty(){
		CreatePaymentWebhookv2 createPaymentWebhookv2 = new CreatePaymentWebhookv2();
		environment.putString("base_url", "");
		ConditionError error = runAndFail(createPaymentWebhookv2);
		assertEquals("CreatePaymentWebhookv2: Base URL is empty", error.getMessage());
	}

	@Test
	public void validatePaymentConsentWebhookHappyPath(){
		CreatePaymentConsentWebhookV2 createPaymentConsentWebhookV2 = new CreatePaymentConsentWebhookV2();
		run(createPaymentConsentWebhookV2);
		assertWebhookUriExists("consent_id");
		assertWebhookUriPath("/open-banking/webhook/v1/payments/v2/consents/", "consent_id");
	}

	@Test
	public void validatePaymentv4WebhookHappyPath(){
		CreatePaymentWebhookV4 createPaymentWebhookV4 = new CreatePaymentWebhookV4();
		run(createPaymentWebhookV4);
		assertWebhookUriExists("payment_id");
		assertWebhookUriPath("/open-banking/webhook/v1/payments/v4/pix/payments/", "payment_id");
	}

	@Test
	public void validatePaymentConsentv4WebhookHappyPath(){
		CreatePaymentConsentWebhookv4 createPaymentConsentWebhookv4 = new CreatePaymentConsentWebhookv4();
		run(createPaymentConsentWebhookv4);
		assertWebhookUriExists("consent_id");
		assertWebhookUriPath("/open-banking/webhook/v1/payments/v4/consents/", "consent_id");
	}

}
