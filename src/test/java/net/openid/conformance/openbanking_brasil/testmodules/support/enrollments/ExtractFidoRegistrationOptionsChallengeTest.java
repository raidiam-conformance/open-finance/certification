package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtractFidoRegistrationOptionsChallengeTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void happyPathTest() {
		ExtractFidoRegistrationOptionsChallenge condition = new ExtractFidoRegistrationOptionsChallenge();
		run(condition);
		String challenge = environment.getString("fido_challenge");
		String decodedChallenge = new String(Base64.getUrlDecoder().decode(challenge), StandardCharsets.UTF_8);
		assertEquals("challenge string", decodedChallenge);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResponse() {
		ExtractFidoRegistrationOptionsChallenge condition = new ExtractFidoRegistrationOptionsChallenge();
		run(condition);
	}

	@Test
	public void unhappyPathMissingBody() {
		environment.putObject(ExtractFidoRegistrationOptionsChallenge.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathInvalidJwt() {
		environment.putObject(
			ExtractFidoRegistrationOptionsChallenge.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya1.b2.c3").build()
		);
		unhappyPathTest("Error parsing JWT response");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putString(ExtractFidoRegistrationOptionsChallenge.RESPONSE_ENV_KEY, "body", "{}");
		unhappyPathTest("Could not extract data.challenge from the registration options response");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptionsNoChallenge.json")
	public void unhappyPathTestMissingChallenge() {
		unhappyPathTest("Could not extract data.challenge from the registration options response");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptionsChallengeWithPadding.json")
	public void unhappyPathTestChallengeBase64WithPadding() {
		unhappyPathTest("data.challenge is not a valid base64 encoded without padding string");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptionsChallengeNotBase64Encoded.json")
	public void unhappyPathTestChallengeInvalidBase64() {
		unhappyPathTest("data.challenge is not a valid base64 encoded without padding string");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptionsEmptyChallenge.json")
	public void unhappyPathTestEmptyChallenge() {
		unhappyPathTest("data.challenge is not a valid base64 encoded without padding string");
	}

	protected void unhappyPathTest(String errorMessage) {
		ExtractFidoRegistrationOptionsChallenge condition = new ExtractFidoRegistrationOptionsChallenge();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(errorMessage));
	}
}
