package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddValidBusinessEntityIdentificationTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		environment.putObject("config", new JsonObject());
	}

	@Test
	public void testHappyPath() {
		AddValidBusinessEntityIdentification cond = new AddValidBusinessEntityIdentification();

		run(cond);

		assertEquals(AddValidBusinessEntityIdentification.VALID_BUSINESS_IDENTIFICAITON, environment.getString("config", "resource.businessEntityIdentification"));
	}
}
