package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GetEnrollmentsOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/enrollments/GetEnrollmentsResponse.json")
	public void happyPathTest200() {
		happyPathTest(200);
	}

	@Test
	@UseResurce("jsonResponses/enrollments/GenericErrorResponse.json")
	public void happyPathTest403() {
		happyPathTest(403);
	}

	@Test
	@UseResurce("jsonResponses/enrollments/GetEnrollmentsDebtorAccCACCMissingIssuer.json")
	public void unhappyPathTestDebtorAccountIssuerConstraint() {
		unhappyPathTest("issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/GetEnrollmentsAuthorisedWithoutDebtorAcc.json")
	public void unhappyPathTestDebtorAccountConstraint() {
		unhappyPathTest("debtorAccount is required when status is [AWAITING_ENROLLMENT, AUTHORISED, REVOKED]");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/GetEnrollmentsAuthorisedWithoutDailyLimit.json")
	public void unhappyPathTestDailyLimitConstraint() {
		unhappyPathTest("dailyLimit is required when status is [AWAITING_ENROLLMENT, AUTHORISED]");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/GetEnrollmentsAuthorisedWithoutTransactionLimit.json")
	public void unhappyPathTestTransactionLimitConstraint() {
		unhappyPathTest("transactionLimit is required when status is [AWAITING_ENROLLMENT, AUTHORISED]");
	}

	protected void happyPathTest(int status) {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", status);
		run(new GetEnrollmentsOASValidatorV1());
	}

	protected void unhappyPathTest(String message) {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
		ConditionError error = runAndFail(new GetEnrollmentsOASValidatorV1());
		assertTrue(error.getMessage().contains(message));
	}
}
