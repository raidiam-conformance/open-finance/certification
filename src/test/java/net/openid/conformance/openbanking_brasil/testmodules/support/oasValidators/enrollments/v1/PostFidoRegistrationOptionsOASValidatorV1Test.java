package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostFidoRegistrationOptionsOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsExtensionsStrings.json")
	public void happyPathTest201() {
		happyPathTest(201);
	}

	@Test
	@UseResurce("jsonResponses/enrollments/GenericEnrollment422Response.json")
	public void happyPathTest422() {
		happyPathTest(422);
	}

	@Test(expected = AssertionError.class)
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsAllExtensions.json")
	public void unhappyPathTestExtensionsWithNonStrings() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 201);
		run(new PostFidoRegistrationOptionsOASValidatorV1());
	}

	protected void happyPathTest(int status) {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", status);
		run(new PostFidoRegistrationOptionsOASValidatorV1());
	}
}
