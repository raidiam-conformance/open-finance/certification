package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revocationReason.EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoRecebedor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revocationReason.EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureRecurringPaymentsConsentsRevocationReasonTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonResponses/automaticpayments/PostRecurringConsents";
	private static final String MISSING_FIELD_MESSAGE = "Unable to find element data.revocation.reason.code in the response payload";

	@Test
	@UseResurce(BASE_FILE_PATH + "RevokedFromIniciadoraRevokedByUsuario.json")
	public void happyPathTestUsuario() {
		EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario condition = new EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario();
		run(condition);
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "RevokedFromDetentoraRevokedByDetentora.json")
	public void happyPathTestRecebedor() {
		EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoRecebedor condition = new EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoRecebedor();
		run(condition);
	}

	@Test
	public void unhappyPathMissingResponse() {
		EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario condition = new EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	public void unhappyPathMissingData() {
		environment.putObject(EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario.RESPONSE_ENV_KEY, new JsonObject());
		EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario condition = new EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRevocationField.json")
	public void unhappyPathMissingRevocation() {
		EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario condition = new EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRevocationReasonField.json")
	public void unhappyPathMissingReason() {
		EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario condition = new EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRevocationReasonCodeField.json")
	public void unhappyPathMissingCode() {
		EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario condition = new EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "RevokedFromDetentoraRevokedByDetentora.json")
	public void unhappyPathRejectedByNotMatchingExpected() {
		EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario condition = new EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "revocation reason returned in the response does not match the expected revocation reason";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
