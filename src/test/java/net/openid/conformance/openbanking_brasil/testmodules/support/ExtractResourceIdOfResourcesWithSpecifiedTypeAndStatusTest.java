package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatusTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3MultipleResources.json")
	public void happyPathTestEmptyExtractedResourceIds() {
		happyPathTest();
		JsonObject extractedResourceIdsObj = environment.getObject("extracted_resource_id");
		JsonArray extractedResourceIds = extractedResourceIdsObj.getAsJsonArray("extractedResourceIds");
		List<String> expectedElements = Arrays.asList("1", "3");
		assertTrue(containsExactlyElements(extractedResourceIds, expectedElements));
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3MultipleResources.json")
	public void happyPathTestWithExtractedResourceIds() {
		JsonArray extractedResourceIds = new JsonArray();
		extractedResourceIds.add("0");
		JsonObject extractedResourceIdsObj = new JsonObjectBuilder().addField("extractedResourceIds", extractedResourceIds).build();
		environment.putObject("extracted_resource_id", extractedResourceIdsObj);
		happyPathTest();
		List<String> expectedElements = Arrays.asList("0", "1", "3");
		assertTrue(containsExactlyElements(extractedResourceIds, expectedElements));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResponseEnvKey() {
		environment.putString("resource_type", "ACCOUNT");
		environment.putString("resource_status", "AVAILABLE");
		run(new ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus());
	}

	@Test(expected = AssertionError.class)
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3MultipleResources.json")
	public void unhappyPathTestMissingResourceType() {
		environment.putString("resource_status", "AVAILABLE");
		run(new ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus());
	}

	@Test(expected = AssertionError.class)
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3MultipleResources.json")
	public void unhappyPathTestMissingResourceStatus() {
		environment.putString("resource_type", "ACCOUNT");
		run(new ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus());
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus.RESPONSE_ENV_KEY,
			new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3MissingResourceId.json")
	public void unhappyPathTestMissingResourceId() {
		unhappyPathTest("Extracted resource does not have resourceId");
	}

	private boolean containsExactlyElements(JsonArray jsonArray, List<String> expectedElements) {
		if (jsonArray.size() != expectedElements.size()) {
			return false;
		}

		for (JsonElement jsonElement : jsonArray) {
			if (!expectedElements.contains(OIDFJSON.getString(jsonElement))) {
				return false;
			}
		}

		return true;
	}

	private void happyPathTest() {
		environment.putString("resource_type", "ACCOUNT");
		environment.putString("resource_status", "AVAILABLE");
		run(new ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus());
	}

	private void unhappyPathTest(String message) {
		environment.putString("resource_type", "ACCOUNT");
		environment.putString("resource_status", "AVAILABLE");
		ConditionError error = runAndFail(new ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus());
		assertTrue(error.getMessage().contains(message));
	}
}
