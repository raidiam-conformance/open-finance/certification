package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.setConfigurationFieldsForRetryTest;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.AbstractSetConfigurationFieldsForRetryTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.SetConfigurationFieldsForIntradayOnePaymentRetryTest;

public class SetConfigurationFieldsForIntradayOnePaymentRetryTestTest extends AbstractSetConfigurationFieldsForRetryTestTest {

	@Override
	protected AbstractSetConfigurationFieldsForRetryTest condition() {
		return new SetConfigurationFieldsForIntradayOnePaymentRetryTest();
	}

	@Override
	protected String expectedPrefix() {
		return "intradayOnePayment";
	}
}
