package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FAPIBrazilAddEnrollmentIdToClientScopeTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		environment.putString("client", "scope", "openid nrp-consents payments");
		environment.putString("enrollment_id", "urn:abc:123");
		FAPIBrazilAddEnrollmentIdToClientScope condition = new FAPIBrazilAddEnrollmentIdToClientScope();
		run(condition);
		String scope = OIDFJSON.getString(environment.getElementFromObject("client", "scope"));
		assertTrue(scope.endsWith("enrollment:urn:abc:123"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingClient() {
		environment.putString("enrollment_id", "urn:abc:123");
		FAPIBrazilAddEnrollmentIdToClientScope condition = new FAPIBrazilAddEnrollmentIdToClientScope();
		run(condition);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingEnrollmentId() {
		environment.putString("client", "scope", "openid nrp-consents payments");
		FAPIBrazilAddEnrollmentIdToClientScope condition = new FAPIBrazilAddEnrollmentIdToClientScope();
		run(condition);
	}

	@Test
	public void unhappyPathTestMissingScope() {
		environment.putString("enrollment_id", "urn:abc:123");
		environment.putObject("client", new JsonObject());
		FAPIBrazilAddEnrollmentIdToClientScope condition = new FAPIBrazilAddEnrollmentIdToClientScope();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("scope missing in client object"));
	}
}
