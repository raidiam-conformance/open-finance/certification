package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.MoveStartDateTimeFromSweepingToRoot;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MoveStartDateTimeFromSweepingToRootTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String KEY = "config";
	protected static final String PATH = "resource.brazilPaymentConsent";
	protected static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/consents/v2/brazilPaymentConsent";
	protected static final String START_DATE_TIME = "2021-05-21T08:30:00Z";

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithSweepingFieldV2.json", key = KEY, path = PATH)
	public void happyPathTestSweeping() {
		run(new MoveStartDateTimeFromSweepingToRoot());

		JsonObject data = environment.getElementFromObject(KEY, PATH + ".data").getAsJsonObject();
		assertTrue(data.has("startDateTime"));
		String startDateTime = OIDFJSON.getString(data.get("startDateTime"));
		assertEquals(START_DATE_TIME, startDateTime);

		JsonObject sweeping = data.getAsJsonObject("recurringConfiguration").getAsJsonObject("sweeping");
		assertFalse(sweeping.has("startDateTime"));
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithAutomaticFieldV2.json", key = KEY, path = PATH)
	public void happyPathTestNotSweeping() {
		run(new MoveStartDateTimeFromSweepingToRoot());

		JsonObject data = environment.getElementFromObject(KEY, PATH + ".data").getAsJsonObject();
		assertFalse(data.has("startDateTime"));
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithSweepingFieldMissingStartDateTimeV2.json", key = KEY, path = PATH)
	public void happyPathTestSweepingMissingStartDateTime() {
		run(new MoveStartDateTimeFromSweepingToRoot());

		JsonObject data = environment.getElementFromObject(KEY, PATH + ".data").getAsJsonObject();
		assertFalse(data.has("startDateTime"));

		JsonObject sweeping = data.getAsJsonObject("recurringConfiguration").getAsJsonObject("sweeping");
		assertFalse(sweeping.has("startDateTime"));
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new MoveStartDateTimeFromSweepingToRoot());
		assertTrue(error.getMessage().contains("Unable to find data field in consents payload"));
	}
}
