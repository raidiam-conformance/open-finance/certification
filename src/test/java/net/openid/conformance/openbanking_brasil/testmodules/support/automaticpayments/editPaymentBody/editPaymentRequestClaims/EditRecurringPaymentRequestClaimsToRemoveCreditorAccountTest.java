package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.editPaymentRequestClaims;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims.EditRecurringPaymentRequestClaimsToRemoveCreditorAccount;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentRequestClaimsToRemoveCreditorAccountTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "resource_request_entity_claims";

	@Test
	@UseResurce(value = "jsonRequests/automaticPayments/payments/v2/PostRecurringPaymentsPixRequestBody.json", key = KEY)
	public void happyPathTest() {
		run(new EditRecurringPaymentRequestClaimsToRemoveCreditorAccount());
		JsonObject data = environment.getElementFromObject(KEY, "data").getAsJsonObject();
		assertFalse(data.has("creditorAccount"));
	}

	@Test
	public void unhappyPathTest() {
		environment.putObject(KEY, new JsonObject());
		ConditionError error = runAndFail(new EditRecurringPaymentRequestClaimsToRemoveCreditorAccount());
		assertTrue(error.getMessage().contains("Unable to find data in recurring-payments request payload"));
	}
}
