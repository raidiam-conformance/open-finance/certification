package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InjectQRCodeWithTransactionIdentifierIntoConfigTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="test_resource_payments_v4_single.json", key="resource")
	public void happyPathSingleTest() {
		InjectQRCodeWithTransactionIdentifierIntoConfig condition = new InjectQRCodeWithTransactionIdentifierIntoConfig();
		run(condition);

		String consentQrCode = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.qrCode"));
		String paymentQrCode = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.qrCode"));

		assertNotNull(consentQrCode);
		assertNotNull(paymentQrCode);
		assertEquals(consentQrCode, paymentQrCode);
	}

	@Test
	@UseResurce(value="test_resource_payments_v4.json", key="resource")
	public void happyPathArrayTest(){
		InjectQRCodeWithTransactionIdentifierIntoConfig condition = new InjectQRCodeWithTransactionIdentifierIntoConfig();
		run(condition);

		String consentQrCode = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.qrCode"));
		assertNotNull(consentQrCode);


		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");

		for (int i = 0 ; i < data.size(); i++) {
			String paymentQrCode = OIDFJSON.getString(data.get(i).getAsJsonObject().get("qrCode"));
			assertNotNull(paymentQrCode);
			assertEquals(consentQrCode, paymentQrCode);
		}
	}

}
