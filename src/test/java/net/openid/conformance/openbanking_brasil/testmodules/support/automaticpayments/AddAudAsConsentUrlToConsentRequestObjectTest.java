package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddAudAsConsentUrlToConsentRequestObject;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddAudAsConsentUrlToConsentRequestObjectTest extends AbstractJsonResponseConditionUnitTest {

	private static final String CONSENT_URL = "https://www.example.com/open-banking/automatic-payments/v1/recurring-consents/urn:example:123";

	@Test
	public void happyPathTest() {
		environment.putObject("consent_endpoint_request", new JsonObject());
		environment.putString("consent_url", CONSENT_URL);
		AddAudAsConsentUrlToConsentRequestObject condition = new AddAudAsConsentUrlToConsentRequestObject();
		run(condition);

		JsonObject request = environment.getObject("consent_endpoint_request");
		assertTrue(
			request.has("aud") &&
			request.get("aud").isJsonPrimitive() &&
			request.get("aud").getAsJsonPrimitive().isString()
		);
		assertEquals(CONSENT_URL, OIDFJSON.getString(request.get("aud")));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingConsentUrl() {
		environment.putObject("consent_endpoint_request", new JsonObject());
		AddAudAsConsentUrlToConsentRequestObject condition = new AddAudAsConsentUrlToConsentRequestObject();
		run(condition);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingConsentEndpointRequest() {
		environment.putString("consent_url", CONSENT_URL);
		AddAudAsConsentUrlToConsentRequestObject condition = new AddAudAsConsentUrlToConsentRequestObject();
		run(condition);
	}
}
