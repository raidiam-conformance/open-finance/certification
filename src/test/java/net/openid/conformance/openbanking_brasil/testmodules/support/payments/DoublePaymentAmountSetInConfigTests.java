package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.DoublePaymentAmountSetInConfig;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

public class DoublePaymentAmountSetInConfigTests extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="test_config_resource.json", key="resource")
	public void happyPathObject(){
		DoublePaymentAmountSetInConfig condition = new DoublePaymentAmountSetInConfig();
		run(condition);

		String newAmount = OIDFJSON.getString( environment.getObject("resource").getAsJsonObject("brazilPaymentConsent").getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		Assert.assertEquals(newAmount, "200000.24");
	}

	@Test
	@UseResurce(value="test_config_resource_arrays.json", key="resource")
	public void happyPathArray(){
		DoublePaymentAmountSetInConfig condition = new DoublePaymentAmountSetInConfig();
		run(condition);

		String newAmount = OIDFJSON.getString( environment.getObject("resource").getAsJsonObject("brazilPaymentConsent").getAsJsonArray("data").get(0).getAsJsonObject().getAsJsonObject("payment").get("amount"));
		Assert.assertEquals(newAmount, "200000.26");
	}

	@Test
	@UseResurce(value="test_config_resource_object_array.json", key="resource")
	//This test has an object at consent, but array at pixPayment
	public void happyPathMixObjectAndArray(){
		DoublePaymentAmountSetInConfig condition = new DoublePaymentAmountSetInConfig();
		run(condition);

		String newAmount = OIDFJSON.getString( environment.getObject("resource").getAsJsonObject("brazilPaymentConsent").getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		Assert.assertEquals(newAmount, "200000.26");
	}

	@Test
	@UseResurce(value="test_config_resource_array_object.json", key="resource")
	//This test has an object at consent, but array at pixPayment
	public void happyPathMixArrayAndObject(){
		DoublePaymentAmountSetInConfig condition = new DoublePaymentAmountSetInConfig();
		run(condition);

		String newAmount = OIDFJSON.getString( environment.getObject("resource").getAsJsonObject("brazilPaymentConsent").getAsJsonArray("data").get(0).getAsJsonObject().getAsJsonObject("payment").get("amount"));
		Assert.assertEquals(newAmount, "200000.26");
	}
}
