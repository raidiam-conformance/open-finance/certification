package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreatePatchConsentsRequestPayloadTest extends AbstractJsonResponseConditionUnitTest {

	private static final String IDENTIFICATION = "11111111111";

	@Test
	public void happyPathTest() {
		environment.putString("config", "resource.loggedUserIdentification", IDENTIFICATION);

		CreatePatchConsentsRequestPayload condition = new CreatePatchConsentsRequestPayload();
		run(condition);

		assertEquals(expectedBody(), environment.getObject("consent_endpoint_request"));
	}

	protected JsonObject expectedBody() {
		return new JsonObjectBuilder()
			.addField("data.status", "CANC")
			.addFields("data.cancellation.cancelledBy.document", Map.of(
				"identification", IDENTIFICATION,
				"rel", "CPF"
			))
			.build();
	}
}
