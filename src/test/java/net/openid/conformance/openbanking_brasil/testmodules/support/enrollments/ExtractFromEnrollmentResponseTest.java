package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.AbstractExtractFromEnrollmentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.ExtractDailyLimitFromEnrollmentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.ExtractTransactionLimitFromEnrollmentResponse;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtractFromEnrollmentResponseTest extends AbstractJsonResponseConditionUnitTest {

	private static final String LIMIT = "100000.12";

	private class ExtractExampleLimitFromEnrollmentResponse extends AbstractExtractFromEnrollmentResponse {

		@Override
		protected String fieldName() {
			return "example";
		}

		@Override
		protected String envVarName() {
			return "example";
		}
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getEnrollments.json")
	public void happyPathTestDailyLimit() {
		ExtractDailyLimitFromEnrollmentResponse condition = new ExtractDailyLimitFromEnrollmentResponse();
		run(condition);
		assertEquals(LIMIT, environment.getString("daily_limit"));
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getEnrollments.json")
	public void happyPathTestTransactionLimit() {
		ExtractTransactionLimitFromEnrollmentResponse condition = new ExtractTransactionLimitFromEnrollmentResponse();
		run(condition);
		assertEquals(LIMIT, environment.getString("transaction_limit"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResponse() {
		ExtractExampleLimitFromEnrollmentResponse condition = new ExtractExampleLimitFromEnrollmentResponse();
		run(condition);
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(AbstractExtractFromEnrollmentResponse.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestInvalidJwt() {
		environment.putObject(
			AbstractExtractFromEnrollmentResponse.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya1.b2.c3").build()
		);
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(
			AbstractExtractFromEnrollmentResponse.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build()
		);
		unhappyPathTest("Could not find data.example in enrollments response");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getEnrollments.json")
	public void unhappyPathTestMissingField() {
		unhappyPathTest("Could not find data.example in enrollments response");
	}

	private void unhappyPathTest(String errorMessage) {
		ExtractExampleLimitFromEnrollmentResponse condition = new ExtractExampleLimitFromEnrollmentResponse();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(errorMessage));
	}
}
