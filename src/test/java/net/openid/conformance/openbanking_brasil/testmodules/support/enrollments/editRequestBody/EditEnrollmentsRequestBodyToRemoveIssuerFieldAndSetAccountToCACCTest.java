package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACCTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="jsonRequests/payments/enrollments/postEnrollments.json", key="resource_request_entity_claims")
	public void happyPathTest() {
		run(new EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACC());
		JsonObject debtorAccount = environment
			.getElementFromObject("resource_request_entity_claims", "data.debtorAccount")
			.getAsJsonObject();
		assertFalse(debtorAccount.has("issuer"));
		assertEquals("CACC", OIDFJSON.getString(debtorAccount.get("accountType")));
	}

	@Test
	@UseResurce(value="jsonRequests/payments/enrollments/postEnrollmentsNoDebtorAccount.json", key="resource_request_entity_claims")
	public void unhappyPathTestMissingDebtorAccount() {
		ConditionError error = runAndFail(new EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACC());
		assertTrue(error.getMessage().contains("Could not find debtorAccount inside the body"));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource_request_entity_claims", new JsonObject());
		ConditionError error = runAndFail(new EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACC());
		assertTrue(error.getMessage().contains("Could not find debtorAccount inside the body"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResourceRequestEntityClaims() {
		run(new EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACC());
	}
}
