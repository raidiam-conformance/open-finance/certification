package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.setConfigurationFieldsForRetryTest;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.AbstractSetConfigurationFieldsForRetryTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.SetConfigurationFieldsForIntradayTwoPaymentsRetryTest;

public class SetConfigurationFieldsForIntradayTwoPaymentsRetryTestTest extends AbstractSetConfigurationFieldsForRetryTestTest {

	@Override
	protected AbstractSetConfigurationFieldsForRetryTest condition() {
		return new SetConfigurationFieldsForIntradayTwoPaymentsRetryTest();
	}

	@Override
	protected String expectedPrefix() {
		return "intradayTwoPayments";
	}
}
