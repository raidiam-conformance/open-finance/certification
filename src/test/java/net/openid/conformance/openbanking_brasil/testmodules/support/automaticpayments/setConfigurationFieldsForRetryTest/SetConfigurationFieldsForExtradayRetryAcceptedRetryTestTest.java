package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.setConfigurationFieldsForRetryTest;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.AbstractSetConfigurationFieldsForRetryTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.SetConfigurationFieldsForExtradayRetryAcceptedRetryTest;

public class SetConfigurationFieldsForExtradayRetryAcceptedRetryTestTest extends AbstractSetConfigurationFieldsForRetryTestTest {

	@Override
	protected AbstractSetConfigurationFieldsForRetryTest condition() {
		return new SetConfigurationFieldsForExtradayRetryAcceptedRetryTest();
	}

	@Override
	protected String expectedPrefix() {
		return "extradayRetryAccepted";
	}
}
