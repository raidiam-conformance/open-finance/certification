package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.junit.Test;
import org.springframework.http.HttpStatus;

public class EnsureResourceResponseCodeWas204or422Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureResourceResponseCodeWas204or422();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.NO_CONTENT.value();
	}

	protected int happyPathResponseCodeOtherPossibility() {
		return HttpStatus.UNPROCESSABLE_ENTITY.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.OK.value();
	}

	@Test
	public void happyPathOtherPossibility(){
		AbstractEnsureResponseCodeWas condition = condition();
		setStatus(happyPathResponseCodeOtherPossibility());
		run(condition);
	}

}
