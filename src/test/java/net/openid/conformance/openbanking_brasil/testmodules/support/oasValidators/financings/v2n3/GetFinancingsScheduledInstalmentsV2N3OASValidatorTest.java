package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetFinancingsScheduledInstalmentsV2N3OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/financings/v2.1.0/instalments.json")
	public void happyPath() {
		run(new GetFinancingsScheduledInstalmentsV2n3OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/financings/v2.1.0/instalmentsNoTotalNumberOfInstalments.json")
	public void unhappyPathNoTotalNumberOfInstalments() {
		ConditionError e = runAndFail(new GetFinancingsScheduledInstalmentsV2n3OASValidator());
		assertThat(e.getMessage(), containsString("totalNumberOfInstalments is required when typeNumberOfInstalments is [DIA, SEMANA, MES, ANO]"));
	}

}
