package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;


public class EnsurePaymentConsentStatusWasPartiallyAcceptedTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentValidatorV4ResponsePartiallyAccepted.json")
	public void validateStructurePostPaymentsConsentPartiallyAccepted() {
		EnsurePaymentConsentStatusWasPartiallyAccepted condition = new EnsurePaymentConsentStatusWasPartiallyAccepted();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentValidatorV4ResponseOK.json")
	public void validateStructurePostPaymentsConsentWrongState() {
		EnsurePaymentConsentStatusWasPartiallyAccepted condition = new EnsurePaymentConsentStatusWasPartiallyAccepted();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Consent not in the expected state"));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/cardIdentification/cardIdentificationResponse.json")
	public void validateStructurePostPaymentsConsentMissingField() {
		EnsurePaymentConsentStatusWasPartiallyAccepted condition = new EnsurePaymentConsentStatusWasPartiallyAccepted();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Body does not have data.status field"));
	}

}
