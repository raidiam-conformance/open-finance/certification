package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class PersonalQualificationResponseOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/naturalPersonQualificationV2/naturalPersonQualificationResponse-V2.1.0.json")
	public void testHappyPath() {
		PersonalQualificationResponseOASValidatorV2 cond = new PersonalQualificationResponseOASValidatorV2();
		run(cond);
	}
}
