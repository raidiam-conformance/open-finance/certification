package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class CreditDiscountedCreditRightsSelectorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/creditOperations/loans/loansResponse.json")
	public void testHappyPath() {
		CreditDiscountedCreditRightsSelector condition = new CreditDiscountedCreditRightsSelector();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentEmptyDataGood.json")
	public void testUnhappyPathNoData() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		CreditDiscountedCreditRightsSelector condition = new CreditDiscountedCreditRightsSelector();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Data field is empty, no further processing required."));
	}

	@Test
	public void testUnhappyPathNoBody() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		CreditDiscountedCreditRightsSelector condition = new CreditDiscountedCreditRightsSelector();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}
}
