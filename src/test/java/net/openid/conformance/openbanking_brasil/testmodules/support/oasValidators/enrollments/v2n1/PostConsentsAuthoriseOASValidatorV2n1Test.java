package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.AbstractEnrollmentsOASV2ValidatorsTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.AbstractEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostConsentsAuthoriseOASValidatorV2;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostConsentsAuthoriseOASValidatorV2n1Test extends AbstractEnrollmentsOASV2n1ValidatorsTest {

	@Override
	protected AbstractEnrollmentsOASValidatorV2n1 validator() {
		return new PostConsentsAuthoriseOASValidatorV2n1();
	}

	@Test
	@UseResurce(JSON_PATH + "GenericErrorResponse.json")
	public void happyPathTest401() {
		happyPathTest(401);
	}
}
