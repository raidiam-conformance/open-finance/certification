package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRevocationRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedByEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedFromEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevocationReasonEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsStatusEnum;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreatePatchRecurringPaymentsConsentsForRevocationRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value= "jsonRequests/automaticPayments/payments/manualRiskSignals.json", key="resource", path="riskSignals")
	public void happyPathTest() {
		CreatePatchRecurringPaymentsConsentsForRevocationRequestBody condition = new CreatePatchRecurringPaymentsConsentsForRevocationRequestBody();
		run(condition);

		JsonObject consentEndpointRequest = environment.getObject("consent_endpoint_request");

		JsonObject data = assertObjectFieldIsPresent(consentEndpointRequest, "data");

		String status = assertStringFieldIsPresent(data, "status");
		assertTrue(RecurringPaymentsConsentsStatusEnum.toSet().contains(status));

		JsonObject revocation = assertObjectFieldIsPresent(data, "revocation");

		String revokedBy = assertStringFieldIsPresent(revocation, "revokedBy");
		assertTrue(RecurringPaymentsConsentsRevokedByEnum.toSet().contains(revokedBy));

		String revokedFrom = assertStringFieldIsPresent(revocation, "revokedFrom");
		assertTrue(RecurringPaymentsConsentsRevokedFromEnum.toSet().contains(revokedFrom));

		JsonObject reason = assertObjectFieldIsPresent(revocation, "reason");

		String code = assertStringFieldIsPresent(reason, "code");
		assertTrue(RecurringPaymentsConsentsRevocationReasonEnum.toSet().contains(code));

		assertStringFieldIsPresent(reason, "detail");

		JsonObject riskSignals = assertObjectFieldIsPresent(data, "riskSignals");
		JsonObject correctRiskSignals = environment
			.getElementFromObject("resource", "riskSignals.manual")
			.getAsJsonObject();
		assertEquals(riskSignals, correctRiskSignals);
	}

	private String assertStringFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonPrimitive() && obj.get(field).getAsJsonPrimitive().isString());
		return OIDFJSON.getString(obj.get(field));
	}

	private JsonObject assertObjectFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonObject());
		return obj.getAsJsonObject(field);
	}
}
