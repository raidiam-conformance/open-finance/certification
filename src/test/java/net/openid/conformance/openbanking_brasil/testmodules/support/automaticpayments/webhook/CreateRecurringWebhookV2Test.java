package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.webhook;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringConsentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringPaymentWebhookV2;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateRecurringWebhookV2Test extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_URL = "https://www.example.com";
	private static final String BASE_URL_WITH_SUFFIX = BASE_URL + "/open-banking/webhook/v1/automatic-payments/v2/";
	private static final String CONSENT_URL = BASE_URL_WITH_SUFFIX + "recurring-consents/";
	private static final String PAYMENT_URL = BASE_URL_WITH_SUFFIX + "pix/recurring-payments/";

	@Test
	public void happyPathTestConsent() {
		environment.putString("base_url", BASE_URL);
		CreateRecurringConsentWebhookV2 condition = new CreateRecurringConsentWebhookV2();
		run(condition);
		assertEquals(CONSENT_URL, environment.getString("webhook_uri_consent_id"));
	}

	@Test
	public void happyPathTestPayment() {
		environment.putString("base_url", BASE_URL);
		CreateRecurringPaymentWebhookV2 condition = new CreateRecurringPaymentWebhookV2();
		run(condition);
		assertEquals(PAYMENT_URL, environment.getString("webhook_uri_payment_id"));
	}
}
