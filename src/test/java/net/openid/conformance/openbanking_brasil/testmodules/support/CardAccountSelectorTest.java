package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class CardAccountSelectorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("massadedados/F2_Cartão de Crédito/get-credit-cards-accounts-200-1.json")
	public void testHappyPath() {
		CardAccountSelector condition = new CardAccountSelector();
		run(condition);
		assertThat(environment.getString("accountId"), Matchers.is("PIITR3459087"));
	}

	@Test(expected= NullPointerException.class)
	@UseResurce("jsonResponses/account/accountV2/listV2/accountListResponse.json")
	public void testUnhappyPathNoCreditCardAccountId() {
		CardAccountSelector condition = new CardAccountSelector();
		runAndFail(condition);
	}

	@Test
	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentEmptyDataGood.json")
	public void testUnhappyPathNoData() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		CardAccountSelector condition = new CardAccountSelector();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Data field is empty, no further processing required"));
	}

	@Test
	public void testUnhappyPathNoBody() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		CardAccountSelector condition = new CardAccountSelector();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}
}
