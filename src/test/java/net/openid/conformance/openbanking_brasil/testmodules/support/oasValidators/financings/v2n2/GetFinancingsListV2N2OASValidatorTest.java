package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetFinancingsListV2N2OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/financings/v2.1.0/list.json")
	public void happyPath() {
		run(new GetFinancingsListV2n2OASValidator());
	}


}

