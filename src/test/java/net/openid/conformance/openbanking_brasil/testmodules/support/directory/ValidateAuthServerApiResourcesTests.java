package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class ValidateAuthServerApiResourcesTests extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		JsonObject config = new JsonObject();
		environment.putObject("config", config);
		JsonObject resource = new JsonObject();
		resource.addProperty("consentUrl", "https://my-super-secure-bank/open-banking/payments/v4/consents");
		environment.getObject("config").add("resource", resource);
		JsonObject server = new JsonObject();
		server.addProperty("token_endpoint", "mockValue");
		server.addProperty("registration_endpoint", "mockValue2");
		environment.putObject("server", server);
	}

	public JsonObject createApiEndpoint(String apiDiscoveryId, String apiEndpoint) {
		JsonObject apiDiscoveryEndpoint = new JsonObject();
		apiDiscoveryEndpoint.addProperty("ApiDiscoveryId", apiDiscoveryId);
		apiDiscoveryEndpoint.addProperty("ApiEndpoint", apiEndpoint);
		return apiDiscoveryEndpoint;
	}

	public JsonObject createApiResource(String apiResourceId, String apiVersion, boolean familyComplete, String apiCertificationUri, String certificationStatus, String certificationStartDate, String certificationExpirationDate, String apiFamilyType, List<JsonObject> apiDiscoveryEndpoints) {
		JsonObject apiResource = new JsonObject();
		apiResource.addProperty("ApiResourceId", apiResourceId);
		apiResource.addProperty("ApiVersion", apiVersion);
		apiResource.addProperty("FamilyComplete", familyComplete);
		apiResource.addProperty("ApiCertificationUri", apiCertificationUri);
		apiResource.addProperty("CertificationStatus", certificationStatus);
		apiResource.addProperty("CertificationStartDate", certificationStartDate);
		apiResource.addProperty("CertificationExpirationDate", certificationExpirationDate);
		apiResource.addProperty("ApiFamilyType", apiFamilyType);

		JsonArray apiDiscoveryEndpointsArray = new JsonArray();
		for (JsonObject apiDiscoveryEndpoint : apiDiscoveryEndpoints) {
			apiDiscoveryEndpointsArray.add(apiDiscoveryEndpoint);
		}
		apiResource.add("ApiDiscoveryEndpoints", apiDiscoveryEndpointsArray);

		return apiResource;
	}

	protected void createAuthServerWithApiResources(JsonArray apiResources) {
		JsonObject authServer = new JsonObject();
		authServer.add("ApiResources", apiResources);
		environment.putObject("authorisation_server", authServer);
	}


	@Test
	public void happyPath() {
		JsonObject apiResource = createApiResource("326d1950-c031-4dbb-be6a-1618128421e8", "2.1.0", true, "https://github.com/OpenBanking-Brasil/conformance/blob/main/submissions/functional/consents/2.1.0/01027058_-Cielo-Open-Banking-1.0.0_consents_v2.1_08-12-2023.json", "Self-Certified", "08/12/2023", null, "consents", List.of(createApiEndpoint("83a4c8ce-b78e-4ab4-9d81-19fd79b367b8", "https://od1.openbanking-hml.cds.com.br/open-banking/consents/v1/consents")));
		JsonArray apiResources = new JsonArray();
		apiResources.add(apiResource);
		createAuthServerWithApiResources(apiResources);
		ValidateAuthServerApiResources condition = new ValidateAuthServerApiResources();
		run(condition);
	}

	@Test
	public void happyPathPlusOne() {
		JsonObject apiResource = createApiResource("326d1950-c031-4dbb-be6a-1618128421e8", "2.1.0", true, "https://github.com/OpenBanking-Brasil/conformance/blob/main/submissions/functional/consents/2.1.0/01027058_-Cielo-Open-Banking-1.0.0_consents_v2.1_08-12-2023.json", "Self-Certified", "10/12/2023", null, "consents", List.of(createApiEndpoint("83a4c8ce-b78e-4ab4-9d81-19fd79b367b8", "https://od1.openbanking-hml.cds.com.br/open-banking/consents/v1/consents")));
		JsonArray apiResources = new JsonArray();
		apiResources.add(apiResource);
		createAuthServerWithApiResources(apiResources);
		ValidateAuthServerApiResources condition = new ValidateAuthServerApiResources();
		run(condition);
	}

	@Test
	public void happyPathBefore() {
		JsonObject apiResource = createApiResource("326d1950-c031-4dbb-be6a-1618128421e8", "2.1.0", true, "https://github.com/OpenBanking-Brasil/conformance/blob/main/submissions/functional/consents/2.1.0/01027058_-Cielo-Open-Banking-1.0.0_consents_v2.1_08-12-2023.json", "Self-Certified", "10/11/2023", null, "consents", List.of(createApiEndpoint("83a4c8ce-b78e-4ab4-9d81-19fd79b367b8", "https://od1.openbanking-hml.cds.com.br/open-banking/consents/v1/consents")));
		JsonArray apiResources = new JsonArray();
		apiResources.add(apiResource);
		createAuthServerWithApiResources(apiResources);
		ValidateAuthServerApiResources condition = new ValidateAuthServerApiResources();
		run(condition);
	}

	@Test
	public void happyPathEqualOneMonth() {
		JsonObject apiResource = createApiResource("326d1950-c031-4dbb-be6a-1618128421e8", "2.1.0", true, "https://github.com/OpenBanking-Brasil/conformance/blob/main/submissions/functional/consents/2.1.0/01027058_-Cielo-Open-Banking-1.0.0_consents_v2.1_08-12-2023.json", "Self-Certified", "08/01/2024", null, "consents", List.of(createApiEndpoint("83a4c8ce-b78e-4ab4-9d81-19fd79b367b8", "https://od1.openbanking-hml.cds.com.br/open-banking/consents/v1/consents")));
		JsonArray apiResources = new JsonArray();
		apiResources.add(apiResource);
		createAuthServerWithApiResources(apiResources);
		ValidateAuthServerApiResources condition = new ValidateAuthServerApiResources();
		run(condition);
	}

	@Test
	public void nullApiResource() {
		JsonArray apiResources = new JsonArray();
		createAuthServerWithApiResources(apiResources);
		ValidateAuthServerApiResources condition = new ValidateAuthServerApiResources();
		run(condition);
		assertNull(environment.getObject("ApiResources"));
	}

	@Test
	public void familyIncomplete() {
		JsonObject apiResource = createApiResource("326d1950-c031-4dbb-be6a-1618128421e8", "2.0.0", false, "https://github.com/OpenBanking-Brasil/conformance/blob/main/submissions/functional/consents/2.1.0/01027058_-Cielo-Open-Banking-1.0.0_consents_v2.1_08-12-2023.json", "Self-Certified", "08/12/2023", null, "consents", List.of(createApiEndpoint("83a4c8ce-b78e-4ab4-9d81-19fd79b367b8", "https://od1.openbanking-hml.cds.com.br/open-banking/consents/v1/consents")));
		JsonArray apiResources = new JsonArray();
		apiResources.add(apiResource);
		createAuthServerWithApiResources(apiResources);
		ValidateAuthServerApiResources condition = new ValidateAuthServerApiResources();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("ApiResource family is not complete"));
	}

	@Test
	public void nullCertStartDate() {
		JsonObject apiResource = createApiResource("326d1950-c031-4dbb-be6a-1618128421e8", "2.1.0", true, "https://github.com/OpenBanking-Brasil/conformance/blob/main/submissions/functional/consents/2.1.0/01027058_-Cielo-Open-Banking-1.0.0_consents_v2.1_08-12-2023.json", "Self-Certified", null, null, "consents", List.of(createApiEndpoint("83a4c8ce-b78e-4ab4-9d81-19fd79b367b8", "https://od1.openbanking-hml.cds.com.br/open-banking/consents/v1/consents")));
		JsonArray apiResources = new JsonArray();
		apiResources.add(apiResource);
		createAuthServerWithApiResources(apiResources);
		ValidateAuthServerApiResources condition = new ValidateAuthServerApiResources();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("CertificationStartDate is null"));
	}



	@Test
	public void wrongCertFamily() {
		JsonObject apiResource = createApiResource("326d1950-c031-4dbb-be6a-1618128421e8", "2.1.0", true, "https://github.com/OpenBanking-Brasil/conformance/blob/main/submissions/functional/payments/2.1.0/01027058_-Cielo-Open-Banking-1.0.0_consents_v2.1_08-12-2023.json", "Self-Certified", "08/12/2023", null, "consents", List.of(createApiEndpoint("83a4c8ce-b78e-4ab4-9d81-19fd79b367b8", "https://od1.openbanking-hml.cds.com.br/open-banking/consents/v1/consents")));
		JsonArray apiResources = new JsonArray();
		apiResources.add(apiResource);
		createAuthServerWithApiResources(apiResources);
		ValidateAuthServerApiResources condition = new ValidateAuthServerApiResources();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("ApiCertificationUri Regex does not match"));
	}


	@Test
	public void wrongCertVersion() {
		JsonObject apiResource = createApiResource("326d1950-c031-4dbb-be6a-1618128421e8", "3.0.0", true, "https://github.com/OpenBanking-Brasil/conformance/blob/main/submissions/functional/consents/2.1.0/01027058_-Cielo-Open-Banking-1.0.0_consents_v2.1_08-12-2023.json", "Self-Certified", "08/12/2023", null, "consents", List.of(createApiEndpoint("83a4c8ce-b78e-4ab4-9d81-19fd79b367b8", "https://od1.openbanking-hml.cds.com.br/open-banking/consents/v1/consents")));
		JsonArray apiResources = new JsonArray();
		apiResources.add(apiResource);
		createAuthServerWithApiResources(apiResources);
		ValidateAuthServerApiResources condition = new ValidateAuthServerApiResources();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("ApiCertificationUri Regex does not match"));
	}
}
