package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureErrorResponseCodeFieldWasStatusConsentimentoInvalidoTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		setJwt(true);
		environment.mapKey("endpoint_response", "resource_endpoint_response_full");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/v2/PostConsentsAuthorise422ResponseStatusConsentimentoInvalido.json")
	public void validateGood422ValorAcimaLimite() {
		run(new EnsureErrorResponseCodeFieldWasStatusConsentimentoInvalido());
	}

	@Test
	@UseResurce("jsonResponses/enrollments/v2/PostConsentsAuthorise422ResponseStatusVinculoInvalido.json")
	public void validateNotFound422ValorAcimaLimite() {
		ConditionError conditionError = runAndFail(new EnsureErrorResponseCodeFieldWasStatusConsentimentoInvalido());
		assertTrue(conditionError.getMessage().contains("Could not find error with expected code in the errors JSON array"));
	}
}
