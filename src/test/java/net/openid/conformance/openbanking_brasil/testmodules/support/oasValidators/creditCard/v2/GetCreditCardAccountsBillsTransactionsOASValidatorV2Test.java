package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GetCreditCardAccountsBillsTransactionsOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/billTransactions.json")
	public void happyPath() {
		run(new GetCreditCardAccountsBillsTransactionsOASValidatorV2());
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/billTransactionsNoAdditionalInfo.json")
	public void noAdditionalInfo() {
		ConditionError e = runAndFail(new GetCreditCardAccountsBillsTransactionsOASValidatorV2());
		assertTrue(e.getMessage().contains("transactionalAdditionalInfo is required when transactionType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/billTransactionsNoChargeIdentificator.json")
	public void noChargeIdentifier() {
		ConditionError e = runAndFail(new GetCreditCardAccountsBillsTransactionsOASValidatorV2());
		assertTrue(e.getMessage().contains("chargeIdentificator is required when paymentType is A_PRAZO"));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/billTransactionsNoChargeNumber.json")
	public void noChargeNumber() {
		ConditionError e = runAndFail(new GetCreditCardAccountsBillsTransactionsOASValidatorV2());
		assertTrue(e.getMessage().contains("chargeNumber is required when paymentType is A_PRAZO"));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/billTransactionsNoFeeType.json")
	public void noFeeType() {
		ConditionError e = runAndFail(new GetCreditCardAccountsBillsTransactionsOASValidatorV2());
		assertTrue(e.getMessage().contains("feeType is required when transactionType is TARIFA"));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/billTransactionsNoFeeTypeAdditionalInfo.json")
	public void noFeeTypeAdditionalInfo() {
		ConditionError e = runAndFail(new GetCreditCardAccountsBillsTransactionsOASValidatorV2());
		assertTrue(e.getMessage().contains("feeTypeAdditionalInfo is required when feeType is OUTRA"));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/billTransactionsNoOtherCreditsAdditionalInfo.json")
	public void noOtherCreditsAdditionalInfo() {
		ConditionError e = runAndFail(new GetCreditCardAccountsBillsTransactionsOASValidatorV2());
		assertTrue(e.getMessage().contains("otherCreditsAdditionalInfo is required when otherCreditsType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/billTransactionsNoOtherCreditsType.json")
	public void noOtherCreditsType() {
		ConditionError e = runAndFail(new GetCreditCardAccountsBillsTransactionsOASValidatorV2());
		assertTrue(e.getMessage().contains("otherCreditsType is required when transactionType is OPERACOES_CREDITO_CONTRATADAS_CARTAO"));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/billTransactionsNoPaymentType.json")
	public void noPaymentType() {
		ConditionError e = runAndFail(new GetCreditCardAccountsBillsTransactionsOASValidatorV2());
		assertTrue(e.getMessage().contains("paymentType is required when transactionType is PAGAMENTO"));
	}
}
