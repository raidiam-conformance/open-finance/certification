package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.common.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetOutagesOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/common/GetOutagesResponse.json")
	public void testHappyPath() {
		GetOutagesOASValidatorV2 cond = new GetOutagesOASValidatorV2();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/common/GetOutagesResponse(missField).json")
	public void testUnhappyPath() {
		GetOutagesOASValidatorV2 cond = new GetOutagesOASValidatorV2();
		runAndFail(cond);
	}
}
