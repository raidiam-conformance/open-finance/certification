package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.consentsRejectionReason;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsConsentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentsConsentsRejectionReasonWasAutenticacaoDivergente;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureRecurringPaymentsConsentsRejectionReasonWasAutenticacaoDivergenteTest extends AbstractEnsureRecurringPaymentsConsentsRejectionReasonTest {

	@Override
	protected AbstractEnsureRecurringPaymentsConsentsRejectionReason condition() {
		return new EnsureRecurringPaymentsConsentsRejectionReasonWasAutenticacaoDivergente();
	}

	@Override
	@Test
	@UseResurce(BASE_FILE_PATH + "RejectionReasonAutenticacaoDivergente.json")
	public void happyPathTest() {
		super.happyPathTest();
	}
}
