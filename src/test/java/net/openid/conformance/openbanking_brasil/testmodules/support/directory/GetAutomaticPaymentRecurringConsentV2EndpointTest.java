package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetAutomaticPaymentRecurringConsentV2EndpointTest extends AbstractGetXFromAuthServerTest {

	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/automatic-payments/v2/recurring-consents";
	}

	@Override
	protected String getApiFamilyType() {
		return "payments-recurring-consents";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return false;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetAutomaticPaymentRecurringConsentV2Endpoint();
	}
}
