package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureResourceResponseCodeWas201Or422Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureResourceResponseCodeWas201Or422();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.CREATED.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.BAD_REQUEST.value();
	}
}
