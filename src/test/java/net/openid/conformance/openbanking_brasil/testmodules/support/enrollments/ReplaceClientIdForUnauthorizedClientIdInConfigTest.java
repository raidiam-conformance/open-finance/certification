package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReplaceClientIdForUnauthorizedClientIdInConfigTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		String originalId = "123";
		String noContractId = "456";
		environment.putObject("client", new JsonObjectBuilder().addField("client_id", originalId).build());
		environment.putString("resource", "noContractClientId", noContractId);

		ReplaceClientIdForUnauthorizedClientIdInConfig condition = new ReplaceClientIdForUnauthorizedClientIdInConfig();
		run(condition);
		String clientId = OIDFJSON.getString(environment.getElementFromObject("client", "client_id"));
		assertEquals(noContractId, clientId);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingClient() {
		environment.putObject("client", new JsonObject());
		ReplaceClientIdForUnauthorizedClientIdInConfig condition = new ReplaceClientIdForUnauthorizedClientIdInConfig();
		run(condition);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResource() {
		environment.putObject("resource", new JsonObject());
		ReplaceClientIdForUnauthorizedClientIdInConfig condition = new ReplaceClientIdForUnauthorizedClientIdInConfig();
		run(condition);
	}

	@Test
	public void unhappyPathTestMissingClientId() {
		environment.putObject("client", new JsonObject());
		environment.putObject("resource", new JsonObject());
		ReplaceClientIdForUnauthorizedClientIdInConfig condition = new ReplaceClientIdForUnauthorizedClientIdInConfig();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("The client_id for no contract test is not present in the resource"));
	}
}
