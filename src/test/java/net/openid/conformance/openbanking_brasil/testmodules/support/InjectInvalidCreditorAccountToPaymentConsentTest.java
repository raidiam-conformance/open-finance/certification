package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectInvalidCreditorAccountToPaymentConsent;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InjectInvalidCreditorAccountToPaymentConsentTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="test_resource_payments_v4_single.json", key="resource")
	public void happyPathSingleTest() {
		InjectInvalidCreditorAccountToPaymentConsent condition = new InjectInvalidCreditorAccountToPaymentConsent();
		run(condition);

		String name = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.creditor").getAsJsonObject().get("name"));
		String cpfCnpj = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.creditor").getAsJsonObject().get("cpfCnpj"));
		String personType = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.creditor").getAsJsonObject().get("personType"));

		assertEquals(name, DictHomologKeys.PROXY_EMAIL_OWNER_NAME);
		assertEquals(cpfCnpj, DictHomologKeys.PROXY_EMAIL_MISMATCHED_CPF);
		assertEquals(personType, DictHomologKeys.PROXY_EMAIL_PERSON_TYPE);

		String issuer = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.creditorAccount").getAsJsonObject().get("issuer"));
		String number = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.creditorAccount").getAsJsonObject().get("number"));
		String accountType = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.creditorAccount").getAsJsonObject().get("accountType"));
		String ispb = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.creditorAccount").getAsJsonObject().get("ispb"));

		assertEquals(issuer, DictHomologKeys.PROXY_EMAIL_BRANCH_NUMBER);
		assertEquals(number, DictHomologKeys.PROXY_EMAIL_ACCOUNT_MISMATCHED_NUMBER);
		assertEquals(accountType, DictHomologKeys.PROXY_EMAIL_ACCOUNT_TYPE);
		assertEquals(ispb, DictHomologKeys.PROXY_EMAIL_MISMATCHED_ISPB);
	}
}
