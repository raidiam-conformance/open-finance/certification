package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GetBankFixedIncomesIdentificationOASValidatorV1n4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/identification.json")
	public void happyPath() {
		run(new GetBankFixedIncomesIdentificationOASValidatorV1n4());
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/identificationNoAdditionalInfo.json")
	public void noAdditionalInfo() {
		ConditionError e = runAndFail(new GetBankFixedIncomesIdentificationOASValidatorV1n4());
		assertTrue(e.getMessage().contains("indexerAdditionalInfo is required when indexer is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/identificationNoIndexerPercentage.json")
	public void noIndexerPercentage() {
		ConditionError e = runAndFail(new GetBankFixedIncomesIdentificationOASValidatorV1n4());
		assertTrue(e.getMessage().contains("postFixedIndexerPercentage is required when indexer is PRE_FIXADO"));

	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/identificationNoPreFixedRate.json")
	public void noPreFixedRate() {
		ConditionError e = runAndFail(new GetBankFixedIncomesIdentificationOASValidatorV1n4());
		assertTrue(e.getMessage().contains("preFixedRate is required when indexer is PRE_FIXADO"));

	}
}
