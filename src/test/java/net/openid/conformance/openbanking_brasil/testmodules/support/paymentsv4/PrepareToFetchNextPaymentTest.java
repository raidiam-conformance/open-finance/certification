package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PrepareToFetchNextPaymentTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_PATH = "environmentVariables/payments/v4/";
	private static final String BASE_URL = "https://www.example.com/payments/v4/pix/payments/";
	private static final String PAYMENT_ID = "TXpRMU9UQTROMWhZV2xSU1FUazJSMDl";

	@Before
	public void init() {
		environment.putString("config", "resource.resourceUrl", BASE_URL);
	}

	@Test
	@UseResurce(value = BASE_PATH + "paymentIdsWith1Element.json", key = "payment_ids")
	public void happyPathTest1PaymentId() {
		PrepareToFetchNextPayment condition = new PrepareToFetchNextPayment();
		run(condition);
		assertEquals(BASE_URL + PAYMENT_ID, environment.getString("protected_resource_url"));
		validateAmountAfterCall(0);
		assertTrue(environment.getBoolean("no_more_payments"));
	}

	@Test
	@UseResurce(value = BASE_PATH + "paymentIdsWithMoreThan1Element.json", key = "payment_ids")
	public void happyPathTestMoreThan1PaymentId() {
		PrepareToFetchNextPayment condition = new PrepareToFetchNextPayment();
		run(condition);
		assertEquals(BASE_URL + PAYMENT_ID, environment.getString("protected_resource_url"));
		validateAmountAfterCall(1);
		assertFalse(environment.getBoolean("no_more_payments"));
	}

	@Test
	public void unhappyPathTestMissingPaymentIds() {
		PrepareToFetchNextPayment condition = new PrepareToFetchNextPayment();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "couldn't find object in environment: payment_ids";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyPathTestEmptyPaymentIds() {
		environment.putObject("payment_ids", new JsonObject());
		environment.getObject("payment_ids").add("paymentIds", new JsonArray());
		PrepareToFetchNextPayment condition = new PrepareToFetchNextPayment();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "Payments array must not be empty";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	protected void validateAmountAfterCall(int expectedAmount) {
		JsonArray paymentIds = environment.getObject("payment_ids").getAsJsonArray("paymentIds");
		assertEquals(expectedAmount, paymentIds.size());
	}
}
