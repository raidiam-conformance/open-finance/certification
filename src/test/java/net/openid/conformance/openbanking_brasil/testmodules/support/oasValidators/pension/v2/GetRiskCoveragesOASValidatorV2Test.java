package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.pension.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

@UseResurce("jsonResponses/opendata/pension/V2/RiskCoveragesResponseV2.json")
public class GetRiskCoveragesOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	public void evaluate() {
		run(new GetRiskCoveragesOASValidatorV2());
	}
}
