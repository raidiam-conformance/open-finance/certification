package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetCreditCardAccountsIdentificationOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/identification.json")
	public void validateStructure() {
		run(new GetCreditCardAccountsIdentificationOASValidatorV2());
	}


}
