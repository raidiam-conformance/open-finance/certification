package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.ConditionError;
import org.junit.Assert;
import org.junit.Test;

public abstract class AbstractEnsureNumberOfTotalRecordsFromMetaTest extends AbstractEnsureNumberOfTotalRecordsTest {


	@Test
	public void missingMeta(){
		environment.putString("resource_endpoint_response_full", "body", new JsonObject().toString());
		ConditionError e = runAndFail(getCondition());
		Assert.assertTrue(e.getMessage().contains("The response does not contain a totalRecords element in meta."));
	}

	@Override
	protected abstract AbstractEnsureNumberOfTotalRecordsFromMeta getCondition();

	@Override
	protected void addTestItems(int items) {
		addMetaToEnv(items);
	}
}
