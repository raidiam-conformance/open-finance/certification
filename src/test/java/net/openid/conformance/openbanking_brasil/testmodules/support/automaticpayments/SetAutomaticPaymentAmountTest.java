package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo1;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo100;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo1AboveMaximumVariableAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo20Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo300;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo30Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo450;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo50;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo50Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountToFirstPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo60Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountToRequestedAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountToUserDefinedAmount;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SetAutomaticPaymentAmountTest extends AbstractJsonResponseConditionUnitTest {


	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest50() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo50();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("50.00", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest100() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo100();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("100.00", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest300() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo300();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("300.00", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest450() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo450();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("450.00", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTestRequestedAmount() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountToRequestedAmount();
		environment.putString("requested_amount","0.15");
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("0.15", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest1() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo1();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("1.00", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest60Cents() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo60Cents();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("0.60", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest50Cents() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo50Cents();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("0.50", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest20Cents() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo20Cents();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("0.20", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest30Cents() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo30Cents();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("0.30", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTestUserDefinedAmount() {
		String paymentAmount = "100.00";
		environment.putString("resource", "recurringPaymentAmount", paymentAmount);
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountToUserDefinedAmount();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals(paymentAmount, amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void unhappyPathTestUserDefinedAmountMissingCents() {
		String paymentAmount = "100";
		environment.putString("resource", "recurringPaymentAmount", paymentAmount);
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountToUserDefinedAmount();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("The value defined for resource.recurringPaymentAmount is not valid"));
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void unhappyPathTestUserDefinedAmountTooBig() {
		String paymentAmount = "10000000000000000.00";
		environment.putString("resource", "recurringPaymentAmount", paymentAmount);
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountToUserDefinedAmount();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("The value defined for resource.recurringPaymentAmount is not valid"));
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void happyPathTest1AboveMaximumVariableAmount() {
		Random random = new Random();
		int value = random.nextInt(100);
		String maximumVariableAmount = String.format("%.2f", value / 100.0);
		String expectedAmount = String.format("%.2f", 1 + value / 100.0);
		environment.putString("maximumVariableAmount", maximumVariableAmount);
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo1AboveMaximumVariableAmount();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals(expectedAmount, amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	@UseResurce(value="environmentVariables/automaticPayments/recurringConfigurationObjectAutomatic.json", key="recurring_configuration_object")
	public void happyPathTestFirstPaymentAmount() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountToFirstPaymentAmount();
		run(condition);
		String amount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals("100000.12", amount);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	public void unhappyPathTestFirstPaymentAmountMissingAutomatic() {
		environment.putObject("recurring_configuration_object", new JsonObject());
		unhappyPathTestFirstPaymentAmount();
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	@UseResurce(value="environmentVariables/automaticPayments/recurringConfigurationObjectAutomaticWithoutFirstPayment.json", key="recurring_configuration_object")
	public void unhappyPathTestFirstPaymentAmountMissingFirstPayment() {
		unhappyPathTestFirstPaymentAmount();
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingAmount.json", key="resource", path="brazilPixPayment")
	@UseResurce(value="environmentVariables/automaticPayments/recurringConfigurationObjectAutomaticWithoutFirstPaymentAmount.json", key="recurring_configuration_object")
	public void unhappyPathTestFirstPaymentAmountMissingFirstPaymentAmount() {
		unhappyPathTestFirstPaymentAmount();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTest100();
		unhappyPathTestRequested();
	}

	protected void unhappyPathTest100() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountTo100();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Unable to find data in payments payload"));
	}

	protected void unhappyPathTestRequested() {
		environment.putString("requested_amount","0.15");
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountToRequestedAmount();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Unable to find data in payments payload"));
	}

	protected void unhappyPathTestFirstPaymentAmount() {
		AbstractSetAutomaticPaymentAmount condition = new SetAutomaticPaymentAmountToFirstPaymentAmount();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Unable to find firstPayment amount in recurring configuration object"));
	}
}
