package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.rejectionReason;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public abstract class AbstractEnsureRecurringPaymentsRejectionReasonTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/payments/PostRecurringPayments201Response";

	protected abstract AbstractEnsureRecurringPaymentsRejectionReason condition();

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject(AbstractEnsureRecurringPaymentsRejectionReason.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putString(AbstractEnsureRecurringPaymentsRejectionReason.RESPONSE_ENV_KEY, "body", "{}");
		unhappyPathTest("Unable to find element data.rejectionReason.code in the response payload");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingOptionalFields.json")
	public void unhappyPathTestMissingRejectionReason() {
		unhappyPathTest("Unable to find element data.rejectionReason.code in the response payload");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRejectionReasonCode.json")
	public void unhappyPathTestMissingRejectionReasonCode() {
		unhappyPathTest("Unable to find element data.rejectionReason.code in the response payload");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}
}
