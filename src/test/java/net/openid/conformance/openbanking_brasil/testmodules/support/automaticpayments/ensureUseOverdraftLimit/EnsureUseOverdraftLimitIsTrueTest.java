package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureUseOverdraftLimit;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit.AbstractEnsureUseOverdraftLimit;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit.EnsureUseOverdraftLimitIsTrue;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureUseOverdraftLimitIsTrueTest extends AbstractEnsureUseOverdraftLimitTest {

	@Override
	protected AbstractEnsureUseOverdraftLimit condition() {
		return new EnsureUseOverdraftLimitIsTrue();
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void happyPathTest() {
		run(condition());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "UseOverdraftLimitFalse200Response.json")
	public void unhappyPathTestMissingUseOverdraftLimitFalse() {
		unhappyPathTest("The useOverdraftLimit value is different from expected");
	}
}
