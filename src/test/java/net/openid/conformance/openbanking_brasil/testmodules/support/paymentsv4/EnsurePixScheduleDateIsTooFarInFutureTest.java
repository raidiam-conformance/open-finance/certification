package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePixScheduleDateIsTooFarInFuture;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class EnsurePixScheduleDateIsTooFarInFutureTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_CONSENTS_PATH_HAPPY = "jsonRequests/payments/consents/paymentConsentRequestArraySingleScheduleDateFarInTheFuture.json";
	private static final String KEY = "consent_endpoint_request";

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY, key = KEY)
	public void EnsureDateIsChangedToTheFutureTest() {

		EnsurePixScheduleDateIsTooFarInFuture condition = new EnsurePixScheduleDateIsTooFarInFuture();

		run(condition);

		String updatedDate = environment.getString(KEY, "data.payment.schedule.single.date");

		String expectedDate = LocalDate.now(ZoneId.of("America/Sao_Paulo")).plusDays(740L).toString();
		assertEquals(expectedDate, updatedDate);

	}
}
