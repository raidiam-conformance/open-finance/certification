package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureTimeIsAfterRequestDateTime.EnsureStatusUpdateDateTimeIsAfterRequestTime;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureStatusUpdateDateTimeIsAfterRequestTimeTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_PATH = "jsonResponses/paymentInitiation/pixByPayments/v4/PatchPixConsent";

	@Test
	@UseResurce(BASE_PATH + "StatusUpdateDateTimeAfterRequestDateTime.json")
	public void happyPathTest() {
		EnsureStatusUpdateDateTimeIsAfterRequestTime condition = new EnsureStatusUpdateDateTimeIsAfterRequestTime();
		run(condition);
	}

	@Test
	@UseResurce(BASE_PATH + "StatusUpdateDateTimeEqualsRequestDateTime.json")
	public void testUpdateAndRequestTimeAreEquals() {
		EnsureStatusUpdateDateTimeIsAfterRequestTime condition = new EnsureStatusUpdateDateTimeIsAfterRequestTime();
		run(condition);
	}

	@Test
	@UseResurce(BASE_PATH + "MissingRequestDateTime.json")
	public void unhappyMissingRequestDateTimeTest() {
		EnsureStatusUpdateDateTimeIsAfterRequestTime condition = new EnsureStatusUpdateDateTimeIsAfterRequestTime();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "It was not possible to retrieve meta.requestDateTime from the response";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyMissingResourceEndpointResponseFullTest() {
		EnsureStatusUpdateDateTimeIsAfterRequestTime condition = new EnsureStatusUpdateDateTimeIsAfterRequestTime();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "Could not extract body from response";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(BASE_PATH + "MissingStatusUpdateDateTime.json")
	public void unhappyMissingStatusUpdateDateTimeTest() {
		EnsureStatusUpdateDateTimeIsAfterRequestTime condition = new EnsureStatusUpdateDateTimeIsAfterRequestTime();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "It was not possible to retrieve data.statusUpdateDateTime from the response";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(BASE_PATH + "StatusUpdateDateTimeBeforeRequestDateTime.json")
	public void unhappyTimeNotAfterTest() {
		EnsureStatusUpdateDateTimeIsAfterRequestTime condition = new EnsureStatusUpdateDateTimeIsAfterRequestTime();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "the statusUpdateDateTime field is not after the request time for every payment";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
