package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class PaymentConsentValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponseOK.json")
	public void validateStructure() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		run(condition);
	}


	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseOKWithGoodExtraFields.json")
	public void validateGoodExtraFields() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseOKWithBadExtraFields.json")
	public void validateBadExtraFields() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldKeyNotMatchPatternMessage("extraField", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponseOKAuthorisedStatus.json")
	public void validateStructureAuthorisedStatus() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponseOKAuthorisedStatusAboveLimit.json")
	public void validateStructureAuthorisedStatusExpirationDateTimeAboveLimit() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldIsntInSecondsRange("expirationDateTime", condition.getApiName())));
	}


	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponse(WithoutOptional).json")
	public void validateStructureWithoutOptional() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponse(WrongBusinessEntity).json")
	public void validateStructureWrongBusinessEntity() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("rel", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponse(WrongEnum).json")
	public void validateStructureWithWrongEnum() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldValueNotMatchEnumerationMessage("type", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponse(WithMissingField).json")
	public void validateStructureWithMissField() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("cpfCnpj", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponse(WrongRegexp).json")
	public void validateStructureWithWrongRegexp() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldValueNotMatchPatternMessage("creationDateTime", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponse(WrongExpirationTimeTooOld).json")
	public void validateStructureWithExpirationOld() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldIsntInSecondsRange("expirationDateTime", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponse(WrongExpirationTimeTooYoung).json")
	public void validateStructureWithExpirationYoung() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldIsntInSecondsRange("expirationDateTime", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v1/paymentInitiationConsentResponseNoDetails.json")
	public void validateStructureWithMissingDetails() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("details", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseQresWithoutQrCode.json")
	public void validateStructureQresWithoutQrCode() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("qrCode", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseDictWithoutQrCode.json")
	public void validateStructureDictWithoutQrCode() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseManuWithProxy.json")
	public void validateStructureManuWithProxy() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createMustNotBePresentMessage("proxy", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseManuWithoutProxy.json")
	public void validateStructureManuWithoutProxy() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseTranWithoutIssuer.json")
	public void validateStructureTranWithoutIssuer() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		run(condition);
	}
	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseCaccWithoutIssuer.json")
	public void validateStructureCaccWithoutIssuer() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("issuer", condition.getApiName())));
	}


	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseWithSchedule.json")
	public void validateStructureTranWithSchedule() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseWithDateAndSchedule.json")
	public void validateStructureTranWithDateAndSchedule() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createMustNotBePresentMessage("date", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationConsentResponseWithoutDateAndSchedule.json")
	public void validateStructureTranWithoutDateAndSchedule() {
		PaymentConsentValidatorV2 condition = new PaymentConsentValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("date", condition.getApiName())));
	}



}
