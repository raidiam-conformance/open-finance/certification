package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateEmptyConsentExpiryTimeTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTestWithNoPreviousExpiryTime() {
		happyPathTest();
	}

	@Test
	@UseResurce(value="environmentVariables/consents/v3/extensionExpiryTimes.json", key="extension_expiry_times")
	public void happyPathTestWithPreviousExpiryTimes() {
		happyPathTest();
	}

	public void happyPathTest() {
		CreateEmptyConsentExpiryTime condition = new CreateEmptyConsentExpiryTime();
		run(condition);
		JsonObject extensionExpiryTimes = environment.getObject("extension_expiry_times");
		assertTrue(extensionExpiryTimes.has("extension_times") &&
			extensionExpiryTimes.get("extension_times").isJsonArray() &&
			extensionExpiryTimes.get("extension_times").getAsJsonArray().contains(new JsonPrimitive("")));
	}
}
