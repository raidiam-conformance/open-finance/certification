package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

public class EditEnrollmentsRequestBodyToChangeDebtorAccountNumberTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_JSON_PATH = "jsonRequests/payments/enrollments/v2/postEnrollmentsRequest";
	private static final String KEY = "resource_request_entity_claims";
	private static final int MAX_LENGTH = 20;

	@Test
	@UseResurce(value = BASE_JSON_PATH + "V2.json", key = KEY)
	public void happyPathTestGeneralCase() {
		happyPathTest("1234567890");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithMaxLengthDebtorAccountNumberV2.json", key = KEY)
	public void happyPathTestMaxLengthNumber() {
		String newNumber = happyPathTest("12345678901234567890");
		assertTrue(newNumber.length() <= MAX_LENGTH);
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithDummyDebtorAccountNumberV2.json", key = KEY)
	public void happyPathTestDummyNumber() {
		happyPathTest("1234");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithoutDebtorAccountNumberV2.json", key = KEY)
	public void happyPathTestMissingNumber() {
		run(new EditEnrollmentsRequestBodyToChangeDebtorAccountNumber());
		JsonObject debtorAccount = environment.getElementFromObject(KEY, "data.debtorAccount").getAsJsonObject();
		assertTrue(debtorAccount.has("number"));
	}

	@Test
	public void unhappyPathTestMissingRequest() {
		assertThrowsExactly(AssertionError.class, () -> {
			run(new EditEnrollmentsRequestBodyToChangeDebtorAccountNumber());
		});
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithoutDebtorAccountV2.json", key = KEY)
	public void unhappyPathTestMissingDebtorAccount() {
		unhappyPathTest();
	}

	private String happyPathTest(String oldNumber) {
		run(new EditEnrollmentsRequestBodyToChangeDebtorAccountNumber());
		String newNumber = OIDFJSON.getString(
			environment.getElementFromObject(KEY, "data.debtorAccount.number")
		);
		assertNotEquals(oldNumber, newNumber);
		assertFalse(newNumber.isEmpty());
		assertEquals(newNumber, environment.getString("debtor_account_number"));
		return newNumber;
	}

	private void unhappyPathTest() {
		ConditionError error = runAndFail(new EditEnrollmentsRequestBodyToChangeDebtorAccountNumber());
		assertTrue(error.getMessage().contains("Could not find debtorAccount inside the body"));
	}
}
