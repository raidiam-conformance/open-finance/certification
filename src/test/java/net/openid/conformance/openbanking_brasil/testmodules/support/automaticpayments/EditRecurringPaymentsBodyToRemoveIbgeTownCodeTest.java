package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToRemoveIbgeTownCode;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentsBodyToRemoveIbgeTownCodeTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/payments/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPixPayment.json", key=KEY, path=PATH)
	public void happyPath() {
		EditRecurringPaymentsBodyToRemoveIbgeTownCode condition = new EditRecurringPaymentsBodyToRemoveIbgeTownCode();
		run(condition);

		JsonObject data = environment
			.getElementFromObject(KEY, PATH+".data")
			.getAsJsonObject();
		assertFalse(data.has("ibgeTownCode"));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		EditRecurringPaymentsBodyToRemoveIbgeTownCode condition = new EditRecurringPaymentsBodyToRemoveIbgeTownCode();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data in payments payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
