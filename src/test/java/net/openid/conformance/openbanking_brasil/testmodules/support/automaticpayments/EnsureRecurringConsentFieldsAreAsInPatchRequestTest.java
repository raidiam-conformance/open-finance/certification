package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureRecurringConsentFieldsAreAsInPatchRequest;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureRecurringConsentFieldsAreAsInPatchRequestTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String EXPECTED_AMOUNT = "0.10";
	protected static final String EXPECTED_EXPIRATION = "2030-10-03T08:30:00Z";
	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/GetRecurringConsents";

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "ConsentEditionExpiration200Response.json")
	public void happyPathTestExpiration() {
		happyPathTest(false, true);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "ConsentEditionAmount200Response.json")
	public void happyPathTestAmount() {
		happyPathTest(true, false);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "ConsentEditionBoth200Response.json")
	public void happyPathTestBoth() {
		happyPathTest(true, true);
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(EnsureRecurringConsentFieldsAreAsInPatchRequest.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(EnsureRecurringConsentFieldsAreAsInPatchRequest.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingExpirationDateTime200Response.json")
	public void unhappyPathTestMissingExpirationDateTime() {
		prepareEnvForTest(false, true);
		unhappyPathTest("Could not extract expirationDateTime from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingMaximumVariableAmount200Response.json")
	public void unhappyPathTestExpirationDateTimeNotUpdated() {
		prepareEnvForTest(false, true);
		unhappyPathTest("The field expirationDateTime has not been updated after consent edition");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "ConsentEditionBoth200Response.json")
	public void unhappyPathTestExpirationDateTimeNotExtendedIndefinitely() {
		prepareEnvForTest(true, false);
		unhappyPathTest("The field expirationDateTime has been indefinitely extended, so it should not exist in the response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRecurringConfiguration200Response.json")
	public void unhappyPathTestMissingRecurringConfiguration() {
		prepareEnvForTest(true, false);
		unhappyPathTest("Could not extract maximumVariableAmount from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "Sweeping200Response.json")
	public void unhappyPathTestMissingAutomatic() {
		prepareEnvForTest(true, false);
		unhappyPathTest("Could not extract maximumVariableAmount from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "ConsentEditionExpiration200Response.json")
	public void unhappyPathTestMissingMaximumVariableAmount() {
		prepareEnvForTest(true, false);
		unhappyPathTest("Could not extract maximumVariableAmount from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void unhappyPathTestMaximumVariableAmountNotUpdated() {
		prepareEnvForTest(true, false);
		unhappyPathTest("The field maximumVariableAmount has not been updated after consent edition");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "ConsentEditionBoth200Response.json")
	public void unhappyPathTestMaximumVariableAmountNotExtendedIndefinitely() {
		prepareEnvForTest(false, true);
		unhappyPathTest("The field maximumVariableAmount has been indefinitely extended, so it should not exist in the response data");
	}

	protected void happyPathTest(boolean setAmount, boolean setExpiration) {
		prepareEnvForTest(setAmount, setExpiration);
		run(new EnsureRecurringConsentFieldsAreAsInPatchRequest());
	}

	protected void prepareEnvForTest(boolean setAmount, boolean setExpiration) {
		if (setAmount) {
			environment.putString("consent_edition_amount", EXPECTED_AMOUNT);
		}
		if (setExpiration) {
			environment.putString("consent_edition_expiration", EXPECTED_EXPIRATION);
		}
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureRecurringConsentFieldsAreAsInPatchRequest());
		assertTrue(error.getMessage().contains(message));
	}
}
