package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


public class GetPaymentsConsentOASValidatorV4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentValidatorV4ResponseOK.json")
	public void happyPath() {
		run(new
			GetPaymentsConsentOASValidatorV4());
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentValidatorV4ResponseDateAndSchedulePresent.json")
	public void unhappyPathDateAndSchedule() {
		ConditionError e = runAndFail(new GetPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("payment.date and payment.schedule cannot both be present; they are mutually exclusive."));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentValidatorV4ResponseNoProxy.json")
	public void unhappyPathProxy1() {
		ConditionError e = runAndFail(new GetPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("payment.details.proxy is required when payment.details.localInstrument is [INIC, DICT, QRDN, QRES]"));	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentValidatorV4ResponseProxyWithManu.json")
	public void unhappyPathProxy2() {
		ConditionError e = runAndFail(new GetPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("payment.details.proxy cannot be present when payment.details.localInstrument has the value MANU"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentValidatorV4ResponseCreditorNoIssuer.json")
	public void ununhappyPathNoIssuer1() {
		ConditionError e = runAndFail(new GetPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("payment.details.creditorAccount.issuer is required when payment.details.creditorAccount.accountType is [CACC, SVGS]"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentValidatorV4ResponseDebtorNoIssuer.json")
	public void ununhappyPathNoIssuer2() {
		ConditionError e = runAndFail(new GetPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("debtorAccount.issuer is required when debtorAccount.accountType is [CACC, SVGS]"));
	}
}
