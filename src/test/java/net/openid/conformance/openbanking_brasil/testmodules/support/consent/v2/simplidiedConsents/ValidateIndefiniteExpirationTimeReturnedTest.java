package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplidiedConsents;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateIndefiniteExpirationTimeReturned;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class ValidateIndefiniteExpirationTimeReturnedTest  extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/account/readConsentResponse_missing_consents.json")
	public void testHappyPath(){
		environment.putString("consent_extension_expiration_time", "2021-05-21T08:30:00Z");
		ValidateIndefiniteExpirationTimeReturned condition = new ValidateIndefiniteExpirationTimeReturned();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/account/readConsentResponse_missing_consents.json")
	public void testUnhappyPathWrongConsentExpirationTime() {
		environment.putString("consent_extension_expiration_time", "2300-01-01T00:00:00Z");
		ValidateIndefiniteExpirationTimeReturned condition = new ValidateIndefiniteExpirationTimeReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Consent expiration time is not the expected value"));
	}

	@Test
	@UseResurce("jsonResponses/account/readConsentResponse_missing_expirationDateTime.json")
	public void testUnhappyPathNoConsentExpirationTime() {
		environment.putString("consent_extension_expiration_time", "2300-01-01T00:00:00Z");
		ValidateIndefiniteExpirationTimeReturned condition = new ValidateIndefiniteExpirationTimeReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Couldn't find data.expirationDateTime in the consent endpoint response"));
	}

	@Test
	@UseResurce("jsonResponses/account/accountV2/balancesV2/accountBalancesResponse.json")
	public void testUnhappyPathNoExpectedConsentExpirationTimeEnvKey() {
		ValidateIndefiniteExpirationTimeReturned condition = new ValidateIndefiniteExpirationTimeReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Expected consent expiration time not found in environment"));
	}
	@Test
	public void testUnhappyPathNoBody() {
		environment.putObject("consent_endpoint_response_full", new JsonObject());
		ValidateIndefiniteExpirationTimeReturned condition = new ValidateIndefiniteExpirationTimeReturned();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}
}
