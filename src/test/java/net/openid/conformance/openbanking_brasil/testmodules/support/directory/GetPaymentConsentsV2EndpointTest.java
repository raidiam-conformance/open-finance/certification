package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import org.junit.Assert;

public class GetPaymentConsentsV2EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/payments/v2/consents";
	}

	@Override
	protected String getApiFamilyType() {
		return "payments-consents";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return false;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetPaymentConsentsV2Endpoint();
	}

	@Override
	protected void assertUrlNotFound() {
		run(getCondition());
		Assert.assertNotNull(environment.getString("warning_message"));
	}
}
