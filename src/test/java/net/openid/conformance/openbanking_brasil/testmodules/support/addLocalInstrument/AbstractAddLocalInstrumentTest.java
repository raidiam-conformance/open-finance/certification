package net.openid.conformance.openbanking_brasil.testmodules.support.addLocalInstrument;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.addLocalInstrument.AddManuLocalInstrument;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AbstractAddLocalInstrumentTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTestManu() {
		run(new AddManuLocalInstrument());
		assertEquals("MANU", environment.getString("local_instrument"));
	}
}
