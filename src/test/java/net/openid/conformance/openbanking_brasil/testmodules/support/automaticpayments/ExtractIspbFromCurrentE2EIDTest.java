package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractIspbFromCurrentE2EID;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ExtractIspbFromCurrentE2EIDTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "resource";
	private static final String PAYMENT_PATH = "brazilPixPayment";
	private static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBody";

	@Test
	@UseResurce(value = BASE_JSON_PATH + "V2.json", key = KEY, path = PAYMENT_PATH)
	public void happyPathTest() {
		run(new ExtractIspbFromCurrentE2EID());
		String ispb = environment.getString("ispb");
		assertTrue(ispb.matches("\\d{8}"));
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Could not find payment request body in the resource");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PAYMENT_PATH, new JsonObject());
		unhappyPathTest("Could not find data.endToEndId in current payment request body");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "MissingEndToEndIdV2.json", key = KEY, path = PAYMENT_PATH)
	public void unhappyPathTestMissingEndToEndId() {
		unhappyPathTest("Could not find data.endToEndId in current payment request body");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new ExtractIspbFromCurrentE2EID());
		assertTrue(error.getMessage().contains(message));
	}
}
