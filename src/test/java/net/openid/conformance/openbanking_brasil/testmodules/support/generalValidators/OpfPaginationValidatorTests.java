package net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators;

import com.google.gson.JsonElement;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class OpfPaginationValidatorTests extends AbstractJsonResponseConditionUnitTest {

	private final static String RESPONSE_ENV_KEY = "resource_endpoint_response_full";
	private final static String JSON_RESPONSES_BASE_PATH = "jsonResponses/phase4b/investments/validators/";

	private String resourceUrl;

	private boolean requestDateTimePresent = true;
	private boolean validRequestDateTime = true;
	private boolean validRequestDateTimeFormat = true;

	class paginationValidator extends AbstractJsonAssertingCondition {

		private final OpfPaginationValidator opfPaginationValidator;

		paginationValidator() {
			opfPaginationValidator = new OpfPaginationValidator();
			opfPaginationValidator.setProperties("UNIT-TEST", mock(TestInstanceEventLog.class), ConditionResult.INFO);
		}

		@Override
		@PreEnvironment(required = RESPONSE_ENV_KEY)
		public Environment evaluate(Environment env) {
			env.mapKey(OpfPaginationValidator.RESPONSE_ENV_KEY, RESPONSE_ENV_KEY);
			env.putString("protected_resource_url", resourceUrl);

			if (requestDateTimePresent) {
				JsonElement body = bodyFrom(env,RESPONSE_ENV_KEY);
				body = generateRequestDateTime(body);
				env.putString(RESPONSE_ENV_KEY, "body", body.toString());
				env.putString("resource_endpoint_response", body.toString());
				env.putObject("consent_endpoint_response", body.getAsJsonObject());
			}
			opfPaginationValidator.evaluate(env);
			return env;
		}

		protected JsonElement generateRequestDateTime(JsonElement body) {
			Instant requestDateTimeInstant = Instant.now();
			if (!validRequestDateTime) {
				requestDateTimeInstant = requestDateTimeInstant.minusSeconds(400);
			}

			if (validRequestDateTimeFormat) {
				requestDateTimeInstant=requestDateTimeInstant.truncatedTo(ChronoUnit.SECONDS);
			} else {
				requestDateTimeInstant=requestDateTimeInstant.truncatedTo(ChronoUnit.MILLIS);
			}
			body.getAsJsonObject()
				.get("meta").getAsJsonObject()
				.addProperty("requestDateTime",requestDateTimeInstant.toString());
			return body;
		}
	}
	//Unit tests for single page responses
	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponse.json")
	public void testHappyPathSingle() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponse.json")
	public void testHappyWithPaginationKey() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&pagination-key=EcQLneaTqwDdNY1AdpkS";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponseOnlySelfNoPageNum.json")
	public void testHappyPathSingleOnlySelfLinkNoPageNum() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponseOnlySelfNoPageNum.json")
	public void testHappyPathSinglePageUnnecessaryPageNum() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponseOnlySelfNoPageNum.json")
	public void testHappyPathSinglePageUnnecessaryPageSize() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page-size=25";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponsePageSize1.json")
	public void testHappyPathSinglePagePageSize1UnnecessaryPageNum() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&page-size=1";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponseSelfFirstLinks.json")
	public void testHappyPathSingleSelfFirstSelfOnly() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponsePageSize1.json")
	public void testHappyPathSinglePageSize1() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page-size=1";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}
	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponseEmptyData.json")
	public void testHappyPathSinglePageEmptyData() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&page-size=25";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponsePageSize1000.json")
	public void testHappyPathSinglePageSize1000() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page-size=1000";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseWrongTotalRecords.json")
	public void testUnhappyPathSingleWrongTotalRecords() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("The actual number of records present in the response differs from the expected value provided on meta"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseInvalidPrevLink.json")
	public void testUnhappyPathSinglePrevLink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("There should not be prev/next links"));
	}
	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseInvalidNextLink.json")
	public void testUnhappyPathSingleNextLink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("There should not be prev/next links"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseWrongPageSize.json")
	public void testUnhappyPathSingleWrongPageSize() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page-size=2";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Number of records exceeds page size, but totalPages is not greater than 1"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseMismatchedSelfLinkToLastlink.json")
	public void testUnhappyPathSingleSelfDifferentFromRequest() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("self link in the response is not the same as the request url."));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseNoSelfLink.json")
	public void testUnhappyPathSingleNoSelfLink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());

		assertThat(conditionError.getMessage(), containsString(
			ErrorMessagesUtils.createElementNotFoundMessage("$.self", "OpfPaginationValidator")));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponse.json")
	public void testUnhappyPathSingleWrongRequestDateTime() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource";
		this.validRequestDateTime = false;
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Value of requestDateTime is not in expected range"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponse.json")
	public void testUnhappyPathSingleInvalidRequestDateTimeFormat() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource";
		this.validRequestDateTimeFormat = false;
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Value from element requestDateTime doesn't " +
			"match the required pattern on the OpfPaginationValidator API response"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponse.json")
	public void testUnhappyPathSingleNoRequestDateTime() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource";
		this.requestDateTimePresent = false;
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Unable to find element $.requestDateTime on the " +
			"OpfPaginationValidator API response"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseInvalidSelfLink.json")
	public void testUnhappyPathSingleInvalidSelfLink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Value from element self doesn't match the required pattern on the OpfPaginationValidator API response"));

	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageSwappedQueryParamsOrderHappyResponse.json")
	public void testHappyPathSingleDifferentQueryParamsOrder() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&fromTransactionDate=2023-07-20&toTransactionDate=2023-07-26";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponseScheduledInstalments.json")
	public void testHappyPathSinglePageScheduledInstalments() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v2/contracts/atotallyuniqueuuid/scheduled-instalments";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponseScheduledInstalmentsNoBalloonPayments.json")
	public void testHappyPathSinglePageNoBalloonPayments() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v2/contracts/atotallyuniqueuuid/scheduled-instalments";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	// Unit tests for multiple page responses

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageHappyResponseFirstPageAllLinks.json")
	public void testHappyPathMultipleFirstPage() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&page-size=10";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageHappyResponsePageSize10Page2Records35.json")
	public void testHappyPathMultipleSecondPageNbRecords35() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=2&page-size=10";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageHappyResponsePageSize10Page4Records35.json")
	public void testHappyPathMultipleFourthPageNbRecords35() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=4&page-size=10";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageHappyResponsePageSize10Page4Records35Last.json")
	public void testHappyPathMultipleFourthPageNbRecords35Page4Last() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=4&page-size=10";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageHappyResponseSecondPage.json")
	public void testHappyPathMultipleSecondPage() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=2&page-size=10";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseInvalidPrevLink.json")
	public void testUnhappyPathMultipleInvalidPrevLink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=2&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Prev link page does not point to the previous page."));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseInvalidPageNumber.json")
	public void testUnhappyPathMultipleInvalidPageNum() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=12&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Current page number is outside of expected page range"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseNoPageNumber.json")
	public void testUnhappyPathMultipleNoPageNum() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Last link parameters are not according to the swagger specification."));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseInvalidTotalPages.json")
	public void testUnhappyPathMultipleInvalidTotalPages() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=12&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("The totalPages value is not equal to the amount of pages expected"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseFinalPageInvalidNumberOfRecords.json")
	public void testUnhappyPathMultipleInvalidNumberOfRecords() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=4&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Number of records does not match expected amount for final page"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponseToAndFromTransactionDateInRequest.json")
	public void testHappyPathSinglePageToAndFromTransactionDateInRequest() {
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = currentDate.format(dateFormatter);
		resourceUrl = String.format("https://api.banco.com.br/open-banking/api/v1/resource?fromTransactionDate=%s&toTransactionDate=%s",
			formattedDate, formattedDate);
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponsePageSizeSmallerThanInRequest.json")
	public void testHappyPathPageSizeSmallerThanInRequest() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&page-size=20";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseLastLinkInTransactions.json")
	public void testUnhappyPathMultiPageLastLinkInTransactions() {
		resourceUrl = "https://api.banco.com.br/open-banking/funds/v1/investments/c0826748-22b6-432d-9b3f-a7e5a876e0bf/transactions?page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("The element last on the OpfPaginationValidator API response must not be present"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseLastLinkInTransactionsCurrent.json")
	public void testUnhappyPathMultiPageLastLinkInTransactionsCurrent() {
		resourceUrl = "https://api.banco.com.br/open-banking/funds/v1/investments/c0826748-22b6-432d-9b3f-a7e5a876e0bf/transactions-current?page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("The element last on the OpfPaginationValidator API response must not be present"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponseSelfFirstLinks.json")
	public void testHappyPathIsUrlEqual() {
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = currentDate.format(dateFormatter);
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page-size=25&page=1&fromTransactionDate="+formattedDate+"&toTransactionDate="+formattedDate;
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageHappyResponseFirstPagePageSize250.json")
	public void testUnhappyPathIsUrlEqualPageSize250And1000() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page-size=1000";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageHappyResponsePageSize250.json")
	public void testUnhappyPathIsUrlEqualPageSize250AndPage10() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?0&0";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("self link in the response is not the same as the request url. (#1)"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseMismatchedSelfLinkToLastlink.json")
	public void testUnhappyPathIsUrlEqualNotPOST() {
		environment.putString("http_method","GET");
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("self link in the response is not the same as the request url. (#2)"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponsePageSize1500.json")
	public void testUnhappyPathInvalidPageSize() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("page-size cannot be greater than 1000"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseMismatchedSelfLinkToFirstlink.json")
	public void testUnhappyPathSinglePageMismatchedSelflinkToFirstlink() {
		resourceUrl = "https://api.bonco.com.br/open-banking/api/v1/resource";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Self link has to be the same as first/last links if they are present"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationSinglePageUnhappyResponseMismatchedSelfLinkToLastlink.json")
	public void testUnhappyPathSinglePageMismatchedSelflinkToLastlink() {
		resourceUrl = "https://api.bonco.com.br/open-banking/api/v1/resource";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Self link has to be the same as first/last links if they are present"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseFirstPageInvalidPrevlink.json")
	public void testUnhappyPathFirstPageInvalidPrevlink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Self link is the first page but prev link was found"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseFirstPageInvalidNextlink.json")
	public void testUnhappyPathFirstPageInvalidNextlink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Next link page does not point to the next page."));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseFirstPageInvalidSelflink.json")
	public void testUnhappyPathFirstPageInvalidSelflinkWithFirstLink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Self link has to be the same as first link if it is present."));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseFirstPageInvalidSelflinkRecordsAmount.json")
	public void testUnhappyPathFirstPageInvalidSelflinkWrongRecords() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=1&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Self link is the first page but does not have the right amount of records"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseSecondPageInvalidSelflink.json")
	public void testUnhappyPathMiddlePageInvalidSelflink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=2&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Self link does not have the right amount of records"));
	}
	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseSecondPageInvalidNextlink.json")
	public void testUnhappyPathMiddlePageInvalidNextlink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=2&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Next link page does not point to the next page."));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseSecondPageInvalidFirstlink.json")
	public void testUnhappyPathMiddlePageInvalidFirstlink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=2&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("First link page is not 1."));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseLastPageInvalidNextlink.json")
	public void testUnhappyPathLastPageInvalidNextlink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=4&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Self link is the last page but next link was found"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseLastPageWrongRecords.json")
	public void testUnhappyPathLastPageWrongRecords() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=4&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Self link does not have the right amount of records"));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseLastPageInvalidPrevlink.json")
	public void testUnhappyPathLastPageInvalidPrevlink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=4&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("Prev link page does not point to the previous page."));
	}

	@Test
	@UseResurce(JSON_RESPONSES_BASE_PATH + "GetPhase4bPaginationMultiPageUnhappyResponseLastPageInvalidFirstlink.json")
	public void testUnhappyPathLastPageInvalidFirstlink() {
		resourceUrl = "https://api.banco.com.br/open-banking/api/v1/resource?page=4&page-size=10";
		ConditionError conditionError = runAndFail(new OpfPaginationValidatorTests.paginationValidator());
		assertThat(conditionError.getMessage(), containsString("First link page is not 1."));
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/cardBillV2/cardBillResponseCorrect.json")
	public void testHappyPathLastlinkInCreditCardBills() {
		resourceUrl = "https://api.bills.transactions.transactions-current.br/open-banking/credit-cards-accounts/v2/accounts/11aaa1aa-1a11-1111-a11a-1110aaaa1a10/bills/11110111-10aa-111a-11aa-a11aa1111aaa/transactions";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/cardBillV2/cardBillResponseCorrectBankName.json")
	public void testHappyPathBankNamedLikeAPi() {
		resourceUrl = "https://api.funds.accounts.br/open-banking/credit-cards-accounts/v2/accounts/11aaa1aa-1a11-1111-a11a-1110aaaa1a10/bills/11110111-10aa-111a-11aa-a11aa1111aaa/transactions";
		run(new OpfPaginationValidatorTests.paginationValidator());
	}
}
