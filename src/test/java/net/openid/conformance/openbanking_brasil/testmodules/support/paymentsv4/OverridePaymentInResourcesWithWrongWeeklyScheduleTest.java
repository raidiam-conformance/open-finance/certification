package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.overridePaymentsInResources.OverridePaymentInResourcesWithWrongWeeklySchedule;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OverridePaymentInResourcesWithWrongWeeklyScheduleTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_CONSENTS_PATH_HAPPY = "jsonRequests/payments/consents/correctPaymentConsentRequest";
	private static final String BASE_PAYMENTS_PATH = "jsonRequests/payments/payments/paymentWithDataArrayWith";
	private static final String KEY = "resource";
	private static final String CONSENTS_PATH = "brazilPaymentConsent";
	private static final String PAYMENTS_PATH = "brazilPixPayment";

	private static final SimpleDateFormat DATETIME_FORMATTER = new SimpleDateFormat("yyyyMMdd");
	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
	private static final String END_TO_END_ID_REGEX = "E(.{8})(\\d{12})(.{11})";

	@Before
	public void init() {
		setPaymentsToBeArray();
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayDailyScheduleWith5Payments.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "5Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void unhappyPaymentsMissingWeeklyFieldTest() {
		OverridePaymentInResourcesWithWrongWeeklySchedule condition = new OverridePaymentInResourcesWithWrongWeeklySchedule();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not extract the weekly from the payment in resource";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayWeeklyScheduleWith5Payments.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "12Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void unhappyPaymentsDataArrayWithWrongSizeTest() {
		OverridePaymentInResourcesWithWrongWeeklySchedule condition = new OverridePaymentInResourcesWithWrongWeeklySchedule();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Payments data array does not have the correct amount of payments in it";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayWeeklyScheduleWith5Payments.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "InvalidEndToEndId.json", key = KEY, path = PAYMENTS_PATH)
	public void unhappyPaymentsWithInvalidEndToEndIdTest() {
		OverridePaymentInResourcesWithWrongWeeklySchedule condition = new OverridePaymentInResourcesWithWrongWeeklySchedule();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "endToEndId is not in a valid format";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "ArrayWeeklyScheduleWith5Payments.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "5Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void happy5Payments1st() {
		OverridePaymentInResourcesWithWrongWeeklySchedule condition = new OverridePaymentInResourcesWithWrongWeeklySchedule();
		run(condition);

		JsonArray data = getDataArrayFromPaymentObject();
		assertEquals(5, data.size());
		validateDates(data, List.of("2023-08-23", "2023-08-30", "2023-09-06", "2023-09-13", "2023-09-20"));
	}

	protected JsonArray getDataArrayFromPaymentObject() {
		return environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonArray();
	}

	protected void validateEndToEndId(String endToEndId, String date) {
		Pattern pattern = Pattern.compile(END_TO_END_ID_REGEX);
		Matcher matcher = pattern.matcher(endToEndId);
		if (matcher.matches()) {
			String dateTimeString = matcher.group(2).substring(0, 8);
			try {
				Date endToEndIdDate = DATETIME_FORMATTER.parse(dateTimeString);
				Date expectedDate = DATE_FORMATTER.parse(date);
				assertEquals(endToEndIdDate, expectedDate);
			} catch (ParseException e) {
				fail("Could not parse dates.");
			}
		} else {
			fail("Invalid endToEndId format.");
		}
	}

	protected void setPaymentsToBeArray() {
		environment.putString("payment_is_array", "ok");
	}

	protected void validateDates(JsonArray data, List<String> dates) {
		for (int i = 0; i < data.size(); i++) {
			String endToEndId = OIDFJSON.getString(data.get(i).getAsJsonObject().get("endToEndId"));
			validateEndToEndId(endToEndId, dates.get(i));
		}
	}
}
