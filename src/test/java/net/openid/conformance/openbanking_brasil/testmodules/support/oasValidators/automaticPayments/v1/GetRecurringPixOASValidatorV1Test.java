package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetRecurringPixOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/getRecurringPaymentsById/GetRecurringPixPaymentsListByIdOK.json")
	public void happyPath() {
		run(new GetRecurringPixOASValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/getRecurringPaymentsById/GetRecurringPixPaymentsListByIdMissingConsentId.json")
	public void unhappyPathRecurringConsentIdMissing() {
		ConditionError e = runAndFail(new GetRecurringPixOASValidatorV1());
		assertThat(e.getMessage(), containsString("recurringConsentId is required when authorisationFlow is FIDO_FLOW"));
	}
}
