package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddRecurringPaymentConsentRequestBodyToConfigTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "config";
	private static final String PATH = "resource";
	private static final String CONSENT_REQUEST_PATH = "resource.brazilPaymentConsent";
	private static final String RESOURCE_PATH = "jsonEnvironmentObjects/resource/";
	private static final String DATE_TIME_FORMAT = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z$";


	@Test
	@UseResurce(value = RESOURCE_PATH + "fullyFilledResource.json", key = KEY, path = PATH)
	@UseResurce(value = "jsonEnvironmentObjects/recurringConfigurationObject/automaticRecurringConfiguration.json",
		key = "recurring_configuration_object")
	public void happyPathTest() {
		runHappyPathTest(true,false);
	}

	@Test
	@UseResurce(value = RESOURCE_PATH + "missingOptionalFieldsResource.json", key = KEY, path = PATH)
	public void happyPathMissingOptionalFieldsTest() {
		runHappyPathTest(false,false);
	}

	@Test
	@UseResurce(value = RESOURCE_PATH + "missingDebtorAccountIspbResource.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingDebtorAccountIspbTest() {
		runUnhappyTest("debtorAccountIspb");
	}

	@Test
	@UseResurce(value = RESOURCE_PATH + "missingDebtorAccountIssuerResource.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingDebtorAccountIssuerTest() {
		runUnhappyTest("debtorAccountIssuer");
	}

	@Test
	@UseResurce(value = RESOURCE_PATH + "missingDebtorAccountNumberResource.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingDebtorAccountNumberTest() {
		runUnhappyTest("debtorAccountNumber");
	}

	@Test
	@UseResurce(value = RESOURCE_PATH + "missingDebtorAccountTypeResource.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingDebtorAccountTypeTest() {
		runUnhappyTest("debtorAccountType");
	}

	@Test
	@UseResurce(value = RESOURCE_PATH + "missingCreditorNameResource.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingCreditorNameTest() {
		runUnhappyTest("creditorName");
	}
	@Test
	@UseResurce(value = RESOURCE_PATH + "fullyFilledFvpResource.json", key = KEY, path = PATH)
	@UseResurce(value = "jsonEnvironmentObjects/recurringConfigurationObject/sweepingRecurringConfiguration.json",
		key = "recurring_configuration_object")
	public void happyPathFvpTest() {
		runHappyPathTest(true, true);
	}

	@Test
	@UseResurce(value = RESOURCE_PATH + "missingOptionalFieldsFvpResource.json", key = KEY, path = PATH)
	public void happyPathMissingOptionalFieldsFvpTest() {
		runHappyPathTest(false, true);
	}

	private String assertStringFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonPrimitive() && obj.get(field).getAsJsonPrimitive().isString());
		return OIDFJSON.getString(obj.get(field));
	}

	private JsonObject assertObjectFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonObject());
		return obj.getAsJsonObject(field);
	}

	private void assertStringIsEqualsToTheResource(JsonObject resource, String fieldName, String value) {
		assertEquals(OIDFJSON.getString(resource.get(fieldName)), value);
	}

	private void runHappyPathTest(boolean isFullyFilled, boolean isFvp) {
		AddRecurringPaymentConsentRequestBodyToConfig condition = new AddRecurringPaymentConsentRequestBodyToConfig();
		run(condition);

		JsonObject resource = environment.getElementFromObject(KEY, PATH).getAsJsonObject();

		JsonObject consentRequest = environment.getElementFromObject(KEY, CONSENT_REQUEST_PATH).getAsJsonObject();

		JsonObject data = assertObjectFieldIsPresent(consentRequest, "data");

		JsonObject loggedUser = assertObjectFieldIsPresent(data, "loggedUser");
		JsonObject document = assertObjectFieldIsPresent(loggedUser, "document");

		String identification = assertStringFieldIsPresent(document, "identification");

		String rel = assertStringFieldIsPresent(document, "rel");
		assertEquals("CPF", rel);

		if (!isFvp) {
			if (isFullyFilled) {
				assertStringIsEqualsToTheResource(resource, "loggedUserIdentification", identification);
			} else {
				assertEquals(DictHomologKeys.PROXY_EMAIL_CPF, identification);
			}

			String expirationDateTime = assertStringFieldIsPresent(data, "expirationDateTime");
			assertTrue(expirationDateTime.matches(DATE_TIME_FORMAT));

			JsonObject debtorAccount = assertObjectFieldIsPresent(data, "debtorAccount");

			String ispb = assertStringFieldIsPresent(debtorAccount, "ispb");
			assertStringIsEqualsToTheResource(resource, "debtorAccountIspb", ispb);

			String issuer = assertStringFieldIsPresent(debtorAccount, "issuer");
			assertStringIsEqualsToTheResource(resource, "debtorAccountIssuer", issuer);

			String number = assertStringFieldIsPresent(debtorAccount, "number");
			assertStringIsEqualsToTheResource(resource, "debtorAccountNumber", number);

			String accountType = assertStringFieldIsPresent(debtorAccount, "accountType");
			assertStringIsEqualsToTheResource(resource, "debtorAccountType", accountType);
		}

		assertTrue(data.has("creditors") && data.get("creditors").isJsonArray());
		JsonArray creditors = data.getAsJsonArray("creditors");
		assertTrue(creditors.size() > 0 && creditors.get(0).isJsonObject());
		JsonObject creditor = creditors.get(0).getAsJsonObject();

		String personType = assertStringFieldIsPresent(creditor, "personType");
		String expectedPersonType = isFullyFilled ? "PESSOA_JURIDICA" : "PESSOA_NATURAL";
		assertEquals(expectedPersonType, personType);

		String cpfCnpj = assertStringFieldIsPresent(creditor, "cpfCnpj");

		if(!isFvp) {
			if (isFullyFilled) {
				assertStringIsEqualsToTheResource(resource, "businessEntityIdentification", cpfCnpj);
			} else {
				assertEquals(DictHomologKeys.PROXY_EMAIL_CPF, cpfCnpj);
			}
		}

		String name = assertStringFieldIsPresent(creditor, "name");
		if(!isFvp) {
			assertStringIsEqualsToTheResource(resource, "creditorName", name);
		}

		if (isFullyFilled) {
			assertObjectFieldIsPresent(data, "recurringConfiguration");

			JsonObject businessEntity = assertObjectFieldIsPresent(data, "businessEntity");
			JsonObject businessEntityDocument = assertObjectFieldIsPresent(businessEntity, "document");

			String businessEntityIdentification = assertStringFieldIsPresent(businessEntityDocument, "identification");
			if(!isFvp) {
				assertStringIsEqualsToTheResource(resource, "businessEntityIdentification", businessEntityIdentification);
			}
			String businessEntityRel = assertStringFieldIsPresent(businessEntityDocument, "rel");
			assertEquals("CNPJ", businessEntityRel);
		}
	}

	private void runUnhappyTest(String missingField) {
		AddRecurringPaymentConsentRequestBodyToConfig condition = new AddRecurringPaymentConsentRequestBodyToConfig();
		ConditionError error = runAndFail(condition);

		String expectedMessage = String.format("Unable to find element resource.%s in config", missingField);
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
