package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetUnarrangedAccountsOverdraftIdentificationV2N3OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/identification.json")
	public void happyPath() {
		run(new GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/identificationIndexerInfoIsOptional.json")
	public void happyPathIndexerInfoIsOptional1() {
		run(new GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/identificationIndexerInfoIsOptional2.json")
	public void happyPathIndexerInfoIsOptional2() {
		run(new GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/identificationIndexerInfoIsOptional3.json")
	public void happyPathIndexerInfoIsOptional3() {
		run(new GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/identificationNoIndexerAdditionalInfo.json")
	public void unhappyPathNoIndexerAdditionalInfo() {
		ConditionError e = runAndFail(new GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator());
		assertThat(e.getMessage(), containsString("referentialRateIndexerAdditionalInfo is required when referentialRateIndexerType and referentialRateIndexerSubType equals to OUTROS_INDEXADORES"));
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/identificationNoChargeAdditionalInfo.json")
	public void unhappyPathNoChargeAdditionalInfo() {
		ConditionError e = runAndFail(new GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator());
		assertThat(e.getMessage(), containsString("chargeAdditionalInfo is required when chargeType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/identificationNoFeeAmount.json")
	public void unhappyPathNoFeeAmount() {
		ConditionError e = runAndFail(new GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator());
		assertThat(e.getMessage(), containsString("feeAmount is required when feeCharge is not PERCENTUA"));
	}


	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/identificationNoFeeRate.json")
	public void unhappyPathNoFeeRate() {
		ConditionError e = runAndFail(new GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator());
		assertThat(e.getMessage(), containsString("feeRate is required when feeCharge is PERCENTUAL"));
	}

}
