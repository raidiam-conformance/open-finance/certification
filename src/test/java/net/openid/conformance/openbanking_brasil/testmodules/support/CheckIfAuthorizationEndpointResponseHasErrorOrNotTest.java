package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CheckIfAuthorizationEndpointResponseHasErrorOrNotTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTestWithError() {
		environment.putObject("authorization_endpoint_response",
			new JsonObjectBuilder().addField("error", "error").build());
		CheckIfAuthorizationEndpointResponseHasErrorOrNot condition = new CheckIfAuthorizationEndpointResponseHasErrorOrNot();
		run(condition);
		assertTrue(environment.getBoolean("token_endpoint_response_was_error"));
	}

	@Test
	public void happyPathTestWithoutError() {
		environment.putObject("authorization_endpoint_response", new JsonObject());
		CheckIfAuthorizationEndpointResponseHasErrorOrNot condition = new CheckIfAuthorizationEndpointResponseHasErrorOrNot();
		run(condition);
		assertFalse(environment.getBoolean("token_endpoint_response_was_error"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingAuthorizationEndpointResponse() {
		CheckIfAuthorizationEndpointResponseHasErrorOrNot condition = new CheckIfAuthorizationEndpointResponseHasErrorOrNot();
		run(condition);
	}
}
