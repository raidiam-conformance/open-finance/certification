package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.validateVersionHeader;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.validateVersionHeader.AbstractValidateVersionHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.validateVersionHeader.ValidateVersionHeader2d0d0;

public class ValidateVersionHeader2d0d0Test extends AbstractValidateVersionHeaderTest {

	@Override
	protected AbstractValidateVersionHeader condition() {
		return new ValidateVersionHeader2d0d0();
	}

	@Override
	protected String expectedVersion() {
		return "2.0.0";
	}
}
