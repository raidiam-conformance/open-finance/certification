package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetInvoiceFinancingsV2EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/invoice-financings/v2/contracts";
	}

	@Override
	protected String getApiFamilyType() {
		return "invoice-financings";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetInvoiceFinancingsV2Endpoint();
	}
}
