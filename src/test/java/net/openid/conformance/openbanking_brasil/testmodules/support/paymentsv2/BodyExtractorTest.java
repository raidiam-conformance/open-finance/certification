package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.text.ParseException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BodyExtractorTest extends AbstractJsonResponseConditionUnitTest {

	private static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Test
	public void missingKeyTest() throws ParseException {
		Optional<JsonElement> element = getResponseFromEnv();
		assertTrue(element.isEmpty());
	}

	@Test
	public void missingBodyTest() throws ParseException {
		addResponseToEnv(new JsonObject());
		Optional<JsonElement> element = getResponseFromEnv();
		assertTrue(element.isEmpty());
	}

	@Test(expected = ParseException.class)
	public void jwtResponseUnparseable() throws ParseException {
		addResponseToEnv(new JsonObjectBuilder().addField("body", "ey1.b.c").build());
		getResponseFromEnv();
	}

	@Test
	@UseResurce("jsonResponses/mockResponses/sampleJson.json")
	public void jwtResponse()  {
		setJwt(true);
		run(new MockCondition());
		JsonObject response = environment.getObject("response");
		assertValues(response);
	}

	@Test
	public void plainEmptyResponse() throws ParseException {
		addResponseToEnv(new JsonObjectBuilder().addField("body", "{}").build());
		JsonObject response = getResponseBodyFromEnv();
		assertEquals(new JsonObject(), response);
	}

	@Test
	@UseResurce("jsonResponses/mockResponses/sampleJson.json")
	public void plainResponse()  {
		setJwt(false);
		run(new MockCondition());
		JsonObject response = environment.getObject("response");
		assertValues(response);
	}

	private Optional<JsonElement> getResponseFromEnv() throws ParseException {
		return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY);
	}

	private JsonObject getResponseBodyFromEnv() throws ParseException {
		return getResponseFromEnv().map(JsonElement::getAsJsonObject).orElse(new JsonObject());
	}

	private void addResponseToEnv(JsonObject object) {
		environment.putObject(RESPONSE_ENV_KEY, object);
	}

	private void assertValues(JsonObject response) {
		assertValuesBaseObject(response);

		JsonObject nestedObject = response.getAsJsonObject("nestedObject");
		assertValuesBaseObject(nestedObject);

		JsonArray arrayOfArrays = response.getAsJsonArray("arrayOfArrays");
		assertNumberArray(arrayOfArrays.get(0).getAsJsonArray());
		assertNumberArray(arrayOfArrays.get(1).getAsJsonArray());

		JsonArray objectArray = response.getAsJsonArray("objectArray");
		assertValuesBaseObject(objectArray.get(0).getAsJsonObject());
		assertValuesBaseObject(objectArray.get(1).getAsJsonObject());
	}

	private void assertValuesBaseObject(JsonObject baseObject) {
		String string = OIDFJSON.getString(baseObject.get("string"));
		assertEquals("string", string);

		int integer = OIDFJSON.getInt(baseObject.get("int"));
		assertEquals(5, integer);

		boolean bool = OIDFJSON.getBoolean(baseObject.get("boolean"));
		assertTrue(bool);

		double floating = OIDFJSON.getDouble(baseObject.get("double"));
		assertEquals(1.5, floating, 0.0);

		JsonObject emptyObject = baseObject.getAsJsonObject("emptyObject");
		assertTrue(emptyObject.isEmpty());

		JsonArray emptyArray = baseObject.getAsJsonArray("emptyArray");
		assertTrue(emptyArray.isEmpty());

		JsonArray numberArray = baseObject.getAsJsonArray("numberArray");
		assertNumberArray(numberArray);
	}

	private void assertNumberArray(JsonArray array) {
		assertEquals(1, OIDFJSON.getInt(array.get(0)));
		assertEquals(2, OIDFJSON.getInt(array.get(1)));
		assertEquals(3, OIDFJSON.getInt(array.get(2)));
	}

	static class MockCondition extends AbstractCondition {

		@Override
		public Environment evaluate(Environment env) {
			try {
				JsonObject response = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.map(JsonElement::getAsJsonObject)
					.orElseThrow(() -> error("Could not extract body"));
				env.putObject("response", response);
			} catch (ParseException e) {
				throw error("Could not parse");
			}
			return env;
		}
	}
}
