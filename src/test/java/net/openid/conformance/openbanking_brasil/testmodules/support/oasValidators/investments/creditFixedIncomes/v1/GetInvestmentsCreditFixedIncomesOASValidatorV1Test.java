package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.investments.creditFixedIncomes.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetInvestmentsCreditFixedIncomesOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFixedIncomeCreditResponseV1.json")
	public void happyPath() {
		run(new GetInvestmentsCreditFixedIncomesOASValidatorV1());
	}

}
