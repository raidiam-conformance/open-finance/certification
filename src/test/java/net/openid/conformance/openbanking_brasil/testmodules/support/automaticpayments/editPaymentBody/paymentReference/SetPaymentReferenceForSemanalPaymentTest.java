package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.paymentReference;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForSemanalPayment;
import org.junit.Test;

public class SetPaymentReferenceForSemanalPaymentTest extends AbstractSetPaymentReferenceTest {

	@Override
	protected AbstractSetPaymentReference condition() {
		return new SetPaymentReferenceForSemanalPayment();
	}

	@Test
	public void happyPathTestDayInNextYear() {
		happyPathTest("2024-12-30", "W1-2025");
	}

	@Test
	public void happyPathTestDayInPreviousYear() {
		happyPathTest("2027-01-01", "W53-2026");
	}

	@Test
	public void happyPathTestExampleWeekPlusBorder() {
		happyPathTest("2025-01-12", "W2-2025");
		happyPathTest("2025-01-13", "W3-2025");
		happyPathTest("2025-01-14", "W3-2025");
		happyPathTest("2025-01-15", "W3-2025");
		happyPathTest("2025-01-16", "W3-2025");
		happyPathTest("2025-01-17", "W3-2025");
		happyPathTest("2025-01-18", "W3-2025");
		happyPathTest("2025-01-19", "W3-2025");
		happyPathTest("2025-01-20", "W4-2025");
	}
}
