package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class CreateExtensionRequestTimeDayPlus60Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentGood.json")
	public void creatingExtensionRequestTimeDayPlus60() {

		Instant expiryTime = Instant.now();
		Instant currentTime = expiryTime.truncatedTo(ChronoUnit.SECONDS);
		CreateExtensionRequestTimeDayPlus60 condition = new CreateExtensionRequestTimeDayPlus60();
		run(condition);

		JsonObject expiryTimes = environment.getObject("extension_expiry_times");
		JsonArray expiryTimesArray = expiryTimes.getAsJsonArray("extension_times");

		Instant generatedTime = Instant.parse(OIDFJSON.getString(expiryTimesArray.get(0)));

		Duration duration = Duration.between(currentTime, generatedTime);
		long durationBetweenExpectedAndGenerated = duration.toDays();

		String savedConsentExtensionExpirationTime = environment.getString("saved_consent_extension_expiration_time");

		assertEquals(60, durationBetweenExpectedAndGenerated);
		assertNull(savedConsentExtensionExpirationTime);

	}
}
