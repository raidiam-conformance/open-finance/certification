package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetAccountTransactionsOASValidatorV2n4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/account/v2n/getAccountTransactionsResponseOAS.json")
	public void testHappyPath() {
		GetAccountTransactionsOASValidatorV2n4 cond = new GetAccountTransactionsOASValidatorV2n4();
		run(cond);

		// Partie Cnpj Cpf constraint.
		JsonObject folhaPagamentoData = new JsonObject();
		folhaPagamentoData.addProperty("type", "FOLHA_PAGAMENTO");
		folhaPagamentoData.addProperty("partieCnpjCpf", "87517400444");

	}

	@Test
	@UseResurce("jsonResponses/errors/goodErrorResponseNoLinksNoMeta.json")
	public void testHappyPathCode403() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 403);
		GetAccountTransactionsOASValidatorV2n4 cond = new GetAccountTransactionsOASValidatorV2n4();
		run(cond);
	}
}
