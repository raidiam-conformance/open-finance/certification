package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject.AbstractEditRecurringPaymentsAutomaticConsentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject.EditRecurringPaymentsAutomaticConsentBodyToAddMinHigherThanMax;
import net.openid.conformance.testmodule.OIDFJSON;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentsAutomaticConsentBodyToAddMinHigherThanMaxTest extends AbstractEditRecurringPaymentsAutomaticConsentBodyTest {

	@Override
	protected AbstractEditRecurringPaymentsAutomaticConsentBody condition() {
		return new EditRecurringPaymentsAutomaticConsentBodyToAddMinHigherThanMax();
	}

	@Override
	protected void assertAutomaticObject(JsonObject automatic) {
		assertTrue(automatic.has("maximumVariableAmount"));
		assertTrue(automatic.has("minimumVariableAmount"));
		assertFalse(automatic.has("fixedAmount"));

		String maximum = OIDFJSON.getString(automatic.get("maximumVariableAmount"));
		String minimum = OIDFJSON.getString(automatic.get("minimumVariableAmount"));

		assertTrue(Double.parseDouble(maximum) < Double.parseDouble(minimum));
	}
}
