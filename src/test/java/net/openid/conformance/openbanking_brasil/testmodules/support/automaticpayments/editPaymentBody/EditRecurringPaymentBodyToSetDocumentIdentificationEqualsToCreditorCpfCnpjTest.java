package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpjTest extends AbstractJsonResponseConditionUnitTest {

	private static final String JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBodyV2.json";
	private static final String KEY = "resource";
	private static final String PAYMENT_PATH = "brazilPixPayment";
	private static final String CREDITOR_IDENTIFICATION_PATH = "creditorCpfCnpj";

	private static final String CPF = "12345678901";
	private static final String CNPJ = "12345678901234";

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PAYMENT_PATH)
	public void happyPathTestCpf() {
		happyPathTest(CPF, "CPF");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PAYMENT_PATH)
	public void happyPathTestCnpj() {
		happyPathTest(CNPJ, "CNPJ");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PAYMENT_PATH)
	public void unhappyPathTestMissingCreditorCpfCnpj() {
		unhappyPathTest("Could not find creditorCpfCnpj in the resource");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PAYMENT_PATH)
	public void unhappyPathTestInvalidCreditorCpfCnpj() {
		addCreditorCpfCnpjToTheEnvironment("123");
		unhappyPathTest("The value in creditorCpfCnpj should be either a CPF or a CNPJ");
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		addCreditorCpfCnpjToTheEnvironment(CPF);
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		addCreditorCpfCnpjToTheEnvironment(CPF);
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	private void happyPathTest(String expectedIdentification, String expectedRel) {
		addCreditorCpfCnpjToTheEnvironment(expectedIdentification);
		run(new EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj());

		JsonObject document = environment.getElementFromObject(KEY, PAYMENT_PATH + ".data.document").getAsJsonObject();
		assertEquals(expectedIdentification, OIDFJSON.getString(document.get("identification")));
		assertEquals(expectedRel, OIDFJSON.getString(document.get("rel")));
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj());
		assertTrue(error.getMessage().contains(message));
	}

	private void addCreditorCpfCnpjToTheEnvironment(String creditorCpfCnpj) {
		environment.putString(KEY, CREDITOR_IDENTIFICATION_PATH, creditorCpfCnpj);
	}
}
