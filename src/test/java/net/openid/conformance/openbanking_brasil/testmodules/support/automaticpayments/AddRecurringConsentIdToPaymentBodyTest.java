package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.AddRecurringConsentIdToPaymentBody;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddRecurringConsentIdToPaymentBodyTest extends AbstractJsonResponseConditionUnitTest {

	private static final String CONSENT_ID = "urn:bancoex:C1DD33123";

	@Before
	public void init() {
		environment.putString("consent_id", CONSENT_ID);
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentMissingConsentId.json", key="resource", path="brazilPixPayment")
	public void happyPathTest() {
		AddRecurringConsentIdToPaymentBody condition = new AddRecurringConsentIdToPaymentBody();
		run(condition);
		String recurringConsentId = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject()
				.getAsJsonObject("data").get("recurringConsentId")
		);
		assertEquals(CONSENT_ID, recurringConsentId);
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		environment.putObject("resource", new JsonObject());
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		AddRecurringConsentIdToPaymentBody condition = new AddRecurringConsentIdToPaymentBody();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Unable to find data in payments payload"));
	}
}
