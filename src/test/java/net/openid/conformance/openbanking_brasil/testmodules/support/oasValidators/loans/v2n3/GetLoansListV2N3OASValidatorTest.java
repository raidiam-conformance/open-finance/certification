package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetLoansListV2N3OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/list.json")
	public void happyPath() {
		run(new GetLoansListV2n3OASValidator());
	}

}
