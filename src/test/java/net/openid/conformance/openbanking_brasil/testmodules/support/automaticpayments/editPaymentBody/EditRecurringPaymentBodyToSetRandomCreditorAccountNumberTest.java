package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetRandomCreditorAccountNumber;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentBodyToSetRandomCreditorAccountNumberTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBody";
	protected static final String KEY = "resource";
	protected static final String PATH = "brazilPixPayment";

	@Test
	@UseResurce(value = BASE_JSON_PATH + "V2.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(new EditRecurringPaymentBodyToSetRandomCreditorAccountNumber());
		String originalNumber = "1234567890";
		String newNumber = OIDFJSON.getString(environment.getElementFromObject(KEY, PATH + ".data.creditorAccount.number"));
		assertNotEquals(originalNumber, newNumber);
		assertTrue(!newNumber.isEmpty() && newNumber.length() <= 20);
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "MissingCreditorAccountV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingCreditorAccount() {
		unhappyPathTest("Unable to find creditorAccount in payments data");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentBodyToSetRandomCreditorAccountNumber());
		assertTrue(error.getMessage().contains(message));
	}
}
