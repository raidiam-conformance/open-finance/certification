package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplidiedConsents;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpirationTest extends AbstractJsonResponseConditionUnitTest {

	private static final String JSON_PATH = "jsonResponses/tokenEndpointResponse/TokenEndpointResponseWith";
	private static final String KEY = "token_endpoint_response";

	@Test
	@UseResurce(value = JSON_PATH + "OpaqueRefreshToken.json", key = KEY)
	public void happyPathTestOpaque() {
		run(new EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration());
	}

	@Test
	@UseResurce(value = JSON_PATH + "JWTRefreshTokenMissingExp.json", key = KEY)
	public void happyPathTestJWTWithoutExp() {
		run(new EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration());
	}

	@Test
	@UseResurce(value = JSON_PATH + "JWTRefreshTokenWithIndefiniteExp.json", key = KEY)
	public void happyPathTestJWTWithIndefiniteExp() {
		run(new EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration());
	}

	@Test
	@UseResurce(value = JSON_PATH + "JWTRefreshTokenWithIndefiniteExpEvenFurther.json", key = KEY)
	public void happyPathTestJWTWithIndefiniteExpEvenFurther() {
		run(new EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration());
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingTokenEndpointResponse() {
		run(new EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration());
	}

	@Test
	@UseResurce(value = JSON_PATH + "outRefreshToken.json", key = KEY)
	public void unhappyPathTestMissingRefreshToken() {
		unhappyPathTest("Couldn't find refresh token");
	}

	@Test
	@UseResurce(value = JSON_PATH + "JWTRefreshTokenWithDefiniteExp.json", key = KEY)
	public void unhappyPathTestExpEarlierThanIndefinite() {
		unhappyPathTest("Refresh token is a JWT with 'exp' field earlier than 01-01-2300");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration());
		assertTrue(error.getMessage().contains(message));
	}
}
