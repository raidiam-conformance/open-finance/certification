package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsForCancellationRequestBody;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CreatePatchRecurringPaymentsForCancellationRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/consents/v2/brazilPaymentConsentWithAutomaticField";
	protected static final String KEY = "resource";
	protected static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithoutBusinessEntityV2.json", key = KEY, path = PATH)
	public void happyPathTestWithoutBusinessEntity() {
		happyPathTest(false);
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "V2.json", key = KEY, path = PATH)
	public void happyPathTestWithBusinessEntity() {
		happyPathTest(true);
	}

	@Test
	public void happyPathTestWithoutBusinessEntityDataFromResponse() {
		environment.putObject(KEY, PATH, new JsonObject());
		JsonObject resource = new JsonObject();
		resource.addProperty("loggedUserIdentification","11111111111");
		environment.putObject("resource", resource);
		happyPathTest(false);
	}

	@Test
	public void happyPathTestWithBusinessEntityDataFromResponse() {
		environment.putObject(KEY, PATH, new JsonObject());
		JsonObject resource = new JsonObject();
		resource.addProperty("loggedUserIdentification","11111111111111");
		environment.putObject("resource", resource);
		happyPathTest(true);
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsentAndInvalidLoggedUserId() {
		environment.putObject(KEY, PATH, new JsonObject());
		JsonObject resource = new JsonObject();
		resource.addProperty("loggedUserIdentification","1");
		environment.putObject("resource", resource);
		unhappyPathTest("POST recurring consents request data missing and invalid loggedUserIdentification: must be either 11 or 14 characters");
	}


	@Test
	public void unhappyPathTestMissingBrazilPaymentConsentAndMissingLoggedUserId() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Unable to find POST recurring consents request data or loggedUserIdentification");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest("Unable to find POST recurring consents request data or loggedUserIdentification");
	}

	protected void happyPathTest(boolean withBusinessEntity) {
		run(new CreatePatchRecurringPaymentsForCancellationRequestBody());

		String expectedIdentification = withBusinessEntity ? "11111111111111" : "11111111111";
		String expectedRel = withBusinessEntity ? "CNPJ" : "CPF";

		JsonObject requestBody = environment.getObject("resource_request_entity_claims");

		JsonObject data = assertObjectField(requestBody, "data");
		assertStringField(data, "status", "CANC");

		JsonObject cancellation = assertObjectField(data, "cancellation");
		JsonObject cancelledBy = assertObjectField(cancellation, "cancelledBy");
		JsonObject document = assertObjectField(cancelledBy, "document");
		assertStringField(document, "identification", expectedIdentification);
		assertStringField(document, "rel", expectedRel);
	}

	protected JsonObject assertObjectField(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonObject());
		return obj.getAsJsonObject(field);
	}

	protected void assertStringField(JsonObject obj, String field, String expectedValue) {
		assertTrue(obj.has(field) && obj.get(field).isJsonPrimitive() && obj.get(field).getAsJsonPrimitive().isString());
		String value = OIDFJSON.getString(obj.get(field));
		assertEquals(expectedValue, value);
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new CreatePatchRecurringPaymentsForCancellationRequestBody());
		assertTrue(error.getMessage().contains(message));
	}
}
