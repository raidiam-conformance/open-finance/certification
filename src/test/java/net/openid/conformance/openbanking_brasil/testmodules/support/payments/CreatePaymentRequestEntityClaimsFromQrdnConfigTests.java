package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreatePaymentRequestEntityClaimsFromQrdnConfigTests extends AbstractJsonResponseConditionUnitTest {

	private static final String CNPJ = "43142666000197";
	private static final String REMITTANCE_INFO = DictHomologKeys.PROXY_QRDN_EDIT_PAYMENT_REMITTANCE_INFORMATION;
	private static final String TRANSACTION_ID = "E00038166201907261559y6j6";
	private static final String MISSING_FIELD_MESSAGE = "Unable to find object in JSON";
	private static final String CONSENT_PATH = "jsonEnvironmentObjects/qrdnPaymentConsent/correctQrdnPaymentConsent.json";

	@Before
	public void init() throws IOException {
		environment.putString("resource", "brazilQrdnCnpj", CNPJ);
		environment.putString("resource", "brazilQrdnRemittance", REMITTANCE_INFO);
		environment.putString("resource", "transactionIdentifier", TRANSACTION_ID);

		JsonObject consent = JsonParser.parseString(
			IOUtils.resourceToString(CONSENT_PATH, Charset.defaultCharset(), getClass().getClassLoader())
		).getAsJsonObject();
		environment.putObject("resource", "brazilQrdnPaymentConsent", consent);
	}

	@Test
	public void happyPathTestObject() {
		happyPathTest(false);
	}

	@Test
	public void happyPathTestArray() {
		happyPathTest(true);
	}

	@Test
	public void happyPathTestMissingOptionals() {
		JsonObject consent = environment.getElementFromObject("resource", "brazilQrdnPaymentConsent")
			.getAsJsonObject();
		removePath(consent, "data.ibgeTownCode");
		environment.getObject("resource").remove("transactionIdentifier");
		happyPathTest(false);
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		unhappyPathTest("As 'payments' is included in the 'scope' within the test configuration, "+
			"a payment consent request JSON object must also be provided in the test configuration.", null);
	}

	@Test
	public void unhappyPathTestMissingData() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data");
	}

	@Test
	public void unhappyPathTestMissingPayment() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment");
	}

	@Test
	public void unhappyPathTestMissingDetails() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.details");
	}

	@Test
	public void unhappyPathTestMissingCreditorAccount() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.details.creditorAccount");
	}

	@Test
	public void unhappyPathTestMissingProxy() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.details.proxy");
	}

	@Test
	public void unhappyPathTestMissingQrCode() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.details.qrCode");
	}

	@Test
	public void unhappyPathTestMissingAmount() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.amount");
	}

	@Test
	public void unhappyPathTestMissingCurrency() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.currency");
	}

	@Test
	public void unhappyPathTestMissingIspb() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.details.creditorAccount.ispb");
	}

	@Test
	public void unhappyPathTestMissingIssuer() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.details.creditorAccount.issuer");
	}

	@Test
	public void unhappyPathTestMissingNumber() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.details.creditorAccount.number");
	}

	@Test
	public void unhappyPathTestMissingAccountType() {
		unhappyPathTest(MISSING_FIELD_MESSAGE, "data.payment.details.creditorAccount.accountType");
	}


	protected void happyPathTest(boolean isArray) {
		if (isArray) {
			environment.putString("payment_is_array", "ok");
		}
		CreatePaymentRequestEntityClaimsFromQrdnConfig condition = new CreatePaymentRequestEntityClaimsFromQrdnConfig();
		run(condition);

		String endToEndId = environment.getString("endToEndId");
		JsonElement paymentData = environment.getObject("resource_request_entity_claims").get("data");
		JsonObject consentPayload = environment.getElementFromObject("resource", "brazilQrdnPaymentConsent")
			.getAsJsonObject();

		if (isArray) {
			for (JsonElement paymentElement : paymentData.getAsJsonArray()) {
				assertPayment(paymentElement.getAsJsonObject(), consentPayload, endToEndId);
			}
		} else {
			assertPayment(paymentData.getAsJsonObject(), consentPayload, endToEndId);
		}
	}

	protected void unhappyPathTest(String expectedMessage, String path) {
		if (path != null) {
			JsonObject consent = environment.getElementFromObject("resource", "brazilQrdnPaymentConsent")
				.getAsJsonObject();
			removePath(consent, path);
		} else {
			environment.getObject("resource").remove("brazilQrdnPaymentConsent");
		}


		CreatePaymentRequestEntityClaimsFromQrdnConfig condition = new CreatePaymentRequestEntityClaimsFromQrdnConfig();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	private void removePath(JsonObject jsonObject, String path) {
		String[] keys = path.split("\\.");
		JsonObject currentObject = jsonObject;
		for (int i = 0; i < keys.length - 1; i++) {
			currentObject = currentObject.get(keys[i]).getAsJsonObject();
		}
		currentObject.remove(keys[keys.length - 1]);
	}

	private void assertPayment(JsonObject data, JsonObject consentPayload, String expectedEndToEndId) {
		JsonObject consentPayment = consentPayload.getAsJsonObject("data").getAsJsonObject("payment");
		JsonObject consentPaymentDetails = consentPayment.getAsJsonObject("details");
		JsonObject consentCreditorAccount = consentPaymentDetails.getAsJsonObject("creditorAccount");

		assertStringField(data, "endToEndId", expectedEndToEndId);
		assertStringField(data, "localInstrument", "QRDN");
		assertStringField(data, "cnpjInitiator", CNPJ);
		assertStringField(data, "proxy", OIDFJSON.getString(consentPaymentDetails.get("proxy")));
		assertStringField(data, "qrCode", OIDFJSON.getString(consentPaymentDetails.get("qrCode")));

		JsonElement ibgeTownCodeElement = data.get("ibgeTownCode");
		if (ibgeTownCodeElement != null) {
			assertEquals(OIDFJSON.getString(consentPayment.get("ibgeTownCode")), OIDFJSON.getString(ibgeTownCodeElement));
		}

		JsonElement transactionIdentifierElement = data.get("transactionIdentifier");
		if (transactionIdentifierElement != null) {
			assertEquals(TRANSACTION_ID, OIDFJSON.getString(transactionIdentifierElement));
		}

		JsonObject creditorAccount = assertObjectField(data, "creditorAccount");
		assertEquals(consentCreditorAccount, creditorAccount);

		JsonObject payment = assertObjectField(data, "payment");
		assertStringField(payment, "amount", OIDFJSON.getString(consentPayment.get("amount")));
		assertStringField(payment, "currency", OIDFJSON.getString(consentPayment.get("currency")));
	}

	private void assertStringField(JsonObject obj, String fieldName, String expectedValue) {
		assertTrue(
			obj.has(fieldName) && obj.get(fieldName).isJsonPrimitive() && obj.get(fieldName).getAsJsonPrimitive().isString()
		);
		assertEquals(expectedValue, OIDFJSON.getString(obj.get(fieldName)));
	}

	private JsonObject assertObjectField(JsonObject obj, String fieldName) {
		assertTrue(obj.has(fieldName) && obj.get(fieldName).isJsonObject());
		return obj.getAsJsonObject(fieldName);
	}
}
