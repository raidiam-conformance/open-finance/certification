package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddSavedTransactionDateTimeAsBookingParamTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPath() {
		AddSavedTransactionDateTimeAsBookingParam condition = new AddSavedTransactionDateTimeAsBookingParam();
		environment.putString("config", "resource.resourceUrl", "https://example.com/accounts/v1/transactions?fromBookingDate=%s&toBookingDate=%s");
		environment.putString("transactionDateTime", "2023-04-16T10:52:00.000Z");
		environment.putString("accountId", "123456");

		run(condition);

		assertEquals("2023-04-16", environment.getString("fromBookingDate"));
		assertEquals("2023-04-16", environment.getString("toBookingDate"));
	}
}
