package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FinancingContractSelectorTest extends AbstractJsonResponseConditionUnitTest {


	@Test
	@UseResurce("jsonResponses/creditOperations/financing/financingV2/financingResponse/financingResponse.json")
	public void testHappyPath() {
		FinancingContractSelector condition = new FinancingContractSelector();
		run(condition);
		String contractId = environment.getString("contractId");
		assertEquals("92792126019929279212650822221989319252576", contractId);
	}

	@Test
	public void testUnhappyPathNoBody() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		FinancingContractSelector condition = new FinancingContractSelector();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "Could not extract body from resource endpoint response";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce("jsonResponses/creditOperations/financing/financingV2/financingResponse/financingResponseEmptyData.json")
	public void testUnhappyPathEmptyData() {
		FinancingContractSelector condition = new FinancingContractSelector();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "Data field is empty, no further processing required.";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
