package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtractSoftwareClientNameFromSSATest extends AbstractJsonResponseConditionUnitTest {
	@Test
	public void happyPathTest() {
		String clientName = "Raidiam Mockbank - Pipeline NRJ";
		environment.putObject(
			"software_statement_assertion",
			new JsonObjectBuilder().addField("claims.software_client_name", clientName).build()
		);
		ExtractSoftwareClientNameFromSSA condition = new ExtractSoftwareClientNameFromSSA();
		run(condition);
		assertEquals(clientName, environment.getString("software_client_name"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingSSA() {
		ExtractSoftwareClientNameFromSSA condition = new ExtractSoftwareClientNameFromSSA();
		run(condition);
	}

	@Test
	public void unhappyPathTestMissingName() {
		environment.putObject("software_statement_assertion", new JsonObject());
		unhappyPathTest("There is no software_client_name field inside the SSA");
	}

	@Test
	public void unhappyPathTestEmptyName() {
		environment.putObject(
			"software_statement_assertion",
			new JsonObjectBuilder().addField("claims.software_client_name", "").build()
		);
		unhappyPathTest("software_client_name is empty");
	}

	private void unhappyPathTest(String errorMessage) {
		ExtractSoftwareClientNameFromSSA condition = new ExtractSoftwareClientNameFromSSA();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(errorMessage));
	}
}
