package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAwaitingAuthorisation;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AbstractEnsureConsentStatusWasTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseAuthorised.json")
	public void happyPathTestAuthorised() {
		run(new EnsureConsentWasAuthorised());
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponse.json")
	public void unhappyPathTestAuthorisedNotMatching() {
		unhappyPathTest("Expected consent to be in the AUTHORISED state after redirect but it was not");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseAwaitingAuthorisation.json")
	public void happyPathTestAwaitingAuthorisation() {
		run(new EnsureConsentWasAwaitingAuthorisation());
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingConsentEndpointResponseFull() {
		run(new EnsureConsentWasAuthorised());
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("consent_endpoint_response_full", new JsonObject());
		unhappyPathTest("Could not extract status from consent response data");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseMissingStatus.json")
	public void unhappyPathTestMissingStatus() {
		unhappyPathTest("Could not extract status from consent response data");
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject("consent_endpoint_response_full",
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureConsentWasAuthorised());
		assertTrue(error.getMessage().contains(message));
	}
}
