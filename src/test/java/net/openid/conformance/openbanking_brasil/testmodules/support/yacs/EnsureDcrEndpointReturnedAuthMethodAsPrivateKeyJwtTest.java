package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureDcrEndpointReturnedAuthMethodAsPrivateKeyJwtTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		prepareDcrResponse("private_key_jwt");
		run(new EnsureDcrEndpointReturnedAuthMethodAsPrivateKeyJwt());
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingDcrEndpointResponse() {
		run(new EnsureDcrEndpointReturnedAuthMethodAsPrivateKeyJwt());
	}

	@Test
	public void unhappyPathTestMissingTokenEndpointAuthMethod() {
		prepareDcrResponse(null);
		unhappyPathTest("There is no body.token_endpoint_auth_method inside the DCR endpoint response");
	}

	@Test
	public void unhappyPathTestInvalidJwt() {
		environment.putObject("dynamic_registration_endpoint_response",
			new JsonObjectBuilder().addField("body", "eya1.b2.c3").build());
		unhappyPathTest("Could not parse body");
	}

	@Test
	public void unhappyPathTestAuthMethodIsNotPrivateKeyJwt() {
		prepareDcrResponse("tls_client_auth");
		unhappyPathTest("DCR endpoint did not return the 'token_endpoint_auth_method' field set as 'private_key_jwt'");
	}

	private void prepareDcrResponse(String authMethod) {
		JsonArray grantType = new JsonArray();
		grantType.add("client_credentials");
		grantType.add("authorization_code");
		grantType.add("refresh_token");
		grantType.add("implicit");

		JsonArray postLogoutRedirectUris = new JsonArray();

		JsonArray responseTypes = new JsonArray();
		responseTypes.add("code id_token");
		responseTypes.add("code");

		JsonArray redirectUris = new JsonArray();
		redirectUris.add("https://localhost.emobix.co.uk:8443/test/a/obbsb/callback");

		JsonObjectBuilder dcrResponseBodyBuilder = new JsonObjectBuilder()
			.addField("application_type", "web")
			.addField("grant_types", grantType)
			.addField("id_token_signed_response_alg", "PS256")
			.addField("post_logout_redirect_uris", postLogoutRedirectUris)
			.addField("require_auth_time", false)
			.addField("response_types", responseTypes)
			.addField("subject_type", "public")
			.addField("request_object_signing_alg", "PS256")
			.addField("require_signed_request_object", true)
			.addField("require_pushed_authorization_requests", true)
			.addField("authorization_signed_response_alg", "PS256")
			.addField("tls_client_certificate_bound_access_tokens", true)
			.addField("client_id_issued_at", "1720202753L")
			.addField("client_id", "123")
			.addField("client_name", "Example")
			.addField("client_uri", "https://www.example.com")
			.addField("default_max_age", 0)
			.addField("jwks_uri", "https://keystore.sandbox.directory.openbankingbrasil.org.br/123/123/application.jwks")
			.addField("logo_uri", "https://example.example.io/directory-ui/brand/obbrazil/1/favicon.svg")
			.addField("policy_uri", "https://www.fapi.new")
			.addField("redirect_uris", redirectUris)
			.addField("scope", "openid accounts credit-cards-accounts consents customers invoice-financings financings resources loans unarranged-accounts-overdraft bank-fixed-incomes credit-fixed-incomes variable-incomes treasure-titles funds recurring-payments exchanges nrp-consents payments")
			.addField("tls_client_auth_subject_dn", "UID=1,1.3.6.1.4.1.311.60.2.1.3=#2,2.5.4.15=#3,2.5.4.5=#4,CN=dev.conformance.directory.openbankingbrasil.org.b,OU=5,O=Open Banking Brasil,L=LONDON,ST=SP,C=BR")
			.addField("request_object_encryption_alg", "RSA-OAEP")
			.addField("request_object_encryption_enc", "A256GCM")
			.addField("id_token_encrypted_response_alg", "RSA-OAEP")
			.addField("id_token_encrypted_response_enc", "A256GCM")
			.addField("software_id", "123")
			.addField("client_description", "example")
			.addField("org_id", "123")
			.addField("org_name", "Example")
			.addField("org_number", "1")
			.addField("registration_client_uri", "https://matls-auth.example.poc.raidiam.io/reg/1")
			.addField("registration_access_token", "123");
		if (authMethod != null) {
			dcrResponseBodyBuilder
				.addField("token_endpoint_auth_method", authMethod)
				.addField("introspection_endpoint_auth_method", authMethod)
				.addField("revocation_endpoint_auth_method", authMethod);
		}

		JsonObject dcrResponseBody = dcrResponseBodyBuilder.build();

		environment.putObject("dynamic_registration_endpoint_response",
			new JsonObjectBuilder().addField("body", dcrResponseBody.toString()).build());
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureDcrEndpointReturnedAuthMethodAsPrivateKeyJwt());
		assertTrue(error.getMessage().contains(message));
	}
}
