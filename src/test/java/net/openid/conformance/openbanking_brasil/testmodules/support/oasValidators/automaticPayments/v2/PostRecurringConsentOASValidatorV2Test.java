package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostRecurringConsentOASValidatorV2Test extends AbstractRecurringConsentOASValidatorV2Test {

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201Response.json")
	public void happyPathTestCompleteConsentResponse() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseMissingOptionalFields.json")
	public void happyPathTestMissingOptionalFieldsConsentResponse() {
		run(validator());
	}

	@Override
	protected int status() {
		return 201;
	}

	@Override
	protected AbstractRecurringConsentOASValidatorV2 validator() {
		return new PostRecurringConsentOASValidatorV2();
	}
}
