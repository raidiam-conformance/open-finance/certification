package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToRemoveSweepingField;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentsConsentBodyToRemoveSweepingFieldTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithSweepingField.json", key=KEY, path=PATH)
	public void happyPathTest() {
		EditRecurringPaymentsConsentBodyToRemoveSweepingField condition = new EditRecurringPaymentsConsentBodyToRemoveSweepingField();
		run(condition);

		JsonObject recurringConfiguration = environment
			.getElementFromObject("resource", "brazilPaymentConsent.data.recurringConfiguration")
			.getAsJsonObject();
		assertFalse(recurringConfiguration.has("sweeping"));
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putObject("resource", new JsonObject());
		EditRecurringPaymentsConsentBodyToRemoveSweepingField condition = new EditRecurringPaymentsConsentBodyToRemoveSweepingField();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find recurringConfiguration field in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPaymentConsent", new JsonObject());
		EditRecurringPaymentsConsentBodyToRemoveSweepingField condition = new EditRecurringPaymentsConsentBodyToRemoveSweepingField();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find recurringConfiguration field in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentMissingRecurringConfiguration.json", key=KEY, path=PATH)
	public void unhappyPathTestMissingRecurringConfiguration() {
		EditRecurringPaymentsConsentBodyToRemoveSweepingField condition = new EditRecurringPaymentsConsentBodyToRemoveSweepingField();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find recurringConfiguration field in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
