package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RemoveProxyFromPaymentConfigTest extends AbstractJsonResponseConditionUnitTest {
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentWithCpf.json", key=KEY, path=PATH)
	public void happyPathSingleTest() {
		RemoveProxyFromPaymentConfig condition = new RemoveProxyFromPaymentConfig();
		run(condition);

		JsonElement proxy = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonObject("data").get("proxy");
		assertNull(proxy);
	}

	@Test
	@UseResurce(value="jsonRequests/payments/payments/paymentWithDataArrayWith5Payments.json", key=KEY, path=PATH)
	public void happyPathArrayTest() {
		RemoveProxyFromPaymentConfig condition = new RemoveProxyFromPaymentConfig();
		run(condition);

		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");

		for (int i = 0 ; i < data.size(); i++) {

			JsonElement proxy = data.get(i).getAsJsonObject().get("proxy");
			assertNull(proxy);
		}
	}
}
