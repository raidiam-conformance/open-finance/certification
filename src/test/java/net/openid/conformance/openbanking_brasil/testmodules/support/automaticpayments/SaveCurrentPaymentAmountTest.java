package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SaveCurrentPaymentAmount;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SaveCurrentPaymentAmountTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBody";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";

	private static final String EXPECTED_AMOUNT = "100000.12";

	@Test
	@UseResurce(value = BASE_JSON_PATH + "V2.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(new SaveCurrentPaymentAmount());
		String amount = environment.getString("payment_amount");
		assertEquals(EXPECTED_AMOUNT, amount);
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "MissingPaymentV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingPayment() {
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "MissingPaymentAmountV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingPaymentAmount() {
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new SaveCurrentPaymentAmount());
		assertTrue(error.getMessage().contains("Unable to find data.payment.amount in payments payload"));
	}
}
