package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PrepareToFetchPaymentTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_URL = "https://www.example.com/payments/v4/pix/payments/";
	private static final String PAYMENT_ID = "TXpRMU9UQTROMWhZV2xSU1FUazJSMDl";

	@Before
	public void init() {
		environment.putString("config", "resource.resourceUrl", BASE_URL);
	}

	@Test
	public void happyPathTest() {
		environment.putString("payment_id", PAYMENT_ID);
		PrepareToFetchPayment condition = new PrepareToFetchPayment();
		run(condition);
		assertEquals(BASE_URL + PAYMENT_ID, environment.getString("protected_resource_url"));
	}

	@Test
	public void unhappyPathTestMissingPaymentId() {
		PrepareToFetchPayment condition = new PrepareToFetchPayment();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "couldn't find string in environment: payment_id";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
