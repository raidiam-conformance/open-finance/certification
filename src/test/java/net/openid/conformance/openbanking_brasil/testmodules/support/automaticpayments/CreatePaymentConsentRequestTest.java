package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.CreatePaymentConsentRequest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreatePaymentConsentRequestTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithSweepingField.json", key=KEY, path=PATH)
	public void happyPathTest() {
		CreatePaymentConsentRequest condition = new CreatePaymentConsentRequest();
		run(condition);
		assertEquals(environment.getElementFromObject(KEY, PATH).getAsJsonObject(), environment.getObject("consent_endpoint_request"));
	}

	@Test
	public void unhappyPathTest() {
		environment.putObject("resource", new JsonObject());
		CreatePaymentConsentRequest condition = new CreatePaymentConsentRequest();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "As 'payments' is included in the 'scope' within the test configuration, a payment consent request JSON object must also be provided in the test configuration.";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
