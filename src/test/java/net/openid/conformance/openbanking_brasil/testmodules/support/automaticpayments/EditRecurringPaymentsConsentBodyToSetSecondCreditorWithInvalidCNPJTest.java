package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value=BASE_FILE_PATH + "brazilPaymentConsentWithSweepingField.json", key=KEY, path=PATH)
	public void happyPathTest() {
		EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ condition = new EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ();
		run(condition);

		JsonArray creditors = environment.getElementFromObject(KEY, PATH).getAsJsonObject()
			.getAsJsonObject("data")
			.getAsJsonArray("creditors");

		assertTrue(creditors.size() >= 2);
		assertNotEquals(creditors.get(0).getAsJsonObject().get("cpfCnpj"),
			creditors.get(1).getAsJsonObject().get("cpfCnpj"));
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putObject("resource", new JsonObject());
		EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ condition = new EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data.creditors in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPaymentConsent", new JsonObject());
		EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ condition = new EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data.creditors in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentMissingCreditors.json", key=KEY, path=PATH)
	public void unhappyPathTestMissingCreditors() {
		EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ condition = new EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data.creditors in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
