package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.setSweepingStartDateTime;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setSweepingStartDateTime.AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setSweepingStartDateTime.EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToOneHourInTheFuture;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToOneHourInTheFutureTest extends AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeTest {

	@Override
	protected AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTime condition() {
		return new EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToOneHourInTheFuture();
	}

	@Override
	protected LocalDateTime expectedDateTime() {
		return ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime().plusHours(1).truncatedTo(ChronoUnit.SECONDS);
	}
}
