package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;

public class GetConsentV3EndpointTest extends AbstractGetXFromAuthServerTest {

	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/consents/v3/consents";
	}

	@Override
	protected String getApiFamilyType() {
		return "consents";
	}

	@Override
	protected String getApiVersion() {
		return "3.0.0";
	}

	@Override
	protected boolean isResource() {
		return false;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetConsentV3Endpoint();
	}

}
