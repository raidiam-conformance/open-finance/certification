package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class OptionallyAllow401or422Test {

	protected Environment setStatus(Integer status) {
		Environment environment = new Environment();
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		environment.getObject("resource_endpoint_response_full").addProperty("status", status);
		return environment;
	}

	protected OptionallyAllow401or422 createCondition() {
		OptionallyAllow401or422 condition = new OptionallyAllow401or422();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		return condition;
	}

	@Test
	public void testResponseCodeIs401() {
		Environment environment = setStatus(401);

		OptionallyAllow401or422 condition = createCondition();
		condition.evaluate(environment);

		assertNull(environment.getString("status_422"));
	}

	@Test
	public void testResponseCodeIs422() {
		Environment environment = setStatus(422);

		OptionallyAllow401or422 condition = createCondition();
		condition.evaluate(environment);

		assertNull(environment.getString("status_401"));
	}

	@Test
	public void testResponseCodeIsNot401or422() {
		Environment environment = setStatus(200);

		OptionallyAllow401or422 condition = createCondition();
		Exception exception = assertThrows(ConditionError.class, () -> {
			condition.evaluate(environment);
		});

		String expectedErrorMessage = "endpoint returned an unexpected http status - either 401 or 422 accepted";
		assertTrue(exception.getMessage().contains(expectedErrorMessage));
	}

	@Test
	public void testResponseCodeIsMissing() {
		Environment environment = setStatus(null);

		OptionallyAllow401or422 condition = createCondition();
		Exception exception = assertThrows(Environment.UnexpectedTypeException.class, () -> {
			condition.evaluate(environment);
		});

		assertNull(environment.getString("422_status"));
	}

	@Test
	public void testResourceEndpointResponseFullMissingInTheEnvironment() {
		Environment environment = new Environment();

		OptionallyAllow401or422 condition = createCondition();
		Exception exception = assertThrows(NullPointerException.class, () -> {
			condition.evaluate(environment);
		});

		assertNull(environment.getString("401_status"));
	}
}
