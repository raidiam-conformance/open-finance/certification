package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.AbstractCondition;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddProductTypeToPhase2ConfigTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateThatProductTypeIsBusinessWhenBrazilCnpjIsProvided() {
		environment.putObject("config", new JsonObject());
		environment.putString("config", "resource.brazilCnpj", "cnpj_number");
		AbstractCondition cond = new AddProductTypeToPhase2Config();

		run(cond);

		assertEquals("business", environment.getString("config", "consent.productType"));
	}

	@Test
	public void validateThatProductTypeIsPersonalByDefault() {
		environment.putObject("config", new JsonObject());
		AbstractCondition cond = new AddProductTypeToPhase2Config();

		run(cond);

		assertEquals("personal", environment.getString("config", "consent.productType"));
	}

}
