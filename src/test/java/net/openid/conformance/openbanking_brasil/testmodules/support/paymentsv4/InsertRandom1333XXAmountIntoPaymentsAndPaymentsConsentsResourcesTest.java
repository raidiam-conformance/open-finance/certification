package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResourcesTest extends AbstractJsonResponseConditionUnitTest {

	private static final String CONSENT_FILE_PATH = "jsonRequests/payments/consents/correctPaymentConsentRequestWithoutSchedule.json";
	private static final String PAYMENT_ARRAY_FILE_PATH = "jsonRequests/payments/payments/paymentWithDataArrayWith5Payments.json";
	private static final String PAYMENT_OBJECT_FILE_PATH = "jsonRequests/payments/payments/paymentWithDataObject.json";
	private static final String KEY = "resource";
	private static final String CONSENT_PATH = "brazilPaymentConsent";
	private static final String PAYMENT_PATH = "brazilPixPayment";
	private static final String AMOUNT_REGEX = "1333\\.\\d\\d";

	@Test
	@UseResurce(value = CONSENT_FILE_PATH, key = KEY, path = CONSENT_PATH)
	@UseResurce(value = PAYMENT_OBJECT_FILE_PATH, key = KEY, path = PAYMENT_PATH)
	public void happyPathTestObject() {
		happyPathTest(true);
	}

	@Test
	@UseResurce(value = CONSENT_FILE_PATH, key = KEY, path = CONSENT_PATH)
	@UseResurce(value = PAYMENT_ARRAY_FILE_PATH, key = KEY, path = PAYMENT_PATH)
	public void happyPathTestArray() {
		happyPathTest(false);
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources condition = new InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Could not find data inside brazilPixPayment"));
	}

	protected void happyPathTest(boolean isObject) {
		InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources condition = new InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources();
		run(condition);

		String consentAmount = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.amount")
		);
		assertTrue(consentAmount.matches(AMOUNT_REGEX));

		if (isObject) {
			String paymentAmount = OIDFJSON.getString(
				environment.getElementFromObject("resource", "brazilPixPayment.data.payment.amount")
			);
			assertEquals(consentAmount, paymentAmount);
		} else {
			JsonArray paymentDataArray = environment.getElementFromObject("resource", "brazilPixPayment.data")
				.getAsJsonArray();
			for (JsonElement paymentElement : paymentDataArray) {
				String paymentAmount = OIDFJSON.getString(
					paymentElement.getAsJsonObject().getAsJsonObject("payment").get("amount")
				);
				assertEquals(consentAmount, paymentAmount);
			}
		}
	}
}
