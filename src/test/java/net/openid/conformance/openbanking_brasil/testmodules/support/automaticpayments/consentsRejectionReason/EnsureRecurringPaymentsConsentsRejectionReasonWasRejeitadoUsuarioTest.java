package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.consentsRejectionReason;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsConsentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentsConsentsRejectionReasonWasRejeitadoUsuario;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureRecurringPaymentsConsentsRejectionReasonWasRejeitadoUsuarioTest extends AbstractEnsureRecurringPaymentsConsentsRejectionReasonTest {

	@Override
	protected AbstractEnsureRecurringPaymentsConsentsRejectionReason condition() {
		return new EnsureRecurringPaymentsConsentsRejectionReasonWasRejeitadoUsuario();
	}

	@Override
	@Test
	@UseResurce(BASE_FILE_PATH + "RejectedFromIniciadoraRejectedByUsuario.json")
	public void happyPathTest() {
		super.happyPathTest();
	}
}
