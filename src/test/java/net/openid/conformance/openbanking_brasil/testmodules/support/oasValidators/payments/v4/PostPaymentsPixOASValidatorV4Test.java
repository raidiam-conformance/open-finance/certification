package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class PostPaymentsPixOASValidatorV4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(201);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixValidatorV4.json")
	public void happyPath() {
		run(new PostPaymentsPixOASValidatorV4());
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixValidatorV4NoConsentId.json")
	public void unhappyPathConsentId() {
		ConditionError e = runAndFail(new PostPaymentsPixOASValidatorV4());
		assertThat(e.getMessage(), containsString("consentId is required when authorisationFlow is [CIBA_FLOW, FIDO_FLOW]"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixValidatorV4NoProxy.json")
	public void unhappyPathProxy1() {
		ConditionError e = runAndFail(new PostPaymentsPixOASValidatorV4());
		assertThat(e.getMessage(), containsString("proxy is required when localInstrument is [INIC, DICT, QRDN, QRES]"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixValidatorV4ManuWithProxy.json")
	public void unhappyPathProxy2() {
		ConditionError e = runAndFail(new PostPaymentsPixOASValidatorV4());
		assertThat(e.getMessage(), containsString("proxy cannot be present when localInstrument has the value MANU"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixValidatorV4CreditorNoIssuer.json")
	public void unhappyPathCreditorNoIssuer() {
		ConditionError e = runAndFail(new PostPaymentsPixOASValidatorV4());
		assertThat(e.getMessage(), containsString("creditorAccount.issuer is required when creditorAccount.accountType is [CACC, SVGS]"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/PostPaymentsPixValidatorV4DebtorNoIssuer.json")
	public void unhappyPathDebtorNoIssuer() {
		ConditionError e = runAndFail(new PostPaymentsPixOASValidatorV4());
		assertThat(e.getMessage(), containsString("debtorAccount.issuer is required when debtorAccount.accountType is [CACC, SVGS]"));
	}
}
