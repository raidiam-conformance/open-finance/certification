package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.creditCards.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GetPersonalCreditCardsOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/creditCards/v1/GetPersonalCreditCards200Response.json")
	public void happyPath() {
		run(new GetPersonalCreditCardsOASValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/opendata/creditCards/v1/GetPersonalCreditCards200ResponseTypeMissingAdditionalInfo.json")
	public void unhappyPathTypeMissingAdditionalInfoWhenOutros() {
		unhappyPath("type", "OUTROS");
	}

	@Test
	@UseResurce("jsonResponses/opendata/creditCards/v1/GetPersonalCreditCards200ResponseNetworkMissingAdditionalInfo.json")
	public void unhappyPathNetworkMissingAdditionalInfoWhenOutros() {
		unhappyPath("network", "OUTRAS");
	}

	@Test
	@UseResurce("jsonResponses/opendata/creditCards/v1/GetPersonalCreditCards200ResponseCodeMissingAdditionalInfo.json")
	public void unhappyPathCodeMissingAdditionalInfoWhenOutros() {
		unhappyPath("code", "OUTROS");
	}

	protected void unhappyPath(String field, String fieldValue) {
		ConditionError error = runAndFail(new GetPersonalCreditCardsOASValidatorV1());
		assertTrue(error.getMessage().contains(String.format("%sAdditionalInfo is required when %s is %s", field, field, fieldValue)));
	}
}
