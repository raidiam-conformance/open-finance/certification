package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;


public class EditConsentsAuthoriseRequestBodyToAddRiskSignalsIsChargingTest extends AbstractJsonResponseConditionUnitTest {

	private static final String JSON_BASE_PATH = "jsonRequests/payments/enrollments/v2/postConsentsAuthoriseRequestWithout";
	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";

	@Test
	@UseResurce(value = JSON_BASE_PATH + "IsChargingV2.json", key = REQUEST_CLAIMS_KEY)
	public void happyPathTest() {
		run(new EditConsentsAuthoriseRequestBodyToAddRiskSignalsIsCharging());
		assertTrue(OIDFJSON.getBoolean(environment.getElementFromObject(REQUEST_CLAIMS_KEY, "data.riskSignals.isCharging")));
	}

	@Test
	public void unhappyPathTestMissingRequest() {
		assertThrowsExactly(AssertionError.class, () -> {
			run(new EditConsentsAuthoriseRequestBodyToAddRiskSignalsIsCharging());
		});
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(REQUEST_CLAIMS_KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "RiskSignalsV2.json", key = REQUEST_CLAIMS_KEY)
	public void unhappyPathTestMissingRiskSignals() {
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new EditConsentsAuthoriseRequestBodyToAddRiskSignalsIsCharging());
		assertTrue(error.getMessage().contains("Could not find riskSignals inside consents authorise request body"));
	}
}
