package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.client.MongoClient;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.extensions.yacs.ParticipantsService;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.info.TestPlanService;
import net.openid.conformance.logging.EventLog;
import net.openid.conformance.token.TokenService;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EnsureScopesCorrespondToTheOrganisationTests extends AbstractJsonResponseConditionUnitTest {

	@MockBean
	ParticipantsService participantsService;

	@MockBean
	MongoClient mongoClient;

	@MockBean
	ServerInfoTemplate serverInfoTemplate;

	@MockBean
	TestInfoService testInfoService;

	@MockBean
	TestPlanService testPlanService;

	@MockBean
	TokenService tokenService;

	@MockBean
	EventLog eventLog;

	@Before
	public void init() {
		environment.putObject("server",new JsonObject());
	}

	@Test
	public void noScopesSupported() throws IOException {
		mockParticipantsService("jsonResponses/participantsEndpoint/participantsResponseDADOS.json");
		EnsureScopesCorrespondToTheOrganisationRoles condition = new EnsureScopesCorrespondToTheOrganisationRoles();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("scopes_supported field missing from server response"));
	}

	@Test
	public void roleDADOSGood() throws IOException {
		mockParticipantsService("jsonResponses/participantsEndpoint/participantsResponseDADOS.json");
		addScopesToServer("DADOS");
		EnsureScopesCorrespondToTheOrganisationRoles condition = new EnsureScopesCorrespondToTheOrganisationRoles();
		run(condition);
	}

	@Test
	public void roleDADOSBad() throws IOException {
		mockParticipantsService("jsonResponses/participantsEndpoint/participantsResponseDADOS.json");
		addScopesToServer("CONTA");
		EnsureScopesCorrespondToTheOrganisationRoles condition = new EnsureScopesCorrespondToTheOrganisationRoles();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("The organisation does not have the necessary ROLE for at least one of its scopes"));
	}

	@Test
	public void roleCONTAGood() throws IOException {
		mockParticipantsService("jsonResponses/participantsEndpoint/participantsResponseCONTA.json");
		addScopesToServer("CONTA");
		EnsureScopesCorrespondToTheOrganisationRoles condition = new EnsureScopesCorrespondToTheOrganisationRoles();
		run(condition);
	}

	@Test
	public void roleCONTABad() throws IOException {
		mockParticipantsService("jsonResponses/participantsEndpoint/participantsResponseCONTA.json");
		addScopesToServer("DADOS");
		EnsureScopesCorrespondToTheOrganisationRoles condition = new EnsureScopesCorrespondToTheOrganisationRoles();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("The organisation does not have the necessary ROLE for at least one of its scopes"));
	}

	@Test
	public void rolePAGTOGood() throws IOException {
		mockParticipantsService("jsonResponses/participantsEndpoint/participantsResponseCONTA.json");
		addScopesToServer("PAGTO");
		EnsureScopesCorrespondToTheOrganisationRoles condition = new EnsureScopesCorrespondToTheOrganisationRoles();
		run(condition);
	}

	@Test
	public void rolePAGTOBad() throws IOException {
		mockParticipantsService("jsonResponses/participantsEndpoint/participantsResponsePAGTO.json");
		addScopesToServer("DADOS");
		EnsureScopesCorrespondToTheOrganisationRoles condition = new EnsureScopesCorrespondToTheOrganisationRoles();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("The organisation does not have the necessary ROLE for at least one of its scopes"));
	}

	private void mockParticipantsService(String url) throws IOException {
		String org = IOUtils.resourceToString(url, Charset.defaultCharset(), getClass().getClassLoader());
		Map all = new Gson().fromJson(org, Map.class);
		Mockito.when(participantsService.orgsFor(Set.of("org1"))).thenReturn(Set.of(all));
		environment.putString("config", "resource.brazilOrganizationId", "org1");
	}

	private void addScopesToServer(String select) {
		JsonObject server = environment.getObject("server");
		if (select.equals("DADOS")) {
			//DADOS
			JsonArray scopesSupported = new JsonArray();
			scopesSupported.add("accounts");
			scopesSupported.add("credit-cards-accounts");
			scopesSupported.add("consents");
			scopesSupported.add("customers");
			scopesSupported.add("invoice-financings");
			scopesSupported.add("financings");
			scopesSupported.add("loans");
			scopesSupported.add("unarranged-accounts-overdraft");
			scopesSupported.add("resources");
			scopesSupported.add("credit-fixed-incomes");
			scopesSupported.add("exchanges");
			server.add("scopes_supported",scopesSupported);
		}
		if (select.equals("CONTA")) {
			//CONTA
			JsonArray scopesSupported = new JsonArray();
			scopesSupported.add("payments");
			server.add("scopes_supported",scopesSupported);
		}
		if (select.equals("PAGTO")) {
			//PAGTO
			JsonArray scopesSupported = new JsonArray();
			server.add("scopes_supported",scopesSupported);
		}
	}


}
