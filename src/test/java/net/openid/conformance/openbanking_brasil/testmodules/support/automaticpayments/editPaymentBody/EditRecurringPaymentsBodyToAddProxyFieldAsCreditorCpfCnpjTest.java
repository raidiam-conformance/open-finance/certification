package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToAddProxyFieldAsCreditorCpfCnpj;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentsBodyToAddProxyFieldAsCreditorCpfCnpjTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBodyV2.json";
	protected static final String KEY = "resource";
	protected static final String PATH = "brazilPixPayment";
	protected static final String CREDITOR_CPF_CNPJ = "12345678901";

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	public void happyPathTest() {
		insertCreditorCpfCnpjIntoEnv();
		run(new EditRecurringPaymentsBodyToAddProxyFieldAsCreditorCpfCnpj());

		JsonObject data = environment.getElementFromObject(KEY, PATH + ".data").getAsJsonObject();
		assertEquals(CREDITOR_CPF_CNPJ, OIDFJSON.getString(data.get("proxy")));
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		insertCreditorCpfCnpjIntoEnv();
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		insertCreditorCpfCnpjIntoEnv();
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	public void unhappyPathTestMissingCreditorCpfCnpj() {
		unhappyPathTest("Unable to find creditor CPF/CNPJ in the resource");
	}

	private void insertCreditorCpfCnpjIntoEnv() {
		environment.putString(KEY, "creditorCpfCnpj", CREDITOR_CPF_CNPJ);
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentsBodyToAddProxyFieldAsCreditorCpfCnpj());
		assertTrue(error.getMessage().contains(message));
	}
}
