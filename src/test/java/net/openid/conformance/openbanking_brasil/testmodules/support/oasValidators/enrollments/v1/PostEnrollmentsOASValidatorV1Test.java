package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PostEnrollmentsOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/enrollments/PostEnrollmentsV1Ok.json")
	public void happyPathTest201() {
		happyPathTest(201);
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostEnrollments422Response.json")
	public void happyPathTest422() {
		happyPathTest(422);
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostEnrollmentsDebtorAccCACCMissingIssuer.json")
	public void unhappyPathTestDebtorAccountIssuerConstraint() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 201);
		ConditionError error = runAndFail(new PostEnrollmentsOASValidatorV1());
		assertTrue(error.getMessage().contains("issuer is required when accountType is [CACC, SVGS]"));
	}

	protected void happyPathTest(int status) {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", status);
		run(new PostEnrollmentsOASValidatorV1());
	}
}
