package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractOriginalRecurringPaymentIdFromListResponse;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ExtractOriginalRecurringPaymentIdFromListResponseTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/payments/GetRecurringPaymentsList200Response";

	@Test
	@UseResurce(BASE_JSON_PATH + ".json")
	public void happyPathTest() {
		run(new ExtractOriginalRecurringPaymentIdFromListResponse());
		assertNotNull(environment.getString("payment_id"));
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(ExtractOriginalRecurringPaymentIdFromListResponse.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(ExtractOriginalRecurringPaymentIdFromListResponse.RESPONSE_ENV_KEY,
			new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRecurringPaymentId.json")
	public void unhappyPathTestMissingRecurringPaymentId() {
		unhappyPathTest("Could not find recurringPaymentId in the original payment data");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new ExtractOriginalRecurringPaymentIdFromListResponse());
		assertTrue(error.getMessage().contains(message));
	}
}
