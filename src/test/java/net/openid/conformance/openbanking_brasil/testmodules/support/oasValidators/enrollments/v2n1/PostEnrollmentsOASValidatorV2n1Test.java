package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.AbstractEnrollmentsOASV2ValidatorsTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.AbstractEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostEnrollmentsOASValidatorV2;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;

public class PostEnrollmentsOASValidatorV2n1Test extends AbstractEnrollmentsOASV2n1ValidatorsTest {

	@Override
	protected AbstractEnrollmentsOASValidatorV2n1 validator() {
		return new PostEnrollmentsOASValidatorV2n1();
	}

	@Before
	public void init() {
		generatePostEnrollments201Response();
	}

	@Test
	public void happyPathTest201() {
		happyPathTest(201);
	}

	@Test
	public void happyPathTest201NoOptionalFields() {
		removeOptionalFields();
		happyPathTest(201);
	}

	@Test
	@UseResurce(JSON_PATH + "PostEnrollments422Response.json")
	public void happyPathTest422() {
		happyPathTest(422);
	}

	@Test
	public void unhappyPathTest201NoIssuerCacc() {
		removeIssuerAndSetAccountType("CACC");
		unhappyPathTest(201, "issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	public void unhappyPathTest201NoIssuerSvgs() {
		removeIssuerAndSetAccountType("SVGS");
		unhappyPathTest(201, "issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	public void unhappyPathTest201CreationDateTimeTooFarInThePast() {
		editTimeToBeTooFar("creationDateTime", true);
		unhappyPathTest(201, "The creationDateTime field value is too far from the current instant in UTC time");
	}

	@Test
	public void unhappyPathTest201CreationDateTimeTooFarInTheFuture() {
		editTimeToBeTooFar("creationDateTime", false);
		unhappyPathTest(201, "The creationDateTime field value is too far from the current instant in UTC time");
	}

	@Test
	public void unhappyPathTest201StatusUpdateDateTimeTooFarInThePast() {
		editTimeToBeTooFar("statusUpdateDateTime", true);
		unhappyPathTest(201, "The statusUpdateDateTime field value is too far from the current instant in UTC time");
	}

	@Test
	public void unhappyPathTest201StatusUpdateDateTimeTooFarInTheFuture() {
		editTimeToBeTooFar("statusUpdateDateTime", false);
		unhappyPathTest(201, "The statusUpdateDateTime field value is too far from the current instant in UTC time");
	}

	protected void generatePostEnrollments201Response() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZone(ZoneOffset.UTC);
		Instant currentTime = Instant.now();
		String currentTimeStr = formatter.format(currentTime);
		String expirationDateTimeStr = formatter.format(currentTime.plusSeconds(300));
		JsonArray permissions = new JsonArray();
		permissions.add("PAYMENTS_INITIATE");

		JsonObject response = new JsonObjectBuilder()
			.addFields("data", Map.of(
				"enrollmentId", "urn:bancoex:C1DD33123",
				"creationDateTime", currentTimeStr,
				"status", "AWAITING_RISK_SIGNALS",
				"statusUpdateDateTime", currentTimeStr,
				"permissions", permissions,
				"expirationDateTime", expirationDateTimeStr,
				"enrollmentName", "Nome Dispositivo"
			))
			.addFields("data.loggedUser.document", Map.of(
				"identification", "11111111111",
				"rel", "CPF"
			))
			.addFields("data.businessEntity.document", Map.of(
				"identification", "11111111111111",
				"rel", "CNPJ"
			))
			.addFields("data.debtorAccount", Map.of(
				"ispb", "12345678",
				"issuer", "1774",
				"number", "1234567890",
				"accountType", "CACC"
			))
			.addField("links.self", "https://api.banco.com.br/open-banking/api/v2/resource")
			.addField("meta.requestDateTime", currentTimeStr)
			.build();

		addResponseToEnv(response);
	}

	private void addResponseToEnv(JsonObject response) {
		environment.putString("resource_endpoint_response_full", "body", response.toString());
	}

	private JsonObject extractResponseFromEnv() {
		return JsonParser.parseString(environment.getString("resource_endpoint_response_full", "body")).getAsJsonObject();
	}

	private void removeOptionalFields() {
		JsonObject response = extractResponseFromEnv();
		JsonObject data = response.getAsJsonObject("data");
		data.remove("businessEntity");
		data.remove("debtorAccount");
		data.remove("enrollmentName");
		addResponseToEnv(response);
	}

	private void removeIssuerAndSetAccountType(String accountType) {
		JsonObject response = extractResponseFromEnv();
		JsonObject debtorAccount = response.getAsJsonObject("data").getAsJsonObject("debtorAccount");
		debtorAccount.remove("issuer");
		debtorAccount.addProperty("accountType", accountType);
		addResponseToEnv(response);
	}

	private void editTimeToBeTooFar(String dateTimeField, boolean isPast) {
		JsonObject response = extractResponseFromEnv();
		JsonObject data = response.getAsJsonObject("data");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZone(ZoneOffset.UTC);
		Instant currentTime = Instant.now();
		Instant dateTime = isPast ? currentTime.minus(2, ChronoUnit.HOURS) : currentTime.plus(2, ChronoUnit.HOURS);
		data.addProperty(dateTimeField, formatter.format(dateTime));
		addResponseToEnv(response);
	}
}
