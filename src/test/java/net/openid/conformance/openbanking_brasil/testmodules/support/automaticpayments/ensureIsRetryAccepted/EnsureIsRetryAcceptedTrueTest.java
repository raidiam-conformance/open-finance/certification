package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureIsRetryAccepted;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted.AbstractEnsureIsRetryAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted.EnsureIsRetryAcceptedTrue;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureIsRetryAcceptedTrueTest extends AbstractEnsureIsRetryAcceptedTest {

	@Override
	protected AbstractEnsureIsRetryAccepted condition() {
		return new EnsureIsRetryAcceptedTrue();
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "IsRetryAcceptedTrue200Response.json")
	public void happyPathTest() {
		run(condition());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void unhappyPathTestUnmatching() {
		unhappyPathTest("The isRetryAcceptedField field does not match the expected value");
	}
}
