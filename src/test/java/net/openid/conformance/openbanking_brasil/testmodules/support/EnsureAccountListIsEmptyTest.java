package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureAccountListIsEmptyTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/account/accountV2/listV2/accountListResponse_Empty.json")
	public void happyPath() {
		EnsureAccountListIsEmpty condition = new EnsureAccountListIsEmpty();
		run(condition);
	}
}
