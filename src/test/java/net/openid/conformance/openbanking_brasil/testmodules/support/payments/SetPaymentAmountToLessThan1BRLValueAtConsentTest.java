package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetPaymentAmountToLessThan1BRLValueAtConsentTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";
	private static final String expectedAmount = "0.75";

	@Test
	@UseResurce(value="jsonRequests/payments/consents/correctPaymentConsentRequestArrayCustomScheduleWith5Payments.json", key=KEY, path=PATH)
	public void happyPath() {
		SetPaymentAmountToLessThan1BRLValueAtConsent condition = new SetPaymentAmountToLessThan1BRLValueAtConsent();
		run(condition);

		String amount = OIDFJSON.getString(environment.getObject("resource").getAsJsonObject("brazilPaymentConsent").getAsJsonObject("data").getAsJsonObject("payment").get("amount"));
		assertEquals(expectedAmount, amount);
	}


}
