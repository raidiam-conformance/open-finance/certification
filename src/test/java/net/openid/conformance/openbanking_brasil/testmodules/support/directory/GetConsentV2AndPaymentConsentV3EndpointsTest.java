package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.ConditionError;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class GetConsentV2AndPaymentConsentV3EndpointsTest extends AbstractGetEndpointFromAuthServerTest {


	@Before
	public void init() {
		environment.putObject("config", "resource", new JsonObject());
	}


	@Test
	public void happyPathOnlyConsentIsPresent() {

		addApiResourcesToServer(List.of(
			createCorrectConsent()
		));
		run(getCondition());
		Assert.assertEquals(getConsentUrl(), environment.getString("config", "resource.consentUrl"));
		Assert.assertEquals(getConsentUrl(), environment.getString("config", "resource.consentUrl2"));
	}

	@Test
	public void happyPathOnlyPaymentConsentIsPresent() {

		addApiResourcesToServer(List.of(
			createCorrectPaymentConsent()
		));
		run(getCondition());
		Assert.assertEquals(getPaymentConsentUrl(), environment.getString("config", "resource.consentUrl"));
		Assert.assertEquals(getPaymentConsentUrl(), environment.getString("config", "resource.consentUrl2"));
	}

	@Test
	public void happyPathPaymentConsentAndConsentArePresent() {

		addApiResourcesToServer(List.of(
			createCorrectPaymentConsent(),
			createCorrectConsent()

		));
		run(getCondition());
		Assert.assertEquals(getConsentUrl(), environment.getString("config", "resource.consentUrl"));
		Assert.assertEquals(getPaymentConsentUrl(), environment.getString("config", "resource.consentUrl2"));
	}

	@Test
	public void unhappyPathNothingIsPresent() {
		addApiResourcesToServer(List.of());
		ConditionError e = runAndFail(getCondition());
		Assert.assertEquals(getErrorMessage("Unable to locate payment consent endpoint and consent endpoint"), e.getMessage());
	}


	private JsonObject createCorrectConsent() {
		return createApiResource("consents", "2.0.0", List.of(getConsentUrl()));
	}

	private String getConsentUrl() {
		return "https://consent.from.server/open-banking/consents/v2/consents";
	}

	private JsonObject createCorrectPaymentConsent() {
		return createApiResource("payments-consents", "3.0.0", List.of(getPaymentConsentUrl()));
	}

	private String getPaymentConsentUrl() {
		return "https://consent.from.server/open-banking/payments/v3/consents";
	}


	@Override
	protected AbstractGetEndpointFromAuthServer getCondition() {
		return new GetConsentV2AndPaymentConsentV3Endpoints();
	}
}
