package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractRecurringPaymentIdToPaymentIdArray;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExtractRecurringPaymentIdToPaymentIdArrayTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/payments/PostRecurringPayments201Response";

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(ExtractRecurringPaymentIdToPaymentIdArray.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(ExtractRecurringPaymentIdToPaymentIdArray.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(ExtractRecurringPaymentIdToPaymentIdArray.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build());
		unhappyPathTest("Could not find data.recurringPaymentId in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRecurringPaymentId.json")
	public void unhappyPathTestMissingRecurringPaymentId() {
		unhappyPathTest("Could not find data.recurringPaymentId in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + ".json")
	public void happyPathTestNoPreviousRecurringPaymentId() {
		run(new ExtractRecurringPaymentIdToPaymentIdArray());

		JsonArray recurringPaymentIdsArray = environment
			.getElementFromObject("recurringPaymentIds", "recurringPaymentIdsArray")
			.getAsJsonArray();
		assertEquals(1, recurringPaymentIdsArray.size());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + ".json")
	public void happyPathTestOnePreviousRecurringPaymentId() {
		JsonArray array = new JsonArray();
		array.add("id");
		environment.putObject("recurringPaymentIds", new JsonObjectBuilder().addField("recurringPaymentIdsArray", array).build());

		run(new ExtractRecurringPaymentIdToPaymentIdArray());

		JsonArray recurringPaymentIdsArray = environment
			.getElementFromObject("recurringPaymentIds", "recurringPaymentIdsArray")
			.getAsJsonArray();
		assertEquals(2, recurringPaymentIdsArray.size());
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new ExtractRecurringPaymentIdToPaymentIdArray());
		assertTrue(error.getMessage().contains(message));
	}
}
