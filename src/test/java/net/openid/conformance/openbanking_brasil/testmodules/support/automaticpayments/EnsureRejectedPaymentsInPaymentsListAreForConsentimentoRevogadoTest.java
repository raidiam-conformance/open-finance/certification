package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogado;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogadoTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/paymentListResponse/getRecurringPaymentResponseWith";

	@Test
	@UseResurce(BASE_JSON_PATH + "2RjctConsentimentoRevogadoAnd1Acsc.json")
	public void happyPathTestWithRjct() {
		run(new EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogado());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "3Payments.json")
	public void happyPathTestWithoutRjct() {
		run(new EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogado());
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject(EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogado.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogado.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Unable to find element data in the response payload");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "1RjctMissingRejectionReason.json")
	public void unhappyPathTestMissingRejectionReason() {
		unhappyPathTest("Unable to find rejection reason code for a payment with \"RJCT\" status");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "1RjctMissingRejectionReasonCode.json")
	public void unhappyPathTestMissingRejectionReasonCode() {
		unhappyPathTest("Unable to find rejection reason code for a payment with \"RJCT\" status");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "1RjctConsentimentoRevogado1RjctValorInvalido.json")
	public void unhappyPathTestRejectionReasonCodeNotMatching() {
		unhappyPathTest("Rejection reason code returned in the response does not match the expected rejection reason code");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogado());
		assertTrue(error.getMessage().contains(message));
	}
}
