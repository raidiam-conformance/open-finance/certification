package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.paymentReference;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class AbstractSetPaymentReferenceTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBody";
	protected static final String KEY = "resource";
	protected static final String PATH = "brazilPixPayment";

	protected abstract AbstractSetPaymentReference condition();

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "MissingDateV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingDate() {
		unhappyPathTest("Unable to find date inside payments payload");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}

	protected void happyPathTest(String date, String expectedPaymentReference) {
		insertPaymentWithDateInTheEnvironment(date);
		run(condition());
		String paymentReference = getPaymentReferenceFromEnvironment();
		assertEquals(expectedPaymentReference, paymentReference);
	}

	protected void insertPaymentWithDateInTheEnvironment(String date) {
		JsonObject payment = new JsonObjectBuilder().addField("data.date", date).build();
		environment.putObject(KEY, PATH, payment);
	}

	protected String getPaymentReferenceFromEnvironment() {
		return OIDFJSON.getString(environment.getElementFromObject(KEY, PATH + ".data.paymentReference"));
	}
}
