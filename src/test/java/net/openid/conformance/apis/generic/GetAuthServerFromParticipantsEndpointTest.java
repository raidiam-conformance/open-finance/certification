package net.openid.conformance.apis.generic;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mongodb.client.MongoClient;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.extensions.yacs.ParticipantsService;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.info.TestPlanService;
import net.openid.conformance.logging.EventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.token.TokenService;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GetAuthServerFromParticipantsEndpointTest extends AbstractJsonResponseConditionUnitTest{

	@MockBean
	ParticipantsService participantsService;

	@MockBean
	MongoClient mongoClient;


	@MockBean
	ServerInfoTemplate serverInfoTemplate;

	@MockBean
	TestInfoService testInfoService;

	@MockBean
	TestPlanService testPlanService;

	@MockBean
	TokenService tokenService;

	@MockBean
	EventLog eventLog;

	@Before
	public void init() throws IOException {
		JsonObject config = new JsonObject();
		environment.putObject("config", config);
		String org = IOUtils.resourceToString("jsonResponses/participantsEndpoint/participantsResponseMultipleServers.json", Charset.defaultCharset(), getClass().getClassLoader());
		List<Map> all = new Gson().fromJson(org, new TypeToken<List<Map>>(){}.getType());
		Mockito.when(participantsService.orgsFor(Set.of("org1"))).thenReturn(Set.copyOf(all));
		environment.putString("config", "resource.brazilOrganizationId", "org1");
	}

	@Test
	public void noAuthServersOnOrganization() throws IOException {
		environment.putString("config", "server.discoveryUrl","https://url0.com");
		GetAuthServerFromParticipantsEndpoint condition = new GetAuthServerFromParticipantsEndpoint();
		ConditionError error = runAndFail(condition);
		String expected = "Could not find Authorisation Server with provided Well-Known URL in the Directory Participants List";
		assertThat(error.getMessage(), containsString(expected));
	}
	@Test
	public void oneAuthServersOnOrganization() throws IOException {
		environment.putString("config", "server.discoveryUrl","https://url1.com");
		GetAuthServerFromParticipantsEndpoint condition = new GetAuthServerFromParticipantsEndpoint();
		run(condition);
	}
	//when 2 servers the first one will be overwritten

}
