package net.openid.conformance.apis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.DataUtils;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonLoadingJUnitRunner;
import net.openid.conformance.util.ResourceObject;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.http.HttpHeaders;

import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.text.ParseException;
import java.util.List;

import static net.openid.conformance.util.JsonUtils.configureJsonPathForGson;
import static org.mockito.Mockito.mock;

@RunWith(JsonLoadingJUnitRunner.class)
public abstract class AbstractJsonResponseConditionUnitTest implements DataUtils {

	protected JsonObject jsonObject;
	protected List<ResourceObject> objectsToAdd;

	protected Environment environment = new Environment();
	private HttpHeaders responseHeaders = new HttpHeaders();

	private boolean isJwt = false;

	protected String key;

	public AbstractJsonResponseConditionUnitTest() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		environment.putObject("consent_endpoint_response_full", new JsonObject());
	}

	protected void setHeaders(String headerName, String... values) {
		responseHeaders.addAll(headerName, List.of(values));
	}

	public void setJwt(boolean jwt) {
		isJwt = jwt;
	}

	protected void setStatus(int status) {
		JsonObject responseCode = new JsonObject();
		responseCode.addProperty("code", status);
		environment.putObject("resource_endpoint_response_code", responseCode);
		environment.getObject("resource_endpoint_response_full").addProperty("status", status);
		environment.getObject("consent_endpoint_response_full").addProperty("status", status);
		environment.putInteger("token_endpoint_response_http_status", status);
	}

	protected void run(final AbstractCondition condition) {
		enrichCondition(condition);

		try {
			condition.execute(environment);
		} catch (ConditionError error) {
			throw new AssertionError("Condition failed", error);
		}
	}

	protected ConditionError runAndFail(final AbstractCondition condition) {
		enrichCondition(condition);

		try {
			condition.execute(environment);
		} catch (ConditionError error) {
			return error;
		}
		throw new AssertionError("The condition passed but we expected it to fail");
	}

	private void addObjectToEnvironment(ResourceObject resourceObject) {
		if (resourceObject.isString()) {
			if (resourceObject.getPath() != null) {
				environment.putString(resourceObject.getKey(), resourceObject.getPath(), resourceObject.getStringValue());
			} else {
				environment.putString(resourceObject.getKey(), resourceObject.getStringValue());
			}
		} else {
			if (resourceObject.getPath() != null) {
				environment.putObject(resourceObject.getKey(), resourceObject.getPath(), resourceObject.getObjectValue());
			} else {
				environment.putObject(resourceObject.getKey(), resourceObject.getObjectValue());
			}
		}
	}

	private void enrichCondition(AbstractCondition condition) {
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		condition.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		Gson gson = new GsonBuilder()
			.serializeNulls()
			.create();

		if (objectsToAdd != null) {
			objectsToAdd.forEach(this::addObjectToEnvironment);
		}

		if (jsonObject != null) {
			String bodyString = gson.toJson(ifJsonWrapped(jsonObject));
			if (isJwt) {
				try {
					JWTClaimsSet claimSet = JWTClaimsSet.parse(bodyString);
					PrivateKey privateKey = KeyPairGenerator.getInstance("RSA").generateKeyPair().getPrivate();
					RSASSASigner signer = new RSASSASigner(privateKey);
					SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.RS256), claimSet);
					signedJWT.sign(signer);
					bodyString = signedJWT.serialize();

				} catch (ParseException e) {
					throw new AssertionError("Could not parse JSON to JWT Claim Set");
				} catch (NoSuchAlgorithmException | JOSEException e) {
					throw new AssertionError("Could sign JWT Claim Set");
				}
			} else {
				environment.putObject("consent_endpoint_response_full", "body_json", jsonObject);
				environment.putObject("resource_endpoint_response_full", "body_json", jsonObject);
			}

			environment.putString("resource_endpoint_response", bodyString);
			environment.putObject("consent_endpoint_response", jsonObject);
			environment.putString("resource_endpoint_response_full", "body", bodyString);
			environment.putString("consent_endpoint_response_full", "body", bodyString);
		}

		JsonObject headersObject = mapToJsonObject(responseHeaders, false);
		environment.putObject("resource_endpoint_response_headers", headersObject);
		environment.putObject("resource_endpoint_response_full", "headers", headersObject);
		environment.putObject("consent_endpoint_response_full", "headers", headersObject);
	}

	private JsonElement ifJsonWrapped(JsonObject jsonObject) {
		if (jsonObject.has(JsonLoadingJUnitRunner.WRAPPED)) {
			return jsonObject.get(JsonLoadingJUnitRunner.WRAPPED);
		}
		return jsonObject;
	}

	@BeforeClass
	public static void setup() {
		configureJsonPathForGson();
	}

}
