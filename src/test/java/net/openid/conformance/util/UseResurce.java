package net.openid.conformance.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(UseResources.class)
public @interface UseResurce {

	String value();

	String key() default "null";

	String path() default "null";

	boolean isAsString() default false;

}
