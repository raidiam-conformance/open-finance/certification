package net.openid.conformance.errorhandling;

import com.google.common.base.Strings;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class FAPIErrorController extends AbstractErrorController {

	@Autowired
	private ServerInfoTemplate serverInfoTemplate;

	public FAPIErrorController(ErrorAttributes errorAttributes) {
		super(errorAttributes);
	}

	@RequestMapping(value = "/error")
	public Object handleError(HttpServletRequest request) {
		Map<String, Object> map = getErrorAttributes(request, ErrorAttributeOptions.of(ErrorAttributeOptions.Include.STACK_TRACE));

		String path = (String) map.get("path");
		if (!Strings.isNullOrEmpty(path) && path.contains("/api/")) {
			return new ResponseEntity<>(map, getStatus(request));
		} else {
			map.put("logo", "/images/" + serverInfoTemplate.getServerInfo().get("logo"));
			return new ModelAndView("error", map);
		}
	}
}
