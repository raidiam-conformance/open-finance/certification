package net.openid.conformance.configuration;

import net.openid.conformance.extensions.DirectoryRootsService;
import net.openid.conformance.extensions.yacs.CachedParticipantsService;
import net.openid.conformance.extensions.yacs.ParticipantsService;
import net.openid.conformance.extensions.yacs.ParticipantsServiceImpl;
import net.openid.conformance.logging.DBEventLog;
import net.openid.conformance.logging.EventLog;
import net.openid.conformance.logging.JsonObjectSanitiser;
import net.openid.conformance.logging.JwksLeafNodeVisitor;
import net.openid.conformance.logging.MapSanitiser;
import net.openid.conformance.logging.PrivateKeyLeafVisitor;
import net.openid.conformance.logging.SanitisingEventLog;
import net.openid.conformance.logging.TestConfigLeafNodeVisitor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Set;

@Configuration
@EnableCaching
@EnableScheduling
public class BasicConfiguration {

	@Bean
	@ConditionalOnExpression("#{!'YACS'.equals(environment.getProperty('fintechlabs.extensions'))}")
	@ConditionalOnProperty(matchIfMissing = false, value = "fintechlabs.sanitise.logs", havingValue = "true")
	public EventLog eventLog(DBEventLog eventLog) {
			return new SanitisingEventLog(eventLog, jsonObjectSanitiser(), mapSanitiser());
	}

	@Bean
	public JsonObjectSanitiser jsonObjectSanitiser() {
		return new JsonObjectSanitiser(Set.of(new PrivateKeyLeafVisitor(), new JwksLeafNodeVisitor()));
	}

	@Bean
	public MapSanitiser mapSanitiser() {
		return new MapSanitiser(Set.of(new PrivateKeyLeafVisitor(), new JwksLeafNodeVisitor(), new TestConfigLeafNodeVisitor()));
	}

	@Bean
	public DirectoryRootsService directoryRootsService() {
		return new DirectoryRootsService();
	}

	@Bean
	public ParticipantsService participantsService(CachedParticipantsService cachedParticipantsService) {
		return new ParticipantsServiceImpl(cachedParticipantsService);
	}
}
