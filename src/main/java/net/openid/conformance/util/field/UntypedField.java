package net.openid.conformance.util.field;

public class UntypedField extends Field {
	private UntypedField(boolean optional, boolean nullable, String path) {
		super(optional, nullable, path);
	}

	public static class Builder {

		private final String path;
		private boolean optional;
		private boolean nullable;

		public Builder(String path) {
			this.path = path;
		}

		public UntypedField.Builder setOptional() {
			this.optional = true;
			return this;
		}

		public UntypedField.Builder setNullable() {
			this.nullable = true;
			return this;
		}

		public UntypedField build() {
			return new UntypedField(this.optional, this.nullable, this.path);
		}
	}
}
