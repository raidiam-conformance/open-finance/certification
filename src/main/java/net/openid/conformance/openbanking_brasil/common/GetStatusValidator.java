package net.openid.conformance.openbanking_brasil.common;

import com.google.common.collect.Sets;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.IntField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringArrayField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api url: https://openbanking-brasil.github.io/openapi/swagger-apis/common/2.0.0-beta.2.yml
 * Api endpoint: GET /status
 * Api version: 2.0.0-beta.2
 */
@ApiName("Common Api GET Status")
public class GetStatusValidator extends AbstractJsonAssertingCondition {

	public static final Set<String> CODE = Sets.newHashSet("OK", "PARTIAL_FAILURE", "UNAVAILABLE", "SCHEDULED_OUTAGE");

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment,"resource_endpoint_response_full");
		assertField(body,
			new ObjectField
				.Builder("data")
				.setValidator(data ->
					assertField(data,
						new ObjectArrayField
							.Builder("status")
							.setValidator(this::assertStatus)
							.setMaxItems(1000)
							.build()))
				.build());

		assertField(body,
			new ObjectField
				.Builder("links")
				.setValidator(this::assertLinks)
				.build());

		assertField(body,
			new ObjectField
				.Builder("meta")
				.setValidator(this::assertMeta)
				.build());

		return environment;
	}

	private void assertStatus(JsonObject status) {
		assertField(status,
			new StringField
				.Builder("code")
				.setEnums(CODE)
				.build());

		assertField(status,
			new StringField
				.Builder("explanation")
				.setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$")
				.setMaxLength(2000)
				.build());

		assertField(status,
			new StringField
				.Builder("detectionTime")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])T(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)Z$")
				.setMaxLength(20)
				.setOptional()
				.build());

		assertField(status,
			new StringField
				.Builder("expectedResolutionTime")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])T(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)Z$")
				.setMaxLength(20)
				.setOptional()
				.build());

		assertField(status,
			new StringField
				.Builder("updateTime")
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])T(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)Z$")
				.setMaxLength(20)
				.setOptional()
				.build());

		assertField(status,
			new StringArrayField
				.Builder("unavailableEndpoints")
				.setMaxLength(2000)
				.setMaxItems(1000)
				.setOptional()
				.build());
	}

	public void assertLinks(JsonObject links) {
		assertField(links,
			new StringField
				.Builder("self")
				.setMaxLength(2000)
				.setPattern("^(https:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$")
				.setOptional()
				.build());

		assertField(links,
			new StringField
				.Builder("first")
				.setOptional()
				.setPattern("^(https:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$")
				.setMaxLength(2000)
				.build());

		assertField(links,
			new StringField
				.Builder("prev")
				.setPattern("^(https:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$")
				.setMaxLength(2000)
				.setOptional()
				.build());

		assertField(links,
			new StringField
				.Builder("next")
				.setPattern("^(https:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$")
				.setMaxLength(2000)
				.setOptional()
				.build());

		assertField(links,
			new StringField
				.Builder("last")
				.setPattern("^(https:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$")
				.setMaxLength(2000)
				.setOptional()
				.build());
	}

	public void assertMeta(JsonObject meta) {
		assertField(meta,
			new IntField
				.Builder("totalRecords")
				.build());

		assertField(meta,
			new IntField
				.Builder("totalPages")
				.build());
	}
}
