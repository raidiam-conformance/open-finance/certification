package net.openid.conformance.openbanking_brasil.opendata.oasValidators.exchange;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api: swagger/opendata/exchange/swagger-exchange-1.0.1.yml
 * Api url: https://openbanking-brasil.github.io/draft-openapi/swagger-apis/exchange/1.0.1.yml
 * Api endpoint: /vet-values
 * Api version: 1.0.1
 */

@ApiName("Vet Value V1")
public class GetExchangeVetValueOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/exchange/swagger-exchange-1.0.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/vet-values";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
