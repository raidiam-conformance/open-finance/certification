package net.openid.conformance.openbanking_brasil.opendata.oasValidators.acquiringServices;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api: swagger/opendata/acquiringServices/swagger-acquiring-service-1.0.1.yml
 * Api url: https://openbanking-brasil.github.io/draft-openapi/swagger-apis/acquiring-services/1.0.1.yml
 * Api endpoint: /personals
 * Api version: 1.0.1
 */

@ApiName("Personal Acquiring Services V1")
public class GetAcquiringServicesPersonalOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/acquiringServices/swagger-acquiring-service-1.0.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/personals";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
