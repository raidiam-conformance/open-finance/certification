package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule.CreditFixedIncomesApiCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.PreFlightCheckV4Module;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule.CreditFixedIncomesApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules.CreditFixedIncomesApiPaginationListConditionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.CreditFixedIncomesApiPaginationTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.CreditFixedIncomesApiPaginationTransactionsCurrentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules.CreditFixedIncomesApiResourcesTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule.CreditFixedIncomesApiTransactionDateTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules.CreditFixedIncomesApiWrongPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules.CreditFixedIncomesApiXFapiTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "credit-fixed-incomes_test-plan",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	displayName = PlanNames.CREDIT_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN,
	summary = PlanNames.CREDIT_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN
)
public class CreditFixedIncomesTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV4Module.class,
					CreditFixedIncomesApiCoreTestModule.class,
					CreditFixedIncomesApiTransactionDateTestModule.class,
					CreditFixedIncomesApiResourcesTestModule.class,
					CreditFixedIncomesApiWrongPermissionsTestModule.class,
					CreditFixedIncomesApiOperationalLimitsTestModule.class,
					CreditFixedIncomesApiPaginationTestModule.class,
					CreditFixedIncomesApiPaginationTransactionsCurrentTestModule.class,
					CreditFixedIncomesApiPaginationListConditionalTestModule.class,
					CreditFixedIncomesApiXFapiTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}

}
