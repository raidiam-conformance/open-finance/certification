package net.openid.conformance.openbanking_brasil.plans.yacs.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.YACSPaymentsConsentsCoreTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.YACSPreFlightCertCheckPaymentsV4;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;
@PublishTestPlan(
	testPlanName = "Open Finance Brasil Functional Production Tests - FVP Phase 3 - Payments",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	displayName = PlanNames.OBB_PROD_FVP_PAYMENTS,
	summary = "Open Finance Brasil Functional Production Tests - FVP - Payments"
)
public class YACSPaymentsProductionTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					YACSPreFlightCertCheckPaymentsV4.class,
					YACSPaymentsConsentsCoreTestModuleV4.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
