package net.openid.conformance.openbanking_brasil.plans.yacs.dcr;

import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRBadMTLSNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRClientDeletionNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRHappyFlowVariant2NoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRHappyFlowVariantNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidJwksByValueNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidJwksUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidRedirectUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidRegistrationAccessTokenNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidSoftwareStatementSignatureNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRNoMTLSNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRNoRedirectUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRNoSoftwareStatementNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRTLSClientAuthNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRUpdateClientConfigBadJwksUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidJwksByValueNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidRedirectUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRUpdateClientConfigNoAuthFlow;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.DCRMultipleClientTest;
import net.openid.conformance.openbanking_brasil.testmodules.DcrNoSubjectTypeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.DcrSandboxCredentialsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.directory.ApiResourceCertificationStatusTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.directory.DirectoryApiServerRegistrationTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiInvalidParametersTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiInvalidParametersTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.FVPDCRAutomaticPaymentsConsentsBadLoggerUserTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.FvpAutomaticPaymentsApiNegativeConsentsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.FvpAutomaticPaymentsApiRejectedConsentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.FvpAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiPixSchedulingDatesUnhappyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiRecurringPaymentsConsentLimitTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiRecurringPaymentsWrongCustomQuantityTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiEnforceDICTTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiEnforceMANUTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiEnforceQRESTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiForceCheckBadSignatureTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiNegativeTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsJsonAcceptHeaderJwtReturnedTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsBadLoggerUserTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n.FvpConsentApiBadConsentsTestModuleV3n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n.FvpConsentApiExtensionInvalidStatusTestModuleV3n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n.FvpConsentApiNegativeTestsV3n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n.FvpConsentsApiPermissionGroupsTestModuleV3n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.PaymentsConsentsServerTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.FVPDCRConsentsBadLoggedUserV3;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.VariantSelection;

import java.util.List;

@PublishTestPlan(
	testPlanName = "dcr-fvp-yacs_test-plan",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	displayName = PlanNames.OBB_PROD_FVP_DCR_TEST_PLAN,
	summary = "Functional DCR Test without browser interaction - FVP"
)
public class DcrFvpYacsTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {

		return List.of(
			new ModuleListEntry(
				List.of(
					FAPI1AdvancedFinalBrazilDCRHappyFlowVariantNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRHappyFlowVariant2NoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRClientDeletionNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidRegistrationAccessTokenNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidSoftwareStatementSignatureNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRNoSoftwareStatementNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRNoMTLSNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRBadMTLSNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigBadJwksUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidJwksByValueNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidRedirectUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRNoRedirectUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidRedirectUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidJwksUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidJwksByValueNoAuthFlow.class,
					FvpEnrollmentsApiInvalidParametersTestModuleV1.class,
					PaymentsConsentsServerTestModule.class,
					DirectoryApiServerRegistrationTestModuleV2.class,
					DcrNoSubjectTypeTestModule.class,
					DCRMultipleClientTest.class,
					DcrSandboxCredentialsTestModule.class,
					FAPI1AdvancedFinalBrazilDCRTLSClientAuthNoAuthFlow.class,
					ApiResourceCertificationStatusTestModule.class,
					FvpConsentApiBadConsentsTestModuleV3n.class,
					FvpConsentApiExtensionInvalidStatusTestModuleV3n.class,
					FvpConsentApiNegativeTestsV3n.class,
					FvpConsentsApiPermissionGroupsTestModuleV3n.class,
					FVPDCRConsentsBadLoggedUserV3.class,
					FvpAutomaticPaymentsApiNegativeConsentsTestModuleV1.class,
					FvpAutomaticPaymentsApiRejectedConsentTestModuleV1.class,
					FvpAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV1.class,
					FvpPaymentsConsentsApiNegativeTestModuleV4.class,
					FvpPaymentsConsentsApiEnforceDICTTestModuleV4.class,
					FvpPaymentsConsentsApiForceCheckBadSignatureTestModuleV4.class,
					FvpPaymentsConsentsJsonAcceptHeaderJwtReturnedTestModuleV4.class,
					FvpPaymentsConsentsApiEnforceMANUTestModuleV4.class,
					FvpPaymentsApiPixSchedulingDatesUnhappyTestModuleV4.class,
					FvpPaymentsConsentsApiEnforceQRESTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsConsentLimitTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsWrongCustomQuantityTestModuleV4.class,
					FVPDCRAutomaticPaymentsConsentsBadLoggerUserTestModuleV1.class,
					PaymentsConsentsBadLoggerUserTestModuleV4.class,
					FvpEnrollmentsApiInvalidParametersTestModuleV2.class
					),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);

	}
	public static String certificationProfileName(VariantSelection variant) {
		return "BR-OB Adv. OP DCR";
	}
}
