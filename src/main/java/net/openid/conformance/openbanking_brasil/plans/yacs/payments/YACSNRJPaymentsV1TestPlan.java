package net.openid.conformance.openbanking_brasil.plans.yacs.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiInvalidChallengeTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiInvalidOriginTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiInvalidPublicKeyTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiInvalidRpIdTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiInvalidStatusSignOptionsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiPaymentsCoreTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiPaymentsKeysSwapTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiPaymentsPreEnrollmentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiPaymentsUnmatchingFieldsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiPreFlightTestModuleV1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;

@PublishTestPlan(
	testPlanName = "fvp-no_redirect_payments-test-plan-v1",
	profile = OBBProfile.OBB_PROFILE_PROD_RESTRICTED_FVP,
	displayName = PlanNames.OBB_PROD_FVP_NO_REDIRECT_PAYMENTS_V1,
	summary = "Open Finance Brasil Functional Production Tests - FVP - No Redirect Payments Version 1"
)
public class YACSNRJPaymentsV1TestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					FvpEnrollmentsApiPreFlightTestModuleV1.class,
					FvpEnrollmentsApiPaymentsCoreTestModuleV1.class,
					FvpEnrollmentsApiInvalidChallengeTestModuleV1.class,
					FvpEnrollmentsApiInvalidOriginTestModuleV1.class,
					FvpEnrollmentsApiInvalidPublicKeyTestModuleV1.class,
					FvpEnrollmentsApiInvalidRpIdTestModuleV1.class,
					FvpEnrollmentsApiPaymentsPreEnrollmentTestModuleV1.class,
					FvpEnrollmentsApiInvalidStatusSignOptionsTestModuleV1.class,
					FvpEnrollmentsApiPaymentsKeysSwapTestModuleV1.class,
					FvpEnrollmentsApiPaymentsUnmatchingFieldsTestModuleV1.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}

