package net.openid.conformance.openbanking_brasil.plans.channels.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.channels.v2.ChannelsElectronicChannelsApiTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "channels-electronic-channels_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.ELECTRONIC_CHANNELS_API_TEST_PLAN_V2,
	summary = PlanNames.ELECTRONIC_CHANNELS_API_TEST_PLAN_V2
)
public class ChannelsElectronicChannelsApiTestPlanV2 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					ChannelsElectronicChannelsApiTestModuleV2.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}
}
