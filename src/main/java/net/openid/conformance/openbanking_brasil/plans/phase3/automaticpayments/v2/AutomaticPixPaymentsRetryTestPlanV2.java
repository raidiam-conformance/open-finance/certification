package net.openid.conformance.openbanking_brasil.plans.phase3.automaticpayments.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixRetryExtradayCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixRetryExtradayRetryUnacceptedTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixRetryIntradayCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixRetryIntradayTimezoneTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "automatic-pix-payments-retry_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.AUTOMATIC_PIX_PAYMENTS_API_RETRY_PHASE_3_V2_TEST_PLAN,
	summary = "Functional test for Open Finance Brasil Automatic Pix Payments API Retry. " +
		"These tests are designed to test the behavior of retrying recurring-payment initiation for Automatic Pix Payments API. " +
		"The tests are based on the Automatic Payments v2 Specifications."
)
public class AutomaticPixPaymentsRetryTestPlanV2 implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					AutomaticPaymentsApiAutomaticPixRetryIntradayCoreTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixRetryIntradayTimezoneTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixRetryExtradayCoreTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixRetryExtradayRetryUnacceptedTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
