package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.PreFlightCheckV4Module;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges.ExchangeApiCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges.ExchangeApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges.ExchangeApiResourcesTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges.ExchangeApiXFapiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges.ExchangeApiEventsPaginationTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges.ExchangeApiPaginationListTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges.ExchangeApiWrongPermissionsTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "exchanges_test-plan",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	displayName = PlanNames.EXCHANGE_API_PHASE_4B_TEST_PLAN,
	summary = PlanNames.EXCHANGE_API_PHASE_4B_TEST_PLAN
)
public class ExchangesTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV4Module.class,
					ExchangeApiCoreTestModule.class,
					ExchangeApiResourcesTestModule.class,
					ExchangeApiXFapiTestModule.class,
					ExchangeApiWrongPermissionsTestModule.class,
					ExchangeApiOperationalLimitsTestModule.class,
					ExchangeApiPaginationListTestModule.class,
					ExchangeApiEventsPaginationTestModule.class
					),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
