package net.openid.conformance.openbanking_brasil.plans.phase3.automaticpayments.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiExpirationDateTimeTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiInvalidScopeTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiMultipleConsentsCoreTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiNegativeConsentsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiRejectedConsentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiRevokedConsentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiStartDateTimeTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiSweepingAccountsConsentsCoreTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiSweepingAccountsCoreTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiSweepingAccountsLimitsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiSweepingAccountsRootCnpjTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiSweepingAccountsWrongCreditorTestModuleV1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "automatic-payments_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.AUTOMATIC_PAYMENTS_API_PHASE_3_V1_TEST_PLAN,
	summary = "Functional test for Open Finance Brasil Automatic Payments API. " +
		"These tests are designed to validate the payment initiation using Automatic Payments API and ensuring " +
		"structural integrity and content validation. The tests are based on the Automatic Payments v1 Specifications."
)
public class AutomaticPaymentsTestPlan implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					AutomaticPaymentsApiSweepingAccountsConsentsCoreTestModuleV1.class,
					AutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV1.class,
					AutomaticPaymentsApiRejectedConsentTestModuleV1.class,
					AutomaticPaymentsApiExpirationDateTimeTestModuleV1.class,
					AutomaticPaymentsApiMultipleConsentsCoreTestModuleV1.class,
					AutomaticPaymentsApiNegativeConsentsTestModuleV1.class,
					AutomaticPaymentsApiRevokedConsentTestModuleV1.class,
					AutomaticPaymentsApiStartDateTimeTestModuleV1.class,
					AutomaticPaymentsApiSweepingAccountsCoreTestModuleV1.class,
					AutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModuleV1.class,
					AutomaticPaymentsApiSweepingAccountsLimitsTestModuleV1.class,
					AutomaticPaymentsApiSweepingAccountsWrongCreditorTestModuleV1.class,
					AutomaticPaymentsApiSweepingAccountsRootCnpjTestModuleV1.class,
					AutomaticPaymentsApiInvalidScopeTestModuleV1.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
