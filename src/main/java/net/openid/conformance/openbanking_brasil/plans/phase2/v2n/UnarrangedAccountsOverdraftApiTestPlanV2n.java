package net.openid.conformance.openbanking_brasil.plans.phase2.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n.UnarrangedAccountsOverdraftApiOperationalLimitsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n.UnarrangedAccountsOverdraftApiResourcesTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n.UnarrangedAccountsOverdraftApiTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n.UnarrangedAccountsOverdraftApiWrongPermissionsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n.UnarrangedAccountsOverdraftApiXFapiTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "unarranged-accounts-overdraft_test-plan_v2-2",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME_V2_2,
	summary = PlanNames.CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME_V2_2
)
public class UnarrangedAccountsOverdraftApiTestPlanV2n implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV2Module.class,
					UnarrangedAccountsOverdraftApiTestModuleV2n.class,
					UnarrangedAccountsOverdraftApiOperationalLimitsTestModuleV2n.class,
					UnarrangedAccountsOverdraftApiResourcesTestModuleV2n.class,
					UnarrangedAccountsOverdraftApiWrongPermissionsTestModuleV2n.class,
					UnarrangedAccountsOverdraftApiXFapiTestModuleV2n.class
				),
				List.of(
					new TestPlan.Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new TestPlan.Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
