package net.openid.conformance.openbanking_brasil.plans.phase2.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n3.FinancingApiWrongPermissionsTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n3.FinancingsApiOperationalLimitsTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n3.FinancingsApiResourcesTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n3.FinancingsApiTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n3.FinancingsApiXFapiTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "financings_test-plan_v2-3",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.FINANCINGS_API_NAME_V2_3,
	summary = PlanNames.FINANCINGS_API_NAME_V2_3
)
public class FinancingsApiTestPlanV2n3 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV2Module.class,
					FinancingsApiTestModuleV2n3.class,
					FinancingApiWrongPermissionsTestModuleV2n3.class,
					FinancingsApiResourcesTestModuleV2n3.class,
					FinancingsApiOperationalLimitsTestModuleV2n3.class,
					FinancingsApiXFapiTestModuleV2n3.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
