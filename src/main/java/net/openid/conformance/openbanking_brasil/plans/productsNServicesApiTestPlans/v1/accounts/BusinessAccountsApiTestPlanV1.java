package net.openid.conformance.openbanking_brasil.plans.productsNServicesApiTestPlans.v1.accounts;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.productsNServices.v1.accounts.BusinessAccountsApiTestModuleV1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "business-accounts_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.BUSINESS_ACCOUNTS_API_TEST_PLAN_V1,
	summary = PlanNames.BUSINESS_ACCOUNTS_API_TEST_PLAN_V1
)
public class BusinessAccountsApiTestPlanV1 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					BusinessAccountsApiTestModuleV1.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}
}
