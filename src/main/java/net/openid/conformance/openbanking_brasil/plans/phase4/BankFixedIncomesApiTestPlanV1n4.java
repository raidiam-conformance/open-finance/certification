package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.PreFlightCheckV4Module;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule.BankFixedIncomesApiOperationalLimitsTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule.BankFixedIncomesApiCoreTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules.BankFixedIncomesApiPaginationListConditionalTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.BankFixedIncomesApiPaginationTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.BankFixedIncomesApiPaginationTransactionsCurrentTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules.BankFixedIncomesApiResourcesTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule.BankFixedIncomesApiTransactionDateTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules.BankFixedIncomesApiWrongPermissionsTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules.BankFixedIncomesApiXFapiTestModuleV1n;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "bank-fixed-incomes_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	displayName = PlanNames.BANK_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN_V1,
	summary = PlanNames.BANK_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN_V1
)
public class BankFixedIncomesApiTestPlanV1n4 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV4Module.class,
					BankFixedIncomesApiCoreTestModuleV1n.class,
					BankFixedIncomesApiTransactionDateTestModuleV1n.class,
					BankFixedIncomesApiResourcesTestModuleV1n.class,
					BankFixedIncomesApiWrongPermissionsTestModuleV1n.class,
					BankFixedIncomesApiOperationalLimitsTestModuleV1n.class,
					BankFixedIncomesApiPaginationTestModuleV1n.class,
					BankFixedIncomesApiPaginationTransactionsCurrentTestModuleV1n.class,
					BankFixedIncomesApiPaginationListConditionalTestModuleV1n.class,
					BankFixedIncomesApiXFapiTestModuleV1n.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
