package net.openid.conformance.openbanking_brasil.plans.phase3.automaticpayments.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.AutomaticPaymentsApiTimezoneTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "automatic-payments-timezone_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.AUTOMATIC_PAYMENTS_API_PHASE_3_V1_TIMEZONE_TEST_PLAN,
	summary = "Functional timezone tests for Open Finance Brasil Automatic Payments API. " +
		"These tests are designed to validate the payment initiation using Automatic Payments API and ensuring " +
		"structural integrity and content validation. The tests are based on the Automatic Payments v1 Specifications."
)
public class AutomaticPaymentsTimezoneTestPlan implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					AutomaticPaymentsApiTimezoneTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
