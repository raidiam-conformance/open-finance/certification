package net.openid.conformance.openbanking_brasil.plans.channels.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.channels.v2.ChannelsSharedAutomatedTellerMachinesApiTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "channels-shared-automated-teller-machines_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.SHARED_AUTOMATED_TELLER_MACHINES_API_TEST_PLAN_V2,
	summary = PlanNames.SHARED_AUTOMATED_TELLER_MACHINES_API_TEST_PLAN_V2
)
public class ChannelsSharedAutomatedTellerMachinesApiTestPlanV2 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					ChannelsSharedAutomatedTellerMachinesApiTestModuleV2.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}
}
