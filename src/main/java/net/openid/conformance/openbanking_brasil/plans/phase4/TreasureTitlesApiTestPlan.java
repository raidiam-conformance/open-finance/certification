package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule.TreasureTitlesApiCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.PreFlightCheckV4Module;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule.TreasureTitlesApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules.TreasureTitlesApiPaginationListConditionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.TreasureTitlesApiPaginationTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.TreasureTitlesApiPaginationTransactionsCurrentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules.TreasureTitlesApiResourcesTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule.TreasureTitlesApiTransactionDateTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules.TreasureTitlesApiWrongPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules.TreasureTitlesApiXFapiTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "treasure-titles_test-plan",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	displayName = PlanNames.TREASURE_TITLES_API_PHASE_4B_TEST_PLAN,
	summary = PlanNames.TREASURE_TITLES_API_PHASE_4B_TEST_PLAN
)
public class TreasureTitlesApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV4Module.class,
					TreasureTitlesApiCoreTestModule.class,
					TreasureTitlesApiTransactionDateTestModule.class,
					TreasureTitlesApiResourcesTestModule.class,
					TreasureTitlesApiWrongPermissionsTestModule.class,
					TreasureTitlesApiOperationalLimitsTestModule.class,
					TreasureTitlesApiPaginationTestModule.class,
					TreasureTitlesApiPaginationTransactionsCurrentTestModule.class,
					TreasureTitlesApiPaginationListConditionalTestModule.class,
					TreasureTitlesApiXFapiTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())

				)
			)
		);
	}
}
