package net.openid.conformance.openbanking_brasil.plans.phase3.automaticpayments.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixRetryAuxiliaryOnePaymentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixRetryAuxiliaryTwoPaymentsTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "automatic-pix-payments-retry-auxiliary_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.AUTOMATIC_PIX_PAYMENTS_API_RETRY_AUXILIARY_PHASE_3_V2_TEST_PLAN,
	summary = "Functional test for Open Finance Brasil Automatic Pix Payments API Retry Auxiliary. " +
		"These tests are designed to create recurring consents and recurring payments using Automatic Pix Payments API " +
		"so that they can later be used for Automatic Pix Payments API Retry tests. " +
		"The tests are based on the Automatic Payments v2 Specifications."
)
public class AutomaticPixPaymentsRetryAuxiliaryTestPlanV2 implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					AutomaticPaymentsApiAutomaticPixRetryAuxiliaryOnePaymentTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixRetryAuxiliaryTwoPaymentsTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
