package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.PreFlightCheckV4Module;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule.CreditFixedIncomesApiOperationalLimitsTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule.CreditFixedIncomesApiCoreTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules.CreditFixedIncomesApiPaginationListConditionalTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.CreditFixedIncomesApiPaginationTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.CreditFixedIncomesApiPaginationTransactionsCurrentTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules.CreditFixedIncomesApiResourcesTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule.CreditFixedIncomesApiTransactionDateTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules.CreditFixedIncomesApiWrongPermissionsTestModuleV1n;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules.CreditFixedIncomesApiXFapiTestModuleV1n;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "credit-fixed-incomes_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	displayName = PlanNames.CREDIT_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN_V1,
	summary = PlanNames.CREDIT_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN_V1
)
public class CreditFixedIncomesTestPlanV1 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV4Module.class,
					CreditFixedIncomesApiCoreTestModuleV1n.class,
					CreditFixedIncomesApiTransactionDateTestModuleV1n.class,
					CreditFixedIncomesApiResourcesTestModuleV1n.class,
					CreditFixedIncomesApiWrongPermissionsTestModuleV1n.class,
					CreditFixedIncomesApiOperationalLimitsTestModuleV1n.class,
					CreditFixedIncomesApiPaginationTestModuleV1n.class,
					CreditFixedIncomesApiPaginationTransactionsCurrentTestModuleV1n.class,
					CreditFixedIncomesApiPaginationListConditionalTestModuleV1n.class,
					CreditFixedIncomesApiXFapiTestModuleV1n.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}

}
