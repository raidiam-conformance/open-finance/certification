package net.openid.conformance.openbanking_brasil.plans.productsNServicesApiTestPlans.v1.creditCard;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.productsNServices.v1.creditCard.BusinessCreditCardApiTestModuleV1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "business-credit_card_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.BUSINESS_CREDIT_CARD_API_TEST_PLAN_V1,
	summary = PlanNames.BUSINESS_CREDIT_CARD_API_TEST_PLAN_V1
)
public class BusinessCreditCardApiTestPlanV1 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					BusinessCreditCardApiTestModuleV1.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}
}
