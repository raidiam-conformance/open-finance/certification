package net.openid.conformance.openbanking_brasil.plans.noredirectpayments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiPreflightTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiCoreEnrollmentTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiExcessiveDailyLimitSchdTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiExcessiveDailyLimitTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiExcessiveTransactionLimitTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiExpiredPostAuthorisationEnrollmentTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiExpiredPreAuthorisationEnrollmentTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiExpiredPreRiskSignalEnrollmentTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiInvalidChallengeTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiInvalidOriginTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiInvalidParametersTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiInvalidPublicKeyTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiInvalidRpIdTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiInvalidStatusOptionsTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiInvalidStatusSignOptionsTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiMultipleConsentsCoreTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiPaymentsCoreTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiPaymentsKeysSwapTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiPaymentsPreAccountHolderValidationTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiPaymentsPreEnrollmentTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiPaymentsUnmatchingFieldsTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiRejectedPostAuthorisationEnrollmentTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiRejectedPreAuthorisationEnrollmentTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiRevokedEnrollmentTestModuleV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1.EnrollmentsApiRiskSignalsTestModuleV2n1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "no-redirect-payments_test-plan_v2-1",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_1_TEST_PLAN,
	summary = PlanNames.NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_1_TEST_PLAN
)
public class NoRedirectPaymentsTestPlanV2n1 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					EnrollmentsApiPreflightTestModuleV2.class,
					EnrollmentsApiExpiredPostAuthorisationEnrollmentTestModuleV2n1.class,
					EnrollmentsApiExpiredPreAuthorisationEnrollmentTestModuleV2n1.class,
					EnrollmentsApiExpiredPreRiskSignalEnrollmentTestModuleV2n1.class,
					EnrollmentsApiInvalidParametersTestModuleV2n1.class,
					EnrollmentsApiInvalidPublicKeyTestModuleV2n1.class,
					EnrollmentsApiInvalidStatusOptionsTestModuleV2n1.class,
					EnrollmentsApiInvalidStatusSignOptionsTestModuleV2n1.class,
					EnrollmentsApiPaymentsPreAccountHolderValidationTestModuleV2n1.class,
					EnrollmentsApiPaymentsPreEnrollmentTestModuleV2n1.class,
					EnrollmentsApiRejectedPostAuthorisationEnrollmentTestModuleV2n1.class,
					EnrollmentsApiRejectedPreAuthorisationEnrollmentTestModuleV2n1.class,
					EnrollmentsApiPaymentsCoreTestModuleV2n1.class,
					EnrollmentsApiInvalidOriginTestModuleV2n1.class,
					EnrollmentsApiInvalidChallengeTestModuleV2n1.class,
					EnrollmentsApiCoreEnrollmentTestModuleV2n1.class,
					EnrollmentsApiRevokedEnrollmentTestModuleV2n1.class,
					EnrollmentsApiInvalidRpIdTestModuleV2n1.class,
					EnrollmentsApiPaymentsKeysSwapTestModuleV2n1.class,
					EnrollmentsApiPaymentsUnmatchingFieldsTestModuleV2n1.class,
					EnrollmentsApiExcessiveTransactionLimitTestModuleV2n1.class,
					EnrollmentsApiExcessiveDailyLimitTestModuleV2n1.class,
					EnrollmentsApiExcessiveDailyLimitSchdTestModuleV2n1.class,
					EnrollmentsApiMultipleConsentsCoreTestModuleV2n1.class,
					EnrollmentsApiRiskSignalsTestModuleV2n1.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
