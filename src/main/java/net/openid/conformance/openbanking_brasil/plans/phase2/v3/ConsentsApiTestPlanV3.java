package net.openid.conformance.openbanking_brasil.plans.phase2.v3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3.ConsentApiExtensionConsentRejectedTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3.ConsentApiExtensionInvalidLoggedUserTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3.ConsentApiExtensionInvalidStatusTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3.ConsentApiExtensionMultipleCondTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3.ConsentApiExtensionMultipleTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3.ConsentApiExtensionNegativeTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3.ConsentApiExtensionNoHeaderV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3.ConsentApiExtensionPastExpirationDateTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3.ConsentApiExtensionTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.ConsentsApiCrossClientTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.PreFlightCheckV3Module;
import net.openid.conformance.openbanking_brasil.testmodules.v3.consents.ConsentApiBadConsentsTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.consents.ConsentApiNegativeTestsV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.consents.ConsentApiTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.consents.ConsentsApiConsentExpiredTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.consents.ConsentsApiDeleteTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.consents.ConsentsApiOperationalLimitsTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.consents.ConsentsApiPermissionGroupsTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.consents.ConsentsApiRevokedAspspTestModuleV3;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "consents_test-plan_v3",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION3,
	displayName = PlanNames.CONSENTS_API_NAME_V3,
	summary = PlanNames.CONSENTS_API_NAME_V3
)

public class ConsentsApiTestPlanV3 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV3Module.class,
					ConsentApiTestModuleV3.class,
					ConsentsApiCrossClientTestModuleV3.class,
					ConsentsApiDeleteTestModuleV3.class,
					ConsentsApiRevokedAspspTestModuleV3.class,
					ConsentApiExtensionConsentRejectedTestModuleV3.class,
					ConsentApiExtensionInvalidLoggedUserTestModuleV3.class,
					ConsentApiExtensionInvalidStatusTestModuleV3.class,
					ConsentApiExtensionMultipleCondTestModuleV3.class,
					ConsentApiExtensionNoHeaderV3.class,
					ConsentApiExtensionPastExpirationDateTestModuleV3.class,
					ConsentApiExtensionTestModuleV3.class,
					ConsentApiExtensionMultipleTestModuleV3.class,
					ConsentApiExtensionNegativeTestModuleV3.class,
					ConsentApiNegativeTestsV3.class,
					ConsentsApiPermissionGroupsTestModuleV3.class,
					ConsentsApiOperationalLimitsTestModuleV3.class,
					ConsentsApiConsentExpiredTestModuleV3.class,
					ConsentApiBadConsentsTestModuleV3.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
