package net.openid.conformance.openbanking_brasil.plans.phase3.automaticpayments.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiTimezoneTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "automatic-payments-timezone_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.AUTOMATIC_PAYMENTS_API_PHASE_3_V2_TIMEZONE_TEST_PLAN,
	summary = "Functional timezone tests for Open Finance Brasil Automatic Payments API. " +
		"These tests are designed to validate the payment initiation using Automatic Payments API and ensuring " +
		"structural integrity and content validation. The tests are based on the Automatic Payments v2 Specifications."
)
public class AutomaticPaymentsTimezoneTestPlanV2 implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					AutomaticPaymentsApiTimezoneTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
