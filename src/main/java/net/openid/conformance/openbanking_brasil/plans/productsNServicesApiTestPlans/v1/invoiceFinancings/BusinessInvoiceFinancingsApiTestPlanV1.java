package net.openid.conformance.openbanking_brasil.plans.productsNServicesApiTestPlans.v1.invoiceFinancings;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.productsNServices.v1.invoiceFinancings.BusinessInvoiceFinancingsApiTestModuleV1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "business-invoice_financings_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.BUSINESS_INVOICE_FINANCINGS_API_TEST_PLAN_V1,
	summary = PlanNames.BUSINESS_INVOICE_FINANCINGS_API_TEST_PLAN_V1
)
public class BusinessInvoiceFinancingsApiTestPlanV1 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					BusinessInvoiceFinancingsApiTestModuleV1.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}
}
