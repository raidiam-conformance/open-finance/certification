package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.PreFlightCheckV4Module;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule.VariableIncomesApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.VariablesIncomesApiBrokerNoteTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule.VariableIncomesApiCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules.VariableIncomesApiPaginationListConditionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.VariableIncomesApiPaginationTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.VariableIncomesApiPaginationTransactionsCurrentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules.VariableIncomesApiResourcesTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule.VariableIncomesApiTransactionDateTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules.VariableIncomesApiWrongPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules.VariableIncomesApiXFapiTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "variable-incomes_test-plan",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	displayName = PlanNames.VARIABLE_INCOMES_API_PHASE_4B_TEST_PLAN,
	summary = PlanNames.VARIABLE_INCOMES_API_PHASE_4B_TEST_PLAN
)
public class VariableIncomesApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV4Module.class,
					VariableIncomesApiCoreTestModule.class,
					VariableIncomesApiTransactionDateTestModule.class,
					VariableIncomesApiResourcesTestModule.class,
					VariableIncomesApiWrongPermissionsTestModule.class,
					VariablesIncomesApiBrokerNoteTestModule.class,
					VariableIncomesApiOperationalLimitsTestModule.class,
					VariableIncomesApiPaginationTestModule.class,
					VariableIncomesApiPaginationTransactionsCurrentTestModule.class,
					VariableIncomesApiPaginationListConditionalTestModule.class,
					VariableIncomesApiXFapiTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
