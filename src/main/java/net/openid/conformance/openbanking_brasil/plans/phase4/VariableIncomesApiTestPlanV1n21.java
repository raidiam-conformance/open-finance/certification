package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.PreFlightCheckV4Module;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule.VariableIncomesApiOperationalLimitsTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.VariablesIncomesApiBrokerNoteTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule.VariableIncomesApiCoreTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules.VariableIncomesApiPaginationListConditionalTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.VariableIncomesApiPaginationTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.VariableIncomesApiPaginationTransactionsCurrentTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules.VariableIncomesApiResourcesTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule.VariableIncomesApiTransactionDateTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules.VariableIncomesApiWrongPermissionsTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules.VariableIncomesApiXFapiTestModuleV1n21;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "variable-incomes_test-plan_V1-2-1",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	displayName = PlanNames.VARIABLE_INCOMES_API_PHASE_4B_TEST_PLAN_V1N21,
	summary = PlanNames.VARIABLE_INCOMES_API_PHASE_4B_TEST_PLAN_V1N21
)
public class VariableIncomesApiTestPlanV1n21 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV4Module.class,
					VariableIncomesApiCoreTestModuleV1n21.class,
					VariableIncomesApiTransactionDateTestModuleV1n21.class,
					VariableIncomesApiResourcesTestModuleV1n21.class,
					VariableIncomesApiWrongPermissionsTestModuleV1n21.class,
					VariablesIncomesApiBrokerNoteTestModuleV1n21.class,
					VariableIncomesApiOperationalLimitsTestModuleV1n21.class,
					VariableIncomesApiPaginationTestModuleV1n21.class,
					VariableIncomesApiPaginationTransactionsCurrentTestModuleV1n21.class,
					VariableIncomesApiPaginationListConditionalTestModuleV1n21.class,
					VariableIncomesApiXFapiTestModuleV1n21.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
