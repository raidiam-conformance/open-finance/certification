package net.openid.conformance.openbanking_brasil.plans.yacs.customerdata;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.YACSCustomerDataUniqueTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountApiConsentsV3TestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.FvpAccountApiConsentsAllPermissionsV3TestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n.CreditCardApiConsentsV3TestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n4.UnarrangedAccountsOverdraftApiConsentsV3TestModuleV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n3.CreditOperationsDiscountedCreditRightsApiConsentsV3TestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n3.FinancingsApiConsentsV3TestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.v2n4.LoansApiConsentsV3TestModuleV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerBusinessDataApiConsentsV3TestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerPersonalDataApiConsentsV3TestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.v3.resources.ResourcesApiTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.yacs.YACSPreFlightCertCheckV3Module;
import net.openid.conformance.openbanking_brasil.testmodules.v3n.consents.ConsentApiTestModuleV3n;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;

@PublishTestPlan(
	testPlanName = "fvp-customer-data-happy-path_test-plan_v3",
	displayName = PlanNames.OBB_PROD_FVP_PHASE_2_HAPPY_PATH_V3,
	profile = OBBProfile.OBB_PROFILE_PROD_RESTRICTED_FVP,
	summary = PlanNames.OBB_PROD_FVP_PHASE_2_HAPPY_PATH_V3
)
public class YACSHappyPathTestPlanV3 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					YACSPreFlightCertCheckV3Module.class,
					ConsentApiTestModuleV3n.class,
					ResourcesApiTestModuleV3.class,
					AccountApiConsentsV3TestModuleV2n.class,
					CreditCardApiConsentsV3TestModuleV2n.class,
					CustomerBusinessDataApiConsentsV3TestModuleV2n.class,
					CustomerPersonalDataApiConsentsV3TestModuleV2n.class,
					FinancingsApiConsentsV3TestModuleV2n3.class,
					CreditOperationsDiscountedCreditRightsApiConsentsV3TestModuleV2n3.class,
					LoansApiConsentsV3TestModuleV2n4.class,
					UnarrangedAccountsOverdraftApiConsentsV3TestModuleV2n4.class,
					FvpAccountApiConsentsAllPermissionsV3TestModuleV2n.class,
					YACSCustomerDataUniqueTestModuleV3.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
