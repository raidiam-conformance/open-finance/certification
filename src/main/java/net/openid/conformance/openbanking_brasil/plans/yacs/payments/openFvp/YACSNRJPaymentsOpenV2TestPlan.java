package net.openid.conformance.openbanking_brasil.plans.yacs.payments.openFvp;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v2.FvpEnrollmentsApiInvalidChallengeOpenTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v2.FvpEnrollmentsApiInvalidOriginOpenTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v2.FvpEnrollmentsApiInvalidPublicKeyOpenTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v2.FvpEnrollmentsApiInvalidRpIdOpenTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v2.FvpEnrollmentsApiInvalidStatusSignOptionsOpenTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v2.FvpEnrollmentsApiPaymentsCoreOpenTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v2.FvpEnrollmentsApiPaymentsKeysSwapOpenTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v2.FvpEnrollmentsApiPaymentsPreEnrollmentOpenTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v2.FvpEnrollmentsApiPaymentsUnmatchingFieldsOpenTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiPreFlightTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;

@PublishTestPlan(
	testPlanName = "fvp-no_redirect_payments_open_test-plan-v2-1",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	displayName = PlanNames.OBB_PROD_FVP_NO_REDIRECT_PAYMENTS_V2,
	summary = "Open Finance Brasil Functional Production Tests - FVP - No Redirect Payments Version 2.1"
)
public class YACSNRJPaymentsOpenV2TestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					FvpEnrollmentsApiPreFlightTestModuleV2.class,
					FvpEnrollmentsApiPaymentsCoreOpenTestModuleV2.class,
					FvpEnrollmentsApiInvalidChallengeOpenTestModuleV2.class,
					FvpEnrollmentsApiInvalidOriginOpenTestModuleV2.class,
					FvpEnrollmentsApiInvalidPublicKeyOpenTestModuleV2.class,
					FvpEnrollmentsApiInvalidRpIdOpenTestModuleV2.class,
					FvpEnrollmentsApiInvalidStatusSignOptionsOpenTestModuleV2.class,
					FvpEnrollmentsApiPaymentsPreEnrollmentOpenTestModuleV2.class,
					FvpEnrollmentsApiPaymentsUnmatchingFieldsOpenTestModuleV2.class,
					FvpEnrollmentsApiPaymentsKeysSwapOpenTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}

