package net.openid.conformance.openbanking_brasil.plans.noredirectpayments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiNoContractTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiCoreEnrollmentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiExcessiveDailyLimitTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiExcessiveTransactionLimitTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiExpiredPostAuthorisationEnrollmentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiExpiredPreAuthorisationEnrollmentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiExpiredPreRiskSignalEnrollmentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiInvalidChallengeTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiInvalidOriginTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiInvalidParametersTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiInvalidPublicKeyTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiInvalidRpIdTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiInvalidStatusOptionsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiInvalidStatusSignOptionsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiPaymentsCoreTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiPaymentsKeysSwapTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiPaymentsPreAccountHolderValidationTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiPaymentsPreEnrollmentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiPaymentsUnmatchingFieldsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiRejectedPostAuthorisationEnrollmentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiRejectedPreAuthorisationEnrollmentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiRevokedEnrollmentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1.EnrollmentsApiPreflightTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.webhook.EnrollmentsApiWebhookRejectedTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.webhook.EnrollmentsApiWebhookRevokedTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "no-redirect-payments_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.NO_REDIRECT_PAYMENTS_API_PHASE_3_V1_TEST_PLAN,
	summary = "Functional tests for Open Finance Brasil No Redirect Payments APIs, including Enrollments API and additional Payments API test modules."
)
public class NoRedirectPaymentsTestPlanV1 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					EnrollmentsApiPreflightTestModuleV1.class,
					EnrollmentsApiExpiredPostAuthorisationEnrollmentTestModuleV1.class,
					EnrollmentsApiExpiredPreAuthorisationEnrollmentTestModuleV1.class,
					EnrollmentsApiExpiredPreRiskSignalEnrollmentTestModuleV1.class,
					EnrollmentsApiInvalidParametersTestModuleV1.class,
					EnrollmentsApiInvalidPublicKeyTestModuleV1.class,
					EnrollmentsApiInvalidStatusOptionsTestModuleV1.class,
					EnrollmentsApiInvalidStatusSignOptionsTestModuleV1.class,
					EnrollmentsApiPaymentsPreAccountHolderValidationTestModuleV1.class,
					EnrollmentsApiPaymentsPreEnrollmentTestModuleV1.class,
					EnrollmentsApiRejectedPostAuthorisationEnrollmentTestModuleV1.class,
					EnrollmentsApiRejectedPreAuthorisationEnrollmentTestModuleV1.class,
					EnrollmentsApiPaymentsCoreTestModuleV1.class,
					EnrollmentsApiInvalidOriginTestModuleV1.class,
					EnrollmentsApiInvalidChallengeTestModuleV1.class,
					EnrollmentsApiCoreEnrollmentTestModuleV1.class,
					EnrollmentsApiRevokedEnrollmentTestModuleV1.class,
					EnrollmentsApiInvalidRpIdTestModuleV1.class,
					EnrollmentsApiPaymentsKeysSwapTestModuleV1.class,
					EnrollmentsApiPaymentsUnmatchingFieldsTestModuleV1.class,
					EnrollmentsApiExcessiveTransactionLimitTestModuleV1.class,
					EnrollmentsApiExcessiveDailyLimitTestModuleV1.class,
					EnrollmentsApiNoContractTestModuleV1.class,
					EnrollmentsApiWebhookRejectedTestModule.class,
					EnrollmentsApiWebhookRevokedTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
