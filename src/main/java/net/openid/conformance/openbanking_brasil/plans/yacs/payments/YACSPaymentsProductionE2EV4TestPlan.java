package net.openid.conformance.openbanking_brasil.plans.yacs.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiRecurringPaymentsCustomCoreTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiRecurringPaymentsCustomNotCancelledTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiRecurringPaymentsMonthlyCoreTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiRecurringPaymentsPatchTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiRecurringPaymentsWeeklyCoreTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentApiNoDebtorProvidedTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRealEmailInvalidCreditorProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiWrongEmailAddressProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsNoFundsTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.YACSPreFlightCertCheckPaymentsV4;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;
@PublishTestPlan(
	testPlanName = "fvp-payments-e2e_test-plan-v4",
	profile = OBBProfile.OBB_PROFILE_PROD_RESTRICTED_FVP,
	displayName = PlanNames.OBB_PROD_FVP_PAYMENTS_V4_E2E,
	summary = "Open Finance Brasil Functional Production Tests - FVP - Payments V4 E2E"
)
public class YACSPaymentsProductionE2EV4TestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					YACSPreFlightCertCheckPaymentsV4.class,
					PaymentApiNoDebtorProvidedTestModuleV4.class,
					PaymentsApiRealEmailInvalidCreditorProxyTestModuleV4.class,
					PaymentsApiWrongEmailAddressProxyTestModuleV4.class,
					PaymentsNoFundsTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsCustomCoreTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsPatchTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsWeeklyCoreTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsMonthlyCoreTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsCustomNotCancelledTestModuleV4.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}

