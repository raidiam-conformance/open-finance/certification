package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithStandardPayload;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments_api_recurring-payments-custom-core_test-module_v4",
	displayName = "fvp-payments_api_recurring-payments-custom-core_test-module_v4",
	summary = "Ensure that consent for custom recurring payments can be carried out and that the payment is successfully scheduled\n" +
		"• Call the POST Consents endpoints with the schedule.custom.dates field set as D+1, D+7, D+32, D+129 and D+500\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is “AUTHORISED” and validate response\n" +
		"• Calls the POST Payments Endpoint with the 5 Payments, using the appropriate endToEndId for each of them\n" +
		"• Expects 201 - Validate Response\n" +
		"• Poll the Get Payments endpoint with the last PaymentID Created while payment status is RCVD or ACCP\n" +
		"• Call the GET Payments for the 5 Payments\n" +
		"• Expect Payment Scheduled in all of them (SCHD) - Validate Response\n" +
		"• Call the PATCH {consentId}\n" +
		"• Expect 200\n" +
		"• Delete the created client",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class FvpPaymentsApiRecurringPaymentsCustomCoreTestModuleV4 extends AbstractFvpPaymentApiSchedulingTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() { return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentConsentsV4Endpoint.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new CustomSchedulePaymentsWithStandardPayload();
	}
}
