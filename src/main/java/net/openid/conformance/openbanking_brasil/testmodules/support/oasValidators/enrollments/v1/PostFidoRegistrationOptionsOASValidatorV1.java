package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1;

import org.springframework.http.HttpMethod;

public class PostFidoRegistrationOptionsOASValidatorV1 extends AbstractEnrollmentsOASValidatorV1 {

	@Override
	protected String getEndpointPathSuffix() {
		return "/{enrollmentId}/fido-registration-options";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}
}
