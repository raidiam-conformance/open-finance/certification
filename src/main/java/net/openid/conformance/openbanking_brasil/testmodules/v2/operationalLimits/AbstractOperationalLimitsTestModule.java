package net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.frontchannel.BrowserControl;
import net.openid.conformance.info.ImageService;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.logging.TestInstanceEventLogIgnoreSuccess;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddOperationalUserToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAccessTokenExpirationTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.RecordCurrentTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200or423;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.EnsureHttpStatusIs422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.openbanking_brasil.testmodules.v2.GenerateRefreshAccessTokenSteps;
import net.openid.conformance.runner.TestExecutionManager;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.variant.ClientAuthType;

import java.util.Map;


public abstract class AbstractOperationalLimitsTestModule extends AbstractOBBrasilFunctionalTestModule {

	private TestInstanceEventLog originalLogger;
	private TestInstanceEventLog ignoreSuccessLogger;
	protected ClientAuthType clientAuthType;

	@Override
	public void setProperties(String id, Map<String, String> owner, TestInstanceEventLog eventLog, BrowserControl browser, TestInfoService testInfo, TestExecutionManager executionManager, ImageService imageService) {
		this.ignoreSuccessLogger = new TestInstanceEventLogIgnoreSuccess(eventLog);
		this.originalLogger = eventLog;
		super.setProperties(id, owner, eventLog, browser, testInfo, executionManager, imageService);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
		callAndStopOnFailure(AddOperationalUserToConfig.class);
		clientAuthType = getVariant(ClientAuthType.class);
		super.onConfigure(config, baseUrl);
	}

	protected void disableLogging() {
		if (!eventLog.equals(ignoreSuccessLogger)) {
			eventLog.log(getName(), "Logging is reduced. Only errors and warnings will be displayed");
			eventLog = ignoreSuccessLogger;
		}
	}

	protected void enableLogging() {
		if (!eventLog.equals(originalLogger)) {
			eventLog = originalLogger;
			eventLog.log(getName(), "Full logging is enabled");
		}
	}

	protected void runInLoggingBlock(Runnable runnable) {
		enableLogging();
		runnable.run();
		disableLogging();
	}

	@Override
	protected boolean scopeContains(String requiredScope) {
		return false;
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();

		call(exec().mapKey("endpoint_response", "consent_endpoint_response_full"));
		callAndContinueOnFailure(EnsureHttpStatusCodeIs201.class, Condition.ConditionResult.WARNING);

		if (getResult() == Result.WARNING) {
			callAndStopOnFailure(EnsureHttpStatusIs422.class);
			fireTestSkipped("The consent creation request has responded with 422 \"UNPROCESSABLE ENTITY\"." +
				"This implies that the server does not support the tested API which will be tracked on this test plan as a SKIPPED");
		} else {
			callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE);
			call(exec().unmapKey("endpoint_response"));
			validatePermissions();

			if (getResult() == Result.WARNING) {
				fireTestSkipped("The consent creation request did not return the valid PERMISSIONS for the tested API. " +
					"This implies that the server does not support the tested API which will be tracked on this test plan as a SKIPPED");
			} else {
				callAndContinueOnFailure(getPostConsentValidator(), Condition.ConditionResult.FAILURE);
				callAndStopOnFailure(ExtractConsentIdFromConsentEndpointResponse.class);
				callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI-R-6.2.1-11", "FAPI1-BASE-6.2.1-11");
				callAndStopOnFailure(FAPIBrazilAddConsentIdToClientScope.class);
			}
		}
	}


	protected Class<? extends OpenAPIJsonSchemaValidator> getPostConsentValidator(){
		return PostConsentOASValidatorV3n2.class;
	}

	protected void validatePermissions() {
		callAndContinueOnFailure(FAPIBrazilConsentEndpointResponseValidatePermissions.class, Condition.ConditionResult.WARNING);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(isSecondClient(), false, addTokenEndpointClientAuthentication, brazilPayments.isTrue(), false, true);
	}

	protected void refreshAccessToken() {
		runInLoggingBlock(() -> {
			GenerateRefreshAccessTokenSteps refreshAccessTokenSteps = new GenerateRefreshAccessTokenSteps(clientAuthType);
			call(refreshAccessTokenSteps);
		});
	}

	protected void makeOverOlCall(String endpointName) {
		runInLoggingBlock(() -> {
			eventLog.startBlock(String.format("Calling %s over the operational limit once", endpointName));
			call(new CallProtectedResourceExpectingFailureSequence()
				.insertAfter(CallProtectedResource.class, condition(EnsureResourceResponseCodeWas200or423.class)));
			expose(String.format("OL %s", endpointName), env.getInteger("resource_endpoint_response_full", "status").toString());
			eventLog.endBlock();
		});
	}


	@Override
	protected void preCallProtectedResource(String blockHeader) {
		checkAccessTokenExpirationTime();
		super.preCallProtectedResource(blockHeader);
	}

	@Override
	protected void preCallProtectedResource() {
		checkAccessTokenExpirationTime();
		super.preCallProtectedResource();
	}

	protected void checkAccessTokenExpirationTime() {
		callAndContinueOnFailure(CheckAccessTokenExpirationTime.class, Condition.ConditionResult.INFO);
		Boolean refresh = env.getBoolean("refresh");
		if(refresh != null && refresh){
			refreshAccessToken();
			env.putBoolean("refresh", false);
		}
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		super.call(builder);

		if (CallTokenEndpoint.class.equals(builder.getConditionClass())) {
			callAndStopOnFailure(RecordCurrentTime.class);
		}

	}

}
