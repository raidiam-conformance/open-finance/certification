package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class CreatePatchRecurringConsentsForEditionRequestBodyExpirationYesterdayNoAmount extends AbstractCreatePatchRecurringConsentsForEditionRequestBody {

	@Override
	protected String getExpirationDateTime() {
		LocalDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		LocalDateTime dateTime = currentDateTime.minusDays(1);
		return dateTime.format(DATE_TIME_FORMATTER);
	}

	@Override
	protected String getMaxVariableAmount() {
		return null;
	}
}
