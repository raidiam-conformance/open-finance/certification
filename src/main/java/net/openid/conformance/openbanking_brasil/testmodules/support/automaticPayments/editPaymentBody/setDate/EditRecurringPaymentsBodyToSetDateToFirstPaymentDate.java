package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EditRecurringPaymentsBodyToSetDateToFirstPaymentDate extends AbstractEditRecurringPaymentsBodyToSetDate {

	protected static String date;

	@Override
	@PreEnvironment(required = {"resource", "recurring_configuration_object"})
	public Environment evaluate(Environment env) {
		JsonObject recurringConfigObj = env.getObject("recurring_configuration_object");
		date = Optional.ofNullable(recurringConfigObj.getAsJsonObject("automatic"))
			.map(automatic -> automatic.getAsJsonObject("firstPayment"))
			.map(firstPayment -> firstPayment.get("date"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find firstPayment date inside automatic recurring configuration object",
				args("recurring configuration object", recurringConfigObj)));

		return super.evaluate(env);
	}

	@Override
	protected String getDate() {
		return date;
	}
}

