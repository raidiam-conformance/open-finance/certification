package net.openid.conformance.openbanking_brasil.testmodules.support;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

public class EnsureSpecificPermissionsWereReturned extends AbstractCondition {

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		JsonObject body;
		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "consent_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		JsonObject data = body.getAsJsonObject("data");
		if (data == null) {
			throw error("Data element is missing in the body", args("body", body));
		}

		JsonArray grantedPermissions = data.getAsJsonArray("permissions");
		Set<String> superset = new HashSet<>();
		grantedPermissions.forEach(e -> superset.add(OIDFJSON.getString(e)));

		String[] builtPermissions = env.getString("consent_permissions").split(" ");

		if(superset.containsAll(Set.of(builtPermissions))){
			logSuccess("Consent endpoint response matches expected permissions");
			return env;
		}

		throw error("Consent endpoint response does not contain expected permissions", args("granted", grantedPermissions, "expected", builtPermissions));
	}
}
