package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilCallPaymentConsentEndpointWithBearerToken;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddResourceUrlToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateSelfLinkRegex;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJWTAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;

import java.util.Objects;
import java.util.Optional;

public abstract class AbstractOBBrasilPaymentFunctionalTestModule extends AbstractOBBrasilFunctionalTestModule {

    protected abstract Class<? extends Condition> postPaymentConsentValidator();
    protected abstract Class<? extends Condition> postPaymentValidator();
    protected abstract Class<? extends Condition> getPaymentConsentValidator();
    protected abstract Class<? extends Condition> getPaymentValidator();

    @Override
    protected void setupResourceEndpoint() {
        callAndStopOnFailure(AddResourceUrlToConfig.class);
        super.setupResourceEndpoint();
    }

    @Override
    protected void validateClientConfiguration() {
		ScopesAndPermissionsBuilder scopesBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesBuilder.addScopes(returnScope(), OPFScopesEnum.OPEN_ID).build();
        super.validateClientConfiguration();
    }

    @Override
    protected void performPreAuthorizationSteps() {
        call(createOBBPreauthSteps());
        callAndStopOnFailure(SaveAccessToken.class);
        runInBlock(currentClientString() + "Validate consents response", this::validatePostConsentResponse);
    }

    @Override
    protected void validateGetConsentResponse() {
        runInBlock("Validate GET Consent Response", () -> {
            env.mapKey("endpoint_response", "consent_endpoint_response_full");
            callAndStopOnFailure(ExtractSignedJwtFromResourceResponse.class);
            env.unmapKey("endpoint_response");
            callAndContinueOnFailure(getPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
        });
    }

    protected void validatePostConsentResponse() {
        callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
		setScheduledPaymentDateTime();
        callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
        callAndStopOnFailure(PrepareToPostConsentRequest.class);
        callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
        configureDictInfo();
    }

    protected abstract void configureDictInfo();
	protected void setScheduledPaymentDateTime() {}

    @Override
    protected void performPostAuthorizationFlow() {
        fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
        env.unmapKey("access_token");
        validateGetConsentResponse();

        eventLog.startBlock(currentClientString() + "Call token endpoint");
        createAuthorizationCodeRequest();
        requestAuthorizationCode();
        requestProtectedResource();
        onPostAuthorizationFlowComplete();
    }

    protected void fetchConsentToCheckStatus(String status, AbstractEnsurePaymentConsentStatusWas statusCondition) {
        runInBlock(String.format("Checking the created consent - Expecting %s status", status), () -> {
            callAndStopOnFailure(PaymentConsentIdExtractor.class);
            callAndStopOnFailure(ExpectJWTResponse.class);
            callAndStopOnFailure(PrepareToFetchConsentRequest.class);
            callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(statusCondition.getClass(), Condition.ConditionResult.FAILURE);
        });
    }

    @Override
    protected void requestProtectedResource() {
        if (!validationStarted) {
            validationStarted = true;
            startPixPaymentsBlock();
            ConditionSequence pixSequence = getPixPaymentSequence();
            call(pixSequence);
            eventLog.endBlock();
            eventLog.startBlock(currentClientString() + "Validate response");
            validateResponse();
            eventLog.endBlock();
        }
    }

	protected void startPixPaymentsBlock() {
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
	}

    protected ConditionSequence getPixPaymentSequence() {
        return new CallPixPaymentsEndpointSequence();
    }

    @Override
    protected void validateResponse() {
		call(postPaymentValidationSequence());
		runRepeatSequence();
        validateFinalState();

    }

	protected void runRepeatSequence() {
		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(5)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();
	}

    protected void validateFinalState() {
        callAndStopOnFailure(EnsurePaymentStatusWasAcsc.class);
    }

    protected ConditionSequence getRepeatSequence() {
        return new ValidateSelfEndpoint()
            .replace(CallProtectedResource.class, sequenceOf(
                condition(AddJWTAcceptHeaderRequest.class),
                condition(CallProtectedResource.class)
            ))
            .skip(SaveOldValues.class, "Not saving old values")
            .skip(LoadOldValues.class, "Not loading old values")
            .insertAfter(EnsureResourceResponseCodeWas200.class, condition(getPaymentPollStatusCondition()));
    }

    protected Class<? extends Condition> getPaymentPollStatusCondition() {
        return CheckPaymentPollStatusRcvdOrAccpOrAcpdV2.class;
    }

    protected ConditionSequence postPaymentValidationSequence() {
        return sequenceOf(
            condition(postPaymentValidator())
                .dontStopOnFailure()
                .onFail(Condition.ConditionResult.FAILURE)
        );
    }

    protected ConditionSequence getPaymentValidationSequence() {
        return sequenceOf(
            condition(getPaymentValidator())
                .dontStopOnFailure()
                .onFail(Condition.ConditionResult.FAILURE)
        );
    }

    @Override
    protected void call(ConditionCallBuilder builder) {
        forceGetCallToUseClientCredentialsToken(builder);

        super.call(builder);

        if (CallProtectedResource.class.equals(builder.getConditionClass())) {
            validateSelfLink("resource_endpoint_response_full");
        }

        if (FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
            FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class.equals(builder.getConditionClass()) ||
            FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders.class.equals(builder.getConditionClass())) {
            validateSelfLink("consent_endpoint_response_full");
        }
    }

	protected void useClientCredentialsAccessToken(){
		env.mapKey("access_token", "old_access_token");
	}

	protected void userAuthorisationCodeAccessToken(){
		env.unmapKey("access_token");

	}

    protected void forceGetCallToUseClientCredentialsToken(ConditionCallBuilder builder) {
        if (CallProtectedResource.class.isAssignableFrom(builder.getConditionClass())) {

			String resourceMethod = "";
			if(CallProtectedResource.class.equals(builder.getConditionClass())){
					resourceMethod = OIDFJSON.getString(Optional.ofNullable(
					env.getElementFromObject("resource", "resourceMethod")).orElse(new JsonPrimitive("")));
			}
			if(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class.equals(builder.getConditionClass())){
				resourceMethod = Optional.ofNullable(env.getString("http_method")).orElse("");
			}

            if (Objects.equals(resourceMethod, "GET") && env.containsObject("old_access_token")) {
                eventLog.log("Load client_credential access token", env.getObject("old_access_token"));
				useClientCredentialsAccessToken();
            }
        }
    }

    private boolean isEndpointCallSuccessful(int status) {
        return status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK;
    }

    protected void validateSelfLink(String responseFull) {

        String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");

        int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
            .orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));

        String expectsFailure = Optional.ofNullable(env.getString("expects_failure")).orElse("false");

        if (!recursion.equals("true") && isEndpointCallSuccessful(status) && !expectsFailure.equals("true")) {

            env.putString("recursion", "true");
            call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
            env.mapKey("resource_endpoint_response_full", responseFull);

            ConditionSequence sequence = new ValidateSelfEndpoint().replace(CallProtectedResource.class, sequenceOf(
                condition(AddJWTAcceptHeaderRequest.class),
                condition(CallProtectedResource.class)
            ));

            if (responseFull.contains("consent")) {
                sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));

                env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
            } else {
                env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
            }
            callAndContinueOnFailure(ValidateSelfLinkRegex.class, Condition.ConditionResult.FAILURE);
            env.unmapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY);

            call(sequence);
            env.unmapKey("resource_endpoint_response_full");
            env.putString("recursion", "false");
        }
        env.removeNativeValue("expects_failure");
    }

	protected OPFScopesEnum returnScope() {
		return OPFScopesEnum.PAYMENTS;
	}
}
