package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddClientCredentialsGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddImplicitGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddRedirectUriToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddRefreshTokenGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddTlsClientAuthSubjectDnToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment;
import net.openid.conformance.condition.client.CallClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CallTokenEndpointAndReturnFullResponse;
import net.openid.conformance.condition.client.CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckClientConfigurationUriFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckClientIdFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckForAccessTokenValue;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CheckRedirectUrisFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckRegistrationClientEndpointContentType;
import net.openid.conformance.condition.client.CheckRegistrationClientEndpointContentTypeHttpStatus200;
import net.openid.conformance.condition.client.CheckTokenEndpointHttpStatus200;
import net.openid.conformance.condition.client.CreateEmptyDynamicRegistrationRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractClientNameFromStoredConfig;
import net.openid.conformance.condition.client.ExtractJWKSDirectFromClientConfiguration;
import net.openid.conformance.condition.client.ExtractMTLSCertificatesFromConfiguration;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetResponseTypeCodeIdTokenInDynamicRegistrationRequest;
import net.openid.conformance.condition.client.SetResponseTypeCodeInDynamicRegistrationRequest;
import net.openid.conformance.condition.client.StoreOriginalClientConfiguration;
import net.openid.conformance.condition.client.ValidateMTLSCertificatesHeader;
import net.openid.conformance.condition.common.CheckDistinctKeyIdValueInClientJWKs;
import net.openid.conformance.fapi1advancedfinal.AbstractFAPI1AdvancedFinalBrazilDCR;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddResourceUrlToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddScopeToClientConfigurationFromConsentUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractPaymentId;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.AddOpenIdScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantConfigurationFields;
import net.openid.conformance.variant.VariantHidesConfigurationFields;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;
import java.util.function.Supplier;

@VariantConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.brazilOrganizationId",
	"directory.apibase",
	"resource.loggedUserIdentification",
	"resource.businessEntityIdentification",
	"resource.debtorAccountIspb",
	"resource.debtorAccountIssuer",
	"resource.debtorAccountNumber",
	"resource.debtorAccountType",
	"resource.paymentAmount",
	"server.discoveryUrl",
	"directory.discoveryUrl"
})
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"consent.productType",
	"resource.brazilCpf",
	"resource.brazilCnpj",
})
public abstract class AbstractBrazilDCRPaymentsWebhook extends AbstractFAPI1AdvancedFinalBrazilDCR {
	@Override
	public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
		Boolean expectingPaymentWebhook = env.getBoolean("payment_webhook_received");
		Boolean expectingPaymentConsentWebhook = env.getBoolean("payment_consent_webhook_received");
		if (expectingPaymentConsentWebhook != null && path.matches("^(open-banking\\/webhook\\/v\\d+\\/payments\\/v\\d+\\/consents\\/)(urn:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+)$") && expectingPaymentConsentWebhook) {
			addWebhookToObjectInEnv(env, true, false, "webhooks_received_consent", requestParts, path);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else if (expectingPaymentWebhook != null && path.matches("^(open-banking\\/webhook\\/v\\d+\\/payments\\/v\\d+\\/pix\\/payments\\/)([a-zA-Z0-9][a-zA-Z0-9\\-]{0,99})$") && expectingPaymentWebhook) {
			addWebhookToObjectInEnv(env, false, true, "webhooks_received_payment", requestParts, path);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else {
			throw new TestFailureException(getId(), "Got a webhook response we weren't expecting");
		}
	}

	public void addWebhookToObjectInEnv(Environment env, boolean consents, boolean payments, String key, JsonObject requestParts, String path){
		JsonObject webhooksReceived = env.getObject(key);
		JsonArray webhooks = webhooksReceived.get("webhooks").getAsJsonArray();
		if (consents){
			requestParts.addProperty("time_received", Instant.now().toString());
			requestParts.addProperty("type", "consent");
			requestParts.addProperty("path", path);
			webhooks.add(requestParts);
			env.putObject(key, webhooksReceived);
		}
		if (payments){
			requestParts.addProperty("time_received", Instant.now().toString());
			requestParts.addProperty("type", "payment");
			requestParts.addProperty("path", path);
			webhooks.add(requestParts);
			env.putObject(key, webhooksReceived);
		}
	}
	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(AddResourceUrlToConfig.class);
		super.setupResourceEndpoint();
	}

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		getPaymentAndPaymentConsentEndpoints();
		callAndStopOnFailure(AddScopeToClientConfigurationFromConsentUrl.class);
		doDcr();
		eventLog.startBlock("Waiting for webhook to be synced");
		callAndStopOnFailure(WaitForSpecifiedTimeFromConfigInSeconds.class);
		eventLog.endBlock();
	}

	protected abstract void getPaymentAndPaymentConsentEndpoints();
	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		initialiseWebhookObject("consent");
		initialiseWebhookObject("payment");
		callAndStopOnFailure(AddOpenIdScope.class);
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
		setScheduledPaymentDateTime();
		setBrazilPixPaymentObject();
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		super.onConfigure(config, baseUrl);
	}

	protected abstract void setBrazilPixPaymentObject();

	protected void initialiseWebhookObject(String type){
		JsonObject webhooksReceived = new JsonObject();
		JsonArray webhooks = new JsonArray();
		webhooksReceived.add("webhooks", webhooks);
		env.putObject("webhooks_received_" + type, webhooksReceived);
	}

	protected void setScheduledPaymentDateTime() {

	}
	protected void validatePaymentResponse(boolean isPostPayment) {
		if (isPostPayment) {
			call(postPaymentValidationSequence());
		} else {
			call(getPaymentValidationSequence());
		}
	}
	protected void validatePaymentStatus(){
		callAndStopOnFailure(EnsurePaymentStatusWasAcsc.class);
	}
	@Override
	protected void requestProtectedResource() {
		enablePaymentWebhook();
		createPaymentWebhook();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		ConditionSequence pixSequence = new CallPixPaymentsEndpointSequence();
		call(pixSequence);
		eventLog.endBlock();
		eventLog.startBlock(currentClientString() + "Validate response");
		validatePaymentResponse(true);
		callAndStopOnFailure(ExtractPaymentId.class);
		eventLog.endBlock();
		waitForWebhookResponse();
		validatePaymentConsentWebhooks();
		validatePaymentWebhooks();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		performClientCredentialsGrant();
		call(getPayment());
		eventLog.endBlock();
		eventLog.startBlock(currentClientString() + "Validate response");
		validatePaymentResponse(false);
		validatePaymentStatus();
		eventLog.endBlock();
	}

	protected void validatePaymentConsentWebhooks(){
		env.putBoolean("payment_consent_webhook_received", false);
		callAndStopOnFailure(ValidatePaymentConsentWebhooks.class);
	}

	protected void validatePaymentWebhooks(){
		env.putBoolean("payment_webhook_received", false);
		callAndStopOnFailure(ValidatePaymentWebhooks.class);
	}
	protected void performClientCredentialsGrant() {
		callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
		callAndStopOnFailure(SetPaymentsScopeOnTokenEndpointRequest.class);
		call(sequence(addTokenEndpointClientAuthentication));
		callAndStopOnFailure(CallTokenEndpointAndReturnFullResponse.class);
		callAndContinueOnFailure(CheckTokenEndpointHttpStatus200.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
		callAndStopOnFailure(CheckForAccessTokenValue.class);
		callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);
	}

	public void createPaymentWebhook() {
		callAndStopOnFailure(setPaymentWebhookCreator());
		exposeEnvString("webhook_uri_payment_id");
	}

	protected void waitForWebhookResponse() {
		long delaySeconds = 5;
		eventLog.startBlock(currentClientString() + "The test will now wait for the webhook to be received");
		for (int attempts = 0; attempts < 20; attempts++) {
			eventLog.log(getId(), "Waiting for webhook to be received at with an interval of " + delaySeconds + " seconds");
			setStatus(Status.WAITING);
			try {
				Thread.sleep(delaySeconds * 1000);
			} catch (InterruptedException e) {
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}
			setStatus(Status.RUNNING);

		}
		eventLog.endBlock();
	}

	protected ConditionSequence getPaymentValidationSequence(){
		return sequenceOf(
			condition(setGetPaymentnValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
	}
	protected ConditionSequence postPaymentValidationSequence(){
		return sequenceOf(
			condition(setPostPaymentValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
	}

	protected abstract Class<? extends Condition> setGetPaymentnValidator();
	protected abstract Class<? extends Condition> setPostPaymentValidator();
	protected ConditionSequenceRepeater repeatSequence(Supplier<ConditionSequence> conditionSequenceSupplier) {
		return new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager, conditionSequenceSupplier);
	}

	protected ConditionSequence getPayment() {
		return new ValidateSelfEndpoint()
			.replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeader.class),
				condition(CallProtectedResource.class)
			))
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values");
	}
	@Override
	protected void performPreAuthorizationSteps() {
		enablePaymentConsentWebhook();
		createConsentWebhook();
		super.performPreAuthorizationSteps();
		validateConsentResponse(setPostConsentValidator());
	}
	protected void enablePaymentConsentWebhook(){
		env.putString("time_of_consent_request", Instant.now().toString());
		env.putBoolean("payment_consent_webhook_received", true);
	}

	protected void enablePaymentWebhook(){
		env.putString("time_of_payment_request", Instant.now().toString());
		env.putBoolean("payment_webhook_received", true);
	}
	protected void validateGetConsentResponse() {
		runInBlock("Validate GET Consent Response", () -> {
			validateConsentResponse(setGetConsentValidator());
		});
	}
	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckAuthorisedStatus();
		validateGetConsentResponse();
		super.performPostAuthorizationFlow();
	}
	protected void fetchConsentToCheckAuthorisedStatus() {
		eventLog.startBlock("Checking the created consent - Expecting AUTHORISED status");
		callAndStopOnFailure(PaymentConsentIdExtractor.class);
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAuthorised.class, Condition.ConditionResult.WARNING);
		eventLog.endBlock();
	}
	protected void validateConsentResponse(Class<? extends Condition> consentResponseValidation) {
		callAndStopOnFailure(consentResponseValidation, Condition.ConditionResult.FAILURE);
	}
	protected abstract Class<? extends Condition> setGetConsentValidator();

	protected abstract Class<? extends Condition> setPostConsentValidator();
	public void createConsentWebhook(){
		callAndStopOnFailure(setPaymentConsentWebhookCreator());
		exposeEnvString("webhook_uri_consent_id");
	}
	protected abstract Class<? extends Condition> setPaymentWebhookCreator();
	protected abstract Class<? extends Condition> setPaymentConsentWebhookCreator();
	@Override
	protected void getSsa() {
		super.getSsa();
		validateWebhookOnSoftwareStatement();
	}

	protected void validateWebhookOnSoftwareStatement(){
		callAndStopOnFailure(EnsureSoftwareStatementContainsWebhook.class);
	}
	@Override
	protected void logFinalEnv(){

	}

	protected void doDcr(){
		ClientAuthType clientAuthType = getVariant(ClientAuthType.class);
		String clientAuth = clientAuthType.toString();
		if (clientAuthType == ClientAuthType.MTLS) {
			// the FAPI auth type variant doesn't use the name required for the registration request as per
			// https://datatracker.ietf.org/doc/html/rfc8705#section-2.1.1
			clientAuth = "tls_client_auth";
		}
		env.putString("client_auth_type", clientAuth);

		callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
		callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);

		// normally our DCR tests create a key on the fly to use, but in this case the key has to be registered
		// manually with the central directory so we must use user supplied keys
		callAndStopOnFailure(ExtractJWKSDirectFromClientConfiguration.class);

		callAndContinueOnFailure(CheckDistinctKeyIdValueInClientJWKs.class, Condition.ConditionResult.FAILURE, "RFC7517-4.5");

		getSsa();

		eventLog.startBlock("Perform Dynamic Client Registration");

		callAndStopOnFailure(StoreOriginalClientConfiguration.class);
		callAndStopOnFailure(ExtractClientNameFromStoredConfig.class);

		setupJwksUri();

		// create basic dynamic registration request
		callAndStopOnFailure(CreateEmptyDynamicRegistrationRequest.class);

		callAndStopOnFailure(AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest.class);
		if (!jarm.isTrue()) {
			// implicit is only required when id_token is returned in frontchannel
			callAndStopOnFailure(AddImplicitGrantTypeToDynamicRegistrationRequest.class);
		}
		callAndStopOnFailure(AddRefreshTokenGrantTypeToDynamicRegistrationRequest.class);
		callAndStopOnFailure(AddClientCredentialsGrantTypeToDynamicRegistrationRequest.class);

		if (clientAuthType == ClientAuthType.MTLS) {
			callAndStopOnFailure(AddTlsClientAuthSubjectDnToDynamicRegistrationRequest.class);
		}

		addJwksToRequest();
		callAndStopOnFailure(AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment.class);
		if (jarm.isTrue()) {
			callAndStopOnFailure(SetResponseTypeCodeInDynamicRegistrationRequest.class);
		} else {
			callAndStopOnFailure(SetResponseTypeCodeIdTokenInDynamicRegistrationRequest.class);
		}
		validateSsa();
		callAndStopOnFailure(AddRedirectUriToDynamicRegistrationRequest.class);

		addSoftwareStatementToRegistrationRequest();

		addWebhookUrisToDynamicRegistrationRequest();

		callRegistrationEndpoint();
	}

	protected void addWebhookUrisToDynamicRegistrationRequest() {
		callAndStopOnFailure(AddWebhookUrisToDynamicRegistrationRequest.class);
	}

	protected void callClientConfigurationEndpoint() {

		callAndStopOnFailure(CallClientConfigurationEndpoint.class);
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentTypeHttpStatus200.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentType.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckClientIdFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckRedirectUrisFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationUriFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
	}

	protected void runInBlock(String blockText, Runnable actor) {
		eventLog.startBlock(blockText);
		actor.run();
		eventLog.endBlock();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps(){
		OpenBankingBrazilPreAuthorizationErrorAgnosticSteps steps = (OpenBankingBrazilPreAuthorizationErrorAgnosticSteps) new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
			);
		return steps;
	}

	protected ConditionSequence getRepeatSequence() {
		return new ValidateSelfEndpoint()
			.replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeader.class),
				condition(CallProtectedResource.class)
			))
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values")
			.insertAfter(EnsureResourceResponseCodeWas200.class, condition(getPaymentPollStatusCondition()));
	}
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrAccpOrAcpdV2.class;
	}

	protected void validateFinalState() {
		callAndStopOnFailure(EnsurePaymentStatusWasAcsc.class);
	}
}
