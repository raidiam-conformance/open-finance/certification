package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public class GetLoansWarrantiesV2n4OASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/loans/loans-v2.4.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/contracts/{contractId}/warranties";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


}
