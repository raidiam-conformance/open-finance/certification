package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1.GetCreditFixedIncomesBalancesV1OASValidator;

public class GetCreditFixedIncomesBalancesV1n3OASValidator extends GetCreditFixedIncomesBalancesV1OASValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/creditFixedIncomes/credit-fixed-incomes-v1.0.3.yml";
	}

}
