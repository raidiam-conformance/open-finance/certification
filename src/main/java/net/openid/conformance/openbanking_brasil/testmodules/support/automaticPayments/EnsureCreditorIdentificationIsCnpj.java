package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EnsureCreditorIdentificationIsCnpj extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		String creditorCpfCnpj = Optional.ofNullable(env.getElementFromObject("resource", "creditorCpfCnpj"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find \"creditorCpfCnpj\" in the resource"));

		if (creditorCpfCnpj.length() != 14) {
			throw error("For automatic pix tests, the \"resource.creditorCpfCnpj\" field should be filled with a CNPJ");
		}
		logSuccess("The \"resource.creditorCpfCnpj\" field is filled with a CNPJ");

		return env;
	}
}
