package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.pension.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

import java.util.Optional;

@ApiName("Pension Survival Coverages V2")
public class GetSurvivalCoveragesOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/pension/swagger-pension-2.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/survival-coverages";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		for(JsonElement dataElement : body.getAsJsonArray("data")) {
			JsonObject data = dataElement.getAsJsonObject();
			assertPaymentMethodAdittionalInfoConstraint(data);
		}
	}

	protected void assertPaymentMethodAdittionalInfoConstraint(JsonObject data) {
		JsonArray defferalPeriods = JsonPath.read(data, "$.society.products[*].defferalPeriod");
		for(JsonElement defferalPeriodElement : defferalPeriods) {
			JsonObject defferalPeriod = defferalPeriodElement.getAsJsonObject();
			JsonArray premiumPaymentMethods = Optional.ofNullable(defferalPeriod.getAsJsonArray("paymentMethods")).orElse(new JsonArray());
			if(premiumPaymentMethods.contains(new JsonPrimitive("OUTROS")) && defferalPeriod.get("premiumPaymentMethodsAdditionalInfo") == null) {
				throw error("data.society.products.premiumPayment.premiumPaymentMethodsAdditionalInfo is required when data.society.products.premiumPayment.paymentMethods contains OUTROS");
			}
		}
	}
}
