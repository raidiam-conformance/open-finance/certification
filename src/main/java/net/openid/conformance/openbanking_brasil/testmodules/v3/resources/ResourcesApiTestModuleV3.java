package net.openid.conformance.openbanking_brasil.testmodules.v3.resources;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ResourceApiV2PollingSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "resources_api_core_test-module_v3",
	displayName = "Validate structure of all resources API V3 resources",
	summary = "Validates the structure of all resources API V3 resources\n" +
		"\u2022 Call the POST Consents API with will all of the existing permissions. \n" +
		"\u2022 Expect a 201 - Checks all of the fields sent on the consent API are specification compliant  \n" +
		"\u2022 Call the GET Consents API with a client_credentials issued token\n" +
		"\u2022 Expect a 200 - Make sure consent is on “AWAITING_AUTHORISATION” - Validate response payload\n" +
		"\u2022 Redirect User to Authorize the Consent and obtain valid access token\n" +
		"\u2022 Call the GET Consents API with a client_credentials issued token\n" +
		"\u2022 Expect a 200 - Make sure consent is on “AUTHORISED” - Validate response payload\n" +
		"\u2022 Calls the GET resources API V3 with the Polling Resources API sequence\n" +
		"\u2022 Expects a 200 - Validate that the all returned fields are aligned with the specification\n" +
		"\u2022 Call the DELETE Consents API\n" +
		"\u2022 Expects a 204",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCnpj",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ResourcesApiTestModuleV3 extends AbstractPhase2TestModule {


	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		String productType = env.getString("config", "consent.productType");
		if (productType.equals("business")) {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ALL_BUSINESS_PHASE2).addScopes(OPFScopesEnum.RESOURCES, OPFScopesEnum.OPEN_ID).build();
		} else {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ALL_PERSONAL_PHASE2).addScopes(OPFScopesEnum.RESOURCES, OPFScopesEnum.OPEN_ID).build();
		}
	}


	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetResourcesV3Endpoint.class)

		);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false)
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().mapKey("resource_endpoint_response", "consent_endpoint_response"))
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, condition(PostConsentOASValidatorV3n2.class).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure())
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().unmapKey("resource_endpoint_response"));
	}


	@Override
	protected void validateResponse() {
		ResourceApiV2PollingSteps pollingSteps = new ResourceApiV2PollingSteps(env, getId(),
			eventLog, testInfo, getTestExecutionManager());
		runInBlock("Polling Resources API", () -> call(pollingSteps));

		callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
		callAndContinueOnFailure(GetResourcesOASValidatorV3.class, Condition.ConditionResult.FAILURE);
	}
}
