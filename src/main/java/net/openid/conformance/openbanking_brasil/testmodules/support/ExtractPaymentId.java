package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JWTUtil;

import java.text.ParseException;

public class ExtractPaymentId extends AbstractCondition {
	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {
		String body = OIDFJSON.getString(env.getElementFromObject("resource_endpoint_response_full", "body"));
		JsonObject decodedJwt;

		try {
			decodedJwt = JWTUtil.jwtStringToJsonObjectForEnvironment(body);
		}  catch (ParseException e) {
			throw error("Couldn't parse response as JWT", e, args("body", body));
		}
		logSuccess("Parsed response as JWT", decodedJwt);
		JsonElement data = decodedJwt.get("claims").getAsJsonObject().get("data");
		JsonElement paymentId;
		if (data.isJsonArray()) {
			paymentId = decodedJwt.get("claims").getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("paymentId");
		} else {
		 	paymentId = decodedJwt.get("claims").getAsJsonObject().get("data").getAsJsonObject().get("paymentId");
		}
		if (paymentId != null) {
			env.putString("payment_id", OIDFJSON.getString(paymentId));
			logSuccess("Extracted payment ID", args("payment_id", paymentId));
			return env;
		} else {
			throw error("Couldn't find payment ID in response");
		}
	}
}
