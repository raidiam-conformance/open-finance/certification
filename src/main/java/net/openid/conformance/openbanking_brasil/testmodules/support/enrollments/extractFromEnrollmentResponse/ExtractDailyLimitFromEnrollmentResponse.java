package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse;

public class ExtractDailyLimitFromEnrollmentResponse extends AbstractExtractFromEnrollmentResponse {

	@Override
	protected String fieldName() {
		return "dailyLimit";
	}

	@Override
	protected String envVarName() {
		return "daily_limit";
	}
}
