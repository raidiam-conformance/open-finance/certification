package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class RemoveAliasFromRedirectUri extends AbstractCondition {
	@Override
	@PostEnvironment(strings = "redirect_uri")
	@PreEnvironment(strings = "redirect_uri", required = "config")
	public Environment evaluate(Environment env) {
		String originalRedirectUri = env.getString("redirect_uri");
		JsonObject config = env.getObject("config");
		String alias = OIDFJSON.getString(config.get("alias"));
		String subStringToRemove = "/a/"+ alias;
		originalRedirectUri = originalRedirectUri.replaceAll(subStringToRemove, "");
		env.putString("redirect_uri", originalRedirectUri);
		logSuccess("Removed alias from redirect_uri", args("redirect_uri", originalRedirectUri));
		return env;
	}
}
