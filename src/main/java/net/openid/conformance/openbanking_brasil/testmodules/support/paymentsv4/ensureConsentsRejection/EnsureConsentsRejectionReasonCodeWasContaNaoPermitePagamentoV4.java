package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureConsentsRejection;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.AbstractEnsureConsentsRejectionReasonCodeWasX;
import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v4.enums.PaymentConsentRejectionReasonEnumV4;

import java.util.List;

public class EnsureConsentsRejectionReasonCodeWasContaNaoPermitePagamentoV4 extends AbstractEnsureConsentsRejectionReasonCodeWasX {

    @Override
    protected List<String> getExpectedRejectionCodes() {
        return List.of(PaymentConsentRejectionReasonEnumV4.CONTA_NAO_PERMITE_PAGAMENTO.toString());
    }
}
