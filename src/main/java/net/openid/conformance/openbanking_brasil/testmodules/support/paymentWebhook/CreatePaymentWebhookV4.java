package net.openid.conformance.openbanking_brasil.testmodules.support.paymentWebhook;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractCreateWebhookEndpoint;

public class CreatePaymentWebhookV4 extends AbstractCreateWebhookEndpoint {
	@Override
	protected String getValueFromEnv() {
			return "payment_id";
	}

	@Override
	protected String getExpectedUrlPath() {
		return "/open-banking/webhook/v1/payments/v4/pix/payments/";
	}
}
