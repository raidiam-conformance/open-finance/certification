package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo;

import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpMethod;

public class PrepareToPostEnrollments extends AbstractPrepareToMakeACallToEnrollmentsEndpoint {

	@Override
	protected HttpMethod httpMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected String endpoint() {
		return "";
	}

	@Override
	protected boolean expectsSelfLink() {
		return true;
	}

	@Override
	protected String getEnrollmentId(Environment env) {
		return null;
	}

	@Override
	protected String createUrl(String enrollmentsUrl, String enrollmentId) {
		return enrollmentsUrl;
	}
}
