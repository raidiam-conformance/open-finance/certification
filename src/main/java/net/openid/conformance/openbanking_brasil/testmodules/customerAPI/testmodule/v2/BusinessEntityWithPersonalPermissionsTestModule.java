package net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureBrazilCNPJ;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureBusinessEntityInConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ForcePersonalProductType;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetBusinessIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasPermissoesPjIncorretas;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "customer-business_api_businessentity-personal-permissions_test-module_v2",
	displayName = "Validate the structure of all consent API resources",
	summary = "This test makes sure that if businessEntity is sent on the consent creation together with personal customer data permission the server will reject the request\n" +
		"\u2022 Make sure that the user has sent a data Payload that contains a BusinessEntity \n" +
		"\u2022 Make a consent request with all the permissions but the Customer Business ones \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class BusinessEntityWithPersonalPermissionsTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validating create consent response", () -> {
			call(new ValidateWellKnownUriSteps());
			call(new ValidateRegisteredEndpoints(sequenceOf(
				condition(GetConsentV3Endpoint.class),
				condition(GetBusinessIdentificationsV2Endpoint.class)
			)));
			callAndStopOnFailure(EnsureBrazilCNPJ.class);
			callAndStopOnFailure(ForcePersonalProductType.class);
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(SetConsentsScopeOnTokenEndpointRequest.class);
			callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
			callAndStopOnFailure(SetContentTypeApplicationJson.class);
			callAndStopOnFailure(EnsureBusinessEntityInConsentRequest.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerToken.class, Condition.ConditionResult.INFO);
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(PostConsentOASValidatorV3n2.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeWasPermissoesPjIncorretas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeWasPermissoesPjIncorretas.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeWasPermissoesPjIncorretas.RESPONSE_ENV_KEY);
		});
	}


}
