package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import org.springframework.http.HttpMethod;

public class PostRecurringPaymentPixOASValidatorV2 extends AbstractRecurringPaymentPixOASValidatorV2 {

	@Override
	protected String getEndpointPath() {
		return "/pix/recurring-payments";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}
}
