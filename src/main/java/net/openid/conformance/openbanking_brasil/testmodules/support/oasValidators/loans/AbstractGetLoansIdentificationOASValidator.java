package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;


public abstract class AbstractGetLoansIdentificationOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/contracts/{contractId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertData(data);
		assertInterestRates(data);
		assertFinanceCharges(data);
		assertContractedFees(data);
	}

	private void assertData(JsonObject data) {

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"amortizationScheduledAdditionalInfo",
			"amortizationScheduled",
			"OUTROS"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"cnpjConsignee",
			"productSubType",
			"CREDITO_PESSOAL_COM_CONSIGNACAO"
		);
	}


	private void assertInterestRates(JsonObject data) {

		data.getAsJsonArray("interestRates").forEach(el -> {
			JsonObject interestRate = el.getAsJsonObject();

			if (OIDFJSON.getString(findByPath(interestRate, "referentialRateIndexerType")).equals("OUTROS_INDEXADORES") &&
				interestRate.has("referentialRateIndexerSubType") &&
				OIDFJSON.getString(findByPath(interestRate, "referentialRateIndexerSubType")).equals("OUTROS_INDEXADORES") &&
				!interestRate.has("referentialRateIndexerAdditionalInfo")
			) {
				throw error("referentialRateIndexerAdditionalInfo is required when referentialRateIndexerType and referentialRateIndexerSubType equals to OUTROS_INDEXADORES");
			}


		});


	}

	private void assertFinanceCharges(JsonObject data) {
		data.getAsJsonArray("contractedFinanceCharges").forEach(el ->
			assertField1IsRequiredWhenField2HasValue2(
				el.getAsJsonObject(),
				"chargeAdditionalInfo",
				"chargeType",
				"OUTROS"
			));


	}

	private void assertContractedFees(JsonObject data) {


		data.getAsJsonArray("contractedFees").forEach(el -> {

				assertField1IsRequiredWhenField2HasValue2(
					el.getAsJsonObject(),
					"feeRate",
					"feeCharge",
					"PERCENTUAL"
				);

				assertField1IsRequiredWhenField2HasNotValue2(
					el.getAsJsonObject(),
					"feeAmount",
					"feeCharge",
					"PERCENTUAL"
				);

			}

		);
	}
}
