package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1.GetVariableIncomesListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1.GetVariableIncomesTransactionsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToVariableIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo365DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "variable-incomes_api_pagination-transaction_test-module",
	displayName = "variable-incomes_api_pagination_test-module",
	summary = "This test will validate the pagination on the investments transaction current endpoint. To have a successful execution, you should have at least 51 records at this endpoint.\n" +
		"Call the POST Consents endpoint with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
		"• Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
		"• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
		"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"• Call the GET Consents endpoint\n" +
		"• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
		"• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
		"• Call the GET Investments List Endpoint\n" +
		"• Expects a 200 response - Validate the Response_body - Extract one of the shared investment products\n" +
		"• Call the GET Investments Transactions endpoint with the extracted investmentID with page-size equal to 1000 - Send query Params fromTransactionDate equal D-365 and toTransactionDate equal to D+0, with current date set as UTC-3\n" +
		"• Expects a 200 response - Ensure that at least 25 records are back - Validate the Response_body\n" +
		"• Call the GET Investments Transactions endpoint with the extracted investmentID and send page-size equal to 51 \n" +
		"• Expects a 200 response - Validate Response and ensure that at least 25 records have been returned\n" +
		"• Call the GET Investments Transactions endpoint with the extracted investmentID and send page-size equal to 1001 - Send query Params fromTransactionDate equal D-365 and toTransactionDate equal to D+0, with current date set as UTC-3\n" +
		"• Expects a 422 response - Validate the Response_body\n" +
		"• Call the GET Investments Transactions endpoint with page-size equal to 25 - Send query Params fromTransactionDate equal D-365 and toTransactionDate equal to D+0, with current date set as UTC-3\n" +
		"• Expect a 201 response - Validate response and ensure that the number of returned transactions is equal to 25. Perform all required links validation, including, but not limited to, ensuring the next uri is different to the self uri, ensure the prev hasn't been sent \n" +
		"• Call the GET Investments Transactions endpoint with the next link returned on the previous call - Send query Params fromTransactionDate equal D-365 and toTransactionDate equal to D+0, with current date set as UTC-3\n" +
		"• Expects a 200 response - Validate response and ensure that the number of returned transactions is equal to 25. Perform all required links validation, including, but not limited to, ensuring the next uri is different to the self uri, ensure the prev, self and next uris are different and are pointing to pages 1, 2 and 3 respectively\n" +
		"• Call the Delete Consents Endpoints\n" +
		"• Expect a 204 without a body",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class VariableIncomesApiPaginationTestModule extends AbstractInvestmentsApiPaginationTestModule {

	@Override
	protected AbstractSetInvestmentApi setInvestmentsApi(){
		return new SetInvestmentApiToVariableIncomes();
	}

	@Override
	protected  Class<? extends Condition> investmentsRootValidator(){
		return GetVariableIncomesListV1OASValidator.class;
	}

	@Override
	protected String endpointName() {
		return "Transactions";
	}

	@Override
	protected ResourceBuilder urlPreparingCondition() {
		return new PrepareUrlForFetchingInvestmentTransactions();
	}

	@Override
	protected Class<? extends Condition> setFromTransaction() {
		return SetFromTransactionDateTo365DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> setToTransactionToToday() {
		return SetToTransactionDateToToday.class;
	}

	@Override
	protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
		return AddToAndFromTransactionDateParametersToProtectedResourceUrl.class;
	}

	@Override
	protected Class<? extends Condition> validator() {
		return GetVariableIncomesTransactionsV1OASValidator.class;
	}

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.VARIABLE_INCOMES;
	}
}
