package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule5DailyPaymentsStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule6DailyPaymentsStarting1DayInTheFuture;

public abstract class AbstractPaymentsApiRecurringPaymentsHigherQuantityTestModule extends AbstractPaymentApiSchedulingUnhappyTestModule {


	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new Schedule6DailyPaymentsStarting1DayInTheFuture();
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(Schedule5DailyPaymentsStarting1DayInTheFuture.class);
	}

}
