package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.field.ExtraField;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetTransactionsOASValidatorV2n4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/accounts/swagger-accounts-2.4.1.yml";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		for(JsonElement dataElement : body.getAsJsonArray("data")) {
			JsonObject data = dataElement.getAsJsonObject();
			this.assertTypeAndPartieCnpjCpfConstraint(data);
		}
	}

	protected void assertTypeAndPartieCnpjCpfConstraint(JsonObject data) {
		String type = OIDFJSON.getString(data.get("type"));
		if("FOLHA_PAGAMENTO".equals(type) && data.get("partieCnpjCpf") == null) {
			throw error("data.partieCnpjCpf is mandatory when data.type is FOLHA_PAGAMENTO");
		}
	}

	@Override
	protected ExtraField getExtraField() {
		return new ExtraField.Builder()
			.setPattern("^([A-Z]{4})(-)(.*)$")
			.build();
	}
}
