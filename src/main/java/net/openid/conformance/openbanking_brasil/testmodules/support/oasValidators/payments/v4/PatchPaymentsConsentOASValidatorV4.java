package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public class PatchPaymentsConsentOASValidatorV4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected ResponseEnvKey getResponseEnvKey() {
		return ResponseEnvKey.FullConsentResponseEnvKey;
	}

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/payments/payments-4.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/pix/payments/consents/{consentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.PATCH;
	}


}
