package net.openid.conformance.openbanking_brasil.testmodules.phase2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareUrlForFetchingCreditOperationsContract;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareUrlForFetchingCreditOperationsContractGuarantees;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareUrlForFetchingCreditOperationsContractInstallments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareUrlForFetchingCreditOperationsContractPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.CreditOperationsContractSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractApiTestModulePhase2 extends AbstractPhase2TestModule {

	protected abstract OPFScopesEnum getScope();
	protected abstract Class<? extends Condition> apiResourceContractListResponseValidator();
	protected abstract Class<? extends Condition> apiResourceContractResponseValidator();
	protected abstract Class<? extends Condition> apiResourceContractGuaranteesResponseValidator();
	protected abstract Class<? extends Condition> apiResourceContractPaymentsResponseValidator();
	protected abstract Class<? extends Condition> apiResourceContractInstallmentsResponseValidator();
	protected abstract EnumResourcesType getApiType();

	@Override
	protected void configureClient() {
		super.configureClient();
		env.putString("api_type", getApiType().name());
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_OPERATIONS).addScopes(getScope(), OPFScopesEnum.OPEN_ID).build();

	}

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(apiResourceContractListResponseValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(CreditOperationsContractSelector.class);

		callAndStopOnFailure(PrepareUrlForFetchingCreditOperationsContract.class);
		preCallProtectedResource("Api Contract Response");
		callAndContinueOnFailure(apiResourceContractResponseValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(PrepareUrlForFetchingCreditOperationsContractGuarantees.class);
		preCallProtectedResource("Api Contract Guarantees Response");
		callAndContinueOnFailure(apiResourceContractGuaranteesResponseValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(PrepareUrlForFetchingCreditOperationsContractPayments.class);
		preCallProtectedResource("Api Contract Payments Response");
		callAndContinueOnFailure(apiResourceContractPaymentsResponseValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(PrepareUrlForFetchingCreditOperationsContractInstallments.class);
		preCallProtectedResource("Api Contract Instalments Response");
		callAndContinueOnFailure(apiResourceContractInstallmentsResponseValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
	}
}
