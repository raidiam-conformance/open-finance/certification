package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj.editRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditPaymentsRequestBodyToRemoveConsentId extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonElement dataElement = Optional.ofNullable(
			env.getElementFromObject("resource_request_entity_claims", "data")
		).orElseThrow(() -> error("Could not find data inside the body"));

		if (dataElement.isJsonArray()) {
			JsonArray data = dataElement.getAsJsonArray();
			for (JsonElement paymentElement : data) {
				JsonObject payment = paymentElement.getAsJsonObject();
				payment.remove("consentId");
			}
		} else if (dataElement.isJsonObject()) {
			JsonObject data = dataElement.getAsJsonObject();
			data.remove("consentId");
		} else {
			throw error("Payment is not of a valid type", args("payment data", dataElement));
		}

		logSuccess("Successfully edited request body to remove consentId field",
			args("request body", env.getObject("resource_request_entity_claims")));

		return env;
	}
}
