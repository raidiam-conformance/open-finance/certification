package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedFrom;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectedFromEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public abstract class AbstractEnsureRecurringPaymentsConsentsRejectedFrom extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			String rejectedFrom = OIDFJSON.getString(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.map(body -> body.getAsJsonObject().getAsJsonObject("data"))
					.map(data -> data.getAsJsonObject("rejection"))
					.map(rejection -> rejection.get("rejectedFrom"))
					.orElseThrow(() -> error("Unable to find element data.rejection.rejectedFrom in the response payload"))
			);
			if (rejectedFrom.equals(expectedRejectedFrom().toString())) {
				logSuccess("rejectedFrom returned in the response matches the expected rejectedFrom");
			} else {
				throw error("rejectedFrom returned in the response does not match the expected rejectedFrom",
					args("rejectedFrom", rejectedFrom,
						"expected rejectedFrom", expectedRejectedFrom().toString())
				);
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected abstract RecurringPaymentsConsentsRejectedFromEnum expectedRejectedFrom();
}
