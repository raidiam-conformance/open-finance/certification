package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

import java.util.List;

public class EnsureResourceResponseCodeWas204or422 extends AbstractEnsureResourceResponseCode {

	@Override
	protected List<HttpStatus> getExpectedStatus() {
		return List.of(HttpStatus.NO_CONTENT, HttpStatus.UNPROCESSABLE_ENTITY);
	}

}
