package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;

public class AddRiskSignalsAutomaticObjectToPixPaymentBody extends AbstractAddRiskSignalsObjectToPixPaymentBody {

	@Override
	protected JsonObject getRiskSignals() {
		JsonObject riskSignals = new JsonObject();
		JsonObject automatic = new JsonObject();

		automatic.addProperty("lastLoginDateTime", "2023-12-21T12:54:51Z");
		automatic.addProperty("pixKeyRegistrationDateTime", "2023-12-21T12:54:51Z");

		riskSignals.add("automatic", automatic);

		return riskSignals;
	}
}
