package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToBankFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo365DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo6DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractBankFixedIncomesApiOperationalLimitsTest extends AbstractInvestmentsApiOperationalLimitsTestModule {
	@Override
	protected OPFScopesEnum setScope() {
		return OPFScopesEnum.BANK_FIXED_INCOMES;
	}

	@Override
	protected AbstractSetInvestmentApi setInvestmentsApi() {
		return new SetInvestmentApiToBankFixedIncomes();
	}

	@Override
	protected Class<? extends Condition> setToTransactionToToday() {
		return SetToTransactionDateToToday.class;
	}

	@Override
	protected Class<? extends Condition> setFromTransactionTo365DaysAgo() {
		return SetFromTransactionDateTo365DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> setFromTransactionTo6DaysAgo() {
		return SetFromTransactionDateTo6DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
		return AddToAndFromTransactionDateParametersToProtectedResourceUrl.class;
	}
}
