package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePixScheduleDateIsInPast;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePixScheduleDateIsTooFarInFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsurePaymentDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsureScheduledPaymentDateIsToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsureScheduledPaymentDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.RemovePaymentDateFromConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDataPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilPaymentConsentProdValues;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;

import java.util.Optional;

public abstract class AbstractFvpPaymentsApiPixSchedulingDatesUnhappyTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilPaymentConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		eventLog.startBlock("[1] Create consent with request payload with both the date and the schedule.single.date fields");
		saveBrazilPaymentConsent();
		ConditionSequence consentCreationSeq1 = consentCreationConditionSequence();
		consentCreationSeq1.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
			sequenceOf(condition(EnsurePaymentDateIsTomorrow.class),
				condition(EnsureScheduledPaymentDateIsTomorrow.class)));

		call(consentCreationSeq1);
		validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		eventLog.endBlock();

		eventLog.startBlock("[2] Create consent with request payload set with schedule.single.date field defined as today");
		restoreBrazilPaymentConsent();
		ConditionSequence consentCreationSeq2 = consentCreationConditionSequence();
		consentCreationSeq2.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
			sequenceOf(condition(EnsureScheduledPaymentDateIsToday.class),
				condition(RemovePaymentDateFromConsentRequest.class)));

		call(consentCreationSeq2);
		validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		eventLog.endBlock();

		eventLog.startBlock("[3] Create consent with request payload set with schedule.single.date field defined as yesterday D-1");
		restoreBrazilPaymentConsent();
		ConditionSequence consentCreationSeq3 = consentCreationConditionSequence();
		consentCreationSeq3.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
			sequenceOf(condition(EnsurePixScheduleDateIsInPast.class),
				condition(RemovePaymentDateFromConsentRequest.class)));

		call(consentCreationSeq3);
		validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		eventLog.endBlock();

		eventLog.startBlock("[4] Create consent with request payload set with schedule.single.date field defined as yesterday D+740");
		restoreBrazilPaymentConsent();
		ConditionSequence consentCreationSeq4 = consentCreationConditionSequence();
		consentCreationSeq4.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
			sequenceOf(condition(EnsurePixScheduleDateIsTooFarInFuture.class),
				condition(RemovePaymentDateFromConsentRequest.class)));

		call(consentCreationSeq4);
		validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		eventLog.endBlock();

		eventLog.startBlock("[5] Create consent with request payload set without the date field provided");
		restoreBrazilPaymentConsent();
		ConditionSequence consentCreationSeq5 = consentCreationConditionSequence();
		consentCreationSeq5.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
			condition(RemovePaymentDateFromConsentRequest.class));

		call(consentCreationSeq5);
		validateResponse(EnsureErrorResponseCodeFieldWasParametroNaoInformado.class);
		eventLog.endBlock();
	}

	protected void saveBrazilPaymentConsent() {
		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.orElseThrow(() -> new TestFailureException(getId(),"Could not extract brazilPaymentConsent")).getAsJsonObject();

		env.putObject("resource", "brazilPaymentConsentSave", brazilPaymentConsent.deepCopy());
	}

	protected void restoreBrazilPaymentConsent() {
		JsonObject brazilPaymentConsentSave = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsentSave"))
			.orElseThrow(() -> new TestFailureException(getId(),"Could not extract brazilPaymentConsentSave")).getAsJsonObject();
		env.putObject("resource", "brazilPaymentConsent", brazilPaymentConsentSave.deepCopy());
	}

	@Override
	protected ConditionSequence getPaymentsConsentSequence() {
		return super.getPaymentsConsentSequence()
			.replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas422.class).onFail(Condition.ConditionResult.FAILURE));
	}

	private ConditionSequence consentCreationConditionSequence() {
		return sequenceOf(
			condition(PrepareToPostConsentRequest.class),
			condition(FAPIBrazilCreatePaymentConsentRequest.class),
			sequence(this::getPaymentsConsentSequence)
		);
	}

	private void validateResponse(Class<? extends Condition> ensureErrorResponse) {
		callAndContinueOnFailure(ensureErrorResponse, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
	}
}
