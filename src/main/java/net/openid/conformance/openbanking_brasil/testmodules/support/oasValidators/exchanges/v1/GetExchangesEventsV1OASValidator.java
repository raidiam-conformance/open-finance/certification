package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;

import java.util.Map;

public class GetExchangesEventsV1OASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/exchanges/exchanges-v1.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/operations/{operationId}/events";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonArray data = body.getAsJsonArray("data");
		data.forEach(el -> assertData(el.getAsJsonObject()));
	}

	private void assertData(JsonObject data){

		String deliveryForeignCurrency = data.get("deliveryForeignCurrency") != null ? OIDFJSON.getString(data.get("deliveryForeignCurrency")) : null;
		//[Restrição] Campo de preenchimento opcional pelas participantes quando o campo 'deliveryForeignCurrency ' for igual EM ESPÉCIE E/OU CHEQUES DE VIAGEM.
		boolean isOptional = deliveryForeignCurrency != null && (deliveryForeignCurrency.equals("ESPECIE_CHEQUES_VIAGEM")
			|| deliveryForeignCurrency.equals("CARTAO_PREPAGO")
			|| !OIDFJSON.getString(data.get("eventType")).equals("4"));

		if(!isOptional && !data.has("foreignPartie")){
			throw error("foreignPartie is required", Map.of("data", data));
		}
	}
}
