package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Map;
import java.util.Optional;

public class SaveKid extends AbstractCondition {

	@Override
	@PreEnvironment(required = "fido_keys_jwk")
	@PostEnvironment(strings = "saved_kid")
	public Environment evaluate(Environment env) {
		JsonObject fidoKeysJwk = env.getObject("fido_keys_jwk");
		String kid = Optional.ofNullable(fidoKeysJwk.get("kid"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find kid in the fido_keys_jwk", Map.of("key", fidoKeysJwk)));
		env.putString("saved_kid", kid);
		logSuccess("Current key id has been saved to the environment", args("kid", kid));
		return env;
	}
}
