package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessFinancialRelations;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessIdentifications;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractConsentIdFromResourceEndpointResponseFull;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetBusinessIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.TestFailureException;

public abstract class AbstractCustomerBusinessWrongPermissionsTestModule extends AbstractPermissionsCheckingFunctionalTestModule {

	@Override
	protected void configureClient(){
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetBusinessIdentificationsV2Endpoint.class)
		)));
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		//Simple UI fix
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.OPEN_ID).build();
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected void preFetchResources() {
		//Not needed for this test
	}

	@Override
	protected void prepareCorrectConsents() {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.BUSINESS_REGISTRATION_DATA).build();
		callAndStopOnFailure(PrepareToGetBusinessQualifications.class);
	}


	@Override
	protected void performPreAuthorizationSteps() {
		if (preFetchResources) {
			call(new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false)
				.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
					sequenceOf(condition(CreateRandomFAPIInteractionId.class),
						condition(AddFAPIInteractionIdToResourceEndpointRequest.class))));
			env.mapKey("endpoint_response", "consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureHttpStatusCodeIs201.class, Condition.ConditionResult.WARNING);

			if(getResult() == Result.WARNING){
				env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
				callAndStopOnFailure(EnsureResourceResponseCodeWas422.class);
				env.unmapKey("resource_endpoint_response_full");
				env.putString("warning_message", "Consent Creation returned a 422 on a POST Consents that included all" +
					" available permissions, excluding customer data. This scenario implies that the server does not support" +
					" any other resource but customer data APIs.");
				callAndContinueOnFailure(ChuckWarning.class, Condition.ConditionResult.WARNING);
				fireTestFinished();

				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
				}

			}else {
				callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE);
				env.unmapKey("endpoint_response");
				callAndContinueOnFailure(FAPIBrazilConsentEndpointResponseValidatePermissions.class, Condition.ConditionResult.INFO);
				callAndStopOnFailure(ExtractConsentIdFromResourceEndpointResponseFull.class);
				callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI-R-6.2.1-11", "FAPI1-BASE-6.2.1-11");
				callAndStopOnFailure(FAPIBrazilAddConsentIdToClientScope.class);
				eventLog.endBlock();
			}
		} else {
			super.performPreAuthorizationSteps();
		}
	}

	@Override
	protected void prepareIncorrectPermissions() {
			ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
			scopesAndPermissionsBuilder
				.addPermissionsBasedOnCategory(OPFCategoryEnum.ACCOUNTS)
				.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_CARDS_ACCOUNTS)
				.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_OPERATIONS)
				.addPermissionsBasedOnCategory(OPFCategoryEnum.INVESTMENTS)
				.addPermissionsBasedOnCategory(OPFCategoryEnum.EXCHANGES)
				.build();
	}

	@Override
	protected void requestResourcesWithIncorrectPermissions() {
		runInBlock("Ensure we cannot call the  Customer Business Qualification V2", () -> {
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the Customer Business Identifications V2", () -> {
			callAndStopOnFailure(PrepareToGetBusinessIdentifications.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the Customer Business Financial-relations V2", () -> {
			callAndStopOnFailure(PrepareToGetBusinessFinancialRelations.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});
	}
}
