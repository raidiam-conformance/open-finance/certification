package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiPaymentsPreAccountHolderValidationTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.GetEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostConsentsAuthoriseOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_payments-pre-account-holder-validation_test-module_v2-1",
	displayName = "enrollments_api_payments-pre-account-holder-validation_test-module_v2-1",
	summary = "Ensure payment without redirect cannot be executed if Enrollment status is \"AWAITING_ACCOUNT_HOLDER_VALIDATION\".\n" +
		"• Call the POST Enrollments Endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Call the POST Risk Signals endpoint sending the appropriate signals\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is AWAITING_ACCOUNT_HOLDER_VALIDATION\n" +
		"• Call POST consents with valid payload, sending the correct EnrollmentID\n" +
		"• Expects 201 - Validate response\n" +
		"• Call POST Consents Authorize With Mocked Payload on the FidoAssertion using the client_credentials token\n" +
		"• Expects 401 or 403\n" +
		"• Call the GET consents endpoint\n" +
		"• Expects 200 - Validate if status is AWAITING_AUTHORISATION",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class EnrollmentsApiPaymentsPreAccountHolderValidationTestModuleV2n1 extends AbstractEnrollmentsApiPaymentsPreAccountHolderValidationTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getConsentsValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postConsentsAuthoriseValidator() {
		return PostConsentsAuthoriseOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetPaymentConsentsV4Endpoint.class), condition(GetPaymentV4Endpoint.class), condition(GetEnrollmentsV2Endpoint.class));
	}

	@Override
	public void cleanup() {
		//not required
	}
}
