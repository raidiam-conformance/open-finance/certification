package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractTreasureTitlesTransactionsOASValidatorV1 extends OpenAPIJsonSchemaValidator {



	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/treasureTitles/treasure-titles-v1.0.0.yml";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		body.getAsJsonArray("data").forEach(el -> assertData(el.getAsJsonObject()));
	}


	private void assertData(JsonObject data){
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionTypeAdditionalInfo",
			"transactionType",
			"OUTROS"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"incomeTax",
			"type",
			"SAIDA"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"financialTransactionTax",
			"type",
			"SAIDA"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"remunerationTransactionRate",
			"type",
			"ENTRADA"
		);

	}
}
