package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.pixqrcode.PixQRCode;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class InjectQRCodeWithRealPhoneNumberIntoConfig extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {

		//Build the QRes
		PixQRCode qrCode = new PixQRCode();
		qrCode.useStandardConfig();
		JsonObject consentRequestPayment = Optional.ofNullable(
				env.getElementFromObject("resource", "brazilPaymentConsent.data.payment")
			).orElseThrow(() -> error("Unable to find resource.brazilPaymentConsent.data.payment"))
			.getAsJsonObject();

		String amount = Optional.ofNullable(consentRequestPayment.get("amount"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find amount in resource.brazilPaymentConsent.data.payment"));
		qrCode.setTransactionAmount(amount);

		//Add proxy as the phone number
		qrCode.setProxy(DictHomologKeys.PROXY_PHONE_NUMBER);

		JsonObject consentRequestPaymentDetails = Optional.ofNullable(
			consentRequestPayment.getAsJsonObject("details")
		).orElseThrow(() -> error("Unable to find details in resource.brazilPaymentConsent.data.payment"));
		consentRequestPaymentDetails.addProperty("qrCode", qrCode.toString());

		JsonElement paymentRequestDataElement = Optional.ofNullable(
			env.getElementFromObject("resource", "brazilPixPayment.data")
		).orElseThrow(() -> error("Unable to find resource.brazilPixPayment.data"));
		if (paymentRequestDataElement.isJsonObject()) {
			paymentRequestDataElement.getAsJsonObject().addProperty("qrCode", qrCode.toString());
		} else if (paymentRequestDataElement.isJsonArray()) {
			for (JsonElement paymentElement : paymentRequestDataElement.getAsJsonArray()) {
				paymentElement.getAsJsonObject().addProperty("qrCode", qrCode.toString());
			}
		} else {
			throw error("Invalid data type for payment request data", args("data", paymentRequestDataElement));
		}

		logSuccess("Added new qrCode to payment consent and payment initiation", args("qrCode", qrCode.toString()));

		return env;
	}
}
