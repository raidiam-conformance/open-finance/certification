package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

public enum TotalRecordsComparisonOperator {
	AT_LEAST, EQUAL
}
