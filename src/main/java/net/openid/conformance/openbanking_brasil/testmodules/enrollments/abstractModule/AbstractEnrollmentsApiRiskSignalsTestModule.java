package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.addNfcHeader.AddNfcHeaderAsTrue;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateCompleteRiskSignalsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditConsentsAuthoriseRequestBodyToAddCompleteRiskSignals;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasRisco;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostRiskSignals;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractEnrollmentsApiRiskSignalsTestModule extends AbstractEnrollmentsApiPaymentsCoreTestModule {

	@Override
	protected PostEnrollmentsResourceSteps createPostRiskSignalsSteps() {
		return new PostEnrollmentsResourceSteps(new PrepareToPostRiskSignals(),
			CreateCompleteRiskSignalsRequestBodyToRequestEntityClaims.class,
			true);
	}

	@Override
	protected ConditionSequence createConsentsAuthoriseSteps() {
		return super.createConsentsAuthoriseSteps()
			.insertAfter(AddIdempotencyKeyHeader.class, condition(AddNfcHeaderAsTrue.class));
	}

	@Override
	protected void postAndValidateConsentAuthorise() {
		runInBlock("Post consents authorise - Expects 204 or 422 RISCO", () -> {
			env.putBoolean("is_client_data_registration", false);
			callAndStopOnFailure(GenerateFidoClientData.class);
			call(createConsentsAuthoriseSteps()
				.insertBefore(SignEnrollmentsRequest.class, condition(EditConsentsAuthoriseRequestBodyToAddCompleteRiskSignals.class)));
			callAndContinueOnFailure(EnsureResourceResponseCodeWas204or422.class, Condition.ConditionResult.FAILURE);
			int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
			if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				callAndContinueOnFailure(postConsentsAuthoriseValidator(), Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasRisco.class, Condition.ConditionResult.FAILURE);
			}
		});
	}

	@Override
	protected void postAndValidatePayment() {}

	protected abstract Class<? extends Condition> postConsentsAuthoriseValidator();

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return null;
	}

	@Override
	protected void executeFurtherSteps() {}

	@Override
	protected void configureDictInfo() {}
}
