package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo;

import org.springframework.http.HttpMethod;

public class PrepareToPatchEnrollments extends AbstractPrepareToMakeACallToEnrollmentsEndpoint {

	@Override
	protected HttpMethod httpMethod() {
		return HttpMethod.PATCH;
	}

	@Override
	protected String endpoint() {
		return "";
	}

	@Override
	protected boolean expectsSelfLink() {
		return true;
	}
}
