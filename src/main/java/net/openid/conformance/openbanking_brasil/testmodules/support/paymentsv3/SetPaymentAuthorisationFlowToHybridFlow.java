package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

public class SetPaymentAuthorisationFlowToHybridFlow extends AbstractSetPaymentAuthorisationFlow {

	@Override
	protected String getAuthorisationFlowType() {
		return AuthorisationFlowEnumV3.HYBRID_FLOW.toString();
	}
}
