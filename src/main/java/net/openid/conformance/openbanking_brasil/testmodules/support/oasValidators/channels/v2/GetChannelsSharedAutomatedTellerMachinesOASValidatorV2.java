package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.channels.v2;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


/**
 * Api url: https://raw.githubusercontent.com/OpenBanking-Brasil/draft-openapi/main/swagger-apis/channels/2.0.1.yml
 * Api endpoint: /shared-automated-teller-machines
 * Api version: v2.0.1
 */
@ApiName("Get Shared Automated Teller Machines Channels V2")
public class GetChannelsSharedAutomatedTellerMachinesOASValidatorV2 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-channels-2.0.1.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/shared-automated-teller-machines";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
