package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiRecurringPaymentsHigherQuantityTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "payments_api_recurring-payments-higher-quantity_test-module_v4",
	displayName = "Ensure that consent for recurring payments can be carried out and that the payment is not successfully scheduled when its the quantity of payments sent is higher what is on the consent",
	summary = "Ensure that consent for recurring payments can be carried out and that the payment is not successfully scheduled when its the quantity of payments sent is higher what is on the consent\n" +
		"• Call the POST Consents endpoints with the schedule.daily.startDate field set as D+1, and schedule.daily.quantity as 5\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent \n" +
		"• Expects 200 - Validate if status is “AUTHORISED” and validate response \n" +
		"• Calls the POST Payments Endpoint with 6 Payments, using the apprpriate endtoendID for each of them, ensuring the day for the first payment is the next day 01 and not the startDate, and adding one more daily payment in sequence \n" +
		"• Expects 422 - PAGAMENTO_DIVERGENTE_CONSENTIMENTO  \n" +
		"• Call GET Consent \n" +
		"• Expects 200 - Validate if status is “CONSUMED” and validate response \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)

public class PaymentsApiRecurringPaymentsHigherQuantityTestModuleV4 extends AbstractPaymentsApiRecurringPaymentsHigherQuantityTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() { return GetPaymentsPixOASValidatorV4.class; }

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento();
	}
}
