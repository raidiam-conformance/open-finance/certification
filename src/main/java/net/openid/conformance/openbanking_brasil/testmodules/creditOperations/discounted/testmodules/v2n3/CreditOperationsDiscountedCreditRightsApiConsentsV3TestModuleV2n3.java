package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractApiTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvoiceFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsIdentificationV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsListV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsPaymentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsScheduledInstalmentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsWarrantiesV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "invoice-financings_api_core_consents_v3-2_test-module_v2-3",
	displayName = "Validate structure of all discounted credit rights API resources V2 with consents V3-2",
	summary = "Validates the structure of all discounted credit rights API resources V2 with consents V3-2\n" +
		"• Creates a consent with all the permissions needed to access the Credit Operations API  (\"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"• Calls GET Discounted Credit Rights  Contracts API V2\n" +
		"• Expects 200 - Fetches one of the IDs returned\n" +
		"• Calls GET Discounted Credit Rights  Contracts API with ID V2\n" +
		"• Expects 200\n" +
		"• Calls GET Discounted Credit Rights  Warranties API V2\n" +
		"• Expects 200\n" +
		"• Calls GET Discounted Credit Rights  Payments API V2\n" +
		"• Expects 200\n" +
		"• Calls GET Discounted Credit Rights  Contracts Instalments API V2\n" +
		"• Expects 200\n" +
		"9",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class CreditOperationsDiscountedCreditRightsApiConsentsV3TestModuleV2n3 extends AbstractApiTestModulePhase2 {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class), condition(GetInvoiceFinancingsV2Endpoint.class));
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.INVOICE_FINANCINGS;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractListResponseValidator() {
		return GetInvoiceFinancingsListV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractResponseValidator() {
		return GetInvoiceFinancingsIdentificationV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractGuaranteesResponseValidator() {
		return GetInvoiceFinancingsWarrantiesV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractPaymentsResponseValidator() {
		return GetInvoiceFinancingsPaymentsV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractInstallmentsResponseValidator() {
		return GetInvoiceFinancingsScheduledInstalmentsV2n3OASValidator.class;
	}

	@Override
	protected EnumResourcesType getApiType() {
		return EnumResourcesType.INVOICE_FINANCING;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
