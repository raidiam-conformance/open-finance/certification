package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.GetConsentExtensionsOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.GetConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentExtendsOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractConsentApiExtensionPastExpirationDateTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_extension-past-expiration_test-module_v3",
	displayName = "consents_api_extension-past-expiration_test-module_v3",
	summary = "\"Ensure a consent extension cannot be done with a past date\n" +
		"\n" +
		"\u2022 Call the POST Consents Endpoint with expirationDateTime as current time + 5 minutes\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Redirect the user to authorize the consent\n" +
		"\u2022 Call the POST Token Endpoint Using the Authorization Code Grant\n" +
		"\u2022  Expects 201 - Validate if the refresh token sent back is not JWT\n" +
		"\u2022 Call the GET Consents\n" +
		"\u2022 Expects 200 - Validate Response and check if the status is AUTHORISED\n" +
		"\u2022 Call the POST Extends Endpoint with expirationDateTime as  D-365, \n" +
		"\u2022 Expects 422  DATA_EXPIRACAO_INVALIDA - Validate Error Message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl","consent.productType"
})
public class ConsentApiExtensionPastExpirationDateTestModuleV3 extends AbstractConsentApiExtensionPastExpirationDateTestModule {

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetConsentOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> setConsentCreationValidation() {
		return PostConsentOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentExtensionValidator() {
		return PostConsentExtendsOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionValidator() {
		return GetConsentExtensionsOASValidatorV3.class;
	}

}
