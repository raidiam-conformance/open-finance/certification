package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforceAbsenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.EnsurePaymentsTemporizationCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.InsertTemporizationTestValuesIntoConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasPndg;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjctNIOrAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusPdngV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;

public abstract class AbstractPaymentsApiTemporizationConditionalTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

    private boolean IS_SECOND_POLL = false;

    @Override
    protected void setupResourceEndpoint() {
        super.setupResourceEndpoint();
        env.putBoolean("continue_test", true);
        callAndContinueOnFailure(EnsurePaymentsTemporizationCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
        callAndStopOnFailure(InsertTemporizationTestValuesIntoConsentRequest.class);
        if (!env.getBoolean("continue_test")) {
            fireTestSkipped("Test skipped since no Temporization CPF/CNPJ was informed.");
        }
    }

    @Override
    protected  void configureDictInfo(){
        callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
    }

    @Override
    protected void requestProtectedResource() {
        eventLog.startBlock("Calling POST payments, expecting PNDG or RCVD, Polling GET Payments till PDNG status");
        call(getPixPaymentSequence());
        validateResponse();
        eventLog.endBlock();

        eventLog.startBlock("Polling GET payments while pending. Expects Rejected - Nao Informado or Accepted");
        repeatSequence(this::getRepeatSequence)
            .untilTrue("payment_proxy_check_for_reject")
            .trailingPause(30)
            .times(5)
            .validationSequence(this::getPaymentValidationSequence)
            .onTimeout(sequenceOf(
                condition(TestTimedOut.class),
                condition(ChuckWarning.class)))
            .run();

        callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
        validateFinalState();
        eventLog.endBlock();
    }

    @Override
    protected void validateFinalState(){
        if(IS_SECOND_POLL){
            callAndStopOnFailure(EnsurePaymentStatusWasRjctNIOrAcsc.class);
        } else {
            IS_SECOND_POLL = true;
            callAndStopOnFailure(EnsurePaymentStatusWasPndg.class);
        }
    }

    @Override
    protected Class<? extends Condition> getPaymentPollStatusCondition() {
        if(IS_SECOND_POLL){
            return CheckPaymentPollStatusPdngV2.class;
        }
        return CheckPaymentPollStatusRcvdOrAccpOrAcpdV2.class;
    }
}
