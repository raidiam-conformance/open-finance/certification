package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.AbstractGetLoansPaymentsOASValidator;


public class GetLoansPaymentsV2n4OASValidator extends AbstractGetLoansPaymentsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/loans/loans-v2.4.0.yml";
	}
}
