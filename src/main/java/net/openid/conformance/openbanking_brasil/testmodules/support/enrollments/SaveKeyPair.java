package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SaveKeyPair extends AbstractCondition {

	@Override
	@PreEnvironment(required = "fido_keys_jwk")
	@PostEnvironment(required = "saved_fido_keys_jwk")
	public Environment evaluate(Environment env) {
		JsonObject fidoKeysJwk = env.getObject("fido_keys_jwk");
		env.putObject("saved_fido_keys_jwk", fidoKeysJwk.deepCopy());
		logSuccess("Current key pair has been saved to the environment");
		return env;
	}
}
