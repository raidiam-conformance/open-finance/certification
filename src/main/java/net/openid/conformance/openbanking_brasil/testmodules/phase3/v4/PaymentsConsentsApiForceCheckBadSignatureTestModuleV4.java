package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsentsApiForceCheckBadSignatureTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_force-check-signature_test-module_v4",
	displayName = "Payments Consents API basic test module ",
	summary = "Ensure error when sending consent endpoint request with a bad signature\n" +
		"• Calls POST Consents Endpoint random times with valid signature\n" +
		"• Expects 201 - Validate Response\n" +
		"• Calls POST Consents Endpoint with badly signed request\n" +
		"• Expects 400 - Validate error message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiForceCheckBadSignatureTestModuleV4 extends AbstractPaymentsConsentsApiForceCheckBadSignatureTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}
}
