package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringConsentIdToQuery;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsManualObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreTwoAcscPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToRemoveExpirationDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToRemoveIbgeTownCode;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.GenerateNewE2EID;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.AddMapFromPaymentIdToEndToEndId;

public abstract class AbstractAutomaticPaymentsApiSweepingAccountsCoreTestModule extends AbstractAutomaticPaymentsFunctionalTestModule {

	protected abstract Class<? extends Condition> getPaymentsByConsentIdValidator();

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToRemoveExpirationDateTime.class);
	}

	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(AddRiskSignalsManualObjectToPixPaymentBody.class);
		super.requestProtectedResource();
		callAndStopOnFailure(AddMapFromPaymentIdToEndToEndId.class);
		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(GenerateNewE2EID.class);
		runInBlock("Call pix/payments endpoint again", () -> call(getPixPaymentSequence()));
		callAndStopOnFailure(AddMapFromPaymentIdToEndToEndId.class);
		runInBlock("Validate response", this::validateResponse);
		fetchConsentToCheckStatus("CONSUMED", new EnsurePaymentConsentStatusWasConsumed());
		runInBlock("GET pix/payments endpoint to check if all payments are there", this::fetchPaymentsEndpoint);
	}

	protected void fetchPaymentsEndpoint() {
		callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
		callAndStopOnFailure(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class);
		callAndStopOnFailure(SetResourceMethodToGet.class);
		callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
		callAndStopOnFailure(AddRecurringConsentIdToQuery.class);
		callAndStopOnFailure(CallProtectedResource.class);
		callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
		callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
		callAndContinueOnFailure(getPaymentsByConsentIdValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureThereAreTwoAcscPayments.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void startPixPaymentsBlock() {
		super.startPixPaymentsBlock();
		callAndStopOnFailure(EditRecurringPaymentsBodyToRemoveIbgeTownCode.class);
	}
}
