package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckAllTransactionsDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckTransactionDateRange;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToBankFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromAndToTransactionDateToSavedTransactionDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo180DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo360DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateTo180DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "bank-fixed-incomes_api_transactiondate_test-module",
    displayName = "bank-fixed-incomes_api_transactiondate_test-module",
    summary = "This test will confirm that the institution has correctly implemented the queryParameters fromTransactionDate and toTransactionDate for the Bank Fixed Incomes API\n" +
        "• Call the POST Consents endpoint with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
        "• Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Consents endpoint\n" +
        "• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
        "• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
        "• Call the GET Investments List Endpoint\n" +
        "• Expect a 200 Response - Extract one InvestmentId - Validate the Response_BODY\n" +
        "• Call the GET Investments Transactions Endpoint with the Extracted investmentId - Don't send any query parameters\n" +
        "• Expect a 200 - Make sure if any transactions are returned, that they are from the current date.\n" +
        "• Call the GET Investments Transactions Endpoint - Send query Params fromTransactionDate=D-360 and toTransactionDate=D+0\n" +
        "• Expect a 200 - Make sure at least one transaction is returned, get the transactionDate of one transaction, make sure this transaction is within the range above\n" +
        "• Call GET Investments Transactions API - Send query Params fromTransactionDate=D-180 and toTransactionDate=D+0\n" +
        "• Expect a 200 - Make sure at least one transaction is returned, get the transactionDate of one transaction, make sure this transaction is within the range above\n" +
        "• Call GET Investments Transactions API - Send query Params fromTransactionDate=D-360 and toTransactionDate=D-180\n" +
        "• Expect a 200 - Make sure at least one transaction is returned, get the transactionDate of one transaction, make sure this transaction is within the range above - Extract the date from one of the transactions returned on that API Call \n" +
        "• Call GET Investments Transactions API, send query parameters fromTransactionDate and toTransactionDate to be the transactionDate extracted on the step above\n" +
        "• Expect 200, make sure that the returned transactions is from exactly the date returned above and that no transactions from other dates were returned\n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "directory.discoveryUrl",
        "resource.brazilCpf"
    }
)
public abstract class AbstractBankFixedIncomesApiTransactionDateTest extends AbstractInvestmentsApiTransactionDateTestModule {

    @Override
    protected AbstractSetInvestmentApi setInvestmentsApi() {
        return new SetInvestmentApiToBankFixedIncomes();
    }

    @Override
    protected Class<? extends Condition> setFromTransactionTo180DaysAgo() {
        return SetFromTransactionDateTo180DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> setToTransactionToToday() {
        return SetToTransactionDateToToday.class;
    }

    @Override
    protected Class<? extends Condition> setFromTransactionTo360DaysAgo() {
        return SetFromTransactionDateTo360DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> setToTransactionTo180DaysAgo() {
        return SetToTransactionDateTo180DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
        return AddToAndFromTransactionDateParametersToProtectedResourceUrl.class;
    }

    @Override
    protected Class<? extends Condition> setFromAndToTransactionToSavedTransaction() {
        return SetFromAndToTransactionDateToSavedTransactionDate.class;
    }

    @Override
    protected Class<? extends Condition> checkTransactionRange() {
        return CheckTransactionDateRange.class;
    }

    @Override
    protected Class<? extends Condition> checkAllDates() {
        return CheckAllTransactionsDates.class;
    }

    @Override
    protected String transactionParameterName() {
        return "TransactionDate";
    }

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.BANK_FIXED_INCOMES;
	}
}
