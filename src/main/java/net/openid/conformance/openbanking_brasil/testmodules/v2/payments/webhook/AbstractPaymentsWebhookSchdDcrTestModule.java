package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractPaymentId;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasCanc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs.EnsureScheduledDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPatchPixPaymentsEndpointSequenceV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.sequence.ConditionSequence;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;

public abstract class AbstractPaymentsWebhookSchdDcrTestModule extends AbstractBrazilDCRPaymentsWebhook{
	@Override
	public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
		Boolean paymentWebhookReceived = env.getBoolean("second_payment_webhook_received");
		 if (paymentWebhookReceived != null && (path.matches("^(open-banking\\/webhook\\/v\\d+\\/payments\\/v\\d+\\/pix\\/payments\\/)([a-zA-Z0-9][a-zA-Z0-9\\-]{0,99})$") && paymentWebhookReceived.booleanValue())) {
			 addWebhookToObjectInEnv(env, false, true, "webhooks_received_payment", requestParts, path);
			 return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else {
			return super.handleHttpMtls(path, req, res, session, requestParts);
		}
	}
	@Override
	protected void validatePaymentStatus(){
		Boolean paymentWebhookReceived = env.getBoolean("second_payment_webhook_received");
		if (paymentWebhookReceived != null && paymentWebhookReceived.booleanValue()) {
			callAndContinueOnFailure(EnsurePaymentStatusWasCanc.class, Condition.ConditionResult.FAILURE);
		} else {
			callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class, Condition.ConditionResult.FAILURE);
		}
	}
	@Override
	protected void updatePaymentConsent() {
	}
	@Override
	protected void setScheduledPaymentDateTime() {
		callAndStopOnFailure(RemovePaymentConsentDate.class);
		callAndStopOnFailure(EnsureScheduledDateIsTomorrow.class);
	}

	@Override
	protected void requestProtectedResource() {
		enablePaymentWebhook();
		createPaymentWebhook();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		ConditionSequence pixSequence = new CallPixPaymentsEndpointSequence();
		call(pixSequence);
		eventLog.endBlock();
		eventLog.startBlock(currentClientString() + "Validate response");
		validatePaymentResponse(true);
		callAndStopOnFailure(ExtractPaymentId.class);
		eventLog.endBlock();
		waitForWebhookResponse();
		validatePaymentConsentWebhooks();
		validatePaymentWebhooks();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		performClientCredentialsGrant();
		call(getPayment());
		eventLog.endBlock();
		eventLog.startBlock(currentClientString() + "Validate response");
		validatePaymentResponse(false);
		validatePaymentStatus();
		eventLog.endBlock();
		env.putString("time_of_payment_request", Instant.now().toString());
		env.putBoolean("second_payment_webhook_received", true);
		callAndStopOnFailure(CreatePatchPaymentsRequestFromConsentRequestV2.class);
		call(new CallPatchPixPaymentsEndpointSequenceV2());
		callAndStopOnFailure(setPatchPaymentValidator());
		waitForWebhookResponse();
		callAndStopOnFailure(ValidatePaymentWebhooks.class);
		env.putBoolean("second_payment_webhook_received", false);
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		performClientCredentialsGrant();
		call(getPayment());
		eventLog.endBlock();
		validatePaymentResponse(false);
	}
	protected abstract Class<? extends Condition> setPatchPaymentValidator();
}
