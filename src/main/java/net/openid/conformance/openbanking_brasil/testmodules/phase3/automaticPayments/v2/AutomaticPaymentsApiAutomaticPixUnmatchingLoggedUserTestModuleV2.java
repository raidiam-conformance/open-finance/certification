package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiAutomaticPixUnmatchingLoggedUserTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_automatic-pix-unmatching-loggedUser_test-module_v2",
	displayName = "automatic-payments_api_automatic-pix-unmatching-loggedUser_test-module_v2",
	summary = "Ensure the consent is REJECTED if the authorisation is performed by a LoggedUser that differs from the one provided in consent creation. For this test, the tester is expected to log in using information belonging to a different cpf during redirection.\n" +
		"\n" +
		"• Call the POST recurring-consents endpoint with automatic pix fields, with startDateTime as D+1, period as SEMANAL, minimumVariableAmount of 0.5 BRL, maximumVariableAmount as 0.8 BRL and sending firstPayment information with amount as 1 BRL, date as D+0, and loggedUser as the one informed in the config.\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirects the user to authorize the created consent - Expect Failure\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is AUTENTICACAO_DIVERGENTE",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.contractDebtorName",
		"resource.contractDebtorIdentification",
		"resource.creditorAccountIspb",
		"resource.creditorAccountIssuer",
		"resource.creditorAccountNumber",
		"resource.creditorAccountAccountType",
		"resource.creditorName",
		"resource.creditorCpfCnpj"
	}
)
public class AutomaticPaymentsApiAutomaticPixUnmatchingLoggedUserTestModuleV2 extends AbstractAutomaticPaymentsApiAutomaticPixUnmatchingLoggedUserTestModule {

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class), condition(GetAutomaticPaymentPixRecurringV2Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}
}
