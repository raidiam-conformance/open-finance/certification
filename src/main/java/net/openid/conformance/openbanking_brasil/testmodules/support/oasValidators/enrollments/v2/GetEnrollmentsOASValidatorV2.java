package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;

import java.util.List;

public class GetEnrollmentsOASValidatorV2 extends AbstractEnrollmentsOASValidatorV2 {

	@Override
	protected String getEndpointPathSuffix() {
		return "/{enrollmentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertDebtorAccountIssuerConstraint(data);
		assertCreationAndStatusUpdateDateTime(data);
		assertDebtorAccountConstraint(data);
		assertTransactionAndDailyLimitsConstraints(data);
		assertTransactionLimitValueConstraint(data);
	}

	protected void assertDebtorAccountConstraint(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"debtorAccount",
			"status",
			List.of("AWAITING_ENROLLMENT", "AUTHORISED", "REVOKED")
		);
	}

	protected void assertTransactionAndDailyLimitsConstraints(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionLimit",
			"status",
			List.of("AWAITING_ENROLLMENT", "AUTHORISED")
		);
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"dailyLimit",
			"status",
			List.of("AWAITING_ENROLLMENT", "AUTHORISED")
		);
	}

	protected void assertTransactionLimitValueConstraint(JsonObject data) {
		if (!data.has("businessEntity") && data.has("transactionLimit")) {
			String transactionLimit = OIDFJSON.getString(data.get("transactionLimit"));
			if (Double.parseDouble(transactionLimit) > 500) {
				throw error("If businessEntity is not in the payload, transactionLimit is limited to 500 BRL",
					args("transactionLimit", transactionLimit));
			}
		}
	}
}
