package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class SetIncorrectCurrencyPayment extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");
		JsonObject consentRequest = resource.getAsJsonObject("brazilPaymentConsent");
		JsonObject paymentRequest = resource.getAsJsonObject("brazilPixPayment");

		JsonElement consentCurrency = Optional.ofNullable(consentRequest
			.getAsJsonObject("data")
			.getAsJsonObject("payment")
			.get("currency")).orElseThrow(() -> error("Consent does not have an associated currency"));

		String wrongCurrency = OIDFJSON.getString(consentCurrency).equalsIgnoreCase("BRL") ? "ZAR" : "BRL";

		JsonElement paymentData = paymentRequest.get("data");
		if (paymentData.isJsonArray()) {
			if (!paymentData.getAsJsonArray().isEmpty()) {
				env.putString(
					"previous_currency",
					OIDFJSON.getString(
						paymentRequest.getAsJsonArray("data").get(0).getAsJsonObject()
							.getAsJsonObject("payment").get("currency")
					)
				);
			}
			log(env.getString("previous_currency"));
			for (int i = 0; i < paymentData.getAsJsonArray().size(); i++) {
				JsonObject payment = paymentData.getAsJsonArray().get(i).getAsJsonObject();
				payment.getAsJsonObject("payment").addProperty("currency", wrongCurrency);
			}

			logSuccess("Successfully set the currency type of the payment request to differ from the consent request", paymentRequest);
			return env;
		}

		env.putString(
			"previous_currency",
			OIDFJSON.getString(
				paymentRequest.getAsJsonObject("data").getAsJsonObject("payment").get("currency")
			)
		);
		log(env.getString("previous_currency"));

		paymentRequest
			.getAsJsonObject("data")
			.getAsJsonObject("payment")
			.addProperty("currency", wrongCurrency);

		logSuccess("Successfully set the currency type of the payment request to differ from the consent request", paymentRequest);
		return env;
	}
}
