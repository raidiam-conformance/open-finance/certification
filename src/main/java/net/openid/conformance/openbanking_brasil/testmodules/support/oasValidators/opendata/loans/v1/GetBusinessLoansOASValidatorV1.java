package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.loans.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetBusinessLoansOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-opendata-loans-v1.0.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/business-loans";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
