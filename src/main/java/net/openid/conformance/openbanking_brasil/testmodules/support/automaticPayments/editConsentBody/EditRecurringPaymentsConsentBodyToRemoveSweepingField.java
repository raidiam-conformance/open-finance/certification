package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditRecurringPaymentsConsentBodyToRemoveSweepingField extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject recurringConfiguration = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.map(consentElement -> consentElement.getAsJsonObject().getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("recurringConfiguration"))
			.orElseThrow(() -> error("Unable to find recurringConfiguration field in consents payload"));

		recurringConfiguration.remove("sweeping");
		logSuccess("sweeping field has been removed from recurringConfiguration",
			args("recurringConfiguration", recurringConfiguration));

		return env;
	}
}
