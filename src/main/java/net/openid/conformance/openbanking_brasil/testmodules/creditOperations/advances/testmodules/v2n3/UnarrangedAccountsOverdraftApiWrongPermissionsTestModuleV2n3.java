package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.AbstractUnarrangedAccountsOverdraftApiWrongPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3.GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3.GetUnarrangedAccountsOverdraftListV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3.GetUnarrangedAccountsOverdraftPaymentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3.GetUnarrangedAccountsOverdraftScheduledInstalmentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3.GetUnarrangedAccountsOverdraftWarrantiesV2n3OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "unarranged-accounts-overdraft_api_wrong-permissions_test-module_V2-3",
	displayName = "Ensures API resource cannot be called with wrong permissions",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"• Creates a consent with all the permissions needed to access the Credit Operations API  (\"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"• Calls GET Unarranged Overdraft Contracts API V2-3\n" +
		"• Expects 200 - Fetches one of the IDs returned\n" +
		"• Calls GET Unarranged Overdraft Contracts API with ID V2-3\n" +
		"• Expects 200\n" +
		"• Calls GET Unarranged Overdraft Warranties API V2-3\n" +
		"• Expects 200\n" +
		"• Calls GET Unarranged Overdraft Payments API V2-3\n" +
		"• Expects 200\n" +
		"• Calls GET Unarranged Overdraft Contracts Instalments API V2-3\n" +
		"• Expects 200\n" +
		"• Creates a consent with all the permissions needed to access the Customer Personal or the Customer Business API (\"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\", \"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\",  \"RESOURCES_READ\")or (“CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ”,” \"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ”, \"RESOURCES_READ\")\n" +
		"• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"• Calls GET Unarranged Overdraft Contracts API V2-3\n" +
		"• Expects 403\n" +
		"• Calls GET Unarranged Overdraft Contracts API with ID V2-3\n" +
		"• Expects 403\n" +
		"• Calls GET Unarranged Overdraft Warranties API V2-3\n" +
		"• Expects 403\n" +
		"• Calls GET Unarranged Overdraft Payments API V2-3\n" +
		"• Expects 403\n" +
		"• Calls GET Unarranged Overdraft Contracts Instalments API V2-3\n" +
		"• Expects 403",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class UnarrangedAccountsOverdraftApiWrongPermissionsTestModuleV2n3 extends AbstractUnarrangedAccountsOverdraftApiWrongPermissionsTestModule {

	@Override
	protected Class<? extends Condition> apiResourceContractListResponseValidator() {
		return GetUnarrangedAccountsOverdraftListV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractResponseValidator() {
		return GetUnarrangedAccountsOverdraftIdentificationV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractGuaranteesResponseValidator() {
		return GetUnarrangedAccountsOverdraftWarrantiesV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractPaymentsResponseValidator() {
		return GetUnarrangedAccountsOverdraftPaymentsV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractInstallmentsResponseValidator() {
		return GetUnarrangedAccountsOverdraftScheduledInstalmentsV2n3OASValidator.class;
	}
}
