package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

public class AddAutomaticPaymentToTheResourceOpenFvp extends AbstractCondition {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmm");

	@Override
	@PostEnvironment(strings = {"endToEndId", "ispb"})
	public Environment evaluate(Environment env) {
		JsonObject resource = Optional.ofNullable(env.getObject("resource"))
			.orElseGet(() -> Optional.ofNullable(env.getElementFromObject("config", "resource"))
				.orElseThrow(() -> error("Could not find resource object"))
				.getAsJsonObject());

		JsonObject consentData = Optional.ofNullable(resource.getAsJsonObject("brazilPaymentConsent"))
			.map(brazilPaymentConsent -> brazilPaymentConsent.getAsJsonObject("data"))
			.orElseThrow(() -> error("Could not extract the consent data from the resource"));

		String rel;
		String consentDataField;
		if (consentData.has("businessEntity")) {
			rel = "CNPJ";
			consentDataField = "businessEntity";
		} else {
			rel = "CPF";
			consentDataField = "loggedUser";
		}
		String identification = Optional.ofNullable(consentData.getAsJsonObject(consentDataField))
			.map(businessEntity -> businessEntity.getAsJsonObject("document"))
			.map(document -> document.get("identification"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find identification field inside " + consentDataField));

		String endToEndId = generateEndToEndId(env);
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

		String proxy = Optional.ofNullable(resource.get("creditorProxy"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find creditorProxy in the resource", args("resource", resource)));

		JsonObject payment = new JsonObjectBuilder()
			.addFields("data",
				Map.of(
					"endToEndId", endToEndId,
					"date", currentDate.toString(),
					"remittanceInformation", DictHomologKeys.PROXY_EMAIL_STANDARD_REMITTANCEINFORMATION,
					"cnpjInitiator", DictHomologKeys.PROXY_CNPJ_INITIATOR,
					"ibgeTownCode", DictHomologKeys.PROXY_EMAIL_STANDARD_IBGETOWNCODE,
					"localInstrument", DictHomologKeys.PROXY_EMAIL_STANDARD_LOCALINSTRUMENT,
					"proxy", proxy
				)
			)
			.addFields("data.payment",
				Map.of(
					"amount", "100.00",
					"currency", "BRL"
				)
			)
			.addFields("data.document",
				Map.of(
					"identification", identification,
					"rel", rel
				)
			)
			.addFields("data.creditorAccount",
				Map.of(
					"ispb", getCreditorAccountFieldFromResource(resource, "Ispb"),
					"issuer", getCreditorAccountFieldFromResource(resource, "Issuer"),
					"number", getCreditorAccountFieldFromResource(resource, "Number"),
					"accountType", getCreditorAccountFieldFromResource(resource, "AccountType")
				)
			)
			.build();

		env.putObject("resource", "brazilPixPayment", payment);
		logSuccess("Hardcoded brazilPixPayment object was added to the resource", payment);

		return env;
	}

	protected String generateEndToEndId(Environment env) {
		String ispb = DictHomologKeys.PROXY_CNPJ_INITIATOR.substring(0, 8);
		String randomString = RandomStringUtils.randomAlphanumeric(11);
		OffsetDateTime currentDateTime = OffsetDateTime.now(ZoneOffset.UTC);
		String formattedDateTime = currentDateTime.format(FORMATTER);
		String endToEndId = String.format("E%s%s%s", ispb, formattedDateTime, randomString);
		env.putString("endToEndId", endToEndId);
		env.putString("ispb", ispb);
		return endToEndId;
	}

	protected String getCreditorAccountFieldFromResource(JsonObject resource, String fieldName) {
		return Optional.ofNullable(resource.get("creditorAccount" + fieldName))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error(String.format("Could not find creditorAccount %s in the resource", fieldName), args("resource", resource)));
	}
}
