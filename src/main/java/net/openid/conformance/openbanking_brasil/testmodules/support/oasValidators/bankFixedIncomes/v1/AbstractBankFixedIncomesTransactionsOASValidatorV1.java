package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.AbstractBankFixedIncomesTransactionsOASValidator;

public abstract class AbstractBankFixedIncomesTransactionsOASValidatorV1 extends AbstractBankFixedIncomesTransactionsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/bank-fixed-incomes/bank-fixed-incomes-v1.0.0.yml";
	}

}
