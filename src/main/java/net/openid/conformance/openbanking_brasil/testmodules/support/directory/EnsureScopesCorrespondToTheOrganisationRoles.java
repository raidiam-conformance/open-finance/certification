package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.extensions.SpringContext;
import net.openid.conformance.extensions.yacs.ParticipantsService;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class EnsureScopesCorrespondToTheOrganisationRoles extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		ParticipantsService participantsService = getParticipantsService();
		String organisationId = getStringFromEnvironment(env, "config", "resource.brazilOrganizationId", "Config brazilOrganizationId Field");
		organisationId = organisationId.replaceAll("-","_");
		Set<Map> orgs = participantsService.orgsFor(Set.of(organisationId));
		logSuccess("Found organisation", args("org", orgs));

		List<String> roles = new ArrayList<>();
		for (Map org : orgs) {
			List<Map> orgDomainRoleClaims = (List<Map>) org.get("OrgDomainRoleClaims");
			for (Map map : orgDomainRoleClaims) {
				String role = (String) map.get("Role");
				roles.add(role);
			}
		}

		log("All the organisation ROLEs have been extracted from the directory",
			args("roles", roles));

		JsonArray scopesSupported = Optional.ofNullable(
			env.getElementFromObject("server", "scopes_supported")
		).orElseThrow(() -> error("scopes_supported field missing from server response")).getAsJsonArray();

		log("All the scopes supported have been extracted from the well-known",
			args("scopes_supported", scopesSupported));

		Map<String, List<String>> roleToScopesMap = getRoleToScopesMap();

		for (JsonElement element : scopesSupported) {
			String scope = OIDFJSON.getString(element);
			String role = getRoleRelatedToScope(roleToScopesMap, scope);
			if (role != null && !roles.contains(role)) {
				throw error("The organisation does not have the necessary ROLE for at least one of its scopes",
					args("scope", scope, "associated role", role, "roles", roles));
			}
		}

		logSuccess("All scopes are covered by the organisation ROLEs");

		return env;
	}

	private ParticipantsService getParticipantsService() {
		return SpringContext.getBean(ParticipantsService.class);
	}

	Map<String, List<String>> getRoleToScopesMap() {
		Map<String, List<String>> roleToScopesMap = new HashMap<>();
		roleToScopesMap.put("DADOS", new ArrayList<>(List.of(
			"accounts",
			"credit-cards-accounts",
			"consents",
			"customers",
			"invoice-financings",
			"financings",
			"loans",
			"unarranged-accounts-overdraft",
			"resources",
			"credit-fixed-incomes",
			"exchanges"
		)));
		roleToScopesMap.put("CONTA", new ArrayList<>(List.of("payments")));
		return roleToScopesMap;
	}

	String getRoleRelatedToScope(Map<String, List<String>> roleToScopesMap, String scope) {
		for (Map.Entry<String, List<String>> entry : roleToScopesMap.entrySet()) {
			if (entry.getValue().contains(scope)) {
				return entry.getKey();
			}
		}
		return null;
	}
}
