package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import com.google.gson.JsonObject;
import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentAspspRevoked;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetBusinessIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPersonalIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v2.GetConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v2.PostConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.consent.v2.OpenBankingBrazilPreAuthorizationConsentApiV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;

import java.util.Objects;

public abstract class AbstractConsentsApiRevokedAspspTestModule extends AbstractPhase2TestModule {

	protected abstract Class<? extends Condition> setPostConsentValidator();

	@Override
	protected void onConfigure(JsonObject config, String baseUrl){
		super.onConfigure(config,baseUrl);
	}

	@Override
	protected void validateClientConfiguration() {
		super.validateClientConfiguration();
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS, OPFScopesEnum.OPEN_ID).build();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		env.putString("proceed_with_test", "true");
		return new OpenBankingBrazilPreAuthorizationConsentApiV2(addTokenEndpointClientAuthentication,false)
			.replace(PostConsentOASValidatorV2.class, condition(setPostConsentValidator()))
			.replace(GetConsentOASValidatorV2.class, condition(getPostConsentValidator()));
	}

	@Override
	protected void requestProtectedResource(){
		call(createGetAccessTokenWithClientCredentialsSequence(addTokenEndpointClientAuthentication));

		ConditionSequenceRepeater repeatSequence = repeatSequence(() -> getPreConsentWithBearerTokenSequence()
			.then(getValidateConsentResponsePollingSequence()))
			.untilTrue("code_returned")
			.refreshSequence(() -> createGetAccessTokenWithClientCredentialsSequence(addTokenEndpointClientAuthentication), 3)
			.times(10)
			.trailingPause(60)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)));

		repeatSequence.run();
	}

	@Override
	protected void validateResponse() {}

	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainAccessTokenWithClientCredentials(clientAuthSequence);
	}

	protected ConditionSequence getValidateConsentResponsePollingSequence(){
		return sequenceOf(
			condition(getPostConsentValidator()),
			condition(EnsureConsentAspspRevoked.class)
		);
	}
	protected ConditionSequence getPreConsentWithBearerTokenSequence(){
		return sequenceOf(
			condition(ConsentIdExtractor.class),
			condition(PrepareToFetchConsentRequest.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
		);
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		String productType = env.getString("config", "consent.productType");
		Condition getIdentification;

		if(Objects.equals(productType, "personal")){
			getIdentification = new GetPersonalIdentificationsV2Endpoint();
		}
		else if(Objects.equals(productType, "business")){
			getIdentification = new GetBusinessIdentificationsV2Endpoint();
		}
		else{
			throw new TestFailureException(getId(),"productType is not valid, it must be either personal or business");
		}

		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(getIdentification.getClass())
		);
	}

}
