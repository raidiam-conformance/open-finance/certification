package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule.AbstractAccountsApiTransactionsCurrentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountTransactionsCurrentOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsListOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "accounts_api_current-transactions_test-module_v2-4",
	displayName = "Test that the server has correctly implemented the current transactions resource V2",
	summary = "Test that the server has correctly implemented the current transactions resource V@\n" +
		"\u2022 Creates a consent with only Accounts permissions\n" +
		"\u2022 Expect - 201 code and successful redirect\n" +
		"\u2022 Using the consent created, call the Accounts API V2\n" +
		"\u2022 Call the GET Accounts API V2\n" +
		"\u2022 Expect OK 200 - Validate all fields of the API - Fetch the first returned account ids to be used on the transactions API Call\n" +
		"\u2022 Call the GET Current Accounts Transactions API V2\n" +
		"\u2022 Expect OK 200 - Validate all fields of the API - Make sure that any transaction returned have today’s date on it - Test can also expect an empty list\n" +
		"\u2022 Call the GET Current Accounts Transactions API V2, send query parameters fromBookingDate and toBookingDate using the max 7 day period\n" +
		"\u2022 Expect OK 200 - Validate all fields of the API - Make sure if transactions are found that none of them are more than 1 week older\n" +
		"\u2022 Call the GET Current Accounts Transactions API V2, send query parameters fromBookingDate and toBookingDate using a period that is not over the expected valid period\n" +
		"\u2022 Expect 422 Unprocessable Entity\n",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class AccountsApiTransactionsCurrentTestModuleV24n extends AbstractAccountsApiTransactionsCurrentTestModule {

	@Override
	protected Class<? extends Condition> getAccountListValidator() {
		return GetAccountsListOASValidatorV2n4.class;
	}


	@Override
	protected Class<? extends Condition> getAccountTransactionsValidator() {
		return GetAccountTransactionsCurrentOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getAccountTransactionCurrentValidator() {
		return GetAccountTransactionsCurrentOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
