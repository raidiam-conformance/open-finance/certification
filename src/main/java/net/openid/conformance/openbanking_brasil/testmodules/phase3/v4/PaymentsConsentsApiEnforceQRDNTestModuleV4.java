package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsentsApiEnforceQRDNTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "payments_api_qrdn-code-enforcement_test-module_v4",
	displayName = "Payments Consents API test module to enforce qrdn local instrument",
	summary = "Ensure a QR code is required when the local instrument is QRDN\n" +
		"\u2022 Create consent with “localInstrument” as QRDN, but no “qrcode” in the payload\n" +
		"\u2022 Expects 422 PARAMETRO_NAO_INFORMADO\n" +
		"\u2022 Validate Error response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public  class PaymentsConsentsApiEnforceQRDNTestModuleV4 extends AbstractPaymentsConsentsApiEnforceQRDNTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}
}
