package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

public class CreateEmptySweepingRecurringConfigurationObject extends AbstractCreateRecurringConfigurationObject {

	@Override
	protected JsonObject buildRecurringConfiguration(Environment env) {

		return new JsonObjectBuilder()
			.addField("sweeping", new JsonObject())
			.build();
	}
}
