package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasRevoked;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequenceExpecting401or422;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractUnhappyAutomaticPaymentsFunctionalTestModule extends AbstractAutomaticPaymentsFunctionalTestModule {

	protected abstract Class<? extends Condition> paymentInitiationErrorValidator();
	protected abstract HttpStatus expectedResponseCode();

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		switch (expectedResponseCode()) {
			case UNAUTHORIZED:
				return new CallPixPaymentsEndpointSequenceExpecting401or422();
			case UNPROCESSABLE_ENTITY:
				return new CallPixPaymentsEndpointSequence()
					.replace(SetProtectedResourceUrlToPaymentsEndpoint.class, condition(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class))
					.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas422.class));
			default:
				return null;
		}
	}

	@Override
	protected void requestProtectedResource() {
		if(expectedResponseCode().equals(HttpStatus.UNAUTHORIZED)){
			checkUnauthorizedStatus();
		}
		userAuthorisationCodeAccessToken();
		super.requestProtectedResource();
	}

	// Should be overridden by tests that expect 422
	protected AbstractEnsureErrorResponseCodeFieldWas ensureErrorResponseCodeFieldCondition() {
		return null;
	}

	// Should be overridden by tests that expect 401
	protected void validateRevokedConsent() {}

	protected void checkUnauthorizedStatus() {
		fetchConsentToCheckStatus("REVOKED", new EnsurePaymentConsentStatusWasRevoked());
		validateRevokedConsent();
	}

	@Override
	protected void validateResponse() {
		if (getResponseStatus() == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			callAndContinueOnFailure(paymentInitiationErrorValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(ensureErrorResponseCodeFieldCondition().getClass(), Condition.ConditionResult.FAILURE);
			env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		}
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return null;
	}

	protected int getResponseStatus() {
		return Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not extract status from response"));
	}
}


