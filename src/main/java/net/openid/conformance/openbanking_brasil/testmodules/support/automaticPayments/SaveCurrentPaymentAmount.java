package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class SaveCurrentPaymentAmount extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	@PostEnvironment(strings = "payment_amount")
	public Environment evaluate(Environment env) {
		String amount = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(paymentElement -> paymentElement.getAsJsonObject().getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("payment"))
			.map(payment -> payment.get("amount"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find data.payment.amount in payments payload"));
		env.putString("payment_amount", amount);
		logSuccess("Extracted payment amount from payment request body", args("amount", amount));
		return env;
	}
}
