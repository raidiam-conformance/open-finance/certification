package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1;

import org.springframework.http.HttpMethod;

public class PatchEnrollmentsOASValidatorV2n1 extends AbstractEnrollmentsOASValidatorV2n1 {

	@Override
	protected String getEndpointPathSuffix() {
		return "/{enrollmentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.PATCH;
	}
}
