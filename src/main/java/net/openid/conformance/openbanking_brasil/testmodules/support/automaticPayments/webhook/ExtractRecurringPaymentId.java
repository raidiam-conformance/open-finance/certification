package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class ExtractRecurringPaymentId extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	@PostEnvironment(strings = "payment_id")
	public Environment evaluate(Environment env) {
		try {
			String recurringPaymentId = OIDFJSON.getString(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.map(body -> body.getAsJsonObject().getAsJsonObject("data"))
					.map(data -> data.get("recurringPaymentId"))
						.orElseThrow(() -> error("Unable to find element data.recurringPaymentId in the response payload"))
			);

			env.putString("payment_id", recurringPaymentId);
			logSuccess("Extracted payment ID", args("payment_id", recurringPaymentId));
			return env;
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}
}
