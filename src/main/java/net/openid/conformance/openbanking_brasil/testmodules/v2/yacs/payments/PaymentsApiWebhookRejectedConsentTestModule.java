package net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.payments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddScopeToTokenEndpointRequest;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckForAccessTokenValue;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractExpiresInFromTokenEndpointResponse;
import net.openid.conformance.condition.client.ExtractTLSTestValuesFromOBResourceConfiguration;
import net.openid.conformance.condition.client.ExtractTLSTestValuesFromResourceConfiguration;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetScopeInClientConfigurationToOpenId;
import net.openid.conformance.condition.client.ValidateExpiresIn;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractOBBrasilPaymentFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureTokenEndpointResponseScopeWasPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.CreatePaymentConsentWebhookNoAlias;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.ValidatePaymentConsentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.WaitFor295Seconds;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;

@PublishTestModule(
	testName = "payments_api_webhook-consents-rejected_test-module",
	displayName = "payments_api_webhook-consents-rejected_test-module",
	summary = "This test will ensure that a webhook call for the consents endpoint is going to be correctly retried if the first answer sent by the server is set to a a value different than 202.\n" +
		"This test requires that the Software Statement client_id that is being used for testing is previously registered against the server that is going to be tested with the Webhook address set as “https://<host>/test-mtls/fvp/w, where the host is defined as the environment where this test is being executed - fvp.directory.openbankingbrasil.org.br or fvp.sandbox.directory.openbankingbrasil.org.br\n" +
		"\u2022Obtain the payments-consents endpoint for the server from the directory using the directory participants API and the provided discoveryUrl\n" +
		"\u2022Create the POST payments-consents payload using the provided brazilCpf and, if present, brazilCnpj. Set all the other fields for the standard Production Creditor Account\n" +
		"\u2022Call the POST Token endpoint with the selected authorization method sending scope=payments and grant as client_credentials\n" +
		"\u2022Call the POST payments-consents endpoint with the obtained Bearer token and the generated payload\n" +
		"\u2022Expects a 201 - Validate that status is “AWAITING_AUTHORISATION”. Validate that all fields all compliant with specifications, including the meta object and that no additional fields were sent\n" +
		"\u2022Set the webhook notification endpoint to be equal to https://<host>/test-mtls/fvp/w/open-banking/webhook/v1/payments/v4/consents/{consentId}  where consentId is equal to the consentId of the created consent.\n" +
		"\u2022Set the test to wait for 4:55 minutes\n" +
		"\u2022Expect an incoming message, for at most 60 seconds,which must be mtls protected, to be received on the webhook endpoint set for this test\n" +
		"\u2022Return a 529 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"\u2022Expect an incoming message, for at most 60 seconds, which must be mtls protected, to be received on the webhook endpoint set for this test\n" +
		"\u2022Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"\u2022Call the GET Consents Endpoint\n" +
		"\u2022Expect a 200 - Validate that status is “REJECTED”. Validate that all fields all compliant with specifications, including the meta object and that no additional fields were sent",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.brazilCnpj",
		"resource.consentUrl"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.consentSyncTime",
})
public class PaymentsApiWebhookRejectedConsentTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {
	boolean firstPaymentConsentWebhookReceived = false;
	@Override
	public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
		Boolean waitingForPaymentConsentWebhooks = env.getBoolean("second_payment_consent_webhook_received");
		eventLog.log("path",path);
		if (waitingForPaymentConsentWebhooks != null && path.matches("^(open-banking\\/webhook\\/v\\d+\\/payments\\/v4\\/consents\\/)(urn:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+)$") && waitingForPaymentConsentWebhooks) {
			if (!firstPaymentConsentWebhookReceived){
				firstPaymentConsentWebhookReceived = true;
				return new ResponseEntity<Object>("", HttpStatus.SERVICE_UNAVAILABLE);
			}
			addWebhookToObjectInEnv(env, true, false, "webhooks_received_consent", requestParts, path);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else {
			return super.handleHttpMtls(path, req, res, session, requestParts);
		}
	}
	public void addWebhookToObjectInEnv(Environment env, boolean consents, boolean payments, String key, JsonObject requestParts, String path){
		JsonObject webhooksReceived = env.getObject(key);
		JsonArray webhooks = webhooksReceived.get("webhooks").getAsJsonArray();
		requestParts.addProperty("time_received", Instant.now().toString());
		requestParts.addProperty("type", "consent");
		requestParts.addProperty("path", path);
		webhooks.add(requestParts);
		env.putObject(key, webhooksReceived);
	}
	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(WaitFor295Seconds.class);
		waitForSpecifiedWebhookResponse();
		validatePaymentWebhooks();
		fetchConsentToCheckRejectedStatus();

	}
	protected void validatePaymentWebhooks(){
		env.putBoolean("second_payment_consent_webhook_received", false);
		callAndStopOnFailure(ValidatePaymentConsentWebhooks.class);
	}
	@Override
	protected void configureDictInfo() {
		// Not needed in this test
	}
	@Override
	protected void performAuthorizationFlow() {
		initialiseWebhookObject("consent");
		enablePaymentConsentWebhook();
		performPreAuthorizationSteps();
		requestProtectedResource();
		fireTestFinished();
	}
	protected void enablePaymentConsentWebhook(){
		env.putString("time_of_consent_request", Instant.now().toString());
		env.putBoolean("second_payment_consent_webhook_received", true);
	}
	protected void initialiseWebhookObject(String type){
		JsonObject webhooksReceived = new JsonObject();
		JsonArray webhooks = new JsonArray();
		webhooksReceived.add("webhooks", webhooks);
		env.putObject("webhooks_received_" + type, webhooksReceived);
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(SetScopeInClientConfigurationToOpenId.class);
		super.validateClientConfiguration();
	}
	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndStopOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(CreatePaymentConsentWebhookNoAlias.class);
		exposeEnvString("webhook_uri_consent_id");
	}
	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		ConditionSequence preAuthSteps = new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.replace(SetPaymentsScopeOnTokenEndpointRequest.class, condition(AddScopeToTokenEndpointRequest.class))
			.insertAfter(CheckIfTokenEndpointResponseError.class, condition(EnsureTokenEndpointResponseScopeWasPayments.class).requirements("RFC6749-3.3").dontStopOnFailure())
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,sequenceOf(condition(CreateRandomFAPIInteractionId.class), condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
			);
		return preAuthSteps;
	}
	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		env.putString("test_start_timestamp", Instant.now().toString());
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
	}

	@Override
	protected void setupResourceEndpoint() {
		call(new ValidateRegisteredEndpoints(
			sequenceOf(
				condition(GetPaymentV4Endpoint.class),
				condition(GetPaymentConsentsV4Endpoint.class))
			)
		);		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		call(sequence(FAPIResourceConfiguration.class));

		callAndStopOnFailure(ExtractTLSTestValuesFromResourceConfiguration.class);
		callAndContinueOnFailure(ExtractTLSTestValuesFromOBResourceConfiguration.class, Condition.ConditionResult.INFO);
	}

	public void waitForSpecifiedWebhookResponse(){
		long delaySeconds = 5;
		eventLog.startBlock(currentClientString() + "The test will now wait for the webhook to be received");
		for (int attempts = 0; attempts < 20; attempts++) {
			eventLog.log(getId(), "Waiting for webhook to be received at with an interval of " + delaySeconds + " seconds");
			setStatus(Status.WAITING);
			try {
				Thread.sleep(delaySeconds * 1000);
			} catch (InterruptedException e) {
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}
			setStatus(Status.RUNNING);
		}
	}

	protected void fetchConsentToCheckRejectedStatus() {
		eventLog.startBlock("Checking the created consent - Expecting REJECTED status");

		/* create client credentials request */

		callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);

		addClientAuthenticationToTokenEndpointRequest();

		/* get access token */

		callAndStopOnFailure(CallTokenEndpoint.class);

		callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);

		callAndStopOnFailure(CheckForAccessTokenValue.class);

		callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);

		callAndContinueOnFailure(ExtractExpiresInFromTokenEndpointResponse.class, Condition.ConditionResult.FAILURE, "RFC6749-4.4.3", "RFC6749-5.1");

		call(condition(ValidateExpiresIn.class)
			.skipIfObjectMissing("expires_in")
			.onSkip(Condition.ConditionResult.INFO)
			.requirements("RFC6749-5.1")
			.onFail(Condition.ConditionResult.FAILURE)
			.dontStopOnFailure());

		callAndContinueOnFailure(SaveConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(SaveAccessToken.class);
		callAndStopOnFailure(PaymentConsentIdExtractor.class);
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentStatusWasRejected.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator(){
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}
}
