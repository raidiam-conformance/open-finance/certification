package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.PostEnrollmentsErrorEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasContaInvalida extends AbstractEnsureErrorResponseCodeFieldWas {

    @Override
    protected List<String> getExpectedCodes() {
        return List.of(PostEnrollmentsErrorEnum.CONTA_INVALIDA.toString());
    }
}
