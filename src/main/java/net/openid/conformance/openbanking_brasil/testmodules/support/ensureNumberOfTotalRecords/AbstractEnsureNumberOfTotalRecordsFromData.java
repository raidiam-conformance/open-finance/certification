package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.JsonHelper;

public abstract class AbstractEnsureNumberOfTotalRecordsFromData extends AbstractEnsureNumberOfTotalRecords {

	@Override
	protected int getTotalRecords(JsonObject response) {
		int totalRecords;
		if (!JsonHelper.ifExists(response, "$.data")) {
			throw error("The response does not contain a data element.");
		}

		JsonElement dataElement = findByPath(response, "$.data");

		totalRecords = dataElement.getAsJsonArray().size();
		return totalRecords;
	}
}
