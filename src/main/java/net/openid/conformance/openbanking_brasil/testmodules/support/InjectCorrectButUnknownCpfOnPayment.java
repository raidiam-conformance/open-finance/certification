package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Map;

public class InjectCorrectButUnknownCpfOnPayment extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		var cpf = "51382920725";
		JsonObject obj = env.getObject("resource");
		obj = env.getString("payment_is_array") == null ?
			obj.getAsJsonObject("brazilPixPayment").getAsJsonObject("data")
			: obj.getAsJsonObject("brazilPixPayment").getAsJsonArray("data").get(0).getAsJsonObject();
		obj.addProperty("proxy", cpf);

		logSuccess("Added correct but unregistered CPF to payment", Map.of("CPF", cpf));

		return env;
	}
}
