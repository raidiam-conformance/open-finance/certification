package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum EnrollmentRejectionReasonEnum {
    REJEITADO_TEMPO_EXPIRADO_AUTHORISATION,
    REJEITADO_TEMPO_EXPIRADO_ENROLLMENT,
	REJEITADO_TEMPO_EXPIRADO_ACCOUNT_HOLDER_VALIDATION,
	REJEITADO_TEMPO_EXPIRADO_RISK_SIGNALS,
    REJEITADO_MAXIMO_CHALLENGES_ATINGIDO,
    REJEITADO_MANUALMENTE,
    REJEITADO_DISPOSITIVO_INCOMPATIVEL,
    REJEITADO_FALHA_INFRAESTRUTURA,
    REJEITADO_FALHA_HYBRID_FLOW,
    REJEITADO_FALHA_FIDO,
    REJEITADO_SEGURANCA_INTERNA,
    REJEITADO_OUTRO;

    public static Set<String> toSet(){
        return Stream.of(values())
            .map(Enum::name)
            .collect(Collectors.toSet());
    }
}
