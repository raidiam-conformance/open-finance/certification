package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public abstract class GetPaymentVXEndpoint extends AbstractGetXFromAuthServer {
	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/payments\\/v\\d+\\/pix\\/payments)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "payments-pix";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
