package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddScopeToTokenEndpointRequest;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetScopeInClientConfigurationToOpenId;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureTokenEndpointResponseScopeWasPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.EnsureRefreshTokenNotRotated;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ExtractAndAddRefreshTokenToEnv;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;

public abstract class AbstractPaymentsConsentsCoreTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	@Override
	protected void setupResourceEndpoint() {
		call(new ValidateRegisteredEndpoints(getPaymentAndPaymentConsentEndpoints()));
		super.setupResourceEndpoint();
	}

	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(ExtractAndAddRefreshTokenToEnv.class);
		call(new RefreshTokenRequestSteps(false,addTokenEndpointClientAuthentication,false));
		callAndStopOnFailure(EnsureRefreshTokenNotRotated.class);
	}

	@Override
	protected void configureDictInfo() {
		// Not needed in this test
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(SetScopeInClientConfigurationToOpenId.class);
		super.validateClientConfiguration();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndStopOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		ConditionSequence preAuthSteps = new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.replace(SetPaymentsScopeOnTokenEndpointRequest.class, condition(AddScopeToTokenEndpointRequest.class))
			.insertAfter(CheckIfTokenEndpointResponseError.class, condition(EnsureTokenEndpointResponseScopeWasPayments.class).requirements("RFC6749-3.3").dontStopOnFailure());

		return preAuthSteps;
	}

	@Override
	protected void requestAuthorizationCode() {
		// Store the original access token and ID token separately (see RefreshTokenRequestSteps)
		env.mapKey("access_token", "first_access_token");
		env.mapKey("id_token", "first_id_token");

		super.requestAuthorizationCode();

		// Set up the mappings for the refreshed access and ID tokens
		env.mapKey("access_token", "second_access_token");
		env.mapKey("id_token", "second_id_token");

	}

	protected abstract ConditionSequence getPaymentAndPaymentConsentEndpoints();

}
