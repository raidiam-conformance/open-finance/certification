package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;

public class CreateCompleteRiskSignalsRequestBodyToRequestEntityClaims extends AbstractCondition {

	@Override
	@PostEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		env.putObject("resource_request_entity_claims", getRiskSignalsObject());
		logSuccess("Successfully created risk-signals request body", args(
			"body", env.getObject("resource_request_entity_claims")
		));
		return env;
	}


	public static JsonObject getRiskSignalsObject(){
		return new JsonObjectBuilder().addFields("data", Map.of(
			"deviceId", "5ad82a8f-37e5-4369-a1a3-be4b1fb9c034",
			"isRootedDevice", true,
			"screenBrightness", 90,
			"elapsedTimeSinceBoot", 28800000,
			"osVersion", "16.6",
			"userTimeZoneOffset", "-03",
			"language", "pt",
			"accountTenure", "2023-01-01"
		)).addFields("data", Map.of(
			"isCallInProgress", true,
			"isDevModeEnabled", true,
			"isMockGPS", true,
			"isEmulated", true,
			"isMonkeyRunner", true,
			"isCharging", true,
			"antennaInformation", "CellIdentityLte:{ mCi=2******60 mPci=274 mTac=5***1 mEarfcn=9510 mBands=[28] mBandwidth=2147483647 mMcc=724 mMnc=10 mAlphaLong=VIVO mAlphaShort=VIVO mAdditionalPlmns={} mCsgInfo=null}, CellIdentityLte:{ mCi=1*****01 mPci=361 mTac=3***6 mEarfcn=9410 mBands=[28] mBandwidth=2147483647 mMcc=724 mMnc=03 mAlphaLong=TIMBRASIL mAlphaShort=TIMBRASIL mAdditionalPlmns={} mCsgInfo=null}",
			"isUsbConnected", true
		)).addFields("data.screenDimensions", Map.of(
			"height", 1080,
			"width", 1920
		)).addFields("data.geolocation", Map.of(
			"latitude", -15.738602,
			"longitude", -47.926498,
			"type", "FINE"
		)).addFields("data.integrity", Map.of(
			"appRecognitionVerdict", "PLAY_RECOGNIZED",
			"deviceRecognitionVerdict", "[\\\"MEETS_DEVICE_INTEGRITY\\\"]"
		)).build();
	}
}
