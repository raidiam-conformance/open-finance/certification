package net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractFvpEnrollmentsApiPaymentsPreEnrollmentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.GetEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostConsentsAuthoriseOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostEnrollmentsOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-enrollments_api_payments-pre-enrollment_test-module_v1",
	displayName = "fvp-enrollments_api_payments-pre-enrollment_test-module_v1",
	summary = "Ensure payment without redirect cannot be executed if Enrollment status is \"AWAITING_ENROLLMENT\".\n" +
		"• Call the POST enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Call the POST Risk Signals endpoint sending the appropriate signals\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AWAITING_ACCOUNT_HOLDER_VALIDATION\"\n" +
		"• Redirect the user to authorize the enrollment Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AWAITING_ENROLLMENT\", and that dailyLimit and transactionLimit are 1 BRL\n" +
		"• Call POST consent with valid payload, and amount of 0.5 BRL\n" +
		"• Expects 201 - Validate response\n" +
		"• Call POST Consents Authorize with a Mocked Payload on the FidoAssertion\n" +
		"• Expects 422 - STATUS_VINCULO_INVALIDO and Validate error response\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is REJECTED, and rejectionReason is NAO_INFORMADO\n" +
		"• Call the PATCH Enrollments {EnrollmentID}\n" +
		"• Expects 204\n"
	,
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class FvpEnrollmentsApiPaymentsPreEnrollmentTestModuleV1 extends AbstractFvpEnrollmentsApiPaymentsPreEnrollmentTestModule {

	@Override
	protected Class<? extends Condition> postConsentsAuthoriseValidator() {
		return PostConsentsAuthoriseOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetPaymentConsentsV4Endpoint.class), condition(GetPaymentV4Endpoint.class), condition(GetEnrollmentsV1Endpoint.class));
	}
}
