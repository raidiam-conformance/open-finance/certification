package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectedByEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectedFromEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectionReasonEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsStatusEnum;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;

public class CreatePatchRecurringPaymentsConsentsForRejectionRequestBody extends AbstractCondition {

	@Override
	@PostEnvironment(required = "consent_endpoint_request")
	public Environment evaluate(Environment env) {
		JsonObject consentRequestBody = new JsonObjectBuilder()
			.addField("data.status", RecurringPaymentsConsentsStatusEnum.REJECTED.toString())
			.addFields("data.rejection", Map.of(
				"rejectedBy", RecurringPaymentsConsentsRejectedByEnum.USUARIO.toString(),
				"rejectedFrom", RecurringPaymentsConsentsRejectedFromEnum.INICIADORA.toString()
			))
			.addFields("data.rejection.reason", Map.of(
				"code", RecurringPaymentsConsentsRejectionReasonEnum.REJEITADO_USUARIO.toString(),
				"detail", RecurringPaymentsConsentsRejectionReasonEnum.REJEITADO_USUARIO.toString()
			))
			.build();
		env.putObject("consent_endpoint_request", consentRequestBody);
		logSuccess("Patch request body for recurring payments consents created", args("body", consentRequestBody));
		return env;
	}
}
