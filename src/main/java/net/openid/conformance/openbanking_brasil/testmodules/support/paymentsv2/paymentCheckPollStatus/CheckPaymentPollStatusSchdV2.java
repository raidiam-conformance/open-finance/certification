package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractCheckPaymentPollStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentStatusEnumV2;

import java.util.List;

public class CheckPaymentPollStatusSchdV2 extends AbstractCheckPaymentPollStatus {

	@Override
	protected List<String> getExpectedStatuses() {
		return List.of(
			PaymentStatusEnumV2.SCHD.toString()
		);
	}
}
