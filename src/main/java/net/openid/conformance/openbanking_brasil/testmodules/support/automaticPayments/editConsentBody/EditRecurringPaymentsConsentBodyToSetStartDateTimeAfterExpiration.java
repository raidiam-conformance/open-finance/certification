package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class EditRecurringPaymentsConsentBodyToSetStartDateTimeAfterExpiration extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.map(consentElement -> consentElement.getAsJsonObject().getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data field in consents payload"));

		JsonObject sweeping = Optional.ofNullable(data.getAsJsonObject("recurringConfiguration"))
			.map(recurringConfiguration -> recurringConfiguration.getAsJsonObject("sweeping"))
			.orElseThrow(() -> error("The recurringConfiguration is not set to sweeping"));

		addDateTimeProperty(sweeping, "startDateTime", 10);
		addDateTimeProperty(data, "expirationDateTime", 5);

		logSuccess("The startDateTime and the expirationDateTime fields have been set to D+10 and D+5 respectively",
			args("data", data));

		return env;
	}

	private void addDateTimeProperty(JsonObject data, String propertyName, int daysToAdd) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		LocalDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		LocalDateTime dateTime = currentDateTime.plusDays(daysToAdd);
		String formattedDateTime = dateTime.format(formatter);
		data.addProperty(propertyName, formattedDateTime);
	}
}
