package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractGetXFromAuthServer extends AbstractGetEndpointFromAuthServer {


	@Override
	@PreEnvironment(required = {"authorisation_server", "config"})
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("config").getAsJsonObject("resource");
		String url = getApiEndpoint(env, getApiFamilyType(), getApiVersionRegex(), getEndpointRegex());

		if (url == null) {
			performUrlNotFoundAction(env);
			return env;
		}

		if(isResource()){
			resource.addProperty(getResourceUrlProperty(), url);
			logSuccess("Inserted resource endpoint into config", args(getResourceUrlProperty(), url));
			return env;
		}

		resource.addProperty("consentUrl", url);
		logSuccess("Inserted consent endpoint into config", args("consentUrl", url));
		return env;

	}

	protected void performUrlNotFoundAction(Environment env) {
		String type = isResource() ? "resource" : "consent";
		throw error(String.format("Unable to locate %s endpoint to continue", type));
	}

	protected String getResourceUrlProperty() {
		return "resourceUrl";
	}

	protected abstract String getEndpointRegex();

	protected abstract String getApiFamilyType();

	protected abstract String getApiVersionRegex();

	protected abstract boolean isResource();

}
