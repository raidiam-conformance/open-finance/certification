package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentStatusEnum;

public class EnsureEnrollmentStatusWasAwaitingAccountHolderValidation extends AbstractEnsureEnrollmentStatusWas {

	@Override
	protected String getExpectedStatus() {
		return EnrollmentStatusEnum.AWAITING_ACCOUNT_HOLDER_VALIDATION.toString();
	}
}
