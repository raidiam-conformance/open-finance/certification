package net.openid.conformance.openbanking_brasil.testmodules.v3n;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractConsentApiTestModule extends AbstractPhase2TestModule {

	protected abstract Class<? extends Condition> setGetConsentValidator();

	@Override
	protected abstract Class<? extends Condition> getPostConsentValidator();

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config,baseUrl);
	}

	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpenBankingBrazilPreAuthorizationSteps steps = (OpenBankingBrazilPreAuthorizationSteps) new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false)
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().mapKey("resource_endpoint_response", "consent_endpoint_response"))
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, condition(getPostConsentValidator()).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure())
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().unmapKey("resource_endpoint_response"));

		return steps;
	}

	@Override
	protected void performPostAuthorizationFlow() {
		runInBlock("Validating get consent response v3-1", () -> {
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
			call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
			callAndContinueOnFailure(setGetConsentValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureConsentResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureConsentWasAuthorised.class, Condition.ConditionResult.FAILURE);
			call(exec().unmapKey("resource_endpoint_response_full"));
		});

		runInBlock("Deleting consent v3-1", () -> {
			callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		});

		fireTestFinished();
	}

	@Override
	protected void validateResponse() {}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class));
	}
}
