package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public abstract class AbstractCreateConsentExtensionExpirationTime extends AbstractCondition {
	@Override
	@PostEnvironment(strings = "consent_extension_expiration_time")
	public Environment evaluate(Environment env) {
		Instant expiryTime = Instant.now().plus(getExtensionTimeInDays(), ChronoUnit.DAYS);
		Instant expiryTimeNoFractionalSeconds = expiryTime.truncatedTo(ChronoUnit.SECONDS);

		String rfc3339ExpiryTime = DateTimeFormatter.ISO_INSTANT.format(expiryTimeNoFractionalSeconds);
		//create json object with array of expiry time, if one is not present already create a new one otherwise just add to the existing one
		JsonObject expiryTimes = env.getObject("extension_expiry_times");
		if (expiryTimes == null) {
			expiryTimes = new JsonObject();
			JsonArray expiryTimesArray = new JsonArray();
			expiryTimesArray.add(rfc3339ExpiryTime);
			expiryTimes.add("extension_times", expiryTimesArray);
		} else {
			JsonArray expiryTimesArray = expiryTimes.getAsJsonArray("extension_times");
			expiryTimesArray.add(rfc3339ExpiryTime);
		}
		env.putObject("extension_expiry_times", expiryTimes);
		env.putString("consent_extension_expiration_time", rfc3339ExpiryTime);
		if (saveConsentExtensionExpirationTime()) {
			env.putString("saved_consent_extension_expiration_time", rfc3339ExpiryTime);
			logSuccess("Saved consent extension expiry time", args("saved_consent_extension_expiration_time", rfc3339ExpiryTime));
		}
		logSuccess("Created consent extension expiry time", args("consent_extension_expiration_time", rfc3339ExpiryTime));

		return env;
	}

	protected abstract int getExtensionTimeInDays();

	protected abstract boolean saveConsentExtensionExpirationTime();
}
