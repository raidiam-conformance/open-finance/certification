package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiAutomaticPixRetryExtradayCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixListOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PatchRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_automatic-pix-extraday-core_test-module_v2",
	displayName = "automatic-payments_api_automatic-pix-extraday-core_test-module_v2",
	summary = "This test will simulate an extraday retry after two failed payment. To achieve this, the test requires a pre-created and approved Consent ID, which should be configured in the \"Extraday Consent ID - Retry Accepted\" field, with two Failed Payment IDs, which will be retrieved during the test. The test will also need a refresh token linked to the client and user specified in the configuration. An access token derived from this refresh token will be used to make the payment, configured in the \"Extraday Refresh Token - Retry Accepted\" field. The pre-set consent must have isRetryAccepted set to true, and no firstPayment should be included.\n" +
		"\n" +
		"• Call the GET recurring-consents {recurringConsentId} endpoint, using the Extraday Consent ID Retry Accepted specified in the Config and a client_credentials token.\n" +
		"• Expect a 200 response - Validate the response, ensure the status is \"Authorised\", isRetryAccepted is true, no firstPayment information is present.\n" +
		"• Call the GET recurring-payment endpoint with the recurringConsent ID in the header.\n" +
		"• Expect a 200 response - Validate the response, ensure there are two payments, the status is RJCT, and the payment date is set to before today or today, and that the rejectionReason expects a new E2EID.\n" +
		"• Call the GET recurring-payments {recurringPaymentId} endpoint to fetch the original payment data.\n" +
		"• Expect a 200 response - Validate the response and extract its fields.\n" +
		"• Call the POST recurring-payments endpoint, using an access token derived from the refresh token in the configuration. Send a new E2EID, date as D+1, and the other fields with the same information as the previous payment, and include the first paymentId in originalRecurringPaymentId\n" +
		"• Expect 201 - Validate Response\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD or ACCP\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is SCHD\n" +
		"• Call the PATCH recurring-payments {recurringPaymentId} endpoint\n" +
		"• Expect 422 - PAGAMENTO_NAO_PERMITE_CANCELAMENTO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.extradayRetryAcceptedConsentId",
		"resource.extradayRetryAcceptedRefreshToken"
	}
)
public class AutomaticPaymentsApiAutomaticPixRetryExtradayCoreTestModuleV2 extends AbstractAutomaticPaymentsApiAutomaticPixRetryExtradayCoreTestModule {

	@Override
	protected Class<? extends Condition> getPaymentsByConsentIdValidator() {
		return GetRecurringPaymentPixListOASValidatorV2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class), condition(GetAutomaticPaymentPixRecurringV2Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> patchPaymentValidator() {
		return PatchRecurringPaymentPixOASValidatorV2.class;
	}
}
