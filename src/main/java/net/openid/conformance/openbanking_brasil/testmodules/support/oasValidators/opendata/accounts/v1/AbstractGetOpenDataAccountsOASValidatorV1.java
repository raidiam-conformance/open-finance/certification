package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.accounts.v1;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

import java.util.List;

public abstract class AbstractGetOpenDataAccountsOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	protected abstract String apiPrefix();
	protected abstract void assertSpecificConstraints(JsonObject obj);

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-opendata-accounts-v1.0.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return String.format("/%s-accounts", apiPrefix());
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonArray data = body.getAsJsonArray("data");
		for (JsonElement element : data) {
			JsonObject obj = element.getAsJsonObject();
			assertField1IsRequiredWhenField2HasValue2(obj, "serviceBundles", "type", List.of("CONTA_DEPOSITO_A_VISTA", "CONTA_POUPANCA"));
			assertOpeningClosingChannelsConstraint(obj);
			assertSpecificConstraints(obj);
		}
	}

	protected void assertOpeningClosingChannelsConstraint(JsonObject obj) {
		JsonArray openingClosingChannels = obj.getAsJsonArray("openingClosingChannels");
		if (openingClosingChannels.contains(new JsonPrimitive("OUTROS")) && !obj.has("openingClosingChannelsAdditionalInfo")) {
			throw error("openingClosingChannelsAdditionalInfo is required when openingClosingChannels has OUTROS in it");
		}
	}
}
