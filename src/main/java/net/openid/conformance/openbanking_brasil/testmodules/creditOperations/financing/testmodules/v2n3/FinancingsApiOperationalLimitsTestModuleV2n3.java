package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.AbstractFinancingsApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsIdentificationV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsListV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsPaymentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsScheduledInstalmentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsWarrantiesV2n3OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "financings_api_operational-limits_test-module_v2-3",
	displayName = "This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Financings API are considered correctly",
	summary = "This test will require the user to have set at least two ACTIVE resources on the Financings API. \n" +
		"This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Financings API are considered correctly.\n" +
		"• Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test have been provided\n" +
		"• Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Credit Operations permission group\n" +
		"• Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"• Redirect User to authorize the Created Consent - Expect a successful authorization\n" +
		"• With the authorized consent id (1), call the GET Financings List API Once - Expect a 200 - Save the first returned ACTIVE resource id (R_1) and the second saved returned active resource id (R_2)\n" +
		"• With the authorized consent id (1), call the GET Financings  API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"• With the authorized consent id (1), call the GET Financings Warranties API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"• With the authorized consent id (1), call the GET Financings Scheduled Instalments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"• With the authorized consent id (1), call the GET Financings Payments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"• With the authorized consent id (1), call the GET Financings API with the saved Resource ID (R_2) 4 times - Expect a 200\n" +
		"• Repeat the exact same process done with the first tested resources (R_1) but now, execute it against the second returned Resource (R_2) \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness"
	}
)
public class FinancingsApiOperationalLimitsTestModuleV2n3 extends AbstractFinancingsApiOperationalLimitsTestModule {

	@Override
	protected Class<? extends Condition> contractsResponseValidator() {
		return GetFinancingsListV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractIdValidator() {
		return GetFinancingsIdentificationV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractWarrantiesValidator() {
		return GetFinancingsWarrantiesV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractScheduledInstalmentsValidator() {
		return GetFinancingsScheduledInstalmentsV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractPaymentsValidator() {
		return GetFinancingsPaymentsV2n3OASValidator.class;
	}
}
