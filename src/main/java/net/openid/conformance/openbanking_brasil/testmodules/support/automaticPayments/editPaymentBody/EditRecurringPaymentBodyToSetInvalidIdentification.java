package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AbstractEditObjectBodyToManipulateCnpjOrCpf;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EditRecurringPaymentBodyToSetInvalidIdentification extends AbstractEditObjectBodyToManipulateCnpjOrCpf {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject document = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(paymentElement -> paymentElement.getAsJsonObject().getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("document"))
			.orElseThrow(() -> error("Unable to find data.document in payments payload"));

		String currentIdentification = OIDFJSON.getString(
			Optional.ofNullable(document.get("identification"))
				.orElseThrow(() -> error("Unable to find identification field for the document", args("document", document)))
		);

		String newIdentification;
		switch (currentIdentification.length()) {
			case CPF_DIGITS:
				newIdentification = generateNewValidCpfFromDifferentRegion(currentIdentification);
				break;
			case CNPJ_DIGITS:
				newIdentification = generateNewRandomCnpj();
				break;
			default:
				throw error("The value inside the identification field is neither a CPF nor a CNPJ",
					args("identification", currentIdentification));
		}

		document.addProperty("identification", newIdentification);
		logSuccess("Updated data.document to have a different identification", args(
			"old identification", currentIdentification,
			"new identification", newIdentification,
			"document", document
		));

		return env;
	}
}
