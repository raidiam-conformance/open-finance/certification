package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class SaveSelfLinkUri extends AbstractCondition {

	@Override
	@PostEnvironment(strings = "saved_self_link_uri")
	public Environment evaluate(Environment env) {

		JsonObject body;
		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElse(env.getObject("resource_endpoint_response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		JsonObject links = body.getAsJsonObject("links");

		env.putString("saved_self_link_uri", OIDFJSON.getString(links.get("self")));

		log(String.format("Self link saved for a second GET attempt: %s", OIDFJSON.getString(links.get("self"))));
		return env;
	}

}
