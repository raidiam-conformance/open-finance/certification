package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractRefreshTokenFromTokenResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractRecurringPaymentIdToPaymentIdArray;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LogRetryAuxTestResults;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectUserDefinedIntervalAndDateAndIsRetryAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToAddDefinedCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetUserDefinedExpirationDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetUserDefinedCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceAccordingToUserDefinedInterval;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToUserDefinedDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountToUserDefinedAmount;

public abstract class AbstractAutomaticPaymentsAutomaticPixRetryAuxiliaryPathTestModule extends AbstractAutomaticPaymentsAutomaticPixTestModule {
	/**
	 * This class should be extended by automatic pix tests that follow the steps bellow (the steps inside the bracket are repeated amountOfPayments() times):
	 *     - Call the POST recurring-consents endpoint with automatic pix fields, with referenceStartDate, expirationDateTime, isRetryAccepted and interval as defined at the configuration, and not sending firstPayment information
	 *     - Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION
	 *     - Redirect the user to authorize consent
	 *     - Call the GET recurring-consents endpoint
	 *     - Expect 201 - Validate Response and ensure status is AUTHORISED
	 *     {
	 *         - Call the POST recurring-payments endpoint with the amount and date defined at the configuration for payment i
	 *         - Expect 201 - Validate Response
	 *     }
	 *     - Log the consentID, paymentIDs and refresh_token used
	 * In order to properly create the recurring-consents and recurring-payments request bodies, the user will need to provide the following fields:
	 *     - resource.debtorAccountIspb
	 *     - resource.debtorAccountIssuer
	 *     - resource.debtorAccountNumber
	 *     - resource.debtorAccountType
	 *     - resource.contractDebtorName
	 *     - resource.contractDebtorIdentification
	 *     - resource.creditorAccountIspb
	 *     - resource.creditorAccountIssuer
	 *     - resource.creditorAccountNumber
	 *     - resource.creditorAccountAccountType
	 *     - resource.creditorName
	 *     - resource.creditorCpfCnp
	 *     - resource.referenceStartDate
	 *     - resource.expirationDateTime
	 *     - resource.isRetryAccepted
	 *     - resource.interval
	 *     - resource.paymentAmount
	 *     - resource.paymentDate
	 */
	protected abstract int amountOfPayments();

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectUserDefinedIntervalAndDateAndIsRetryAccepted();
	}

	@Override
	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return new SetAutomaticPaymentAmountToUserDefinedAmount();
	}

	@Override
	protected void configurePaymentDate() {
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToUserDefinedDate.class);
		callAndStopOnFailure(SetPaymentReferenceAccordingToUserDefinedInterval.class);
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToSetUserDefinedExpirationDateTime.class);
	}

	@Override
	protected void configureCreditorFields() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToAddDefinedCreditor.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetUserDefinedCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj.class);
	}

	@Override
	protected void requestAuthorizationCode() {
		super.requestAuthorizationCode();
		callAndStopOnFailure(ExtractRefreshTokenFromTokenResponse.class);
	}

	@Override
	protected void requestProtectedResource() {
		for (int payment = 1; payment <= amountOfPayments(); payment++) {
			userAuthorisationCodeAccessToken();
			runInBlock(String.format("Call POST recurring-payments for payment %d - Expect 201", payment), () -> {
				callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
				call(getPixPaymentSequence());
				call(postPaymentValidationSequence());
				callAndStopOnFailure(ExtractRecurringPaymentIdToPaymentIdArray.class, Condition.ConditionResult.FAILURE);
			});
		}
		callAndStopOnFailure(LogRetryAuxTestResults.class);
	}
}
