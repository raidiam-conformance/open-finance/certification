package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class CardBillSelector extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {
		JsonObject body;
		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		JsonArray data = Optional.ofNullable(body.get("data")).orElse(new JsonArray()).getAsJsonArray();
		if(data.isEmpty()) {
			throw error("Data field is empty, no further processing required.");
		}

		JsonObject firstAccount = data.get(0).getAsJsonObject();
		String billId = OIDFJSON.getString(firstAccount.get("billId"));
		env.putString("billId", billId);
		return env;
	}

}
