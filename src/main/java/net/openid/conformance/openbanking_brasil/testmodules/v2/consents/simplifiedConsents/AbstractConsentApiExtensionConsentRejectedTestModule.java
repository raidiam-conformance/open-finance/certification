package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentRejectAspspMaxDateReached;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus365;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateIndefiniteConsentExpiryTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureResponseCodeWas401or403or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.FapiBrazilSetConsentExpirationNowPlus120Seconds;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.PrepareToGetConsentExtensions;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateConsent422EstadoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInConsentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInGetSizeOne;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.WaitFor120Seconds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPostConsentExtensionsExpectingErrorSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.sequence.ConditionSequence;


public abstract class AbstractConsentApiExtensionConsentRejectedTestModule extends AbstractSimplifiedConsentRenewalTestModule {
	boolean firstGetConsent = true;

	@Override
	protected abstract Class<? extends Condition> setPostConsentExtensionValidator();

	@Override
	protected abstract Class<? extends Condition> setGetConsentExtensionValidator();

	@Override
	public void callExtensionEndpoints(){
		firstGetConsent = false;
		callAndStopOnFailure(WaitFor120Seconds.class);
		runInBlock("Validating get consent response", this::callGetConsentEndpoint);
		runInBlock("Validating post consent extension response expecting error - D+365", this::callPostConsentExtensionEndpoint);
	}
	@Override
	public ConditionSequence getPostConsentExtensionSequence() {
		return new CallPostConsentExtensionsExpectingErrorSequence()
			.replace(CreateIndefiniteConsentExpiryTime.class, condition(setExtensionExpirationTime()).onFail(Condition.ConditionResult.FAILURE))
			.replace(EnsureConsentResponseCodeWas422.class, condition(EnsureResponseCodeWas401or403or422.class).onFail(Condition.ConditionResult.FAILURE))
			.replace(ValidateConsent422EstadoInvalido.class, condition(ValidateConsent422EstadoInvalido.class).skipIfStringMissing("422_status"));
	}

	@Override
	public void validateGetConsentEndpointResponse() {
		callAndContinueOnFailure(setGetConsentValidator(), Condition.ConditionResult.FAILURE);
		if (firstGetConsent) {
			callAndContinueOnFailure(EnsurePaymentConsentStatusWasAuthorised.class, Condition.ConditionResult.FAILURE);
		} else {
			callAndContinueOnFailure(EnsureConsentStatusWasRejected.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureConsentRejectAspspMaxDateReached.class);
		}
	}

	@Override
	protected void configureClient() {
		env.putString("metaOnlyRequestDateTime", "true");
		super.configureClient();
	}

	@Override
	protected void callGetConsentAndOrResourceUrlSequence(){
		call(new ValidateRegisteredEndpoints(
				sequenceOf(
					condition(GetConsentV3Endpoint.class),
					condition(GetResourcesV3Endpoint.class)
				)
			)
		);
	}

	@Override
	protected Class<? extends Condition> validateExpirationTimeReturned(){
		return ValidateExtensionExpiryTimeInConsentResponse.class;
	}

	@Override
	protected Class<? extends Condition> validateGetConsentExtensionEndpointResponse() {
		return ValidateExtensionExpiryTimeInGetSizeOne.class;
	}

	@Override
	protected Class<? extends Condition> buildConsentRequestBody() {
		return FAPIBrazilOpenBankingCreateConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {
		return CreateExtensionRequestTimeDayPlus365.class;
	}

	@Override
	public Class<? extends Condition> setConsentExpirationTime() {
		return FapiBrazilSetConsentExpirationNowPlus120Seconds.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionEndpoint() {
		return PrepareToGetConsentExtensions.class;
	}

	@Override
	protected void checkTokenValue() {
		// not needed in this test
	}
}
