package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionCurrentTimezoneTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.AbstractInvestmentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckCurrentTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactionsCurrent;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractInvestmentsApiTransactionsCurrentTimezoneTestModule extends AbstractInvestmentsApiTestModule {

	@Override
	protected void executeParticularTestSteps() {
		runInBlock("GET Investments Transactions-Current API without any query parameters", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class);
			callAndStopOnFailure(CallProtectedResource.class);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(investmentsTransactionsValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(checkAllDates(), Condition.ConditionResult.FAILURE);
		});
		runInBlock(String.format("GET Investments Transactions-Current API - Send query Params from%s=D-6 and to%s=D+0", transactionParameterName(), transactionParameterName()), () -> {
			callAndStopOnFailure(setFromTransactionTo6DaysAgo());
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class);
			callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
			callAndStopOnFailure(CallProtectedResource.class);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(investmentsTransactionsValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(checkTransactionRange(), Condition.ConditionResult.FAILURE);
		});
		runInBlock(String.format("GET Investments Transactions-Current API - Send query Params from%s=D-7 and to%s=D+0", transactionParameterName(), transactionParameterName()), () -> {
			callAndStopOnFailure(setFromTransactionTo7DaysAgo());
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class);
			callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
			callAndStopOnFailure(CallProtectedResource.class);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected ConditionSequence deleteConsent() {
		return sequenceOf(
			super.deleteConsent(),
			condition(CheckCurrentTime.class)
		);
	}

	protected abstract Class<? extends Condition> investmentsTransactionsValidator();
	protected abstract Class<? extends Condition> setFromTransactionTo6DaysAgo();
	protected abstract Class<? extends Condition> setToTransactionToToday();
	protected abstract Class<? extends Condition> setFromTransactionTo7DaysAgo();
	protected abstract Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl();
	protected abstract Class<? extends Condition> checkTransactionRange();
	protected abstract Class<? extends Condition> checkAllDates();
	protected abstract String transactionParameterName();
}
