package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize1000;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextOrStopPolling;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas202;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.ResourcesApiPollingTimeout;
import net.openid.conformance.runner.TestExecutionManager;
import net.openid.conformance.sequence.AbstractConditionSequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.Environment;

public class PollApiRootToExtractEveryIdFromEveryPage extends AbstractConditionSequence {

    private final TestInstanceEventLog eventLog;
    private final TestInfoService testInfo;
    private final TestExecutionManager executionManager;
    private String id;
    private Environment env;
    private final Class<? extends Condition> idExtractingCondition;


    public PollApiRootToExtractEveryIdFromEveryPage(Environment env, String id, TestInstanceEventLog eventLog, TestInfoService testInfo, TestExecutionManager executionManager, Class<? extends Condition> idExtractingCondition) {
        this.id = id;
        this.eventLog = eventLog;
        this.testInfo = testInfo;
        this.executionManager = executionManager;
        this.env = env;
        this.idExtractingCondition = idExtractingCondition;
    }

    @Override
    public void evaluate() {
        ConditionSequenceRepeater repeatSequence = new ConditionSequenceRepeater(env, id, eventLog, testInfo, executionManager,
            () -> getPreCallProtectedResourceSequence()
                .then(getPollingSequence()))
            .untilTrue("is_last_page")
            .times(30)
            .trailingPause(1)
            .onTimeout(sequenceOf(
                condition(EnsureResourceResponseCodeWas202.class),
                condition(ResourcesApiPollingTimeout.class)));

        repeatSequence.run();
    }

    protected ConditionSequence getPreCallProtectedResourceSequence() {
        return sequenceOf(
            condition(CreateEmptyResourceEndpointRequestHeaders.class),
            condition(AddFAPIAuthDateToResourceEndpointRequest.class),
            condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
            condition(CreateRandomFAPIInteractionId.class),
            condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
            condition(SetProtectedResourceUrlPageSize1000.class),
            condition(CallProtectedResource.class),
            condition(idExtractingCondition)
        );
    }

    protected ConditionSequence getPollingSequence() {
        return sequenceOf(
            condition(SetProtectedResourceUrlToNextOrStopPolling.class)
        );
    }
}
