package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.checkCurrentTime;

import java.time.LocalTime;

public class CheckCurrentTimeBefore12AM extends AbstractCheckCurrentTime {

	@Override
	protected LocalTime getStartTime() {
		return LocalTime.of(0, 0);
	}

	@Override
	protected LocalTime getEndTime() {
		return LocalTime.of(11, 59, 59);
	}
}
