package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.overridePaymentsInResources;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.LocalDate;
import java.util.Optional;

public class OverridePaymentInResourcesWithWrongWeeklySchedule extends AbstractOverridePaymentInResources {

	@Override
	protected void overridePaymentsData(JsonArray data, JsonObject schedule) {
		JsonObject weekly = Optional.ofNullable(schedule.getAsJsonObject("weekly"))
			.orElseThrow(() -> error("Could not extract the weekly from the payment in resource"));

		int quantity = OIDFJSON.getInt(weekly.get("quantity"));
		String startDateString = OIDFJSON.getString(weekly.get("startDate"));
		if (data.size() != quantity) {
			throw error("Payments data array does not have the correct amount of payments in it");
		}

		for (JsonElement paymentElement : data) {
			JsonObject payment = paymentElement.getAsJsonObject();

			String oldEndToEndId = OIDFJSON.getString(payment.get("endToEndId"));
			String newEndToEndId = generateEndToEndId(oldEndToEndId, startDateString);
			payment.addProperty("endToEndId", newEndToEndId);

			startDateString = nextDateString(startDateString);
		}
	}

	protected String nextDateString(String currentString) {
		LocalDate currentDate = LocalDate.parse(currentString);
		currentDate = currentDate.plusWeeks(1);
		return currentDate.format(DATE_FORMATTER);
	}
}
