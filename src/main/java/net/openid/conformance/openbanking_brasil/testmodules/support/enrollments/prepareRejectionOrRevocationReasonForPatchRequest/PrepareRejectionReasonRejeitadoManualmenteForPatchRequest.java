package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareRejectionOrRevocationReasonForPatchRequest;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentRejectionReasonEnum;
import net.openid.conformance.testmodule.Environment;

public class PrepareRejectionReasonRejeitadoManualmenteForPatchRequest extends AbstractCondition {

	@Override
	@PostEnvironment(strings = "rejection_reason")
	public Environment evaluate(Environment env) {
		env.putString("rejection_reason", EnrollmentRejectionReasonEnum.REJEITADO_MANUALMENTE.toString());
		env.removeNativeValue("revocation_reason");
		logSuccess("rejectionReason is going to be set as REJEITADO_MANUALMENTE in the PATCH request body");
		return env;
	}
}
