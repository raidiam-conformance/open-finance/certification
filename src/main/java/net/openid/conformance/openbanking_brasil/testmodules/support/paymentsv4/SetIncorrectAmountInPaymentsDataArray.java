package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Random;

public class SetIncorrectAmountInPaymentsDataArray extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");
		JsonObject consentRequest = resource.getAsJsonObject("brazilPaymentConsent");
		JsonObject paymentRequest = resource.getAsJsonObject("brazilPixPayment");

		String consentAmount = OIDFJSON.getString(consentRequest
			.getAsJsonObject("data")
			.getAsJsonObject("payment")
			.get("amount"));
		log("Previous amount: " + consentAmount);

		String paymentAmount = generateDifferentAmount(consentAmount);

		JsonArray data = paymentRequest.getAsJsonArray("data");
		for (JsonElement paymentElement : data) {
			JsonObject payment = paymentElement.getAsJsonObject().getAsJsonObject("payment");
			payment.addProperty("amount", paymentAmount);
		}

		logSuccess("Successfully set the amount in the payment request to differ from the consent", paymentRequest);

		return env;
	}

	private static String generateDifferentAmount(String originalAmount) {
		Random random = new Random();
		double randomValue = Double.parseDouble(originalAmount) + random.nextDouble() + 0.01d;
		return String.format("%.2f", randomValue);
	}
}
