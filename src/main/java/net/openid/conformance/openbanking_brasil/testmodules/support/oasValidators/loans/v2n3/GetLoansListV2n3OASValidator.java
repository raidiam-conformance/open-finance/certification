package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public class GetLoansListV2n3OASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/loans/loans-v2.3.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/contracts";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


}
