package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.accounts.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.List;

public class GetPersonalAccountsOASValidatorV1 extends AbstractGetOpenDataAccountsOASValidatorV1 {

	@Override
	protected String apiPrefix() {
		return "personal";
	}

	@Override
	protected void assertSpecificConstraints(JsonObject obj) {
		String type = OIDFJSON.getString(obj.get("type"));
		List<String> mandatoryTypes = List.of("CONTA_DEPOSITO_A_VISTA", "CONTA_POUPANCA");

		JsonObject fees = obj.getAsJsonObject("fees");

		if(mandatoryTypes.contains(type) && !isPresent(fees, "priorityServices")) {
			throw error(String.format("priorityServices is required when type is %s", mandatoryTypes));
		}
	}
}
