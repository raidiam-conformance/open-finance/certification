package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.PostEnrollmentsFidoRegistrationErrorEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasChallengeInvalido extends AbstractEnsureErrorResponseCodeFieldWas {

    @Override
    protected List<String> getExpectedCodes() {
        return List.of(PostEnrollmentsFidoRegistrationErrorEnum.CHALLENGE_INVALIDO.toString());
    }
}
