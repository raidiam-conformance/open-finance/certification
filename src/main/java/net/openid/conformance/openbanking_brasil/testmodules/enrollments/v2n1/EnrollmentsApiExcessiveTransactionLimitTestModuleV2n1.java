package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiExcessiveTransactionLimitTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.addNfcHeader.AddNfcHeaderAsFalse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateFidoAuthorizeRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateFidoAuthorizeRequestBodyWithEmptyUserHandleToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.GetEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostFidoRegistrationOptionsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostFidoSignOptionsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_excessive-transactionLimit_test-module_v2-1",
	displayName = "enrollments_api_excessive-transactionLimit_test-module_v2-1",
	summary = "Ensure payment cannot be executed if the amount is over the expected transactionLimit\n" +
		"• GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array\n" +
		"• Execute a full enrollment journey sending the origin extracted above, extract the enrollment ID, and store the refresh_token generated ensuring it has the nrp-consents scope\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AUTHORISED\", and extract the transactionLimit sent\n" +
		"• Call POST consent with the amount as transactionLimit+1\n" +
		"• Expects 201 - Validate response\n" +
		"• Call the POST sign-options endpoint with the consentID created\n" +
		"• Expect 201 - Validate response and extract challenge\n" +
		"• Call POST consent authorise, signing the challenge with the compliant private key, and sending the x-bcb-nfc header as false, and sending the userHandle field as an empty string\n" +
		"• Expect 204\n" +
		"• Call POST Pix Payment Endpoint\n" +
		"• Expect 201 or 422 VALOR_ACIMA_LIMITE - Validate response\n" +
		"\nIf response is 201:\n" +
		"• Poll GET Payments Endpoint while status is RCVD, ACCP or ACPD\n" +
		"• Call Get Pix Payment Endpoint\n" +
		"• Expect 201 - Validate response and check payment status is RJCT and rejectionReason is VALOR_ACIMA_LIMITE",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
	}
)
public class EnrollmentsApiExcessiveTransactionLimitTestModuleV2n1 extends AbstractEnrollmentsApiExcessiveTransactionLimitTestModule {

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postFidoSignOptionsValidator() {
		return PostFidoSignOptionsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetPaymentConsentsV4Endpoint.class), condition(GetPaymentV4Endpoint.class));
	}

	@Override
	protected ConditionSequence createConsentsAuthoriseSteps() {
		return super.createConsentsAuthoriseSteps()
			.insertAfter(AddIdempotencyKeyHeader.class, condition(AddNfcHeaderAsFalse.class))
			.replace(CreateFidoAuthorizeRequestBodyToRequestEntityClaims.class, condition(CreateFidoAuthorizeRequestBodyWithEmptyUserHandleToRequestEntityClaims.class));
	}

	@Override
	public void cleanup() {
		//not required
	}
}
