package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1.GetBankFixedIncomesBalancesOASValidatorV1;

public class GetBankFixedIncomesBalancesOASValidatorV1n4 extends GetBankFixedIncomesBalancesOASValidatorV1 {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/bank-fixed-incomes/bank-fixed-incomes-v1.0.4.yml";
	}

}
