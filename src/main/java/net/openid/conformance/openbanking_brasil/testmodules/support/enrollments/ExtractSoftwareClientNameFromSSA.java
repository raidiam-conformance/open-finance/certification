package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class ExtractSoftwareClientNameFromSSA extends AbstractCondition {


	@Override
	@PreEnvironment(required = "software_statement_assertion")
	@PostEnvironment(strings = "software_client_name")
	public Environment evaluate(Environment env) {
		String clientName = Optional.ofNullable(env.getObject("software_statement_assertion").get("claims"))
			.map(JsonElement::getAsJsonObject)
			.map(claims -> claims.get("software_client_name"))
			.map(JsonElement::getAsString)
			.orElseThrow(() -> error("There is no software_client_name field inside the SSA"));

		if (clientName.isEmpty()) {
			throw error("software_client_name is empty");
		}

		env.putString("software_client_name", clientName);
		logSuccess("Successfully extracted the SSA software_client_name field");

		return env;
	}
}
