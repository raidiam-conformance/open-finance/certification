package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractApiWrongPermissionsTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.PrepareUrlForDiscountedRoot;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.PrepareUrlForFetchingCreditDiscountedCreditRightsContract;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.PrepareUrlForFetchingCreditDiscountedCreditRightsContractGuarantees;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.PrepareUrlForFetchingCreditDiscountedCreditRightsContractInstalments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.PrepareUrlForFetchingCreditDiscountedCreditRightsContractPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.CreditDiscountedCreditRightsSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvoiceFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractCreditOperationsDiscountedApiWrongPermissionsTestModule extends AbstractApiWrongPermissionsTestModulePhase2 {

	@Override
	protected abstract Class<? extends Condition> apiResourceContractListResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractGuaranteesResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractPaymentsResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractInstallmentsResponseValidator();

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.INVOICE_FINANCINGS;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetInvoiceFinancingsV2Endpoint.class)
		);
	}

	@Override
	protected Class<? extends Condition> contractSelector() {
		return CreditDiscountedCreditRightsSelector.class;
	}


	@Override
	protected Class<? extends Condition> prepareUrlForFetchingRootEndpoint() {
		return PrepareUrlForDiscountedRoot.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractEndpoint() {
		return PrepareUrlForFetchingCreditDiscountedCreditRightsContract.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractWarrantiesEndpoint() {
		return PrepareUrlForFetchingCreditDiscountedCreditRightsContractGuarantees.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractPaymentsEndpoint() {
		return PrepareUrlForFetchingCreditDiscountedCreditRightsContractPayments.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractScheduledInstalmentsEndpoint() {
		return PrepareUrlForFetchingCreditDiscountedCreditRightsContractInstalments.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
