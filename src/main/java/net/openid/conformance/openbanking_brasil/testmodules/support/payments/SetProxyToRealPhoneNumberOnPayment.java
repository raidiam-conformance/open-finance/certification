package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProxyToRealPhoneNumber;
import net.openid.conformance.testmodule.Environment;

/**
 * @deprecated
 * To set proxy as real phone number on payment request use {@link SetProxyToRealPhoneNumber}
 */
@Deprecated
public class SetProxyToRealPhoneNumberOnPayment extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonObject obj = env.getObject("resource");
		obj = obj.getAsJsonObject("brazilPixPayment");
		obj = obj.getAsJsonObject("data");
		obj.addProperty("proxy", DictHomologKeys.PROXY_PHONE_NUMBER);

		logSuccess("Added real email address as proxy to payment");

		return env;
	}

}
