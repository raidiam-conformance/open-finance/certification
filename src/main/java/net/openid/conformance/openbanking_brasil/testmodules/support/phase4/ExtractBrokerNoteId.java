package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class ExtractBrokerNoteId extends AbstractCondition {


	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	@PostEnvironment(strings = "brokerNoteId")
	public Environment evaluate(Environment env) {
		String bodyString = OIDFJSON.getString(Optional.ofNullable(env.getObject(RESPONSE_ENV_KEY).get("body"))
			.orElseThrow(() -> error("No body in the response")));
		JsonArray data = JsonParser.parseString(bodyString).getAsJsonObject().get("data").getAsJsonArray();
		if (data.isJsonNull() || data.isEmpty()) {
			throw error("data array does not have any investment");
		}
		for (JsonElement transaction : data) {
			String brokerNoteId = OIDFJSON.getString(Optional.ofNullable(transaction.getAsJsonObject()
				.get("brokerNoteId")).orElse(new JsonPrimitive("")));
			if (!brokerNoteId.equals("")) {
				env.putString("brokerNoteId", brokerNoteId);
				logSuccess("brokerNoteId found", args("brokerNoteId", env.getString("brokerNoteId")));
				return env;
			}
		}
		throw error("No brokerNoteId found");
	}
}
