package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import org.springframework.http.HttpMethod;

public class PostFidoRegistrationOASValidatorV2 extends AbstractEnrollmentsOASValidatorV2 {

	@Override
	protected String getEndpointPathSuffix() {
		return "/{enrollmentId}/fido-registration";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}
}
