package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum AuthorisationFlowEnumV3 {

	HYBRID_FLOW, CIBA_FLOW, FIDO_FLOW;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}


}
