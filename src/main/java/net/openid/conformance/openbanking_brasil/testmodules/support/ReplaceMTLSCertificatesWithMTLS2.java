package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.util.PEMFormatter;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Map;

public class ReplaceMTLSCertificatesWithMTLS2 extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String certString = env.getString("config", "mtls2.cert");
		String keyString = env.getString("config", "mtls2.key");
		JsonObject alias = (JsonObject) env.getElementFromObject("config", "mtls2.mtls_alias");
		String caString = env.getString("config", "mtls2.ca");

		if (Strings.isNullOrEmpty(certString) || Strings.isNullOrEmpty(keyString) || Strings.isNullOrEmpty(caString)) {
			throw error("mtls2: one or more of the following are missing: cert, key, ca");
		}

		try {
			certString = PEMFormatter.stripPEM(certString);

			keyString = PEMFormatter.stripPEM(keyString);

			caString = PEMFormatter.stripPEM(caString);
		} catch (IllegalArgumentException e) {
			throw error("Couldn't decode certificate, key, or CA chain from Base64", e, args("cert", certString, "key", keyString, "ca", caString));
		}

		JsonObject mtls = new JsonObject();
		mtls.addProperty("cert", certString);
		mtls.addProperty("key", keyString);
		mtls.addProperty("ca", caString);
		if(alias != null) {
			mtls.add("mtls_alias", alias);
			log("Adding mtls alias", Map.of("alias", OIDFJSON.getString(alias.get("key"))));
		}
		env.putObject("config", "mtls", mtls);

		logSuccess("Replaced MTLS with mtls2");
		return env;
	}
}
