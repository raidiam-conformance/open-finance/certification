package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class CreateIndefiniteConsentExpiryTime extends AbstractCondition {
	private final static String CONSENT_INDEFINITE_EXPIRATION_TIME = "2300-01-01T00:00:00Z";
	@Override
	@PreEnvironment(required = "consent_endpoint_request" )
	@PostEnvironment(required = "consent_endpoint_request")
	public Environment evaluate(Environment env) {

		JsonObject consentRequest = env.getObject("consent_endpoint_request");
		JsonObject data = Optional.ofNullable(consentRequest.getAsJsonObject("data"))
				.orElseThrow(() -> error("Consent Endpoint Request does not have data field"));

		data.addProperty("expirationDateTime", CONSENT_INDEFINITE_EXPIRATION_TIME);
		env.putString("consent_extension_expiration_time", CONSENT_INDEFINITE_EXPIRATION_TIME);

		logSuccess("Added indefinite expiration time to consent request", args("consent_endpoint_request", consentRequest));

		return env;
	}
}
