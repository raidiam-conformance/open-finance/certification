package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class GetConsentV2AndPaymentConsentV3Endpoints extends AbstractGetEndpointFromAuthServer {
	@Override
	@PreEnvironment(required = {"authorisation_server","config"})
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("config").getAsJsonObject("resource");
		String consentUrl;
		String paymentConsentUrl;

			consentUrl = getApiEndpoint(env, "consents", "^(2.[0-9].[0-9])$","^(https:\\/\\/)(.*?)(\\/open-banking\\/consents\\/v\\d+\\/consents)$");
			paymentConsentUrl = getApiEndpoint(env, "payments-consents", "^(3.[0-9].[0-9])$","^(https:\\/\\/)(.*?)(\\/open-banking\\/payments\\/v\\d+\\/consents)$");

		if (consentUrl == null && paymentConsentUrl == null) {
			throw error("Unable to locate payment consent endpoint and consent endpoint");
		}
		if (consentUrl == null) {
			consentUrl = paymentConsentUrl;
			logSuccess("Only found payment consent endpoint, setting both consent endpoints to payment consent endpoint", args("paymentConsentUrl", paymentConsentUrl));
		}
		if (paymentConsentUrl == null) {
			paymentConsentUrl = consentUrl;
			logSuccess("Only found consent endpoint, setting second both endpoints to consents endpoint", args("consentUrl", consentUrl));
		}
		resource.addProperty("consentUrl", consentUrl);
		resource.addProperty("consentUrl2", paymentConsentUrl);
		logSuccess("Inserted consent endpoints into config", args("consentUrl", consentUrl, "consentUrl2", paymentConsentUrl));
		return env;
	}

}
