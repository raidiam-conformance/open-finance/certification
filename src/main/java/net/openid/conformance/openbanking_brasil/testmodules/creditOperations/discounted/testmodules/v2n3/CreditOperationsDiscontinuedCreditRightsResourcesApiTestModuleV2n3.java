package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.AbstractCreditOperationsDiscontinuedCreditRightsResourcesApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsListV2n3OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "invoice-financings_api_resources_test-module_v2-3",
	displayName = "Validate structure of discounted credit rights API and Resources API resources",
	summary = "Makes sure that the Resource API and the API that is the scope of this test plan are returning the same available IDs\n" +
		"\u2022Create a consent with all the permissions needed to access the tested API\n" +
		"\u2022 Expects server to create the consent with 201\n" +
		"\u2022 Redirect the user to authorize at the financial institution\n" +
		"\u2022 Call the tested resource API\n" +
		"\u2022 Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the id provided by this API\n" +
		"\u2022 Call the resources API\n" +
		"\u2022 Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class CreditOperationsDiscontinuedCreditRightsResourcesApiTestModuleV2n3 extends AbstractCreditOperationsDiscontinuedCreditRightsResourcesApiTestModule {

	@Override
	protected Class<? extends Condition> apiValidator() {
		return GetInvoiceFinancingsListV2n3OASValidator.class;
	}

}
