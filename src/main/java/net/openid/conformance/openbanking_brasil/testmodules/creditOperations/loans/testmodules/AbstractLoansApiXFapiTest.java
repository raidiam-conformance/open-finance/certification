package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.AbstractCreditOperationsXFapiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetLoansV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractLoansApiXFapiTest extends AbstractCreditOperationsXFapiTestModule {

	@Override
	protected OPFCategoryEnum getCategory() {
		return OPFCategoryEnum.CREDIT_OPERATIONS;
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.LOANS;
	}

	@Override
	protected EnumResourcesType getApiType() {
		return EnumResourcesType.LOAN;
	}

	@Override
	protected Class<? extends Condition> getGetConsentValidator() {
		return GetConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetLoansV2Endpoint.class)
		);
	}
}
