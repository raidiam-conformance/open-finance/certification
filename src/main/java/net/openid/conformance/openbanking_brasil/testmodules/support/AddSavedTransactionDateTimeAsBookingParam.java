package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AddSavedTransactionDateTimeAsBookingParam extends AbstractAddBookingDateParameters {

	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	@Override
	@PreEnvironment(strings = {"accountId","transactionDateTime"})
	@PostEnvironment(strings = {"fromBookingDate","toBookingDate"})
	public Environment evaluate(Environment env) {

		String transactionDateTime = env.getString("transactionDateTime");

		String fromBookingDate = parseStringToDateString(transactionDateTime);
		String toBookingDate = parseStringToDateString(transactionDateTime);

		addBookingDateParamsToAccountsEndpoint(env, fromBookingDate, toBookingDate,"/accounts/%s/transactions?fromBookingDate=%s&toBookingDate=%s");

		return super.evaluate(env);
	}

	protected LocalDate parseStringToDate(String date) {
		return LocalDateTime.parse(date, DATE_TIME_FORMATTER).toLocalDate();
	}
	protected String parseStringToDateString(String date) {
		return parseStringToDate(date).format(DATE_FORMATTER);
	}

}

