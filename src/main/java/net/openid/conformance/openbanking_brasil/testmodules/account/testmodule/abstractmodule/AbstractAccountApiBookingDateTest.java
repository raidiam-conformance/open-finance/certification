package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountResource;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AccountSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBookingDate6MonthsOlderThanCurrent;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBookingDateSixMonthsBefore;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddSavedTransactionDateTimeAsBookingParam;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromBookingDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckExpectedBookingDateTimeResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.CopyResourceEndpointResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateTransactionWithinRangeV2n;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public abstract class AbstractAccountApiBookingDateTest extends AbstractPhase2TestModule {

	protected abstract Class<? extends Condition> getAccountListValidator();
	protected abstract Class<? extends Condition> getAccountIdentificationResponseValidator();
	protected abstract Class<? extends Condition> getAccountTransactionsValidator();


	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");


	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class),
			condition(GetAccountsV2Endpoint.class));
	}

	@Override
	protected void configureClient() {
		super.configureClient();
	}
	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ACCOUNTS).addScopes(OPFScopesEnum.ACCOUNTS, OPFScopesEnum.OPEN_ID).build();
	}
	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(getAccountListValidator(), Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(AccountSelector.class);
		callAndStopOnFailure(PrepareUrlForFetchingAccountResource.class);
		preCallProtectedResource("Fetch Account");
		callAndContinueOnFailure(getAccountIdentificationResponseValidator(), Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(PrepareUrlForFetchingAccountTransactions.class);
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromBookingDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toBookingDate", currentDate.format(FORMATTER));
		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);
		preCallProtectedResource("Fetch Account transactions");
		callAndStopOnFailure(CopyResourceEndpointResponse.class);
		env.mapKey("full_range_response", "resource_endpoint_response_full_copy");

		eventLog.startBlock("Add booking date query parameters");
		callAndContinueOnFailure(AddBookingDateSixMonthsBefore.class, Condition.ConditionResult.FAILURE);
		preCallProtectedResource("Fetch Account transactions with query parameters");
		eventLog.startBlock("Validating random transaction returned");
		callAndStopOnFailure(CheckExpectedBookingDateTimeResponse.class);
		callAndStopOnFailure(ValidateTransactionWithinRangeV2n.class);
		call(accountTransactionsValidationSequence());

		eventLog.startBlock("Add booking date query parameters");
		callAndStopOnFailure(AddBookingDate6MonthsOlderThanCurrent.class);
		preCallProtectedResource("Fetch Account transactions with query parameters");
		eventLog.startBlock("Validating random transaction returned");
		callAndStopOnFailure(CheckExpectedBookingDateTimeResponse.class);
		callAndStopOnFailure(ValidateTransactionWithinRangeV2n.class);
		call(accountTransactionsValidationSequence());

		eventLog.startBlock("Add booking date query parameters using value from transaction returned");
		callAndStopOnFailure(AddSavedTransactionDateTimeAsBookingParam.class);
		preCallProtectedResource("Fetch Account transactions with query parameters");
		eventLog.startBlock("Validating random transaction returned");
		callAndStopOnFailure(CheckExpectedBookingDateTimeResponse.class);
		callAndStopOnFailure(ValidateTransactionWithinRangeV2n.class);
		call(accountTransactionsValidationSequence());
	}

	private ConditionSequence accountTransactionsValidationSequence(){
		return sequenceOf(
			condition(getAccountTransactionsValidator()).dontStopOnFailure()
		);
	}
}
