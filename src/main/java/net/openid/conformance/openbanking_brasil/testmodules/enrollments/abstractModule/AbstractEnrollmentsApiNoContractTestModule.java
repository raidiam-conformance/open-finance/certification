package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.EnsureNoContractClientIdIsPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ReplaceClientIdForUnauthorizedClientIdInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsSteps;

public abstract class AbstractEnrollmentsApiNoContractTestModule extends AbstractEnrollmentsApiTestModule {

	@Override
	protected void configureClient() {
		callAndStopOnFailure(EnsureNoContractClientIdIsPresentInConfig.class);
		super.configureClient();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		postAndValidateEnrollments();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected void postAndValidateEnrollments() {
		callAndStopOnFailure(ReplaceClientIdForUnauthorizedClientIdInConfig.class);
		call(createPostEnrollmentsSteps());
		validateEnrollmentsResponse();
	}

	@Override
	protected PostEnrollmentsSteps createPostEnrollmentsSteps() {
		return new PostEnrollmentsSteps(addTokenEndpointClientAuthentication, true);
	}

	protected void validateEnrollmentsResponse() {
		runInBlock("Validate POST enrollments response", () -> {
			callAndContinueOnFailure(EnsureResourceResponseCodeWas403.class, Condition.ConditionResult.FAILURE);
			validateResourceResponse(postEnrollmentsValidator());
		});
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return null;
	}

	@Override
	protected void executeTestSteps() {}
}
