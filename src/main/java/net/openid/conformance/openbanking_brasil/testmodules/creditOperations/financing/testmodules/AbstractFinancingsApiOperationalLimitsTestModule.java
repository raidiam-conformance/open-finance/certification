package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractApiOperationalLimitsTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractInstallmentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractPaymentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractWarrantiesResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureSpecificCreditOperationsPermissionsWereReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractFinancingsApiOperationalLimitsTestModule extends AbstractApiOperationalLimitsTestModulePhase2 {

	@Override
	protected abstract Class<? extends Condition> contractsResponseValidator();

	@Override
	protected abstract Class<? extends Condition> contractIdValidator();

	@Override
	protected abstract Class<? extends Condition> contractWarrantiesValidator();

	@Override
	protected abstract Class<? extends Condition> contractScheduledInstalmentsValidator();

	@Override
	protected abstract Class<? extends Condition> contractPaymentsValidator();

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getResourceEndpoint() {
		return GetFinancingsV2Endpoint.class;
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.FINANCINGS;
	}

	@Override
	protected EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType getPermissionType() {
		return EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType.FINANCINGS;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractId() {
		return PrepareUrlForFetchingFinancingContractResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingWarranties() {
		return PrepareUrlForFetchingFinancingContractWarrantiesResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingScheduledInstalments() {
		return PrepareUrlForFetchingFinancingContractInstallmentsResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingPayments() {
		return PrepareUrlForFetchingFinancingContractPaymentsResource.class;
	}

}
