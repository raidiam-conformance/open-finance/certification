package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class RemoveWebhookFromClientConfigurationRequest extends AbstractCondition {
	@Override
	@PreEnvironment(required = { "registration_client_endpoint_request_body" })
	@PostEnvironment(required = "registration_client_endpoint_request_body")
	public Environment evaluate(Environment env) {
		JsonObject request = env.getObject("registration_client_endpoint_request_body");

		request.remove("webhook_uris");

		env.putObject("registration_client_endpoint_request_body", request);

		log("Removed webhook URIs from client configuration request",
			args("client_configuration_request", request));

		return env;
	}
}
