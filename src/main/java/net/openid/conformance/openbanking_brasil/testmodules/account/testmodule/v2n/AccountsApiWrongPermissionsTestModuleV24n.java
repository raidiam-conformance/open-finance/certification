package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule.AbstractAccountsApiWrongPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountTransactionsOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsBalancesOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsIdentificationOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsLimitsOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsListOASValidatorV2n4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "accounts_api_wrong-permissions_test-module_v2-4",
	displayName = "Ensures API resource cannot be called with wrong permissions",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a Consent with the accounts permission group (\"ACCOUNTS_READ\", \"ACCOUNTS_BALANCES_READ\", \"RESOURCES_READ\", \"ACCOUNTS_TRANSACTIONS_READ\", \"ACCOUNTS_OVERDRAFT_LIMITS_READ\")\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET Accounts API V2\n" +
		"\u2022 Expects a 200 response \n" +
		"\u2022 Calls GET Accounts API V2 specifying an account ID\n" +
		"\u2022 Expects a 200 response \n" +
		"\u2022 Calls GET Accounts Balances API V2 specifying an account ID\n" +
		"\u2022 Expects a 200 response \n" +
		"\u2022 Calls GET Accounts Limits API V2 specifying an account ID\n" +
		"\u2022 Expects a 200 response \n" +
		"\u2022 Calls GET Accounts Transactions API V2 specifying an account ID\n" +
		"\u2022 Expects a 200 response \n" +
		"\u2022 Creates a Consent with the customer business and personal permissions group (\"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\", \"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\", \"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\", \"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\", \"RESOURCES_READ\")\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET Accounts API V2\n" +
		"\u2022 Expects a 403 response \n" +
		"\u2022 Calls GET Accounts API V2 specifying an account ID\n" +
		"\u2022 Expects a 403 response \n" +
		"\u2022 Calls GET Accounts Balances API V2 specifying an account ID\n" +
		"\u2022 Expects a 403 response \n" +
		"\u2022 Calls GET Accounts Limits API V2 specifying an account ID\n" +
		"\u2022 Expects a 403 response \n" +
		"\u2022 Calls GET Accounts Transactions API V2 specifying an account ID\n" +
		"\u2022 Expects a 403 response ",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class AccountsApiWrongPermissionsTestModuleV24n extends AbstractAccountsApiWrongPermissionsTestModule {

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getAccountListValidator() {
		return GetAccountsListOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getAccountBalancesValidator() {
		return GetAccountsBalancesOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getAccountIdentificationValidator() {
		return GetAccountsIdentificationOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getAccountLimitsValidator() {
		return GetAccountsLimitsOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getAccountTransactionValidator() {
		return GetAccountTransactionsOASValidatorV2n4.class;
	}
}
