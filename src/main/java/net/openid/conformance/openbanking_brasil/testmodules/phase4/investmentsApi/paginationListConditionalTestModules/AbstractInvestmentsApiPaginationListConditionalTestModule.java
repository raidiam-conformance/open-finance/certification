package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.AbstractInvestmentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize1000;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize25;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.EnsureNumberOfTotalRecordsIsAtLeast51FromMeta;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.EnsureInvestmentsPaginationListCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentsRoot;

public abstract class AbstractInvestmentsApiPaginationListConditionalTestModule extends AbstractInvestmentsApiTestModule {

    @Override
    protected void setupResourceEndpoint() {
        super.setupResourceEndpoint();
        env.putBoolean("continue_test", true);
        callAndContinueOnFailure(EnsureInvestmentsPaginationListCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
        if (!env.getBoolean("continue_test")) {
            fireTestSkipped("Test skipped since no Pagination List CPF/CNPJ was informed.");
        }
    }

    @Override
    protected void requestProtectedResource() {
        runInBlock("Call GET Investments List endpoint with page-size equal to 1000 - Expects 200 status code and totalRecords > 51", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingInvestmentsRoot.class);
            callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);
            preCallProtectedResource();
            callAndContinueOnFailure(investmentsRootValidator(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(EnsureNumberOfTotalRecordsIsAtLeast51FromMeta.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("Call GET Investments List endpoint with page-size equal to 25 - Expects 200 status code, 25 investments and valid links logic", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingInvestmentsRoot.class);
            callAndStopOnFailure(SetProtectedResourceUrlPageSize25.class);
            preCallProtectedResource();
            callAndContinueOnFailure(investmentsRootValidator(), Condition.ConditionResult.FAILURE);
        });

        runInBlock("Call GET Investments List endpoint with the next link returned on the previous call - Expects 200 status code, 25 investments and valid links logic", () -> {
            callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);
            preCallProtectedResource();
            callAndContinueOnFailure(investmentsRootValidator(), Condition.ConditionResult.FAILURE);
        });
    }

    @Override
    protected void executeParticularTestSteps() {}
}
