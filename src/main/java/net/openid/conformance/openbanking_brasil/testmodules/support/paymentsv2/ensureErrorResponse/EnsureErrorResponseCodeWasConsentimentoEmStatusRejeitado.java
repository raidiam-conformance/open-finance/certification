package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse;

import net.openid.conformance.openbanking_brasil.testmodules.support.enums.consentsV3.DeleteConsent422ErrorEnumV3;

import java.util.List;

public class EnsureErrorResponseCodeWasConsentimentoEmStatusRejeitado extends AbstractEnsureErrorResponseCodeFieldWas {

	@Override
	protected List<String> getExpectedCodes() {
		return List.of(DeleteConsent422ErrorEnumV3.CONSENTIMENTO_EM_STATUS_REJEITADO.toString());
	}
}
