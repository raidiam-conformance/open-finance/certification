package net.openid.conformance.openbanking_brasil.testmodules.phase4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.ValidateConsentsOperationalFieldV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckV2Module;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "consents_api_preflight-adv_test-module_v2",
	displayName = "consents_api_preflight-adv_test-module_v2",
	summary = "Test confirms that the mandatory configuration fields have been provided and that all required registration has been correctly done on the participant directory.\n" +
		"• Extract all the organisation information for the organization_id that is provided\n" +
		"• Validate that the well-known provided is present on the participants API dump for the extracted organisation\n" +
		"• Validate that a Consents API URI is present for this well-known - Display this URI, which will be used for the tests\n" +
		"• Obtain a SSA with the transport certificated and the client_id provided by the organisation\n" +
		"• Validate that the field brazilCpf has been provided on the test configuration\n" +
		"• Validate that the field brazilCpf for Operational Limits has been provided on the test configuration",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf",
		"resource.brazilCnpj",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness",
        "directory.client_id"
	}
)

public class PreFlightCheckV4Module extends PreFlightCheckV2Module {

	@Override
	protected void runTests() {
		super.runTests();
		runInBlock("Pre-flight Consent operational field check", () -> {
			callAndContinueOnFailure(ValidateConsentsOperationalFieldV2.class, Condition.ConditionResult.FAILURE);
		});
	}
}
