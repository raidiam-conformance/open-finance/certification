package net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation;

public class EnsureConsentRejectAspspMaxDateReached extends AbstractConsentRejectionValidation {

	@Override
	protected String getRejectionReasonCode() {
		return "CONSENT_MAX_DATE_REACHED";
	}

	@Override
	protected String getRejectedBy() {
		return "ASPSP";
	}
}
