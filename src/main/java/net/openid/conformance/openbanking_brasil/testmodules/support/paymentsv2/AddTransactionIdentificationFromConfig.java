package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class AddTransactionIdentificationFromConfig extends AbstractCondition {

	@Override
	public final Environment evaluate(Environment env) {
		String transactionIdentifierString = OIDFJSON.getString(
			Optional.ofNullable(env.getElementFromObject("config", "resource.transactionIdentifier"))
				.orElseThrow(() -> error("Transaction Identifier field was null")));
		log("Setting transaction identification to a new value");
		JsonObject resource = env.getObject("resource");
		JsonElement paymentRequest = Optional.ofNullable(resource.getAsJsonObject("brazilPixPayment").get("data"))
			.orElseThrow(() -> error ("Could not find data inside brazilPixPayment"));

		if (paymentRequest.isJsonArray()) {
			JsonArray payments = paymentRequest.getAsJsonArray();
			for (int i = 0; i < payments.size(); i++) {

				JsonObject payment = payments.get(i).getAsJsonObject();
				payment.addProperty("transactionIdentification", transactionIdentifierString);
				log(payment);
			}

		} else {

			paymentRequest.getAsJsonObject().addProperty("transactionIdentification", transactionIdentifierString);
			logSuccess("Successfully added TransactionIdentification from config", paymentRequest.getAsJsonObject());

		}

		return env;

	}
}

