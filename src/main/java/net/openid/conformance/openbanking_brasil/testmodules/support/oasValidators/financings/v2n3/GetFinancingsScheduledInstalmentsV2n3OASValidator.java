package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

import java.util.List;


public class GetFinancingsScheduledInstalmentsV2n3OASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/financings/financings-v2.3.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/contracts/{contractId}/scheduled-instalments";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		assertData(body.getAsJsonObject("data"));
	}

	private void assertData(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"totalNumberOfInstalments",
			"typeNumberOfInstalments",
			List.of("DIA", "SEMANA", "MES", "ANO")
		);
	}
}
