package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.AbstractPostConsentOASValidator;

@ApiName("Post Consent V3")
public class PostConsentOASValidatorV3 extends AbstractPostConsentOASValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/consents/swagger-consents-3.0.1.yml";
	}
}
