package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.text.ParseException;
import java.util.Optional;

public class ExtractRecurringPaymentIdToPaymentIdArray extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	@PostEnvironment(required = "recurringPaymentIds")
	public Environment evaluate(Environment env) {
		JsonObject response = extractResponseFromEnv(env);
		String recurringPaymentId = extractRecurringPaymentIdFromResponse(response);
		JsonObject recurringPaymentIds = extractRecurringPaymentIdsObjectFromEnv(env);
		JsonArray recurringPaymentIdsArray = recurringPaymentIds.getAsJsonArray("recurringPaymentIdsArray");
		recurringPaymentIdsArray.add(recurringPaymentId);
		env.putObject("recurringPaymentIds", recurringPaymentIds);
		logSuccess("The recurringPaymentId has been extracted from the payment and added to the array",
			args("recurringPaymentIds", recurringPaymentIdsArray));

		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	private String extractRecurringPaymentIdFromResponse(JsonObject response) {
		return Optional.ofNullable(response.getAsJsonObject("data"))
			.map(data -> data.get("recurringPaymentId"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find data.recurringPaymentId in the response", args("response", response)));
	}

	private JsonObject extractRecurringPaymentIdsObjectFromEnv(Environment env) {
		return Optional.ofNullable(env.getObject("recurringPaymentIds"))
			.orElse(createNewRecurringPaymentIdsObject());
	}

	private JsonObject createNewRecurringPaymentIdsObject() {
		JsonArray recurringPaymentIdsArray = new JsonArray();
		return new JsonObjectBuilder().addField("recurringPaymentIdsArray", recurringPaymentIdsArray).build();
	}
}
