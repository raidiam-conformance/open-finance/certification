package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiMultipleConsentsNoFundsConditionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasSaldoInsuficienteOrValorAcimaLimiteV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_multiple-consents-no-funds-conditional_test-module_v4",
	displayName = "Payments API multiple consents no funds conditional test module",
	summary = "This test module should be executed only by institutions that currently support consents with multiple accounts, in case the institution does not support this feature the ‘Payment consent - Logged User CPF - Multiple Consents Test' and 'Payment consent - Business Entity CNPJ - Multiple Consents Test’ should be left empty, which in turn will make the test return a SKIPPED\n" +
		"This test module validates that the payment will reach a RJCT for payments without limit\n" +
		"\u2022 Validates if the user has provided the field “Payment consent - Logged User CPF - Multiple Consents Test”. If field is not provided the whole test scenario must be SKIPPED\n" +
		"\u2022 Set the payload to be customer business if Payment consent - Business Entity CNPJ - Multiple Consents Test was provided. If left blank, it should be customer personal\n" +
		"\u2022 Creates consent request_body with valid email proxy (cliente-a00001@pix.bcb.gov.br) and its standardized payload, set the Payment consent - Logged User CPF - Multiple Consents Test” on the loggedUser identification field, and  the amount to be 9999999999.99, Debtor account should not be sent\n" +
		"\u2022 Call the POST Consents API\n" +
		"\u2022 Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"\u2022 Redirects the user to authorize the created consent - Expect Failure\n" +
		"\u2022 Ensure an error is returned and that the authorization code was not sent back\n" +
		"\u2022 Call the GET Consents API\n" +
		"\u2022 Expects 200 - Validate response - Check if Status is REJECTED and rejectionReason is SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE",

	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"conditionalResources.brazilCpfJointAccount",
		"conditionalResources.brazilCnpjJointAccount",
	}
)
public class PaymentsApiMultipleConsentsNoFundsConditionalTestModuleV4 extends AbstractPaymentsApiMultipleConsentsNoFundsConditionalTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getEnsureConsentsRejectionReasonCode() {
		return EnsureConsentsRejectionReasonCodeWasSaldoInsuficienteOrValorAcimaLimiteV3.class;
	}
}
