package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.AbstractGetCreditFixedIncomesIdentificationOASValidator;

public class GetCreditFixedIncomesIdentificationV1OASValidator extends AbstractGetCreditFixedIncomesIdentificationOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/creditFixedIncomes/credit-fixed-incomes-v1.0.0.yml";
	}

}
