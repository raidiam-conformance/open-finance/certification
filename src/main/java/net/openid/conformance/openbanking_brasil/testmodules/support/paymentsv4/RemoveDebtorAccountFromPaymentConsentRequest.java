package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class RemoveDebtorAccountFromPaymentConsentRequest extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent.data"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Could not find payment consent data in the environment"));
		data.remove("debtorAccount");
		logSuccess("The \"debtorAccount\" field has been removed from the payment consent request payload",
			args("data", data));
		return env;
	}
}
