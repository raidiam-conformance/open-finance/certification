package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiInvalidScopeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringConsentOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_invalid-scope_test-module_v1",
	displayName = "automatic-payments_api_invalid-scope_test-module_v1",
	summary = "Ensure scope validations are being executed at the POST recurring-payments endpoint, and a payment cannot be created in unhappy requests\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts data \n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent, sending the scopes as scope=openid&recurring-payments&consent:{recurringConsentId} \n" +
		"• Check if an error is returned at the redirect, and no authorization code is sent back\n" +
		"If no errors are returned: \n" +
		"• Call the GET recurring-consents endpoint \n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED \n" +
		"• Call the POST recurring-payments endpoint using an auth code token with a incorrect scope, as requested at the redirect \n" +
		"• Expect 403 - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiInvalidScopeTestModuleV1 extends AbstractAutomaticPaymentsApiInvalidScopeTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return false;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV1Endpoint.class), condition(GetAutomaticPaymentPixRecurringV1Endpoint.class));
	}
}
