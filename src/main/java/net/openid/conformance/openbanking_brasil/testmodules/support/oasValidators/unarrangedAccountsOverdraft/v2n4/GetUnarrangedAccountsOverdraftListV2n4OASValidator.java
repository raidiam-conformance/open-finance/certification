package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.AbstractGetUnarrangedAccountsOverdraftListOASValidator;


public class GetUnarrangedAccountsOverdraftListV2n4OASValidator extends AbstractGetUnarrangedAccountsOverdraftListOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/unarrangedAccountsOverdraft/unarranged-accounts-overdraft-v2.4.0.yml";
	}
}
