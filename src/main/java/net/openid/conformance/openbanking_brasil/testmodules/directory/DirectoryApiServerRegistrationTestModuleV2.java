package net.openid.conformance.openbanking_brasil.testmodules.directory;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.GetDynamicServerConfiguration;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractBlockLoggingTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.EnsureScopesCorrespondToTheOrganisationRoles;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.ValidateAuthServerApiResources;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.ValidateAuthServerCertifications;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.ValidateAuthServerLogo;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.VariantParameters;

@PublishTestModule(
	testName = "directory_api_server-registration_test_module_v2",
	displayName = "Directory Registration Server Check - FVP",
	summary = "Certify that the Security Certification has been correctly added into the Authorisation Server\n" +
		"\u2022 Obtain the all valid Security Certifications published for the tested Authorisation Server\n" +
		"\u2022 Check on Authorisation Server Configurantion the  client authentication methods supported (mtls or private_key_jwt). Also confirm if server supports pushed authorization request (PAR) and if it mandatory or not.\n" +
		"\u2022 Validate if a published Security Certification correctly corresponds to each client authentication methods supported by the authorisation server\n" +
		"\u2022 Validate if the Certification URI value has the following structure (https:\\/\\/openid\\.net\\/wordpress-content\\/uploads\\/)(<year>)(\\/)(<month>\\/)(.*)(.zip) in accordance with the certification start date\n" +
		"Certify that the Functional Certification has been correctly registered into the Functional APIs\n" +
		"\u2022 Obtain the Api Resources for the tested Authorisation Server\n" +
		"\u2022 For all the ApiResources that have an ApiFamilyType of Phase 3 and that have the ApiVersion set as 2 validate if the Certification URI value has the following structure (https:\\/\\/github\\.com\\/OpenBanking-Brasil\\/conformance\\/)(blob|raw)(\\/main\\/submissions\\/functional\\/)(<api>\\/)((2|3)\\.\\d\\.\\d)(.*)(<month>)(-)(<year>)((\\.zip)|(\\.json)) in accordance with the certification start date\n" +
		"\u2022 For all the ApiResources that have an ApiFamilyType of Phase 2 and that have the ApiVersion set as 2 validate if the ApiCertificationUri value has the following structure (https:\\/\\/github\\.com\\/OpenBanking-Brasil\\/conformance\\/)(blob|raw)(\\/main\\/submissions\\/functional\\/)(<api>\\/)(2\\.\\d\\.\\d)(.*)(<month>)(-)(<year>)((\\.zip)|(\\.json)) in accordance with the certification start date\n" +
		"\u2022 Validate if the required endpoints have been registered - Check if the FamilyComplete type is set to False\n" +
		"Certify that the Server has been registered with valid metadata\n" +
		"\u2022 Call the registered CustomerFriendlyLogoUri \n" +
		"\u2022 Validate the logo in accordance to the UX GuideLines\n" +
		"Certify that there is a match between the scopes supported from the server configuration, and the API Family Types published\n" +
		"\u2022 List all scopes supported from the well-known\n" +
		"\u2022 List all API Family Types published by the Authorization Server\n" +
		"\u2022 Verify if scopes supported <> API Family Types correspond\n" +
		"Certify that the correct Domain ROLEs are created, according to the scopes supported\n" +
		"\u2022 List all scopes supported from the well-known\n" +
		"\u2022 Look for the corresponding Domain ROLEs into the directory",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	configurationFields = {
		"server.authorisationServerId",
		"resource.brazilOrganizationId"
	}
)
@VariantParameters({
	FAPI1FinalOPProfile.class,
	ClientAuthType.class,
	FAPIAuthRequestMethod.class
})
public class DirectoryApiServerRegistrationTestModuleV2 extends AbstractBlockLoggingTestModule {
	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {
		env.putObject("config", config);
		callAndStopOnFailure(GetDynamicServerConfiguration.class);
		setStatus(Status.CONFIGURED);
	}

	@Override
	public void start() {
		setStatus(Status.RUNNING);
		eventLog.startBlock("Extracted Authorisation Server");
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		eventLog.endBlock();
		eventLog.startBlock("Validate Authorisation Server Certifications");
		callAndContinueOnFailure(ValidateAuthServerCertifications.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
		eventLog.startBlock("Validate Authorisation Server Api Resources Certifications");
		callAndContinueOnFailure(ValidateAuthServerApiResources.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
		eventLog.startBlock("Validate Authorisation Server Logo");
		callAndContinueOnFailure(ValidateAuthServerLogo.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
		eventLog.startBlock("Validate Authorisation Server Scopes Supported");
		callAndContinueOnFailure(EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
		eventLog.startBlock("Validate Domain ROLEs are According to the API Family Types");
		callAndContinueOnFailure(EnsureScopesCorrespondToTheOrganisationRoles.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
		fireTestFinished();
	}

	@Override
	protected void logFinalEnv() {
		// nothing to add
	}
}
