package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedFrom;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedFromEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public abstract class AbstractEnsureRecurringPaymentsConsentsRevokedFrom extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			String revokedFrom = OIDFJSON.getString(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.map(body -> body.getAsJsonObject().getAsJsonObject("data"))
					.map(data -> data.getAsJsonObject("revocation"))
					.map(revocation -> revocation.get("revokedFrom"))
					.orElseThrow(() -> error("Unable to find element data.revocation.revokedFrom in the response payload"))
			);
			if (revokedFrom.equals(expectedRevokedFrom().toString())) {
				logSuccess("revokedFrom returned in the response matches the expected revokedFrom");
			} else {
				throw error("revokedFrom returned in the response does not match the expected revokedFrom",
					args("revokedFrom", revokedFrom,
						"expected revokedFrom", expectedRevokedFrom().toString())
				);
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected abstract RecurringPaymentsConsentsRevokedFromEnum expectedRevokedFrom();
}
