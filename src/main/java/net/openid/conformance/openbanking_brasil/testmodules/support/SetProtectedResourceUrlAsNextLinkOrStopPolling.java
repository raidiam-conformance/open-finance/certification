package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class SetProtectedResourceUrlAsNextLinkOrStopPolling extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			JsonObject body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.orElseThrow(() -> error("Could not extract body from response"))
			);

			JsonObject links = Optional.ofNullable(body.getAsJsonObject("links"))
				.orElseThrow(() -> error("No links object found"));

			JsonElement nextElement = links.get("next");
			if (nextElement == null) {
				env.putBoolean("no_next", true);
				log("There is no \"next\" link in the response", args("links", links));
			} else {
				env.putBoolean("no_next", false);
				String next = OIDFJSON.getString(nextElement);
				env.putString("protected_resource_url", next);
				log("Protected resource url has been set to \"next\"", args("url", next));
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}
}
