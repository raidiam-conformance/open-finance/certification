package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePaymentConsentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePaymentWebhookv2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AbstractPaymentsWebhookWithDcmWrongWebhookTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "dcr_api_dcm-pagto-wrong-webhook_test-module",
	displayName = "dcr_api_dcm-pagto-wrong-webhook_test-module",
	summary = "Ensure that the tested institution validates the field webhook_uris matches the software_api_webhook_uris on the SSA and correctly returns an error if this is not working as expected. \n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;\n" +
		"\u2022Obtain a SSA from the Directory\n" +
		"\u2022Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;, where the alias is to be obtained from the field alias on the test configuration\n" +
		"\u2022Call the Registration Endpoint, but send the field \"webhook_uris\" equal to:[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v1”]\n" +
		"\u2022Expect a 400 error - Validate institution returns error field as “invalid_webhook_uris” ",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.webhookWaitTime"
	}
)
public class PaymentsWebhookWithDcmWrongWebhookTestModuleV2 extends AbstractPaymentsWebhookWithDcmWrongWebhookTestModule {
	@Override
	protected void getPaymentAndPaymentConsentEndpoints() {
		call(new ValidateRegisteredEndpoints(
			sequenceOf(
				condition(GetPaymentV2Endpoint.class),
				condition(GetPaymentConsentsV2Endpoint.class)
			))
		);
	}

	@Override
	protected Class<? extends Condition> setGetPaymentnValidator() {
		return PaymentInitiationPixPaymentsValidatorV2.class;
	}
	@Override
	protected void setBrazilPixPaymentObject() {
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
	}
	@Override
	protected Class<? extends Condition> setPostPaymentValidator() {
		return PaymentInitiationPixPaymentsValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return PaymentConsentValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PaymentConsentValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPaymentWebhookCreator() {
		return CreatePaymentWebhookv2.class;
	}

	@Override
	protected Class<? extends Condition> setPaymentConsentWebhookCreator() {
		return CreatePaymentConsentWebhookV2.class;
	}
}
