package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiExpiredPostAuthorisationEnrollmentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.GetEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostEnrollmentsOASValidatorV2n1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "enrollments_api_expired-postauthorisation-enrollment_test-module_v2-1",
    displayName = "enrollments_api_expired-postauthorisation-enrollment_test-module_v2-1",
    summary = "Ensure that enrollment is expired in 5 minutes when status is AWAITING_ENROLLMENT\n" +
		"• Call the POST enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Call the POST Risk Signals endpoint sending the appropriate signals\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is AWAITING_ACCOUNT_HOLDER_VALIDATION\n" +
		"• Redirect the user to authorize the enrollment\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AWAITING_ENROLLMENT\"\n" +
		"• Set the conformance Suite to sleep for 5 minutes\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"REJECTED\", cancelledFrom is DETENTORA, and rejectionReason is REJEITADO_TEMPO_EXPIRADO_ENROLLMENT",
    profile = OBBProfile.OBB_PROFILE,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "resource.enrollmentsUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
    }
)
public class EnrollmentsApiExpiredPostAuthorisationEnrollmentTestModuleV2n1 extends AbstractEnrollmentsApiExpiredPostAuthorisationEnrollmentTestModule {

    @Override
    protected Class<? extends Condition> postEnrollmentsValidator() {
        return PostEnrollmentsOASValidatorV2n1.class;
    }

    @Override
    protected Class<? extends Condition> getEnrollmentsValidator() {
        return GetEnrollmentsOASValidatorV2n1.class;
    }

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetEnrollmentsV2Endpoint.class));
	}

	@Override
	public void cleanup() {
		//not required
	}
}
