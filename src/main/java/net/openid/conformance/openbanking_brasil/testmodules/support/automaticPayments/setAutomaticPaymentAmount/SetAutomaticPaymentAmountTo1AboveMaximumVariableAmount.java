package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SetAutomaticPaymentAmountTo1AboveMaximumVariableAmount extends AbstractSetAutomaticPaymentAmount {

	private String maximumVariableAmount;

	@Override
	@PreEnvironment(strings = "maximumVariableAmount")
	public Environment evaluate(Environment env) {
		maximumVariableAmount = env.getString("maximumVariableAmount");
		return super.evaluate(env);
	}

	@Override
	protected String automaticPaymentAmount() {
		double value = Double.parseDouble(maximumVariableAmount);
		return String.format("%.2f", 1 + value);
	}
}
