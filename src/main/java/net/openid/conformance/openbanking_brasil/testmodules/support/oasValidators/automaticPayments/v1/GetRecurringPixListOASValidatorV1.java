package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public class GetRecurringPixListOASValidatorV1 extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/automatic-payments/swagger-automatic-payments-1.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/pix/recurring-payments";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
