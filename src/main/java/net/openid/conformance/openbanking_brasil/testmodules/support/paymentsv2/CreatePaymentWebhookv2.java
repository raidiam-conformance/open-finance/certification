package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

public class CreatePaymentWebhookv2 extends AbstractCreateWebhookEndpoint{
	@Override
	protected String getValueFromEnv() {
		return "payment_id";
	}

	@Override
	protected String getExpectedUrlPath() {
		return "/open-banking/webhook/v1/payments/v2/pix/payments/";
	}
}
