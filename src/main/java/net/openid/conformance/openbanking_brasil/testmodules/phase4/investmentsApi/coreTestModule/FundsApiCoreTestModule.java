package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsBalancesV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsIdentificationV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsTransactionsCurrentV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsTransactionsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.AddToAndFromTransactionConversionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToFunds;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetFromTransactionConversionDateTo360DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetFromTransactionConversionDateTo6DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetToTransactionConversionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "funds_api_core_test-module",
    displayName = "funds_api_core_test-module",
    summary = "Test will call all of the endpoints of the Funds Investments API, confirming that they return the expected response_body, as defined on the swaggers\n" +
        "• Call the POST Consents endpoint with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
        "• Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Consents endpoint\n" +
        "• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
        "• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
        "• Call the GET Investments List Endpoint\n" +
        "• Expects a 200 response - Validate the Response_body - Extract one of the shared investment products\n" +
        "• Call the GET Investments Identification endpoint with the extracted investmentID\n" +
        "• Expects a 200 response - Validate the Response_body\n" +
        "• Call the GET Investments Balance endpoint with the extracted investmentID\n" +
        "• Expects a 200 response - Validate the Response_body\n" +
        "• Call the GET Investments Transactions endpoint with the extracted investmentID - Send query Params fromTransactionConversionDate equal D+0 and toTransactionConversionDate equal to D+360\n" +
        "• Expects a 200 response - Validate the Response_body\n" +
        "• Call the GET Investments Transactions-Current endpoint with the extracted investmentID. Send query Params fromTransactionConversionDate equal D+0 and toTransactionConversionDate equal to D+6\n" +
        "• Expects a 200 response - Validate the Response_body\n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "directory.discoveryUrl",
        "resource.brazilCpf"
    }
)
public class FundsApiCoreTestModule extends AbstractInvestmentsApiCoreTestModule {

    @Override
    protected AbstractSetInvestmentApi setInvestmentsApi() {
        return new SetInvestmentApiToFunds();
    }

    @Override
    protected Class<? extends Condition> investmentsRootValidator() {
        return GetFundsListV1OASValidator.class;
    }

    @Override
    protected Class<? extends Condition> investmentsIdentificationValidator() {
        return GetFundsIdentificationV1OASValidator.class;
    }

    @Override
    protected Class<? extends Condition> investmentsBalancesValidator() {
        return GetFundsBalancesV1OASValidator.class;
    }

    @Override
    protected Class<? extends Condition> investmentsTransactionsValidator() {
        return GetFundsTransactionsV1OASValidator.class;
    }

    @Override
    protected Class<? extends Condition> investmentsTransactionsCurrentValidator() {
        return GetFundsTransactionsCurrentV1OASValidator.class;
    }

    @Override
    protected Class<? extends Condition> setToTransactionToToday() {
        return SetToTransactionConversionDateToToday.class;
    }

    @Override
    protected Class<? extends Condition> setFromTransactionTo360DaysAgo() {
        return SetFromTransactionConversionDateTo360DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> setFromTransactionTo6DaysAgo() {
        return SetFromTransactionConversionDateTo6DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
        return AddToAndFromTransactionConversionDateParametersToProtectedResourceUrl.class;
    }

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.FUNDS;
	}
}
