package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted;

public class EnsureIsRetryAcceptedTrue extends AbstractEnsureIsRetryAccepted {

	@Override
	protected boolean isRetryAcceptedExpectedValue() {
		return true;
	}
}
