package net.openid.conformance.openbanking_brasil.testmodules.support;

public class AddPaginationUserToConfig extends AbstractAddVariantUserToConfig {
	@Override
	protected String getVariantUserKey() {
		return "brazilCpfPaginationList";
	}

	@Override
	protected String getVariantBusinessKey() {
		return "brazilCnpjPaginationList";
	}
}
