package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit;

public class EnsureUseOverdraftLimitIsTrue extends AbstractEnsureUseOverdraftLimit {

	@Override
	protected boolean expectedUseOverdraftLimitValue() {
		return true;
	}
}
