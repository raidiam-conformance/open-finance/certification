package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1;

public class GetBankFixedIncomesTransactionsOASValidatorV1 extends AbstractBankFixedIncomesTransactionsOASValidatorV1 {

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}/transactions-current";
	}

}
