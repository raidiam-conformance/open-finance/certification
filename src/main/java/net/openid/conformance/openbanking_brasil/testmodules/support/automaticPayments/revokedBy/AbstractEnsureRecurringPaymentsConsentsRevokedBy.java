package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedBy;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedByEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public abstract class AbstractEnsureRecurringPaymentsConsentsRevokedBy extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			String revokedBy = OIDFJSON.getString(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.map(body -> body.getAsJsonObject().getAsJsonObject("data"))
					.map(data -> data.getAsJsonObject("revocation"))
					.map(revocation -> revocation.get("revokedBy"))
					.orElseThrow(() -> error("Unable to find element data.revocation.revokedBy in the response payload"))
			);
			if (revokedBy.equals(expectedRevokedBy().toString())) {
				logSuccess("revokedBy returned in the response matches the expected revokedBy");
			} else {
				throw error("revokedBy returned in the response does not match the expected revokedBy",
					args("revokedBy", revokedBy,
						"expected revokedBy", expectedRevokedBy().toString())
				);
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected abstract RecurringPaymentsConsentsRevokedByEnum expectedRevokedBy();
}
