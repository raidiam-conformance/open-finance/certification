package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import com.google.gson.JsonObject;
import org.springframework.http.HttpMethod;

public class PostRecurringConsentOASValidatorV2 extends AbstractRecurringConsentOASValidatorV2 {

	@Override
	protected String getEndpointPath() {
		return "/recurring-consents";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected void assertIbgeTownCodeConstraints(JsonObject data, JsonObject debtorAccount) {}

	@Override
	protected void assertApprovalDueDateConstraints(JsonObject data) {}
}
