package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Optional;

public class EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonElement cpf = env.getElementFromObject("config", "conditionalResources.brazilCpfJointAccount");
		JsonElement cnpj = env.getElementFromObject("config", "conditionalResources.brazilCnpjJointAccount");

		if (cpf == null && cnpj ==  null) {
			env.putBoolean("continue_test", false);
			throw error("Brazil CPF and CNPJ for Joint Account field is empty. Institution is assumed to not have this functionality");
		}

		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("config", "resource.brazilPaymentConsent.data"))
			.orElseThrow(() -> error("Could not find brazilPaymentConsent")).getAsJsonObject();

		String personType = null;
		String cpfCnpj = null;
		String name = env.getString("config", "resource.creditorName");

		if (cpf != null) {
			personType = "PESSOA_NATURAL";
			cpfCnpj = OIDFJSON.getString(cpf);

			JsonObject loggedUser = new JsonObjectBuilder()
				.addField( "document.identification", OIDFJSON.getString(cpf))
				.addField( "document.rel", "CPF")
				.build();

			brazilPaymentConsent.add("loggedUser", loggedUser);
			logSuccess("Brazil CPF for Joint Account field is present and added", args("loggedUser", loggedUser));
		}

		if (cnpj != null) {
			personType = "PESSOA_JURIDICA";
			cpfCnpj = OIDFJSON.getString(cnpj);

			JsonObject businessEntity = new JsonObjectBuilder()
				.addField( "document.identification", OIDFJSON.getString(cnpj))
				.addField( "document.rel", "CNPJ")
				.build();

			brazilPaymentConsent.add("businessEntity", businessEntity);
			logSuccess("Brazil CNPJ for Joint Account field is present and added", args("businessEntity", businessEntity));
		}

		JsonArray creditors = new JsonArray();
		creditors.add(
			new JsonObjectBuilder()
				.addField("personType", personType)
				.addField("cpfCnpj", cpfCnpj)
				.addField("name", name)
				.build()
		);
		brazilPaymentConsent.add("creditors", creditors);

		return env;
	}
}
