package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetPaymentV4Endpoint extends GetPaymentVXEndpoint {
	@Override
	protected String getApiVersionRegex() {
		return "^(4.[0-9].[0-9])$";
	}
}
