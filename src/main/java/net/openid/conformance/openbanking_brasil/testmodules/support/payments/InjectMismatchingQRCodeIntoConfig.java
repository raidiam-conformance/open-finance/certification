package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.pixqrcode.PixQRCode;
import net.openid.conformance.testmodule.Environment;

import java.util.Locale;
import java.util.Random;

public class InjectMismatchingQRCodeIntoConfig extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonObject consentPayment = env.getElementFromObject("resource", "brazilPaymentConsent.data.payment").getAsJsonObject();
		JsonElement paymentInitiation = env.getElementFromObject("resource", "brazilPixPayment.data");


		//Generate random amount
		Random r = new Random();
		float random = r.nextFloat() * 100;
		String amount = String.format(Locale.UK, "%.02f", random);

		consentPayment.addProperty("amount", amount);

		//Build the QRes for HAPPY PATH on consent
		PixQRCode qrCode = new PixQRCode();
		qrCode.useStandardConfig();
		qrCode.setTransactionAmount(amount);
		consentPayment.getAsJsonObject("details").addProperty("qrCode", qrCode.toString());

		//Build the QRes for a WRONG CITY on payment
		qrCode.setMerchantCity("SALVADOR");

		if (paymentInitiation.isJsonArray()) {
			for (int i = 0; i < paymentInitiation.getAsJsonArray().size(); i++) {
				JsonObject payment = paymentInitiation.getAsJsonArray().get(i).getAsJsonObject();
				payment.getAsJsonObject("payment").addProperty("amount", amount);
				payment.addProperty("qrCode", qrCode.toString());
			}
			logSuccess(String.format("Added new QRes to payment consent and payment initiation with amount %s BRL", amount), args("QRes", qrCode.toString()));
			return env;
		}

		JsonObject payment = paymentInitiation.getAsJsonObject();
		payment.getAsJsonObject("payment").addProperty("amount", amount);
		payment.addProperty("qrCode", qrCode.toString());

		logSuccess(String.format("Added new QRes to payment consent and payment initiation with amount %s BRL", amount), args("QRes", qrCode.toString()));
		return env;
	}
}
