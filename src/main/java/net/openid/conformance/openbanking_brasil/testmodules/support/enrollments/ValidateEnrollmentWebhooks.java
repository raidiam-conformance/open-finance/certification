package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AbstractValidateWebhooksReceived;

public class ValidateEnrollmentWebhooks extends AbstractValidateWebhooksReceived {

	@Override
	protected int expectedWebhooksReceivedAmount() {
		return 1;
	}

	@Override
	protected String webhookType() {
		return "enrollment";
	}
}
