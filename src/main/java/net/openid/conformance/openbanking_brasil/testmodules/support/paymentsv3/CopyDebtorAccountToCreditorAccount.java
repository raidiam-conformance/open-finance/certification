package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CopyDebtorAccountToCreditorAccount extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {

		log("Setting debtorAccount and creditorAccount to match the same config value");

		JsonElement debtorElem = env.getElementFromObject("config", "resource.brazilPaymentConsent.data.debtorAccount");
		if (debtorElem == null) {
			throw error("Debtor object not found in config");
		}

		JsonElement creditorElem = env.getElementFromObject("config", "resource.brazilPaymentConsent.data.payment.details.creditorAccount");
		if (creditorElem == null) {
			throw error("creditorAccount not found");
		}

		JsonObject creditor = creditorElem.getAsJsonObject();

		JsonObject debtorObject = debtorElem.getAsJsonObject();
		env.putObject("originalCreditorAccount", creditor);
		env.putObject("config", "resource.brazilPaymentConsent.data.debtorAccount", debtorObject);
		env.putObject("config", "resource.brazilPaymentConsent.data.payment.details.creditorAccount", debtorObject);

		logSuccess("debtorAccount was successfully copied to creditorAccount");
		return env;
	}

}
