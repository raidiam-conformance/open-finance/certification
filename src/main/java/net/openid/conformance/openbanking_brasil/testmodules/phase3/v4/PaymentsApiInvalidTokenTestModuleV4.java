package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiInvalidTokenTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PatchPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_invalid-token_test-module_v4",
	displayName = "Payments API v4 test module for invalid token type",
	summary = "This test is a PIX Payments that aims to test if the institution accepts the correct type of Token for each different API method\n" +
		"• Create consent with request payload with both the schedule.single.date field set as D+1\n" +
		"• Call the POST Consents endpoints using token issued via client_credentials grant\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent using token issued via client_credentials grant\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\" and validate the response\n" +
		"• Call the POST Payments Endpoint with a token issued via using token issued via client_credentials grant\n" +
		"• Expect either a 401 to be returned\n" +
		"• Call the POST Payments Endpoint  using a token issued via authorization_code grant\n" +
		"• Expects 201 - Validate Response\n" +
		"• Call the GET Payments endpoint using a token issued via authorization_code grant\n" +
		"• Expect either a 401 to be returned\n" +
		"• Call the PATCH Payments endpoint using a token issued via authorization_code grant\n" +
		"• Expect either a 401 to be returned\n" +
		"• Call the POST Consents endpoint using a token issued via authorization_code grant\n" +
		"• Expect either a 401 to be returned",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiInvalidTokenTestModuleV4 extends AbstractPaymentsApiInvalidTokenTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> patchPaymentPixValidator() {
		return PatchPaymentsPixOASValidatorV4.class;
	}
}
