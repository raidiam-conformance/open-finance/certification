package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetRecurringPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetInvalidCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilAutomaticPaymentRecurringConsentProdValues;

public abstract class AbstractFvpAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> postPaymentConsentErrorValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetRecurringPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilAutomaticPaymentRecurringConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		eventLog.startBlock("POST consents sending the logged user not matching the creditor - Expects 422 PARAMETRO_INVALIDO");
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToSetInvalidCreditor.class);
		call(getPaymentsConsentSequence().replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas422.class)));
		callAndContinueOnFailure(postPaymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
		env.mapKey(EnsureErrorResponseCodeFieldWasParametroInvalido.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class, Condition.ConditionResult.FAILURE);
		env.unmapKey(EnsureErrorResponseCodeFieldWasParametroInvalido.RESPONSE_ENV_KEY);
		eventLog.endBlock();
	}
}
