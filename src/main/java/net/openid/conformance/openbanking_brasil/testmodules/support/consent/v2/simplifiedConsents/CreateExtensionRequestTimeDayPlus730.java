package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

public class CreateExtensionRequestTimeDayPlus730 extends AbstractCreateConsentExtensionExpirationTime{
	@Override
	protected int getExtensionTimeInDays() {
		return 730;
	}

	@Override
	protected boolean saveConsentExtensionExpirationTime() {
		return false;
	}
}
