package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

public class SetPaymentAuthorisationFlowToFidoFlow extends AbstractSetPaymentAuthorisationFlow {

	@Override
	protected String getAuthorisationFlowType() {
		return AuthorisationFlowEnumV3.FIDO_FLOW.toString();
	}
}
