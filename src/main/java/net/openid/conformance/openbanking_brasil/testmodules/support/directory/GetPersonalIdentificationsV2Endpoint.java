package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetPersonalIdentificationsV2Endpoint extends AbstractGetXFromAuthServer {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/customers\\/v\\d+\\/personal\\/identifications)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "customers-personal";
	}

	@Override
	protected String getApiVersionRegex() {
		return  "^(2.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
