package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoRegistrationOptionsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasStatusVinculoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoSignOptions;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;

public abstract class AbstractEnrollmentsApiInvalidStatusSignOptionsTestModule extends AbstractEnrollmentsApiTestModule {

	@Override
	protected void executeTestSteps() {
		postAndValidateConsent();

		runInBlock("Call POST sign-options endpoint with the consentID created - Expect 422 STATUS_VINCULO_INVALIDO", () -> {
			// CreateEnrollmentsFidoRegistrationOptionsRequestBody is called in order to put rp_id in env
			callAndContinueOnFailure(CreateEnrollmentsFidoRegistrationOptionsRequestBody.class, Condition.ConditionResult.FAILURE);

			PostEnrollmentsResourceSteps signOptionsSteps = new PostEnrollmentsResourceSteps(
				new PrepareToPostFidoSignOptions(),
				CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaims.class,
				true
			);
			call(signOptionsSteps);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.RESPONSE_ENV_KEY);
		});
	}

	protected void postAndValidateConsent() {
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		call(createOBBPreauthSteps()
			.skip(FAPIBrazilAddConsentIdToClientScope.class, "Consent id is not added to the scope in non-redirect tests"));
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(SaveAccessToken.class);
		runInBlock(currentClientString() + "Validate consents response", this::validatePostConsentResponse);
	}

	protected void validatePostConsentResponse() {
		callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
	}

	protected abstract Class<? extends Condition> postPaymentConsentValidator();
}
