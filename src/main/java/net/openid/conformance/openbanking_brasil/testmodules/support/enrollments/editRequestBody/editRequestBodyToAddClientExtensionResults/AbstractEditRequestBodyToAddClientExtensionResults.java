package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.editRequestBodyToAddClientExtensionResults;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public abstract class AbstractEditRequestBodyToAddClientExtensionResults extends AbstractCondition {

	protected abstract String getPathToClientExtensionResults();

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonObject clientExtensionResults = env.getObject("client_extension_results");

		if (clientExtensionResults == null || clientExtensionResults.isEmpty()) {
			logSuccess("There are no extension results to be added to the request");
		} else {
			JsonObject obj = Optional.ofNullable(
				env.getElementFromObject("resource_request_entity_claims", getPathToClientExtensionResults())
			).map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error(String.format("Could not find %s inside the request body", getPathToClientExtensionResults())));

			obj.add("clientExtensionResults", clientExtensionResults);
			logSuccess("The clientExtensionResults field has been added to the request body", args(getPathToClientExtensionResults(), obj));
		}

		return env;
	}
}
