package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions;

public class SetToTransactionConversionDateTo180DaysAgo extends AbstractSetToTransactionConversionDate {

    @Override
    protected int amountOfDaysToThePast() {
        return 180;
    }
}
