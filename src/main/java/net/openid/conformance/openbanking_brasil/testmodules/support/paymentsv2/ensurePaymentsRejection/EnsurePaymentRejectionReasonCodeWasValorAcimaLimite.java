package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection;

import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v4.enums.PaymentRejectionReasonEnumV4;

import java.util.List;

public class EnsurePaymentRejectionReasonCodeWasValorAcimaLimite extends AbstractEnsurePaymentRejectionReasonCodeWasX {

	@Override
	protected List<String> getExpectedRejectionCodes() {
		return List.of(PaymentRejectionReasonEnumV4.VALOR_ACIMA_LIMITE.toString());
	}
}
