package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public class ResetPaymentRequest extends AbstractCondition {

	@Override
	public final Environment evaluate(Environment env) {
		log("Resetting consent request");
		JsonObject resource = env.getObject("resource");
		JsonElement paymentElement = env.getElementFromObject("resource", "brazilPixPayment.data");
		JsonObject payment = null;
		int execTimes = 1;

		for (int i = 0; i < execTimes; i++) {
			if(paymentElement.isJsonArray()){
				execTimes = paymentElement.getAsJsonArray().size();
				payment = paymentElement.getAsJsonArray().get(i).getAsJsonObject().getAsJsonObject("payment");
			}
			else {
				 payment = resource.getAsJsonObject("brazilPixPayment").getAsJsonObject("data").getAsJsonObject("payment");
			}
			if(env.getString("previous_currency") != null){
				payment.addProperty("currency", env.getString("previous_currency"));
				logSuccess("Successfully reset currency of payment request", payment);
			}
			if(env.getString("previous_amount") != null){
				payment.addProperty("amount", env.getString("previous_amount"));
				logSuccess("Successfully reset amount of payment request", payment);
			}
		}
		logSuccess("Successfully reset payment payload.", payment);
		return env;
	}
}
