package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetInvoiceFinancingsV2Endpoint extends AbstractGetXFromAuthServer {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/invoice-financings\\/v\\d+\\/contracts)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "invoice-financings";
	}

	@Override
	protected String getApiVersionRegex() {
		return "^(2.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
