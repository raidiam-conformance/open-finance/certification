package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("Business Identification V2.2.0")
public class BusinessIdentificationOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/customers/swagger-customers-2.2.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/business/identifications";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		for(JsonElement dataElement : body.getAsJsonArray("data")) {
			JsonObject data = dataElement.getAsJsonObject();
			this.assertPersonTypeConstraint(data);
			this.assertPhonesConstraint(data);
		}
	}

	protected void assertPersonTypeConstraint(JsonObject data) {
		JsonArray parties = JsonPath.read(data, "$.parties[*]");
		for(JsonElement partyObject : parties) {
			JsonObject party = partyObject.getAsJsonObject();
			assertField1IsRequiredWhenField2HasValue2(
				party,
				"civilName",
				"personType",
				"PESSOA_NATURAL"
			);
			assertField1IsRequiredWhenField2HasValue2(
				party,
				"companyName",
				"personType",
				"PESSOA_JURIDICA"
			);
		}
	}

	protected void assertPhonesConstraint(JsonObject data) {
		JsonArray phones = JsonPath.read(data, "$.contacts.phones[*]");
		for(JsonElement phoneElement : phones) {
			assertField1IsRequiredWhenField2HasValue2(
				phoneElement.getAsJsonObject(),
				"additionalInfo",
				"type",
				"OUTRO"
			);
		}
	}
}
