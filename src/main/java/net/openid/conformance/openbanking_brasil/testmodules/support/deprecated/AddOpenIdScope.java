package net.openid.conformance.openbanking_brasil.testmodules.support.deprecated;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;


/**
 * @deprecated Use {@link ScopesAndPermissionsBuilder} instead.
 */
@Deprecated
public class AddOpenIdScope extends AbstractScopeAddingCondition {

	@Override
	protected String newScope() {
		return "openid";
	}
}
