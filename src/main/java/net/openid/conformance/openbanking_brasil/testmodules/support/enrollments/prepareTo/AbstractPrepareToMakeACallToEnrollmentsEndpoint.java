package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;

import java.util.Optional;

public abstract class AbstractPrepareToMakeACallToEnrollmentsEndpoint extends AbstractCondition {

	protected abstract HttpMethod httpMethod();
	protected abstract String endpoint();
	protected abstract boolean expectsSelfLink();

	@Override
	@PreEnvironment(required = "config")
	@PostEnvironment(strings = {"enrollments_url", "http_method"})
	public Environment evaluate(Environment env) {
		env.putString("enrollments_url", createUrl(getEnrollmentsUrl(env), getEnrollmentId(env)));
		env.putString("http_method", httpMethod().name());
		env.putBoolean("expects_self_link", expectsSelfLink());

		logSuccess("Call to enrollments API will be an HTTP POST",
			args("http method", env.getString("http_method"), "url", env.getString("enrollments_url")));

		return env;
	}

	protected String getEnrollmentsUrl(Environment env) {
		return Optional.ofNullable(env.getElementFromObject("config", "resource.enrollmentsUrl"))
			.map(OIDFJSON::getString).orElseThrow(() -> error("enrollments url missing from configuration"));
	}

	protected String getEnrollmentId(Environment env) {
		return Optional.ofNullable(env.getString("enrollment_id"))
			.orElseThrow(() -> error("enrollmentId is missing from environment"));
	}

	protected String createUrl(String enrollmentsUrl, String enrollmentId) {
		String url = String.format("%s/%s", enrollmentsUrl, enrollmentId);
		if (!endpoint().isEmpty()) {
			url += "/" + endpoint();
		}
		return url;
	}
}
