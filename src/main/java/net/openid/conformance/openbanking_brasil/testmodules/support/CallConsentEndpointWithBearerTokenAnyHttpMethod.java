package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.CallConsentEndpointWithBearerToken;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collections;


public class CallConsentEndpointWithBearerTokenAnyHttpMethod extends CallConsentEndpointWithBearerToken {

	@Override
	protected String getUri(Environment env) {
		HttpMethod httpMethod = getMethod(env);
		if (!httpMethod.equals(HttpMethod.POST)) {
			String consentUrl = env.getString("consent_url");
			if (Strings.isNullOrEmpty(consentUrl)) {
				throw error("consent url missing from configuration");
			}
			return consentUrl;
		}

		return super.getUri(env);
	}

	@Override
	protected HttpMethod getMethod(Environment env) {

		String method = env.getString("http_method");
		if (Strings.isNullOrEmpty(method)) {
			throw error("HTTP method not found");
		}
		return HttpMethod.valueOf(method);
	}

	@Override
	protected Object getBody(Environment env) {
		HttpMethod httpMethod = getMethod(env);
		if (httpMethod.equals(HttpMethod.GET) || httpMethod.equals(HttpMethod.DELETE)) {
			return null;
		}
		return env.getObject("consent_endpoint_request").toString();
	}

	@Override
	protected Environment handleClientResponse(Environment env, JsonObject responseCode, String responseBody, JsonObject responseHeaders, JsonObject fullResponse) {

		env.putObject("consent_endpoint_response_full", fullResponse);

		// consent_endpoint_response is deprecated and is being removed
		env.putObject("consent_endpoint_response", new Gson().fromJson(responseBody, JsonObject.class));
		env.putObject("resource_endpoint_response_headers", responseHeaders);

		logSuccess("Got a response from the consent endpoint", fullResponse);
		return env;
	}

	@Override
	@PreEnvironment(required = { "access_token", "resource", "consent_endpoint_request", "resource_endpoint_request_headers" }, strings = "http_method")
	@PostEnvironment(required = { "resource_endpoint_response_headers", "consent_endpoint_response", "consent_endpoint_response_full" })
	public Environment evaluate(Environment env) { return callProtectedResource(env); }

	@Override
	protected Environment callProtectedResource(Environment env) {
		String uri = getUri(env);

		try {
			RestTemplate restTemplate = createRestTemplate(env);

			if (treatAllHttpStatusAsSuccess()) {
				restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
					@Override
					public boolean hasError(ClientHttpResponse response) throws IOException {
						// Treat all http status codes as 'not an error', so spring never throws an exception due to the http
						// status code meaning the rest of our code can handle http status codes how it likes
						return false;
					}
				});
			}

			HttpMethod method = getMethod(env);
			HttpHeaders headers = getHeaders(env);

			if (headers.getAccept().isEmpty()) {
				headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			}

			if (method == HttpMethod.POST && headers.getContentType() == null) {
				// See https://bitbucket.org/openid/connect/issues/1137/is-content-type-application-x-www-form
				headers.setContentType(getContentType(env));
			}

			HttpEntity<?> request = new HttpEntity<>(getBody(env), headers);

			ResponseEntity<String> response = restTemplate.exchange(uri, method, request, String.class);
			JsonObject responseCode = new JsonObject();
			responseCode.addProperty("code", response.getStatusCodeValue());
			String responseBody = response.getBody();
			JsonObject responseHeaders = mapToJsonObject(response.getHeaders(), true);
			JsonObject fullResponse;

			if (!Strings.isNullOrEmpty(responseBody)) {
				if (requireJsonResponseBody()) {
					fullResponse = convertJsonResponseForEnvironment("resource", response);
				} else {
					fullResponse = convertResponseForEnvironment("resource", response);
				}
			} else {
				fullResponse = responseCode;
			}
			return handleClientResponse(env, responseCode, responseBody, responseHeaders, fullResponse);
		} catch (RestClientResponseException e) {
			return handleClientResponseException(env, e);
		} catch (UnrecoverableKeyException | KeyManagementException | CertificateException | InvalidKeySpecException |
				 NoSuchAlgorithmException | KeyStoreException | IOException e) {
			throw error("Error creating HTTP client", e);
		} catch (RestClientException e) {
			String msg = "Call to protected resource " + uri + " failed";
			if (e.getCause() != null) {
				msg += " - " +e.getCause().getMessage();
			}
			throw error(msg, e);
		}
	}
}


