package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixUniquePaymentPathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalFutureDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetRandomCreditorAccountNumber;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.AbstractEnsurePaymentStatusWasX;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixFirstPaymentInvalidCreditorTestModule extends AbstractAutomaticPaymentsAutomaticPixUniquePaymentPathTestModule {

	@Override
	protected boolean isHappyPath() {
		return false;
	}

	@Override
	protected AbstractEnsurePaymentStatusWasX finalPaymentStatusCondition() {
		return new EnsurePaymentStatusWasRjct();
	}

	@Override
	protected AbstractEnsurePaymentConsentStatusWas finalConsentStatusCondition() {
		return new EnsurePaymentConsentStatusWasAuthorised();
	}

	@Override
	protected String finalConsentStatus() {
		return "REJECTED";
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorResponseCodeFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento();
	}

	@Override
	protected AbstractEnsureRecurringPaymentsRejectionReason ensureRejectionReasonCondition() {
		return new EnsureRecurringPaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento();
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalFutureDate();
	}

	@Override
	protected void configureCreditorFields() {
		super.configureCreditorFields();
		callAndStopOnFailure(EditRecurringPaymentBodyToSetRandomCreditorAccountNumber.class);
	}
}
