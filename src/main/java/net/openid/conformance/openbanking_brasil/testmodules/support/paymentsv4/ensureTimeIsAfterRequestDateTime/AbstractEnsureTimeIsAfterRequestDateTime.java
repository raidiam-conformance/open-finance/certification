package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureTimeIsAfterRequestDateTime;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public abstract class AbstractEnsureTimeIsAfterRequestDateTime extends AbstractCondition {

	private static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonObject response = bodyFrom(env).getAsJsonObject();
		String requestDateTimeStr = OIDFJSON.getString(
			Optional.ofNullable(response.getAsJsonObject("meta"))
				.map(meta -> meta.get("requestDateTime"))
				.orElseThrow(() -> error("It was not possible to retrieve meta.requestDateTime from the response"))
		);

		checkIfDatesAreAfterRequestDateTime(response, requestDateTimeStr);

		return env;
	}

	protected JsonElement bodyFrom(Environment environment) {
		try {
			return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Error parsing JWT response");
		}
	}

	protected abstract void checkIfDatesAreAfterRequestDateTime(JsonObject response, String requestDateTimeStr);

	protected boolean isAfterOrEqualsRequestDateTime(String timeStr, String requestDateTimeStr) {
		LocalDateTime time = LocalDateTime.parse(timeStr, DateTimeFormatter.ISO_DATE_TIME);
		LocalDateTime requestDateTime = LocalDateTime.parse(requestDateTimeStr, DateTimeFormatter.ISO_DATE_TIME);
		return time.isAfter(requestDateTime) || time.isEqual(requestDateTime);
	}
}
