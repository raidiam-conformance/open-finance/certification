package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.editRequestBodyToAddClientExtensionResults;

public class EditConsentsAuthoriseRequestBodyToAddClientExtensionResults extends AbstractEditRequestBodyToAddClientExtensionResults {

	@Override
	protected String getPathToClientExtensionResults() {
		return "data.fidoAssertion";
	}
}
