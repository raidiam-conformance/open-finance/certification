package net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas;

public class EnsureConsentWasAwaitingAuthorisation extends AbstractEnsureConsentStatusWas {

	@Override
	protected String getExpectedStatus() {
		return "AWAITING_AUTHORISATION";
	}
}
