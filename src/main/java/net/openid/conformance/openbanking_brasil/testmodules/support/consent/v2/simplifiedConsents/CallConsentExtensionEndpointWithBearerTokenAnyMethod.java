package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.common.base.Strings;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.testmodule.Environment;

public class CallConsentExtensionEndpointWithBearerTokenAnyMethod extends CallConsentEndpointWithBearerTokenAnyHttpMethod {
	@Override
	protected String getUri(Environment env) {
		String consentUrl = env.getString("consent_extension_url");
		if (Strings.isNullOrEmpty(consentUrl)) {
			throw error("consent_extension_url missing from environment");
		}
		return consentUrl;
	}
}
