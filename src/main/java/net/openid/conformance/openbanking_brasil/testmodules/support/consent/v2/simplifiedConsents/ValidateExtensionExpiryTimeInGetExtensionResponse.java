package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.Instant;

public abstract class ValidateExtensionExpiryTimeInGetExtensionResponse extends AbstractJsonAssertingCondition {
	@Override
	@PreEnvironment(required = {"extension_expiry_times","resource_endpoint_response_full"})
	public Environment evaluate(Environment environment) {
		JsonElement consentResponse = bodyFrom(environment,"resource_endpoint_response_full");
		JsonArray data = consentResponse.getAsJsonObject().getAsJsonArray("data");
		JsonObject extensionTimes = environment.getObject("extension_expiry_times");
		JsonArray extensionTimesArray = extensionTimes.getAsJsonArray("extension_times");
		if (extensionTimesArray == null) {
			throw error("Expected extension_times array to be present, this is a bug in the test module", args("extension_times", extensionTimes));
		}
		if (data.size() > expectedResponseArraySize()) {
			throw error("Expected extension response array size to be: " + expectedResponseArraySize(), args("extension_response", data));
		}
		data.forEach(element -> {
			JsonElement expirationDateTime = element.getAsJsonObject().get("expirationDateTime");
			String time = expirationDateTime == null ? "" : OIDFJSON.getString(expirationDateTime);
			if(!extensionTimesArray.contains(new JsonPrimitive(time))) {
				throw error("Expected consent to match one of the saved extension time", args("expirationDateTime", time, "extension_times", extensionTimesArray));
			}
			logSuccess("Updated consent expirationDateTime matches expected", args("expirationDateTime", time));
		});
		validateExtensionTimeInDescOrder(data);
		return environment;
	}
	private void validateExtensionTimeInDescOrder(JsonArray extensionResponseArray){
		Instant previousTime = null;
		for(JsonElement element : extensionResponseArray){
			if (element.getAsJsonObject().get("expirationDateTime") == null) {
				continue;
			}
			Instant currentTime = Instant.parse(OIDFJSON.getString(element.getAsJsonObject().get("expirationDateTime")));
			if(previousTime != null && currentTime.isAfter(previousTime)){
				throw error("Expected extension response array to be in descending order", args("extension_response", extensionResponseArray));
			}
			previousTime = currentTime;
		}
		logSuccess("Extension response array is in descending order", args("extension_response", extensionResponseArray));
	}
	protected abstract int expectedResponseArraySize();
}
