package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class SetProtectedResourceUrlToRecurringPaymentsEndpoint extends ResourceBuilder {

	@Override
	public Environment evaluate(Environment env) {
		setApi("automatic-payments");
		setEndpoint("/pix/recurring-payments");

		return super.evaluate(env);
	}
}
