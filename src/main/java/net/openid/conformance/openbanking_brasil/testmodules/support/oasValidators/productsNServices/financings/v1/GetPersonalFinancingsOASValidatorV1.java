package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.productsNServices.financings.v1;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


@ApiName("ProductsNServices Personal Financings V1.0.1")
public class GetPersonalFinancingsOASValidatorV1 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-opendata-financings-1.0.1.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/personal-financings";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
