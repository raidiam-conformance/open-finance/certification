package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1;

public class GetFundsTransactionsV1OASValidator extends AbstractFundsTransactionsOASValidator {

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}/transactions";
	}
}
