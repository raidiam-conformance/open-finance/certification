package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.openbanking_brasil.testmodules.RememberOriginalScopes;
import net.openid.conformance.openbanking_brasil.testmodules.ResetScopesToConfigured;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.SetPatchPaymentRequestRelToInvalidValue;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoNaoPermiteCancelamento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.EnsurePaymentsConsentIsNotScheduled;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractPaymentsApiPixSchedulingPatchUnhappyTestModule extends AbstractPaymentsPixSchedulingTestModule {

	private static final String MOCK_BANK_WELL_KNOWN = "https://auth.mockbank.poc.raidiam.io/.well-known/openid-configuration";

	protected boolean secondTest = false;

	protected abstract Class<? extends Condition> patchPaymentErrorValidator();

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		eventLog.log(getName(), "Payments scope present - protected resource assumed to be a payments endpoint");
		ConditionSequence steps = new OpenBankingBrazilPreAuthorizationSteps(
			false,
			false,
			addTokenEndpointClientAuthentication,
			true,
			false,
			false
		).insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class)))
			.insertAfter(CheckForFAPIInteractionIdInResourceResponse.class,
				condition(EnsureMatchingFAPIInteractionId.class));
		if (!secondTest) {
			steps = steps.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class, condition(RememberOriginalScopes.class));
		} else {
			steps = steps.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class, condition(ResetScopesToConfigured.class))
				.insertAfter(ExtractAccessTokenFromTokenResponse.class,condition(SaveAccessToken.class));
		}
		return steps;
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
	}

	@Override
	protected ConditionSequence getCallPatchConditionSequence() {
		return super.getCallPatchConditionSequence()
			.replace(EnsureResourceResponseCodeWas200.class, condition(EnsureResourceResponseCodeWas422.class));
	}

	@Override
	protected ConditionSequence getCreatePatchRequestSequence() {
		ConditionSequence sequence = super.getCreatePatchRequestSequence();
		if (!secondTest) {
			sequence = sequence
				.insertAfter(CreatePatchPaymentsRequestFromConsentRequestV2.class, condition(SetPatchPaymentRequestRelToInvalidValue.class));
		}
		return sequence;
	}

	@Override
	protected ConditionSequence getPatchValidationSequence() {
		env.mapKey(EnsureErrorResponseCodeFieldWasPagamentoNaoPermiteCancelamento.RESPONSE_ENV_KEY,"resource_endpoint_response_full");
		return super.getPatchValidationSequence()
			.replace(patchPaymentConsentValidator(), condition(patchPaymentErrorValidator()))
			.insertAfter(patchPaymentConsentValidator(), condition(EnsureErrorResponseCodeFieldWasPagamentoNaoPermiteCancelamento.class));
	}

	@Override
	protected void validateResponse() {
		if (!secondTest) {
			super.validateResponse();
		} else {
			call(getPaymentValidationSequence().replace(getPaymentValidator(),condition(postPaymentValidator())));
			callAndStopOnFailure(SaveOldValues.class);
			executePostPollingSteps();
			callAndStopOnFailure(LoadOldValues.class);
			callAndStopOnFailure(SetProtectedResourceUrlToSelfEndpoint.class);
			repeatSequence(this::getRepeatSequenceSecondTest)
				.untilTrue("payment_proxy_check_for_reject")
				.trailingPause(30)
				.times(5)
				.validationSequence(this::getPaymentValidationSequence)
				.onTimeout(sequenceOf(
					condition(TestTimedOut.class),
					condition(ChuckWarning.class)))
				.run();

			validateFinalState();
		}
	}

	protected ConditionSequence getRepeatSequenceSecondTest() {
		return getRepeatSequence()
			.skip(SetProtectedResourceUrlToSelfEndpoint.class, "Url already set");
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		if (!secondTest) {
			return super.getPaymentPollStatusCondition();
		} else {
			return CheckPaymentPollStatusRcvdOrAccpOrAcpdV2.class;
		}
	}

	@Override
	protected void validateFinalState() {
		if (!secondTest) {
			super.validateFinalState();
		} else {
			callAndStopOnFailure(EnsurePaymentStatusWasAcsc.class);
		}
	}

	protected void configureSecondTest() {
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
		callAndStopOnFailure(EnsurePaymentsConsentIsNotScheduled.class);
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		if (env.getString("config", "server.discoveryUrl").equals(MOCK_BANK_WELL_KNOWN)) {
			callAndStopOnFailure(InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources.class);
		}
		configureDictInfo();
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		if (!secondTest) {
			secondTest = true;
			validationStarted = false;
			eventLog.startBlock(currentClientString() + "Setting up second part of the test");
			configureSecondTest();
			eventLog.endBlock();
			performAuthorizationFlow();
		} else {
			super.onPostAuthorizationFlowComplete();
		}
	}
}
