package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.*;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonUtils;

import java.util.Map;
import java.util.Optional;

public class ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatusMultiplePages extends AbstractCondition {
    private static final Gson GSON = JsonUtils.createBigDecimalAwareGson();

    @Override
    @PreEnvironment(strings = {"resource_type", "resource_status"}, required = "resource_endpoint_response_full")
    @PostEnvironment(required = "extracted_resource_id")
    public Environment evaluate(Environment env) {
        String bodyJsonString = env.getString("resource_endpoint_response_full", "body");
        JsonObject body;
        try {
            body = GSON.fromJson(bodyJsonString, JsonObject.class);
        } catch (JsonSyntaxException e) {
            throw error("Body is not json", args("body", bodyJsonString));
        }
        JsonArray data = body.getAsJsonArray("data");
        String requiredType = env.getString("resource_type");
        String requiredStatus = env.getString("resource_status");

        JsonArray extractedIds = Optional.ofNullable(env.getObject("extracted_resource_id"))
            .map(json -> json.getAsJsonArray("extractedResourceIds"))
            .orElse(new JsonArray());

        for (JsonElement jsonElement : data) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            String resourceType = OIDFJSON.getString(jsonObject.get("type"));
            String resourceStatus = OIDFJSON.getString(jsonObject.get("status"));
            if (resourceType.equals(requiredType) && resourceStatus.equals(requiredStatus)) {
                if (jsonObject.has("resourceId")) {
                    String resourceId = OIDFJSON.getString(jsonObject.get("resourceId"));
                    extractedIds.add(resourceId);
                } else {
                    throw error("Extracted resource does not have resourceId", Map.of("Resource Body", jsonObject));
                }

            }
        }

        JsonObject object = new JsonObject();
        object.add("extractedResourceIds", extractedIds);
        env.putObject("extracted_resource_id", object);
        logSuccess("Extracted resourceId's of corresponding API", Map.of("Resource type", requiredType, "Resource Status", requiredStatus, "Extracted resourceId", extractedIds));

        return env;
    }
}
