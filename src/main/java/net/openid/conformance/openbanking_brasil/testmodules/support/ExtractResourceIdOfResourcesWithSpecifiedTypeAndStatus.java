package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(strings = {"resource_type", "resource_status"}, required = RESPONSE_ENV_KEY)
	@PostEnvironment(required = "extracted_resource_id")
	public Environment evaluate(Environment env) {
		try {
			JsonObject body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.orElseThrow(() -> error("Could not extract body from response"))
			);

			JsonArray data = body.getAsJsonArray("data");
			String requiredType = env.getString("resource_type");
			String requiredStatus = env.getString("resource_status");

			JsonArray extractedIds = new JsonArray();
			for (JsonElement resourceElement : data) {
				JsonObject resource = resourceElement.getAsJsonObject();
				String resourceType = OIDFJSON.getString(resource.get("type"));
				String resourceStatus = OIDFJSON.getString(resource.get("status"));
				if (resourceType.equals(requiredType) && resourceStatus.equals(requiredStatus)) {
					if (resource.has("resourceId")) {
						String resourceId = OIDFJSON.getString(resource.get("resourceId"));
						extractedIds.add(resourceId);
					} else {
						throw error("Extracted resource does not have resourceId", args("Resource Body", resource));
					}
				}
			}

			JsonObject extractedResourceIdObj = env.getObject("extracted_resource_id");
			if (extractedResourceIdObj == null) {
				extractedResourceIdObj = new JsonObject();
				extractedResourceIdObj.add("extractedResourceIds", extractedIds);
				env.putObject("extracted_resource_id", extractedResourceIdObj);
			} else {
				JsonArray extractedResourceIds = extractedResourceIdObj.getAsJsonArray("extractedResourceIds");
				extractedResourceIds.addAll(extractedIds);
			}

			logSuccess("Extracted resourceId's of corresponding API", args("Resource type", requiredType, "Resource Status", requiredStatus, "Extracted resourceId", extractedIds));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}

		return env;
	}
}
