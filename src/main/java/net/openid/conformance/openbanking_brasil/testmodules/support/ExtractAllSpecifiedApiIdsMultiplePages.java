package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.*;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonUtils;

import java.util.Map;
import java.util.Optional;

public class ExtractAllSpecifiedApiIdsMultiplePages extends AbstractCondition {

    private static final Gson GSON = JsonUtils.createBigDecimalAwareGson();

    @Override
    @PreEnvironment(strings = "apiIdName")
    @PostEnvironment(required = "extracted_api_ids")
    public Environment evaluate(Environment env) {
        String bodyJsonString = env.getString("resource_endpoint_response_full", "body");
        JsonObject body;
        try {
            body = GSON.fromJson(bodyJsonString, JsonObject.class);
        } catch (JsonSyntaxException e) {
            throw error("Body is not json", args("body", bodyJsonString));
        }

        JsonArray extractedApiIds = Optional.ofNullable(env.getObject("extracted_api_ids"))
            .map(json -> json.getAsJsonArray("extractedApiIds"))
            .orElse(new JsonArray());

        String specifiedApiIdName = env.getString("apiIdName");

        JsonArray data = body.getAsJsonArray("data");
        for (JsonElement jsonElement : data) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            extractedApiIds.add(jsonObject.get(specifiedApiIdName));
        }
        if(!extractedApiIds.isEmpty()){
            JsonObject object = new JsonObject();
            object.add("extractedApiIds", extractedApiIds);
            env.putObject("extracted_api_ids", object);
            logSuccess("Extracted all API IDs", Map.of("Extracted ID's", extractedApiIds));
        } else {
            throw error("No API IDs were extracted", Map.of("data", data, "API ID", specifiedApiIdName));
        }

        return env;
    }
}
