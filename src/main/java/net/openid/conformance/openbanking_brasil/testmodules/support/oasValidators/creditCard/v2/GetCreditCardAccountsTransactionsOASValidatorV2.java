package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2;

public class GetCreditCardAccountsTransactionsOASValidatorV2 extends AbstractGetCreditCardAccountsTransactionsOASValidatorV2 {

	@Override
	protected String getEndpointPath() {
		return "/accounts/{creditCardAccountId}/transactions";
	}
}
