package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractApiTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractFinancingsApiTestModule extends AbstractApiTestModulePhase2 {

	@Override
	protected abstract Class<? extends Condition> apiResourceContractListResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractGuaranteesResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractPaymentsResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractInstallmentsResponseValidator();

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetFinancingsV2Endpoint.class)
		);
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.FINANCINGS;
	}

	@Override
	protected EnumResourcesType getApiType() {
		return EnumResourcesType.FINANCING;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
