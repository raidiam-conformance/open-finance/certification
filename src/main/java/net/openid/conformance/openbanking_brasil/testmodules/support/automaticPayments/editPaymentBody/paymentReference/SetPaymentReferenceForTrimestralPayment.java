package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.IsoFields;

public class SetPaymentReferenceForTrimestralPayment extends AbstractSetPaymentReference {

	@Override
	protected String getPaymentReference(String dateStr) {
		LocalDate date = getPaymentDate(dateStr);
		int quarter = date.get(IsoFields.QUARTER_OF_YEAR);
		int year = date.get(ChronoField.YEAR);

		return String.format("Q%d-%d", quarter, year);
	}
}
