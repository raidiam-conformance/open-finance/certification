package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules;

import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToBankFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractBankFixedIncomesApiPaginationListConditionalTest extends AbstractInvestmentsApiPaginationListConditionalTestModule {

    @Override
    protected AbstractSetInvestmentApi setInvestmentsApi() {
        return new SetInvestmentApiToBankFixedIncomes();
    }

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.BANK_FIXED_INCOMES;
	}
}
