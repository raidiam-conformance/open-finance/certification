package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentExpiryDateTimeGreaterThanAYear;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentExpiryDateTimeInThePast;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentExpiryDateTimePoorlyFormed;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFPermissionsEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostConsentWithBadRequestSequence;

public abstract class AbstractFvpConsentApiNegativeTests extends AbstractFvpDcrXConsentsTestModule {

	ScopesAndPermissionsBuilder scopesAndPermissionsBuilder;

	protected abstract Class<? extends Condition> postConsentValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetConsentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void runTests() {
		scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);

		validateBadPermissionCombination("Resource read only", OPFPermissionsEnum.RESOURCES_READ);
		validateBadPermissionCombination("Customers identification permissions only", OPFPermissionsEnum.CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ);
		validateBadPermissionCombination("incomplete credit card permission group", OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_BILLS_READ, OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_READ, OPFPermissionsEnum.RESOURCES_READ);
		validateBadPermissionCombination("incomplete account permission group", OPFPermissionsEnum.ACCOUNTS_READ);
		validateBadPermissionCombination("less incomplete account permission group", OPFPermissionsEnum.ACCOUNTS_READ, OPFPermissionsEnum.RESOURCES_READ);
		validateBadPermissionCombination("incomplete combination of Limits & Credit Operations Contract Data permission groups", OPFPermissionsEnum.ACCOUNTS_OVERDRAFT_LIMITS_READ, OPFPermissionsEnum.RESOURCES_READ, OPFPermissionsEnum.LOANS_READ,
			OPFPermissionsEnum.LOANS_WARRANTIES_READ, OPFPermissionsEnum.LOANS_SCHEDULED_INSTALMENTS_READ, OPFPermissionsEnum.LOANS_PAYMENTS_READ, OPFPermissionsEnum.FINANCINGS_READ, OPFPermissionsEnum.FINANCINGS_WARRANTIES_READ, OPFPermissionsEnum.FINANCINGS_SCHEDULED_INSTALMENTS_READ,
			OPFPermissionsEnum.FINANCINGS_PAYMENTS_READ, OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_READ, OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ, OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ,
			OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_WARRANTIES_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_PAYMENTS_READ, OPFPermissionsEnum.RESOURCES_READ);
		validateNonExistentPermission();
		validateBadExpiration(ConsentExpiryDateTimeGreaterThanAYear.class, "422", "DateTime greater than 1 year from now", EnsureConsentResponseCodeWas422.class);
		validateBadExpiration(ConsentExpiryDateTimeInThePast.class, "422", "DateTime in the past", EnsureConsentResponseCodeWas422.class);
		validateBadExpiration(ConsentExpiryDateTimePoorlyFormed.class, "400", "DateTime poorly formed", EnsureConsentResponseCodeWas400.class);
	}

	private void validateBadPermissionCombination(String description, OPFPermissionsEnum... permissions) {
		String logMessage = String.format("Check for HTTP 422 COMBINACAO_PERMISSOES_INCORRETA response from consent api request for %s", description);
		eventLog.startBlock(logMessage);
		scopesAndPermissionsBuilder.reset();
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS);
		scopesAndPermissionsBuilder.addPermissions(permissions).build();
		env.mapKey(EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		call(sequenceOf(
			condition(PrepareToPostConsentRequest.class),
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(SetContentTypeApplicationJson.class),
			condition(FAPIBrazilOpenBankingCreateConsentRequest.class),
			condition(FAPIBrazilAddExpirationToConsentRequest.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(EnsureConsentResponseCodeWas422.class)
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(postConsentValidator())
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta.class)
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)
		));
		env.unmapKey(EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta.RESPONSE_ENV_KEY);
		eventLog.endBlock();
	}

	private void validateNonExistentPermission() {
		String logMessage = "Check for HTTP 400 response from consent api request for non-existent permission group";
		eventLog.startBlock(logMessage);
		scopesAndPermissionsBuilder.reset();
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS);
		env.putString("consent_permissions", "BAD_PERMISSION");
		call(sequenceOf(
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(SetContentTypeApplicationJson.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
		));
		call(new PostConsentWithBadRequestSequence()
			.insertAfter(EnsureConsentResponseCodeWas400.class, condition(postConsentValidator())
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)));
		eventLog.endBlock();
	}

	private void validateBadExpiration(Class<? extends Condition> setupClass, String statusCode, String description, Class<? extends Condition> errorCodeCondition) {
		String logMessage = String.format("Check for HTTP %s response from consent api request for %s.", statusCode, description);
		eventLog.startBlock(logMessage);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_OPERATIONS).build();
		callAndStopOnFailure(setupClass);
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(SetContentTypeApplicationJson.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(errorCodeCondition, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(postConsentValidator(), Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
	}

	@Override
	protected void insertConsentProdValues() {}
}
