package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CompareIdTokenClaims;
import net.openid.conformance.condition.client.EnsureAccessTokenValuesAreDifferent;
import net.openid.conformance.condition.client.ExtractRefreshTokenFromTokenResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddResourceUrlToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseDoesNotHaveLinks;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractFidoRegistrationOptionsChallenge;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateFidoAuthorizeRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostConsentsAuthorize;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoSignOptions;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj.SetConsentIdInPaymentsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.SetPaymentAuthorisationFlowToFidoFlow;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;

public abstract class AbstractEnrollmentsApiPaymentsCoreTestModule extends AbstractCompleteEnrollmentsApiEnrollmentTestModule {
	/**
	 * This class makes all the steps for a complete no redirect journey for a payment:
	 * - GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array
	 * - Execute a full enrollment journey sending the origin extracted above, extract the enrollment ID, and store the refresh_token generated ensuring it has the nrp-consents scope
	 * - Call the GET enrollments endpoint
	 * - Expect a 200 response - Validate the response and check if the status is "AUTHORISED"
	 * - Call POST consent with valid payload
	 * - Expects 201 - Validate response
	 * - Call the POST sign-options endpoint with the consentID created
	 * - Expect 201 - Validate response and extract challenge
	 * - Call POST consent authorise with valid payload, signing the challenge with the compliant private key
	 * - Expects 204
	 * - Call GET Consent
	 * - Expects 200 - Validate if status is "AUTHORISED"
	 * - Calls the POST Token endpoint with the refresh_token grant, ensuring a token with the nrp-consent scope was issued
	 * - Calls the POST Payments Endpoint, using the access token with the nrp-consents, authorisationFlow as FIDO_FLOW and consentId with the corresponding ConsentID
	 * - Expects 201 - Validate Response
	 * - Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD
	 * - Expects Accepted state (ACSC) - Validate Response
	 */

	protected boolean isEnrollmentsFlowComplete = false;

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected void performPreAuthorizationSteps() {
		if (isEnrollmentsFlowComplete) {
			postAndValidateConsent();
			postAndValidateSignOptions();
		} else {
			super.performPreAuthorizationSteps();
		}
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		if (!isEnrollmentsFlowComplete) {
			isEnrollmentsFlowComplete = true;
			configPaymentsFlow();
			performPreAuthorizationSteps();
			makeRefreshTokenCall();
			executeTestSteps();
			onPostAuthorizationFlowComplete();
		} else {
			fireTestFinished();
		}
	}

	@Override
	protected void requestAuthorizationCode() {
		super.requestAuthorizationCode();
		callAndStopOnFailure(ExtractRefreshTokenFromTokenResponse.class);
	}

	protected void makeRefreshTokenCall() {
		userAuthorisationCodeAccessToken();
		call(new RefreshTokenRequestSteps(false, addTokenEndpointClientAuthentication)
			.skip(EnsureAccessTokenValuesAreDifferent.class, "Will not compare tokens")
			.skip(CompareIdTokenClaims.class, "Will not compare tokens"));
	}

	protected void configPaymentsFlow() {
		callAndStopOnFailure(AddResourceUrlToConfig.class);
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(SetPaymentAuthorisationFlowToFidoFlow.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		configureDictInfo();
	}

	protected void postAndValidateConsent() {
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		call(createOBBPreauthSteps()
			.skip(FAPIBrazilAddConsentIdToClientScope.class, "Consent id is not added to the scope in non-redirect tests"));
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(SaveAccessToken.class);
		runInBlock(currentClientString() + "Validate consents response", this::validatePostConsentResponse);
	}

	protected void validatePostConsentResponse() {
		callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
	}

	protected void postAndValidateSignOptions() {
		runInBlock("Post fido-sign-options - Expects 201", () -> {
			call(createPostSignOptionsSteps());
		});
		runInBlock("Validate fido-sign-options response", () -> {
			validateResourceResponse(postFidoSignOptionsValidator());
			callAndContinueOnFailure(EnsureResponseDoesNotHaveLinks.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(ExtractFidoRegistrationOptionsChallenge.class);
		});
	}

	protected PostEnrollmentsResourceSteps createPostSignOptionsSteps() {
		return new PostEnrollmentsResourceSteps(
			new PrepareToPostFidoSignOptions(),
			CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaims.class
		);
	}

	protected void postAndValidateConsentAuthorise() {
		runInBlock("Post consents authorise - Expects 204", () -> {
			env.putBoolean("is_client_data_registration", false);
			callAndStopOnFailure(GenerateFidoClientData.class);
			call(createConsentsAuthoriseSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected ConditionSequence createConsentsAuthoriseSteps() {
		return new PostEnrollmentsResourceSteps(
			new PrepareToPostConsentsAuthorize(),
			CreateFidoAuthorizeRequestBodyToRequestEntityClaims.class,
			true
		);
	}

	protected void getConsentAfterConsentAuthorise() {
		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
	}

	protected void fetchConsentToCheckStatus(String status, AbstractEnsurePaymentConsentStatusWas statusCondition) {
		runInBlock(String.format("Checking the created consent - Expecting %s status", status), () -> {
			callAndStopOnFailure(PaymentConsentIdExtractor.class);
			callAndStopOnFailure(AddJWTAcceptHeader.class);
			callAndStopOnFailure(ExpectJWTResponse.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(statusCondition.getClass(), Condition.ConditionResult.WARNING);
		});
	}

	@Override
	protected void executeTestSteps() {
		if (isEnrollmentsFlowComplete) {
			postAndValidateConsentAuthorise();
			getConsentAfterConsentAuthorise();
			postAndValidatePayment();
			executeFurtherSteps();
		}
	}

	protected void postAndValidatePayment() {
		userAuthorisationCodeAccessToken();
		runInBlock(currentClientString() + "Call pix/payments endpoint", () -> {
			callAndStopOnFailure(SetConsentIdInPaymentsRequestBody.class);
			call(getPixPaymentSequence());
		});
		runInBlock(currentClientString() + "Validate response", this::validatePaymentsResponse);
	}

	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence();
	}

	protected void validatePaymentsResponse() {
		call(postPaymentValidationSequence());

		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(5)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		validateFinalState();

	}

	protected ConditionSequence postPaymentValidationSequence() {
		return sequenceOf(
			condition(postPaymentValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
	}

	protected ConditionSequence getPaymentValidationSequence() {
		return sequenceOf(
			condition(getPaymentValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
	}

	protected void validateFinalState() {
		callAndStopOnFailure(EnsurePaymentStatusWasAcsc.class);
	}

	protected ConditionSequence getRepeatSequence() {
		return new ValidateSelfEndpoint()
			.replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeader.class),
				condition(CallProtectedResource.class)
			))
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values")
			.insertAfter(EnsureResourceResponseCodeWas200.class, condition(getPaymentPollStatusCondition()));
	}

	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrAccpOrAcpdV2.class;
	}

	protected abstract void executeFurtherSteps();

	protected abstract void configureDictInfo();

	protected abstract Class<? extends Condition> postPaymentConsentValidator();

	protected abstract Class<? extends Condition> postPaymentValidator();

	protected abstract Class<? extends Condition> getPaymentValidator();

	protected abstract Class<? extends Condition> postFidoSignOptionsValidator();

}
