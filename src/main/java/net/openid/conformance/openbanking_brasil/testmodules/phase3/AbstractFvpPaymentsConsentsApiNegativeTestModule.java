package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectBADPaymentType;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectInvalidPersonType;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetConsentDateInPast;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateInvalidFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.SetInvalidPaymentCurrency;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDataPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilPaymentConsentProdValues;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractFvpPaymentsConsentsApiNegativeTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilPaymentConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		saveBrazilPaymentConsent();

		call(exec().startBlock("Ensure error when Payment Type is \"BAD\""));
		executeUnhappyPath(SelectBADPaymentType.class, EnsureErrorResponseCodeFieldWasParametroInvalido.class);

		call(exec().startBlock("Ensure error when an invalid person type is sent"));
		executeUnhappyPath(SelectInvalidPersonType.class, EnsureErrorResponseCodeFieldWasParametroInvalido.class);

		call(exec().startBlock("Ensure error when invalid currency is sent"));
		executeUnhappyPath(SetInvalidPaymentCurrency.class, EnsureErrorResponseCodeFieldWasParametroInvalido.class);

		call(exec().startBlock("Ensure error when invalid date is sent"));
		executeUnhappyPath(SetConsentDateInPast.class, EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);

		call(exec().startBlock("Ensure x-fapi-interaction-id is validated"));
		executeInvalidFapiInteractionIdPath();
	}

	protected void executeUnhappyPath(Class<? extends Condition> selectWrongType, Class<? extends Condition> ensureError) {
		restoreBrazilPaymentConsent();
		callAndStopOnFailure(selectWrongType);
		call(getPaymentsConsentSequence().replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas422.class)));
		validateErrorMessage();
		callAndContinueOnFailure(ensureError, Condition.ConditionResult.FAILURE);
	}

	protected void executeInvalidFapiInteractionIdPath() {
		restoreBrazilPaymentConsent();
		call(getPaymentsConsentSequence().replace(CreateRandomFAPIInteractionId.class, condition(CreateInvalidFAPIInteractionId.class))
			.insertAfter(EnsureConsentResponseCodeWas201.class, condition(CheckForFAPIInteractionIdInResourceResponse.class))
			.replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas400Or422.class)));
		validateErrorMessage();
		if (OIDFJSON.getInt(env.getObject("consent_endpoint_response_full").get("status")) == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class, Condition.ConditionResult.FAILURE);
		}
	}

	protected void saveBrazilPaymentConsent() {
		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not extract brazilPaymentConsent")).getAsJsonObject();

		env.putObject("resource", "brazilPaymentConsentSave", brazilPaymentConsent.deepCopy());
	}

	protected void restoreBrazilPaymentConsent() {
		JsonObject brazilPaymentConsentSave = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsentSave"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not extract brazilPaymentConsentSave")).getAsJsonObject();

		env.putObject("resource", "brazilPaymentConsent", brazilPaymentConsentSave.deepCopy());
	}

	protected void validateErrorMessage() {
		callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
	}
}
