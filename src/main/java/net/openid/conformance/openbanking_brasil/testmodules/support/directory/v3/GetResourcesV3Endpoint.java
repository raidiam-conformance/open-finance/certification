package net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;

public class GetResourcesV3Endpoint extends AbstractGetXFromAuthServer {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/resources\\/v\\d+\\/resources)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "resources";
	}


	@Override
	protected String getApiVersionRegex() {
		return "^(3.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
