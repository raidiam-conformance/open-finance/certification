package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionsTimezoneTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.AbstractInvestmentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckCurrentTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactions;

public abstract class AbstractInvestmentsApiTransactionsTimezoneTestModule extends AbstractInvestmentsApiTestModule {

    @Override
    protected void executeParticularTestSteps() {
        runInBlock("GET Investments Transactions API without any query parameters", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactions.class);
            callAndStopOnFailure(CallProtectedResource.class);
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(investmentsTransactionsValidator(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(checkAllDates(), Condition.ConditionResult.FAILURE);
        });

        runInBlock(String.format("GET Investments Transactions API - Send query Params from%s=D-365 and to%s=D+0", transactionParameterName(), transactionParameterName()), () -> {
            callAndStopOnFailure(setFromTransactionTo365DaysAgo());
            callAndStopOnFailure(setToTransactionToToday());
            callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactions.class);
            callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
            callAndStopOnFailure(CallProtectedResource.class);
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(investmentsTransactionsValidator(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(checkTransactionRange(), Condition.ConditionResult.FAILURE);
        });

        runInBlock(String.format("GET Investments Transactions API - Send query Params from%s=D-370 and to%s=D+0", transactionParameterName(), transactionParameterName()), () -> {
            callAndStopOnFailure(setFromTransactionTo370DaysAgo());
            callAndStopOnFailure(setToTransactionToToday());
            callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactions.class);
            callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
            callAndStopOnFailure(CallProtectedResource.class);
            callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);

        });

		runInBlock("Deleting consent and validating if test was executed in the expected time interval", () -> {
		call(deleteConsent());
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("resource_endpoint_response_full");
		callAndContinueOnFailure(CheckCurrentTime.class, Condition.ConditionResult.FAILURE);
		});

    }

	@Override
	public void cleanup() {
		// We'll be performing validations after deleting the consent
	}

    protected abstract Class<? extends Condition> investmentsTransactionsValidator();
    protected abstract Class<? extends Condition> setFromTransactionTo365DaysAgo();
    protected abstract Class<? extends Condition> setFromTransactionTo370DaysAgo();
    protected abstract Class<? extends Condition> setToTransactionToToday();
    protected abstract Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl();
    protected abstract Class<? extends Condition> checkTransactionRange();
    protected abstract Class<? extends Condition> checkAllDates();
    protected abstract String transactionParameterName();

}
