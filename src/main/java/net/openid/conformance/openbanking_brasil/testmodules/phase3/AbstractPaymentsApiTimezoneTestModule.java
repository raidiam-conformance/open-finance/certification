package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckCurrentTime;

public abstract class AbstractPaymentsApiTimezoneTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {


	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();
		callAndContinueOnFailure(CheckCurrentTime.class, Condition.ConditionResult.FAILURE);
	}
}
