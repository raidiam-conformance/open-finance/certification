package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom;

import java.util.List;

public class CustomSchedulePaymentsWithRepeatedDates extends AbstractCustomSchedulePayment {

	@Override
	protected List<Integer> getNumberOfDaysInTheFutureList() {
		return List.of(1,1);
	}
}
