package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules;

import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToCreditFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractCreditFixedIncomesApiWrongPermissionsTest extends AbstractInvestmentsApiWrongPermissionsTestModule {

    @Override
    protected AbstractSetInvestmentApi investmentApi() {
        return new SetInvestmentApiToCreditFixedIncomes();
    }

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.CREDIT_FIXED_INCOMES;
	}
}
