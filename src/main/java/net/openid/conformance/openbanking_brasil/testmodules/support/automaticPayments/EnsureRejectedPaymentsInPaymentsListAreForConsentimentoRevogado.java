package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogado extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			JsonArray data = extractDataFromEnv(env);

			for (JsonElement paymentElement : data) {
				JsonObject payment = paymentElement.getAsJsonObject();
				String status = OIDFJSON.getString(payment.get("status"));
				if (status.equals("RJCT")) {
					String rejectionReasonCode = extractRejectionReasonCodeFromPayment(payment);
					if (!rejectionReasonCode.equals("CONSENTIMENTO_REVOGADO")) {
						throw error("Rejection reason code returned in the response does not match the expected rejection reason code",
							args("rejection reason code", rejectionReasonCode, "expected rejection reason code", "CONSENTIMENTO_REVOGADO"));
					}
				}
			}

			logSuccess("There were no payments with \"RJCT\" status with rejection reason code other than \"CONSENTIMENTO_REVOGADO\"");
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected JsonArray extractDataFromEnv(Environment env) throws ParseException {
		return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonArray("data"))
			.orElseThrow(() -> error("Unable to find element data in the response payload"));
	}

	protected String extractRejectionReasonCodeFromPayment(JsonObject payment) {
		return Optional.ofNullable(payment.getAsJsonObject("rejectionReason"))
			.map(rejectionReason -> rejectionReason.get("code"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find rejection reason code for a payment with \"RJCT\" status",
				args("payment", payment)));
	}
}
