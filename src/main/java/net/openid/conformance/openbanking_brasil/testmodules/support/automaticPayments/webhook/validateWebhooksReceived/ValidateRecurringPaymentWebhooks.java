package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived;

import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AbstractValidateWebhooksReceived;

public class ValidateRecurringPaymentWebhooks extends AbstractValidateWebhooksReceived {

	@Override
	protected int expectedWebhooksReceivedAmount() {
		return 1;
	}

	@Override
	protected String webhookType() {
		return "recurring_payment";
	}
}
