package net.openid.conformance.openbanking_brasil.testmodules.support.phase2.ensureErrorResponse;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase2.enums.CreateResourceErrorEnumV2;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasStatusResourcePendingAuthorisation extends AbstractEnsureErrorResponseCodeFieldWas {
	@Override
	protected List<String> getExpectedCodes() {
		return List.of(CreateResourceErrorEnumV2.STATUS_RESOURCE_PENDING_AUTHORISATION.toString());
	}
}
