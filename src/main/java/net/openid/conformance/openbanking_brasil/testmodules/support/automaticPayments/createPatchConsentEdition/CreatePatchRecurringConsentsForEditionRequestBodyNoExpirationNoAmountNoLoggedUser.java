package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition;

import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationNoAmountNoLoggedUser extends AbstractCreatePatchRecurringConsentsForEditionRequestBody {

	@Override
	@PreEnvironment(required = "resource")
	@PostEnvironment(required = "consent_endpoint_request")
	public Environment evaluate(Environment env) {
		env = super.evaluate(env);
		env.getObject("consent_endpoint_request").getAsJsonObject("data").remove("loggedUser");
		return env;
	}

	@Override
	protected String getExpirationDateTime() {
		return null;
	}

	@Override
	protected String getMaxVariableAmount() {
		return null;
	}
}
