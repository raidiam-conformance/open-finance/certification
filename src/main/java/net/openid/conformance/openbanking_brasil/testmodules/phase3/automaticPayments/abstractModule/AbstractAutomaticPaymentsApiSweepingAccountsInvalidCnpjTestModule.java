package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticUnhappyPaymentsConsentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureBusinessEntityIdentificationIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;

public abstract class AbstractAutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModule extends AbstractAutomaticUnhappyPaymentsConsentsFunctionalTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsureBusinessEntityIdentificationIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no 'Payment consent - Business Entity CNPJ' was informed.");
		}
	}

	@Override
	protected void performTestSteps() {
		performTestStep(
			"POST consents endpoint with sweeping accounts, sending 2 creditors with different CNPJ - Expects 422 PARAMETRO_INVALIDO",
			EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ.class,
			EnsureErrorResponseCodeFieldWasParametroInvalido.class
		);
	}
}
