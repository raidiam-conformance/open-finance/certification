package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractApiTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetUnarrangedAccountsOverdraftV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n4.GetUnarrangedAccountsOverdraftIdentificationV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n4.GetUnarrangedAccountsOverdraftListV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n4.GetUnarrangedAccountsOverdraftPaymentsV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n4.GetUnarrangedAccountsOverdraftScheduledInstalmentsV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n4.GetUnarrangedAccountsOverdraftWarrantiesV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "unarranged-accounts-overdraft_api_core_consents_v3-2_test-module_v2-4",
	displayName = "Validate structure of all unarranged overdraft API resources V2-4 with consents V3-2",
	summary = "Validates the structure of all unarranged overdraft API resources V2-4 with consents V3-2\n" +
		"• Creates a consent with all the permissions needed to access the Credit Operations API  (\"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"• Calls GET Unarranged Overdraft Contracts API V2-4\n" +
		"• Expects 200 - Fetches one of the IDs returned\n" +
		"• Calls GET Unarranged Overdraft Contracts API with ID V2-4\n" +
		"• Expects 200\n" +
		"• Calls GET Unarranged Overdraft Warranties API V2-4\n" +
		"• Expects 200\n" +
		"• Calls GET Unarranged Overdraft Payments API V2-4\n" +
		"• Expects 200\n" +
		"• Calls GET Unarranged Overdraft Contracts Instalments API V2-4\n" +
		"• Expects 200\n" +
		"11",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class UnarrangedAccountsOverdraftApiConsentsV3TestModuleV2n4 extends AbstractApiTestModulePhase2 {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
	}


	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class), condition(GetUnarrangedAccountsOverdraftV2Endpoint.class));
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.UNARRANGED_ACCOUNTS_OVERDRAFT;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractListResponseValidator() {
		return GetUnarrangedAccountsOverdraftListV2n4OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractResponseValidator() {
		return GetUnarrangedAccountsOverdraftIdentificationV2n4OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractGuaranteesResponseValidator() {
		return GetUnarrangedAccountsOverdraftWarrantiesV2n4OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractPaymentsResponseValidator() {
		return GetUnarrangedAccountsOverdraftPaymentsV2n4OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractInstallmentsResponseValidator() {
		return GetUnarrangedAccountsOverdraftScheduledInstalmentsV2n4OASValidator.class;
	}

	@Override
	protected EnumResourcesType getApiType() {
		return EnumResourcesType.UNARRANGED_ACCOUNT_OVERDRAFT;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
