package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRESCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilPaymentConsentProdValues;
import net.openid.conformance.sequence.ConditionSequence;

import java.util.Optional;

public abstract class AbstractFvpPaymentsConsentsApiEnforceQRESTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilPaymentConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		eventLog.startBlock("Validate payment initiation consent");
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

		call(getPaymentsConsentSequence());
		callAndStopOnFailure(CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida.class, Condition.ConditionResult.FAILURE);
		if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
			fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
		}
		callAndStopOnFailure(EnsureConsentResponseCodeWas422.class);

		callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasParametroNaoInformado.class);
		eventLog.endBlock();
	}

	@Override
	protected void configureDummyData() {
		super.configureDummyData();
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndContinueOnFailure(SelectQRESCodeLocalInstrument.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected ConditionSequence getPaymentsConsentSequence() {
		return super.getPaymentsConsentSequence()
			.replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas422.class).onFail(Condition.ConditionResult.FAILURE));
	}
}
