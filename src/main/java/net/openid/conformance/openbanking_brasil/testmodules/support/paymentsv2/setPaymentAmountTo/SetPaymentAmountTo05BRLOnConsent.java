package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractSetAmount;

public class SetPaymentAmountTo05BRLOnConsent extends AbstractSetAmount {
	@Override
	protected String getAmount() {
		return "0.50";
	}

	@Override
	protected String getEnvKey() {
		return "brazilPaymentConsent";
	}
}
