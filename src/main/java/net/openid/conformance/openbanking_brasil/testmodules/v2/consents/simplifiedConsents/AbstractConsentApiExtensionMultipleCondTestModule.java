package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureJointAccountCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus365;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureConsentStatusUpdateTimeHasNotChanged;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.PrepareToGetConsentExtensions;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateConsent422DependeMultipla;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInConsentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInGetSizeOne;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateIndefiniteExpirationTimeReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.sequence.ConditionSequence;


public abstract class AbstractConsentApiExtensionMultipleCondTestModule extends AbstractSimplifiedConsentRenewalTestModule {

	@Override
	protected abstract Class<? extends Condition> setConsentCreationValidation();

	@Override
	protected abstract Class<? extends Condition> setGetConsentValidator();

	@Override
	protected abstract Class<? extends Condition> setPostConsentExtensionValidator();

	@Override
	protected abstract Class<? extends Condition> setGetConsentExtensionValidator();

	@Override
	protected void configureClient(){
		env.putBoolean("continue_test", true);
		env.putString("metaOnlyRequestDateTime", "true");
		callAndContinueOnFailure(EnsureJointAccountCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestFinished();
		}
		super.configureClient();
	}


	@Override
	public void callExtensionEndpoints(){
		runInBlock("Validating post consent extension response", this::callPostConsentExtensionEndpoint);
	}
	@Override
	public ConditionSequence getPostConsentExtensionSequence() {
		ConditionSequence postConsentExtensionSequence = super.getPostConsentExtensionSequence();
		postConsentExtensionSequence
			.skip(EnsureConsentWasAuthorised.class, "Expecting a 422 in this test")
			.skip(ValidateIndefiniteExpirationTimeReturned.class, "Expecting a 422 in this test")
			.replace(EnsureConsentResponseCodeWas201.class,condition(EnsureConsentResponseCodeWas422.class))
			.insertAfter(EnsureConsentResponseCodeWas201.class,condition(setPostConsentExtensionValidator()))
			.replace(EnsureConsentStatusUpdateTimeHasNotChanged.class,condition(ValidateConsent422DependeMultipla.class));

		return postConsentExtensionSequence;
	}
	@Override
	protected void callGetConsentAndOrResourceUrlSequence(){
		call(new ValidateRegisteredEndpoints(
				sequenceOf(
					condition(GetConsentV3Endpoint.class),
					condition(GetResourcesV3Endpoint.class)
				)
			)
		);
	}

	@Override
	protected Class<? extends Condition> validateExpirationTimeReturned(){
		return ValidateExtensionExpiryTimeInConsentResponse.class;
	}

	@Override
	protected Class<? extends Condition> validateGetConsentExtensionEndpointResponse() {
		return ValidateExtensionExpiryTimeInGetSizeOne.class;
	}

	@Override
	protected Class<? extends Condition> buildConsentRequestBody() {
		return FAPIBrazilOpenBankingCreateConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setConsentExpirationTime() {
		return FAPIBrazilAddExpirationToConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {
		return CreateExtensionRequestTimeDayPlus365.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionEndpoint() {
		return PrepareToGetConsentExtensions.class;
	}
}
