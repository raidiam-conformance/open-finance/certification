package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1;

import com.google.gson.JsonObject;
import org.springframework.http.HttpMethod;


public class PostEnrollmentsOASValidatorV2n1 extends AbstractEnrollmentsOASValidatorV2n1 {

	@Override
	protected String getEndpointPathSuffix() {
		return "";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertDebtorAccountIssuerConstraint(data);
		assertCreationAndStatusUpdateDateTime(data);
	}
}
