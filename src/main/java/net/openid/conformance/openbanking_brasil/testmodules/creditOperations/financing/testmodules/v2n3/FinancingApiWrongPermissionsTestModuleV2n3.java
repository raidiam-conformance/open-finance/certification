package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.AbstractFinancingApiWrongPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsIdentificationV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsListV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsPaymentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsScheduledInstalmentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsWarrantiesV2n3OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "financings_api_wrong-permissions_test-module_v2-3",
	displayName = "Ensures API resource cannot be called with wrong permissions V2",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Credit Operations API  (\"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Financings Contracts API V2\n" +
		"\u2022 Expects 200 - Fetches one of the IDs returned\n" +
		"\u2022 Calls GET Financings Contracts API V2 with ID \n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Financings Warranties API V2\n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Financings Payments API V2\n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Financings Contracts Instalments API V2\n" +
		"\u2022 Expects 200\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Customer Personal or the Customer Business API (\"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\", \"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\",  \"RESOURCES_READ\")or (“CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ”,” \"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ”, \"RESOURCES_READ\")\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Financings Contracts API V2\n" +
		"\u2022 Expects 403\n" +
		"\u2022 Calls GET Financings Contracts API with ID V2\n" +
		"\u2022 Expects 403\n" +
		"\u2022 Calls GET Financings Warranties API V2\n" +
		"\u2022 Expects 403\n" +
		"\u2022 Calls GET Financings Payments API V2\n" +
		"\u2022 Expects 403\n" +
		"\u2022 Calls GET Financings Contracts Instalments API V2\n" +
		"\u2022 Expects 403",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class FinancingApiWrongPermissionsTestModuleV2n3 extends AbstractFinancingApiWrongPermissionsTestModule {

	@Override
	protected Class<? extends Condition> apiResourceContractListResponseValidator() {
		return GetFinancingsListV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractResponseValidator() {
		return GetFinancingsIdentificationV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractGuaranteesResponseValidator() {
		return GetFinancingsWarrantiesV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractPaymentsResponseValidator() {
		return GetFinancingsPaymentsV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractInstallmentsResponseValidator() {
		return GetFinancingsScheduledInstalmentsV2n3OASValidator.class;
	}

}
