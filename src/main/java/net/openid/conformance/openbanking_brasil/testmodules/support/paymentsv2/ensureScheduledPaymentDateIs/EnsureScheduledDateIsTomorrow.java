package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs;

public class EnsureScheduledDateIsTomorrow extends AbstractEnsureScheduledDateIsX {

	@Override
	protected long getNumberOfDaysToAddToCurrentDate() {
		return 1L;
	}
}
