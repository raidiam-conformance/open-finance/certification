package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators;

import com.google.common.collect.Sets;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.LocalInstrumentsEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentAccountTypesEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentConsentStatusEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PersonTypesEnumV2;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.ExtraField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Doc https://openbanking-brasil.github.io/openapi/swagger-apis/payments/2.0.0.yml
 * URL: /consents/{consentId}
 * URL: /consents
 * Api git hash: 35bf08ce0287a76880add849532a9cf83d8ba558
 */

@ApiName("Payment Initiation Consent V2")
public class PaymentConsentValidatorV2 extends AbstractJsonAssertingCondition {

	public static final Set<String> TYPES = Sets.newHashSet("PIX");
	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment environment) {

		JsonObject body = environment.getObject("consent_endpoint_response");
		assertHasField(body, ROOT_PATH);
		assertField(body, new ObjectField.Builder(ROOT_PATH).setValidator(this::assertInnerFields).build());
		return environment;
	}

	private void assertInnerFields(JsonObject body) {
		parseResponseBody(body, ROOT_PATH);

		assertField(body, new StringField
			.Builder("consentId")
			.setPattern("^urn:[a-zA-Z0-9][a-zA-Z0-9-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+$")
			.setMaxLength(256)
			.build());

		assertField(body,
			new DatetimeField
				.Builder("creationDateTime")
				.setPattern(DatetimeField.ALTERNATIVE_PATTERN)
				.setMaxLength(20)
				.build());

		assertField(body,
			new StringField
				.Builder("status")
				.setEnums(PaymentConsentStatusEnumV2.toSet())
				.setMaxLength(22)
				.build());

		String status = OIDFJSON.getString(body.get("status"));

		if (status.equalsIgnoreCase(PaymentConsentStatusEnumV2.AUTHORISED.toString()) || status.equalsIgnoreCase(PaymentConsentStatusEnumV2.CONSUMED.toString())) {
			assertField(body,
				new DatetimeField
					.Builder("expirationDateTime")
					.setMaxSecondsOlderThan(3600, "creationDateTime")
					.setPattern(DatetimeField.ALTERNATIVE_PATTERN)
					.setMaxLength(20)
					.build());
		}
		else {
			assertField(body,
				new DatetimeField
					.Builder("expirationDateTime")
					.setSecondsOlderThan(300, "creationDateTime")
					.setPattern(DatetimeField.ALTERNATIVE_PATTERN)
					.setMaxLength(20)
					.build());
		}

		assertField(body,
			new DatetimeField
				.Builder("statusUpdateDateTime")
				.setPattern(DatetimeField.ALTERNATIVE_PATTERN)
				.setMaxLength(20)
				.build());

		assertField(body,
			new ObjectField.Builder("loggedUser")
				.setValidator(this::assertLoggedUser)
				.build());


		assertField(body,
			new ObjectField
				.Builder("businessEntity")
				.setValidator(this::assertBusinessEntity)
				.setOptional()
				.setNullable()
				.build());

		assertField(body,
			new ObjectField
				.Builder("creditor")
				.setValidator(this::assertCreditor)
				.build());


		assertField(body,
			new ObjectField
				.Builder("payment")
				.setValidator(this::assertPayment)
				.build());

		assertField(body,
			new ObjectField
				.Builder("debtorAccount")
				.setValidator(this::assertDebtorAccount)
				.setOptional()
				.build());

		assertExtraFields(new ExtraField.Builder()
			.setPattern("^([A-Z]{4})(-)(.*)$")
			.build());
	}

	private void assertLoggedUser(JsonObject loggedUser) {
		assertField(loggedUser,
			new ObjectField
				.Builder("document")
				.setValidator(this::assertLoggedUserDocument)
				.build());
	}

	private void assertLoggedUserDocument(JsonObject document) {
		assertField(document,
			new StringField
				.Builder("identification")
				.setPattern("^\\d{11}$")
				.setMaxLength(11)
				.build());

		assertField(document,
			new StringField
				.Builder("rel")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}

	private void assertBusinessEntity(JsonObject businessEntity) {
		assertField(businessEntity,
			new ObjectField
				.Builder("document")
				.setValidator(this::assertBusinessEntityDocument)
				.build());
	}

	private void assertBusinessEntityDocument(JsonObject document) {
		assertField(document,
			new StringField
				.Builder("identification")
				.setPattern("^\\d{14}$")
				.setMaxLength(14)
				.build());

		assertField(document,
			new StringField
				.Builder("rel")
				.setPattern("^[A-Z]{4}$")
				.setMaxLength(4)
				.build());
	}

	private void assertCreditor(JsonObject creditor) {

		assertField(creditor,
			new StringField
				.Builder("personType")
				.setEnums(PersonTypesEnumV2.toSet())
				.setMaxLength(15)
				.build());

		assertField(creditor,
			new StringField
				.Builder("cpfCnpj")
				.setPattern("^\\d{11}$|^\\d{14}$")
				.setMinLength(11)
				.setMaxLength(14)
				.build());

		assertField(creditor,
			new StringField
				.Builder("name")
				.setPattern("^([A-Za-zÀ-ÖØ-öø-ÿ'' -]+)$")
				.setMaxLength(120)
				.build());
	}

	private void assertPayment(JsonObject payment) {

		assertField(payment,
			new StringField
				.Builder("type")
				.setMaxLength(3)
				.setEnums(TYPES)
				.build());


		DatetimeField.Builder dateBuilder = new DatetimeField.Builder("date");
		ObjectField.Builder scheduleBuilder = new ObjectField.Builder("schedule");

		if (payment.has("date")) {
			scheduleBuilder.setMustNotBePresent();
		}

		if (payment.has("schedule")) {
			dateBuilder.setMustNotBePresent();
		}

		assertField(payment,
			dateBuilder
				.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
				.setMaxLength(10)
				.build());

		assertField(payment,
			scheduleBuilder
				.setValidator(this::assertSchedule)
				.build());


		assertField(payment,
			new StringField
				.Builder("currency")
				.setPattern("^([A-Z]{3})$")
				.setMaxLength(3)
				.build());

		assertField(payment,
			new StringField
				.Builder("amount")
				.setMinLength(4)
				.setMaxLength(19)
				.setPattern("^((\\d{1,16}\\.\\d{2}))$")
				.build());

		assertField(payment,
			new StringField
				.Builder("ibgeTownCode")
				.setMinLength(7)
				.setMaxLength(7)
				.setPattern("^\\d{7}$")
				.setOptional()
				.build());

		assertField(payment,
			new ObjectField
				.Builder("details")
				.setValidator(this::assertDetails)
				.build());
	}

	private void assertSchedule(JsonObject schedule) {
		assertField(schedule, new ObjectField
			.Builder("single")
			.setValidator(this::assertSingle)
			.build());
	}

	private void assertSingle(JsonObject single) {
		assertField(single, new StringField
			.Builder("date")
			.setMaxLength(10)
			.setPattern("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")
			.build());
	}

	private void assertDetails(JsonObject details) {

		assertField(details,
			new StringField
				.Builder("localInstrument")
				.setEnums(LocalInstrumentsEnumV2.toSet())
				.setMaxLength(4)
				.build());
		String localInstrument = OIDFJSON.getString(details.get("localInstrument"));

		StringField.Builder qrCodeBuilder = new StringField
			.Builder("qrCode")
			.setPattern("[\\w\\W\\s]*")
			.setMaxLength(512);

		if (localInstrument.equalsIgnoreCase(LocalInstrumentsEnumV2.QRES.toString()) ||
			localInstrument.equalsIgnoreCase(LocalInstrumentsEnumV2.QRDN.toString())) {

			assertField(details, qrCodeBuilder.build());
		} else {
			assertField(details,
				qrCodeBuilder
					.setOptional()
					.build());
		}

		StringField.Builder proxyBuilder = new StringField.Builder("proxy");

		if (localInstrument.equalsIgnoreCase(LocalInstrumentsEnumV2.MANU.toString())) {
			proxyBuilder.setMustNotBePresent();
		}

		assertField(details, proxyBuilder
			.setPattern("[\\w\\W\\s]*")
			.setMaxLength(77)
			.build());


		assertField(details,
			new ObjectField
				.Builder("creditorAccount")
				.setValidator(this::assertDebtorAccount)
				.build());
	}

	private void assertDebtorAccount(JsonObject debtorAccount) {

		assertField(debtorAccount,
			new StringField
				.Builder("ispb")
				.setPattern("^[0-9]{8}$")
				.setMaxLength(8)
				.setMinLength(8)
				.build());


		assertField(debtorAccount,
			new StringField
				.Builder("number")
				.setPattern("^[0-9]{1,20}$")
				.setMinLength(1)
				.setMaxLength(20)
				.build());

		assertField(debtorAccount,
			new StringField
				.Builder("accountType")
				.setEnums(PaymentAccountTypesEnumV2.toSet())
				.build());

		String accountType = OIDFJSON.getString(debtorAccount.get("accountType"));
		StringField.Builder issuerBuilder = new StringField.Builder("issuer");

		if (accountType.equalsIgnoreCase(PaymentAccountTypesEnumV2.TRAN.toString())) {
			issuerBuilder.setOptional();
		}

		assertField(debtorAccount, issuerBuilder
			.setPattern("^[0-9]{1,4}$")
			.setMinLength(1)
			.setMaxLength(4)
			.build());
	}
}
