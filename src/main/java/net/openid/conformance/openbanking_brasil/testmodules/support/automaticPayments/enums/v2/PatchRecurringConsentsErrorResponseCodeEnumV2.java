package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.v2;

public enum PatchRecurringConsentsErrorResponseCodeEnumV2 {

	CONSENTIMENTO_NAO_PERMITE_CANCELAMENTO,
	CAMPO_NAO_PERMITIDO,
	PERMISSAO_INSUFICIENTE,
	DETALHE_EDICAO_INVALIDO,
	FALTAM_SINAIS_OBRIGATORIOS_PLATAFORMA
}
