package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class Validate529Or201ReturnedFromConsents extends AbstractCondition {
	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		JsonObject response = env.getObject("consent_endpoint_response_full");
		int status = OIDFJSON.getInt(response.get("status"));
		if (status == 529){
			logSuccess("Resource server returned 529, will continue polling");
			return env;
		} else if (status == 201) {
			env.putBoolean("correct_consent_response", true);
			logSuccess("Correct status obtained", args("status", status));
		} else {
			throw error("Failed to obtain expected status", args("expected status", "201", "obtained status", status));
		}
		return env;
	}
}
