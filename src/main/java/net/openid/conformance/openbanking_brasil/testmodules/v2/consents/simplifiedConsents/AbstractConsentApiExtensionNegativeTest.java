package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;

public abstract class AbstractConsentApiExtensionNegativeTest extends AbstractConsentApiExtensionNegativeTestModule{

	@Override
	protected abstract Class<? extends Condition> setPostConsentExtensionValidator();
	@Override
	protected abstract Class<? extends Condition> setGetConsentExtensionValidator();

	@Override
	protected abstract Class<? extends Condition> setConsentCreationValidation();

	@Override
	protected abstract Class<? extends Condition> setGetConsentValidator();

	boolean firstExtension = true;
	boolean secondExtension = false;
	boolean thirdExtension = false;

	@Override
	public void callExtensionEndpoints(){
		runInBlock("Validating post consent extension response - D+180", this::callPostConsentExtensionEndpoint);
		runInBlock("Validating post consent extension response expecting error - D+90", () -> {
			call(getPostConsentExtensionExpectingErrorSequence(EnsureConsentResponseCodeWas422.class, ValidateConsents422ExpiracaoInvalida.class, CreateExtensionRequestTimeDayPlus90.class));
		});
		runInBlock("Validating post consent extension response expecting error - D+180", () -> {
			call(getPostConsentExtensionExpectingErrorSequence(EnsureConsentResponseCodeWas422.class, ValidateConsents422ExpiracaoInvalida.class, ExtractSavedConsentExtensionExpirationTime.class));
		});
		runInBlock("Validating post consent extension response - D+365", this::callPostConsentExtensionEndpoint);
		runInBlock("Validating post consent extension response expecting error - D+730", () -> {
			call(getPostConsentExtensionExpectingErrorSequence(EnsureConsentResponseCodeWas422.class, ValidateConsents422ExpiracaoInvalida.class, CreateExtensionRequestTimeDayPlus730.class));
		});
		runInBlock("Validating post consent extension response expecting error - Indefinite Expiration", () -> {
			call(getPostConsentExtensionExpectingErrorSequence(EnsureConsentResponseCodeWas422.class, ValidateConsents422ExpiracaoInvalida.class, CreateIndefiniteConsentExpiryTime.class));
		});
		runInBlock("Validating post consent extension response - no expirationDateTime", this::callPostConsentExtensionEndpoint);
		runInBlock("Validating get consent extension response", this::callGetConsentExtensionEndpoint);
	}

	@Override
	protected void configureClient(){
		env.putString("metaOnlyRequestDateTime", "true");
		super.configureClient();
	}

	@Override
	protected void callGetConsentAndOrResourceUrlSequence(){
		call(new ValidateRegisteredEndpoints(
				sequenceOf(
					condition(GetConsentV3Endpoint.class),
					condition(GetResourcesV3Endpoint.class)
				)
			)
		);
	}

	@Override
	protected Class<? extends Condition> validateExpirationTimeReturned(){
		return ValidateExtensionExpiryTimeInConsentResponse.class;
	}

	@Override
	protected Class<? extends Condition> validateGetConsentExtensionEndpointResponse() {
		return ValidateExtensionExpiryTimeInGetSizeThree.class;
	}

	@Override
	protected Class<? extends Condition> buildConsentRequestBody() {
		return FAPIBrazilOpenBankingCreateConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setConsentExpirationTime() {
		return FAPIBrazilAddExpirationToConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {

		if(firstExtension){
			firstExtension = false;
			secondExtension = true;
			return CreateExtensionRequestTimeDayPlus180AndSaveTime.class;
		} else if(secondExtension && !thirdExtension){
			thirdExtension = true;
			return CreateExtensionRequestTimeDayPlus365.class;
		} else {
			return(thirdExtensionExpiryTimeCondition());
		}
	}

	@Override
	protected Class<? extends Condition> thirdExtensionExpiryTimeCondition() {
		return CreateEmptyConsentExpiryTime.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionEndpoint() {
		return PrepareToGetConsentExtensions.class;
	}

}
