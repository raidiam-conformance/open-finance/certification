package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.UnregisterDynamicallyRegisteredClient;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentApiSchedulingTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRejectionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PatchPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetPaymentAmountToLessThan1BRLValueAtConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetPaymentAmountToLessThan1BRLValueAtPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.CreatePatchConsentsRequestPayload;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.PrepareToPatchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SavePaymentIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractFvpPaymentApiSchedulingTestModule extends AbstractPaymentApiSchedulingTestModule {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SetPaymentAmountToLessThan1BRLValueAtPayments.class);
		callAndStopOnFailure(SetPaymentAmountToLessThan1BRLValueAtConsent.class);
	}

	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		call(new ValidateRegisteredEndpoints(
			sequenceOf(
				condition(GetPaymentV4Endpoint.class),
				condition(GetPaymentConsentsV4Endpoint.class))
			)
		);
		super.setupResourceEndpoint();
	}

	@Override
	protected void validateAllPayments() {
		callAndStopOnFailure(SavePaymentIds.class);
		super.validateAllPayments();
	}

	protected ConditionSequence getPatchConsentsValidationSequence() {
		return sequenceOf(
			condition(EnsureConsentResponseCodeWas200.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(PatchPaymentsConsentOASValidatorV4.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)
		);
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();
		testCleanup();
	}

	public void testCleanup() {
		runInBlock("Patch consents endpoint - Expects 200", () -> {
			useClientCredentialsAccessToken();
			callAndStopOnFailure(PrepareToPatchConsentRequest.class);
			callAndStopOnFailure(CreatePatchConsentsRequestPayload.class);
			call(new CallPatchAutomaticPaymentsConsentsEndpointSequence()
				.skip(CreatePatchRecurringPaymentsConsentsForRejectionRequestBody.class, "The patch payload was already created"));
			call(getPatchConsentsValidationSequence());
			callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
		});

		unregisterClient();
	}

	public void unregisterClient() {
		eventLog.startBlock(currentClientString() + "Unregister dynamically registered client");

		call(condition(UnregisterDynamicallyRegisteredClient.class)
			.skipIfObjectsMissing(new String[] {"client"})
			.onSkip(Condition.ConditionResult.INFO)
			.onFail(Condition.ConditionResult.WARNING)
			.dontStopOnFailure());

		eventLog.endBlock();
	}

	@Override
	protected void validateSelfLink(String responseFull) {
		String httpMethod = env.getString("http_method");

		if (httpMethod.equals("PATCH") && responseFull.contains("consent")) {
			eventLog.log("Validate Consent Self link", "Validate self link skipped for link.self returned in the response");
			return;
		}
		super.validateSelfLink(responseFull);
	}
}
