package net.openid.conformance.openbanking_brasil.testmodules.phase3;


import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;

public abstract class AbstractPaymentsConsentsApiDICTPixResponseTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(EnsureProxyPresentInConfig.class);
	}

}
