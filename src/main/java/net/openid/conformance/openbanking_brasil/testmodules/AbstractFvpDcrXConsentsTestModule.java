package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddClientAssertionToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddClientIdToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.ClientManagementEndpointAndAccessTokenRequired;
import net.openid.conformance.condition.client.CreateClientAuthenticationAssertionClaims;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.FAPIBrazilSignPaymentConsentRequest;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.condition.client.SignClientAuthenticationAssertion;
import net.openid.conformance.fapi1advancedfinal.FAPI1AdvancedFinalBrazilDCRHappyFlow;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.CopyClientJwksToClient;
import net.openid.conformance.openbanking_brasil.testmodules.support.CreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.InsertProdCpfValueToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.SignedPaymentConsentWithoutValidationSequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.CallDynamicRegistrationEndpointAndVerifySuccessfulResponse;
import net.openid.conformance.variant.ClientAuthType;

public abstract class AbstractFvpDcrXConsentsTestModule extends FAPI1AdvancedFinalBrazilDCRHappyFlow {

	protected ClientAuthType clientAuthType;

	protected abstract Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint();
	protected abstract Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest();
	protected abstract void insertConsentProdValues();
	protected abstract void runTests();

	@Override
	public void start() {
		setStatus(Status.RUNNING);
		super.onPostAuthorizationFlowComplete();
	}

	@Override
	protected void configureClient() {
		clientAuthType = getVariant(ClientAuthType.class);
		env.putString("config","resource.consentUrl","");
		env.putString("config","resource.resourceUrl","");
		super.configureClient();
	}

	@Override
	protected void setupResourceEndpoint() {
		// not needed as resource endpoint won't be called
	}

	@Override
	protected boolean scopeContains(String requiredScope) {
		// Not needed as scope field is optional
		return false;
	}

	@Override
	protected void callRegistrationEndpoint() {
		checkConsentURL();

		call(sequence(CallDynamicRegistrationEndpointAndVerifySuccessfulResponse.class));
		callAndContinueOnFailure(ClientManagementEndpointAndAccessTokenRequired.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1", "RFC7592-2");
		eventLog.endBlock();

		prepareToRunTests();
		runTests();
	}

	protected void checkConsentURL() {
		eventLog.startBlock("Checking consentURL");
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		callAndContinueOnFailure(getConsentEndpoint(), Condition.ConditionResult.WARNING);
		String consentUrl = env.getString("config", "resource.consentUrl");
		if (Strings.isNullOrEmpty(consentUrl)) {
			fireTestSkipped("Authorisation Server does not have consent url available");
		}
		eventLog.endBlock();
	}

	protected void prepareToRunTests() {
		eventLog.startBlock("Calling Token Endpoint using Client Credentials");
		call(callTokenEndpointShortVersion());
		eventLog.endBlock();

		eventLog.startBlock("Configuring dummy data");
		configureDummyData();
		eventLog.endBlock();
	}

	protected void configureDummyData() {
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		insertConsentProdValues();
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		callAndStopOnFailure(InsertProdCpfValueToConfig.class);
	}

	protected ConditionSequence callTokenEndpointShortVersion() {
		ConditionSequence sequence = sequenceOf(
			condition(CreateTokenEndpointRequestForClientCredentialsGrant.class),
			condition(setConsentScopeOnTokenEndpointRequest()),
			condition(AddClientIdToTokenEndpointRequest.class),
			condition(CreateClientAuthenticationAssertionClaims.class).dontStopOnFailure(),
			condition(SignClientAuthenticationAssertion.class).dontStopOnFailure(),
			condition(AddClientAssertionToTokenEndpointRequest.class).dontStopOnFailure(),
			condition(CallTokenEndpoint.class),
			condition(CheckIfTokenEndpointResponseError.class),
			condition(ExtractAccessTokenFromTokenResponse.class)
		);

		if (clientAuthType == ClientAuthType.MTLS) {
			sequence.skip(CreateClientAuthenticationAssertionClaims.class, "Not needed for MTLS")
				.skip(SignClientAuthenticationAssertion.class, "Not needed for MTLS")
				.skip(AddClientAssertionToTokenEndpointRequest.class, "Not needed for MTLS");
		}
		return sequence;
	}

	protected ConditionSequence getPaymentsConsentSequence() {
		return new SignedPaymentConsentWithoutValidationSequence()
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class, condition(CreatePaymentConsentRequest.class))
			.insertBefore(FAPIBrazilSignPaymentConsentRequest.class, condition(CopyClientJwksToClient.class));
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		// not needed as resource endpoint won't be called
	}

	@Override
	protected void logFinalEnv() {
		//Not needed here
	}
}
