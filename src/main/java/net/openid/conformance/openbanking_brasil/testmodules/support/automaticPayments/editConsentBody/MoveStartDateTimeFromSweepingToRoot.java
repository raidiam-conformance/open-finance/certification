package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class MoveStartDateTimeFromSweepingToRoot extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("config", "resource.brazilPaymentConsent"))
			.map(consentElement -> consentElement.getAsJsonObject().getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data field in consents payload"));

		String startDateTime = Optional.ofNullable(data.getAsJsonObject("recurringConfiguration"))
			.map(recurringConfiguration -> recurringConfiguration.getAsJsonObject("sweeping"))
			.map(sweeping -> sweeping.get("startDateTime"))
			.map(OIDFJSON::getString)
			.orElse("");

		if (!startDateTime.isEmpty()) {
			JsonObject sweeping = data.getAsJsonObject("recurringConfiguration").getAsJsonObject("sweeping");
			sweeping.remove("startDateTime");
			data.addProperty("startDateTime", startDateTime);
			logSuccess("The startDateTime field has been moved from the sweeping object to the root data object",
				args("data", data));
		} else {
			logSuccess("There was no startDateTime field to be moved from sweeping", args("data", data));
		}

		return env;
	}
}
