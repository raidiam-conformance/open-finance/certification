package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.common.v2;


import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api url: https://openbanking-brasil.github.io/openapi/swagger-apis/common/2.0.0-beta.2.yml
 * Api endpoint: GET /outages
 * Api version: 2.0.1
 */

@ApiName("Common Api GET Outages")
public class GetOutagesOASValidatorV2 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/common/swagger-common.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/outages";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
