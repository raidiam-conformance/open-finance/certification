package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractPixLocalInstrumentCondition extends AbstractCondition {

	@Override
	public final Environment evaluate(Environment env) {
		JsonObject obj = env.getObject("resource");
		obj = obj.getAsJsonObject("brazilPixPayment");
		if (env.getString("payment_is_array") != null) {
			for (JsonElement payment: obj.getAsJsonArray("data")) {
				obj = payment.getAsJsonObject();
				obj.addProperty("localInstrument", getPixLocalInstrument());
			}
		} else {
			obj = obj.getAsJsonObject("data");
			obj.addProperty("localInstrument", getPixLocalInstrument());
		}
		return env;
	}

	protected abstract String getPixLocalInstrument();

}
