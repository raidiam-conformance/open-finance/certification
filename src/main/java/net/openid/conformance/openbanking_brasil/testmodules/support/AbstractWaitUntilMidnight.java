package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public abstract class AbstractWaitUntilMidnight extends AbstractCondition {

	private final Supplier<ZonedDateTime> currentTimeSupplier;

	public AbstractWaitUntilMidnight(Supplier<ZonedDateTime> currentTimeSupplier) {
		this.currentTimeSupplier = currentTimeSupplier;
	}

	public AbstractWaitUntilMidnight() {
		this.currentTimeSupplier = () -> ZonedDateTime.now();
	}

	public long getSecondsUntilMidnight() {
		ZonedDateTime now = currentTimeSupplier.get();
		LocalDate today = now.toLocalDate();
		LocalDate nextDay = today.plusDays(1);
		LocalTime midnight = LocalTime.of(0, 0, 0);
		ZonedDateTime targetMidnight = ZonedDateTime.of(nextDay, midnight, now.getZone());

		return ChronoUnit.SECONDS.between(now, targetMidnight);
	}

	@Override
	public Environment evaluate(Environment env) {
		try {
			long secondsUntilMidnight = getSecondsUntilMidnight();

			logSuccess("Pausing until midnight");

			TimeUnit.SECONDS.sleep(secondsUntilMidnight);

			logSuccess("Woke up after waiting until midnight");
		} catch (InterruptedException e) {
			throw error("Interrupted while sleeping", e);
		}
		return env;
	}
}
