package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.abstractModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.AbstractAutomaticPaymentsDCRWebhookTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforceAbsenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.ExtractRecurringPaymentId;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooksOptionalAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrPatcOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureErrorResponse.EnsureErrorResponseCodeFieldWasConsentimentoPendeteAutorizacao;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus.CheckPaymentConsentPollStatusWasPartiallyAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasPartiallyAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractAutomaticPaymentsApiWebhookMultipleConsentsTestModule extends AbstractAutomaticPaymentsDCRWebhookTestModule {

	protected boolean firstPost = true;
	protected abstract Class<? extends Condition> paymentInitiationErrorValidator();

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		env.putString("recurring_total_amount", "300.00");
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckStatus("PARTIALLY_ACCEPTED", new EnsurePaymentConsentStatusWasPartiallyAccepted());
		env.unmapKey("access_token");
		validateGetConsentResponse();

		eventLog.startBlock(currentClientString() + "Call token endpoint");
		createAuthorizationCodeRequest();
		requestAuthorizationCode();
		requestProtectedResource();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected ConditionSequence postPaymentValidationSequence() {
		if (firstPost) {
			firstPost = false;
			return sequenceOf(
				condition(paymentInitiationErrorValidator())
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE),
				condition(EnsureResourceResponseCodeWas422.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE),
				condition(EnsureErrorResponseCodeFieldWasConsentimentoPendeteAutorizacao.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE)
			);
		}
		return super.postPaymentValidationSequence();
	}

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no 'Payment consent - Logged User CPF - Multiple Consents Test' was informed.");
		}
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
	}

	@Override
	protected void validateResponse() {
		env.mapKey("endpoint_response","resource_endpoint_response_full");
		call(postPaymentValidationSequence());
		env.unmapKey("endpoint_response");
		useClientCredentialsAccessToken();

		repeatSequence(this::getRepeatConsentSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(19)
			.validationSequence(this::getPaymentConsentValidationSequence)
			.run();

		userAuthorisationCodeAccessToken();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		call(new CallPixPaymentsEndpointSequence()
			.replace(SetProtectedResourceUrlToPaymentsEndpoint.class, condition(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class)));
		eventLog.endBlock();

		eventLog.startBlock(currentClientString() + "Validate response");
		validateSecondPostResponse();

		fetchConsentToCheckStatus("CONSUMED", new EnsurePaymentConsentStatusWasConsumed());
	}

	protected void validateSecondPostResponse() {
		call(postPaymentValidationSequence());
		useClientCredentialsAccessToken();

		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(5)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		validateFinalState();
	}

	protected ConditionSequence getRepeatConsentSequence() {
		return new ValidateSelfEndpoint()
			.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class))
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values")
			.insertAfter(EnsureResourceResponseCodeWas200.class, condition(getConsentPollStatusCondition()));
	}

	protected Class<? extends Condition> getConsentPollStatusCondition() {
		return CheckPaymentConsentPollStatusWasPartiallyAccepted.class;
	}

	protected ConditionSequence getPaymentConsentValidationSequence() {
		return sequenceOf(
			condition(setGetConsentValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas422.class))
			.replace(SetProtectedResourceUrlToPaymentsEndpoint.class, condition(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class));
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrPatcOrAccpOrAcpdV2.class;
	}

	@Override
	protected void handleAccessToken() {
		useClientCredentialsAccessToken();
	}

	@Override
	protected void validatePaymentWebhooks(){
		callAndStopOnFailure(ExtractRecurringPaymentId.class);
		super.validatePaymentWebhooks();
	}
	@Override
	protected void validatePaymentConsentWebhooks(){
		env.putBoolean("recurring_payment_consent_webhook_received", false);
		callAndStopOnFailure(ValidateRecurringConsentWebhooksOptionalAmount.class);
	}
}
