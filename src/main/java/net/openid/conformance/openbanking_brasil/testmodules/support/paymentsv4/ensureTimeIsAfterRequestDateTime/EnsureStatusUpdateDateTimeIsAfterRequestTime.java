package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureTimeIsAfterRequestDateTime;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EnsureStatusUpdateDateTimeIsAfterRequestTime extends AbstractEnsureTimeIsAfterRequestDateTime {

	@Override
	protected void checkIfDatesAreAfterRequestDateTime(JsonObject response, String requestDateTimeStr) {
		boolean isCorrect = true;
		JsonArray data = response.getAsJsonArray("data");

		for (JsonElement paymentElement : data) {
			String statusUpdateDateTime = OIDFJSON.getString(
				Optional.ofNullable(paymentElement.getAsJsonObject())
					.map(payment -> payment.get("statusUpdateDateTime"))
					.orElseThrow(() -> error("It was not possible to retrieve data.statusUpdateDateTime from the response"))
			);
			isCorrect = isAfterOrEqualsRequestDateTime(statusUpdateDateTime, requestDateTimeStr);
		}

		if (isCorrect) {
			logSuccess("the statusUpdateDateTime field is after the request time for every payment");
		} else {
			throw error("the statusUpdateDateTime field is not after the request time for every payment");
		}
	}
}
