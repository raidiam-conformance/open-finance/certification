package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACC extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonObject debtorAccount = Optional.ofNullable(
			env.getElementFromObject("resource_request_entity_claims", "data.debtorAccount")
		).orElseThrow(() -> error("Could not find debtorAccount inside the body")).getAsJsonObject();

		debtorAccount.remove("issuer");
		debtorAccount.addProperty("accountType", "CACC");

		logSuccess("Successfully edited request body to remove issuer and set accountType to CACC",
			args("request body", env.getObject("resource_request_entity_claims")));

		return env;
	}
}
