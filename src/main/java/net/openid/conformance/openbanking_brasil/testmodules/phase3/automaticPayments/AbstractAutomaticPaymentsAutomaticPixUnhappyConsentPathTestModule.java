package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LoadConsentsBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SaveConsentsBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractAutomaticPaymentsAutomaticPixUnhappyConsentPathTestModule extends AbstractAutomaticPaymentsAutomaticPixTestModule {
	/**
	 * This class should be extended by automatic pix tests that follow the steps bellow (one or multiple times):
	 *     - Call the POST recurring-consents endpoint with automatic pix fields, sending {value}
	 *     - Expect 422 {value} - Validate Error Message
	 * In order to properly create the recurring-consents request body, the user will need to provide the following fields:
	 *     - resource.debtorAccountIspb
	 *     - resource.debtorAccountIssuer
	 *     - resource.debtorAccountNumber
	 *     - resource.debtorAccountType
	 *     - resource.contractDebtorName
	 *     - resource.contractDebtorIdentification
	 *     - resource.creditorAccountIspb
	 *     - resource.creditorAccountIssuer
	 *     - resource.creditorAccountNumber
	 *     - resource.creditorAccountAccountType
	 *     - resource.creditorName
	 *     - resource.creditorCpfCnp
	 */

	protected abstract void performTestSteps();

	@Override
	protected void performPreAuthorizationSteps() {
		performTestSteps();
		fireTestFinished();
	}

	protected void performTestStep(String blockHeader,
								   Class<? extends Condition> consentsEditingCondition,
								   Class<? extends Condition> errorResponseCodeCondition) {
		runInBlock("Call the POST recurring-consents endpoint with automatic pix fields" + blockHeader, () -> {
			callAndStopOnFailure(LoadConsentsBody.class);
			callAndStopOnFailure(consentsEditingCondition);
			call(postConsentSequence());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey("endpoint_response", "consent_endpoint_response_full");
			callAndContinueOnFailure(errorResponseCodeCondition, Condition.ConditionResult.FAILURE);
			env.unmapKey("endpoint_response");
		});
	}

	protected ConditionSequence postConsentSequence() {
		return new OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps(
			addTokenEndpointClientAuthentication,
			true
		);
	}

	@Override
	protected void configureDictInfo() {
		super.configureDictInfo();
		callAndStopOnFailure(SaveConsentsBody.class);
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return null;
	}
}
