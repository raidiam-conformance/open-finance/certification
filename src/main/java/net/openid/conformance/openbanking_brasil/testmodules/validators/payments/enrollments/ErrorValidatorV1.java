package net.openid.conformance.openbanking_brasil.testmodules.validators.payments.enrollments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ExtraField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.StringField;

/**
 * @deprecated
 * OAS Validators must be used instead
 */
@Deprecated(forRemoval = true)
public class ErrorValidatorV1 extends AbstractJsonAssertingCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment, RESPONSE_ENV_KEY);

		assertField(body,
			new ObjectArrayField
				.Builder("errors")
				.setValidator(this::assertError)
				.setMinItems(1)
				.setMaxItems(13)
				.build());

		return environment;
	}

	private void assertError(JsonObject error) {
		parseResponseBody(error, "errors");

		assertField(error,
			new StringField
				.Builder("code")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(255)
				.build());

		assertField(error,
			new StringField
				.Builder("title")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(255)
				.build());

		assertField(error,
			new StringField
				.Builder("detail")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(2048)
				.build());

		assertExtraFields(new ExtraField.Builder()
			.setPattern("^([A-Z]{4})(-)(.*)$")
			.build());
	}
}
