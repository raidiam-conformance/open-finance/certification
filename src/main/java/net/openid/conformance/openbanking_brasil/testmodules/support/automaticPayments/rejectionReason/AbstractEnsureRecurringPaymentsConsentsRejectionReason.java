package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectionReasonEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public abstract class AbstractEnsureRecurringPaymentsConsentsRejectionReason extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			String rejectionReason = OIDFJSON.getString(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.map(body -> body.getAsJsonObject().getAsJsonObject("data"))
					.map(data -> data.getAsJsonObject("rejection"))
					.map(rejection -> rejection.getAsJsonObject("reason"))
					.map(reason -> reason.get("code"))
					.orElseThrow(() -> error("Unable to find element data.rejection.reason.code in the response payload"))
			);
			if (rejectionReason.equals(expectedRejectionReason().toString())) {
				logSuccess("Rejection reason returned in the response matches the expected rejection reason");
			} else {
				throw error("Rejection reason returned in the response does not match the expected rejection reason",
					args("rejection reason", rejectionReason,
						"expected rejection reason", expectedRejectionReason().toString())
				);
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected abstract RecurringPaymentsConsentsRejectionReasonEnum expectedRejectionReason();
}
