package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureBrazilCpf;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.EnsureDcrClientExists;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPreFlightCertCheckPayments  extends AbstractClientCredentialsGrantFunctionalTestModule {
	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return super.createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence)
			.replace(SetConsentsScopeOnTokenEndpointRequest.class, condition(SetPaymentsScopeOnTokenEndpointRequest.class));
	}

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(EnsureBrazilCpf.class);
		callAndStopOnFailure(EnsureDcrClientExists.class);
		call(new ValidateWellKnownUriSteps());
	}

	@Override
	protected void runTests() {
		call(new ValidateRegisteredEndpoints(getPaymentAndPaymentConsentEndpoints()));
	}

	@Override
	protected void logFinalEnv() {
		//Not needed here
	}

	protected abstract ConditionSequence getPaymentAndPaymentConsentEndpoints();
}
