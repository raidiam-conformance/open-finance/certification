package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonObject;

public class EditRecurringPaymentsAutomaticConsentBodyToSetInvalidFixedAmount extends AbstractEditRecurringPaymentsAutomaticConsentBody {

	@Override
	protected void updateAutomaticObject(JsonObject automatic) {
		automatic.addProperty("fixedAmount", "1");
		automatic.remove("maximumVariableAmount");
		automatic.remove("minimumVariableAmount");
	}

	@Override
	protected String logMessage() {
		return "set an invalid value for the \"fixedAmount\" field";
	}
}
