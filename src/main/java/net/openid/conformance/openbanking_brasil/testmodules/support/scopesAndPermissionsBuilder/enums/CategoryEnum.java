package net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums;

import java.util.Set;

public interface CategoryEnum {

	Set<PermissionsEnum> getPermissions();

	Set<ScopesEnum> getScopes();
}
