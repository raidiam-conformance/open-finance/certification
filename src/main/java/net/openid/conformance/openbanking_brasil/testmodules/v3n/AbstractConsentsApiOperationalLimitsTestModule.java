package net.openid.conformance.openbanking_brasil.testmodules.v3n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.EnsureResourceResponseReturnedJsonContentType;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.consent.v2.OpenBankingBrazilPreAuthorizationConsentApiV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.AbstractOperationalLimitsTestModule;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractConsentsApiOperationalLimitsTestModule extends AbstractOperationalLimitsTestModule {


	protected abstract Class<? extends Condition> setGetConsentValidator();

	protected abstract Class<? extends Condition> setPostConsentValidator();


	private static final int NUMBER_OF_EXECUTIONS = 600;


	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
	}

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(GetConsentV3Endpoint.class));
		//Arbitrary resource
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		String productType = env.getString("config", "consent.productType");
		if (productType.equals("business")) {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.BUSINESS_REGISTRATION_DATA).build();
		} else {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA).build();
		}		super.configureClient();
	}

	@Override
	protected void validateClientConfiguration() {
		super.validateClientConfiguration();
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS, OPFScopesEnum.CUSTOMERS, OPFScopesEnum.OPEN_ID).build();
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		env.putString("proceed_with_test", "true");
		return new OpenBankingBrazilPreAuthorizationConsentApiV2(addTokenEndpointClientAuthentication, true);
	}


	@Override
	protected void performPreAuthorizationSteps() {

		call(createOBBPreauthSteps());

		callAndContinueOnFailure(EnsureConsentResponseCodeWas201.class, Condition.ConditionResult.WARNING);

		if (getResult() == Result.WARNING) {
			fireTestSkipped("The consent creation request has not responded with 201." +
				"This implies that the server does not support the tested API which will be tracked on this test plan as a SKIPPED");
		} else {
			callAndContinueOnFailure(EnsureResourceResponseReturnedJsonContentType.class, Condition.ConditionResult.FAILURE);
			env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
			env.mapKey("resource_endpoint_response", "consent_endpoint_response");

			callAndContinueOnFailure(FAPIBrazilConsentEndpointResponseValidatePermissions.class, Condition.ConditionResult.WARNING);

			if (getResult() == Result.WARNING) {
				fireTestSkipped("The consent creation request did not return the valid PERMISSIONS for the tested API. " +
					"This implies that the server does not support the tested API which will be tracked on this test plan as a SKIPPED");
			} else {
				env.unmapKey("consent_endpoint_response");


				callAndContinueOnFailure(setPostConsentValidator(), Condition.ConditionResult.FAILURE);

				call(exec().startBlock("Validating get consent response"));
				callAndStopOnFailure(ConsentIdExtractor.class);
				callAndStopOnFailure(PrepareToFetchConsentRequest.class);
				callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(setGetConsentValidator(), Condition.ConditionResult.FAILURE);
				callAndStopOnFailure(FAPIBrazilAddConsentIdToClientScope.class);
			}

		}

	}

	@Override
	protected void requestProtectedResource() {
		for (int i = 0; i < NUMBER_OF_EXECUTIONS; i++) {
			checkAccessTokenExpirationTime();
			eventLog.startBlock(currentClientString() + String.format("[%d] Calling consent endpoint.", i + 1));
			call(getPreConsentWithBearerTokenSequence());
			eventLog.endBlock();

			if (i == 0) {
				runInLoggingBlock(() -> call(getValidateConsentResponsePollingSequence()));
			}

		}
	}

	@Override
	protected void refreshAccessToken() {
		runInLoggingBlock(() -> {
			eventLog.startBlock(currentClientString() + "Refreshing access token.");
			call(createGetAccessTokenWithClientCredentialsSequence(addTokenEndpointClientAuthentication));
			eventLog.endBlock();
		});

	}

	@Override
	protected void validateResponse() {
		// Not needed for the test
	}

	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainAccessTokenWithClientCredentials(clientAuthSequence);
	}

	protected ConditionSequence getValidateConsentResponsePollingSequence() {
		return sequenceOf(
			condition(EnsureConsentResponseCodeWas200.class),
			condition(setGetConsentValidator())
		);
	}

	protected ConditionSequence getPreConsentWithBearerTokenSequence() {
		return sequenceOf(
			condition(ConsentIdExtractor.class),
			condition(PrepareToFetchConsentRequest.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
		);
	}

	@Override
	protected void performPostAuthorizationFlow() {
		eventLog.startBlock(currentClientString() + "Call token endpoint");
		call(createGetAccessTokenWithClientCredentialsSequence(addTokenEndpointClientAuthentication));
		requestProtectedResource();
		onPostAuthorizationFlowComplete();
	}
}
