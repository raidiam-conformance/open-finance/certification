package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractRefreshTokenFromTokenResponse;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractUnhappyAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRejectionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRevocationRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToRemoveExpirationDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revocationReason.EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedBy.EnsureRecurringPaymentsConsentsRevokedByUsuario;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedFrom.EnsureRecurringPaymentsConsentsRevokedFromIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.EnsureErrorResponseCodeFieldWasConsentimentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.sequence.client.RefreshTokenRequestExpectingErrorSteps;
import org.springframework.http.HttpStatus;

public abstract class AbstractAutomaticPaymentsApiRevokedConsentTestModule extends AbstractUnhappyAutomaticPaymentsFunctionalTestModule {

	protected abstract Class<? extends Condition> patchPaymentConsentValidator();

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected HttpStatus expectedResponseCode() {
		return HttpStatus.UNAUTHORIZED;
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToRemoveExpirationDateTime.class);
	}


	@Override
	protected void requestProtectedResource() {
		runInBlock("Call PATCH consents - Expects 200", this::makePatchConsentsRequest);
		super.requestProtectedResource();
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();

		runInBlock("Refreshing Access Token expecting 400", () -> call(sequenceOf(
			condition(ExtractRefreshTokenFromTokenResponse.class),
			new RefreshTokenRequestExpectingErrorSteps(false, addTokenEndpointClientAuthentication)
		)));
	}

	@Override
	protected void validateRevokedConsent() {
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRevokedByUsuario.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRevokedFromIniciadora.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario.class, Condition.ConditionResult.FAILURE);
	}

	protected void makePatchConsentsRequest() {
		useClientCredentialsAccessToken();

		Class<? extends Condition> consentRevocationCondition = isNewerVersion() ?
			CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody.class :
			CreatePatchRecurringPaymentsConsentsForRevocationRequestBody.class;

		call(new CallPatchAutomaticPaymentsConsentsEndpointSequence()
			.replace(CreatePatchRecurringPaymentsConsentsForRejectionRequestBody.class,
				condition(consentRevocationCondition)));
		callAndContinueOnFailure(patchPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
		userAuthorisationCodeAccessToken();
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas ensureErrorResponseCodeFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasConsentimentoInvalido();
	}
}
