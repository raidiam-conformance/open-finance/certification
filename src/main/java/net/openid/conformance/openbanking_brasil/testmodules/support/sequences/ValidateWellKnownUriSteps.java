package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.condition.client.CheckDiscEndpointDiscoveryUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureWellKnownUriIsRegistered;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class ValidateWellKnownUriSteps extends AbstractConditionSequence {


	@Override
	public void evaluate() {
		call(exec().startBlock("Validating Well-Known URI"));
		callAndStopOnFailure(CheckDiscEndpointDiscoveryUrl.class);
		callAndStopOnFailure(EnsureWellKnownUriIsRegistered.class);
		call(exec().endBlock());

	}
}
