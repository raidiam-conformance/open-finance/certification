package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsentsApiEnforceMANUTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_manu-fail_test-module_v4",
	displayName = "Payments Consents API test module for manu local instrument",
	summary = "Ensure error if a proxy or qrcode is sent when localInstrument is MANU (Ref Error 2.2.2.8)" +
		"\u2022 Calls POST Consents Endpoint with localInstrument as MANU and QRCode with email on the payload\n" +
		"\u2022 Expects 422 DETALHE_PAGAMENTO_INVALIDO - validate error message\n" +
		"\u2022 Calls POST Consents Endpoint with localInstrument as MANU and proxy with email  on the payload\n" +
		"\u2022 Expects 422 DETALHE_PAGAMENTO_INVALIDO - validate error message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public  class PaymentsConsentsApiEnforceMANUTestModuleV4 extends AbstractPaymentsConsentsApiEnforceMANUTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

}
