package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilSignPaymentInitiationRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiSweepingAccountsWrongCreditorTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsManualObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LoadPaymentsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SavePaymentsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetUserDefinedCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToAddProxyFieldAsCreditorCpfCnpj;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims.EditRecurringPaymentRequestClaimsToRemoveCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims.EditRecurringPaymentRequestClaimsToSetDifferentCreditorAccountInfo;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims.EditRecurringPaymentRequestClaimsToSetDifferentIdentification;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims.EditRecurringPaymentRequestClaimsToSetDifferentProxy;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import org.springframework.http.HttpStatus;

@PublishTestModule(
	testName = "automatic-payments_api_sweeping-accounts-wrong-creditor_test-module_v2",
	displayName = "automatic-payments_api_sweeping-accounts-wrong-creditor_test-module_v2",
	summary = "Ensure a payment cannot be executed for a creditor different from the one registered at the consent\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, sending the creditor account as the CPF informed at the config, the amount value as 600.00\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED\n" +
		"\n" +
		"• Call the POST recurring-payments endpoint with the amount as 300.00, but with a different document information\n" +
		"• Expect 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO or 201 - Validate Response\n" +
		"If a 201 is returned:\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"\n" +
		"• Call the POST recurring-payments endpoint with the amount as 300.00, but with a different creditor proxy, but the correct creditorAccount information\n" +
		"• Expect 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO or 201 - Validate Response\n" +
		"If a 201 is returned:\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"\n" +
		"• Call the POST recurring-payments endpoint with the amount as 300.00, but with a different creditorAccount information, but the correct proxy\n" +
		"• Expect 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO or 201 - Validate Response\n" +
		"If a 201 is returned:\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"\n" +
		"• Call the POST recurring-payments endpoint with the amount as 300.00, but without sending the creditorAccount information\n" +
		"• Expect 422 PARAMETRO_NAO_INFORMADO - Validate Error Message\n" +
		"\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 200 - Validate Response and ensure status is AUTHORISED",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorAccountIspb",
		"resource.creditorAccountIssuer",
		"resource.creditorAccountNumber",
		"resource.creditorAccountAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiSweepingAccountsWrongCreditorTestModuleV2 extends AbstractAutomaticPaymentsApiSweepingAccountsWrongCreditorTestModule {

	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(SavePaymentsRequestBody.class);

		postAndValidatePaymentsWithInvalidPayload(
			EditRecurringPaymentRequestClaimsToSetDifferentIdentification.class,
			"with a different document information",
			"PAGAMENTO_DIVERGENTE_CONSENTIMENTO or 201",
			new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento(),
			true,
			new EnsureRecurringPaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento()
		);

		postAndValidatePaymentsWithInvalidPayload(
			EditRecurringPaymentRequestClaimsToSetDifferentProxy.class,
			"with a different creditor proxy, but the correct creditorAccount information",
			"PAGAMENTO_DIVERGENTE_CONSENTIMENTO or 201",
			new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento(),
			true,
			new EnsureRecurringPaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento()
		);

		postAndValidatePaymentsWithInvalidPayload(
			EditRecurringPaymentRequestClaimsToSetDifferentCreditorAccountInfo.class,
			"with a different creditorAccount information, but the correct proxy",
			"PAGAMENTO_DIVERGENTE_CONSENTIMENTO or 201",
			new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento(),
			true,
			new EnsureRecurringPaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento()
		);

		postAndValidatePaymentsWithInvalidPayload(
			EditRecurringPaymentRequestClaimsToRemoveCreditorAccount.class,
			"without sending the creditorAccount information",
			"PARAMETRO_NAO_INFORMADO",
			new EnsureErrorResponseCodeFieldWasParametroNaoInformado(),
			false,
			null
		);

		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
	}

	protected void postAndValidatePaymentsWithInvalidPayload(Class<? extends Condition> payloadEditingCondition,
															 String blockHeaderInfo,
															 String expectedError,
															 AbstractEnsureErrorResponseCodeFieldWas errorCodeCondition,
															 boolean allowsFor201,
															 AbstractEnsureRecurringPaymentsRejectionReason rejectionReasonCondition) {
		runInBlock(String.format("Call the POST recurring-payments endpoint with the amount as 300.00, %s - Expects 422 %s",
			blockHeaderInfo, expectedError), () ->
		{
			userAuthorisationCodeAccessToken();
			callAndStopOnFailure(LoadPaymentsRequestBody.class);

			Class<? extends Condition> statusCodeCondition = allowsFor201 ?
				EnsureResourceResponseCodeWas201Or422.class :
				EnsureResourceResponseCodeWas422.class;
			call(getPixPaymentSequence().insertBefore(FAPIBrazilSignPaymentInitiationRequest.class, condition(payloadEditingCondition))
				.replace(EnsureResourceResponseCodeWas201.class, condition(statusCodeCondition).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)));

			int status = getResponseStatus();
			if (status == HttpStatus.CREATED.value() && allowsFor201) {
				call(postPaymentValidationSequence());
				runRepeatSequence();
				validateFinalState();
				callAndContinueOnFailure(rejectionReasonCondition.getClass(), Condition.ConditionResult.FAILURE);
			} else {
				callAndContinueOnFailure(paymentInitiationErrorValidator(), Condition.ConditionResult.FAILURE);
				env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				callAndContinueOnFailure(errorCodeCondition.getClass(), Condition.ConditionResult.FAILURE);
				env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
			}
		});
	}

	@Override
	protected void validateFinalState() {
		callAndContinueOnFailure(EnsurePaymentStatusWasRjct.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.replace(SetProtectedResourceUrlToPaymentsEndpoint.class, condition(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class));
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetUserDefinedCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentsBodyToAddProxyFieldAsCreditorCpfCnpj.class);
		callAndStopOnFailure(AddRiskSignalsManualObjectToPixPaymentBody.class);
	}

	@Override
	protected void configureDictInfo() {}

	@Override
	protected void validateResponse() {}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class), condition(GetAutomaticPaymentPixRecurringV2Endpoint.class));
	}

}
