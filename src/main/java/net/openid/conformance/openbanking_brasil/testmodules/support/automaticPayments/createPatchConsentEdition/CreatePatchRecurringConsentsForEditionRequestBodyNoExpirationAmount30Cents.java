package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition;

public class CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount30Cents extends AbstractCreatePatchRecurringConsentsForEditionRequestBody {

	@Override
	protected String getExpirationDateTime() {
		return null;
	}

	@Override
	protected String getMaxVariableAmount() {
		return "0.30";
	}
}
