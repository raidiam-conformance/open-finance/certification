package net.openid.conformance.openbanking_brasil.testmodules.support.validateField;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

public class ValidateConsentsOperationalFieldV2 extends AbstractValidateField {

	@Override
	protected void validateConsentUrl(JsonObject config) {}

	@Override
	protected void validateBrazilCpf(JsonObject config) {
		JsonElement brazilCpfOperationalElement = findElementOrThrowError(config, "$.resource.brazilCpfOperational");
		String brazilCpfOperational = OIDFJSON.getString(brazilCpfOperationalElement);
		if (Strings.isNullOrEmpty(brazilCpfOperational) || brazilCpfOperational.length() != CPF_LENGTH) {
			throw error("brazilCpfOperational is not valid", args("brazilCpfOperational", brazilCpfOperational));
		}
	}

	@Override
	protected void validateProductType(JsonObject config) {
		JsonElement productTypeElement = findElementOrThrowError(config, "$.consent.productType");
		String productType = OIDFJSON.getString(productTypeElement);
		if (Strings.isNullOrEmpty(productType) || (!productType.equals("business") && !productType.equals("personal"))) {
			throw error("Product type (Business or Personal) must be specified in the test configuration");
		}

		if (productType.equals("business")) {
			JsonElement brazilCnpjOperationalBusinessElement = findElementOrThrowError(config, "$.resource.brazilCnpjOperationalBusiness");
			String brazilCnpjOperationalBusiness = OIDFJSON.getString(brazilCnpjOperationalBusinessElement);
			if(Strings.isNullOrEmpty(brazilCnpjOperationalBusiness) || brazilCnpjOperationalBusiness.length() != CNPJ_LENGTH) {
				throw error("brazilCnpjOperationalBusiness is not valid", args("brazilCnpjOperationalBusiness", brazilCnpjOperationalBusiness));
			}
		}
	}

	@Override
	protected int getConsentVersion() {
		return 0;
	}
}
