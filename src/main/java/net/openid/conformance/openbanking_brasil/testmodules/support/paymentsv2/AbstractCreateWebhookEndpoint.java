package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.runner.TestDispatcher;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractCreateWebhookEndpoint extends AbstractCondition {
	@Override
	@PreEnvironment(strings = {"base_url"})
	public Environment evaluate(Environment env) {
		String baseUrl = env.getString("base_url");
		baseUrl = baseUrl.replaceFirst(TestDispatcher.TEST_PATH, TestDispatcher.TEST_MTLS_PATH);
		if (baseUrl.isEmpty()) {
			throw error("Base URL is empty");
		}
		String webhookUri = baseUrl + getExpectedUrlPath();
		env.putString("webhook_uri_" + getValueFromEnv(), webhookUri);
		logSuccess("Created webhook URI", args("webhook_uri", webhookUri));
		return env;
	}

	abstract protected String getValueFromEnv();

	abstract protected String getExpectedUrlPath();
}
