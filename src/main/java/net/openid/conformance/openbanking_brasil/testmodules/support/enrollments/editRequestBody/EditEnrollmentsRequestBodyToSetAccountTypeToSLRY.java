package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditEnrollmentsRequestBodyToSetAccountTypeToSLRY extends AbstractCondition {

	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";

	@Override
	@PreEnvironment(required = REQUEST_CLAIMS_KEY)
	public Environment evaluate(Environment env) {
		JsonObject debtorAccount = Optional.ofNullable(
			env.getElementFromObject(REQUEST_CLAIMS_KEY, "data.debtorAccount")
		).orElseThrow(() -> error("Could not find debtorAccount inside the body")).getAsJsonObject();

		debtorAccount.addProperty("accountType", "SLRY");

		logSuccess("Successfully edited request body to set accountType to SLRY",
			args("request body", env.getObject(REQUEST_CLAIMS_KEY)));

		return env;
	}
}
