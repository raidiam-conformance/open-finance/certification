package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class EditRecurringPaymentsBodyToSetDateToOneDayInTheFuture extends AbstractEditRecurringPaymentsBodyToSetDate {

	@Override
	protected String getDate() {
		LocalDate futureDate = LocalDate.now(ZoneId.of("UTC-3")).plusDays(1);
		return futureDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
}
