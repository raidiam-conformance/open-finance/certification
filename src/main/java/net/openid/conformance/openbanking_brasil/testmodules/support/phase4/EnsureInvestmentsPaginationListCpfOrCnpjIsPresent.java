package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class EnsureInvestmentsPaginationListCpfOrCnpjIsPresent extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config")
    public Environment evaluate(Environment env) {
        JsonObject resource = env.getElementFromObject("config", "resource").getAsJsonObject();
        JsonElement cpfElement = resource.get("brazilCpfPaginationList");
        JsonElement cnpjElement = resource.get("brazilCnpjPaginationList");

        if (cpfElement == null && cnpjElement == null) {
            env.putBoolean("continue_test", false);
            throw error("Neither CPF nor CNPJ for Pagination List test were provided. Test cannot be executed.");
        }
        if (cpfElement != null) {
            resource.addProperty("brazilCpf", OIDFJSON.getString(cpfElement));
            log("brazilCpf changed to the value in brazilCpf for Pagination List Test",
                args("brazilCpf", OIDFJSON.getString(resource.get("brazilCpf"))));
        }
        if (cnpjElement != null) {
            resource.addProperty("brazilCnpj", OIDFJSON.getString(cnpjElement));
            log("brazilCnpj changed to the value in brazilCnpj for Pagination List Test",
                args("brazilCnpj", OIDFJSON.getString(resource.get("brazilCnpj"))));
        }

        logSuccess("CPF and/or CNPJ values for Pagination List are being used.");

        return env;
    }
}
