package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.abstractModule;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.ExtractSpecifiedResourceApiAccountIdsFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingCurrentAccountTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardBills;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardLimits;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCreditCardAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddSpecifiedPageSizeParameterToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromDueDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureOnlyOneRecordWasReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractAllSpecifiedApiIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.FetchSpecifiedNumberOfExtractedApiIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.OperationalLimitsToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetSpecifiedValueToSpecifiedUrlParameter;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateNumberOfRecordsPage1;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateNumberOfRecordsPage2;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetCreditCardsAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.EnsureNumberOfTotalRecordsIsAtLeast20FromData;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.AbstractOperationalLimitsTestModule;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.variant.ClientAuthType;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public abstract class AbstractCreditCardsApiOperationalLimitsTestModule extends AbstractOperationalLimitsTestModule {

    protected abstract Class<? extends Condition> cardIdentificationResponseValidator();
    protected abstract Class<? extends Condition> creditCardAccountsTransactionResponseValidator();
    protected abstract Class<? extends Condition> creditCardBillValidator();
    protected abstract Class<? extends Condition> creditCardAccountsLimitsResponseValidator();
    protected abstract Class<? extends Condition> creditCardAccountsTransactionCurrentResponseValidator();
    protected abstract Class<? extends Condition> cardAccountsDataResponseResponseValidator();

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private static final int NUMBER_OF_IDS_TO_FETCH = 2;
    private static final String API_RESOURCE_ID = "creditCardAccountId";

    private static final int REQUIRED_NUMBER_OF_RECORDS = 20;

    @Override
    protected void configureClient() {
        call(new ValidateWellKnownUriSteps());
        call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetCreditCardsAccountsV2Endpoint.class),
			condition(GetConsentV3Endpoint.class)
		)));
        super.configureClient();
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        super.onConfigure(config, baseUrl);
        env.putString("api_type", EnumResourcesType.CREDIT_CARD_ACCOUNT.name());
        callAndContinueOnFailure(ExtractSpecifiedResourceApiAccountIdsFromConfig.class, Condition.ConditionResult.INFO);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_CARDS_ACCOUNTS).addScopes(OPFScopesEnum.CREDIT_CARDS_ACCOUNTS, OPFScopesEnum.OPEN_ID).build();

        callAndContinueOnFailure(OperationalLimitsToConsentRequest.class, Condition.ConditionResult.FAILURE);
        clientAuthType = getVariant(ClientAuthType.class);
    }

    @Override
    protected void validateResponse() {
        // Not used in this test module
    }

    @Override
    protected void requestProtectedResource() {

        JsonArray fetchedApiIds = prepareApiIds();

        for (int i = 0; i < NUMBER_OF_IDS_TO_FETCH; i++) {
            int currentResourceId = i + 1;

            // If during the second resource test part, fetchedApiIds size is less than numberOfIdsToFetch it means that the second account ID was not provided by the client or was not fetched.
            // In that case we assume that the multiple accounts for the same users feature is not supported, therefore we skip the part of the test with the second resource ID.
            if (currentResourceId == 2 && fetchedApiIds.size() < NUMBER_OF_IDS_TO_FETCH) {
                eventLog.log(getName(), "Could not find second credit card account ID. Skipping part of the test with the second resource ID");
                continue;
            }
            JsonElement creditCardAccountIdElement = fetchedApiIds.get(i);

            String creditCardAccountId = OIDFJSON.getString(creditCardAccountIdElement);
            // Call to credit card account GET once with validation
            runInLoggingBlock(() -> {
                env.putString("accountId", creditCardAccountId);
                callAndStopOnFailure(PrepareUrlForFetchingCreditCardAccount.class);

                preCallProtectedResource(String.format("Fetching Credit Card Account using resource_id_%d", currentResourceId));
                validateResponse("Validate Credit Card Account response", cardIdentificationResponseValidator());
            });


            // Call to credit card account GET 3 times
            for (int j = 1; j < 4; j++) {
                preCallProtectedResource(String.format("[%d] Fetching Credit Card Account using resource_id_%d", j + 1, currentResourceId));
            }

            makeOverOlCall(String.format("CC Account [%d]", currentResourceId));

            // Call to credit card transactions  with dates GET once with validation
            LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

            runInLoggingBlock(() -> {
				env.putString("metaOnlyRequestDateTime", "true");
                env.putString("fromTransactionDate", currentDate.minusDays(6).format(FORMATTER));
                env.putString("toTransactionDate", currentDate.format(FORMATTER));

                callAndStopOnFailure(PrepareUrlForFetchingCardTransactions.class);
                callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);

                preCallProtectedResource(String.format("Fetch Credit Card Transactions using resource_id_%d", currentResourceId));
                validateResponse("Validate Credit Card Transactions Response", creditCardAccountsTransactionResponseValidator());
                callAndStopOnFailure(EnsureNumberOfTotalRecordsIsAtLeast20FromData.class);
            });


            // Call to credit card transactions  with dates GET 3 times
            for (int j = 1; j < 4; j++) {
                preCallProtectedResource(String.format("[%d] Fetch Credit Card Transactions using resource_id_%d", j + 1, currentResourceId));
            }

            makeOverOlCall(String.format("CC Trans. [%d]", currentResourceId));


            // Call to credit card bills GET Once with Validation
            runInLoggingBlock(() -> {
				env.putString("metaOnlyRequestDateTime", "false");
                env.putString("fromDueDate", currentDate.minusDays(6).format(FORMATTER));
                env.putString("toDueDate", currentDate.format(FORMATTER));
                callAndStopOnFailure(AddToAndFromDueDateParametersToProtectedResourceUrl.class);
                callAndStopOnFailure(PrepareUrlForFetchingCardBills.class);
                preCallProtectedResource(String.format("Fetch Credit Card Bills using resource_id_%d", currentResourceId));
                validateResponse("Validate Credit Card Bills Response", creditCardBillValidator());
            });

            // Call to credit card bills GET 29 times
            for (int j = 1; j < 30; j++) {
                preCallProtectedResource(String.format("[%d] Fetch Credit Card Bills using resource_id_%d", j + 1, currentResourceId));
            }

            makeOverOlCall(String.format("CC Bills [%d]", currentResourceId));


            // Call to credit card limits GET once with validation

            runInLoggingBlock(() -> {
                callAndStopOnFailure(PrepareUrlForFetchingCardLimits.class);

                preCallProtectedResource(String.format("Fetch Credit Card Limits using resource_id_%d", currentResourceId));
                validateResponse("Validate Credit Card Limits Response", creditCardAccountsLimitsResponseValidator());

            });

            // Call to credit card limits GET 239 times
            for (int j = 1; j < 240; j++) {
                preCallProtectedResource(String.format("[%d] Fetch Credit Card Limits using resource_id_%d", j + 1, currentResourceId));
            }

            makeOverOlCall(String.format("CC Limits [%d]", currentResourceId));

            // Call to credit card current transactions GET once with validation
            runInLoggingBlock(() -> {
				env.putString("metaOnlyRequestDateTime", "true");
                callAndStopOnFailure(PrepareUrlForFetchingCurrentAccountTransactions.class);

                preCallProtectedResource(String.format("Fetch Credit Card Transactions Current using resource_id_%d", currentResourceId));

				validateResponse("Validate Credit Card Transactions Current Response", creditCardAccountsTransactionCurrentResponseValidator());

            });
            // Call to credit card current transactions GET 239 times
            for (int j = 1; j < 240; j++) {
                preCallProtectedResource(String.format("[%d] Fetch Credit Card Transactions Current using resource_id_%d", j + 1, currentResourceId));
            }


            // Call to credit card current transactions with dates and page size fetched from next once with validation
            runInLoggingBlock(() -> {
                env.putString("fromTransactionDate", currentDate.minusDays(6).format(FORMATTER));
                env.putString("toTransactionDate", currentDate.format(FORMATTER));

                callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
                env.putInteger("required_page_size", 1);
                callAndStopOnFailure(AddSpecifiedPageSizeParameterToProtectedResourceUrl.class);

                preCallProtectedResource(String.format("Fetch Credit Card Transactions Current next link using resource_id_%d", currentResourceId));
                validateResponse("Validate Credit Card Transactions Current Response", creditCardAccountsTransactionCurrentResponseValidator());
                callAndStopOnFailure(ValidateNumberOfRecordsPage1.class);
                validateNextLinkResponse();
            });

            // Call to credit card current transactions with dates and page size fetched from next link 19 times GET
            for (int j = 1; j < REQUIRED_NUMBER_OF_RECORDS; j++) {
                String message = String.format("[%d] Fetch Credit Card Transactions Current next link using resource_id_%d", j + 1, currentResourceId);
                eventLog.startBlock(message);

                preCallProtectedResource();
                if (j == 1) {
                    callAndStopOnFailure(ValidateNumberOfRecordsPage2.class);
                }
				if(j!=REQUIRED_NUMBER_OF_RECORDS-1){
					validateNextLinkResponse();
				}

                eventLog.endBlock();
            }

            makeOverOlCall(String.format("CC CurTr [%d]", currentResourceId));


            enableLogging();
        }

    }

    private JsonArray prepareApiIds() {
        JsonArray fetchedApiIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");

        // IF no resource IDs were provided in the config, we will fetch instead
        if (fetchedApiIds.isEmpty()) {

            preCallProtectedResource("Fetching Credit Card Accounts");
            // Validate credit card response
            runInBlock("Validating Credit Card response", () -> {
                callAndContinueOnFailure(cardAccountsDataResponseResponseValidator(), Condition.ConditionResult.FAILURE);

                env.putString("apiIdName", API_RESOURCE_ID);
                callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);

                JsonArray extractedApiIds = env.getObject("extracted_api_ids").getAsJsonArray("extractedApiIds");

                if (extractedApiIds.size() >= NUMBER_OF_IDS_TO_FETCH) {
                    env.putInteger("number_of_ids_to_fetch", NUMBER_OF_IDS_TO_FETCH);
                } else {
                    env.putInteger("number_of_ids_to_fetch", 1);
                }

                callAndStopOnFailure(FetchSpecifiedNumberOfExtractedApiIds.class);
            });

            fetchedApiIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");
        }
        return fetchedApiIds;
    }


    private void validateNextLinkResponse() {

        callAndStopOnFailure(EnsureOnlyOneRecordWasReturned.class);
        callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);

        env.putString("value", "1");
        env.putString("parameter", "page-size");
        callAndStopOnFailure(SetSpecifiedValueToSpecifiedUrlParameter.class);

    }
    protected void validateResponse(String message, Class<? extends Condition> validationClass) {
        runInBlock(message, () -> callAndContinueOnFailure(validationClass, Condition.ConditionResult.FAILURE));
    }
}
