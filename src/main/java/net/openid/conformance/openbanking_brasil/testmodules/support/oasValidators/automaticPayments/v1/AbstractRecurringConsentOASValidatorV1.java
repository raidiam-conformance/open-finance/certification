package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;

import java.util.List;

public abstract class AbstractRecurringConsentOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	@Override
	protected ResponseEnvKey getResponseEnvKey() {
		return ResponseEnvKey.FullConsentResponseEnvKey;
	}

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/automatic-payments/swagger-automatic-payments-1.0.0.yml";
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		assertData(body.getAsJsonObject("data"));
	}

	protected void assertData(JsonObject data) {
		JsonObject recurringConfiguration = data.getAsJsonObject("recurringConfiguration");

		if (recurringConfiguration.has("automatic")) {
			assertAutomaticRecurringConfigurationConstraints(recurringConfiguration.getAsJsonObject("automatic"));
		}

		if (recurringConfiguration.has("sweeping")) {
			JsonObject sweepingConfig = recurringConfiguration.getAsJsonObject("sweeping");
			if (sweepingConfig.has("periodicLimits")) {
				assertSweepingOrVrpPeriodicLimitsConstraints(sweepingConfig.getAsJsonObject("periodicLimits"));
			}
		}

		if (recurringConfiguration.has("vrp")) {
			JsonObject vrpConfig = recurringConfiguration.getAsJsonObject("vrp");
			if (vrpConfig.has("periodicLimits")) {
				assertSweepingOrVrpPeriodicLimitsConstraints(vrpConfig.getAsJsonObject("periodicLimits"));
			}
		}
	}

	private void assertAutomaticRecurringConfigurationConstraints(JsonObject automaticConfig) {
		assertField1IsRequiredWhenField2HasValue2(automaticConfig, "dayOfMonth", "period", List.of("MENSAL", "ANUAL"));
		assertField1IsRequiredWhenField2HasValue2(automaticConfig, "dayOfWeek", "period", "SEMANAL");
		assertField1IsRequiredWhenField2HasValue2(automaticConfig, "month", "period", "ANUAL");
	}

	private void assertSweepingOrVrpPeriodicLimitsConstraints(JsonObject periodicLimits) {
		String[] periods = {"day", "week", "month", "year"};
		for (String period : periods) {
			if (periodicLimits != null && periodicLimits.has(period) && periodicLimits.get(period).isJsonObject()) {
				JsonObject periodObject = periodicLimits.getAsJsonObject(period);
				boolean hasQuantityLimit = isPresent(periodObject, "quantityLimit");
				boolean hasTransactionLimit = isPresent(periodObject, "transactionLimit");

				if (!hasQuantityLimit && !hasTransactionLimit) {
					throw error(String.format("%s must have at least one of quantityLimit or transactionLimit.", period));
				}
			}
		}
	}
}

