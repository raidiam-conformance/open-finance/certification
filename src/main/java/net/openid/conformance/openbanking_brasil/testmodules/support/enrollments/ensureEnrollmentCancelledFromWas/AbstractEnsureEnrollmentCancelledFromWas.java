package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentCancelledFromWas;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public abstract class AbstractEnsureEnrollmentCancelledFromWas extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    public Environment evaluate(Environment env) {
        try {
            String cancelledFrom = OIDFJSON.getString(BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                .flatMap(responseBody -> Optional.ofNullable(responseBody.getAsJsonObject().getAsJsonObject("data")))
                .flatMap(data -> Optional.ofNullable(data.getAsJsonObject("cancellation")))
                .flatMap(cancellation -> Optional.ofNullable(cancellation.get("cancelledFrom")))
                .orElseThrow(() -> error("Could not extract cancelledFrom from the the resource_endpoint_response_full.data.cancellation")));

            String expectedCancelledFromType = getExpectedCancelledFromType();

            if (!expectedCancelledFromType.equals(cancelledFrom)) {
                throw error("Enrollments cancelledFrom type is not what was expected",
                    args("Expected", expectedCancelledFromType, "Actual", cancelledFrom));
            }

            logSuccess("Received expected cancelledFrom type",
                args("Expected", expectedCancelledFromType, "Received", cancelledFrom));
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }
        return env;
    }

    protected abstract String getExpectedCancelledFromType();
}
