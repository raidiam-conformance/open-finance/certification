package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddDummyCustomerIpAddress extends AbstractCondition {
	@Override
	@PreEnvironment(required = "resource_endpoint_request_headers")
	@PostEnvironment(required = "resource_endpoint_request_headers")
	public Environment evaluate(Environment env) {
		JsonObject headers = env.getObject("resource_endpoint_request_headers");
		if (headers == null) {
			headers = new JsonObject();
		}
		headers.addProperty("x-fapi-customer-ip-address", "198.51.100.119");
		env.putObject("resource_endpoint_request_headers", headers);
		logSuccess("Added dummy customer IP address to resource endpoint request headers", headers);
		return env;
	}
}
