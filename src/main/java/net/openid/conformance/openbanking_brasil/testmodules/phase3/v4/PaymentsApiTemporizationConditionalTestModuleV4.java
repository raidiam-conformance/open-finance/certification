package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiTemporizationConditionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_temporization-conditional_test-module_v4",
	displayName = "Payments API v4 test module for checking the optional flow for temporization",
	summary = "This test module is optional and should be executed only by institutions that wish to test the different flows for a payment that will fall into a PNDG state, meaning it was halted for additional verification. The test will only be executed if the \"Payment consent - Logged User CPF - Temporization Test\" is provided on the test configuration.\n" +
		"\n" +
		"The test modules validate if the payment will reach a final state is reached after a PDNG status" +
		"\n" +
		"• Validates if the user has provided the field “Payment consent - Logged User CPF - Temporization”. If field is not provided the whole test scenario must be SKIPPED\n" +
		"• Set the payload to be customer business if Payment consent - Business Entity CNPJ - Temporization was provided. If left blank, it should be customer personal\n" +
		"• Creates consent request_body with valid email proxy specific for this test (cliente-a00002@pix.bcb.gov.br), localInstrument as DICT, Payment consent - Logged User CPF - Temporization” on the loggedUser identification field, Value as 12345,67, and IBGE Town Code as 1400704 (Uiramutã), Debtor account should not be sent\n" +
		"• Call the POST Consents API\n" +
		"• Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"• Redirects the user to authorize the created consent - Expect Successful Authorization\n" +
		"• Calls the POST Payments Endpoint\n" +
		"• Expects a 201 with status set as either PNDG or RCVD - Validate response_body\n" +
		"• Poll the Get Payments endpoint - End Polling when payment response returns a PNDG status\n" +
		"• Poll the Get Payments endpoint with the PaymentID Created while payment status is PNDG – Wait for user to provide approval\n" +
		"• Expect 200 with either:\n" +
		"\n" +
		"• Rejected State (RJCT) - Confirm that RejectionReason equal to NAO_INFORMADO is returned\n" +
		"\n" +
		"• Accepted State (ACSC)\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount",
		"conditionalResources.brazilCpfTemporization",
		"conditionalResources.brazilCnpjTemporization",
	}
)
public class PaymentsApiTemporizationConditionalTestModuleV4 extends AbstractPaymentsApiTemporizationConditionalTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}
}
