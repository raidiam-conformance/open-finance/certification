package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiExpiredPreRiskSignalEnrollmentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.GetEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostEnrollmentsOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_expired-prerisksignal-enrollment_test-module_v2",
	displayName = "enrollments_api_expired-prerisksignal-enrollment_test-module_v2",
	summary = "Ensure that enrollment is expired in 5 minutes when status is set as AWAITING_RISK_SIGNALS:\n" +
		"• Call the POST enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Set the conformance Suite to sleep for 5 minutes\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"REJECTED\", cancelledFrom is DETENTORA, and rejectionReason is REJEITADO_TEMPO_EXPIRADO_RISK_SIGNALS",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
	}
)
public class EnrollmentsApiExpiredPreRiskSignalEnrollmentTestModuleV2 extends AbstractEnrollmentsApiExpiredPreRiskSignalEnrollmentTestModule {

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetEnrollmentsV2Endpoint.class));
	}

	@Override
	public void cleanup() {
		//not required
	}
}
