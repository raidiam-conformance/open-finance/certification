package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public class GetInvoiceFinancingsListV2n3OASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/invoiceFinancings/invoice-financings-2.3.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/contracts";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


}
