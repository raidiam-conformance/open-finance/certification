package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedFrom;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectedFromEnum;

public class EnsureRecurringPaymentsConsentsRejectedFromIniciadora extends AbstractEnsureRecurringPaymentsConsentsRejectedFrom {

	@Override
	protected RecurringPaymentsConsentsRejectedFromEnum expectedRejectedFrom() {
		return RecurringPaymentsConsentsRejectedFromEnum.INICIADORA;
	}
}
