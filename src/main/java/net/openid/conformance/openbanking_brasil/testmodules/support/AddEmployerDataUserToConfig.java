package net.openid.conformance.openbanking_brasil.testmodules.support;

public class AddEmployerDataUserToConfig extends AbstractAddVariantUserToConfig {
	@Override
	protected String getVariantUserKey() {
		return "employerDataCpf";
	}

	@Override
	protected String getVariantBusinessKey() {
		return "employerDataCnpj";
	}
}
