package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixRetryPathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SaveCurrentPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.checkCurrentTime.CheckCurrentTimeBefore12AM;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToAddSavedAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToDoubleAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasDetalheTentativaInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted.AbstractEnsureIsRetryAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted.EnsureIsRetryAcceptedFalse;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.AbstractEnsurePaymentListDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.EnsureOnePaymentForToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereIsOneRjctPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentRejectionReasonCodeWasDetalheTentativaInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.AbstractSetConfigurationFieldsForRetryTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.SetConfigurationFieldsForIntradayOnePaymentRetryTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.GenerateNewE2EID;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixRetryIntradayCoreTestModule extends AbstractAutomaticPaymentsAutomaticPixRetryPathTestModule {

	private boolean firstPayment = true;

	@Override
	protected void postAndValidateRecurringPayment() {
		super.postAndValidateRecurringPayment();
		firstPayment = false;
		validationStarted = false;
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(GenerateNewE2EID.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToAddSavedAmount.class);
		requestProtectedResource();
		callAndContinueOnFailure(CheckCurrentTimeBefore12AM.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		ConditionSequence sequence = new CallPixPaymentsEndpointSequence()
			.replace(SetProtectedResourceUrlToPaymentsEndpoint.class, condition(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class));

		if (firstPayment) {
			return sequence
				.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class));
		} else {
			return sequence;
		}
	}

	@Override
	protected void validateResponse() {
		if (firstPayment) {
			super.validateResponse();
		} else {
			call(postPaymentValidationSequence());
			runRepeatSequence();
			validateFinalState();
		}
	}

	@Override
	protected void configurePayment() {
		super.configurePayment();
		callAndStopOnFailure(SaveCurrentPaymentAmount.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToDoubleAmount.class);
	}

	@Override
	protected void validateFinalState() {
		if (firstPayment) {
			super.validateFinalState();
		} else {
			callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class, Condition.ConditionResult.FAILURE);
		}
	}

	@Override
	protected AbstractSetConfigurationFieldsForRetryTest getSetConfigurationFieldsForRetryTestCondition() {
		return new SetConfigurationFieldsForIntradayOnePaymentRetryTest();
	}

	@Override
	protected AbstractEnsureIsRetryAccepted getEnsureIsRetryAcceptedCondition() {
		return new EnsureIsRetryAcceptedFalse();
	}

	@Override
	protected AbstractEnsureThereArePayments getEnsureThereArePaymentsCondition() {
		return new EnsureThereIsOneRjctPayment();
	}

	@Override
	protected AbstractEnsurePaymentListDates getEnsurePaymentListDatesCondition() {
		return new EnsureOnePaymentForToday();
	}

	@Override
	protected AbstractEnsureRecurringPaymentsRejectionReason rejectionReasonCondition() {
		return new EnsureRecurringPaymentRejectionReasonCodeWasDetalheTentativaInvalido();
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorCodeCondition() {
		return new EnsureErrorResponseCodeFieldWasDetalheTentativaInvalido();
	}

	@Override
	protected AbstractEditRecurringPaymentsBodyToSetDate setPaymentDateCondition() {
		return new EditRecurringPaymentsBodyToSetDateToToday();
	}
}
