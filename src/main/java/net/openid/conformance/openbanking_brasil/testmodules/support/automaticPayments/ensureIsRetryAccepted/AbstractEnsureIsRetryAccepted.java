package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public abstract class AbstractEnsureIsRetryAccepted extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonObject response = extractResponseFromEnv(env);
		JsonObject automatic = getAutomaticRecurringConfigurationFromResponse(response);
		boolean isRetryAccepted = Optional.ofNullable(automatic.get("isRetryAccepted"))
			.map(OIDFJSON::getBoolean)
			.orElseThrow(() -> error("Could not find isRetryAccepted field inside automatic recurringConfiguration object",
				args("automatic", automatic)));
		if (isRetryAccepted != isRetryAcceptedExpectedValue()) {
			throw error("The isRetryAcceptedField field does not match the expected value",
				args("isRetryAccepted", isRetryAccepted, "expected", isRetryAcceptedExpectedValue()));
		}
		logSuccess("The isRetryAccepted field matches the expected value");
		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	private JsonObject getAutomaticRecurringConfigurationFromResponse(JsonObject response) {
		return Optional.ofNullable(response.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("recurringConfiguration"))
			.map(recurringConfiguration -> recurringConfiguration.getAsJsonObject("automatic"))
			.orElseThrow(() -> error("Could not find data.recurringConfiguration.automatic in the response", args("response", response)));
	}

	protected abstract boolean isRetryAcceptedExpectedValue();
}
