package net.openid.conformance.openbanking_brasil.testmodules.support.paymentWebhook;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractCreateWebhookEndpoint;

public class CreatePaymentConsentWebhookv4 extends AbstractCreateWebhookEndpoint {
	@Override
	protected String getValueFromEnv() {
			return "consent_id";
	}

	@Override
	protected String getExpectedUrlPath() {
		return "/open-banking/webhook/v1/payments/v4/consents/";
	}
}
