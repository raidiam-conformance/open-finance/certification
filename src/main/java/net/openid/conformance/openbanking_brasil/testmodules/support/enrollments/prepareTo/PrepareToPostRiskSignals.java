package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo;

import org.springframework.http.HttpMethod;

public class PrepareToPostRiskSignals extends AbstractPrepareToMakeACallToEnrollmentsEndpoint {

	@Override
	protected HttpMethod httpMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected String endpoint() {
		return "risk-signals";
	}

	@Override
	protected boolean expectsSelfLink() {
		return false;
	}
}
