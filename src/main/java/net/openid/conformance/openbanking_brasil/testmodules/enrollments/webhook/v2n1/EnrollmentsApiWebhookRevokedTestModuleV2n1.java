package net.openid.conformance.openbanking_brasil.testmodules.enrollments.webhook.v2n1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.EnsureSoftwareClientNameMatchesSSA;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.webhook.EnrollmentsApiWebhookRevokedTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractSoftwareClientNameFromSSA;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.GetEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostFidoRegistrationOptionsOASValidatorV2n1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_webhook-revoked_test-module_v2-1",
	displayName = "enrollments_api_webhook-revoked_test-module_v2-1",
	summary = "Ensure that the tested institution has correctly implemented the webhook notification endpoint and that this endpoint is correctly called when a enrollment is revoked. \n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>\n" +
		"• Obtain a SSA from the Directory\n" +
		"• Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration. Also, extract the value of software_client_name.\n" +
		"• Ensure the field software_origin_uris is present in the SSA and extract the first uri from the array\n" +
		"• Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>”]\n" +
		"• Expect a 201 - Validate Response\n" +
		"• Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"• Set the enrollments webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>/open-banking/enrollments/v2/enrollments/{enrollmentId}, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Call the POST enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Call the POST Risk Signals endpoint sending the appropriate signals\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is AWAITING_ACCOUNT_HOLDER_VALIDATION\n" +
		"• Redirect the user to authorize the enrollment\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AWAITING_ENROLLMENT\"\n" +
		"• Call the POST fido-registration-options endpoint\n" +
		"• Expect a 201 response - Validate the response, extract the challenge, and ensure the value of data.rp.name matches the field software_client_name at the SSA.\n" +
		"• Create the FIDO Credentials and create the attestationObject\n" +
		"• Call the POST fido-registration endpoint, sending the compliant attestationObject, with the origin extracted on the SSA at the ClientDataJson.origin\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AUTHORISED\"\n" +
		"• Call the PATCH enrollments endpoint\n" +
		"• Expect a 204 response\n" +
		"• Expect an incoming message for enrollments, which must be mtls protected - Wait 60 seconds for both messages to be returned\n" +
		"• Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within now and the start of the test\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"REVOKED\", cancelledFrom is INICIADORA and revocationReason is REVOGADO_MANUALMENTE",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.brazilOrganizationId",
		"resource.enrollmentsUrl",
		"directory.apibase",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"server.discoveryUrl",
		"directory.discoveryUrl"
	}
)
public class EnrollmentsApiWebhookRevokedTestModuleV2n1 extends EnrollmentsApiWebhookRevokedTestModule {

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetEnrollmentsV2Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV2n1.class;
	}

	@Override
	protected void getSsa() {
		super.getSsa();
		callAndStopOnFailure(ExtractSoftwareClientNameFromSSA.class);
	}

	@Override
	protected void postAndValidateFidoRegistrationOptions() {
		super.postAndValidateFidoRegistrationOptions();
		runInBlock("Checking the RP name against the client name", () -> callAndContinueOnFailure(EnsureSoftwareClientNameMatchesSSA.class, Condition.ConditionResult.FAILURE));
	}
}
