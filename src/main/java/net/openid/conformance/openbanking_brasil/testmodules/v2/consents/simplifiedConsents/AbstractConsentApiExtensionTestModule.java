package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;

import java.util.Optional;


public abstract class AbstractConsentApiExtensionTestModule extends AbstractSimplifiedConsentRenewalTestModule {

	@Override
	protected abstract Class<? extends Condition> setGetConsentValidator();

	@Override
	protected abstract Class<? extends Condition> setConsentCreationValidation();

	@Override
	protected abstract Class<? extends Condition> setPostConsentExtensionValidator();

	@Override
	protected abstract Class<? extends Condition> setGetConsentExtensionValidator();

	@Override
	protected void configureClient(){
		env.putString("metaOnlyRequestDateTime", "true");
		super.configureClient();
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.RESOURCES, OPFScopesEnum.OPEN_ID).build();
	}


	@Override
	protected void requestAuthorizationCode(){
		super.requestAuthorizationCode();
		callAndStopOnFailure(ExtractRefreshTokenFromTokenResponse.class);
	}
	@Override
	public void callExtensionEndpoints(){
		runInBlock("Validating post consent extension response", this::callPostConsentExtensionEndpoint);

		runInBlock("Validating get consent extension response", this::callGetConsentExtensionEndpoint);

		runInBlock("Validating get consent response", this::callGetConsentEndpoint);
		callAndStopOnFailure(validateExpirationTimeReturned());
		callAndStopOnFailure(WaitFor120Seconds.class);
		ConditionSequence refreshTokenSteps = new RefreshTokenRequestSteps(false, addTokenEndpointClientAuthentication);
		refreshTokenSteps.skip(EnsureAccessTokenValuesAreDifferent.class, "No need to make this check");
		call(refreshTokenSteps);
		requestProtectedResource();
	}
	@Override
	protected void requestProtectedResource() {
		// verify the access token against a protected resource
		eventLog.startBlock(currentClientString() + "Resource server endpoint tests");
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		// these are optional; only add them for the first client
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
		callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
		call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));
		Optional<ConditionSequence> statusCheckingSequence = getBrazilPaymentsStatusCodeCheck();
		call(statusCheckingSequence.orElse(
			sequenceOf(condition(EnsureHttpStatusCodeIs200.class).onFail(Condition.ConditionResult.FAILURE))
		));
		call(exec().unmapKey("endpoint_response"));
		callAndContinueOnFailure(CheckForDateHeaderInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-10");

		callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");

		callAndContinueOnFailure(EnsureResourceResponseReturnedJsonContentType.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-9", "FAPI1-BASE-6.2.1-10");

		eventLog.endBlock();
	}

	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {
		return CreateExtensionRequestTimeDayPlus365.class;
	}

	@Override
	protected void callGetConsentAndOrResourceUrlSequence(){
		call(new ValidateRegisteredEndpoints(
				sequenceOf(
					condition(GetConsentV3Endpoint.class),
					condition(GetResourcesV3Endpoint.class)
				)
			)
		);
	}


	@Override
	protected Class<? extends Condition> validateExpirationTimeReturned(){
		return ValidateExtensionExpiryTimeInConsentResponse.class;
	}

	@Override
	protected Class<? extends Condition> validateGetConsentExtensionEndpointResponse() {
		return ValidateExtensionExpiryTimeInGetSizeOne.class;
	}

	@Override
	protected Class<? extends Condition> buildConsentRequestBody() {
		return FAPIBrazilOpenBankingCreateConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setConsentExpirationTime() {
		return FAPIBrazilAddExpirationToConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionEndpoint() {
		return PrepareToGetConsentExtensions.class;
	}

}
