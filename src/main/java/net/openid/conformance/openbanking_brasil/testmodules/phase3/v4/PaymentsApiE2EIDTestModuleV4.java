package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiE2EIDTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_e2eid_test-module_v4",
	displayName = "Payments API E2EID test module",
	summary = "Validate that the server is correctly validating if a correct E2EID field is being sent before accepting the PIX Payments requests\n" +
		"• Call the POST Consent API with DICT initiation type using a valid e-mail proxy key\n" +
		"• Expects 201 - Validate Response\n" +
		"• Redirect the user to authorize the Consent\n" +
		"• Call GET Consent, with the same idempotency id used at POST Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\" \n" +
		"• Calls the POST  Payments  without sending an E2EID\n" +
		"• Expects  400 in JSON or a 422 JWT. For a 422 returned code must be set to \"PARAMETRO_NAO_INFORMADO\"\n" +
		"• Call the POST Consents API\n" +
		"• Redirect the user to authorize the Consent\n" +
		"• Calls the POST  Payments API with a bad E2EID which sets the month field to 13\n" +
		"• Expects a  400 in JSON or a 422 JWT. For a 422 returned code must be set to \"PARAMETRO_INVALIDO\"\n" +
		"• Call the POST Consents API\n" +
		"• Redirect the user to authorize the Consent\n" +
		"• Calls the POST  Payments API with a valid E2EID\n" +
		"• Expects a success 201\n" +
		"• Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"• Expectes Definitive  state (ACSC) with matching E2EID- Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiE2EIDTestModuleV4 extends AbstractPaymentsApiE2EIDTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected void createPaymentErrorValidator() {
		callAndContinueOnFailure(PostPaymentsPixOASValidatorV4.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}
}
