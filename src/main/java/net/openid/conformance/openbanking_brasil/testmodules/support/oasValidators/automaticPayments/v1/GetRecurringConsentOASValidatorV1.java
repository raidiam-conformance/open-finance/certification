package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1;

import org.springframework.http.HttpMethod;

public class GetRecurringConsentOASValidatorV1 extends AbstractRecurringConsentOASValidatorV1 {

	@Override
	protected String getEndpointPath() {
		return "/recurring-consents/{recurringConsentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


}
