package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.AddAudAsEnrollmentsUrlToRequestObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.CallEnrollmentsEndpointWithBearerToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractEnrollmentIdFromEnrollmentsEndpointResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.FAPIBrazilAddEnrollmentIdToClientScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.sequence.AbstractConditionSequence;
import net.openid.conformance.sequence.ConditionSequence;

public class PostEnrollmentsSteps extends AbstractConditionSequence {

    private final Class<? extends ConditionSequence> addClientAuthenticationToTokenEndpointRequest;
    private final boolean stopAfterEnrollmentsEndpointCall;


    public PostEnrollmentsSteps(Class<? extends ConditionSequence> addClientAuthenticationToTokenEndpointRequest, boolean stopAfterEnrollmentsEndpointCall) {
        this.addClientAuthenticationToTokenEndpointRequest = addClientAuthenticationToTokenEndpointRequest;
        this.stopAfterEnrollmentsEndpointCall = stopAfterEnrollmentsEndpointCall;
    }

	public PostEnrollmentsSteps(Class<? extends ConditionSequence> addClientAuthenticationToTokenEndpointRequest) {
		this.addClientAuthenticationToTokenEndpointRequest = addClientAuthenticationToTokenEndpointRequest;
		this.stopAfterEnrollmentsEndpointCall = false;
	}

    @Override
    public void evaluate() {
		// Create client_credentials token request
        call(exec().startBlock("Use client_credentials grant to obtain enrollment"));
        callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
        callAndStopOnFailure(SetPaymentsScopeOnTokenEndpointRequest.class);
        call(sequence(addClientAuthenticationToTokenEndpointRequest));

		// Make request for client_credentials token and validate it
        callAndStopOnFailure(CallTokenEndpoint.class);
        callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
        callAndStopOnFailure(CheckForAccessTokenValue.class);
        callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);
        callAndContinueOnFailure(ExtractExpiresInFromTokenEndpointResponse.class, "RFC6749-4.4.3", "RFC6749-5.1");
        call(condition(ValidateExpiresIn.class)
            .skipIfObjectMissing("expires_in")
            .onSkip(Condition.ConditionResult.INFO)
            .requirements("RFC6749-5.1")
            .onFail(Condition.ConditionResult.FAILURE)
            .dontStopOnFailure());

		// Create enrollments request
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
        callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
		callAndStopOnFailure(AddAudAsEnrollmentsUrlToRequestObject.class);
        call(exec().mapKey("request_object_claims", "resource_request_entity_claims"));
        callAndStopOnFailure(AddIssAsCertificateOuToRequestObject.class, "BrazilOB-6.1");
        callAndStopOnFailure(AddJtiAsUuidToRequestObject.class, "BrazilOB-6.1");
        callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");
        call(exec().unmapKey("request_object_claims"));
        callAndStopOnFailure(ValidateOrganizationJWKsPrivatePart.class);

		// Sign request payload and call enrollments endpoint
        callAndStopOnFailure(SignEnrollmentsRequest.class);
        callAndStopOnFailure(CallEnrollmentsEndpointWithBearerToken.class);

		// Optional stopping point if we don't want to validate the response
        if (stopAfterEnrollmentsEndpointCall) {
            return;
        }

		// Validate response and extract enrollment id
        call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));
        call(exec().mapKey("endpoint_response_jwt", "consent_endpoint_response_jwt"));
        callAndContinueOnFailure(EnsureContentTypeApplicationJwt.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
        callAndContinueOnFailure(EnsureHttpStatusCodeIs201.class, Condition.ConditionResult.FAILURE);
        callAndStopOnFailure(ExtractSignedJwtFromResourceResponse.class, "BrazilOB-6.1");
        callAndContinueOnFailure(FAPIBrazilValidateResourceResponseSigningAlg.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
        callAndContinueOnFailure(FAPIBrazilValidateResourceResponseTyp.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
        callAndStopOnFailure(FAPIBrazilGetKeystoreJwksUri.class, Condition.ConditionResult.FAILURE);
        call(exec().mapKey("server", "org_server"));
        call(exec().mapKey("server_jwks", "org_server_jwks"));
        callAndStopOnFailure(FetchServerKeys.class);
        call(exec().unmapKey("server"));
        call(exec().unmapKey("server_jwks"));
        callAndContinueOnFailure(ValidateResourceResponseSignature.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
        callAndContinueOnFailure(ValidateResourceResponseJwtClaims.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
        call(exec().unmapKey("endpoint_response"));
        call(exec().unmapKey("endpoint_response_jwt"));
        callAndStopOnFailure(ExtractEnrollmentIdFromEnrollmentsEndpointResponse.class);
        callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI-R-6.2.1-11", "FAPI1-BASE-6.2.1-11");
		callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
		callAndStopOnFailure(FAPIBrazilAddEnrollmentIdToClientScope.class);

        call(exec().endBlock());
    }
}
