package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SetAutomaticPaymentAmountToUserDefinedAmount extends AbstractSetAutomaticPaymentAmount {

	private String amount;

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		amount = env.getString("resource", "recurringPaymentAmount");
		return super.evaluate(env);
	}

	@Override
	protected String automaticPaymentAmount() {
		if (!amount.matches("^\\d{1,16}\\.\\d{2}$")) {
			throw error("The value defined for resource.recurringPaymentAmount is not valid", args("recurringPaymentAmount", amount));
		}
		return amount;
	}
}
