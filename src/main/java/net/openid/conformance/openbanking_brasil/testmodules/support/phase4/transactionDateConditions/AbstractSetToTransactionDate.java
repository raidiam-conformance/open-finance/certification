package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public abstract class AbstractSetToTransactionDate extends AbstractCondition {

    protected final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    protected final LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

    protected abstract int amountOfDaysToThePast();

    @Override
    @PostEnvironment(strings = "toTransactionDate")
    public Environment evaluate(Environment env) {
        env.putString("toTransactionDate", currentDate.minusDays(amountOfDaysToThePast()).format(FORMATTER));
        logSuccess("toTransactionDate has been set",
            args("toTransactionDate", env.getString("toTransactionDate")));
        return env;
    }
}
