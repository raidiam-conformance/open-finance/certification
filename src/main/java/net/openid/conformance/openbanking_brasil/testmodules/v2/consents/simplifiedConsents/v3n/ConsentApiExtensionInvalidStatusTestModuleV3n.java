package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentExtendsOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractConsentApiExtensionInvalidStatusTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_extension-invalid-status_test-module_v3-2",
	displayName = "consents_api_extension-invalid-status_test-module_v3-2",
	summary = "Ensure an extension cannot be done if consent is at AWAITING_AUTHORISATION status\n" +
		"\u2022 Call the POST Consents Endpoint with expirationDateTime as current time + 5 minutes\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Call the GET Consents\n" +
		"\u2022 Expects 200 - Validate Response and check if the status is AWAITING_AUTHORISATION\n" +
		"\u2022 Call the POST Extends Endpoint with expirationDateTime as  D+365, and all required headers\n" +
		"\u2022 Expects 401 or 403",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl","consent.productType"
})
public class ConsentApiExtensionInvalidStatusTestModuleV3n extends AbstractConsentApiExtensionInvalidStatusTestModule {

	@Override
	protected Class<? extends Condition> validatePostConsentResponse() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends Condition> validateGetConsentEndpointResponse() {
		return GetConsentOASValidatorV3n2.class;
	}

	@Override
	public ConditionSequence getPostConsentExtensionExpectingErrorSequence(Class<? extends Condition> errorCondition, Class<? extends Condition> errorCodeMessageCondition, Class<? extends Condition> expiryTimeCondition) {
		return super.getPostConsentExtensionExpectingErrorSequence(errorCondition, errorCodeMessageCondition, expiryTimeCondition)
			.skip(PostConsentExtendsOASValidatorV3.class,"OFB-5589, removing 401 and 403 response body validation");
	}
}
