package net.openid.conformance.openbanking_brasil.testmodules.v3.consents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3n.AbstractConsentsApiPermissionGroupsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_permission-groups_test-module_v3",
	displayName = "Validate that consent API V3 accepts the consent groups",
	summary = "Validates that consent API V3 accepts all the consent groups. The test module will creates a series of consent requests with valid permissions group and expect for each of them a 201 to be returned by the server with matching permissions or, instead, a 422 SEM_PERMISSOES_FUNCIONAIS_RESTANTES error response because server does not support the sent group. For Credit Operation, Investments and Exchange, a 201 response is always expected.\n" +
		"\u2022 Validates consent API V3 request for 'Personal Registration Data' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Personal Additional Information' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Business Registration Data' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Business Additional Information' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Accounts Balances' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Accounts Limits' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Credit Card Invoices' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Credit Operations' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Balances & Credit Card Limits' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Accounts Transactions' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Credit Card Limits' permission group(s)\n" +
		"\u2022 Validates consent API V3 request for 'Credit Card Transactions' permission group(s)\n" +
	    "\u2022 Validates consent API V3 request for 'Investments' permission group(s)\n" +
	    "\u2022 Validates consent API V3 request for 'Exchanges' permission group(s)\n",

	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ConsentsApiPermissionGroupsTestModuleV3 extends AbstractConsentsApiPermissionGroupsTestModule {


	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3.class;
	}
}
