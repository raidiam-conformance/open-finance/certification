package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1.GetCreditFixedIncomesListV1OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "credit-fixed-incomes_api_resources_test-module",
    displayName = "credit-fixed-incomes_api_resources_test-module",
    summary = "Makes sure that the Resource API and the API that is the scope of this test plan are returning the same available IDs\n" +
        "• Call the POST Consents endpoint with the Credit Fixed Incomes API Permission Group\n" +
        "• Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Consents endpoint\n" +
        "• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
        "• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
        "• Call the GET Resources API\n" +
        "• Expect a 200 success - Extract all of the AVAILABLE resources that have been returned with the tested investment API type - CREDIT_FIXED_INCOME - Validate Response Body\n" +
        "• Call the GET Investments List Endpoint\n" +
        "• Expect a 200 - Validate Response - Make sure that all the fields that were set as AVAILABLE on the Resources API are returned on the Investments List Endpoint\n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "directory.discoveryUrl",
        "resource.brazilCpf"
    }
)
public class CreditFixedIncomesApiResourcesTestModule extends AbstractCreditFixedIncomesApiResourcesTest {

    @Override
    protected Class<? extends Condition> apiValidator() {
        return GetCreditFixedIncomesListV1OASValidator.class;
    }

}
