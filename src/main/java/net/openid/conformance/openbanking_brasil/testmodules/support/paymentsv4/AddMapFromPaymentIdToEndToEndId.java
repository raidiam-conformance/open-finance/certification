package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class AddMapFromPaymentIdToEndToEndId extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	@PostEnvironment(required = "end_to_end_id_map")
	public Environment evaluate(Environment env) {
		JsonObject body;
		try {
			body = BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
				.orElseThrow(() -> error("Could not extract body from response"))
				.getAsJsonObject();
		} catch (ParseException e) {
			throw error("Could not parse the response body");
		}

		JsonElement data = body.get("data");
		JsonObject endToEndMap = getEndToEndIdMap(env);

		if(data.isJsonArray()) {
			mapPayments(endToEndMap, data.getAsJsonArray());
		} else {
			mapPayment(endToEndMap, data.getAsJsonObject());
		}

		logSuccess("Mapped payment IDs to end to end IDs", args("end_to_end_id_map", endToEndMap));
		return env;
	}

	private JsonObject getEndToEndIdMap(Environment env) {
		JsonObject endToEndMap = env.getObject("end_to_end_id_map");
		if(endToEndMap == null) {
			endToEndMap = new JsonObject();
			env.putObject("end_to_end_id_map", endToEndMap);
		}
		return endToEndMap;
	}

	private void mapPayments(JsonObject endToEndMap, JsonArray dataArray) {
		for(JsonElement data : dataArray) {
			mapPayment(endToEndMap, data.getAsJsonObject());
		}
	}

	private void mapPayment(JsonObject endToEndMap, JsonObject data) {
		JsonObject paymentData = data.getAsJsonObject();
		String paymentIdFieldName = data.has("recurringPaymentId") ? "recurringPaymentId" : "paymentId";
		String paymentId = OIDFJSON.getString(
			Optional.ofNullable(paymentData.get(paymentIdFieldName))
				.orElseThrow(() -> error(String.format("Could not extract %s from payment", paymentIdFieldName)))
		);
		String endToEndId = OIDFJSON.getString(
			Optional.ofNullable(paymentData.get("endToEndId"))
				.orElseThrow(() -> error("Could not extract endToEndId from payment"))
		);
		endToEndMap.addProperty(paymentId, endToEndId);
	}
}
