package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions;

public class SetToTransactionDateTo180DaysAgo extends AbstractSetToTransactionDate {

    @Override
    protected int amountOfDaysToThePast() {
        return 180;
    }
}
