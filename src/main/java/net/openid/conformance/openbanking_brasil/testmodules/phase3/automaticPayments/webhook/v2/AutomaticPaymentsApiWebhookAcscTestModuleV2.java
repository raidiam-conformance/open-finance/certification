package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.abstractModule.AbstractAutomaticPaymentsApiWebhookAcscTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.addLocalInstrument.AddManuLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetUserDefinedCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToRemoveProxy;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringConsentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringPaymentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_webhook-acsc_test-module_v2",
	displayName = "automatic-payments_api_webhook-acsc_test-module_v2",
	summary = "Ensure that the tested institution has correctly implemented the webhook notification endpoint and that this endpoint is correctly called when a payments reaches the ACSC, and teh consent reaches the CONSUMED state\n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;\n" +
		"• Obtain a SSA from the Directory\n" +
		"• Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;”]\n" +
		"• Expect a 201 - Validate Response\n" +
		"• Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"• Set the recurring consents webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v2/automatic-payments/v2/recurring-consents/{recurringConsentId}, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Calls POST recurring-consents Endpoint with sweeping accounts information, and the field  recurringConfigurations.swepping.amount as 300\n" +
		"• Expects 201 - Validate Response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET recurring-consents Endpoint\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"• Calls the POST recurring-payments with one of the selected creditors, and 300 as the payment amount\n" +
		"• Expects 201 - Validate response\n" +
		"• Set the recurring payments webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v2/automatic-payments/v2/pix/recurring-payments/{recurringPaymentId} where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is ACSC\n" +
		"• Call GET recurring-consents Endpoint\n" +
		"• Expects 200 - Validate if status is \"CONSUMED\"\n" +
		"• Expect an incoming message, one for each defined endpoint, payments and consents, both which  must be mtls protected and can be received on any endpoint at any order - Wait 60 seconds for both messages to be returned\n" +
		"• For both webhook calls - Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"• Call the Delete Registration Endpoint\n" +
		"• Expect a 204 - No Content",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorAccountIspb",
		"resource.creditorAccountIssuer",
		"resource.creditorAccountNumber",
		"resource.creditorAccountAccountType",
		"resource.creditorName",
		"resource.webhookWaitTime",
		"directory.client_id",
		"directory.discoveryUrl"
	}
)
public class AutomaticPaymentsApiWebhookAcscTestModuleV2 extends AbstractAutomaticPaymentsApiWebhookAcscTestModule {

	@Override
	protected void keepOldStartDateTime() {}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddManuLocalInstrument.class);
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetUserDefinedCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentsBodyToRemoveProxy.class);
	}

	@Override
	protected ConditionSequence getUrlsFromAuthServerCondition() {
		return sequenceOf(
			condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class),
			condition(GetAutomaticPaymentPixRecurringV2Endpoint.class)
		);
	}

	@Override
	protected Class<? extends Condition> setPaymentConsentWebhookCreator() {
		return CreateRecurringConsentWebhookV2.class;
	}

	@Override
	protected Class<? extends Condition> setPaymentWebhookCreator() {
		return CreateRecurringPaymentWebhookV2.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setGetPaymentValidator() {
		return GetRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPostPaymentValidator() {
		return PostRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}
}
