package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

import java.util.List;


public class GetPaymentsConsentOASValidatorV4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected ResponseEnvKey getResponseEnvKey() {
		return ResponseEnvKey.FullConsentResponseEnvKey;
	}

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/payments/payments-4.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/consents/{consentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		assertData(body.getAsJsonObject("data"));
	}

	private void assertData(JsonObject data) {
		assertField1IsMutuallyExclusiveWithField2(
			data,
			"payment.date",
			"payment.schedule"
		);
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"payment.details.proxy",
			"payment.details.localInstrument",
			List.of("INIC","DICT","QRDN","QRES")
		);
		assertField2IsNotPresentWhenField1HasValue1(
			data,
			"payment.details.localInstrument",
			"MANU",
			"payment.details.proxy"
		);
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"payment.details.creditorAccount.issuer",
			"payment.details.creditorAccount.accountType",
			List.of("CACC","SVGS")
		);
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"debtorAccount.issuer",
			"debtorAccount.accountType",
			List.of("CACC","SVGS")
		);
	}

}
