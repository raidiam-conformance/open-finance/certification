package net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v3.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum CreatePaymentErrorEnumV3 {
	SALDO_INSUFICIENTE,
	VALOR_ACIMA_LIMITE,
	VALOR_INVALIDO,
	COBRANCA_INVALIDA,
	CONSENTIMENTO_INVALIDO,
	PARAMETRO_NAO_INFORMADO,
	PARAMETRO_INVALIDO,
	NAO_INFORMADO,
	PAGAMENTO_DIVERGENTE_CONSENTIMENTO,
	DETALHE_PAGAMENTO_INVALIDO,
	PAGAMENTO_RECUSADO_DETENTORA,
	PAGAMENTO_NAO_PERMITE_CANCELAMENTO,
	PAGAMENTO_RECUSADO_SPI,
	ERRO_IDEMPOTENCIA;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
