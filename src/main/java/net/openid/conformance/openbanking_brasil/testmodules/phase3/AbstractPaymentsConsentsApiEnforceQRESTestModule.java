package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddResourceUrlToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRESCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PaymentConsentErrorTestingSequence;
import net.openid.conformance.sequence.ConditionSequence;

import java.util.Optional;

public abstract class AbstractPaymentsConsentsApiEnforceQRESTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected void postConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndContinueOnFailure(SelectQRESCodeLocalInstrument.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
		callAndStopOnFailure(AddResourceUrlToConfig.class);
	}

	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void runTests() {
		runInBlock("Validate payment initiation consent", () -> {
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

			call(sequence(PaymentConsentErrorTestingSequence.class));
			callAndStopOnFailure(CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida.class, Condition.ConditionResult.FAILURE);
			if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
				fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
			}
			callAndStopOnFailure(EnsureConsentResponseCodeWas422.class);

			callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureErrorResponseCodeFieldWasParametroNaoInformado.class);
		});
	}


}
