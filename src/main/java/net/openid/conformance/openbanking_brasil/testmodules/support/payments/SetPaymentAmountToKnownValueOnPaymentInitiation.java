package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SetPaymentAmountToKnownValueOnPaymentInitiation extends AbstractCondition {

	private static final Pattern PATTERN = Pattern.compile("^\\{\\{(?<amount>\\d+\\.\\d+)\\}\\}$");

	@Override
	public Environment evaluate(Environment env) {
		JsonElement obj = env.getElementFromObject("resource", "brazilPixPayment.data");

		if (obj.isJsonArray()) {
			for (int i = 0; i < obj.getAsJsonArray().size(); i++) {
				JsonObject data = obj.getAsJsonArray().get(i).getAsJsonObject();
				setPaymentAmount(data);
			}
			return env;
		}

		setPaymentAmount(obj.getAsJsonObject());
		return env;
	}

	private void setPaymentAmount(JsonObject data) {
		JsonObject payment = data.getAsJsonObject("payment");

		String amount = OIDFJSON.getString(payment.get("amount"));

		Matcher matcher = PATTERN.matcher(amount);
		if(matcher.matches()) {
			amount = matcher.group("amount");
			logSuccess("Allowed configured amount to pass through", Map.of("amount", amount));
			payment.addProperty("amount", amount);
			return;
		}

		payment.addProperty("amount", "100.00");

		logSuccess("Added payment amount of 100.00 to payment");
	}
}
