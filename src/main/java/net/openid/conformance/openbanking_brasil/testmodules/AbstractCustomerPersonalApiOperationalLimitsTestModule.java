package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalFinancialRelationships;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.OperationalLimitsToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPersonalIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.AbstractOperationalLimitsTestModule;

public abstract class AbstractCustomerPersonalApiOperationalLimitsTestModule extends AbstractOperationalLimitsTestModule {

	protected abstract Class<? extends AbstractJsonAssertingCondition> getPersonalQualificationValidator();
	protected abstract Class<? extends AbstractJsonAssertingCondition> getPersonalRelationsValidator();
	protected abstract Class<? extends AbstractJsonAssertingCondition> getPersonalIdentificationValidator();

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetPersonalIdentificationsV2Endpoint.class)
		)));
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder
			.addPermissionsBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA)
			.addScopes(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.OPEN_ID).build();

		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void validateResponse() {
		// Validate Personal Identification response
		callAndStopOnFailure(getPersonalIdentificationValidator());

		eventLog.endBlock();
		disableLogging();
		// Call Personal Identification 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Personal Identification Endpoint", i + 1));
		}

		makeOverOlCall("Identification");

		// Call Personal Qualifications once with validation
		runInLoggingBlock(() -> {
			callAndStopOnFailure(PrepareToGetPersonalQualifications.class);

			preCallProtectedResource("Calling Personal Qualifications Endpoint");
			validateResponse("Validate Personal Qualifications response", getPersonalQualificationValidator());
		});


		// Call Personal Qualifications 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Personal Qualifications Endpoint", i + 1));
		}

		makeOverOlCall("Qualifications");

		// Call Customer Personal Financial Relations once with validation
		runInLoggingBlock(() -> {
			callAndStopOnFailure(PrepareToGetPersonalFinancialRelationships.class);

			preCallProtectedResource("Calling Customer Personal Financial Relations Endpoint");
			validateResponse("Validate Customer Personal Financial Relations response", getPersonalRelationsValidator());
		});


		// Call Customer Personal Financial Relations 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Customer Personal Financial Relations Endpoint", i + 1));
		}

		makeOverOlCall("Fin Relations");

	}

	private void validateResponse(String message, Class<? extends Condition> validator) {
		runInBlock(message, () -> {
			callAndStopOnFailure(validator);
		});
	}

}
