package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums;

public enum RecurringPaymentsConsentsAutomaticPixIntervalEnum {

	SEMANAL, MENSAL, ANUAL, SEMESTRAL, TRIMESTRAL;
}
