package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public abstract class AbstractAddRiskSignalsObjectToPixPaymentBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(paymentElement -> paymentElement.getAsJsonObject().getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));

		JsonObject riskSignals = getRiskSignals();

		// Add riskSignals to the data object
		data.add("riskSignals", riskSignals);

		logSuccess("The risk signals object has been added to the payment request body.",
			args("data", data));

		return env;
	}

	protected abstract JsonObject getRiskSignals();
}
