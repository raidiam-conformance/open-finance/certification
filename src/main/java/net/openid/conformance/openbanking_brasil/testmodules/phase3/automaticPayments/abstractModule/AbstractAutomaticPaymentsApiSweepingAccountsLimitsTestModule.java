package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsAutomaticObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithDailyLimits;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasLimitePeriodoQuantidadeExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasLimitePeriodoValorExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo100;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo300;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo450;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo50;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.GenerateNewE2EID;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasLimitePeriodoQuantidadeExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasLimitePeriodoValorExcedido;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractAutomaticPaymentsApiSweepingAccountsLimitsTestModule extends AbstractAutomaticPaymentsFunctionalTestModule {
	protected boolean isFailure = false;
	protected boolean isFinalCall = false;

	@Override
	protected void requestProtectedResource() {
		postAndValidatePaymentExceedingTransactionLimit();

		postAndValidatePaymentValidLimit();

		postAndValidatePaymentExceedingDayTransactionLimit();

		postAndValidatePaymentValidDayLimit();

		postAndValidatePaymentExceedingDayQuantityLimit();
	}

	protected void postAndValidatePaymentExceedingTransactionLimit() {
		callAndStopOnFailure(AddRiskSignalsAutomaticObjectToPixPaymentBody.class);
		postPaymentExpectingFailure(new SetAutomaticPaymentAmountTo450(),
			new EnsureErrorResponseCodeFieldWasLimitePeriodoValorExcedido(),
			"450.00", "LIMITE_PERIODO_VALOR_EXCEDIDO");
	}

	protected void postAndValidatePaymentValidLimit() {
		userAuthorisationCodeAccessToken();
		postPaymentsExpectingSuccess(new SetAutomaticPaymentAmountTo300(), "300.00");
	}

	protected void postAndValidatePaymentExceedingDayTransactionLimit() {
		userAuthorisationCodeAccessToken();
		postPaymentExpectingFailure(new SetAutomaticPaymentAmountTo300(),
			new EnsureErrorResponseCodeFieldWasLimitePeriodoValorExcedido(),
			"300.00", "LIMITE_PERIODO_VALOR_EXCEDIDO");
	}

	protected void postAndValidatePaymentValidDayLimit() {
		userAuthorisationCodeAccessToken();
		postPaymentsExpectingSuccess(new SetAutomaticPaymentAmountTo100(), "100.00");
	}

	protected void postAndValidatePaymentExceedingDayQuantityLimit() {
		isFinalCall = true;
		userAuthorisationCodeAccessToken();
		postPaymentExpectingFailure(new SetAutomaticPaymentAmountTo50(),
			new EnsureErrorResponseCodeFieldWasLimitePeriodoQuantidadeExcedido(),
			"50.00", "LIMITE_PERIODO_QUANTIDADE_EXCEDIDO");
	}

	protected void postPaymentsExpectingSuccess(AbstractSetAutomaticPaymentAmount setAmountCondition, String amount) {
		callAndStopOnFailure(GenerateNewE2EID.class);
		isFailure = false;
		runInBlock(String.format("POST recurring-payments with amount equals to %s - Expects 201", amount), () -> {
			callAndStopOnFailure(setAmountCondition.getClass());
			call(getPixPaymentSequence());
		});
		validateResponse();
	}

	protected void postPaymentExpectingFailure(AbstractSetAutomaticPaymentAmount setAmountCondition,
											   AbstractEnsureErrorResponseCodeFieldWas errorCodeCondition,
											   String amount,
											   String errorCode) {
		isFailure = true;
		runInBlock(String.format("POST recurring-payments with amount equals to %s - Expects 422 %s", amount, errorCode), () -> {
			callAndStopOnFailure(GenerateNewE2EID.class);
			callAndStopOnFailure(setAmountCondition.getClass());
			call(getPixPaymentSequence().replace(EnsureResourceResponseCodeWas201.class,
				condition(EnsureResourceResponseCodeWas201Or422.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)));

			int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
			if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				callAndContinueOnFailure(postPaymentValidator(), Condition.ConditionResult.FAILURE);
				call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));
				callAndContinueOnFailure(errorCodeCondition.getClass(), Condition.ConditionResult.FAILURE);
				call(exec().unmapKey("endpoint_response"));
			} else if (status == HttpStatus.CREATED.value()) {
				validateResponse();
			}
		});
	}

	@Override
	protected void validateFinalState() {
		if (isFailure) {
			callAndContinueOnFailure(EnsurePaymentStatusWasRjct.class, Condition.ConditionResult.FAILURE);
			if (isFinalCall) {
				callAndContinueOnFailure(EnsurePaymentRejectionReasonCodeWasLimitePeriodoQuantidadeExcedido.class, Condition.ConditionResult.FAILURE);
			} else {
				callAndContinueOnFailure(EnsurePaymentRejectionReasonCodeWasLimitePeriodoValorExcedido.class, Condition.ConditionResult.FAILURE);
			}
		} else {
			callAndStopOnFailure(EnsurePaymentStatusWasAcsc.class, Condition.ConditionResult.FAILURE);
		}
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithDailyLimits();
	}
}
