package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.abstractModule.AbstractAutomaticPaymentsApiWebhookMultipleConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringConsentWebhookV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringPaymentWebhookV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringPixOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringPixOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_webhook-multiple-consents_test-module_v1",
	displayName = "automatic-payments_api_webhook-multiple-consents_test-module_v1",
	summary = "Ensure that the tested institution has correctly implemented the webhook notification endpoint and that this endpoint is correctly called when a consent reaches the AUTHORISED state, if this is realetd to a multiple consents account. This test will use the CPF/CNPJ on the config for \"Payment consent -  XXX - Multiple Consents Test\", or Skipped if the field is not filled. The CPF/CNPJ informed will also be sent at the creditor account.\n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;\n" +
		"• Obtain a SSA from the Directory\n" +
		"• Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;”]\n" +
		"• Expect a 201 - Validate Response\n" +
		"• Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"• Set the recurring consents webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v1/automatic-payments/v1/recurring-consents/{recurringConsentId}, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, with the Multiple Accounts CPF/CNPJ\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is PARTIALLY_ACCEPTED\n" +
		"• Call the POST recurring-payments endpoint with the selected creditor\n" +
		"• Expect 422 CONSENTIMENTO_PENDENTE_AUTORIZACAO - Validate Response\n" +
		"• Poll the GET recurring-consents endpoint while status is PARTIALLY_ACCEPTED\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED\n" +
		"• Call the POST recurring-payments endpoint with the selected creditor\n" +
		"• Expect 201 - Validate Response\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is CONSUMED\n" +
		"• Expect an incoming message, the defined consents endpoint, which  must be mtls protected - Wait 60 seconds for the messages to be returned\n" +
		"• For the webhook calls - Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within now and the start of the test\n" +
		"• Call the Delete Registration Endpoint\n" +
		"• Expect a 204 - No Content",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName",
		"conditionalResources.brazilCpfJointAccount",
		"conditionalResources.brazilCnpjJointAccount",
		"resource.webhookWaitTime",
		"directory.client_id",
		"directory.discoveryUrl"
	}
)
public class AutomaticPaymentsApiWebhookMultipleConsentsTestModuleV1 extends AbstractAutomaticPaymentsApiWebhookMultipleConsentsTestModule {

	@Override
	protected ConditionSequence getUrlsFromAuthServerCondition() {
		return sequenceOf(
			condition(GetAutomaticPaymentRecurringConsentV1Endpoint.class),
			condition(GetAutomaticPaymentPixRecurringV1Endpoint.class)
		);
	}

	@Override
	protected Class<? extends Condition> setPaymentConsentWebhookCreator() {
		return CreateRecurringConsentWebhookV1.class;
	}

	@Override
	protected Class<? extends Condition> setPaymentWebhookCreator() {
		return CreateRecurringPaymentWebhookV1.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PostRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> setGetPaymentValidator() {
		return GetRecurringPixOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> setPostPaymentValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return false;
	}
}
