package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo;

import org.springframework.http.HttpMethod;

public class PrepareToFetchEnrollmentsRequest extends AbstractPrepareToMakeACallToEnrollmentsEndpoint {

	@Override
	protected HttpMethod httpMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected String endpoint() {
		return "";
	}

	@Override
	protected boolean expectsSelfLink() {
		return true;
	}
}
