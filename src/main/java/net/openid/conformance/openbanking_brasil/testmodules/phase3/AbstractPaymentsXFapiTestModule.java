package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadSelfLinkUri;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow200or406;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveSelfLinkUri;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs.EnsureScheduledDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentConsentEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentConsentEndpointSequenceErrorAgnostic;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentEndpointSequenceErrorAgnostic;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPatchPixPaymentsEndpointSequenceErrorAgnostic;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPatchPixPaymentsEndpointSequenceV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequenceErrorAgnostic;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;


public abstract class AbstractPaymentsXFapiTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {
	protected abstract Class<? extends Condition> paymentConsentErrorValidator();
	protected abstract Class<? extends Condition> patchValidator();

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
	}

	@Override
	public void start() {
		setStatus(Status.RUNNING);

		call(exec().startBlock("Ensure error when calling POST Consent without x-fapi-interaction id"));
		executePostConsentUnhappyPath();
		eventLog.endBlock();

		call(exec().startBlock("Creating scheduled-payment Consent with x-fapi-interaction id, expecting success"));
		createScheduledPaymentConsent();
		eventLog.endBlock();
	}

	@Override
	protected void performPostAuthorizationFlow() {
		call(exec().startBlock("Ensure error when calling GET Consent without x-fapi-interaction id"));
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		ConditionSequence getConsentErrorSequence = new CallGetPaymentConsentEndpointSequenceErrorAgnostic()
			.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Not adding x-fapi-i-id , expecting error")
			.replace(OptionallyAllow200or406.class, condition(EnsureConsentResponseCodeWas400.class))
			.skip(EnsureMatchingFAPIInteractionId.class, "Not adding x-fapi-i-id , expecting error")
			.insertAfter(FetchServerKeys.class,condition(getPaymentConsentValidator()));
		call(getConsentErrorSequence);
		eventLog.endBlock();


		call(exec().startBlock("Calling GET Consent with x-fapi-interaction id, expecting success"));
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		ConditionSequence getConsentSequence = new CallGetPaymentConsentEndpointSequence()
			.replace(PaymentConsentValidatorV2.class,condition(getPaymentConsentValidator()));
		call(getConsentSequence);
		eventLog.endBlock();

		env.unmapKey("access_token");
		env.unmapKey("old_access_token");
		eventLog.startBlock(currentClientString() + "Call token endpoint");
		createAuthorizationCodeRequest();
		requestAuthorizationCode();

		call(exec().startBlock("Ensure error when calling POST Payments without x-fapi-interaction id"));
		ConditionSequence errorPostPixSequence = new CallPixPaymentsEndpointSequenceErrorAgnostic()
			.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Not adding x-fapi-i-id , expecting error")
			.replace(OptionallyAllow200or406.class, condition(EnsureResourceResponseCodeWas400.class))
			.skip(EnsureMatchingFAPIInteractionId.class, "Not adding x-fapi-i-id , expecting error");
		call(errorPostPixSequence);
		callAndContinueOnFailure(postPaymentValidator(), Condition.ConditionResult.FAILURE);
		eventLog.endBlock();


		call(exec().startBlock("Calling POST Payments with x-fapi-interaction id, expecting success"));
		ConditionSequence successPostPixSequence = getPixPaymentSequence();
		call(successPostPixSequence);
		postPaymentValidationSequence();
		eventLog.endBlock();


		call(exec().startBlock("Ensure error when calling GET Payments without x-fapi-interaction id"));
		ConditionSequence errorGetPixSequence = new CallGetPaymentEndpointSequenceErrorAgnostic()
			.insertAfter(SetProtectedResourceUrlToSelfEndpoint.class, condition(SaveSelfLinkUri.class))
			.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Not adding x-fapi-i-id , expecting error")
			.replace(OptionallyAllow200or406.class, condition(EnsureResourceResponseCodeWas400.class))
			.skip(EnsureMatchingFAPIInteractionId.class, "Not adding x-fapi-i-id , expecting error")
			.insertAfter(EnsureContentTypeJson.class,condition(getPaymentValidator()));
		call(errorGetPixSequence);
		eventLog.endBlock();


		call(exec().startBlock("Calling GET Payments with x-fapi-interaction id, expecting success"));
		ConditionSequence getPaymentNoAcceptSequence = new CallGetPaymentEndpointSequence()
			.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(LoadSelfLinkUri.class))
			.replace(PaymentInitiationPixPaymentsValidatorV2.class, condition(getPaymentValidator()));
		call(getPaymentNoAcceptSequence);
		pollGetPaymentSequence();
		eventLog.endBlock();

		call(exec().startBlock("Ensure error when calling PATCH Payments without x-fapi-interaction id"));
		callAndStopOnFailure(CreatePatchPaymentsRequestFromConsentRequestV2.class);
		ConditionSequence patchPaymentsErrorSequence = new CallPatchPixPaymentsEndpointSequenceErrorAgnostic()
			.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Not adding x-fapi-i-id , expecting error")
			.replace(EnsureResourceResponseCodeWas200.class, condition(EnsureResourceResponseCodeWas400.class))
			.skip(EnsureMatchingFAPIInteractionId.class, "Not adding x-fapi-i-id , expecting error");
		call(patchPaymentsErrorSequence);
		callAndContinueOnFailure(patchValidator(), Condition.ConditionResult.FAILURE);
		eventLog.endBlock();

		call(exec().startBlock("Calling PATCH Payments with x-fapi-interaction id, expecting success"));
		ConditionSequence patchPaymentsSequence = new CallPatchPixPaymentsEndpointSequenceV2()
			.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(LoadSelfLinkUri.class));
		call(patchPaymentsSequence);
		callAndContinueOnFailure(patchValidator(), Condition.ConditionResult.FAILURE);
		eventLog.endBlock();

		fireTestFinished();
	}

	protected void executePostConsentUnhappyPath() {
		call(new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Not adding x-fapi-i-id , expecting error")
			.skip(EnsureMatchingFAPIInteractionId.class, "Not adding x-fapi-i-id , expecting error")
			.skip(ExtractConsentIdFromConsentEndpointResponse.class, "Not needed in this test")
			.skip(FAPIBrazilAddConsentIdToClientScope.class, "Not needed in this test")
			.replace(EnsureHttpStatusCodeIs201.class, condition(EnsureConsentResponseCodeWas400.class))
			.skip(EnsureContentTypeApplicationJwt.class, "Expecting a JSON")
			.skip(ExtractSignedJwtFromResourceResponse.class, "Expecting a JSON")
			.skip(FAPIBrazilValidateResourceResponseSigningAlg.class, "Expecting a JSON")
			.skip(FAPIBrazilValidateResourceResponseTyp.class, "Expecting a JSON")
			.skip(ValidateResourceResponseSignature.class, "Expecting a JSON")
			.skip(ValidateResourceResponseJwtClaims.class, "Expecting a JSON")
			.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Not adding x-fapi-i-id, expecting error")
			.skip(EnsureMatchingFAPIInteractionId.class, "Not adding x-fapi-i-id, expecting error"));
		callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
	}

	protected void createScheduledPaymentConsent() {
		callAndStopOnFailure(RemovePaymentConsentDate.class);
		callAndStopOnFailure(EnsureScheduledDateIsTomorrow.class);
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		configureDictInfo();
		performAuthorizationFlow();
	}

	@Override
	protected void updatePaymentConsent() {
		// date is not needed in this test since the payment is scheduled
	}

	protected void pollGetPaymentSequence() {

		call(getPaymentValidationSequence());
		callAndStopOnFailure(SetProtectedResourceUrlToSelfEndpoint.class);
		repeatSequence(this::getRepeatSequenceSecondTest)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(5)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		callAndStopOnFailure(EnsurePaymentStatusWasSchd.class);
	}

	protected ConditionSequence getRepeatSequenceSecondTest() {
		return getRepeatSequence()
			.skip(SetProtectedResourceUrlToSelfEndpoint.class, "URL already set");
	}

}
