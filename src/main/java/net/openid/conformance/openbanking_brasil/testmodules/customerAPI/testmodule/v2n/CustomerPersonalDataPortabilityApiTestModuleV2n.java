package net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractCustomerPersonalDataApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePortabilitiesReceived;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPersonalIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.PersonalIdentificationResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.PersonalQualificationResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.PersonalRelationsResponseOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "customer-personal_api_portability_test-module_v2-2",
	displayName = "Validate structure of all personal customer data API resources",
	summary = """
		Validates the structure of all personal Customer Data API Endpoints. This test will simulate that the bank has received a portability , therefore, will need to share the employee data at the PortabilitiesReceived object.
		• Creates a Consent will the customer personal permissions ("CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ","CUSTOMERS_PERSONAL_ADITTIONALINFO_READ","RESOURCES_READ")
		• Expects a success 201 - Check all of the fields sent on the consent API is spec compliant
		• Calls GET Personal Qualifications resources V2
		• Expects a success 200
		• Calls GET Personal financial relations resources V2
		• Expects a success 200 - Ensure portabilitiesReceived field is included in response and validate response
		• Calls GET Personal identifications relations resources V2
		• Expects a success 200
	""",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class CustomerPersonalDataPortabilityApiTestModuleV2n extends AbstractCustomerPersonalDataApiTestModule {
	@Override
	protected ConditionSequence getEndpoints() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetPersonalIdentificationsV2Endpoint.class)
		);
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPersonalQualificationValidator() {
		return PersonalQualificationResponseOASValidatorV2.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPersonalRelationsValidator() {
		return PersonalRelationsResponseOASValidatorV2.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPersonalIdentificationValidator() {
		return PersonalIdentificationResponseOASValidatorV2.class;
	}

	@Override
	protected void validatePersonalRelationsResponse() {
		super.validatePersonalRelationsResponse();
		callAndContinueOnFailure(EnsurePortabilitiesReceived.class, Condition.ConditionResult.FAILURE);
	}
}
