package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SaveResponse extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	@PostEnvironment(required = "saved_response")
	public Environment evaluate(Environment env) {
		env.putObject("saved_response", env.getObject(RESPONSE_ENV_KEY).deepCopy());
		logSuccess("Last endpoint response has been saved in the environment");
		return env;
	}
}
