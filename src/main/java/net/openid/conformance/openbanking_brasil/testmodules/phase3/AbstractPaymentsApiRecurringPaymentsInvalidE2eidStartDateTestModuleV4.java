package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.overridePaymentsInResources.OverridePaymentInResourcesWithWrongWeeklySchedule;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.Schedule5WeeklyPaymentsForMondayStarting1DayInTheFuture;

public abstract class AbstractPaymentsApiRecurringPaymentsInvalidE2eidStartDateTestModuleV4 extends AbstractPaymentApiSchedulingUnhappyTestModule {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(OverridePaymentInResourcesWithWrongWeeklySchedule.class);
	}

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new Schedule5WeeklyPaymentsForMondayStarting1DayInTheFuture();
	}
}
