package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiNoContractTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostEnrollmentsOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_no-contract_test-module_v1",
	displayName = "enrollments_api_no-contract_test-module_v1",
	summary = "Ensure no enrollment can be created if the bilateral contract is not in place. For this test module, the Conformance Suite will send a request with the client ID set to the value set for this test in the config:\n" +
		"• Call the POST enrollments endpoint with a token issued to the client_id that does not have a contract\n" +
		"• Expect 403 - Validate error response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.noContractClientId"
	}
)
public class EnrollmentsApiNoContractTestModuleV1 extends AbstractEnrollmentsApiNoContractTestModule {

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetEnrollmentsV1Endpoint.class));
	}

	@Override
	public void cleanup() {
		//not required
	}
}
