package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LoadOldKeyPair extends AbstractCondition {

	@Override
	@PreEnvironment(required = "saved_fido_keys_jwk")
	@PostEnvironment(required = "fido_keys_jwk")
	public Environment evaluate(Environment env) {
		JsonObject savedFidoKeysJwk = env.getObject("saved_fido_keys_jwk");
		env.putObject("fido_keys_jwk", savedFidoKeysJwk);
		logSuccess("Saved key pair has been loaded from the environment");
		return env;
	}
}
