package net.openid.conformance.openbanking_brasil.testmodules.productsNServices.v1.unarrangedAccountOverdraft;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountOverdraft.v1.GetUnarrangedAccountBusinessOverdraftOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.LogOnlyFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToGetProductsNChannelsApi;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "unarranged_account_business_overdraft_api_structural_test-module_v1",
	displayName = "Validate structure of ProductsNServices Business Unarranged Account Overdraft Api resources",
	summary =
		"\u2022 Make a unauthenticated request at the required Endpoint\n" +
		"\u2022 Expect 200 - Validate Response",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4
)
public class UnarrangedAccountBusinessOverdraftApiTestModuleV1 extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate ProductsNServices Business Unarranged Account Overdraft response",
			() -> {
				callAndStopOnFailure(PrepareToGetProductsNChannelsApi.class);
				preCallResource();
				callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(LogOnlyFailure.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(GetUnarrangedAccountBusinessOverdraftOASValidatorV1.class,
					Condition.ConditionResult.FAILURE);
			});
	}
}
