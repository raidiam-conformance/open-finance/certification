package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class ReplaceClientIdForUnauthorizedClientIdInConfig extends AbstractCondition {

	@Override
	@PreEnvironment(required = { "client", "resource" })
	public Environment evaluate(Environment env) {
		String clientId = Optional.ofNullable(env.getElementFromObject("resource", "noContractClientId"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("The client_id for no contract test is not present in the resource"));
		env.putString("client", "client_id", clientId);
		logSuccess("Client id has been replaced by an unauthorized one", args("client_id", clientId));
		return env;
	}
}
