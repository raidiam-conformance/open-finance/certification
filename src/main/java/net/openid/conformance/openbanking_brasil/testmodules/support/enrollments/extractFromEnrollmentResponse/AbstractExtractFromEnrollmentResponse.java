package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public abstract class AbstractExtractFromEnrollmentResponse extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	protected abstract String fieldName();
	protected abstract String envVarName();

	protected JsonElement bodyFrom(Environment environment) {
		try {
			return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {

		String fieldValue = Optional.ofNullable(bodyFrom(env))
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.map(data -> data.get(fieldName()))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error(String.format("Could not find data.%s in enrollments response", fieldName())));

		env.putString(envVarName(), fieldValue);
		logSuccess("Successfully extracted " + fieldName(), args(fieldName(), fieldValue));

		return env;
	}
}
