package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CheckForDateHeaderInResourceResponse;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureResourceResponseReturnedJsonContentType;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForAccountsRoot;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountBalances;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureAccountListIsEmpty;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureJointAccountCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureSpecifiedIdIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractAllSpecifiedApiIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractFirstResourceIdToSpecifiedEnvironmentKey;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.FindSpecifiedResourceId;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasStatusResourcePendingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFGroupingEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractAccountsApiResourcesMultipleConsentsTestModule extends AbstractPhase2TestModule {

	private static final String RESOURCE_TYPE = EnumResourcesType.ACCOUNT.name();

	private static final String API_RESOURCE_ID = "accountId";

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class),
			condition(GetResourcesV3Endpoint.class));
	}


	@Override
	protected void configureClient() {
		super.configureClient();
		env.putBoolean("continue_test", true);
		env.putBoolean("functionality_skipped",false);
		callAndContinueOnFailure(EnsureJointAccountCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			env.putBoolean("functionality_skipped",true);
			fireTestSkipped("Brazil CPF and CNPJ for Joint Account field is empty. Institution is assumed to not have this functionality");
		}
	}

	@Override
	protected void requestProtectedResource() {
		// Call Resources API
		callAndStopOnFailure(GetResourcesV3Endpoint.class);
		runInBlock("Call Resources V3 API", () -> call(getPreCallProtectedResourceSequence()));

		runInBlock("Validate Resources V3 response", () -> {
			callAndStopOnFailure(GetResourcesOASValidatorV3.class, Condition.ConditionResult.FAILURE);

			env.putString("resource_type", RESOURCE_TYPE);
			env.putString("resource_status", EnumResourcesStatus.PENDING_AUTHORISATION.name());
			callAndStopOnFailure(ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus.class);

			env.putString("environment_key", API_RESOURCE_ID);
			callAndStopOnFailure(ExtractFirstResourceIdToSpecifiedEnvironmentKey.class);

		});

		callAndStopOnFailure(GetAccountsV2Endpoint.class);
		callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
		super.requestProtectedResource(); // Call Accounts API
	}

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getAccountListValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getAccountBalancesValidator();

	@Override
	protected void validateResponse() {
		// Accounts Validation
		callAndStopOnFailure(EnsureAccountListIsEmpty.class);
		callAndStopOnFailure(getAccountListValidator());

		// Call ACCOUNTS BALANCES API
		callAndStopOnFailure(PrepareUrlForFetchingAccountBalances.class);
		runInBlock("Call Accounts Balances API", () -> call(getPreCallProtectedResourceSequence()
			.replace(EnsureResourceResponseCodeWas200.class, condition(EnsureResourceResponseCodeWas403.class))
		));

		runInBlock("Validate Accounts Balances response", () -> {
			call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));
			callAndStopOnFailure(getAccountBalancesValidator());
			callAndStopOnFailure(EnsureErrorResponseCodeFieldWasStatusResourcePendingAuthorisation.class);
			call(exec().unmapKey("endpoint_response"));
		});

		// Poll Resources API
		runInBlock("Poll Resources API", () -> {
			callAndStopOnFailure(GetResourcesV3Endpoint.class);
			callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);

			env.putString("resource_status", EnumResourcesStatus.AVAILABLE.name());
			env.putString("resource_id", env.getString("accountId"));
			ConditionSequenceRepeater repeatSequence = repeatSequence(() -> getPreCallProtectedResourceSequence()
				.then(getPollingSequence()))
				.untilTrue("resource_found")
				.times(10)
				.trailingPause(30)
				.onTimeout(sequenceOf(
					condition(TestTimedOut.class),
					condition(ChuckWarning.class)));

			repeatSequence.run();

		});

		// Call accounts API
		callAndStopOnFailure(GetAccountsV2Endpoint.class);
		callAndStopOnFailure(PrepareUrlForAccountsRoot.class);
		runInBlock("Call Accounts API", () -> call(getPreCallProtectedResourceSequence()));

		runInBlock("Validate Accounts response", () -> {
			env.putString("apiIdName", API_RESOURCE_ID);
			callAndStopOnFailure(getAccountListValidator());
			callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);
			callAndStopOnFailure(EnsureSpecifiedIdIsPresent.class);
		});


	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.ACCOUNTS, OPFScopesEnum.RESOURCES, OPFScopesEnum.OPEN_ID)
			.addPermissionsBasedOnGrouping(OPFGroupingEnum.ACCOUNTS_BALANCES)
			.build();
	}

	protected ConditionSequence getPollingSequence() {
		return sequenceOf(
			condition(GetResourcesOASValidatorV3.class),
			condition(FindSpecifiedResourceId.class)
		);
	}

	protected ConditionSequence getPreCallProtectedResourceSequence() {
		return sequenceOf(
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(CallProtectedResource.class),
			condition(EnsureResourceResponseCodeWas200.class),
			condition(CheckForDateHeaderInResourceResponse.class),
			condition(CheckForFAPIInteractionIdInResourceResponse.class),
			condition(EnsureResourceResponseReturnedJsonContentType.class)
		);
	}

	@Override
	public void cleanup() {
		if(!env.getBoolean("functionality_skipped")) {
			super.cleanup();
		}
	}
}
