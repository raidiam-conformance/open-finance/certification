package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.AddDummyCustomerIpAddress;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus365;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateIndefiniteConsentExpiryTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureConsentStatusUpdateTimeHasNotChanged;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureResponseCodeWas401or403;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.PrepareToGetConsentExtensions;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInConsentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInGetSizeOne;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateIndefiniteExpirationTimeReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPostConsentExtensionsSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.sequence.ConditionSequence;


public abstract class AbstractConsentApiExtensionNoHeader extends AbstractSimplifiedConsentRenewalTestModule {

	@Override
	protected abstract Class<? extends Condition> setGetConsentValidator();

	@Override
	protected abstract Class<? extends Condition> setConsentCreationValidation();

	@Override
	protected abstract Class<? extends Condition> setPostConsentExtensionValidator();

	@Override
	protected abstract Class<? extends Condition> setGetConsentExtensionValidator();

	protected boolean isWithCustomerIpHeader = false;

	@Override
	public void callExtensionEndpoints(){
		runInBlock("Validating post consent extension response without customer ip address header", this::callPostConsentExtensionEndpoint);
		env.mapKey("access_token", "client_credentials_token");
		runInBlock("Validating post consent extension response with client credentials token", this::callPostConsentExtensionEndpoint);
	}
	@Override
	public ConditionSequence getPostConsentExtensionSequence() {
		ConditionSequence postConsentExtensionSequence = new CallPostConsentExtensionsSequence()
			.replace(CreateIndefiniteConsentExpiryTime.class, condition(setExtensionExpirationTime()).onFail(Condition.ConditionResult.FAILURE))
			.replace(ValidateIndefiniteExpirationTimeReturned.class, condition(validateExpirationTimeReturned()).onFail(Condition.ConditionResult.FAILURE))
			.skip(EnsureConsentWasAuthorised.class, "Expecting a 401 or 403 in this test")
			.replace(EnsureConsentResponseCodeWas201.class,condition(EnsureResponseCodeWas401or403.class))
			.skip(ValidateIndefiniteExpirationTimeReturned.class,"Expecting a 401 or 403 in this test")
			.skip(EnsureConsentStatusUpdateTimeHasNotChanged.class,"Expecting a 401 or 403 in this test");

		if(!isWithCustomerIpHeader){
			postConsentExtensionSequence
				.skip(AddDummyCustomerIpAddress.class, "Skipping this to expect an error")
				.replace(EnsureConsentResponseCodeWas201.class,condition(EnsureResourceResponseCodeWas400.class).dontStopOnFailure())
				.insertAfter(EnsureConsentResponseCodeWas201.class,condition(setPostConsentExtensionValidator()));
		}
		isWithCustomerIpHeader = true;
		return postConsentExtensionSequence;
	}

	@Override
	protected void configureClient() {
		env.putString("metaOnlyRequestDateTime", "true");
		super.configureClient();
	}

	@Override
	protected void callGetConsentAndOrResourceUrlSequence(){
		call(new ValidateRegisteredEndpoints(
				sequenceOf(
					condition(GetConsentV3Endpoint.class),
					condition(GetResourcesV3Endpoint.class)
				)
			)
		);
	}

	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {
		return CreateExtensionRequestTimeDayPlus365.class;
	}
	@Override
	protected Class<? extends Condition> validateExpirationTimeReturned(){
		return ValidateExtensionExpiryTimeInConsentResponse.class;
	}

	@Override
	protected Class<? extends Condition> validateGetConsentExtensionEndpointResponse() {
		return ValidateExtensionExpiryTimeInGetSizeOne.class;
	}

	@Override
	protected Class<? extends Condition> buildConsentRequestBody() {
		return FAPIBrazilOpenBankingCreateConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setConsentExpirationTime() {
		return FAPIBrazilAddExpirationToConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionEndpoint() {
		return PrepareToGetConsentExtensions.class;
	}

}
