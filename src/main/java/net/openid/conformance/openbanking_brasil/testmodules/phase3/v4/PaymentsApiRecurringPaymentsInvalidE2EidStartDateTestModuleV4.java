package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiRecurringPaymentsInvalidE2eidStartDateTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "payments_api_recurring-payments-invalid-e2eid-startDate_test-module_v4",
	displayName = "Ensure that consent for monthly recurring payments can be carried out and that the payment is not successfully scheduled when the e2eid does not match the consent. This test cannot be executed on a Sunday.",
	summary = "Ensure that consent for monthly recurring payments can be carried out and that the payment is not successfully scheduled when the e2eid does not match the consent. This test cannot be executed on a Sunday.\n" +
		"\u2022 Call the POST Consents endpoints with the schedule.weekly.dayOfWeek field set as SEGUNDA_FEIRA, schedule.weekly.startDate field set as D+1, and schedule.weekly.quantity as 5\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is ““AUTHORISED”” and validate response\n" +
		"\u2022 Calls the POST Payments Endpoint with the 5 Payments, using the apprpriate wth the e2eid starting from D+1 with a week gap with each other, and not the correct expected date\n" +
		"\u2022 Expects 422 - PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is ““CONSUMED”” and validate respons",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiRecurringPaymentsInvalidE2EidStartDateTestModuleV4
	extends AbstractPaymentsApiRecurringPaymentsInvalidE2eidStartDateTestModuleV4 {


	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento();
	}
}
