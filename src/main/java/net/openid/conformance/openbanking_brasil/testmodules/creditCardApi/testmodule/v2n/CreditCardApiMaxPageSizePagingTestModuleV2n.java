package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.abstractModule.AbstractCreditCardApiMaxPageSizePagingTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsTransactionsOASValidatorV2;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "credit-cards_api_max-page-size_test-module_v2-1",
	displayName = "Test result set paging: Banks should configure a test credit card transactions list which contains their maximum page-size + 1 items (maximum page-size must be between 25 and 1000). For example, if the bank support a maximum page-size of 50, then they must setup a test resource with at least 51 items. The initial request should receive a response with 50 items. Requesting the 'next' link, found in the metadata, should receive a response with at least 1 item",
	summary = "Test result set paging: Banks should configure a test credit card transactions list that contains their maximum page-size + 1 item (maximum page-size must be between 25 and 1000). For example, if the bank supports a maximum page-size of 50, then they must set up a test resource with at least 51 items. The initial request should receive a response with 50 items. Requesting the 'next' link, found in the metadata, should receive a response with at least 1 item\n" +
		"• Creates a Consent with the complete set of the credit cards permission group ([\"CREDIT_CARDS_ACCOUNTS_READ\", \"CREDIT_CARDS_ACCOUNTS_BILLS_READ\", \"CREDIT_CARDS_ACCOUNTS_BILLS_TRANSACTIONS_READ\", \"CREDIT_CARDS_ACCOUNTS_LIMITS_READ\", \"CREDIT_CARDS_ACCOUNTS_TRANSACTIONS_READ\", \"RESOURCES_READ\"])\n" +
		"• Expects a success 201 - Expects a success on Redirect as well \n" +
		"• Calls GET accounts endpoint \n" +
		"• Expects a 200 response and extracts an accountId \n" +
		"• Calls GET account transaction endpoint for the extracted accountId with page size=1000 from D-360 to D-0\n" +
		"• Expects a 200 response and expect that the links and meta attributes display the next page \n" +
		"• Validates that the number of records returned is equals to page-size from \"self\" link\n" +
		"• Fetch the \"next\" link\n" +
		"• Expects a 200 response\n" +
		"• Validates that there is only one record",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class CreditCardApiMaxPageSizePagingTestModuleV2n extends AbstractCreditCardApiMaxPageSizePagingTestModule {

	@Override
	protected Class<? extends Condition> getCardAccountsTransactionsValidator() {
		return GetCreditCardAccountsTransactionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}

