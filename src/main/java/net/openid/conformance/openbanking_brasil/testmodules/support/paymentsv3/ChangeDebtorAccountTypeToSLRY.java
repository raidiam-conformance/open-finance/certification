package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;


import net.openid.conformance.testmodule.Environment;

public class ChangeDebtorAccountTypeToSLRY extends AbstractChangeDebtorAccountType {

	@Override
	protected String getAccountType(Environment env) {
		return AccountTypeEnumV3.SLRY.toString();
	}
}
