package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.extensions.SpringContext;
import net.openid.conformance.extensions.yacs.ParticipantsService;
import net.openid.conformance.testmodule.Environment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class EnsureWellKnownUriIsRegistered extends AbstractCondition {


	@Override
	@PreEnvironment(required = {"config"})
	public Environment evaluate(Environment env) {

		ParticipantsService participantsService = getParticipantsService();
		String organisationId = getStringFromEnvironment(env, "config", "resource.brazilOrganizationId", "Config brazilOrganizationId Field");
		String configWellKnownUri = env.getString("config", "server.discoveryUrl");
		String authorisationServerId = env.getString("authorisationServerId");


		final Predicate<Map> serverFilter = getServerFilter(configWellKnownUri, authorisationServerId);

		organisationId = organisationId.replaceAll("-", "_");

		Set<Map> orgs = participantsService.orgsFor(Set.of(organisationId));
		List<Map> authServers = new ArrayList<>();

		orgs.stream()
			.map(org -> (List<Map>) org.get("AuthorisationServers"))
			.forEach(serverList -> authServers.addAll(serverList.stream()
				.filter(serverFilter)
				.collect(Collectors.toList())));

		if (authServers.isEmpty()) {
			if (!Strings.isNullOrEmpty(authorisationServerId)) {
				throw error("Could not find Authorisation Server with provided Authorisation Server ID in the Directory Participants List",
					args("Authorisation Server ID", authorisationServerId, "OrganisationID", organisationId));
			}

			throw error("Could not find Authorisation Server with provided Well-Known URL in the Directory Participants List",
				args("Well-Known", configWellKnownUri, "OrganisationID", organisationId));

		}


		if (!Strings.isNullOrEmpty(authorisationServerId)) {
			logSuccess("Found Authorisation Server with provided Authorisation Server ID in the Directory Participants List",
				args("Authorisation Server ID", authorisationServerId, "OrganisationID", organisationId));
		} else {

			logSuccess("Found Authorisation Server with provided Well-Known URL in the Directory Participants List",
				args("Well-Known", configWellKnownUri, "OrganisationID", organisationId));
		}


		return env;
	}

	private Predicate<Map> getServerFilter(String configWellKnownUri, String authorisationServerId) {
		Predicate<Map> wellKnownFilter = server -> String.valueOf(server.get("OpenIDDiscoveryDocument")).equals(configWellKnownUri);
		Predicate<Map> authorisationServerIdFilter = server -> String.valueOf(server.get("AuthorisationServerId")).equals(authorisationServerId);


		if (!Strings.isNullOrEmpty(authorisationServerId)) {
			return authorisationServerIdFilter;
		}

		if (!Strings.isNullOrEmpty(configWellKnownUri)) {
			return wellKnownFilter;
		}
		throw error("Either config.server.discoveryUrl or authorisationServerId must be present in the environment");
	}

	private ParticipantsService getParticipantsService() {
		return SpringContext.getBean(ParticipantsService.class);
	}
}
