package net.openid.conformance.openbanking_brasil.testmodules.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddClientAssertionToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddClientIdToTokenEndpointRequest;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckForAccessTokenValue;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CreateClientAuthenticationAssertionClaims;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractRefreshTokenFromTokenResponse;
import net.openid.conformance.condition.client.SignClientAuthenticationAssertion;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.GenerateRefreshTokenRequest;
import net.openid.conformance.sequence.AbstractConditionSequence;
import net.openid.conformance.variant.ClientAuthType;

public class GenerateRefreshAccessTokenSteps extends AbstractConditionSequence {
	private ClientAuthType clientAuthType;

	public GenerateRefreshAccessTokenSteps(ClientAuthType clientAuthType) {
		this.clientAuthType = clientAuthType;
	}
	@Override
	public void evaluate() {
		call(exec().startBlock("Refreshing Access Token"));
		call(condition(ExtractRefreshTokenFromTokenResponse.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.INFO));

		callAndStopOnFailure(GenerateRefreshTokenRequest.class);

		if(clientAuthType == ClientAuthType.PRIVATE_KEY_JWT) {
			callAndStopOnFailure(CreateClientAuthenticationAssertionClaims.class);
			callAndStopOnFailure(SignClientAuthenticationAssertion.class);
			callAndStopOnFailure(AddClientAssertionToTokenEndpointRequest.class);
		} else {
			callAndStopOnFailure(AddClientIdToTokenEndpointRequest.class);
		}

		callAndStopOnFailure(CallTokenEndpoint.class);
		callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
		callAndStopOnFailure(CheckForAccessTokenValue.class);
		callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);
	}
}
