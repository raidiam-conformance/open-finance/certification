package net.openid.conformance.openbanking_brasil.testmodules.v3.consents;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentExpiryDateTimeGreaterThanAYear;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentExpiryDateTimeInThePast;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentExpiryDateTimePoorlyFormed;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFPermissionsEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostConsentWithBadRequestSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_negative_test-module_v3",
	displayName = "Runs various negative tests",
	summary = "Runs various negative tests\n" +
		"\u2022 Creates a Consent with only RESOURCES_READ permissions V3\n" +
		"\u2022 Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"\u2022 Creates consent with only customers identification permission group (\"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\") or (\"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\")\n" +
		"\u2022 Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"\u2022 Creates consent with incomplete credit card permission group (\"CREDIT_CARDS_ACCOUNTS_BILLS_READ\",\"CREDIT_CARDS_ACCOUNTS_READ\",\"RESOURCES_READ\")\n" +
		"\u2022 Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"\u2022 Creates consent with incomplete account permission group 1 (\"ACCOUNTS_READ\")\n" +
		"\u2022 Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"\u2022 Creates consent with incomplete account permission group 2 (\"ACCOUNTS_READ\",\"RESOURCES_READ\")\n" +
		"\u2022 Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"\u2022 Creates consent with incomplete Limits & Credit Operations Contract Data permissions group (\"RESOURCES_READ\", \"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"\u2022 Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"\u2022 Creates consent with non-existent permission group 2 (\"BAD_PERMISSION\")\n" +
		"\u2022 Expects failure with a 400 Error\n" +
		"\u2022 Creates consent api V3 request for DateTime greater than 1 year from now\n" +
		"\u2022 Expects failure with a 422 Error\n" +
		"\u2022 Creates consent api V3 request for DateTime in the past\n" +
		"\u2022 Expects failure with a 422 Error\n" +
		"\u2022 Creates consent api V3 request for DateTime poorly formed.\n" +
		"\u2022 Expects failure with a 400 Error",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ConsentApiNegativeTestsV3 extends AbstractClientCredentialsGrantFunctionalTestModule {

	ScopesAndPermissionsBuilder scopesAndPermissionsBuilder;

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		env.putString("metaOnlyRequestDateTime", "true");
		scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
	}

	@Override
	protected void runTests() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(GetConsentV3Endpoint.class));
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);

		validateBadPermissionCombination("Resource read only", OPFPermissionsEnum.RESOURCES_READ);

		String productType = env.getString("config", "consent.productType");
		if (productType.equals("personal")) {
			validateBadPermissionCombination("Customers identification permissions only", OPFPermissionsEnum.CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ);
		} else {
			validateBadPermissionCombination("Customers identification permissions only", OPFPermissionsEnum.CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ);
		}

		validateBadPermissionCombination("incomplete credit card permission group", OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_BILLS_READ, OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_READ, OPFPermissionsEnum.RESOURCES_READ);
		validateBadPermissionCombination("incomplete account permission group", OPFPermissionsEnum.ACCOUNTS_READ);
		validateBadPermissionCombination("less incomplete account permission group", OPFPermissionsEnum.ACCOUNTS_READ, OPFPermissionsEnum.RESOURCES_READ);
		validateBadPermissionCombination("incomplete combination of Limits & Credit Operations Contract Data permission groups", OPFPermissionsEnum.ACCOUNTS_OVERDRAFT_LIMITS_READ, OPFPermissionsEnum.RESOURCES_READ, OPFPermissionsEnum.LOANS_READ,
			OPFPermissionsEnum.LOANS_WARRANTIES_READ, OPFPermissionsEnum.LOANS_SCHEDULED_INSTALMENTS_READ, OPFPermissionsEnum.LOANS_PAYMENTS_READ, OPFPermissionsEnum.FINANCINGS_READ, OPFPermissionsEnum.FINANCINGS_WARRANTIES_READ, OPFPermissionsEnum.FINANCINGS_SCHEDULED_INSTALMENTS_READ,
			OPFPermissionsEnum.FINANCINGS_PAYMENTS_READ, OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_READ, OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ, OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ,
			OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_WARRANTIES_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_PAYMENTS_READ, OPFPermissionsEnum.RESOURCES_READ);
		validateNonExistentPermission();

		validateBadExpiration(ConsentExpiryDateTimeGreaterThanAYear.class, "DateTime greater than 1 year from now", EnsureConsentResponseCodeWas422.class);
		validateBadExpiration(ConsentExpiryDateTimeInThePast.class, "DateTime in the past", EnsureConsentResponseCodeWas422.class);
		validateBadExpiration(ConsentExpiryDateTimePoorlyFormed.class, "DateTime poorly formed", EnsureConsentResponseCodeWas400.class);
	}

	private void validateBadPermissionCombination(String description, OPFPermissionsEnum... permissions) {
		String logMessage = String.format("Check for HTTP 422 COMBINACAO_PERMISSOES_INCORRETA response from consent api request for %s", description);
		runInBlock(logMessage, () -> {
			callAndStopOnFailure(SetContentTypeApplicationJson.class);
			scopesAndPermissionsBuilder.reset();
			scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS);
			scopesAndPermissionsBuilder.addPermissions(permissions).build();
			env.mapKey(EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			call(sequenceOf(
				condition(PrepareToPostConsentRequest.class),
				condition(FAPIBrazilOpenBankingCreateConsentRequest.class),
				condition(FAPIBrazilAddExpirationToConsentRequest.class),
				condition(CreateRandomFAPIInteractionId.class),
				condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
				condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
					.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
				condition(EnsureConsentResponseCodeWas422.class),
				condition(PostConsentOASValidatorV3.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
				condition(EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta.class)
					.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)
			));
			env.unmapKey(EnsureErrorResponseCodeWasCombinacaoPermissoesIncorreta.RESPONSE_ENV_KEY);
		});
	}

	private void validateNonExistentPermission() {
		String logMessage = "Check for HTTP 400 response from consent api request for non-existent permission group";
		runInBlock(logMessage, () -> {
			callAndStopOnFailure(SetContentTypeApplicationJson.class);
			scopesAndPermissionsBuilder.reset();
			scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS);
			env.putString("consent_permissions", "BAD_PERMISSION");
			call(sequenceOf(
				condition(CreateRandomFAPIInteractionId.class),
				condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
				sequence(PostConsentWithBadRequestSequence.class),
				condition(PostConsentOASValidatorV3.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)
			));
		});
	}

	private void validateBadExpiration(Class<? extends Condition> setupClass, String description,Class<? extends Condition> errorCodeCondition) {
		String logMessage = String.format("Check for HTTP 400 response from consent api request for %s.", description);
		runInBlock(logMessage, () -> {
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_OPERATIONS).build();
			callAndStopOnFailure(setupClass);
			callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
			callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(errorCodeCondition);
			callAndContinueOnFailure(PostConsentOASValidatorV3.class, Condition.ConditionResult.FAILURE);
		});
	}

}
