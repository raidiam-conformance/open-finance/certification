package net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetExchangesV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.v3.AbstractCustomerDataXFapiTestModuleV3;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "exchange_api_x-fapi_test-module",
	displayName = "exchange_api_x-fapi_test-module",
	summary = "Test will ensure that the x-fapi-interaction-id is required at the request\n" +
		"\u2022 Call the POST Consents endpoint with the Exchange API Permission Group\n" +
		"\u2022 Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION\n" +
		"\u2022 Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"\u2022 Call the GET Consents endpoint\n" +
		"\u2022 Expects 200 - Validate response and ensure status is AUTHORISED\n" +
		"\u2022 Call the GET Operations Endpoint without the x-fapi-interaction-id\n" +
		"\u2022 Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"\u2022 Call the GET Operations Endpoint with an invalid x-fapi-interaction-id\n" +
		"\u2022 Expects 400 - Validate error message\n" +
		"\u2022 Call the GET Operations Endpoint with the x-fapi-interaction-id\n" +
		"\u2022 Expects 200 - Validate response\n",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class ExchangeApiXFapiTestModule extends AbstractCustomerDataXFapiTestModuleV3 {
	@Override
	protected boolean isConsents() {
		return false;
	}

	@Override
	protected Class<? extends Condition> getResourceValidator() {
		return GetExchangesOperationsListV1OASValidator.class;
	}
	@Override
	protected OPFCategoryEnum getCategory() {
		return OPFCategoryEnum.EXCHANGES;
	}
	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.EXCHANGES;
	}
	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetExchangesV1Endpoint.class)
		);
	}
}
