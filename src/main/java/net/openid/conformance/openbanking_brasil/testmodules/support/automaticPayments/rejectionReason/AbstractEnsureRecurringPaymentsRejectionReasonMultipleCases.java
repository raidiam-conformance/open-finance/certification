package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.v2.RecurringPaymentsRejectionReasonEnumV2;

import java.util.List;

public abstract class AbstractEnsureRecurringPaymentsRejectionReasonMultipleCases extends AbstractEnsureRecurringPaymentsRejectionReason {

	@Override
	protected boolean checkIfRejectionReasonMatches(String rejectionReasonCode) {
		RecurringPaymentsRejectionReasonEnumV2 rejectionReason = RecurringPaymentsRejectionReasonEnumV2.valueOf(rejectionReasonCode);
		return expectedRejectionReasons().contains(rejectionReason);
	}

	@Override
	protected RecurringPaymentsRejectionReasonEnumV2 expectedRejectionReason() {
		return null;
	}

	protected abstract List<RecurringPaymentsRejectionReasonEnumV2> expectedRejectionReasons();
}
