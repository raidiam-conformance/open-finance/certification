package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class CheckEnrollmentNameConsistency extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";
	private static final String ENROLLMENT_NAME_KEY = "enrollment_name";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY, strings = ENROLLMENT_NAME_KEY)
	public Environment evaluate(Environment env) {
		String expectedEnrollmentName = env.getString(ENROLLMENT_NAME_KEY);
		JsonObject body = extractBodyFromEnv(env);
		String enrollmentName = Optional.ofNullable(body.getAsJsonObject("data"))
			.map(data -> data.get("enrollmentName"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not extract data.enrollmentName from response body", args("response body", body)));

		if (enrollmentName.equals(expectedEnrollmentName)) {
			logSuccess("\"enrollmentName\" matches the expected value");
			return env;
		}

		throw error("\"enrollmentName\" does not match the expected value",
			args("expected", expectedEnrollmentName, "received", enrollmentName));
	}

	private JsonObject extractBodyFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse body");
		}
	}
}
