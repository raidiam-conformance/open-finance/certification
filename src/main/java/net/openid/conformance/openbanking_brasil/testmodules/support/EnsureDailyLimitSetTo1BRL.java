package net.openid.conformance.openbanking_brasil.testmodules.support;

public class EnsureDailyLimitSetTo1BRL extends AbstractEnsureEnrollmentVariableSetToValue {
	@Override
	protected String fieldName() {
		return "dailyLimit";
	}

	@Override
	protected String envVarName() {
		return "daily_limit";
	}

	@Override
	protected String value() {
		return "1.00";
	}
}
