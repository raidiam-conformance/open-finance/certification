package net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums;

public enum OPFScopesEnum implements ScopesEnum {

	OPEN_ID("openid"),
	CONSENTS("consents"),
	CUSTOMERS("customers"),
	RESOURCES("resources"),
	ACCOUNTS("accounts"),
	CREDIT_CARDS_ACCOUNTS("credit-cards-accounts"),
	LOANS("loans"),
	FINANCINGS("financings"),
	UNARRANGED_ACCOUNTS_OVERDRAFT("unarranged-accounts-overdraft"),
	INVOICE_FINANCINGS("invoice-financings"),
	BANK_FIXED_INCOMES("bank-fixed-incomes"),
	CREDIT_FIXED_INCOMES("credit-fixed-incomes"),
	VARIABLE_INCOMES("variable-incomes"),
	TREASURE_TITLES("treasure-titles"),
	FUNDS("funds"),
	EXCHANGES("exchanges"),
	PAYMENTS("payments"),
	RECURRING_PAYMENTS("recurring-payments"),
	ENROLLMENTS("enrollments"),
	NRP_CONSENTS("nrp-consents");


	private final String scope;

	OPFScopesEnum(String scope) {
		this.scope = scope;
	}

	@Override
	public String getScope() {
		return scope;
	}
}

