package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;

public class CreateAutomaticRecurringConfigurationObjectSemanalVariableAmount extends AbstractCreateAutomaticRecurringConfigurationObject {

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.SEMANAL;
	}

	@Override
	protected String getFixedAmount() {
		return null;
	}

	@Override
	protected String getMinimumVariableAmount() {
		return "0.50";
	}

	@Override
	protected String getMaximumVariableAmount() {
		return "0.80";
	}
}
