package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiAccountTypeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_account-type_test-module_v4",
	displayName = "Ensure accountType SLRY com no longer be requested, and authorisationFlow can be sent as HYBRID_FLOW",
	summary = "Ensure accountType SLRY com no longer be requested, and authorisationFlow can be sent as HYBRID_FLOW\n" +
		"• Call the POST Consent with accountType as SLRY on debtorAccount \n" +
		"• Expects 422 PARAMETRO_INVALIDO  - Validate error message\n" +
		"• Call the POST Consent with accountType as CACC/SVGS/TRAN on debtorAccount, depending on what was provided on the test configuration\n" +
		"• Expets 201 - Validate Response \n" +
		"• Redirects the user to authorize the created consent \n" +
		"• Call GET Consent \n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"  \n" +
		"• Calls the POST Payments with authorisationFlow as HYBRID_FLOW \n" +
		"• Expects 201 - Validate response \n" +
		"• Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"• Expects Definitive state (ACSC) - Validate Response\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiAccountTypeTestModuleV4 extends AbstractPaymentsApiAccountTypeTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}


}
