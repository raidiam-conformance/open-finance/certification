package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LoadPaymentsRequestBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = {"resource", "savedBrazilPixPayment"})
	public Environment evaluate(Environment env) {
		JsonObject savedPaymentsBody = env.getObject("savedBrazilPixPayment");
		env.putObject("resource", "brazilPixPayment",
			savedPaymentsBody.deepCopy());
		logSuccess("Payments request body has been loaded");
		return env;
	}
}
