package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public abstract class AbstractExtractLastPaymentIds extends AbstractCondition {

	protected abstract int amountOfPayments();

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	@PostEnvironment(required = "payment_ids")
	public Environment evaluate(Environment env) {
		JsonObject body;
		try {
			body = BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
				.orElseThrow(() -> error("Could not extract body from response"))
				.getAsJsonObject();
		} catch (ParseException e) {
			throw error("Could not parse the response body");
		}

		JsonArray data = body.getAsJsonArray("data");
		int dataArraySize = data.size();
		int expectedAmountOfPayments = amountOfPayments();
		if (dataArraySize < expectedAmountOfPayments) {
			throw error("The response payload is expected to have at least " + expectedAmountOfPayments + " payments in it",
				args("amount of payments", dataArraySize));
		}

		JsonArray paymentIds = new JsonArray();
		for (int i = dataArraySize - expectedAmountOfPayments; i < dataArraySize; i++) {
			JsonObject payment = data.get(i).getAsJsonObject();
			String paymentId = OIDFJSON.getString(
				Optional.ofNullable(payment.get("paymentId"))
					.orElseThrow(() -> error("Could not extract payment ID from payment"))
			);
			paymentIds.add(paymentId);
		}

		JsonObject paymentsObject = new JsonObject();
		paymentsObject.add("paymentIds", paymentIds);

		env.putObject("payment_ids", paymentsObject);
		logSuccess("Extracted payment IDs", args("payment_ids", paymentIds));

		return env;
	}
}
