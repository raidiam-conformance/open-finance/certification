package net.openid.conformance.openbanking_brasil.testmodules.opendata.pension.V2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.opendata.utils.PrepareToGetOpenDataApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.pension.v2.GetRiskCoveragesOASValidatorV2;
import net.openid.conformance.openinsurance.testplan.utils.CallNoCacheResource;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "opendata-pension-risk-coverages_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.PENSION_RISK_COVERAGES_API_PHASE_4A_TEST_PLAN_V2,
	summary = PlanNames.PENSION_RISK_COVERAGES_API_PHASE_4A_TEST_PLAN_V2
)
public class RiskCoveragesTestPlanV2 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(net.openid.conformance.openbanking_brasil.testmodules.opendata.pension.V2.RiskCoveragesTestPlanV2.RiskCoveragesTestModuleV2.class),
				List.of(new Variant(ClientAuthType.class, "none"))
			)
		);
	}

	@PublishTestModule(
		testName = "opendata-pension-risk-coverages_api_structural_test-module_v2",
		displayName = "Validate structure of Pension Risk Coverages response",
		summary = "Validate structure of Pension Risk Coverages response",
		profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4
	)
	public static class RiskCoveragesTestModuleV2 extends AbstractNoAuthFunctionalTestModule {

		@Override
		protected void runTests() {
			runInBlock("Validate Pension Risk Coverages response", () -> {
				callAndStopOnFailure(PrepareToGetOpenDataApi.class);
				callAndStopOnFailure(CallNoCacheResource.class);
				callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(GetRiskCoveragesOASValidatorV2.class, Condition.ConditionResult.FAILURE);
			});
		}
	}
}

