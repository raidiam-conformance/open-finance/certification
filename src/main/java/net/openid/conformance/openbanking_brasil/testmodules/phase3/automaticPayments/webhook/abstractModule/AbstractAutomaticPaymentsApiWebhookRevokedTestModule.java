package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.abstractModule;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.AbstractAutomaticPaymentsDCRWebhookTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRejectionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRevocationRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToRemoveExpirationDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revocationReason.EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedBy.EnsureRecurringPaymentsConsentsRevokedByUsuario;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedFrom.EnsureRecurringPaymentsConsentsRevokedFromIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooksOptionalAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasRevoked;
import net.openid.conformance.testmodule.OIDFJSON;

public abstract class AbstractAutomaticPaymentsApiWebhookRevokedTestModule extends AbstractAutomaticPaymentsDCRWebhookTestModule {

	protected abstract Class<? extends Condition> patchPaymentConsentValidator();

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected void performPostAuthorizationFlow() {
		runInBlock("Call PATCH consents - Expects 200", this::makePatchConsentsRequest);
		fetchConsentToCheckStatus("REVOKED", new EnsurePaymentConsentStatusWasRevoked());
		validateRevokedConsent();
		env.unmapKey("access_token");
		validateGetConsentResponse();
		eventLog.startBlock(currentClientString() + "Call token endpoint");
		waitForWebhookResponse();
		validatePaymentConsentWebhooks();
		fireTestFinished();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToRemoveExpirationDateTime.class);
	}

	protected void validateRevokedConsent() {
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRevokedByUsuario.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRevokedFromIniciadora.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoUsuario.class, Condition.ConditionResult.FAILURE);
	}

	protected void makePatchConsentsRequest() {
		Class<? extends Condition> consentRevocationCondition = isNewerVersion() ?
			CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody.class :
			CreatePatchRecurringPaymentsConsentsForRevocationRequestBody.class;

		call(new CallPatchAutomaticPaymentsConsentsEndpointSequence()
			.replace(CreatePatchRecurringPaymentsConsentsForRejectionRequestBody.class,
				condition(consentRevocationCondition)));
		callAndContinueOnFailure(patchPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void validatePaymentConsentWebhooks(){
		JsonArray webhooks = env.getObject("webhooks_received_recurring_consent").get("webhooks").getAsJsonArray();
		int numberOfWebhooks = 0;
		for (JsonElement webhook : webhooks) {
			String type = OIDFJSON.getString(webhook.getAsJsonObject().get("type"));
			if (type.equals("recurring-consent")) {
				numberOfWebhooks++;
			}
		}

		env.putBoolean("recurring_payment_consent_webhook_received", false);
		if (numberOfWebhooks > 1) {
			callAndStopOnFailure(ValidateRecurringConsentWebhooksOptionalAmount.class);
		} else {
			callAndStopOnFailure(ValidateRecurringConsentWebhooks.class);
		}
	}

	@Override
	protected Class<? extends Condition> setPaymentWebhookCreator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> setGetPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> setPostPaymentValidator() {
		return null;
	}
}
