package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.investments.treasureTitles.v1;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api url: https://openbanking-brasil.github.io/draft-openapi/swagger-apis/investments/1.0.1.yml
 * Api endpoint: /treasure-titles
 * Api version: 1.0.1
 */

@ApiName("Investments Treasure Titles V1")
public class GetInvestmentsTreasureTitlesOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-investments-1.0.1.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/treasure-titles";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
