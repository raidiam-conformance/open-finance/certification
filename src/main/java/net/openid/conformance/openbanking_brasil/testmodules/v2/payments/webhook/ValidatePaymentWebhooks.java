package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

public class ValidatePaymentWebhooks extends AbstractValidateWebhooksReceived{
	@Override
	protected int expectedWebhooksReceivedAmount() {
		return 1;
	}

	@Override
	protected String webhookType() {
		return "payment";
	}
}
