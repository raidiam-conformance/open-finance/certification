package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckForUnexpectedParametersInErrorResponseFromAuthorizationEndpoint;
import net.openid.conformance.condition.client.CheckMatchingCallbackParameters;
import net.openid.conformance.condition.client.CheckStateInAuthorizationResponse;
import net.openid.conformance.condition.client.RejectStateInUrlQueryForHybridFlow;
import net.openid.conformance.condition.client.ValidateIssIfPresentInAuthorizationResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRESCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectQRCodeWithWrongAmountIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRESCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetPaymentAmountToKnownValueOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetPaymentAmountToKnownValueOnPaymentInitiation;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasValorInvalidoV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;

public abstract class AbstractPaymentsApiQRESWrongAmountProxyTestModule extends AbstractPaymentConsentUnhappyPathTestModule {

	@Override
	protected void onAuthorizationCallbackResponse(){
		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);

		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");

		callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);

		callAndContinueOnFailure(CheckForUnexpectedParametersInErrorResponseFromAuthorizationEndpoint.class, Condition.ConditionResult.WARNING, "OIDCC-3.1.2.6");

		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");

		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OAuth2-iss-2");

		fetchConsentToCheckStatus("REJECTED",new EnsureConsentStatusWasRejected());
		callAndContinueOnFailure(EnsureConsentsRejectionReasonCodeWasValorInvalidoV3.class, Condition.ConditionResult.FAILURE);

		fireTestFinished();
	}

    @Override
    protected void configureDictInfo() {
        callAndStopOnFailure(SelectQRESCodeLocalInstrument.class);
        callAndStopOnFailure(SelectQRESCodePixLocalInstrument.class);
        callAndStopOnFailure(SetPaymentAmountToKnownValueOnConsent.class);
        callAndStopOnFailure(SetPaymentAmountToKnownValueOnPaymentInitiation.class);
        callAndStopOnFailure(InjectQRCodeWithWrongAmountIntoConfig.class);
    }

    @Override
    protected void validatePaymentRejectionReasonCode() {
		// Test won't be reaching payment initiation.
    }

    @Override
    protected void validate422ErrorResponseCode() {
		// Test won't be reaching payment initiation.
    }

    @Override
    protected Class<EnsureResourceResponseCodeWas201Or422> getExpectedPaymentResponseCode() {
		// Test won't be reaching payment initiation.
        return null;
    }


}
