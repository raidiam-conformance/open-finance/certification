package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.pixqrcode.PixQRCode;
import net.openid.conformance.testmodule.Environment;


public class InjectQRCodeWithTransactionIdentifierIntoConfig extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {

		//Build the QRes
		PixQRCode qrCode = new PixQRCode();
		qrCode.useStandardConfig();

		String amount = env.getString("resource", "brazilPaymentConsent.data.payment.amount");

		qrCode.setTransactionAmount(amount);
		qrCode.setAdditionalField("03***");

		JsonObject consentPaymentDetails = env.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details").getAsJsonObject();
		JsonElement paymentInitiation = env.getElementFromObject("resource", "brazilPixPayment.data");
		consentPaymentDetails.addProperty("qrCode", qrCode.toString());

		if (paymentInitiation.isJsonArray()) {
			for (int i = 0; i < paymentInitiation.getAsJsonArray().size(); i++) {
				JsonObject payment = paymentInitiation.getAsJsonArray().get(i).getAsJsonObject();
				payment.addProperty("qrCode", qrCode.toString());
			}
			logSuccess(String.format("Added new QRes to payment consent and payment initiation with amount %s BRL", amount), args("QRes", qrCode.toString()));
			return env;
		}


		paymentInitiation.getAsJsonObject().addProperty("qrCode", qrCode.toString());

		logSuccess(String.format("Added new QRes to payment consent and payment initiation with amount %s BRL", amount), args("QRes", qrCode.toString()));
		return env;
	}
}
