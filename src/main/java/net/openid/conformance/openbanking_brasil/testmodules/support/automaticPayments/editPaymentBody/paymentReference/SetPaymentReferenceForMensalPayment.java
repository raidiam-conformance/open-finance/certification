package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

public class SetPaymentReferenceForMensalPayment extends AbstractSetPaymentReference {

	@Override
	protected String getPaymentReference(String dateStr) {
		LocalDate date = getPaymentDate(dateStr);
		int month = date.getMonthValue();
		int year = date.get(ChronoField.YEAR);

		return String.format("M%02d-%d", month, year);
	}
}
