package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.NextCallExpectedToFail;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs.EnsureScheduledDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPatchPixPaymentsEndpointSequenceV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PaymentConsentErrorTestingSequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;

import java.util.Objects;

public abstract class AbstractPaymentsApiInvalidTokenTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

    protected abstract Class<? extends Condition> patchPaymentPixValidator();

    private final String SKIP_MESSAGE = "Not expecting a jwt response";

    @Override
    protected void updatePaymentConsent() {}

    @Override
    protected Class<? extends Condition> getPaymentPollStatusCondition() {
        return CheckPaymentPollStatusRcvdOrAccpV2.class;
    }

    @Override
    protected void validateFinalState() {
        callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void configureDictInfo() {
        callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
        callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
        callAndStopOnFailure(EnsureProxyPresentInConfig.class);
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        callAndStopOnFailure(EnsureScheduledDateIsTomorrow.class);
        callAndStopOnFailure(RemovePaymentConsentDate.class);
        super.onConfigure(config, baseUrl);
    }

    @Override
    protected void requestProtectedResource() {
        if (!validationStarted) {
            validationStarted = true;
            runInBlock("POST payments endpoint using client_credentials token - expects 401", () -> {
                env.mapKey("old_access_token", "authorization_code_access_token");
                callAndStopOnFailure(SaveAccessToken.class);
                eventLog.log("Saved the authorization_code access token", env.getObject("authorization_code_access_token"));
                env.unmapKey("old_access_token");
                callAndStopOnFailure(LoadOldAccessToken.class);
                callAndStopOnFailure(SaveAccessToken.class);
                eventLog.log("Loaded the client_credentials access token", env.getObject("access_token"));

                ConditionSequence pixFailSequence = getPixPaymentSequence()
                    .insertBefore(CallProtectedResource.class, condition(NextCallExpectedToFail.class))
                    .replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas401.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .replace(EnsureContentTypeApplicationJwt.class, condition(EnsureContentTypeJson.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .skip(ExtractSignedJwtFromResourceResponse.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseSigningAlg.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseTyp.class, SKIP_MESSAGE)
                    .skip(FetchServerKeys.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseSignature.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseJwtClaims.class, SKIP_MESSAGE);
                call(pixFailSequence);
                callAndContinueOnFailure(postPaymentValidator(), Condition.ConditionResult.FAILURE);
            });

            runInBlock("POST payments endpoint using authorization_code token - expects 201", () -> {
                loadAuthorizationCode();
                ConditionSequence pixSequence = getPixPaymentSequence();
                call(pixSequence);
                callAndStopOnFailure(SaveOldValues.class);
                validateResponse();
            });

            runInBlock("GET payments endpoint using authorization_code token - expects 401", () -> {
                loadAuthorizationCode();
                ConditionSequence getPixSequence = new CallGetPaymentEndpointSequence()
                    .insertBefore(CallProtectedResource.class, condition(NextCallExpectedToFail.class))
                    .replace(EnsureResourceResponseCodeWas200.class, condition(EnsureResourceResponseCodeWas401.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .replace(EnsureContentTypeApplicationJwt.class, condition(EnsureContentTypeJson.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .skip(ExtractSignedJwtFromResourceResponse.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseSigningAlg.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseTyp.class, SKIP_MESSAGE)
                    .skip(FetchServerKeys.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseSignature.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseJwtClaims.class, SKIP_MESSAGE)
                    .skip(PaymentInitiationPixPaymentsValidatorV2.class, "Not a 200 response");
                call(getPixSequence);
                callAndContinueOnFailure(getPaymentValidator(), Condition.ConditionResult.FAILURE);
            });

            runInBlock("PATCH payments endpoint using authorization_code token - expects 401", () -> {
                loadAuthorizationCode();
                callAndStopOnFailure(CreatePatchPaymentsRequestFromConsentRequestV2.class);
                callAndStopOnFailure(LoadOldValues.class);
                ConditionSequence patchPixSequence = new CallPatchPixPaymentsEndpointSequenceV2()
                    .insertBefore(CallProtectedResource.class, condition(NextCallExpectedToFail.class))
                    .replace(EnsureResourceResponseCodeWas200.class, condition(EnsureResourceResponseCodeWas401.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .replace(EnsureContentTypeApplicationJwt.class, condition(EnsureContentTypeJson.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
                    .skip(ExtractSignedJwtFromResourceResponse.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseSigningAlg.class, SKIP_MESSAGE)
                    .skip(FAPIBrazilValidateResourceResponseTyp.class, SKIP_MESSAGE)
                    .skip(FetchServerKeys.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseSignature.class, SKIP_MESSAGE)
                    .skip(ValidateResourceResponseJwtClaims.class, SKIP_MESSAGE);
                call(patchPixSequence);
                callAndContinueOnFailure(patchPaymentPixValidator(), Condition.ConditionResult.FAILURE);
            });

            runInBlock("POST consents endpoint using authorization_code token - expects 401", () -> {
                loadAuthorizationCode();
                callAndStopOnFailure(PrepareToPostConsentRequest.class);
                callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);
                ConditionSequence postPixConsentSequence = new PaymentConsentErrorTestingSequence()
                    .replace(EnsureContentTypeApplicationJwt.class, condition(EnsureContentTypeJson.class)
                        .dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE));
                call(postPixConsentSequence);
                env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
                callAndContinueOnFailure(EnsureResourceResponseCodeWas401.class, Condition.ConditionResult.FAILURE);
                callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
            });
        }
    }

    @Override
    protected void forceGetCallToUseClientCredentialsToken(ConditionCallBuilder builder) {}

    @Override
    protected void validateSelfLink(String responseFull) {
        if (Objects.equals(responseFull, "resource_endpoint_response_full") && env.containsObject("old_access_token")) {
            eventLog.log("Loaded client_credential access token", env.getObject("old_access_token"));
            callAndStopOnFailure(LoadOldAccessToken.class);
            callAndStopOnFailure(SaveAccessToken.class);
        }
        super.validateSelfLink(responseFull);
    }

    protected void loadAuthorizationCode() {
        env.mapKey("old_access_token", "authorization_code_access_token");
        callAndStopOnFailure(LoadOldAccessToken.class);
        callAndStopOnFailure(SaveAccessToken.class);
        eventLog.log("Loaded the authorization_code access token", env.getObject("access_token"));
        env.unmapKey("old_access_token");
    }
}

