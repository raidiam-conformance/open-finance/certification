package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class EnsureSoftwareClientNameMatchesSSA extends AbstractCondition {


	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	//ensure the value of data.rp.name matches the field software_client_name at the SSA
	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY, strings = "software_client_name")
	public Environment evaluate(Environment env) {
		JsonObject body;
		try {
			body = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from jwt response"))
				.getAsJsonObject();
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}


		JsonElement rpName = Optional.ofNullable(body.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("rp"))
			.map(rp -> rp.get("name"))
			.orElseThrow(() -> error("Could not extract the name from the RP in the response"));

		// get the client name from the configuration
		String expected = env.getString("software_client_name");
		String actual = OIDFJSON.getString(rpName);

		if (!Strings.isNullOrEmpty(expected) && expected.equalsIgnoreCase(actual)) {
			logSuccess("Software client name matches, ignoring case", args("software_client_name", Strings.nullToEmpty(actual)));
			return env;
		} else {
			throw error("Mismatch between software client name and RP name", args("expected", Strings.nullToEmpty(expected), "actual", Strings.nullToEmpty(actual)));
		}

	}

}
