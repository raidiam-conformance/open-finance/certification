package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.WaitFor15Minutes;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentCancelledFromWas.EnsureEnrollmentCancelledFromWasDetentora;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas.EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoAccountHolderValidation;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRejected;

public abstract class AbstractEnrollmentsApiExpiredPreAuthorisationEnrollmentTestModule extends AbstractEnrollmentsApiTestModule {

	@Override
	protected void performAuthorizationFlow(){
		performPreAuthorizationSteps();
		executeTestSteps();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected void executeTestSteps() {
		callAndStopOnFailure(WaitFor15Minutes.class);
		createNewClientCredentialsToken();
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRejected(), "REJECTED");
		runInBlock("Validate enrollments fields related to cancellation", () -> {
			callAndContinueOnFailure(EnsureEnrollmentCancelledFromWasDetentora.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoAccountHolderValidation.class, Condition.ConditionResult.FAILURE);
		});
	}

}
