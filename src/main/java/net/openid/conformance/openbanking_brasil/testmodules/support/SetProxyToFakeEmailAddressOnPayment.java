package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public class SetProxyToFakeEmailAddressOnPayment extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonElement paymentData = env.getElementFromObject("resource", "brazilPixPayment.data");

		if (paymentData.isJsonArray()) {
			for (int i = 0; i < paymentData.getAsJsonArray().size(); i++) {
				JsonObject payment = paymentData.getAsJsonArray().get(i).getAsJsonObject();
				payment.addProperty("proxy", "fakeperson@example.com");
			}
			logSuccess("Added non-existent email address as proxy to payment");
			return env;
		}

		paymentData.getAsJsonObject().addProperty("proxy", "fakeperson@example.com");
		logSuccess("Added non-existent email address as proxy to payment");

		return env;
	}

}
