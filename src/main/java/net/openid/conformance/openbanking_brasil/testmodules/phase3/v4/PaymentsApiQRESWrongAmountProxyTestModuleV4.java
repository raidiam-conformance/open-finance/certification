package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiQRESWrongAmountProxyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringPixOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_qres-wrong-amount-proxy_test-module_v4",
	displayName = "Payments Consents API test module for QRES local instrument with divergent amount",
	summary = "Ensure that consent fails when the amount sent on the Static QR Code differs from the payload (Reference Error 4.1.3)\n" +
		"• Call POST Consent with different amount values on QRES and the payload\n" +
		"• Expects 422 - DETALHE_PAGAMENTO_INVALIDO\n" +
		"• Validate Error message\n" +
		"If no errors are identified:\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user\n" +
		"• Ensure an error is returned and that the authorization code was not sent back\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"REJECTED\" and rejectionReason is  VALOR_INVALIDO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiQRESWrongAmountProxyTestModuleV4 extends AbstractPaymentsApiQRESWrongAmountProxyTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

		@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

}
