package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessFinancialRelations;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyBusinessProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.OperationalLimitsToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetBusinessIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.AbstractOperationalLimitsTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractCustomerBusinessApiOperationalLimitsTestModule extends AbstractOperationalLimitsTestModule {

	protected abstract Class<? extends AbstractJsonAssertingCondition> getBusinessQualificationResponseValidator();
	protected abstract Class<? extends AbstractJsonAssertingCondition> getBusinessIdentificationResponseValidator();
	protected abstract Class<? extends AbstractJsonAssertingCondition> getBusinessRelationsResponseValidator();

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetBusinessIdentificationsV2Endpoint.class)
		)));
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder
			.addPermissionsBasedOnCategory(OPFCategoryEnum.BUSINESS_REGISTRATION_DATA)
			.addScopes(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.OPEN_ID)
			.build();

		callAndStopOnFailure(AddDummyBusinessProductTypeToConfig.class);
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class, Condition.ConditionResult.FAILURE);
	}


	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(isSecondClient(), false, addTokenEndpointClientAuthentication, brazilPayments.isTrue(), false, false).
			insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class)));
	}

	@Override
	protected void validateResponse() {
		// Validate Business Identification response
		callAndStopOnFailure(getBusinessIdentificationResponseValidator());

		eventLog.endBlock();

		// Call Business Identification 3 times
		disableLogging();
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Business Identification Endpoint", i + 1));
		}

		makeOverOlCall("Identification");

		// Call Business Qualifications once with validation
		runInLoggingBlock(() -> {
			callAndStopOnFailure(PrepareToGetBusinessQualifications.class);

			preCallProtectedResource("Calling Business Qualifications Endpoint");
			validateResponse("Validate Business Qualifications response", getBusinessQualificationResponseValidator());
		});

		// Call Business Qualifications 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Business Qualifications Endpoint", i + 1));
		}

		makeOverOlCall("Qualifications");



		// Call Customer Business Financial Relations once with validation
		runInLoggingBlock(() -> {
			callAndStopOnFailure(PrepareToGetBusinessFinancialRelations.class);

			preCallProtectedResource("Calling Customer Business Financial Relations Endpoint");
			validateResponse("Validate Customer Business Financial Relations response", getBusinessRelationsResponseValidator());

		});

		// Call Customer Business Financial Relations 3 times
		for (int i = 1; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Calling Customer Business Financial Relations Endpoint", i + 1));
		}

		makeOverOlCall("Fin Relations");


	}

	private void validateResponse(String message, Class<? extends Condition> validator) {
		runInBlock(message, () -> {
			callAndStopOnFailure(validator);
		});
	}

}
