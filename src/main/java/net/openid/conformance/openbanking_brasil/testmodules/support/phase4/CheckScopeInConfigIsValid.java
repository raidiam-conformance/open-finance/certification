package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CheckScopeInConfigIsValid extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {

		JsonElement scopeElement = env.getElementFromObject("config", "client.scope");

		if (scopeElement != null && scopeElement.toString().contains("payments")) {
			env.putString("config", "client.scope", "");
			log("Test initiated with incorrect scope in config - Resetting");
		}
		return env;
	}
}
