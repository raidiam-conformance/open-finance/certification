package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class CleanBrazilIds extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate (Environment env) {

		Optional.ofNullable(env.getString("config", "resource.brazilCpf")).ifPresent(brazilCpf -> {
			brazilCpf = cleanId(brazilCpf, "CPF");
			validateId(brazilCpf, "CPF", "^\\d{11}$", "11 digits");
			env.putString("config", "resource.brazilCpf", brazilCpf);
			logSuccess("brazilCpf is valid.", args("brazilCpf", brazilCpf));
		});

		Optional.ofNullable(env.getString("config", "resource.brazilCnpj")).ifPresent(brazilCnpj -> {
			brazilCnpj = cleanId(brazilCnpj, "CNPJ");
			validateId(brazilCnpj, "CNPJ", "^\\d{14}$", "14 digits");
			env.putString("config", "resource.brazilCnpj", brazilCnpj);
			logSuccess("brazilCnpj is valid.", args("brazilCnpj", brazilCnpj));
		});

		return env;
	}

	private String cleanId(String id, String idType) {
		// Remove allowed special characters.
		String cleanedId = id.replaceAll("[./-]", "");

		// Check for any other special characters
		if (!cleanedId.matches("^[0-9]+$")) {
			throw error("The " + idType + " field has special characters different from '.' or '-', meaning the test module cannot continue.");
		}

		return cleanedId;
	}

	private void validateId(String id, String idType, String regex, String requiredDigits) {
		if (!id.matches(regex)) {
			throw error("The " + idType + " field is not in a compliant format (should contain " + requiredDigits + "), meaning the test module cannot continue.");
		}
	}
}
