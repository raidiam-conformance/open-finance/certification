package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilSignPaymentConsentRequest;
import net.openid.conformance.condition.client.InvalidateConsentEndpointRequestSignature;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilPaymentConsentProdValues;

import java.util.Random;

public abstract class AbstractFvpPaymentsConsentsApiForceCheckBadSignatureTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> paymentConsentValidator();
	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilPaymentConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		int numOfGoodTests = new Random().nextInt(20) + 1;

		eventLog.startBlock("Making a random number of good payment consent requests");
		for(int i = 0; i < numOfGoodTests; i++) {
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);
			call(getPaymentsConsentSequence());
			if(i==0){
				callAndContinueOnFailure(paymentConsentValidator(), Condition.ConditionResult.FAILURE);
			}
		}

		eventLog.startBlock("Trying a badly signed payment consent request");
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);
		call(getPaymentsConsentSequence()
			.replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas400.class))
			.insertAfter(FAPIBrazilSignPaymentConsentRequest.class, condition(InvalidateConsentEndpointRequestSignature.class)));
		callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void configureDummyData() {
		super.configureDummyData();
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndContinueOnFailure(SanitiseQrCodeConfig.class, Condition.ConditionResult.FAILURE);
	}
}
