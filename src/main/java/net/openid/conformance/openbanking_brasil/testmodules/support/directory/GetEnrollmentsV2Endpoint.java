package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetEnrollmentsV2Endpoint extends AbstractGetEnrollmentsFromAuthServer  {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/enrollments\\/v\\d+\\/enrollments)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "enrollments";
	}

	@Override
	protected String getApiVersionRegex() {
		return  "^(2.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
