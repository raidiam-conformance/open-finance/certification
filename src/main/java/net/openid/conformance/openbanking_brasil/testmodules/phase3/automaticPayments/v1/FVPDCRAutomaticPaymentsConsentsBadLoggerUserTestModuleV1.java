package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsConsentsBadLoggerUserTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringConsentOASValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp_automatic_payments_consents_api_bad-logged_test-module_v1",
	displayName = "FAPI1-Advanced-Final: Brazil DCR happy flow without authentication flow",
	summary = "This test will try to use the recently created DCR to access a Automatic Payments V1.\n" +
		"\u2022 Call with a dummy, but well-formatted payload to make sure that the server will read the request but won’t be able to process it.\n" +
		"\u2022 Validate that the server contains the automatic-payments-consents v1 endpoint published in the directory\n" +
		"\u2022 Create a client by performing a DCR against the provided server - Expect Success\n" +
		"\u2022 Generate a token with the client_id created using client_credentials grant with recurring-payments\n" +
		"\u2022 Call the POST recurring-consents endpoint with sweeping accounts data, filling all fields for periodic limits\n"+
		"\u2022 Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"\u2022 Call the GET recurring-consents endpoint\n"+
		"\u2022 Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.authorisationServerId",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"directory.client_id",
		"directory.apibase",
		"resource.brazilOrganizationId"
	}
)
public class FVPDCRAutomaticPaymentsConsentsBadLoggerUserTestModuleV1 extends AbstractAutomaticPaymentsConsentsBadLoggerUserTestModule {

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() { return PostRecurringConsentOASValidatorV1.class; }

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetAutomaticPaymentRecurringConsentV1Endpoint.class;
	}
}
