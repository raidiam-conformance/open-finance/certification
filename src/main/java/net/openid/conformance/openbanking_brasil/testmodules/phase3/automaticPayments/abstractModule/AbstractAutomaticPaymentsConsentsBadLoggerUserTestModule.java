package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractRecurringConsentIdFromConsentEndpointResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetRecurringPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilAutomaticPaymentRecurringConsentProdValues;



public abstract class AbstractAutomaticPaymentsConsentsBadLoggerUserTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> getPaymentConsentValidator();
	protected abstract Class<? extends Condition> postPaymentConsentValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetRecurringPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilAutomaticPaymentRecurringConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		eventLog.startBlock("Calling POST recurring-consent endpoint");
		call(getPaymentsConsentSequence());
		eventLog.endBlock();

		eventLog.startBlock("Validate response - Expecting status AWAITING_AUTHORISATION");
		validatePostConsentResponse();
		callAndStopOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(ExtractRecurringConsentIdFromConsentEndpointResponse.class);
		eventLog.endBlock();

		eventLog.startBlock("Calling GET recurring-consent endpoint - Validate response, expecting status AWAITING_AUTHORISATION");
		fetchConsentToCheckStatus(new EnsurePaymentConsentStatusWasAwaitingAuthorisation());
		validateGetConsentResponse();
		eventLog.endBlock();
	}

	protected void validatePostConsentResponse() {
		callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
	}

	protected void validateGetConsentResponse() {
		env.mapKey("endpoint_response", "consent_endpoint_response_full");
		callAndStopOnFailure(ExtractSignedJwtFromResourceResponse.class);
		env.unmapKey("endpoint_response");
		callAndContinueOnFailure(getPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
	}

	protected void fetchConsentToCheckStatus(AbstractEnsurePaymentConsentStatusWas statusCondition) {
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(statusCondition.getClass(), Condition.ConditionResult.WARNING);
	}

}
