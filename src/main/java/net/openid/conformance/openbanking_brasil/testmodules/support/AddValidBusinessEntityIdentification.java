package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddValidBusinessEntityIdentification extends AbstractCondition {

	public static final String VALID_BUSINESS_IDENTIFICAITON = "98380199000125";

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		env.putString("config", "resource.businessEntityIdentification", VALID_BUSINESS_IDENTIFICAITON);
		return env;
	}
}
