package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToCreditFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractCreditFixedIncomesApiPaginationTest extends AbstractInvestmentsApiPaginationTestModule{

	@Override
	protected AbstractSetInvestmentApi setInvestmentsApi(){
		return new SetInvestmentApiToCreditFixedIncomes();
	}

	@Override
	protected Class<? extends Condition> setToTransactionToToday() {
		return SetToTransactionDateToToday.class;
	}

	@Override
	protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
		return AddToAndFromTransactionDateParametersToProtectedResourceUrl.class;
	}

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.CREDIT_FIXED_INCOMES;
	}
}
