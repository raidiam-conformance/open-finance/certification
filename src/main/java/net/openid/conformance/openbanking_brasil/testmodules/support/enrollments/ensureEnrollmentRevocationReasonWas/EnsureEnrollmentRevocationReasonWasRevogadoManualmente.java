package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRevocationReasonWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentRevocationReasonEnum;

public class EnsureEnrollmentRevocationReasonWasRevogadoManualmente extends AbstractEnsureEnrollmentRevocationReasonWas {

	@Override
	protected String getExpectedRevocationReason() {
		return EnrollmentRevocationReasonEnum.REVOGADO_MANUALMENTE.toString();
	}
}
