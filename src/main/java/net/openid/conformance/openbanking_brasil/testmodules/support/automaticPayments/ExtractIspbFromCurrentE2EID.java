package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class ExtractIspbFromCurrentE2EID extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	@PostEnvironment(strings = "ispb")
	public Environment evaluate(Environment env) {
		JsonObject requestBody = getPaymentFromResource(env);
		String endToEndId = getEndToEndIdFromPaymentRequestBody(requestBody);
		String ispb = getIspbFromEndToEndId(endToEndId);
		env.putString("ispb", ispb);
		logSuccess("Successfully extracted the ispb from the current endToEndId", args("ispb", ispb));
		return env;
	}

	private JsonObject getPaymentFromResource(Environment env) {
		return Optional.ofNullable(env.getObject("resource"))
			.map(resource -> resource.getAsJsonObject("brazilPixPayment"))
			.orElseThrow(() -> error("Could not find payment request body in the resource"));
	}

	private String getEndToEndIdFromPaymentRequestBody(JsonObject requestBody) {
		return Optional.ofNullable(requestBody.getAsJsonObject("data"))
			.map(data -> data.get("endToEndId"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find data.endToEndId in current payment request body",
				args("request body", requestBody)));
	}

	private String getIspbFromEndToEndId(String endToEndId) {
		return endToEndId.substring(1, 9);
	}
}
