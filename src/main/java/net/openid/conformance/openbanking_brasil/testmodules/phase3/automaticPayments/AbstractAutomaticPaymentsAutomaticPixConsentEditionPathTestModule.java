package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureUpdatedAtDateTimeFieldWasUpdated;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveCurrentInstant;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRejectionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureRecurringConsentFieldsAreAsInPatchRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToAddDefinedCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetUserDefinedCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForSemanalPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToTwoDaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasLimiteValorTransacaoConsentimentoExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentRejectionReasonCodeWasLimiteValorTransacaoConsentimentoExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo30Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractAutomaticPaymentsAutomaticPixConsentEditionPathTestModule extends AbstractAutomaticPaymentsAutomaticPixTestModule {
	/**
	 * This class should be extended by automatic pix tests that follow the steps bellow (what defines which bracket path will be taken is the method isHappyPath()):
	 *     - Call the POST recurring-consents endpoint with automatic pix fields, with referenceStartDate as {value}, expirationDateTime as {value}, period as SEMANAL, a maximumVariableAmount of {value}, and not sending firstPayment information
	 *     - Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION
	 *     - Redirect the user to authorize consent
	 *     - Call the GET recurring-consents endpoint
	 *     - Expect 201 - Validate Response and ensure status is AUTHORISED
	 *     - Call the PATCH recurring-consents endpoint with consent edition, sending creditor name as "Openflix", expirationDateTime as {value} and maximumVariableAmount as {value}
	 *     - Expect {value}- Validate Response and ensure status is AUTHORISED
	 *     - Poll the the GET recurring-consents endpoint while the maximumVariableAmount is not sent back or for 10 minutes
	 *     - Call the GET recurring-consents endpoint
	 *     - Expect 201 - Validate response, ensure status is AUTHORISED and and ensure {value}
	 *     - Call the POST recurring-payments endpoint with amount as 0.3 BRL and date as D+2
	 *     {
	 *         - Expect 422 LIMITE_VALOR_TRANSACAO_CONSENTIMENTO_EXCEDIDO or 201 - Validate Response
	 *         If a 201 is returned:
	 *         - Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP
	 *         - Call the GET recurring-payments {recurringPaymentId}
	 *         - Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is LIMITE_VALOR_TRANSACAO_CONSENTIMENTO_EXCEDIDO
	 *     } OR
	 *     {
	 *         - Expect 201 - Validate Response
	 *         - Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP
	 *         - Call the GET recurring-payments {recurringPaymentId}
	 *         - Expect 200 - Validate Response and ensure status is SCHD
	 *     }
	 * In order to properly create the recurring-consents and recurring-payments request bodies, the user will need to provide the following fields:
	 *     - resource.debtorAccountIspb
	 *     - resource.debtorAccountIssuer
	 *     - resource.debtorAccountNumber
	 *     - resource.debtorAccountType
	 *     - resource.contractDebtorName
	 *     - resource.contractDebtorIdentification
	 *     - resource.creditorAccountIspb
	 *     - resource.creditorAccountIssuer
	 *     - resource.creditorAccountNumber
	 *     - resource.creditorAccountAccountType
	 *     - resource.creditorName
	 *     - resource.creditorCpfCnp
	 */

	protected abstract AbstractCreatePatchRecurringConsentsForEditionRequestBody createPatchRequestCondition();
	protected abstract boolean isHappyPath();

	@Override
	protected void configurePaymentDate() {
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToTwoDaysInTheFuture.class);
		callAndStopOnFailure(SetPaymentReferenceForSemanalPayment.class);
		callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
	}

	@Override
	protected void configureCreditorFields() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToAddDefinedCreditor.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetUserDefinedCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj.class);
	}

	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
		env.unmapKey("access_token");
		validateGetConsentResponse();
		makePatchConsentsRequest();

		eventLog.startBlock(currentClientString() + "Call token endpoint");
		createAuthorizationCodeRequest();
		requestAuthorizationCode();
		requestProtectedResource();
		onPostAuthorizationFlowComplete();
	}

	protected void makePatchConsentsRequest() {
		callAndStopOnFailure(SaveCurrentInstant.class);
		runInBlock("Call PATCH consents - Expects 200", () -> {
			call(new CallPatchAutomaticPaymentsConsentsEndpointSequence()
				.replace(CreatePatchRecurringPaymentsConsentsForRejectionRequestBody.class,
					condition(createPatchRequestCondition().getClass())));
			callAndContinueOnFailure(patchPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
		});

		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
		runInBlock("Validate PATCH consent response", () -> {
			callAndContinueOnFailure(EnsureRecurringConsentFieldsAreAsInPatchRequest.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureUpdatedAtDateTimeFieldWasUpdated.class, Condition.ConditionResult.FAILURE);
		});
		userAuthorisationCodeAccessToken();
	}

	@Override
	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return new SetAutomaticPaymentAmountTo30Cents();
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		if (isHappyPath()) {
			return super.getPixPaymentSequence();
		}
		return super.getPixPaymentSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class));
	}

	@Override
	protected void validateResponse() {
		if (isHappyPath()) {
			super.validateResponse();
		} else {
			int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
			if (status == HttpStatus.CREATED.value()) {
				super.validateResponse();
			} else if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				callAndContinueOnFailure(postPaymentValidator(), Condition.ConditionResult.FAILURE);
				env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasLimiteValorTransacaoConsentimentoExcedido.class, Condition.ConditionResult.FAILURE);
				env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
			}
		}
	}

	@Override
	protected void validateFinalState() {
		if (isHappyPath()) {
			callAndStopOnFailure(EnsurePaymentStatusWasSchd.class);
		} else {
			callAndContinueOnFailure(EnsurePaymentStatusWasRjct.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureRecurringPaymentRejectionReasonCodeWasLimiteValorTransacaoConsentimentoExcedido.class, Condition.ConditionResult.FAILURE);
		}
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrAccpV2.class;
	}

	protected abstract Class<? extends Condition> patchPaymentConsentValidator();
}
