package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AccountSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromBookingDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlTransactionsPageSize1000;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public abstract class AbstractAccountsApiMaxPageSizePagingTestModule extends AbstractPhase2TestModule {
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	protected abstract Class<? extends Condition> getPostConsentValidator();

	protected abstract Class<? extends Condition> getAccountTransactionsValidator();


	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class),
			condition(GetAccountsV2Endpoint.class));
	}


	@Override
	protected void configureClient() {
		super.configureClient();
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromBookingDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toBookingDate", currentDate.format(FORMATTER));
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ACCOUNTS).addScopes(OPFScopesEnum.ACCOUNTS, OPFScopesEnum.OPEN_ID).build();
	}

	@Override
	protected void validateResponse() {

		preCallProtectedResource("Prepare to Fetch Account Transactions V2");
		callAndStopOnFailure(AccountSelector.class);
		callAndStopOnFailure(SetProtectedResourceUrlTransactionsPageSize1000.class);
		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);
		callAndStopOnFailure(SetResourceMethodToGet.class);
		callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
		callAndStopOnFailure(CallProtectedResource.class);
		callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
		callAndContinueOnFailure(getAccountTransactionsValidator(), Condition.ConditionResult.FAILURE);

		preCallProtectedResource("Prepare to Fetch page 2 of Account Transactions V2");
		callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
		callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);
		callAndStopOnFailure(SetResourceMethodToGet.class);
		callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
		callAndStopOnFailure(CallProtectedResource.class);
		callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
		callAndContinueOnFailure(getAccountTransactionsValidator(), Condition.ConditionResult.FAILURE);

	}

}
