package net.openid.conformance.openbanking_brasil.testmodules.v3.yacs;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureBrazilCpf;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.EnsureDcrClientExists;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "fvp-preflight-check-test-v3",
	displayName = "Pre-Flights test that ensures that tested server accepted a DCR request and all the required tested URIs are registered within the Directory",
	summary = "Make sure the brazilCpf has been provided\n" +
		"\n" +
		"Make sure a client_id has been generated on the DCR\n" +
		"\n" +
		"Call the Tested Server Token endpoint with client_credentials grant - Make sure that server returns a 200\n" +
		"\n" +
		"Call the Directory participants endpoint for either Sandbox or Production, depending on the used platform\n" +
		"\n" +
		"Make sure The Provided Well-Known is registered within the Directory\n" +
		"\n" +
		"Make sure that the server has registered a ApiFamilyType of value consents with ApiVersion of value “3.x.x\". Return the ApiEndpoint that ends with /consents/v3/consents\n" +
		"\n" +
		"Make sure that the server has registered a ApiFamilyType of value resources with ApiVersion of value “3.x.x\". Return the ApiEndpoint that ends with /resources/v3/resources\n",

	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"resource.brazilCpf",
		"resource.brazilCnpj",
		"directory.client_id"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.consentUrl",
})
public class YACSPreFlightCertCheckV3Module extends AbstractClientCredentialsGrantFunctionalTestModule {
	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(EnsureBrazilCpf.class);
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(EnsureDcrClientExists.class);
		call(new ValidateWellKnownUriSteps());
	}

	@Override
	protected void runTests() {
		call(new ValidateRegisteredEndpoints(GetConsentV3Endpoint.class)
			.insertAfter(GetConsentV3Endpoint.class, condition(GetResourcesV3Endpoint.class)));
	}

	@Override
	protected void logFinalEnv() {
		//Not needed here
	}
}



