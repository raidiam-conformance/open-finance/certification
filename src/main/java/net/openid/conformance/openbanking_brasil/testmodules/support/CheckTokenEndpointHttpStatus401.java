package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.client.AbstractCheckTokenEndpointHttpStatus;
import org.springframework.http.HttpStatus;

public class CheckTokenEndpointHttpStatus401 extends AbstractCheckTokenEndpointHttpStatus {

    @Override
    protected HttpStatus getExpectedHttpStatus() {
        return HttpStatus.UNAUTHORIZED;
    }
}
