package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsentsApiEnforceDICTTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_dict_test-module_v4",
	displayName = "Payments Consents API test module for dict local instrument",
	summary = "Ensure error when localInstrument is DICT and QRCode is sent (Ref Error 2.2.2.8)\n" +
		"• Calls POST Consents Endpoint with localInstrument as DICT and QRCode on the payload\n" +
		"• Expects 422 DETALHE_PAGAMENTO_INVALIDO - Validate Error Message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiEnforceDICTTestModuleV4 extends AbstractPaymentsConsentsApiEnforceDICTTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}
}
