package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setStartDate;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public abstract class AbstractEditRecurringPaymentsConsentBodyToSetStartDate extends AbstractCondition {

	protected abstract LocalDateTime calculateStartDateTime(LocalDateTime currentDateTime);

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.map(consentElement -> consentElement.getAsJsonObject().getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data field in consents payload"));

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		LocalDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		LocalDateTime dateTime = calculateStartDateTime(currentDateTime);
		String formattedDateTime = dateTime.format(formatter);
		data.addProperty("startDateTime", formattedDateTime);

		logSuccess("The startDateTime field has been set",
			args("data", data));

		return env;
	}
}
