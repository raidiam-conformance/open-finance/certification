package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoRegistrationOptionsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreatePatchEnrollmentsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentCancelledFromWas.EnsureEnrollmentCancelledFromWasIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas.EnsureEnrollmentRejectionReasonWasRejeitadoManualmente;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRejected;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareRejectionOrRevocationReasonForPatchRequest.PrepareRejectionReasonRejeitadoManualmenteForPatchRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPatchEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoRegistrationOptions;

public abstract class AbstractEnrollmentsApiRejectedPreAuthorisationEnrollmentTestModule extends AbstractEnrollmentsApiTestModule {

	protected abstract Class<? extends Condition> postFidoRegistrationOptionsValidator();

	@Override
	protected void performAuthorizationFlow(){
		performPreAuthorizationSteps();
		executeTestSteps();
		onPostAuthorizationFlowComplete();
	}

    @Override
    protected void executeTestSteps() {

		runInBlock("Call PATCH enrollments - Expects 204", () -> {
			callAndStopOnFailure(PrepareRejectionReasonRejeitadoManualmenteForPatchRequest.class);
			PostEnrollmentsResourceSteps patchEnrollmentsSteps = new PostEnrollmentsResourceSteps(
				new PrepareToPatchEnrollments(),
				CreatePatchEnrollmentsRequestBodyToRequestEntityClaims.class,
				true
			);
			call(patchEnrollmentsSteps);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
		});

		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRejected(), "REJECTED");
		runInBlock("Validate enrollments fields related to cancellation", () -> {
			callAndContinueOnFailure(EnsureEnrollmentCancelledFromWasIniciadora.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureEnrollmentRejectionReasonWasRejeitadoManualmente.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Call POST fido-registration-options with client_credentials token - Expects 401", () -> {
			PostEnrollmentsResourceSteps postFidoRegistrationOptionsSteps = new PostEnrollmentsResourceSteps(
				new PrepareToPostFidoRegistrationOptions(),
				CreateEnrollmentsFidoRegistrationOptionsRequestBody.class,
				true
			);
			call(postFidoRegistrationOptionsSteps);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas401.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postFidoRegistrationOptionsValidator(), Condition.ConditionResult.FAILURE);
		});

    }
}
