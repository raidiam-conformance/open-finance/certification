package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilCreateBadConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateInvalidFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasPermissoesPfPjEmConjunto;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractConsentApiBadConsentsTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	protected abstract Class<? extends Condition> setPostConsentValidator();

	@Override
	protected void runTests() {

		runInBlock("Create incompatible consents v3-1", () -> {
			call(new ValidateWellKnownUriSteps());
			call(new ValidateRegisteredEndpoints(GetConsentV3Endpoint.class));
			callAndStopOnFailure(AddProductTypeToPhase2Config.class);
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
			scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS).build();
			callAndStopOnFailure(FAPIBrazilCreateBadConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
			callAndStopOnFailure(SetContentTypeApplicationJson.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureConsentResponseCodeWas422.class);
			env.mapKey(EnsureErrorResponseCodeWasPermissoesPfPjEmConjunto.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndStopOnFailure(EnsureErrorResponseCodeWasPermissoesPfPjEmConjunto.class);
			env.putString("metaOnlyRequestDateTime", "true");
			callAndContinueOnFailure(setPostConsentValidator(), Condition.ConditionResult.FAILURE);
		});

		runInBlock("Create consent without x-fapi-interaction-id v3-1", () -> {
			call(createConsentSequence());
		});

		runInBlock("Create consent with an invalid x-fapi-interaction-id v3-1", () -> {
			call(createConsentSequence()
				.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class, sequenceOf(
					condition(CreateInvalidFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
				))
			);
		});
	}

	protected ConditionSequence createConsentSequence() {
		return sequenceOf(
			condition(FAPIBrazilOpenBankingCreateConsentRequest.class),
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(FAPIBrazilAddExpirationToConsentRequest.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(SetContentTypeApplicationJson.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(EnsureConsentResponseCodeWas400.class),
			condition(setPostConsentValidator())
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(CheckForFAPIInteractionIdInResourceResponse.class)
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)
		);
	}

}
