package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n2;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.AbstractGetInvoiceFinancingsScheduledInstalmentsOASValidator;


public class GetInvoiceFinancingsScheduledInstalmentsV2n2OASValidator extends AbstractGetInvoiceFinancingsScheduledInstalmentsOASValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/invoiceFinancings/invoice-financings-2.2.0.yml";
	}

}
