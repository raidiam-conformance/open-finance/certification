package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.AbstractGetUnarrangedAccountsOverdraftWarrantiesOASValidator;


public class GetUnarrangedAccountsOverdraftWarrantiesV2n3OASValidator extends AbstractGetUnarrangedAccountsOverdraftWarrantiesOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/unarrangedAccountsOverdraft/unarranged-accounts-overdraft-v2.3.0.yml";
	}
}
