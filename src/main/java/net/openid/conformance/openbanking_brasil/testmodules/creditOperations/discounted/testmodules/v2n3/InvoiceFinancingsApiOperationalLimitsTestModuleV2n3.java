package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.AbstractInvoiceFinancingsApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsIdentificationV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsListV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsPaymentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsScheduledInstalmentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsWarrantiesV2n3OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "invoice-financings_api_operational-limits_test-module_v2-3",
	displayName ="This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Financings API are considered correctly\n",
	summary = "This test will require the user to have set at least two ACTIVE resources on the Invoice Financings API. \n" +
		"This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Invoice Financings API are considered correctly.\n" +
		"\u2022 Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test have been provided\n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Credit Operations permission group\n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 Redirect User to authorize the Created Consent - Expect a successful authorization\n" +
		"\u2022 With the authorized consent id (1), call the GET Invoice Financings List API Once - Expect a 200 - Save the first returned ACTIVE resource id (R_1) and the second saved returned active resource id (R_2)\n" +
		"\u2022 With the authorized consent id (1), call the GET Invoice Financings  API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Invoice Financings Warranties API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Invoice Financings Scheduled Instalments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Invoice Financings Payments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"\u2022 With the authorized consent id (1), call the GET Invoice Financings API with the saved Resource ID (R_2) 4 times - Expect a 200\n" +
		"\u2022 Repeat the exact same process done with the first tested resources (R_1) but now, execute it against the second returned Resource (R_2) \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness"
	}
)
public class InvoiceFinancingsApiOperationalLimitsTestModuleV2n3 extends AbstractInvoiceFinancingsApiOperationalLimitsTestModule {

	@Override
	protected Class<? extends Condition> contractsResponseValidator() {
		return GetInvoiceFinancingsListV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractIdValidator() {
		return GetInvoiceFinancingsIdentificationV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractWarrantiesValidator() {
		return GetInvoiceFinancingsWarrantiesV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractScheduledInstalmentsValidator() {
		return GetInvoiceFinancingsScheduledInstalmentsV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractPaymentsValidator() {
		return GetInvoiceFinancingsPaymentsV2n3OASValidator.class;
	}

}

