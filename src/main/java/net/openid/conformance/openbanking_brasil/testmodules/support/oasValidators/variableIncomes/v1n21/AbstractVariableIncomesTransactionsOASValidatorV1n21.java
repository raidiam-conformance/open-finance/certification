package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

import java.util.List;

public abstract class AbstractVariableIncomesTransactionsOASValidatorV1n21 extends OpenAPIJsonSchemaValidator {



	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/variableIncome/variable-income-v1.2.1.yml";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		body.getAsJsonArray("data").forEach(el -> assertData(el.getAsJsonObject()));
	}


	private void assertData(JsonObject data){
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionTypeAdditionalInfo",
			"transactionType",
			"OUTROS"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionUnitPrice",
			"transactionType",
			List.of("COMPRA", "VENDA")
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionQuantity",
			"transactionType",
			List.of("COMPRA", "VENDA")
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"brokerNoteId",
			"transactionType",
			List.of("COMPRA", "VENDA")
		);

	}
}
