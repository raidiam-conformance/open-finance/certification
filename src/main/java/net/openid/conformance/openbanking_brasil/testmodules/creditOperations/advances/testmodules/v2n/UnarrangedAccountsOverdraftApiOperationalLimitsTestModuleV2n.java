package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.AbstractUnarrangedAccountsOverdraftApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftIdentificationV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftListV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftPaymentsV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftScheduledInstalmentsV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftWarrantiesV2n2OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "unarranged-accounts-overdraft_api_operational-limits_test-module_V2-2",
	displayName = "This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Unarragend Accounts Overdrafts API are considered correctly\n",
	summary = "This test will require the user to have set at least two ACTIVE resources on the Unarragend Accounts Overdrafts  API. \n" +
		"This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Unarragend Accounts Overdrafts  API are considered correctly.\n" +
		"• Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test have been provided\n" +
		"• Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Credit Operations permission group \n" +
		"• Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"• Redirect User to authorize the Created Consent - Expect a successful authorization\n" +
		"• With the authorized consent id (1), call the GET Unarranged Accounts List API Once - Expect a 200 - Save the first returned ACTIVE resource id (R_1) and the second saved returned active resource id (R_2)\n" +
		"• With the authorized consent id (1), call the GET Unarranged Accounts  API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"• With the authorized consent id (1), call the GET Unarranged Accounts Warranties API with the saved Resource ID (R_1) 4 times - Expect a 200\n" +
		"• With the authorized consent id (1), call the GET Unarranged Accounts Scheduled Instalments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"• With the authorized consent id (1), call the GET Unarranged Accounts Payments API with the saved Resource ID (R_1) 30 times - Expect a 200\n" +
		"• With the authorized consent id (1), call the GET Unarranged Accounts API with the saved Resource ID (R_2) 4 times - Expect a 200\n" +
		"• Repeat the exact same process done with the first tested resources (R_1) but now, execute it against the second returned Resource (R_2) \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness",
		"resource.first_unarranged_account_overdraft_account_id",
		"resource.second_unarranged_account_overdraft_account_id"
	}
)

public class UnarrangedAccountsOverdraftApiOperationalLimitsTestModuleV2n extends AbstractUnarrangedAccountsOverdraftApiOperationalLimitsTestModule {

	@Override
	protected Class<? extends Condition> contractsResponseValidator() {
		return GetUnarrangedAccountsOverdraftListV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractIdValidator() {
		return GetUnarrangedAccountsOverdraftIdentificationV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractWarrantiesValidator() {
		return GetUnarrangedAccountsOverdraftWarrantiesV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractScheduledInstalmentsValidator() {
		return GetUnarrangedAccountsOverdraftScheduledInstalmentsV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> contractPaymentsValidator() {
		return GetUnarrangedAccountsOverdraftPaymentsV2n2OASValidator.class;
	}
}
