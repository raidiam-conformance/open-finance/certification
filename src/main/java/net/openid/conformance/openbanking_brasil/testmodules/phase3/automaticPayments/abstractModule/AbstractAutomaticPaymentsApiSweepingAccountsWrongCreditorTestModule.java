package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractUnhappyAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsManualObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetInvalidIdentification;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import org.springframework.http.HttpStatus;

public abstract class AbstractAutomaticPaymentsApiSweepingAccountsWrongCreditorTestModule extends AbstractUnhappyAutomaticPaymentsFunctionalTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected HttpStatus expectedResponseCode() {
		return HttpStatus.UNPROCESSABLE_ENTITY;
	}

	@Override
	protected void requestProtectedResource(){
		callAndStopOnFailure(AddRiskSignalsManualObjectToPixPaymentBody.class);
		super.requestProtectedResource();
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas ensureErrorResponseCodeFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento();
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EditRecurringPaymentBodyToSetInvalidIdentification.class);
	}
}
