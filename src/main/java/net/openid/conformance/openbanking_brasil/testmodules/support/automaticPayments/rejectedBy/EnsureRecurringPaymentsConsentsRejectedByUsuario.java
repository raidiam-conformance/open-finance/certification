package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedBy;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectedByEnum;

public class EnsureRecurringPaymentsConsentsRejectedByUsuario extends AbstractEnsureRecurringPaymentsConsentsRejectedBy {

	@Override
	protected RecurringPaymentsConsentsRejectedByEnum expectedRejectedBy() {
		return RecurringPaymentsConsentsRejectedByEnum.USUARIO;
	}
}
