package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsentsCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.StopIfWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments-consents-core-test-v4",
	displayName = "The test will make the user authorize a payment consent request on his home app, changing payment status to AUTHORISED, however a payment won’t be concluded leaving the AUTHORISED consent to be expired.",
	summary = """
			Obtain the payments-consents endpoint for the server from the directory using the directory participants API and the provided discoveryUrl
			Create the POST payments-consents payload using the provided brazilCpf and, if present, brazilCnpj. Set all the other fields for the standard Production Creditor Account
			Call the POST Token endpoint with the selected authorization method sending scope=payments and grant as client_credentials
			Call the POST payments-consents endpoint with the obtaine Bearer token and the generated payload
			Expects a 201 - Validate that status is “AWAITING_AUTHORISATION”. Validate that all fields all compliant with specifications, including the meta object and that no additional fields were sent
			Redirect the user to authorize his Consent
			Expect an authorization code to be returned
			Call the POST Token endpoint with authorization_code grant and the returned authorization code from the redirect
			Validate that a Token was created and that a refresh token is included
			Call the POST Token endpoint with the refresh_token grant
			Validate that a new Token was created and that the refresh_token has not been rotated
		""",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.brazilCnpj",
		"resource.brazilCpf",
		"resource.consentUrl"
	}
)
public class YACSPaymentsConsentsCoreTestModuleV4 extends AbstractPaymentsConsentsCoreTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected ConditionSequence getPaymentAndPaymentConsentEndpoints() {
		return sequenceOf(
			condition(GetPaymentConsentsV4Endpoint.class),
			condition(StopIfWarning.class),
			condition(GetPaymentV4Endpoint.class)
		);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator(){
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

}
