package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.pixqrcode.PixQRCode;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Locale;
import java.util.Random;

public class InjectQRCodeWithWrongAmountIntoConfig extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonObject consentPayment = env.getElementFromObject("resource", "brazilPaymentConsent.data.payment").getAsJsonObject();
		String givenAmount = OIDFJSON.getString(consentPayment.get("amount"));

		JsonElement paymentInitiation = env.getElementFromObject("resource", "brazilPixPayment.data");
		JsonObject consentPaymentDetails = env.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details").getAsJsonObject();
		PixQRCode qrCode = new PixQRCode();
		qrCode.useStandardConfig();

		String amount;
		do {
			float random = new Random().nextFloat() * 100;
			amount = String.format(Locale.UK, "%.02f", random);
		} while (
			givenAmount.equals(amount)
		);
		qrCode.setTransactionAmount(amount);

		consentPaymentDetails.addProperty("qrCode", qrCode.toString());

		if (paymentInitiation.isJsonArray()) {
			for (int i = 0; i < paymentInitiation.getAsJsonArray().size(); i++) {
				JsonObject payment = paymentInitiation.getAsJsonArray().get(i).getAsJsonObject();
				payment.addProperty("qrCode", qrCode.toString());
			}
			logSuccess(String.format("Added new QRes to payment consent and payment initiation with amount %s BRL", amount), args("QRes", qrCode.toString()));
			return env;
		}

		paymentInitiation.getAsJsonObject().addProperty("qrCode", qrCode.toString());
		logSuccess(String.format("Added new QRes to payment consent and payment initiation with amount %s BRL", amount), args("QRes", qrCode.toString()));
		return env;
	}
}
