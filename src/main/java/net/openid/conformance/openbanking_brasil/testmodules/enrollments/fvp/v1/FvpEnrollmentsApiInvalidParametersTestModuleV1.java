package net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiInvalidParametersTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.RemoveEnrollmentsUrlFromResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsRequestBodyWithoutDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.GetEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-enrollments_api_invalid-parameters_test-module_v1",
	displayName = "fvp-enrollments_api_invalid-parameters_test-module_v1",
	summary = "Ensure that enrollment can't be requested if request parameters are sent wrongly\n" +
		"• Call the POST enrollments endpoint, don't send an x-fapi-interaction-id on the header\n" +
		"• Expect a 400 response\n" +
		"• Call the POST enrollments endpoint, sign the message with an incorrect private key\n" +
		"• Expect a 400 response\n" +
		"• Call the POST enrollments endpoint without sending the issuer field, but with accountType as CACC\n" +
		"• Expect a 422 response - Validate Code CONTA_INVALIDA\n" +
		"• Call the POST enrollments endpoint without sending the debtorAccount.accountType field\n" +
		"• Expect a 422 response - With Code CONTA_INVALIDA\n" +
		"• Call the POST enrollments endpoint and send an invalid permission value Expect a 422 response - With Code PERMISSOES_INVALIDAS\n" +
		"• Call the PATCH Enrollments {EnrollmentID}\n" +
		"• Expects 204\n"
	,
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
	}
)
public class FvpEnrollmentsApiInvalidParametersTestModuleV1 extends AbstractEnrollmentsApiInvalidParametersTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CreateEnrollmentsRequestBodyWithoutDebtorAccount.class);
		callAndStopOnFailure(PrepareToPostEnrollments.class);
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf();
	}

	@Override
	protected void configureClient() {
		callAndStopOnFailure(RemoveEnrollmentsUrlFromResource.class);

		call(new ValidateWellKnownUriSteps());
		call(exec().startBlock("Validating Authorisation Server has supported Endpoint"));
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		callAndStopOnFailure(GetPaymentConsentsV4Endpoint.class);
		callAndStopOnFailure(GetPaymentV4Endpoint.class);
		callAndContinueOnFailure(GetEnrollmentsV1Endpoint.class, Condition.ConditionResult.INFO);
		if (env.getObject("config").getAsJsonObject("resource").get("enrollmentsUrl")==null){
			//the test will stop here
			fireTestSkipped("Skipping test as a enrollmentsUrl was not found on participants server.");
		}
		call(exec().endBlock());
		super.configureClient();
	}
	@Override
	public void cleanup() {
		//not required
	}

}
