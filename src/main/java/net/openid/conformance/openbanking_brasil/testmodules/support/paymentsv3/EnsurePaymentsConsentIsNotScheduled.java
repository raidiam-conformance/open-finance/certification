package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Optional;

public class EnsurePaymentsConsentIsNotScheduled extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config")
    public Environment evaluate(Environment env) {
        JsonObject payment = Optional.ofNullable(env.getElementFromObject("config", "resource.brazilPaymentConsent.data.payment"))
            .orElseThrow(() -> error("Could not find config.resource.brazilPaymentConsent.data.payment"))
            .getAsJsonObject();

        LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
        payment.remove("schedule");
        if (!payment.has("date")) {
            payment.addProperty("date", currentDate.toString());
        }

        logSuccess("Payments Consent is not scheduled", args("payment object in consent", payment));

        return env;
    }
}
