package net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public abstract class AbstractEnsureConsentStatusWas extends AbstractCondition {

	protected abstract String getExpectedStatus();

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		try {
			String status = BodyExtractor.bodyFrom(env, "consent_endpoint_response_full")
				.map(OIDFJSON::toObject)
				.map(body -> body.getAsJsonObject("data"))
				.map(data -> data.get("status"))
				.map(OIDFJSON::getString)
				.orElseThrow(() -> error("Could not extract status from consent response data"));

			if (!status.equals(getExpectedStatus())) {
				throw error(String.format("Expected consent to be in the %s state after redirect but it was not", getExpectedStatus()),
					args("status", status));
			}
			logSuccess(String.format("Consent was in the %s state after redirect", getExpectedStatus()));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}

		return env;
	}
}
