package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Optional;

public class InsertTemporizationTestValuesIntoConsentRequest extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {

		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("config", "resource.brazilPaymentConsent.data"))
			.orElseThrow(() -> error("Could not find brazilPaymentConsent")).getAsJsonObject();

		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));


		JsonElement payment = new JsonObjectBuilder()
			.addField("amount", "12345.67")
			.addField("type", "PIX")
			.addField("currency", "BRL")
			.addField("ibgeTownCode", "1400704")
			.addField("date", currentDate.toString())
			.addField("details.localInstrument", DictHomologKeys.PROXY_EMAIL_STANDARD_LOCALINSTRUMENT)
			.addField("details.proxy", DictHomologKeys.PROXY_TEMPORIZATION_EMAIL)
			.addField("details.creditorAccount.ispb", DictHomologKeys.PROXY_TEMPORIZATION_EMAIL_ISPB)
			.addField("details.creditorAccount.issuer", DictHomologKeys.PROXY_TEMPORIZATION_EMAIL_BRANCH_NUMBER)
			.addField("details.creditorAccount.number", DictHomologKeys.PROXY_TEMPORIZATION_EMAIL_ACCOUNT_NUMBER)
			.addField("details.creditorAccount.accountType", DictHomologKeys.PROXY_TEMPORIZATION_EMAIL_ACCOUNT_TYPE)
			.build();


		brazilPaymentConsent.add("payment", payment.getAsJsonObject());

		JsonObject creditor = brazilPaymentConsent.getAsJsonObject("creditor");
		creditor.addProperty("cpfCnpj", DictHomologKeys.PROXY_TEMPORIZATION_EMAIL_CPF);
		creditor.addProperty("name", DictHomologKeys.PROXY_TEMPORIZATION_EMAIL_NAME);
		creditor.addProperty("personType", DictHomologKeys.PROXY_TEMPORIZATION_EMAIL_PERSON_TYPE);

		logSuccess("Specific values added for temporization test", args("payment consent", brazilPaymentConsent));

		return env;
		}
}
