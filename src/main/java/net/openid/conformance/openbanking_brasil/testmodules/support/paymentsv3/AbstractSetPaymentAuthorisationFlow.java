package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractSetPaymentAuthorisationFlow extends AbstractCondition {

	protected abstract String getAuthorisationFlowType();

	@Override
	public Environment evaluate(Environment env) {

		JsonElement pixPaymentDataElem = env.getElementFromObject("config", "resource.brazilPixPayment.data");
		if (pixPaymentDataElem == null) {
			throw error("PixPayment object not found in config");
		}

		if (pixPaymentDataElem.isJsonArray()) {
			JsonArray pixPaymentData = pixPaymentDataElem.getAsJsonArray();
			for (int i = 0; i < pixPaymentData.size(); i++) {
				JsonObject pixPayment = pixPaymentData.get(i).getAsJsonObject();
				pixPayment.addProperty("authorisationFlow", getAuthorisationFlowType());
				logSuccess(String.format("authorisation flow set to %s", getAuthorisationFlowType()), pixPayment);
			}
			return env;
		}

		JsonObject pixPaymentData = pixPaymentDataElem.getAsJsonObject();
		pixPaymentData.addProperty("authorisationFlow", getAuthorisationFlowType());

		logSuccess(String.format("authorisation flow set to %s", getAuthorisationFlowType()), pixPaymentData);
		return env;
	}
}
