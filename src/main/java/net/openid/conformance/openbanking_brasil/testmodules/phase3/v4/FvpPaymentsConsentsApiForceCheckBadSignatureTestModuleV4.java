package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractFvpPaymentsConsentsApiForceCheckBadSignatureTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments_api_force-check-signature_test-module_v4",
	displayName = "Payments Consents API basic test module ",
	summary = "Ensure error when sending consent endpoint request with a bad signature\n" +
		"• Create a client by performing a DCR against the provided server - Expect Success\n" +
		"• Calls POST Consents Endpoint random times with valid signature\n" +
		"• Expects 201 - Validate Response\n" +
		"• Calls POST Consents Endpoint with badly signed request\n" +
		"• Expects 400 - Validate error message\n" +
		"• Delete the created client",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class FvpPaymentsConsentsApiForceCheckBadSignatureTestModuleV4 extends AbstractFvpPaymentsConsentsApiForceCheckBadSignatureTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetPaymentConsentsV4Endpoint.class;
	}
}
