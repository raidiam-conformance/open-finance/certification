package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class ValidateCaChainReturnedSequence extends AbstractConditionSequence {
	@Override
	public void evaluate() {
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		callAndContinueOnFailure(ValidateCaChainReturnedPhase2Phase3.class, Condition.ConditionResult.FAILURE);
	}
}
