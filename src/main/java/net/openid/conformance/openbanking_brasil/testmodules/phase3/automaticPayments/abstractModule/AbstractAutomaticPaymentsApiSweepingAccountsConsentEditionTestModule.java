package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsConsentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRejectionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureExpirationDateTimeFieldIsUnaltered;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractExpirationDateTimeFromRecurringConsentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyExpirationD270;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasCampoNaoPermitido;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractAutomaticPaymentsApiSweepingAccountsConsentEditionTestModule extends AbstractAutomaticPaymentsConsentsFunctionalTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture.class);
	}

	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(ExtractExpirationDateTimeFromRecurringConsentResponse.class);

		patchAutomaticPaymentsConsentEndpoint();

		fetchConsentToCheckIfEditionDidNotHappen();
	}

	protected void patchAutomaticPaymentsConsentEndpoint() {
		useClientCredentialsAccessToken();

		ConditionSequence patchSequence = new CallPatchAutomaticPaymentsConsentsEndpointSequence()
			.replace(CreatePatchRecurringPaymentsConsentsForRejectionRequestBody.class, condition(CreatePatchRecurringConsentsForEditionRequestBodyExpirationD270.class))
			.replace(EnsureConsentResponseCodeWas200.class, condition(EnsureConsentResponseCodeWas422.class))
			.insertAfter(EnsureMatchingFAPIInteractionId.class, sequenceOf(
				exec().mapKey(EnsureErrorResponseCodeFieldWasCampoNaoPermitido.RESPONSE_ENV_KEY, "consent_endpoint_response_full"),
				condition(patchPaymentConsentValidator()), condition(EnsureErrorResponseCodeFieldWasCampoNaoPermitido.class),
				exec().unmapKey(EnsureErrorResponseCodeFieldWasCampoNaoPermitido.RESPONSE_ENV_KEY)
			));

		runInBlock("PATCH recurring-consents endpoint with expirationDateTime as D+270 - Expects 422 CAMPO_NAO_PERMITIDO", () -> {
			call(patchSequence);
		});
	}

	protected void fetchConsentToCheckIfEditionDidNotHappen() {
		runInBlock("GET recurring-consents endpoint - Expects 200 with unaltered fields", () -> {
			callAndStopOnFailure(AddJWTAcceptHeader.class);
			callAndStopOnFailure(ExpectJWTResponse.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureConsentResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureExpirationDateTimeFieldIsUnaltered.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected abstract Class<? extends Condition> patchPaymentConsentValidator();
}
