package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractCreateWebhookEndpoint;

public class CreateRecurringPaymentWebhookV2 extends AbstractCreateWebhookEndpoint {

	@Override
	protected String getValueFromEnv() {
		return "payment_id";
	}

	@Override
	protected String getExpectedUrlPath() {
		return "/open-banking/webhook/v1/automatic-payments/v2/pix/recurring-payments/";
	}
}
