package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2;

import net.openid.conformance.logging.ApiName;

@ApiName("Credit Card Accounts Transaction Current V2")
public class GetCreditCardAccountsTransactionsCurrentOASValidatorV2 extends AbstractGetCreditCardAccountsTransactionsOASValidatorV2 {
	@Override
	protected String getEndpointPath() {
		return "/accounts/{creditCardAccountId}/transactions-current";
	}
}
