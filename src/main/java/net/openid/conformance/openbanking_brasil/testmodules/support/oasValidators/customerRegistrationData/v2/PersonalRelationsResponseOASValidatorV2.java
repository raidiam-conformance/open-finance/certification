package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;

@ApiName("Natural Person Relationship V2.2.0")
public class PersonalRelationsResponseOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/customers/swagger-customers-2.2.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/personal/financial-relations";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		this.assertProductsServicesTypeAdditionalInfoConstraint(data);
		this.assertAccountBranchCodeConstraint(data);

	}

	protected void assertProductsServicesTypeAdditionalInfoConstraint(JsonObject data) {
		JsonArray productsServicesTypes = data.getAsJsonArray("productsServicesType");
		boolean productsServicesTypeAdditionalInfoIsRequired = false;
		for(JsonElement productsServicesType : productsServicesTypes) {
			if("OUTROS".equals(OIDFJSON.getString(productsServicesType))) {
				productsServicesTypeAdditionalInfoIsRequired = true;
				break;
			}
		}
		if(productsServicesTypeAdditionalInfoIsRequired && data.get("productsServicesTypeAdditionalInfo") == null) {
			throw error("data.productsServicesTypeAdditionalInfo is required when data.productsServicesType contains \"OUTROS\"");
		}
	}

	protected void assertAccountBranchCodeConstraint(JsonObject data) {
		JsonArray accounts = JsonPath.read(data, "$.accounts[*]");
		for(JsonElement accountElement : accounts) {
			JsonObject account = accountElement.getAsJsonObject();
			String accountType = OIDFJSON.getString(account.get("type"));
			if(!"CONTA_PAGAMENTO_PRE_PAGA".equals(accountType) && account.get("branchCode") == null) {
				throw error("data.accounts.branchCode is required when data.accounts.accountType is not \"CONTA_PAGAMENTO_PRE_PAGA\"");
			}
		}
	}
}
