package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EditRecurringPaymentRequestClaimsToSetDifferentCreditorAccountInfo extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonObject creditorAccount = Optional.ofNullable(env.getElementFromObject("resource_request_entity_claims", "data.creditorAccount"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Unable to find data.creditorAccount in recurring-payments request payload"));

		String oldNumber = OIDFJSON.getString(creditorAccount.get("number"));
		String newNumber = oldNumber.length() < 20 ? oldNumber + "0" : "1234";
		creditorAccount.addProperty("number", newNumber);

		String oldAccountType = OIDFJSON.getString(creditorAccount.get("accountType"));
		String newAccountType = oldAccountType.equals("CACC") ? "SVGS" : "CACC";
		creditorAccount.addProperty("accountType", newAccountType);

		logSuccess("Updated creditorAccount info to differ from original values", args("creditorAccount", creditorAccount));

		return env;
	}
}
