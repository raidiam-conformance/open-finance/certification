package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class ExtractEnrollmentIdFromEnrollmentsEndpointResponse extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    @PostEnvironment(strings = "enrollment_id")
    public Environment evaluate(Environment env) {
        JsonObject body;
        try {
            body = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                .orElseThrow(() -> error("Could not extract body from jwt response"))
                .getAsJsonObject();
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }

        String enrollmentId = OIDFJSON.getString(
            Optional.ofNullable(
                body.getAsJsonObject("data").get("enrollmentId")
            ).orElseThrow(() -> error("Could not find the enrollmentId"))
        );
        env.putString("enrollment_id", enrollmentId);

        logSuccess("Extracted the enrollment id", args("enrollment_id", enrollmentId));
        return env;
    }
}
