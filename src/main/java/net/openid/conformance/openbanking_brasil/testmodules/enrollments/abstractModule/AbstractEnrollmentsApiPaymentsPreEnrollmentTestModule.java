package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CompareIdTokenClaims;
import net.openid.conformance.condition.client.EnsureAccessTokenValuesAreDifferent;
import net.openid.conformance.condition.client.ExtractRefreshTokenFromTokenResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateFidoAuthorizeRequestBodyWithMockedFidoAssertionToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasStatusVinculoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostConsentsAuthorize;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasNaoInformadoV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;

public abstract class AbstractEnrollmentsApiPaymentsPreEnrollmentTestModule extends AbstractEnrollmentsApiTestModule {

	@Override
	protected void executeTestSteps() {
		runInBlock("Call POST consents - Expects 201", this::postAndValidateConsent);

		makeRefreshTokenCall();

		postAndValidateConsentsAuthorise();

		getAndValidateConsentAfterConsentsAuthorise();
	}

	protected void getAndValidateConsentAfterConsentsAuthorise() {
		runInBlock("Call GET consents - Expects 200 with REJECTED status and rejectionReason NAO_INFORMADO", this::getAndValidateConsent);
	}

	protected void postAndValidateConsentsAuthorise() {
		runInBlock("Call POST consents authorise with mocked fido assertion - Expects 422 STATUS_VINCULO_INVALIDO", () -> {
			call(createConsentsAuthoriseSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.RESPONSE_ENV_KEY);
			callAndContinueOnFailure(postConsentsAuthoriseValidator(), Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected void requestAuthorizationCode() {
		super.requestAuthorizationCode();
		callAndStopOnFailure(ExtractRefreshTokenFromTokenResponse.class);
	}

	protected void makeRefreshTokenCall() {
		userAuthorisationCodeAccessToken();
		call(new RefreshTokenRequestSteps(false, addTokenEndpointClientAuthentication)
			.skip(EnsureAccessTokenValuesAreDifferent.class, "Will not compare tokens")
			.skip(CompareIdTokenClaims.class, "Will not compare tokens"));
	}

	protected void postAndValidateConsent() {
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		call(createOBBPreauthSteps()
			.skip(FAPIBrazilAddConsentIdToClientScope.class, "Consent id is not added to the scope in non-redirect tests"));
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(SaveAccessToken.class);
		runInBlock(currentClientString() + "Validate consents response", this::validatePostConsentResponse);
	}

	protected void validatePostConsentResponse() {
		callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
	}

	protected ConditionSequence createConsentsAuthoriseSteps() {
		return new PostEnrollmentsResourceSteps(
			new PrepareToPostConsentsAuthorize(),
			CreateFidoAuthorizeRequestBodyWithMockedFidoAssertionToRequestEntityClaims.class,
			true
		);
	}

	protected void getAndValidateConsent() {
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(getConsentsValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentStatusWasRejected.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentsRejectionReasonCodeWasNaoInformadoV3.class, Condition.ConditionResult.FAILURE);
	}

	protected abstract Class<? extends Condition> postPaymentConsentValidator();

	protected abstract Class<? extends Condition> getConsentsValidator();

	protected abstract Class<? extends Condition> postConsentsAuthoriseValidator();

}
