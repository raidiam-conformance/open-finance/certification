package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.AbstractUnarrangedAccountsOverdraftApiResourcesTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3.GetUnarrangedAccountsOverdraftListV2n3OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "unarranged-accounts-overdraft_api_resources_test-module_V2-3",
	displayName = "Validate structure of unarranged overdraft API and Resources API resources V2-3",
	summary = "Makes sure that the Resource API and the API that is the scope of this test plan are returning the same available IDs\n" +
		"•Create a consent with all the permissions needed to access the tested API\n" +
		"• Expects server to create the consent with 201\n" +
		"• Redirect the user to authorize at the financial institution\n" +
		"• Call the tested resource API\n" +
		"• Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the id provided by this API\n" +
		"• Call the resources API\n" +
		"• Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)

public class UnarrangedAccountsOverdraftApiResourcesTestModuleV2n3 extends AbstractUnarrangedAccountsOverdraftApiResourcesTestModule {

	@Override
	protected Class<? extends Condition> apiValidator() {
		return GetUnarrangedAccountsOverdraftListV2n3OASValidator.class;
	}
}
