package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;

public class InsertBrazilAutomaticPaymentRecurringConsentProdValues extends AbstractCondition {

	private static final String TRANSACTION_LIMIT = "1000000000.00";
	private static final int QUANTITY_LIMIT = 300;
	protected static final String AMOUNT = "600.00";

    @Override
    public Environment evaluate(Environment env) {

		JsonArray creditors = new JsonArray();
		creditors.add(new JsonObjectBuilder()
			.addField("personType", DictHomologKeys.PROXY_PRODUCTION_PERSON_TYPE)
			.addField("cpfCnpj", DictHomologKeys.PROXY_PRODUCTION_CPF)
			.addField("name", DictHomologKeys.PROXY_PRODUCTION_NAME).build());

		JsonObjectBuilder consentRequestBuilder = new JsonObjectBuilder()
			.addFields(
				"data.loggedUser.document",
				Map.of("identification", DictHomologKeys.PROXY_PRODUCTION_CPF, "rel", "CPF")
			).addField("data.creditors", creditors)
			.addField("data.recurringConfiguration", this.buildRecurringConfiguration());

        String businessEntityIdentification = env.getString("config", "resource.brazilCnpj");
		if (businessEntityIdentification != null) {
			consentRequestBuilder.addFields(
				"data.businessEntity.document",
				Map.of("identification", businessEntityIdentification,"rel", "CNPJ")
			);
		}

        JsonObject consentRequest = consentRequestBuilder.build();
        env.putObject("config", "resource.brazilPaymentConsent", consentRequest);
        logSuccess("Consent request body was added to the config", args("consentRequest", consentRequest));

        return env;
    }

	protected JsonObject buildRecurringConfiguration() {
		return new JsonObjectBuilder()
			.addFields("sweeping", Map.of(
				"totalAllowedAmount", AMOUNT,
				"transactionLimit", TRANSACTION_LIMIT
			))
			.addFields("sweeping.periodicLimits.day", Map.of(
				"quantityLimit", QUANTITY_LIMIT,
				"transactionLimit", TRANSACTION_LIMIT
			))
			.addFields("sweeping.periodicLimits.week", Map.of(
				"quantityLimit", QUANTITY_LIMIT,
				"transactionLimit", TRANSACTION_LIMIT
			))
			.addFields("sweeping.periodicLimits.month", Map.of(
				"quantityLimit", QUANTITY_LIMIT,
				"transactionLimit", TRANSACTION_LIMIT
			))
			.addFields("sweeping.periodicLimits.year", Map.of(
				"quantityLimit", QUANTITY_LIMIT,
				"transactionLimit", TRANSACTION_LIMIT
			))
			.build();
	}
}
