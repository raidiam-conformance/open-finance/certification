package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetCreditCardAccountsBillsOASValidatorV2 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/creditCard/swagger-credit-cards-2.3.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/accounts/{creditCardAccountId}/bills";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
