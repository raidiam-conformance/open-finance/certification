package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetPaymentV2Endpoint extends GetPaymentVXEndpoint {
	@Override
	protected String getApiVersionRegex() {
		return "^(2.[0-9].[0-9])$";
	}
}
