package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;

public class CreatePatchConsentsRequestPayload extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	@PostEnvironment(required = "consent_endpoint_request")
	public Environment evaluate(Environment env) {
		String identification = OIDFJSON.getString(env.getElementFromObject("config", "resource.loggedUserIdentification"));
		JsonObject consentRequestBody = new JsonObjectBuilder()
			.addField("data.status", "CANC")
			.addFields("data.cancellation.cancelledBy.document", Map.of(
				"identification", identification,
				"rel", "CPF"
			))
			.build();
		env.putObject("consent_endpoint_request", consentRequestBody);
		logSuccess("Patch consents payload created", env.getObject("consent_endpoint_request"));
		return env;
	}
}
