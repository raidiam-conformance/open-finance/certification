package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddClientAssertionToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddClientIdToTokenEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CreateClientAuthenticationAssertionClaims;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.SignClientAuthenticationAssertion;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixConsentEditionPathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveCurrentInstant;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CheckIsMaximumVariableAmountPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureUpdatedAtDateTimeFieldWasUpdated;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractMaximumVariableAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetRecurringPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit.EnsureUseOverdraftLimitIsFalse;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit.EnsureUseOverdraftLimitIsTrue;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalWithoutFirstPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo1AboveMaximumVariableAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJWTAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixConsentEditionAccountHolderTestModule extends AbstractAutomaticPaymentsAutomaticPixConsentEditionPathTestModule {

	@Override
	protected boolean isHappyPath() {
		return false;
	}

	@Override
	protected AbstractCreatePatchRecurringConsentsForEditionRequestBody createPatchRequestCondition() {
		return null;
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalWithoutFirstPayment();
	}

	@Override
	protected void makePatchConsentsRequest() {
		callAndContinueOnFailure(EnsureUseOverdraftLimitIsTrue.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(SaveCurrentInstant.class);
		runGetRecurringConsentRepeatSequence();

		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
		runInBlock("Validate PATCH consent response", () -> {
			callAndContinueOnFailure(EnsureUseOverdraftLimitIsFalse.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureUpdatedAtDateTimeFieldWasUpdated.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(ExtractMaximumVariableAmount.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(SetAutomaticPaymentAmountTo1AboveMaximumVariableAmount.class);
		});
		userAuthorisationCodeAccessToken();
	}

	protected void runGetRecurringConsentRepeatSequence() {
		executeRepeatSequence();

		if (!env.getBoolean("max_var_amount_present")) {
			call(callTokenEndpointShortVersion());
			executeRepeatSequenceWithTimeout();
		}
	}

	protected void executeRepeatSequence() {
		repeatSequence(this::getRecurringConsentRepeatSequence)
			.untilTrue("max_var_amount_present")
			.trailingPause(30)
			.times(10)
			.validationSequence(this::getRecurringConsentValidationSequence)
			.run();
	}

	protected void executeRepeatSequenceWithTimeout() {
		repeatSequence(this::getRecurringConsentRepeatSequence)
			.untilTrue("max_var_amount_present")
			.trailingPause(30)
			.times(10)
			.validationSequence(this::getRecurringConsentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();
	}

	protected ConditionSequence getRecurringConsentRepeatSequence() {
		return new ValidateSelfEndpoint()
			.replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeaderRequest.class),
				condition(CallProtectedResource.class)
			))
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values")
			.insertAfter(EnsureResourceResponseCodeWas200.class, condition(CheckIsMaximumVariableAmountPresent.class));
	}

	protected ConditionSequence getRecurringConsentValidationSequence() {
		return sequenceOf(
			condition(getPaymentConsentValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
	}

	protected ConditionSequence callTokenEndpointShortVersion() {
		return sequenceOf(
			condition(CreateTokenEndpointRequestForClientCredentialsGrant.class),
			condition(SetRecurringPaymentsScopeOnTokenEndpointRequest.class),
			condition(AddClientIdToTokenEndpointRequest.class),
			condition(CreateClientAuthenticationAssertionClaims.class).dontStopOnFailure(),
			condition(SignClientAuthenticationAssertion.class).dontStopOnFailure(),
			condition(AddClientAssertionToTokenEndpointRequest.class).dontStopOnFailure(),
			condition(CallTokenEndpoint.class),
			condition(CheckIfTokenEndpointResponseError.class),
			condition(ExtractAccessTokenFromTokenResponse.class),
			condition(SaveAccessToken.class)
		);
	}
}
