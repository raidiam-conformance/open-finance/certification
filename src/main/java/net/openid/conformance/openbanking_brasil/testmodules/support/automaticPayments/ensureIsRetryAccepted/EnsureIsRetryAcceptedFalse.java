package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted;

public class EnsureIsRetryAcceptedFalse extends AbstractEnsureIsRetryAccepted {

	@Override
	protected boolean isRetryAcceptedExpectedValue() {
		return false;
	}
}
