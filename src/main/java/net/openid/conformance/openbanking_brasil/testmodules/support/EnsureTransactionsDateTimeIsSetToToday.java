package net.openid.conformance.openbanking_brasil.testmodules.support;


import java.time.LocalDate;

public class EnsureTransactionsDateTimeIsSetToToday extends ValidateTransactionsDateTime {

	@Override
	protected boolean isDateInvalid(LocalDate currentDate, LocalDate transactionDate) {

			boolean isSameDay = currentDate.getYear() == transactionDate.getYear() &&
				currentDate.getMonth() == transactionDate.getMonth() &&
				currentDate.getDayOfMonth() == transactionDate.getDayOfMonth();

			return !isSameDay;

	}

	@Override
	protected String getErrorMessage() {
		return "The dates of the transactions are not today's date";
	}
}
