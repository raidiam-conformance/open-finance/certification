package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.extensions.SpringContext;
import net.openid.conformance.extensions.yacs.ParticipantsService;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes extends AbstractCondition {

	private final static List<String> SCOPES_WITHOUT_API_FAMILY_TYPES = List.of("openid");
	private final static List<String> MANDATORY_SCOPES = List.of(
		"invoice-financings",
		"financings",
		"loans",
		"unarranged-accounts-overdraft",
		"bank-fixed-incomes",
		"credit-fixed-incomes",
		"variable-incomes",
		"treasure-titles",
		"funds",
		"exchanges"
	);

	private boolean conditionFailure;
	private Set<String> errorHashSet = new HashSet<>();


	@Override
	@PreEnvironment(required = {"server", "authorisation_server"})
	public Environment evaluate(Environment env) {
		conditionFailure = false;

		JsonElement scopesSupportedJson = Optional.ofNullable(
			env.getElementFromObject("server", "scopes_supported")
		).orElseThrow(() -> error("scopes_supported field missing from server response"));

		List<String> supportedScopesFromServer = new Gson()
			.fromJson(scopesSupportedJson, new TypeToken<List<String>>() {
			}.getType());

		log("All the scopes supported have been extracted from the well-known",
			args("scopes_supported", supportedScopesFromServer));


		JsonArray apiResources = Optional.ofNullable(
			env.getElementFromObject("authorisation_server", "ApiResources")
		).orElseThrow(() -> error("ApiResources field missing from authorisation server")).getAsJsonArray();

		List<String> distinctApiFamilyTypesFromServer = StreamSupport.stream(apiResources.spliterator(), false)
			.map(JsonElement::getAsJsonObject)
			.map(resource -> resource.get("ApiFamilyType"))
			.map(OIDFJSON::getString)
			.distinct()
			.collect(Collectors.toList());


		log("All the API family types have been extracted from the authorisation server",
			args("API family types", distinctApiFamilyTypesFromServer));

		ParticipantsService participantsService = getParticipantsService();
		String organisationId = getStringFromEnvironment(env, "config", "resource.brazilOrganizationId", "Config brazilOrganizationId Field");
		Set<Map> orgs = participantsService.orgsFor(Set.of(organisationId));
		List<String> roles = new ArrayList<>();
		for (Map org : orgs) {
			List<Map> orgDomainRoleClaims = (List<Map>) org.get("OrgDomainRoleClaims");
			for (Map map : orgDomainRoleClaims) {
				String role = (String) map.get("Role");
				roles.add(role);
			}
		}

		validateMandatoryScopesArePresent(supportedScopesFromServer, roles);
		validateApiFamiliesCorrespondToScopes(supportedScopesFromServer, distinctApiFamilyTypesFromServer);
		validateScopesCorrespondToApiFamilies(supportedScopesFromServer, distinctApiFamilyTypesFromServer);
		if(conditionFailure){
			Iterator<String> i = errorHashSet.iterator();
			String errorString = "";
			while(i.hasNext()){
				errorString += i.next() + "\n";
			}
			throw error(errorString, args("scopes_supported",supportedScopesFromServer,"API family types", distinctApiFamilyTypesFromServer));
		}
		logSuccess("All scopes supported from well-known and API Family Types from auth server correspond");

		return env;
	}

	private void validateScopesCorrespondToApiFamilies(List<String> supportedScopesFromServer, List<String> distinctApiFamilyTypesFromServer) {
		List<String> validApiFamilyTypes = distinctApiFamilyTypesFromServer.stream()
			.filter(apiFamilyType -> {
				String expectedScope = getScopeForFamilyType(apiFamilyType);
				return expectedScope != null && !expectedScope.isEmpty();
			})
			.toList();

		for (String apiFamilyType : validApiFamilyTypes) {
			String expectedScope = getScopeForFamilyType(apiFamilyType);

			if (!supportedScopesFromServer.contains(expectedScope)) {
				conditionFailure = true;
				errorHashSet.add("The scopes supported from well-known do not have a scope corresponding to a family type from the auth server");
				logFailure("The scopes supported from well-known do not have a scope corresponding to a family type from the auth server",
					args("API family type", apiFamilyType, "scopes supported", supportedScopesFromServer));
			}
		}
	}

	private void validateApiFamiliesCorrespondToScopes(List<String> supportedScopesFromServer, List<String> distinctApiFamilyTypesFromServer) {
		for (String scope : supportedScopesFromServer) {
			// Mandatory scopes must be defined even if AS does not support a corresponding API.
			// So we don't validate them
			if (SCOPES_WITHOUT_API_FAMILY_TYPES.contains(scope) || MANDATORY_SCOPES.contains(scope)) {
				continue;
			}

			FamilyTypeRule familyTypeRule = getScopeToFamilyTypeMap().get(scope);
			if (familyTypeRule == null) {
				log("skipping api family validation for scope", args("scope", scope));
				continue;
			}

			List<String> missingExpectedFamilyTypes = familyTypeRule.validate(distinctApiFamilyTypesFromServer);


			if (!missingExpectedFamilyTypes.isEmpty()) {
				conditionFailure = true;
				errorHashSet.add("The API family types from auth server do not have a family type corresponding to a scope from well-known");
				logFailure("The API family types from auth server do not have a family type corresponding to a scope from well-known",
					args("Missing Family Types", missingExpectedFamilyTypes, "API family types", distinctApiFamilyTypesFromServer));
			}
		}
	}


	private void validateMandatoryScopesArePresent(List<String> scopesFromServer, List<String> roles) {
		if (roles.contains("DADOS")) {
			List<String> missingMandatoryScopes = MANDATORY_SCOPES.stream()
				.filter(scope -> !scopesFromServer.contains(scope))
				.collect(Collectors.toList());

			if (!missingMandatoryScopes.isEmpty()) {
				conditionFailure = true;
				errorHashSet.add("Authorisation Server is missing mandatory scopes");
				logFailure("Authorisation Server is missing mandatory scopes",
					args("scopes", scopesFromServer, "mandatory_scopes", missingMandatoryScopes));
			}
		}
	}

	private Map<String, FamilyTypeRule> getScopeToFamilyTypeMap() {
		Map<String, FamilyTypeRule> scopeToFamilyTypeMap = new HashMap<>();

		/* Phase 2 */
		scopeToFamilyTypeMap.put("consents", new FamilyTypeRule("consents"));
		scopeToFamilyTypeMap.put("resources", new FamilyTypeRule("resources"));

		scopeToFamilyTypeMap.put("customers", new FamilyTypeRule()
			.add(List.of("customers-personal"))
			.add(List.of("customers-business")));

		scopeToFamilyTypeMap.put("credit-cards-accounts", new FamilyTypeRule("credit-cards-accounts"));
		scopeToFamilyTypeMap.put("accounts", new FamilyTypeRule("accounts"));
		scopeToFamilyTypeMap.put("loans", new FamilyTypeRule("loans"));
		scopeToFamilyTypeMap.put("financings", new FamilyTypeRule("financings"));
		scopeToFamilyTypeMap.put("unarranged-accounts-overdraft", new FamilyTypeRule("unarranged-accounts-overdraft"));
		scopeToFamilyTypeMap.put("invoice-financings", new FamilyTypeRule("invoice-financings"));

		/* Phase 3 */
		scopeToFamilyTypeMap.put("payments", new FamilyTypeRule(List.of(
			"payments-consents",
			"payments-pix"
		)));

		scopeToFamilyTypeMap.put("recurring-payments", new FamilyTypeRule()
			.add(List.of("payments-recurring-consents-automatic-personal", "payments-pix-recurring-payments-automatic-personal"))
			.add(List.of("payments-recurring-consents-automatic-business", "payments-pix-recurring-payments-automatic-business"))
			.add(List.of("payments-recurring-consents", "payments-pix-recurring-payments")));

		scopeToFamilyTypeMap.put("nrp-consents", new FamilyTypeRule("enrollments"));

		/* Phase 4B */
		scopeToFamilyTypeMap.put("bank-fixed-incomes", new FamilyTypeRule("bank-fixed-incomes"));
		scopeToFamilyTypeMap.put("credit-fixed-incomes", new FamilyTypeRule("credit-fixed-incomes"));
		scopeToFamilyTypeMap.put("variable-incomes", new FamilyTypeRule("variable-incomes"));
		scopeToFamilyTypeMap.put("treasure-titles", new FamilyTypeRule("treasure-titles"));
		scopeToFamilyTypeMap.put("funds", new FamilyTypeRule("funds"));

		return scopeToFamilyTypeMap;
	}

	private String getScopeForFamilyType(String familyType) {
		for (Map.Entry<String, FamilyTypeRule> entry : getScopeToFamilyTypeMap().entrySet()) {
			if (entry.getValue().contains(familyType)) {
				return entry.getKey();
			}
		}
		// API Family types that are not mapped
		return null;
	}

	private ParticipantsService getParticipantsService() {
		return SpringContext.getBean(ParticipantsService.class);
	}


	/**
	 * The FamilyTypeRule class represents a set of rules for determining whether family types returned from the server are complete.
	 * Each rule has the following structure -
	 * Outer list represents non-mutually-exclusive "OR" conditions and inner list represents "AND" conditions.
	 * Example:
	 * -----
	 * 			List.of(
	 * 				List.of("type1", "type2"),
	 * 				List.of("type3", "type4")
	 * 			);
	 * This rule tells that AS must have either (type1 AMD type2) OR (type3 AND type4) OR both
	 * -----
	 * new FamilyTypeRule("discovery")
	 * This rule tells that AS must have discovery family type
	 * -----
	 * new FamilyTypeRule(List.of(
	 * 			"opendata-exchange_online-rates",
	 * 			"opendata-exchange_vet-values"
	 * 		))
	 * This rule tells that AS must have opendata-exchange_online-rates AND opendata-exchange_vet-values family types
	 * -----
	 * new FamilyTypeRule()
	 * 			.add(List.of("payments-recurring-consents-automatic-personal", "payments-pix-recurring-payments-automatic-personal"))
	 * 			.add(List.of("payments-recurring-consents-automatic-business", "payments-pix-recurring-payments-automatic-business"))
	 * 			.add(List.of("payments-recurring-consents", "payments-pix-recurring-payments"))
	 * This rule tells that AS must have
	 * 	(payments-recurring-consents-automatic-personal AND payments-pix-recurring-payments-automatic-personal) OR
	 * 	(payments-recurring-consents-automatic-business AND payments-pix-recurring-payments-automatic-business) OR
	 * 	(payments-recurring-consents AND payments-pix-recurring-payments)
	 *
	 */
	public static class FamilyTypeRule {

		private final List<List<String>> familyTypeRules;

		public FamilyTypeRule() {
			this.familyTypeRules = new ArrayList<>();
		}

		public FamilyTypeRule(List<String> familyTypeRules) {
			this.familyTypeRules = List.of(familyTypeRules);
		}

		public FamilyTypeRule(String familyType) {
			this.familyTypeRules = List.of(List.of(familyType));
		}


		public boolean contains(String familyType) {

			return this.familyTypeRules.stream()
				.anyMatch(types -> types.contains(familyType));

		}

		public FamilyTypeRule add(List<String> familyTypes) {
			this.familyTypeRules.add(familyTypes);
			return this;
		}

		@Override
		public String toString() {
			return this.familyTypeRules.stream()
				.map(rules -> rules.stream().collect(Collectors.joining(", ", "[", "]")))
				.collect(Collectors.joining(" OR "));
		}

		public List<String> validate(List<String> familyTypesFromServer) {
			List<String> missingExpectedFamilyTypes = new ArrayList<>();
			boolean partialMatch = false;

			for (List<String> rules : familyTypeRules) {
				if (familyTypesFromServer.stream().anyMatch(rules::contains)) {
					List<String> collect = rules.stream()
						.filter(familyType -> !familyTypesFromServer.contains(familyType))
						.toList();

					missingExpectedFamilyTypes.addAll(collect);
					partialMatch = true;
				}

			}

			if (!partialMatch) {
				missingExpectedFamilyTypes.add(toString());
			}

			return missingExpectedFamilyTypes;
		}
	}


}
