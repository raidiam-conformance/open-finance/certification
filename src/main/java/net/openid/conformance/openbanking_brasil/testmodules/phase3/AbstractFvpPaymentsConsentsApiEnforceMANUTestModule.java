package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveProxyFromConsentConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectMANUCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectQRCodeWithRealEmailIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilPaymentConsentProdValues;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractFvpPaymentsConsentsApiEnforceMANUTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilPaymentConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		eventLog.startBlock("Validate payment initiation consent");
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

		call(getPaymentsConsentSequence());
		callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);

		eventLog.endBlock();

		eventLog.startBlock("Setting payload proxy as e-mail with no QRCode");
		callAndStopOnFailure(EnsureProxyPresentInConfig.class);
		eventLog.endBlock();

		eventLog.startBlock("Validate payment initiation consent");
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

		call(getPaymentsConsentSequence());
		callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);

		eventLog.endBlock();
	}

	@Override
	protected void configureDummyData() {
		super.configureDummyData();
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndContinueOnFailure(SelectMANUCodeLocalInstrument.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(InjectQRCodeWithRealEmailIntoConfig.class);
		callAndStopOnFailure(RemoveProxyFromConsentConfig.class);
	}

	@Override
	protected ConditionSequence getPaymentsConsentSequence() {
		return super.getPaymentsConsentSequence()
			.replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas422.class).onFail(Condition.ConditionResult.FAILURE));
	}
}
