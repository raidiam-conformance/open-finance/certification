package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public class RemoveProxyFromPaymentConfig extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		log("Removing proxy field from payment config");
		JsonObject obj = env.getObject("resource");
		obj = obj.getAsJsonObject("brazilPixPayment");
		JsonElement paymentData = obj.get("data");

		if (paymentData.isJsonArray()) {
			for (int i = 0; i < paymentData.getAsJsonArray().size(); i++) {
				JsonObject payment = paymentData.getAsJsonArray().get(i).getAsJsonObject();
				payment.remove("proxy");
			}
			logSuccess("set proxy in config to be unpopulated");
			return env;
		}

		paymentData.getAsJsonObject().remove("proxy");
		logSuccess("set proxy in config to be unpopulated");
		return env;
	}

}
