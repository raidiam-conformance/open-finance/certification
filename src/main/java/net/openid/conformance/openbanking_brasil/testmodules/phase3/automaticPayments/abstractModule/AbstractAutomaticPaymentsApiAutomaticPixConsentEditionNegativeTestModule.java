package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixConsentEditionPathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRejectionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyExpirationTomorrowNoAmountNoRiskSignals;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyExpirationYesterdayNoAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount30Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationNoAmountNoLoggedUser;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalMinVarAmount050WithoutFirstPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasCampoNaoPermitido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasDetalheEdicaoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasFaltamSinaisObrigatoriosPlataforma;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixConsentEditionNegativeTestModule extends AbstractAutomaticPaymentsAutomaticPixConsentEditionPathTestModule {

	@Override
	protected boolean isHappyPath() {
		return false;
	}

	@Override
	protected AbstractCreatePatchRecurringConsentsForEditionRequestBody createPatchRequestCondition() {
		return null;
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalMinVarAmount050WithoutFirstPayment();
	}

	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
		env.unmapKey("access_token");
		validateGetConsentResponse();
		makePatchConsentsRequest();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected void makePatchConsentsRequest() {
		runPatchConsentRequestExpectingFailure(
			new CreatePatchRecurringConsentsForEditionRequestBodyExpirationYesterdayNoAmount(),
			new EnsureErrorResponseCodeFieldWasDetalheEdicaoInvalido(),
			"sending expirationDateTime as D-1 - Expects 422 DETALHE_EDICAO_INVALIDO"
		);
		runPatchConsentRequestExpectingFailure(
			new CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount30Cents(),
			new EnsureErrorResponseCodeFieldWasCampoNaoPermitido(),
			"sending maximumVariableAmount as 0.30 BRL - Expects 422 CAMPO_NAO_PERMITIDO"
		);
		runPatchConsentRequestExpectingFailure(
			new CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationNoAmountNoLoggedUser(),
			new EnsureErrorResponseCodeFieldWasCampoNaoPermitido(),
			"without sending loggedUser - Expects 422 CAMPO_NAO_PERMITIDO"
		);
		runPatchConsentRequestExpectingFailure(
			new CreatePatchRecurringConsentsForEditionRequestBodyExpirationTomorrowNoAmountNoRiskSignals(),
			new EnsureErrorResponseCodeFieldWasFaltamSinaisObrigatoriosPlataforma(),
			"without sending riskSignals - Expects 422 FALTAM_SINAIS_OBRIGATORIOS_PLATAFORMA"
		);
	}

	protected void runPatchConsentRequestExpectingFailure(
		AbstractCreatePatchRecurringConsentsForEditionRequestBody createRequestBodyCondition,
		AbstractEnsureErrorResponseCodeFieldWas errorResponseCodeCondition,
		String headerText
	) {
		runInBlock(String.format("Call PATCH consents %s", headerText), () -> {
			call(new CallPatchAutomaticPaymentsConsentsEndpointSequence()
				.replace(CreatePatchRecurringPaymentsConsentsForRejectionRequestBody.class,
					condition(createRequestBodyCondition.getClass()))
				.replace(EnsureConsentResponseCodeWas200.class, condition(EnsureConsentResponseCodeWas422.class))
			);
			callAndContinueOnFailure(patchPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndContinueOnFailure(errorResponseCodeCondition.getClass(), Condition.ConditionResult.FAILURE);
			env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		});
	}
}
