package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.openinsurance.testmodule.support.AbstractPageSizeCondition;

public class SetProtectedResourceUrlPageSize25 extends AbstractPageSizeCondition {

	@Override
	protected int getPageSize() {
		return 25;
	}
}

