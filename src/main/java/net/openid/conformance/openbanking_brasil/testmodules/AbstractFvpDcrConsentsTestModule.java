package net.openid.conformance.openbanking_brasil.testmodules;

import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.addExpirationToConsentRequest.AddExpirationPlus30DaysToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractConsentIdFromResourceEndpointResponseFull;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.Validate529Or201ReturnedFromConsents;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractFvpDcrConsentsTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends AbstractJsonAssertingCondition> postConsentValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetConsentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {}

	@Override
	protected void runTests() {
		eventLog.startBlock("Calling Consents API");
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS).build();
		ConditionSequenceRepeater sequenceRepeater = new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager, this::consentsApiSequence)
			.untilTrue("correct_consent_response")
			.times(3)
			.trailingPause(10)
			.onTimeout(sequenceOf(condition(EnsureConsentResponseCodeWas201.class).onFail(Condition.ConditionResult.FAILURE)));
		sequenceRepeater.run();
		call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
		callAndContinueOnFailure(postConsentValidator(), Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(ExtractConsentIdFromResourceEndpointResponseFull.class);
		deleteConsent();
		callAndStopOnFailure(EnsureResourceResponseCodeWas204.class);
	}

	protected ConditionSequence consentsApiSequence() {
		return sequenceOf(
			condition(PrepareToPostConsentRequest.class),
			condition(GetResourceEndpointConfiguration.class),
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(FAPIBrazilOpenBankingCreateConsentRequest.class),
			condition(AddExpirationPlus30DaysToConsentRequest.class),
			condition(SetContentTypeApplicationJson.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(Validate529Or201ReturnedFromConsents.class).onFail(Condition.ConditionResult.FAILURE)
		);
	}

	public void deleteConsent() {
		eventLog.startBlock("Deleting consent");
		callAndContinueOnFailure(PrepareToFetchConsentRequest.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
	}
}
