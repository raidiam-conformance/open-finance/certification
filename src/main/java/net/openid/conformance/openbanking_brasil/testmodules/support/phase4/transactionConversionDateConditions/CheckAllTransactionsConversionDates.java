package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions;

import com.google.common.base.Strings;
import com.google.gson.*;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonUtils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class CheckAllTransactionsConversionDates extends AbstractCondition {

    protected final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    protected final LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

    @Override
    public Environment evaluate(Environment env) {
        String transactionConversionDate = Optional.ofNullable(env.getString("transactionConversionDate")).orElse(currentDate.format(FORMATTER));
        String bodyJsonString = env.getString("resource_endpoint_response_full", "body");
        if (Strings.isNullOrEmpty(bodyJsonString)) {
            throw error("Body element is missing in the resource_endpoint_response_full");
        }
        JsonObject body;
        try {
            Gson gson = JsonUtils.createBigDecimalAwareGson();
            body = gson.fromJson(bodyJsonString, JsonObject.class);
        } catch (JsonSyntaxException e) {
            throw error("Body is not json", args("body", bodyJsonString));
        }

        JsonArray transactions = body.getAsJsonArray("data");
        if (transactions == null || transactions.isEmpty()) {
            if (transactionConversionDate.equals(currentDate.format(FORMATTER))) {
                logSuccess("No transaction has been done in the current date");
                return env;
            }
            throw error("No transactions returned unable to validate the defined behaviour with booking date query parameters",
                args("response", env.getObject("resource_endpoint_response_full"),
                    "body", body,
                    "data", transactions));
        }

        for (JsonElement transactionElement : transactions) {
            JsonObject transaction = transactionElement.getAsJsonObject();
            String date = OIDFJSON.getString(transaction.get("transactionConversionDate"));
            if (!date.equals(transactionConversionDate)) {
                throw error("transactionConversionDate does not match the expected date",
                    args("transactionConversionDate found", date, "expected transactionConversionDate", transactionConversionDate));
            }
        }

        logSuccess("Every transaction matched the expected transactionConversionDate");
        return env;
    }
}
