package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount;

public class SetAutomaticPaymentAmountTo300 extends AbstractSetAutomaticPaymentAmount {

	@Override
	protected String automaticPaymentAmount() {
		return "300.00";
	}
}
