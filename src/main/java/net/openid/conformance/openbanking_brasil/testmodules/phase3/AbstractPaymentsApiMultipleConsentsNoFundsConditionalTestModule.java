package net.openid.conformance.openbanking_brasil.testmodules.phase3;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckMatchingCallbackParameters;
import net.openid.conformance.condition.client.CheckStateInAuthorizationResponse;
import net.openid.conformance.condition.client.RejectAuthCodeInAuthorizationEndpointResponse;
import net.openid.conformance.condition.client.RejectStateInUrlQueryForHybridFlow;
import net.openid.conformance.condition.client.ValidateIssIfPresentInAuthorizationResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforceAbsenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePaymentsJointAccountCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToOutOfRangeOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.EnsureExpirationDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;

public abstract class AbstractPaymentsApiMultipleConsentsNoFundsConditionalTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	protected abstract Class<? extends Condition> getEnsureConsentsRejectionReasonCode();

	@Override
	protected void requestProtectedResource() {
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		//Not needed for this test
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		//Not needed for this test
		return null;
	}

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsurePaymentsJointAccountCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no 'Payment consent - Logged User CPF - Multiple Consents Test' was informed.");
		}
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
		callAndStopOnFailure(SetPaymentAmountToOutOfRangeOnConsent.class);
	}

	@Override
	protected void onAuthorizationCallbackResponse() {

		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);

		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");

		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");

		// as https://tools.ietf.org/html/draft-ietf-oauth-iss-auth-resp is still a draft we only warn if the value is wrong,
		// and do not require it to be present.
		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.WARNING, "OAuth2-iss-2");

		callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);

		callAndStopOnFailure(RejectAuthCodeInAuthorizationEndpointResponse.class);

		performPostAuthorizationFlow();
	}

	@Override
	protected void performPostAuthorizationFlow() {

		callAndStopOnFailure(LoadOldAccessToken.class);
		fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
		callAndStopOnFailure(getEnsureConsentsRejectionReasonCode());
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureExpirationDateTime.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("resource_endpoint_response_full");
		fireTestFinished();
	}
}
