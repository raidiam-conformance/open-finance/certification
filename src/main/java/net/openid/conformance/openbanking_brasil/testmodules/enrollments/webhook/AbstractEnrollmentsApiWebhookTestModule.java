package net.openid.conformance.openbanking_brasil.testmodules.enrollments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddClientCredentialsGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddImplicitGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddRedirectUriToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddRefreshTokenGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddTlsClientAuthSubjectDnToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateEmptyDynamicRegistrationRequest;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractClientNameFromStoredConfig;
import net.openid.conformance.condition.client.ExtractJWKSDirectFromClientConfiguration;
import net.openid.conformance.condition.client.ExtractMTLSCertificatesFromConfiguration;
import net.openid.conformance.condition.client.FAPIBrazilCallPaymentConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.SetResponseTypeCodeIdTokenInDynamicRegistrationRequest;
import net.openid.conformance.condition.client.SetResponseTypeCodeInDynamicRegistrationRequest;
import net.openid.conformance.condition.client.StoreOriginalClientConfiguration;
import net.openid.conformance.condition.client.ValidateMTLSCertificatesHeader;
import net.openid.conformance.condition.common.CheckDistinctKeyIdValueInClientJWKs;
import net.openid.conformance.fapi1advancedfinal.AbstractFAPI1AdvancedFinalBrazilDCR;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckIfLinksIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateSelfLinkRegex;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.CallEnrollmentsEndpointWithBearerToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.PreparePermissionsForEnrollmentsTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ValidateEnrollmentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.WebhookWithEnrollmentIdV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreatePatchEnrollmentsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateRiskSignalsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.AbstractEnsureEnrollmentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasAwaitingAccountHolderValidation;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasAwaitingEnrollment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasAwaitingRiskSignals;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToFetchEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPatchEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostRiskSignals;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJWTAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AddWebhookUrisToDynamicRegistrationRequest;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureSoftwareStatementContainsWebhook;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.WaitForSpecifiedTimeFromConfigInSeconds;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

public abstract class AbstractEnrollmentsApiWebhookTestModule extends AbstractFAPI1AdvancedFinalBrazilDCR {

	@Override
	public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
		Boolean expectingEnrollmentWebhook = env.getBoolean("enrollment_webhook_received");
		String pattern = "^open-banking/webhook/v\\d+/enrollments/v\\d+/enrollments/urn:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%/?#]+$";
		boolean isMatch = Pattern.matches(pattern, path);

		if (expectingEnrollmentWebhook != null && isMatch && expectingEnrollmentWebhook) {
			JsonObject webhooksReceived = env.getObject("webhooks_received_enrollment");
			JsonArray webhooks = webhooksReceived.get("webhooks").getAsJsonArray();
			requestParts.addProperty("time_received", Instant.now().toString());
			requestParts.addProperty("type", "consent");
			requestParts.addProperty("path", path);
			webhooks.add(requestParts);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else {
			throw new TestFailureException(getId(), "Got a webhook response we weren't expecting");
		}
	}

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(getConsentAndResourceEndpointSequence()));
		callAndStopOnFailure(PreparePermissionsForEnrollmentsTest.class);
		env.putString("config", "client.scope", "openid payments nrp-consents");
		doDcr();
		eventLog.startBlock("Waiting for webhook to be synced");
		callAndStopOnFailure(WaitForSpecifiedTimeFromConfigInSeconds.class);
		eventLog.endBlock();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.PAYMENTS, OPFScopesEnum.NRP_CONSENTS, OPFScopesEnum.OPEN_ID).build();
		JsonObject webhooksReceived = new JsonObject();
		JsonArray webhooks = new JsonArray();
		webhooksReceived.add("webhooks", webhooks);
		env.putObject("webhooks_received_enrollment", webhooksReceived);
		callAndStopOnFailure(CreateEnrollmentsRequestBodyToRequestEntityClaims.class);
		callAndStopOnFailure(PrepareToPostEnrollments.class);
		super.onConfigure(config, baseUrl);
	}

	protected void runInBlock(String blockText, Runnable actor) {
		eventLog.startBlock(blockText);
		actor.run();
		eventLog.endBlock();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		enableEnrollmentWebhook();
		createEnrollmentWebhook();
		postAndValidateEnrollments();
		postAndValidateRiskSignals();
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasAwaitingAccountHolderValidation(),
			"AWAITING_ACCOUNT_HOLDER_VALIDATION");
	}

	protected void enableEnrollmentWebhook(){
		env.putString("time_of_enrollment_request", Instant.now().toString());
		env.putBoolean("enrollment_webhook_received", true);
	}

	protected void createEnrollmentWebhook() {
		callAndStopOnFailure(WebhookWithEnrollmentIdV1.class);
		exposeEnvString("webhook_uri_enrollment_id");
	}

	protected void waitForWebhookResponse() {
		long delaySeconds = 5;
		eventLog.startBlock(currentClientString() + "The test will now wait for the webhook to be received");
		for (int attempts = 0; attempts < 12; attempts++) {
			eventLog.log(getId(), "Waiting for webhook to be received at with an interval of " + delaySeconds + " seconds");
			setStatus(Status.WAITING);
			try {
				Thread.sleep(delaySeconds * 1000);
			} catch (InterruptedException e) {
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}
			setStatus(Status.RUNNING);

		}
		eventLog.endBlock();
	}

	protected void validateEnrollmentWebhooks(){
		env.putBoolean("enrollment_webhook_received", false);
		callAndStopOnFailure(ValidateEnrollmentWebhooks.class);
	}

	protected void postAndValidateEnrollments() {
		call(createPostEnrollmentsSteps());
		callAndStopOnFailure(SaveAccessToken.class);
		runInBlock("Validate enrollments response", () -> {
			validateResourceResponse(postEnrollmentsValidator());
			callAndContinueOnFailure(EnsureEnrollmentStatusWasAwaitingRiskSignals.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void postAndValidateRiskSignals() {
		runInBlock("Post risk-signals - Expects 204", () -> {
			call(createPostRiskSignalsSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void getAndValidateEnrollmentsStatus(AbstractEnsureEnrollmentStatusWas ensureEnrollmentStatusWas, String statusName) {
		runInBlock("Get enrollment - Expects 200 " + statusName, () -> {
			fetchEnrollmentToCheckStatus(ensureEnrollmentStatusWas);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			validateResourceResponse(getEnrollmentsValidator());
		});
	}

	protected void patchAndValidateEnrollments() {
		runInBlock("Call PATCH enrollments - Expects 204", () -> {
			callAndStopOnFailure(prepareReason());
			PostEnrollmentsResourceSteps patchEnrollmentsSteps = new PostEnrollmentsResourceSteps(
				new PrepareToPatchEnrollments(),
				CreatePatchEnrollmentsRequestBodyToRequestEntityClaims.class,
				true
			);
			call(patchEnrollmentsSteps);
			callAndStopOnFailure(EnsureResourceResponseCodeWas204.class);
		});
	}
	protected abstract Class<? extends Condition> prepareReason();

	protected PostEnrollmentsSteps createPostEnrollmentsSteps() {
		return new PostEnrollmentsSteps(addTokenEndpointClientAuthentication);
	}

	protected PostEnrollmentsResourceSteps createPostRiskSignalsSteps() {
		return new PostEnrollmentsResourceSteps(new PrepareToPostRiskSignals(),
			CreateRiskSignalsRequestBodyToRequestEntityClaims.class,
			true);
	}

	@Override
	protected void performPostAuthorizationFlow() {
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasAwaitingEnrollment(), "AWAITING_ENROLLMENT");

		userAuthorisationCodeAccessToken();
		eventLog.startBlock(currentClientString() + "Call token endpoint");
		createAuthorizationCodeRequest();
		requestAuthorizationCode();
		requestProtectedResource();
		onPostAuthorizationFlowComplete();
	}

	protected void fetchEnrollmentToCheckStatus(AbstractEnsureEnrollmentStatusWas ensureEnrollmentStatusWas) {
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchEnrollmentsRequest.class);
		callAndContinueOnFailure(CallEnrollmentsEndpointWithBearerToken.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(ensureEnrollmentStatusWas.getClass(), Condition.ConditionResult.FAILURE);
	}

	protected void validateResourceResponse(Class<? extends Condition> validator) {
		callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void requestProtectedResource() {
		executeTestSteps();
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		forceGetCallToUseClientCredentialsToken(builder);

		super.call(builder);

		if (CallProtectedResource.class.equals(builder.getConditionClass()) ||
			CallEnrollmentsEndpointWithBearerToken.class.equals(builder.getConditionClass())) {
			validateSelfLink("resource_endpoint_response_full");
		}

		if (FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
			FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class.equals(builder.getConditionClass()) ||
			FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders.class.equals(builder.getConditionClass())) {
			validateSelfLink("consent_endpoint_response_full");
		}
	}

	protected void forceGetCallToUseClientCredentialsToken(ConditionCallBuilder builder) {
		if (CallProtectedResource.class.isAssignableFrom(builder.getConditionClass())) {

			String resourceMethod = "";
			if(CallProtectedResource.class.equals(builder.getConditionClass())){
				resourceMethod = OIDFJSON.getString(Optional.ofNullable(
					env.getElementFromObject("resource", "resourceMethod")).orElse(new JsonPrimitive("")));
			}
			if(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class.equals(builder.getConditionClass()) ||
				CallEnrollmentsEndpointWithBearerToken.class.equals(builder.getConditionClass())) {
				resourceMethod = Optional.ofNullable(env.getString("http_method")).orElse("");
			}

			if (Objects.equals(resourceMethod, "GET") && env.containsObject("old_access_token")) {
				eventLog.log("Load client_credential access token", env.getObject("old_access_token"));
				useClientCredentialsAccessToken();
			}
		}
	}

	private boolean isEndpointCallSuccessful(int status) {
		return status == org.apache.http.HttpStatus.SC_CREATED || status == org.apache.http.HttpStatus.SC_OK;
	}

	protected void validateSelfLink(String responseFull) {

		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
			.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));

		String expectsFailure = Optional.ofNullable(env.getString("expects_failure")).orElse("false");

		if (!recursion.equals("true") && isEndpointCallSuccessful(status) && !expectsFailure.equals("true")) {

			env.putString("recursion", "true");
			call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
			env.mapKey("resource_endpoint_response_full", responseFull);

			callAndStopOnFailure(CheckIfLinksIsPresent.class);
			if (!env.getBoolean("is_links_present")) {
				return;
			}

			ConditionSequence sequence = new ValidateSelfEndpoint().replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeaderRequest.class),
				condition(CallProtectedResource.class)
			));

			if (responseFull.contains("consent")) {
				sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));

				env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			} else {
				env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			}
			callAndContinueOnFailure(ValidateSelfLinkRegex.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY);

			call(sequence);
			env.unmapKey("resource_endpoint_response_full");
			env.putString("recursion", "false");
		}
		env.removeNativeValue("expects_failure");
	}

	@Override
	protected void getSsa() {
		super.getSsa();
		validateWebhookOnSoftwareStatement();
	}

	protected void validateWebhookOnSoftwareStatement(){
		callAndStopOnFailure(EnsureSoftwareStatementContainsWebhook.class);
	}
	@Override
	protected void logFinalEnv(){

	}

	protected void doDcr(){
		ClientAuthType clientAuthType = getVariant(ClientAuthType.class);
		String clientAuth = clientAuthType.toString();
		if (clientAuthType == ClientAuthType.MTLS) {
			// the FAPI auth type variant doesn't use the name required for the registration request as per
			// https://datatracker.ietf.org/doc/html/rfc8705#section-2.1.1
			clientAuth = "tls_client_auth";
		}
		env.putString("client_auth_type", clientAuth);

		callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
		callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);

		// normally our DCR tests create a key on the fly to use, but in this case the key has to be registered
		// manually with the central directory so we must use user supplied keys
		callAndStopOnFailure(ExtractJWKSDirectFromClientConfiguration.class);

		callAndContinueOnFailure(CheckDistinctKeyIdValueInClientJWKs.class, Condition.ConditionResult.FAILURE, "RFC7517-4.5");

		getSsa();

		eventLog.startBlock("Perform Dynamic Client Registration");

		callAndStopOnFailure(StoreOriginalClientConfiguration.class);
		callAndStopOnFailure(ExtractClientNameFromStoredConfig.class);

		setupJwksUri();

		// create basic dynamic registration request
		callAndStopOnFailure(CreateEmptyDynamicRegistrationRequest.class);

		callAndStopOnFailure(AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest.class);
		if (!jarm.isTrue()) {
			// implicit is only required when id_token is returned in frontchannel
			callAndStopOnFailure(AddImplicitGrantTypeToDynamicRegistrationRequest.class);
		}
		callAndStopOnFailure(AddRefreshTokenGrantTypeToDynamicRegistrationRequest.class);
		callAndStopOnFailure(AddClientCredentialsGrantTypeToDynamicRegistrationRequest.class);

		if (clientAuthType == ClientAuthType.MTLS) {
			callAndStopOnFailure(AddTlsClientAuthSubjectDnToDynamicRegistrationRequest.class);
		}

		addJwksToRequest();
		callAndStopOnFailure(AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment.class);
		if (jarm.isTrue()) {
			callAndStopOnFailure(SetResponseTypeCodeInDynamicRegistrationRequest.class);
		} else {
			callAndStopOnFailure(SetResponseTypeCodeIdTokenInDynamicRegistrationRequest.class);
		}
		validateSsa();
		callAndStopOnFailure(AddRedirectUriToDynamicRegistrationRequest.class);

		addSoftwareStatementToRegistrationRequest();

		addWebhookUrisToDynamicRegistrationRequest();

		callRegistrationEndpoint();
	}

	protected void addWebhookUrisToDynamicRegistrationRequest() {
		callAndStopOnFailure(AddWebhookUrisToDynamicRegistrationRequest.class);
	}

	protected void useClientCredentialsAccessToken() {
		env.mapKey("access_token", "old_access_token");
	}

	protected void userAuthorisationCodeAccessToken() {
		env.unmapKey("access_token");
	}

	protected abstract Class<? extends Condition> postEnrollmentsValidator();
	protected abstract Class<? extends Condition> getEnrollmentsValidator();
	protected abstract void executeTestSteps();

	protected abstract ConditionSequence getConsentAndResourceEndpointSequence();

	}
