package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.AbstractCallProtectedResource;
import net.openid.conformance.testmodule.Environment;


public class CallResource extends AbstractCallProtectedResource {

	@Override
	public Environment evaluate(Environment env) {
		return callProtectedResource(env);
	}

	@Override
	protected Environment handleClientResponse(Environment env, JsonObject responseCode, String responseBody, JsonObject responseHeaders, JsonObject fullResponse) {
		// resource_endpoint_response env var is deprecated and is being removed
		env.putString("resource_endpoint_response", responseBody);

		env.putObject("resource_endpoint_response_full", fullResponse);
		logSuccess("Got a response from the resource endpoint: ", fullResponse);
		return env;
	}
}
