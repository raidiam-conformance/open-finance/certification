package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsNoFundsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_nofunds_test-module_v4",
	displayName = "Ensure a payment with an amount of 9999999999.99 will lead to a RJCT Payment with SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE error",
	summary = "Ensure a payment with an amount of 9999999999.99 will lead to a RJCT Payment with SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE error\n" +
		"\u2022 Creates consent request_body with valid email proxy (cliente-a00001@pix.bcb.gov.br) and it’s standardized payload, however set the amount to be 9999999999.99\n" +
		"\u2022 Call the POST Consents API \n" +
		"\u2022 Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"\u2022 Redirects the user to authorize the created consent - Expect Failure\n" +
		"\u2022 Ensure an error is returned and that the authorization code was not sent back\n" +
		"\u2022 Call the GET Consents API\n" +
		"\u2022 Expects 200 - Validate response - Check if Status is REJECTED and rejectionReason is SALDO_INSUFICIENTE or VALOR_ACIMA_LIMITE",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsNoFundsTestModuleV4 extends AbstractPaymentsNoFundsTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return null;
	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		// Not needed
	}

	@Override
	protected void validate422ErrorResponseCode() {
		// Not needed
	}

	@Override
	protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		return null;
	}

	@Override
	protected void setupResourceEndpoint() {
		call(new ValidateRegisteredEndpoints(
			sequenceOf(
				condition(GetPaymentV4Endpoint.class),
				condition(GetPaymentConsentsV4Endpoint.class))
			)
		);		super.setupResourceEndpoint();
	}
}
