package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;

public class AddRiskSignalsManualObjectToPixPaymentBody extends AbstractAddRiskSignalsObjectToPixPaymentBody {
	@Override
	protected JsonObject getRiskSignals() {
		JsonObject riskSignals = new JsonObject();

		JsonObject manual = new JsonObject();
		manual.addProperty("deviceId", "00000000-54b3-e7c7-0000-000046bffd97");
		manual.addProperty("isRootedDevice", false);
		manual.addProperty("screenBrightness", 0);
		manual.addProperty("elapsedTimeSinceBoot", 0);
		manual.addProperty("osVersion", "string");
		manual.addProperty("userTimeZoneOffset", "-03");
		manual.addProperty("language", "pt");

		JsonObject screenDimensions = new JsonObject();
		screenDimensions.addProperty("height",0);
		screenDimensions.addProperty("width",0);
		manual.add("screenDimensions", screenDimensions);

		manual.addProperty("accountTenure", "2023-12-21");

		riskSignals.add("manual", manual);
		return riskSignals;
	}
}
