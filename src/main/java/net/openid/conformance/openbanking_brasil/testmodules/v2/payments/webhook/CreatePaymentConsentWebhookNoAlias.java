package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.extensions.SpringContext;
import net.openid.conformance.extensions.TestModuleRepository;
import net.openid.conformance.testmodule.Environment;

import java.net.URI;
import java.net.URISyntaxException;

public class CreatePaymentConsentWebhookNoAlias extends AbstractCondition {
	@Override
	@PostEnvironment(strings = "webhook_uri_consent_id")
	@PreEnvironment(required = "config", strings = "consent_id")
	public Environment evaluate(Environment env) {
		String baseUrl = env.getString("base_url");
		String id = env.getString("consent_id");
		TestModuleRepository inMemoryTestModuleRepo = getInMemoryTestModuleRepo();
		JsonObject testModuleInfo = new JsonObject();
		testModuleInfo.addProperty("consent_id", id);
		inMemoryTestModuleRepo.saveTestModuleDetails(getTestId(), testModuleInfo);
		try {
			URI uri = new URI(baseUrl);
			String host = uri.getHost();
			String webhookUrl = String.format("https://%s/test-mtls/fvp/w/open-banking/webhook/v1/payments/v3/consents/%s",host, id);
			env.putString("webhook_uri_consent_id", webhookUrl);
			logSuccess("Created webhook URI", args("webhook_uri", webhookUrl));
		} catch (URISyntaxException e) {
			throw error("Failed to create webhook uri for test", args("error", e.getMessage()));
		}
		return env;
	}

	private TestModuleRepository getInMemoryTestModuleRepo() {
		return SpringContext.getBean(TestModuleRepository.class);
	}
}
