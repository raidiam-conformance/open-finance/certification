package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.v2.RecurringPaymentsRejectionReasonEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public abstract class AbstractEnsureRecurringPaymentsRejectionReason extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			String rejectionReasonCode = extractRejectionReasonFromEnv(env);
			if (checkIfRejectionReasonMatches(rejectionReasonCode)) {
				logSuccess("Rejection reason returned in the response matches the expected rejection reason");
			} else {
				throw error("Rejection reason returned in the response does not match the expected rejection reason",
					args("rejection reason", rejectionReasonCode,
						"expected rejection reason", expectedRejectionReason())
				);
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected String extractRejectionReasonFromEnv(Environment env) throws ParseException {
		return OIDFJSON.getString(
			BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(body -> body.getAsJsonObject().getAsJsonObject("data"))
				.map(data -> data.getAsJsonObject("rejectionReason"))
				.map(rejectionReason -> rejectionReason.get("code"))
				.orElseThrow(() -> error("Unable to find element data.rejectionReason.code in the response payload"))
		);
	}

	protected boolean checkIfRejectionReasonMatches(String rejectionReasonCode) {
		return rejectionReasonCode.equals(expectedRejectionReason().toString());
	}

	protected abstract RecurringPaymentsRejectionReasonEnumV2 expectedRejectionReason();
}
