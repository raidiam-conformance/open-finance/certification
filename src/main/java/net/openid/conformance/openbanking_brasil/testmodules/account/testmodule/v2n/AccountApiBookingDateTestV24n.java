package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule.AbstractAccountApiBookingDateTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountTransactionsOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsIdentificationOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsListOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "accounts_api_bookingdate_test-module_v2-4",
	displayName = "Test the max date of a payment",
	summary = "Testing that the server is respecting the BookingDate filter rules\n" +
		"• Creates a consent with only ACCOUNTS permissions\n" +
		"• 201 code and successful redirect\n" +
		"• Using the consent created, call the Accounts API\n" +
		"• Call GET Accounts Transactions API, send query parameters fromBookingDate and toBookingDate using 6 months before current date (From D to D-180)\n" +
		"• Expect success, fetch a transaction, get the transactionDateTime, make sure this transaction is within the range above\n" +
		"• Call GET Accounts Transactions API, send query parameters fromBookingDate and toBookingDate using 6 months older than current date (From D-180 to D-360)\n" +
		"• Expect success, fetch a transaction, get the transactionDateTime, make sure this transaction is within the range above - Save the date from one of the transactions returned on that API Call - Save it's date\n" +
		"• Call GET Accounts Transactions API, send query parameters fromBookingDate and toBookingDate to be the transactionDateTime saved on the test below\n" +
		"• Expect success, make sure that the returned transactions is from exactly the date returned above\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class AccountApiBookingDateTestV24n extends AbstractAccountApiBookingDateTest {

	@Override
	protected Class<? extends Condition> getAccountListValidator() {
		return GetAccountsListOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountIdentificationResponseValidator() {
		return GetAccountsIdentificationOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountTransactionsValidator() {
		return GetAccountTransactionsOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
