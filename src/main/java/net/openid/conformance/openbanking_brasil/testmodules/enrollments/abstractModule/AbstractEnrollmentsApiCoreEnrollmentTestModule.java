package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

public abstract class AbstractEnrollmentsApiCoreEnrollmentTestModule extends AbstractCompleteEnrollmentsApiEnrollmentTestModule {

	@Override
	protected void executeTestSteps() {}
}
