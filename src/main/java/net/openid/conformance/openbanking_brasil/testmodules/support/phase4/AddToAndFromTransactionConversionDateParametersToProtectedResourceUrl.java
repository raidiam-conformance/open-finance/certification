package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Map;

public class AddToAndFromTransactionConversionDateParametersToProtectedResourceUrl extends AbstractCondition {

    @Override
    @PreEnvironment(strings = {"protected_resource_url", "fromTransactionConversionDate", "toTransactionConversionDate"})
    public Environment evaluate(Environment env) {
        String baseUrl = env.getString("protected_resource_url");
        String fromTransactionConversionDate = env.getString("fromTransactionConversionDate");
        String toTransactionConversionDate = env.getString("toTransactionConversionDate");

        //Remove leftover transaction dates query params
        baseUrl = baseUrl.replaceAll("fromTransactionConversionDate=[0-9-]{10,11}&?","");
        baseUrl = baseUrl.replaceAll("toTransactionConversionDate=[0-9-]{10,11}&?","");

        while(baseUrl.endsWith("&") || baseUrl.endsWith("?")) {
            baseUrl=baseUrl.substring(0,baseUrl.length()-1);
        }

        String pattern = "%s?fromTransactionConversionDate=%s&toTransactionConversionDate=%s";
        if(baseUrl.contains("?")){
            pattern = "%s&fromTransactionConversionDate=%s&toTransactionConversionDate=%s";
        }
        String url = String.format(pattern, baseUrl, fromTransactionConversionDate, toTransactionConversionDate);
        env.putString("protected_resource_url", url);
        logSuccess("Parameters were added to the resource URL", Map.of("URL", url));
        return env;
    }
}
