package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionCurrentTimezoneTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsTransactionsCurrentV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.AddToAndFromTransactionConversionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckTransactionConversionDateRange;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToFunds;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.CheckAllTransactionsConversionDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetFromTransactionConversionDateTo6DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetFromTransactionConversionDateTo7DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetToTransactionConversionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "funds_api_transactions-current-timezone_test-module",
	displayName = "funds_api_transactions-current-timezone_test-module",
	summary = "Test the transaction current endpoint by sending different parameters to the endpoint and ensuring that they are correctly returned.\n" +
		"To ensure that the server can process the the date, which is set as UTC-3, this test must be executed between 9pm UTC-3 and 11:59pm UTC-3\n" +
		"Call the POST Consents endpoint with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
		"Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
		"Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
		"Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"Call the GET Consents endpoint\n" +
		"Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
		"Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
		"Call the GET Investments List Endpoint \n" +
		"Expect a 200 Response - Extract one InvestmentId - Validate the Response_body\n" +
		"Call GET Investments Transactions-Current API without any query parameters\n" +
		"Expect a 200 Response- Validate all fields of the API - Make sure if transactions are found that they are all from the current date - Test can also expect an empty list\n" +
		"Call GET Investments Transactions-Current API -  Send query Params fromTransactionConversionDate=D-6 and toTransactionConversionDate=D+0, with D being defined as UTC-3 \n" +
		"Expect a 200 Response - Validate all fields of the API - Make sure that at least one transaction is returned and that its transaction date is set between D-6 and D\n" +
		"Call GET Investments Transactions-Current API -  Send query Params fromTransactionConversionDate=D-7 and toTransactionConversionDate=D+0\n" +
		"Expect 422 Unprocessable Entity\n" +
		"Call the Delete Consents Endpoints\n" +
		"Expect a 204 without a body\n" +
		"Validate if the current time is set between 9pm UTC-3 and 11:59pm UTC-3 - Return failure if not as defined on the test summary",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class FundsApiTransactionsCurrentTimezoneTestModule extends AbstractInvestmentsApiTransactionsCurrentTimezoneTestModule {

	@Override
	protected AbstractSetInvestmentApi setInvestmentsApi()  {
		return new SetInvestmentApiToFunds();
	}

	@Override
	protected Class<? extends Condition> investmentsRootValidator()  {
		return GetFundsListV1OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsTransactionsValidator() {
		return GetFundsTransactionsCurrentV1OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> setFromTransactionTo6DaysAgo() {
		return SetFromTransactionConversionDateTo6DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> setToTransactionToToday() {
		return SetToTransactionConversionDateToToday.class;
	}

	@Override
	protected Class<? extends Condition> setFromTransactionTo7DaysAgo() {
		return SetFromTransactionConversionDateTo7DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
		return AddToAndFromTransactionConversionDateParametersToProtectedResourceUrl.class;
	}

	@Override
	protected Class<? extends Condition> checkTransactionRange() {
		return CheckTransactionConversionDateRange.class;
	}

	@Override
	protected Class<? extends Condition> checkAllDates() {
		return CheckAllTransactionsConversionDates.class;
	}

	@Override
	protected String transactionParameterName() {
		return "TransactionConversionDate";
	}

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.FUNDS;
	}
}
