package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21.GetVariableIncomeBrokerNotesV1n21OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21.GetVariableIncomesListV1n21OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "variable-incomes_api_broker-note_test-module_v1-2-1",
    displayName = "variable-incomes_api_broker-note_test-module_v1-2-1",
    summary = "Test that the institution has correctly implemented the Broker Notes endpoint.\n" +
        "• Call the POST Consents endpoint with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
        "• Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Consents endpoint\n" +
        "• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
        "• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
        "• Call the GET Investments List Endpoint\n" +
        "• Expect a 200 Response - Validate Response_body - Extract one investmentId\n" +
        "• Call the GET Investments Transactions Endpoint  with the Extracted investmentId - Send page-size= 1000 - Send query Params fromTransactionDate=D-360 and toTransactionDate=D+0\n" +
        "• Expect a 200 Response - Validate all fields of the API - Make sure the field \"brokerNoteId\" is returned for at least one of the transactions - Extract the brokerNoteId\n" +
        "• Call the GET Broker Note Details Endpoint with the Extracted investmentId and the Extracted brokerNoteId \n" +
        "• Expect a 200 Response - Validate all fields of the API\n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "directory.discoveryUrl",
        "resource.brazilCpf"
    }
)
public class VariablesIncomesApiBrokerNoteTestModuleV1n21 extends VariablesIncomesApiBrokerNoteTestModule {

    @Override
    protected Class<? extends Condition> investmentsRootValidator() {
		return GetVariableIncomesListV1n21OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsBrokerNoteValidator() {
		return GetVariableIncomeBrokerNotesV1n21OASValidator.class;
	}
}
