package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public enum AuthServerCertificationsEnum {
	FAPI_ADV_OP_WITH_MTLS("FAPI Adv. OP w/ MTLS"),
	FAPI_ADV_OP_WITH_MTLS_PAR("FAPI Adv. OP w/ MTLS, PAR"),
	FAPI_ADV_OP_WITH_PRIVATE_KEY("FAPI Adv. OP w/ Private Key"),
	FAPI_ADV_OP_WITH_PRIVATE_KEY_PAR("FAPI Adv. OP w/ Private Key, PAR"),
	BR_OF_ADV_OP_WITH_PRIVATE_KEY_PAR_FAPI_BR_V2("BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)");

	private final String value;

	AuthServerCertificationsEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
