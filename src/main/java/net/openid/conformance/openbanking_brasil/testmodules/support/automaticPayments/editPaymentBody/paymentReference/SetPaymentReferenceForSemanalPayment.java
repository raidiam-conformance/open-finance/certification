package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference;

import java.time.LocalDate;
import java.time.temporal.WeekFields;

public class SetPaymentReferenceForSemanalPayment extends AbstractSetPaymentReference {

	@Override
	protected String getPaymentReference(String dateStr) {
		LocalDate date = getPaymentDate(dateStr);
		int weekNumber = date.get(WeekFields.ISO.weekOfWeekBasedYear());
		int weekBasedYear = date.get(WeekFields.ISO.weekBasedYear());

		return String.format("W%d-%d", weekNumber, weekBasedYear);
	}
}
