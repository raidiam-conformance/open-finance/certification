package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditEnrollmentsRequestBodyToRemoveDebtorAccountAccountTypeField extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonObject debtorAccount = Optional.ofNullable(
			env.getElementFromObject("resource_request_entity_claims", "data.debtorAccount")
		).orElse(new JsonObject()).getAsJsonObject();

		debtorAccount.remove("accountType");

		logSuccess("Successfully edited request body to remove debtorAccount.accountType",
			args("request body", env.getObject("resource_request_entity_claims")));

		return env;
	}
}
