package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiRecurringPaymentsLowerQuantityTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_recurring-payments-lower-quantity_test-module_v4",
	displayName = "Ensure that consent for recurring payments can be carried out and that the payment is not successfully scheduled when its the quantity of payments sent is below what is on the consent",
	summary = "Ensure that consent for recurring payments can be carried out and that the payment is not successfully scheduled when its the quantity of payments sent is below what is on the consent\n" +
		"\u2022 Call the POST Consents endpoints with the schedule.custom.dates field set as D+1, D+7, D+32, D+129 and D+500\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is ““AUTHORISED”” and validate response\n" +
		"\u2022 Calls the POST Payments Endpoint with only 4 Payments, using the apprpriate endtoendID for each of them, ensuring the day for the first payment is the next day 01 and not the startDate, and skipping the last one\n" +
		"\u2022 Expects 422 - PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is “CONSUMED” and validate response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)

public class PaymentsApiRecurringPaymentsLowerQuantityTestModuleV4 extends AbstractPaymentsApiRecurringPaymentsLowerQuantityTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

}
