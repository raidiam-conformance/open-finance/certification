package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes;

import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetBankFixedIncomesIdentificationOASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertRenumeration(data);
	}

	private void assertRenumeration(JsonObject data) {
		JsonObject remuneration = JsonPath.read(data, "$.remuneration");

		assertField1IsRequiredWhenField2HasValue2(
			remuneration,
			"preFixedRate",
			"indexer",
			"PRE_FIXADO"
		);

		assertField1IsRequiredWhenField2HasValue2(
			remuneration,
			"postFixedIndexerPercentage",
			"indexer",
			"PRE_FIXADO"
		);

		assertField1IsRequiredWhenField2HasValue2(
			remuneration,
			"indexerAdditionalInfo",
			"indexer",
			"OUTROS"
		);

	}
}
