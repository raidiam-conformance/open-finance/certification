package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import java.util.Arrays;

public enum AuthServerApiResourcePhaseEnum {
	PHASE_3("payments-consents", "payments-pix"),
	PHASE_2("accounts", "consents", "credit-cards-accounts","customers-business","customers-personal","financings","invoice-financings","loans","resources","unarranged-accounts-overdraft","credit-fixed-incomes","bank-fixed-incomes","variable-incomes","treasure-titles","funds");

	private final String[] strings;

	AuthServerApiResourcePhaseEnum(String... strings) {
		this.strings = Arrays.copyOf(strings, strings.length);
	}

	public String[] getStrings() {
		return strings;
	}
}
