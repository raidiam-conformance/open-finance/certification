package net.openid.conformance.openbanking_brasil.testmodules.v2.yacs;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "fvp_consents_api_bad-logged_test-module_v3-2",
	displayName = "FAPI1-Advanced-Final: Brazil DCR happy flow without authentication flow",
	summary = "This test will try to use the recently created DCR to access a Consent v3-2 Call with a dummy, but well-formatted payload to make sure that the server will read the request but won’t be able to process it.\n" +
		"\u2022 Validate that the server contains the consents v3 endpoint published in the directory\n" +
		"\u2022 Create a client by performing a DCR against the provided server - Expect Success\n" +
		"\u2022 Generate a token with the client_id created using client_credentials grant with consents\n" +
		"\u2022 Use the token to call the POST Consents v3 API with a pre-defined payload\n" +
		"\u2022 If the Institution returns a 529 instead of a 201, wait 10 seconds and retry the endpoint call - Do this sequence 3 times. Return a failure if all three calls were a 529.\n" +
		"\u2022 Expect the server to accept the message and return a 201 - Validate the response_body",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.authorisationServerId",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"directory.client_id",
		"directory.apibase",
		"resource.consentUrl",
		"resource.consentUrl2",
		"resource.brazilOrganizationId"
	}
)

public class FVPDCRConsentsBadLoggedUserV3 extends AbstractFvpDcrConsentsTestModule {
	@Override
	protected Class<? extends AbstractJsonAssertingCondition> postConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetConsentV3Endpoint.class;
	}

}
