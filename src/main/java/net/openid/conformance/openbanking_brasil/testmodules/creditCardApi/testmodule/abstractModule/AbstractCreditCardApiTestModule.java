package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.abstractModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingBillTransactionResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardBills;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardLimits;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromDueDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.CardAccountSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.CardBillSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetCreditCardsAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public abstract class AbstractCreditCardApiTestModule extends AbstractPhase2TestModule {

	protected abstract Class<? extends Condition> getCardAccountsListValidator();
	protected abstract Class<? extends Condition> getCardAccountsTransactionsValidator();
	protected abstract Class<? extends Condition> getCardAccountsLimitsValidator();
	protected abstract Class<? extends Condition> getCardIdentificationValidator();
	protected abstract Class<? extends Condition> getCardBillsValidator();
	protected abstract Class<? extends Condition> getCardBillsTransactionsValidator();

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetCreditCardsAccountsV2Endpoint.class),
			condition(GetConsentV3Endpoint.class)
		);
	}

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	protected void configureClient(){
		super.configureClient();
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromTransactionDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toTransactionDate", currentDate.format(FORMATTER));
		env.putString("fromDueDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toDueDate", currentDate.format(FORMATTER));
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_CARDS_ACCOUNTS).addScopes(OPFScopesEnum.CREDIT_CARDS_ACCOUNTS, OPFScopesEnum.OPEN_ID).build();

	}

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(getCardAccountsListValidator(), Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(CardAccountSelector.class);
		callAndStopOnFailure(PrepareUrlForFetchingAccountResource.class);
		preCallProtectedResource("Fetch Credit Card details V2");
		callAndContinueOnFailure(getCardIdentificationValidator(), Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(PrepareUrlForFetchingCardLimits.class);
		preCallProtectedResource("Fetch card limits V2");
		callAndContinueOnFailure(getCardAccountsLimitsValidator(), Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(PrepareUrlForFetchingCardTransactions.class);
		callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
		preCallProtectedResource("Fetch card transactions V2");
		callAndContinueOnFailure(getCardAccountsTransactionsValidator(), Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(PrepareUrlForFetchingCardBills.class);
		callAndStopOnFailure(AddToAndFromDueDateParametersToProtectedResourceUrl.class);
		preCallProtectedResource("Fetch card bills V2");
		callAndContinueOnFailure(getCardBillsValidator(), Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(CardBillSelector.class);
		callAndStopOnFailure(PrepareUrlForFetchingBillTransactionResource.class);
		callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
		preCallProtectedResource("Fetch Credit Card bill transaction V2");
		callAndContinueOnFailure(getCardBillsTransactionsValidator(), Condition.ConditionResult.FAILURE);
	}

}
