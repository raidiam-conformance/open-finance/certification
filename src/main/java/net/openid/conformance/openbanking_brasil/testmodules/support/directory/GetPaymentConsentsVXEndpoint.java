package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import net.openid.conformance.testmodule.Environment;

public abstract class GetPaymentConsentsVXEndpoint extends AbstractGetXFromAuthServer {
	@Override
	protected void performUrlNotFoundAction(Environment env) {
		env.putString("warning_message", "Couldn't locate the payment consents endpoint from the authorisation server");
	}

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/payments\\/v\\d+\\/consents)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "payments-consents";
	}

	@Override
	protected boolean isResource() {
		return false;
	}
}
