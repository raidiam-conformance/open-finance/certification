package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount;

public class SetAutomaticPaymentAmountTo50 extends AbstractSetAutomaticPaymentAmount {

	@Override
	protected String automaticPaymentAmount() {
		return "50.00";
	}
}
