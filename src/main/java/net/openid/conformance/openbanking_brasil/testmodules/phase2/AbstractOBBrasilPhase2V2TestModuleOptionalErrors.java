package net.openid.conformance.openbanking_brasil.testmodules.phase2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractOBBrasilFunctionalTestModuleOptionalErrors;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOBBrasilPhase2V2TestModuleOptionalErrors extends AbstractOBBrasilFunctionalTestModuleOptionalErrors {

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndContinueOnFailure(SaveConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	public void cleanup() {
		eventLog.startBlock("Deleting consent");
		callAndContinueOnFailure(LoadConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(PrepareToFetchConsentRequest.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps()
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class)));
	}
}
