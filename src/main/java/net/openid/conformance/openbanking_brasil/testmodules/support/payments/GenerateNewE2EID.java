package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class GenerateNewE2EID extends AbstractCondition {

	static private final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmm");

	@Override
	@PreEnvironment(required = "resource", strings = "ispb")
	public Environment evaluate(Environment env) {
		log("Generating new endToEndId for the payment request object");

		JsonObject resource = env.getObject("resource");
		JsonElement paymentRequest = resource.getAsJsonObject("brazilPixPayment").get("data");

		if (paymentRequest.isJsonArray()) {
			JsonArray payments = paymentRequest.getAsJsonArray();
			for (int i = 0; i < payments.size(); i++) {

				JsonObject payment = payments.get(i).getAsJsonObject();
				String endToEndId = generateEndToEndIdWithCurrentDateTime(env);
				payment.addProperty("endToEndId", endToEndId);
				logSuccess("Successfully generated a new endToEndId", payment);
			}

		} else {

			String endToEndId = generateEndToEndIdWithCurrentDateTime(env);
			paymentRequest.getAsJsonObject().addProperty("endToEndId", endToEndId);
			logSuccess("Successfully generated a new endToEndId", paymentRequest.getAsJsonObject());

		}

		return env;
	}

	protected String generateEndToEndIdWithCurrentDateTime(Environment env) {
		String ispb = env.getString("ispb");
		OffsetDateTime currentDateTime = OffsetDateTime.now(ZoneOffset.UTC);
		String formattedCurrentDateTime = currentDateTime.format(FORMATTER);
		String randomString = RandomStringUtils.randomAlphanumeric(11);
		String endToEndId = String.format("E%s%s%s", ispb, formattedCurrentDateTime, randomString);
		env.putString("endToEndId", endToEndId);
		env.putString("ispb", ispb);
		return endToEndId;
	}
}
