package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractApiOperationalLimitsTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.PrepareUrlForFetchingCreditDiscountedCreditRightsContract;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.PrepareUrlForFetchingCreditDiscountedCreditRightsContractGuarantees;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.PrepareUrlForFetchingCreditDiscountedCreditRightsContractInstalments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.PrepareUrlForFetchingCreditDiscountedCreditRightsContractPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureSpecificCreditOperationsPermissionsWereReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvoiceFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractInvoiceFinancingsApiOperationalLimitsTestModule  extends AbstractApiOperationalLimitsTestModulePhase2 {

	@Override
	protected abstract Class<? extends Condition> contractsResponseValidator();

	@Override
	protected abstract Class<? extends Condition> contractIdValidator();

	@Override
	protected abstract Class<? extends Condition> contractWarrantiesValidator();

	@Override
	protected abstract Class<? extends Condition> contractScheduledInstalmentsValidator();

	@Override
	protected abstract Class<? extends Condition> contractPaymentsValidator();

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getResourceEndpoint() {
		return GetInvoiceFinancingsV2Endpoint.class;
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.INVOICE_FINANCINGS;
	}

	@Override
	protected EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType getPermissionType() {
		return EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType.INVOICE_FINANCINGS;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractId() {
		return PrepareUrlForFetchingCreditDiscountedCreditRightsContract.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingWarranties() {
		return PrepareUrlForFetchingCreditDiscountedCreditRightsContractGuarantees.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingScheduledInstalments() {
		return PrepareUrlForFetchingCreditDiscountedCreditRightsContractInstalments.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingPayments() {
		return PrepareUrlForFetchingCreditDiscountedCreditRightsContractPayments.class;
	}

}
