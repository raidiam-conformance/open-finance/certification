package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensurePaymentsRejection;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public abstract class AbstractEnsurePaymentRejectionReasonCodeWasXV3 extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			JsonObject responseBody = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response")).getAsJsonObject();

			JsonObject data = Optional.ofNullable(responseBody.getAsJsonObject("data"))
				.orElseThrow(() -> error("Could not extract data from body", args("body", responseBody)));


			JsonObject rejectionReason = Optional.ofNullable(data.getAsJsonObject("rejectionReason"))
				.orElseThrow(() -> error("Could not extract rejectionReason from data", args("data", data)));


			String rejectionCode = OIDFJSON.getString(Optional.ofNullable(rejectionReason.get("code"))
				.orElseThrow(() -> error("Could not extract code from rejectionReason", args("rejectionReason", rejectionReason))));

			List<String> expectedRejectionCodes = getExpectedRejectionCodes();

			if (!expectedRejectionCodes.contains(rejectionCode)) {
				throw error("Payment rejection code is not what was expected",
					args("Expected", expectedRejectionCodes, "Actual", rejectionCode));
			}

			logSuccess("Received expected rejection code",
				args("Expected", expectedRejectionCodes, "Received", rejectionCode));

		} catch (ClassCastException e) {
			throw error("A Json Object was expected");

		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected abstract List<String> getExpectedRejectionCodes();

}
