package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic;

import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class CreateAutomaticRecurringConfigurationObjectUserDefinedIntervalAndDateAndIsRetryAccepted extends AbstractCreateAutomaticRecurringConfigurationObject {

	private String interval;
	private String referenceStartDate;
	private String isRetryAccepted;

	@Override
	@PreEnvironment(required = "config")
	@PostEnvironment(required = "recurring_configuration_object")
	public Environment evaluate(Environment env) {
		interval = getFieldFromConfig(env, "interval");
		referenceStartDate = getFieldFromConfig(env, "referenceStartDate");
		isRetryAccepted = getFieldFromConfig(env, "isRetryAccepted");
		return super.evaluate(env);
	}

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getInterval() {
		if (interval == null) {
			throw error(getErrorMessage("interval"), args("interval", "null"));
		}
		try {
			return RecurringPaymentsConsentsAutomaticPixIntervalEnum.valueOf(interval);
		} catch (IllegalArgumentException e) {
			throw error(getErrorMessage("interval"), args("interval", interval));
		}
	}

	@Override
	public String getReferenceStartDate() {
		if (referenceStartDate == null) {
			throw error(getErrorMessage("referenceStartDate"), args("referenceStartDate", "null"));
		}
		try {
			LocalDate.parse(referenceStartDate, DATE_FORMATTER);
			return referenceStartDate;
		} catch (DateTimeParseException e) {
			throw error(getErrorMessage("referenceStartDate"), args("referenceStartDate", referenceStartDate));
		}
	}

	@Override
	public boolean isRetryAccepted() {
		if (isRetryAccepted == null) {
			throw error(getErrorMessage("isRetryAccepted"), args("isRetryAccepted", "null"));
		}
		String lowerCaseValue = isRetryAccepted.trim().toLowerCase();
		return switch (lowerCaseValue) {
			case "true" -> true;
			case "false" -> false;
			default ->
				throw error(getErrorMessage("isRetryAccepted"), args("isRetryAccepted", isRetryAccepted));
		};
	}

	@Override
	protected String getFixedAmount() {
		return null;
	}

	@Override
	protected boolean hasFirstPayment() {
		return false;
	}

	private String getFieldFromConfig(Environment env, String field) {
		return env.getString("config", "resource." + field);
	}

	private String getErrorMessage(String field) {
		return String.format("The value defined for resource.%s is not valid", field);
	}
}
