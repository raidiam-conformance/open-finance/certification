package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ConsentExtensionRequestInsertInvalidUser;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus365;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureResponseCodeWas401or403;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.FAPIBrazilOpenBankingCreateConsentExtensionRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInConsentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInGetSizeOne;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.sequence.ConditionSequence;


public abstract class AbstractConsentApiExtensionInvalidLoggedUserTestModule extends AbstractSimplifiedConsentRenewalTestModule {

	@Override
	protected abstract Class<? extends Condition> setConsentCreationValidation();

	@Override
	protected abstract Class<? extends Condition> setPostConsentExtensionValidator();

	@Override
	protected abstract Class<? extends Condition> setGetConsentExtensionValidator();

	@Override
	protected abstract Class<? extends Condition> setGetConsentExtensionEndpoint();

	@Override
	public ConditionSequence getPostConsentExtensionSequence() {
		ConditionSequence getPostConsentExtensionExpectingErrorSequence = getPostConsentExtensionExpectingErrorSequence(EnsureResponseCodeWas401or403.class, null, setExtensionExpirationTime());
		getPostConsentExtensionExpectingErrorSequence.insertAfter(FAPIBrazilOpenBankingCreateConsentExtensionRequest.class, condition(ConsentExtensionRequestInsertInvalidUser.class));
		return getPostConsentExtensionExpectingErrorSequence;
	}

	@Override
	public void callExtensionEndpoints(){
		runInBlock("Validating post consent extension response - expecting 401 or 403", this::callPostConsentExtensionEndpoint);
	}

	@Override
	protected void configureClient() {
		env.putString("metaOnlyRequestDateTime", "true");
		super.configureClient();
	}

	@Override
	protected void callGetConsentAndOrResourceUrlSequence(){
		call(new ValidateRegisteredEndpoints(
				sequenceOf(
					condition(GetConsentV3Endpoint.class),
					condition(GetResourcesV3Endpoint.class)
				)
			)
		);
	}

	@Override
	protected Class<? extends Condition> validateExpirationTimeReturned(){
		return ValidateExtensionExpiryTimeInConsentResponse.class;
	}

	@Override
	protected Class<? extends Condition> validateGetConsentExtensionEndpointResponse() {
		return ValidateExtensionExpiryTimeInGetSizeOne.class;
	}

	@Override
	protected Class<? extends Condition> buildConsentRequestBody() {
		return FAPIBrazilOpenBankingCreateConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setConsentExpirationTime() {
		return FAPIBrazilAddExpirationToConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {
		return CreateExtensionRequestTimeDayPlus365.class;
	}

}
