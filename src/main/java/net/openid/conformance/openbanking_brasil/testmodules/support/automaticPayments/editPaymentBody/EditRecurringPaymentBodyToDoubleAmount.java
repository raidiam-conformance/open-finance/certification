package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EditRecurringPaymentBodyToDoubleAmount extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject payment = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(paymentElement -> paymentElement.getAsJsonObject().getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("payment"))
			.orElseThrow(() -> error("Unable to find data.payment in payments payload"));

		String amount = Optional.ofNullable(payment.get("amount"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find amount in payments payload"));

		double amountValue = Double.parseDouble(amount);
		double newAmountValue = 2 * amountValue;
		String newAmount = String.format("%.2f", newAmountValue);

		payment.addProperty("amount", newAmount);
		logSuccess("Payment payload has been updated with double the amount", args("payment", payment, "old amount", amount));

		return env;
	}
}
