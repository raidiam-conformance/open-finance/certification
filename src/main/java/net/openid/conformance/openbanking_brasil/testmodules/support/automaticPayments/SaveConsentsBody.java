package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class SaveConsentsBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	@PostEnvironment(required = "savedBrazilPaymentConsent")
	public Environment evaluate(Environment env) {
		JsonObject consentsBody = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.orElseThrow(() -> error("Unable to find consents body inside the resource",
				args("resource", env.getObject("resource"))))
			.getAsJsonObject();
		env.putObject("savedBrazilPaymentConsent", consentsBody.deepCopy());
		logSuccess("Consents request body has been saved");
		return env;
	}
}
