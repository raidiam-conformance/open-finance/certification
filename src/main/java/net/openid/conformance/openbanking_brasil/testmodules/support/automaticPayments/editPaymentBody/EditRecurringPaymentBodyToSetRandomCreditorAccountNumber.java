package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Optional;

public class EditRecurringPaymentBodyToSetRandomCreditorAccountNumber extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));
		JsonObject creditorAccount = Optional.ofNullable(data.getAsJsonObject("creditorAccount"))
			.orElseThrow(() -> error("Unable to find creditorAccount in payments data"));

		creditorAccount.addProperty("number", RandomStringUtils.randomNumeric(1, 21));

		logSuccess("A random creditor account number has been added to the recurring-payments payload",
			args("creditorAccount", creditorAccount));

		return env;
	}
}
