package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiRejectedPreAuthorisationEnrollmentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.GetEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostFidoRegistrationOptionsOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "enrollments_api_rejected-preauthorisation-enrollment_test-module_v1",
    displayName = "enrollments_api_rejected-preauthorisation-enrollment_test-module_v1",
    summary = "Ensure that enrollment can be successfully rejected when status is AWAITING_ACCOUNT_HOLDER_VALIDATION:\n" +
		"• Call the POST enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Call the POST Risk Signals endpoint sending the appropriate signals\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is AWAITING_ACCOUNT_HOLDER_VALIDATION\n" +
		"• Call the PATCH enrollments endpoint\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"REJECTED\", cancelledFrom is INICIADORA and rejectionReason is REJEITADO_MANUALMENTE\n" +
		"• Call the POST fido-registration-options endpoint using client_credentials token\n" +
		"• Expect a 401 - Validate error response\n",
    profile = OBBProfile.OBB_PROFILE,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "resource.enrollmentsUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
    }
)
public class EnrollmentsApiRejectedPreAuthorisationEnrollmentTestModuleV1 extends AbstractEnrollmentsApiRejectedPreAuthorisationEnrollmentTestModule {

    @Override
    protected Class<? extends Condition> postEnrollmentsValidator() {
        return PostEnrollmentsOASValidatorV1.class;
    }

    @Override
    protected Class<? extends Condition> getEnrollmentsValidator() {
        return GetEnrollmentsOASValidatorV1.class;
    }

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetEnrollmentsV1Endpoint.class));
	}

	@Override
	public void cleanup() {
		//not required
	}
}
