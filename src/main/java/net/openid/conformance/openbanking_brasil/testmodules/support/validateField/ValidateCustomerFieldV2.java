package net.openid.conformance.openbanking_brasil.testmodules.support.validateField;

import com.google.gson.JsonObject;

public class ValidateCustomerFieldV2 extends AbstractValidateField {

	@Override
	protected void validateProductType(JsonObject config) {}

	@Override
	protected int getConsentVersion() {
		return 3;
	}
}
