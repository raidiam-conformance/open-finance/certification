package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.field.BooleanField;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.ExtraField;
import net.openid.conformance.util.field.IntArrayField;
import net.openid.conformance.util.field.IntField;
import net.openid.conformance.util.field.NumberArrayField;
import net.openid.conformance.util.field.NumberField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringArrayField;
import net.openid.conformance.util.field.StringField;
import net.openid.conformance.util.field.UntypedField;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static java.lang.Thread.currentThread;

/**
 * This class is intended to validate a JSON object based on the Open API specification provided in YAML format.
 * <a href="https://json-schema.org/draft/2020-12/json-schema-validation">JSON Schema Validation: A Vocabulary for Structural Validation of JSON</a>.
 * <p>
 * A child class just has to inform the path to the specification and which endpoint path and method will be
 * considered during validation.
 * <p>
 * The logic of OpenAPIJsonSchemaValidator already looks for a status code in the environment to know exactly which
 * schema component to use. In other words, it will use the schema component associated to the combination of
 * endpoint path, endpoint method and response status code, where the status code is obtained in runtime.
 */
@SuppressWarnings("unchecked")
public abstract class OpenAPIJsonSchemaValidator extends AbstractJsonAssertingCondition {
	public static final String REFERENCE_KEY = "$ref";
	public static final String ONE_OF_KEY = "oneOf";
	public static final String ALL_OF_KEY = "allOf";
	public static final String PATHS_KEY = "paths";
	public static final String RESPONSES_KEY = "responses";
	public static final String CONTENT_KEY = "content";
	public static final String SCHEMA_KEY = "schema";
	public static final String REQUIRED_KEY = "required";
	public static final String PROPERTIES_KEY = "properties";
	public static final String ADDITIONAL_PROPERTIES_KEY = "additionalProperties";
	public static final String TYPE_KEY = "type";
	public static final String STRING_TYPE = "string";
	public static final String INTEGER_TYPE = "integer";
	public static final String NUMBER_TYPE = "number";
	public static final String BOOLEAN_TYPE = "boolean";
	public static final String OBJECT_TYPE = "object";
	public static final String ARRAY_TYPE = "array";
	public static final String FORMAT_KEY = "format";
	public static final String DATE_FORMAT = "date";
	public static final String DATETIME_FORMAT = "date-time";
	public static final String MIN_LENGTH_KEY = "minLength";
	public static final String MAX_LENGTH_KEY = "maxLength";
	public static final String MAX_ITEMS_KEY = "maxItems";
	public static final String MIN_ITEMS_KEY = "minItems";
	public static final String MIN_VALUE_KEY = "minimum";
	public static final String MAX_VALUE_KEY = "maximum";
	public static final String ENUM_KEY = "enum";
	public static final String PATTERN_KEY = "pattern";
	public static final String ITEMS_KEY = "items";
	private Map<String, Object> openApiSpec;
	TestInstanceEventLog log;
	private ConditionResult conditionResultOnFailure;

	@Override
	public Environment evaluate(Environment env) {
		this.loadOpenApiSpec(getPathToOpenApiSpec());

		Integer statusCode = getStatusCode(env);
		Map<String, Object> bodySchema = this.getSchema(getEndpointPath(), getEndpointMethod().name().toLowerCase(), statusCode);
		JsonObject body = bodyFrom(env, getResponseEnvKey().getKey()).getAsJsonObject();

		this.assertBodySchema(body, bodySchema);
		this.assertSchemaAdditionalConstraints(body, statusCode);

		return env;
	}

	protected abstract String getPathToOpenApiSpec();
	protected abstract String getEndpointPath();
	protected abstract HttpMethod getEndpointMethod();
	protected Integer getSuccessfulStatusCode() {
		return getEndpointMethod() == HttpMethod.POST ? 201 : 200;
	}

	/**
	 * After validating the {@param body} against the spec, this method is called to perform additional validations.
	 */
	protected void assertSchemaAdditionalConstraints(JsonObject body, Integer statusCode) {
		if(getSuccessfulStatusCode().equals(statusCode)) {
			assertSchemaSuccessfulResponseAdditionalConstraints(body);
		} else {
			assertSchemaNonSuccessfulResponseAdditionalConstraints(body);
		}
	}

	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {}
	protected void assertSchemaNonSuccessfulResponseAdditionalConstraints(JsonObject body) {}

	/**
	 * This method is intended to validate additional constraints based on the schema component reference name.
	 * It's useful for components that have additional constraints and are repeat a lot.
	 * @param referenceToFieldSchema Example: "#/components/schemas/AutoInsuranceCoverageAttributesDetails".
	 * @param object has a field {@param fieldKey} whose schema is referenced by {@param referenceToFieldSchema}.
	 */
	protected void assertSchemaComponentAdditionalConstraints(JsonObject object, String fieldKey, String referenceToFieldSchema) {}

	protected ResponseEnvKey getResponseEnvKey() {
		return ResponseEnvKey.FullResponseEnvKey;
	}

	/**
	 * Child classes can override this method to choose how to evaluate extra fields.
	 */
	protected ExtraField getExtraField() {
		// Return a dummy extra field.
		return new ExtraField.Builder().build();
	}

	private Integer getStatusCode(Environment env) {
		return Optional.ofNullable(env.getInteger(getResponseEnvKey().getKey(), "status")).orElseThrow(
			() -> error("Could not find the status code", args(
				"response", env.getObject(getResponseEnvKey().getKey())
			))
		);
	}

	private void assertBodySchema(JsonObject body, Map<String, Object> bodySchema) {

		parseResponseBody(
			body,
			"$" // Parse all the field names starting from the root.
		);

		this.assertRootObjectSchema(body, bodySchema);

		assertExtraFields(getExtraField());
	}

	/**
	 * Assert that {@param object} contains a key {@param fieldKey} that complies with {@param fieldSchema}.
	 * {@param fieldKey} may be optional depending on the value of {@param isOptional}.
	 */
	private void assertFieldSchema(JsonObject object, String fieldKey, Map<String, Object> fieldSchema, boolean isOptional) {
		String referenceToSchema = (String) fieldSchema.get(REFERENCE_KEY);

		if(referenceToSchema != null) {
			// A reference to a schema component was found, then we will replace fieldSchema with the referenced schema.
			fieldSchema = getSchema(referenceToSchema);
		}

		if(fieldSchema.get(ONE_OF_KEY) != null) {
			// There are many schemas to validate, but only one must be valid.
			assertFieldOneOfSchemas(object, fieldKey, fieldSchema, isOptional);
			return;
		}

		if(fieldSchema.get(ALL_OF_KEY) != null) {
			// There are many schemas to validate and all of them must be valid
			assertFieldAllOfSchemas(object, fieldKey, fieldSchema, isOptional);
			return;
		}

		String type = (String) fieldSchema.get(TYPE_KEY);
		if(StringUtils.isBlank(type)) {
			this.assertUntypedField(object, fieldKey, isOptional);
			return;
		}
		switch (type) {
			case STRING_TYPE:
				this.assertStringFieldSchema(object, fieldKey, fieldSchema, isOptional);
				break;
			case INTEGER_TYPE:
				this.assertIntegerFieldSchema(object, fieldKey, fieldSchema, isOptional);
				break;
			case NUMBER_TYPE:
				this.assertNumberFieldSchema(object, fieldKey, fieldSchema, isOptional);
				break;
			case BOOLEAN_TYPE:
				this.assertBooleanFieldSchema(object, fieldKey, isOptional);
				break;
			case OBJECT_TYPE:
				this.assertObjectSchema(object, fieldKey, fieldSchema, isOptional);
				break;
			case ARRAY_TYPE:
				this.assertArrayFieldSchema(object, fieldKey, fieldSchema, isOptional);
				break;
			default:
				throw error("invalid type", args("type", type));
		}

		if(referenceToSchema != null) {
			assertSchemaComponentAdditionalConstraints(object, fieldKey, referenceToSchema);
		}
	}

	private void assertFieldAllOfSchemas(JsonObject object, String fieldKey, Map<String, Object> fieldSchema, boolean isOptional) {
		List<Map<String, Object>> schemas = (List<Map<String, Object>>) fieldSchema.get(ALL_OF_KEY);

		for (Map<String, Object> schema : schemas) {
			assertFieldSchema(object, fieldKey, schema, isOptional);
		}
	}

	private void assertFieldOneOfSchemas(JsonObject object, String fieldKey, Map<String, Object> fieldSchema, boolean isOptional) {
		List<Map<String, Object>> schemas = (List<Map<String, Object>>) fieldSchema.get(ONE_OF_KEY);
		int numberOfMatchingSchemas = 0;
		List<String> errors = new ArrayList<>();

		// Check if the field is present before validation.
		if (!object.has(fieldKey)) {
			if (!isOptional) {
				throw error(String.format("The field %s is required but not present", fieldKey));
			}
			return;
		}

		// Try to validate the fieldKey of object against all the schemas available.
		// Since one is expected to succeed and the rest to fail, we'll log the failures as INFO.
		// Then later we'll evaluate the result and log a failure as FAILURE if needed.
		setConditionResultOnFailure(ConditionResult.INFO);
		for(Map<String, Object> schema : schemas) {
			try {
				assertFieldSchema(object, fieldKey, schema, isOptional);
				numberOfMatchingSchemas++;
			} catch (ConditionError e) {
				errors.add(e.getMessage());
			}
		}
		setConditionResultOnFailure(conditionResultOnFailure);

		// Only one schema must be valid.
		if(numberOfMatchingSchemas != 1) {
			throw error(String.format("The field %s is expected to have exactly one valid schema", fieldKey), args(
				"field", fieldKey,
				"expected number valid schemas", 1,
				"number of valid schemas", numberOfMatchingSchemas,
				"number of schemas available", schemas.size(),
				"errors", errors
			));
		}

	}


	private void assertUntypedField(JsonObject object, String fieldKey, boolean isOptional) {
		UntypedField.Builder builder = new UntypedField.Builder(fieldKey);
		if(isOptional) {
			builder.setOptional();
		}

		this.assertField(object, builder.build());
	}

	private void assertStringFieldSchema(JsonObject object, String fieldKey, Map<String, Object> fieldSchema, boolean isOptional) {

		String format = (String) fieldSchema.get(FORMAT_KEY);
		if(DATE_FORMAT.equals(format) || DATETIME_FORMAT.equals(format)) {
			assertDatetimeStringFieldSchema(object, fieldKey, fieldSchema, isOptional);
			return;
		}

		StringField.Builder builder = new StringField.Builder(fieldKey);

		if(isOptional) {
			builder.setOptional();
		}

		Integer minLength = (Integer) fieldSchema.get(MIN_LENGTH_KEY);
		if(minLength != null) {
			builder.setMinLength(minLength);
		}

		Integer maxLength = (Integer) fieldSchema.get(MAX_LENGTH_KEY);
		if(maxLength != null) {
			builder.setMaxLength(maxLength);
		}

		String pattern = (String) fieldSchema.get(PATTERN_KEY);
		if(pattern != null) {
			builder.setPattern(pattern);
		}

		List<String> enums = (List<String>) fieldSchema.get(ENUM_KEY);
		if(enums != null) {
			builder.setEnums(Set.copyOf(enums));
		}

		this.assertField(object, builder.build());
	}

	private void assertDatetimeStringFieldSchema(JsonObject object, String fieldKey, Map<String, Object> fieldSchema, boolean isOptional) {
		DatetimeField.Builder builder = new DatetimeField.Builder(fieldKey);

		if(isOptional) {
			builder.setOptional();
		}

		Integer minLength = (Integer) fieldSchema.get(MIN_LENGTH_KEY);
		if(minLength != null) {
			builder.setMinLength(minLength);
		}

		Integer maxLength = (Integer) fieldSchema.get(MAX_LENGTH_KEY);
		if(maxLength != null) {
			builder.setMaxLength(maxLength);
		}

		// Configure the date pattern.
		String pattern = (String) fieldSchema.get(PATTERN_KEY);
		String format = (String) fieldSchema.get(FORMAT_KEY);
		if(pattern != null) {
			builder.setPattern(pattern);
		} else if(DATE_FORMAT.equals(format)) {
			builder.setPattern(DatetimeField.PATTERN_YYYY_MM_DD);
		} else {
			builder.setPattern(DatetimeField.ALTERNATIVE_PATTERN);
		}

		this.assertField(object, builder.build());
	}

	private void assertIntegerFieldSchema(JsonObject object, String fieldKey, Map<String, Object> fieldSchema, boolean isOptional) {

		IntField.Builder builder = new IntField.Builder(fieldKey);

		if(isOptional) {
			builder.setOptional();
		}

		Integer min = (Integer) fieldSchema.get(MIN_VALUE_KEY);
		if(min != null) {
			builder.setMinValue(min);
		}

		Integer max = (Integer) fieldSchema.get(MAX_VALUE_KEY);
		if(max != null) {
			builder.setMaxValue(max);
		}

		this.assertField(object, builder.build());
	}

	private void assertNumberFieldSchema(JsonObject object, String fieldKey, Map<String, Object> fieldSchema, boolean isOptional) {

		NumberField.Builder builder = new NumberField.Builder(fieldKey);

		if(isOptional) {
			builder.setOptional();
		}

		Number min = (Number) fieldSchema.get(MIN_VALUE_KEY);
		if(min != null) {
			builder.setMinValue(min);
		}

		Number max = (Number) fieldSchema.get(MAX_VALUE_KEY);
		if(max != null) {
			builder.setMaxValue(max);
		}

		this.assertField(object, builder.build());
	}

	private void assertBooleanFieldSchema(JsonObject object, String fieldKey, boolean isOptional) {
		BooleanField.Builder builder = new BooleanField.Builder(fieldKey);

		if(isOptional) {
			builder.setOptional();
		}

		this.assertField(object, builder.build());
	}

	private void assertRootObjectSchema(JsonObject object, Map<String, Object> schema) {
		assertObjectSchemaWithParentPath(object, schema, "$.");
	}

	private void assertObjectSchema(JsonObject object, Map<String, Object> schema) {
		assertObjectSchemaWithParentPath(
			object,
			schema,
			this.parentPath // Keep the parent path the same.
		);
	}

	private void assertObjectSchemaWithParentPath(JsonObject object, Map<String, Object> schema, String parentPath) {
		Map<String, Object> innerFieldSchemas = (Map<String, Object>) schema.get(PROPERTIES_KEY);
		List<String> requiredInnerFields = this.getRequiredFields(schema);
		for(String innerFieldKey : innerFieldSchemas.keySet()) {
			// Reset the parent path.
			// The object fields will have this as prefix, e.g. 'parentPath.innerFieldKey'.
			this.parentPath = parentPath;
			Map<String, Object> innerFieldSchema = (Map<String, Object>) innerFieldSchemas.get(innerFieldKey);
			boolean isInnerOptional = !requiredInnerFields.contains(innerFieldKey);
			this.assertFieldSchema(object, innerFieldKey, innerFieldSchema, isInnerOptional);
		}

		assertAdditionalProperties(object, schema);
	}

	private void assertObjectSchema(JsonObject object, String fieldKey, Map<String, Object> fieldSchema, boolean isOptional) {
		Map<String, Object> innerFieldSchemas = (Map<String, Object>) fieldSchema.get(PROPERTIES_KEY);
		List<String> requiredInnerFields = this.getRequiredFields(fieldSchema);

		if (innerFieldSchemas != null) {
			this.assertField(object,
				new ObjectField
					.Builder(fieldKey)
					.setValidator(innerObj -> {
						for(String innerFieldKey : innerFieldSchemas.keySet()) {
							Map<String, Object> innerFieldSchema = (Map<String, Object>) innerFieldSchemas.get(innerFieldKey);
							boolean isInnerOptional = !requiredInnerFields.contains(innerFieldKey);
							this.assertFieldSchema(innerObj, innerFieldKey, innerFieldSchema, isInnerOptional);
						}
					})
					.setOptional(isOptional)
					.build());
		}

		this.assertAdditionalProperties(object.getAsJsonObject(fieldKey), fieldSchema);
	}

	private void assertAdditionalProperties(JsonObject object, Map<String, Object> fieldSchema) {
		Object additionalProperties = fieldSchema.get(ADDITIONAL_PROPERTIES_KEY);
		if (additionalProperties == null || object == null) {
			// If additionalProperties is not specified, any additional properties are allowed.
			// If the object itself is null, there is no need to check the additional properties
			return;
		}
		if (additionalProperties instanceof Boolean) {
			assertBooleanAdditionalSchema(object, fieldSchema, (Boolean) additionalProperties);
		} else if (additionalProperties instanceof Map) {
			assertMapAdditionalSchema(object, fieldSchema, (Map<String, Object>) additionalProperties);
		} else {
			throw error("Invalid 'additionalProperties' value");
		}
	}

	private void assertBooleanAdditionalSchema (JsonObject object, Map<String, Object> fieldSchema, Boolean additionalProperties) {
		// If additionalProperties is true, any additional properties are allowed. Otherwise, no additional properties are allowed.
		if(!additionalProperties) {
			for (String key : object.keySet()) {
				if (!fieldSchema.containsKey(PROPERTIES_KEY) || !((Map<String, Object>) fieldSchema.get(PROPERTIES_KEY)).containsKey(key)) {
					throw error("Additional properties are not allowed", args("key", key));
				}
			}
		}
	}

	private void assertMapAdditionalSchema (JsonObject object, Map<String, Object> fieldSchema, Map <String, Object> additionalProperties) {
		// If additionalProperties is a schema, validate additional properties against this schema.
		for (String key : object.keySet()) {
			if (!fieldSchema.containsKey(PROPERTIES_KEY) || !((Map<String, Object>) fieldSchema.get(PROPERTIES_KEY)).containsKey(key)) {
				this.assertFieldSchema(object, key, additionalProperties, true);
			}
		}
	}

	private void assertArrayFieldSchema(JsonObject object, String fieldKey, Map<String, Object> fieldSchema, boolean isOptional) {
		Map<String, Object> itemSchema = (Map<String, Object>) fieldSchema.get(ITEMS_KEY);

		String referenceToSchema = (String) itemSchema.get(REFERENCE_KEY);
		if(referenceToSchema != null) {
			// A reference to a schema component was found, then we will replace itemSchema with the referenced schema.
			itemSchema = getSchema(referenceToSchema);
		}

		Integer minItems = (Integer) fieldSchema.get(MIN_ITEMS_KEY);
		Integer maxItems = (Integer) fieldSchema.get(MAX_ITEMS_KEY);

		if (itemSchema.get(ONE_OF_KEY) != null) {
			assertOneOfArrayFieldSchema(object, fieldKey, itemSchema, isOptional, minItems, maxItems);
			return;
		}

			String type = (String) itemSchema.get(TYPE_KEY);
			switch (type) {
				case STRING_TYPE:
					this.assertStringArrayFieldSchema(object, fieldKey, itemSchema, minItems, maxItems, isOptional);
					break;
				case INTEGER_TYPE:
					this.assertIntegerArrayFieldSchema(object, fieldKey, itemSchema, minItems, maxItems, isOptional);
					break;
				case NUMBER_TYPE:
					this.assertNumberArrayFieldSchema(object, fieldKey, itemSchema, minItems, maxItems, isOptional);
					break;
				case OBJECT_TYPE:
					this.assertObjectArrayFieldSchema(object, fieldKey, itemSchema, minItems, maxItems, isOptional);
					break;
				default:
					throw error("invalid type", args("type", type));
			}
		}

	private void assertOneOfArrayFieldSchema(JsonObject object, String fieldKey, Map<String, Object> itemSchema, boolean isOptional, Integer minItems, Integer maxItems) {

		if (!object.has(fieldKey)) {
			if (!isOptional) {
				throw error(String.format("The field %s is required but not present", fieldKey));
			}
			return;
		}

		if (!object.get(fieldKey).isJsonArray()) {
			throw error(String.format("The field %s is expected to be an array", fieldKey));
		}

		JsonArray array = object.getAsJsonArray(fieldKey);
		if (minItems != null && array.size() < minItems) {
			throw error(String.format("The field %s is expected to have at least %d items", fieldKey, minItems));
		}

		if (maxItems != null && array.size() > maxItems) {
			throw error(String.format("The field %s is expected to have at most %d items", fieldKey, maxItems));
		}

		for (JsonElement item : array) {
			JsonObject itemObject = new JsonObject();
			itemObject.add(fieldKey, item);

			assertFieldSchema(itemObject, fieldKey, itemSchema, false);
		}
	}

	private void assertStringArrayFieldSchema(JsonObject object, String fieldKey, Map<String, Object> itemSchema, Integer minItems, Integer maxItems, boolean isOptional){
		StringArrayField.Builder builder = new StringArrayField.Builder(fieldKey);

		if(isOptional) {
			builder.setOptional();
		}

		if(minItems != null) {
			builder.setMinItems(minItems);
		}

		if(maxItems != null) {
			builder.setMaxItems(maxItems);
		}

		Integer minLength = (Integer) itemSchema.get(MIN_LENGTH_KEY);
		if(minLength != null) {
			builder.setMinLength(minLength);
		}

		Integer maxLength = (Integer) itemSchema.get(MAX_LENGTH_KEY);
		if(maxLength != null) {
			builder.setMaxLength(maxLength);
		}

		String pattern = (String) itemSchema.get(PATTERN_KEY);
		if(pattern != null) {
			builder.setPattern(pattern);
		}

		List<String> enums = (List<String>) itemSchema.get(ENUM_KEY);
		if(enums != null) {
			builder.setEnums(Set.copyOf(enums));
		}

		this.assertField(object, builder.build());
	}

	private void assertIntegerArrayFieldSchema(JsonObject object, String fieldKey, Map<String, Object> itemSchema, Integer minItems, Integer maxItems, boolean isOptional) {
		IntArrayField.Builder builder = new IntArrayField.Builder(fieldKey);

		if(isOptional) {
			builder.setOptional();
		}

		if(minItems != null) {
			builder.setMinItems(minItems);
		}

		if(maxItems != null) {
			builder.setMaxItems(maxItems);
		}

		Integer min = (Integer) itemSchema.get(MIN_VALUE_KEY);
		if(min != null) {
			builder.setMinValue(min);
		}

		Integer max = (Integer) itemSchema.get(MAX_VALUE_KEY);
		if(max != null) {
			builder.setMaxValue(max);
		}

		this.assertField(object, builder.build());
	}

	private void assertNumberArrayFieldSchema(JsonObject object, String fieldKey, Map<String, Object> itemSchema, Integer minItems, Integer maxItems, boolean isOptional) {
		NumberArrayField.Builder builder = new NumberArrayField.Builder(fieldKey);

		if(isOptional) {
			builder.setOptional();
		}

		if(minItems != null) {
			builder.setMinItems(minItems);
		}

		if(maxItems != null) {
			builder.setMaxItems(maxItems);
		}

		Number min = (Number) itemSchema.get(MIN_VALUE_KEY);
		if(min != null) {
			builder.setMinValue(min);
		}

		Number max = (Number) itemSchema.get(MAX_VALUE_KEY);
		if(max != null) {
			builder.setMaxValue(max);
		}

		this.assertField(object, builder.build());
	}

	private void assertObjectArrayFieldSchema(JsonObject object, String fieldKey, Map<String, Object> itemSchema, Integer minItems, Integer maxItems, boolean isOptional) {

		ObjectArrayField.Builder builder = new ObjectArrayField
			.Builder(fieldKey)
			.setValidator(innerObject -> {
				assertObjectSchema(innerObject, itemSchema);
			})
			.setOptional(isOptional);

		if(minItems != null) {
			builder.setMinItems(minItems);
		}

		if(maxItems != null) {
			builder.setMaxItems(maxItems);
		}

		this.assertField(object, builder.build());
	}

	public void loadOpenApiSpec(String pathToOpenApiSpec) {
		InputStream in = currentThread().getContextClassLoader().getResourceAsStream(pathToOpenApiSpec);
		this.openApiSpec = new Yaml().load(in);
	}

	public Map<String, Object> getSchema(String endpointPath, String endpointMethod, Integer statusCode) {
		/* Here an example of how to find the response schema.
		  paths:
		    /recurring-consents:
		      post:
		        responses:
		          '201':
		            $ref: '#/components/responses/RecurringConsentsPost'
		 */
		Map<String, Object> responseSchema = (Map<String, Object>) this.openApiSpec.get(PATHS_KEY);
		responseSchema = (Map<String, Object>) responseSchema.get(endpointPath);
		responseSchema = (Map<String, Object>) responseSchema.get(endpointMethod);
		responseSchema = (Map<String, Object>) responseSchema.get(RESPONSES_KEY);
		responseSchema = (Map<String, Object>) responseSchema.get(statusCode.toString());
		String referenceToResponseSchema = (String) responseSchema.get(REFERENCE_KEY);

		if(referenceToResponseSchema != null) {
			responseSchema = getSchema(referenceToResponseSchema);
		}
		/* Here an example of how to find the schema.
		      RecurringConsentsPost:
		        content:
		          application/jwt:
		            schema:
		              $ref: '#/components/schemas/ResponsePostRecurringConsent'
		 */
		responseSchema = (Map<String, Object>) responseSchema.get(CONTENT_KEY);
		responseSchema = (Map<String, Object>) responseSchema.get(getContentTypeKey(responseSchema));
		responseSchema = (Map<String, Object>) responseSchema.get(SCHEMA_KEY);
		referenceToResponseSchema = (String) responseSchema.get(REFERENCE_KEY);
		if(referenceToResponseSchema == null) {
			return responseSchema;
		}
		return getSchema(referenceToResponseSchema);
	}

	private String getContentTypeKey(Map<String, Object> contentSchema) {
		/* The content type is the only key in the schema.
		          application/jwt:
		            schema:
		              $ref: '#/components/schemas/ResponsePostRecurringConsent'
		 */
		return contentSchema.keySet().stream().findFirst().orElse("application/jwt");
	}

	/**
	 * @param referenceToSchema a String pointing to a schema. E.g. "#/components/parameters/page".
	 */
	public Map<String, Object> getSchema(String referenceToSchema) {
		String[] referenceToSchemaPath = referenceToSchema.replace("#/", "").split("/");
		Map<String, Object> schema = this.openApiSpec;
		for(String key : referenceToSchemaPath) {
			schema = (Map<String, Object>) schema.get(key);
		}
		return schema;
	}

	private List<String> getRequiredFields(Map<String, Object> fieldSchema) {
		Object required = fieldSchema.get(REQUIRED_KEY);
		if(required == null) {
			return Collections.emptyList();
		}

		return (List<String>) required;
	}

	/**
	 * The only way to set the condition result is by setting all the other parameters in {@link net.openid.conformance.condition.AbstractCondition#setProperties}.
	 * To do so, this method is being overridden to store these parameters.
	 */
	@Override
	public void setProperties(String testId, TestInstanceEventLog log, ConditionResult conditionResultOnFailure, String... requirements) {
		this.log = log;
		this.conditionResultOnFailure = conditionResultOnFailure;
		super.setProperties(testId, log, conditionResultOnFailure, requirements);
	}

	private void setConditionResultOnFailure(ConditionResult conditionResultOnFailure) {
		super.setProperties(getTestId(), this.log, conditionResultOnFailure, getRequirements().toArray(new String[0]));
	}


	protected void assertField1isRequiredWhenField2IsPresent(JsonObject data, String field1, String field2){
		if(isPresent(data, field2) && !isPresent(data, field1)) {
			throw error("Field '%s' is required when field '%s' is present".formatted(field1, field2));
		}
	}

	protected void assertField1isRequiredWhenField2IsNotPresent(JsonObject data, String field1, String field2){
		if(!isPresent(data, field2) && !isPresent(data, field1)) {
			throw error("Field '%s' is required when field '%s' is not present".formatted(field1, field2));
		}
	}

	protected void assertField1IsRequiredWhenField2HasValue2(JsonObject data, String field1, String field2, List<String> value2) {
		if(!isPresent(data,field2)){
			return;
		}
		JsonElement actualValue2 = findByPath(data, field2);
		if(value2.contains(OIDFJSON.getString(actualValue2)) && !isPresent(data, field1)) {
			throw error(String.format("%s is required when %s is %s", field1, field2, value2));
		}
	}

	protected void assertField1IsRequiredWhenField2HasValue2(JsonObject data, String field1, String field2, String value2) {
		if(!isPresent(data,field2)){
			return;
		}
		JsonElement actualValue2 = findByPath(data, field2);
		if(OIDFJSON.getString(actualValue2).equals(value2) && !isPresent(data, field1)) {
			throw error(String.format("%s is required when %s is %s", field1, field2, value2));
		}
	}

	protected void assertField1IsRequiredWhenField2HasNotValue2(JsonObject data, String field1, String field2, List<String> value2) {
		if(!isPresent(data, field1) && (!isPresent(data, field2) || !value2.contains(OIDFJSON.getString(findByPath(data, field2))))){
			throw error(String.format("%s is required when %s is not %s", field1, field2, value2));
		}
	}

	protected void assertField1IsRequiredWhenField2HasNotValue2(JsonObject data, String field1, String field2, String value2) {
		if(!isPresent(data, field1) && (!isPresent(data, field2) || !OIDFJSON.getString(findByPath(data, field2)).equals(value2))){
			throw error(String.format("%s is required when %s is not %s", field1, field2, value2));
		}
	}

	protected void assertField1IsRequiredWhenField2HasValue2(JsonObject data, String field1, String field2, Boolean value2) {
		if(!isPresent(data,field2)){
			return;
		}
		JsonElement actualValue2 = findByPath(data, field2);
		if (OIDFJSON.getBoolean(actualValue2) == value2 && !isPresent(data, field1)) {
			throw error(String.format("%s is required when %s is %s", field1, field2, value2));
		}
	}

	protected void assertField1IsMutuallyExclusiveWithField2(JsonObject data, String field1, String field2) {
		boolean isField1Present = isPresent(data, field1);
		boolean isField2Present = isPresent(data, field2);

		if (isField1Present && isField2Present) {
			throw error(String.format("%s and %s cannot both be present; they are mutually exclusive.", field1, field2));
		}
	}

	protected void assertField2IsNotPresentWhenField1HasValue1(JsonObject data, String field1, String value1, String field2) {
		JsonElement actualValue1 = findByPath(data, field1);

		if (OIDFJSON.getString(actualValue1).equals(value1) && isPresent(data, field2)) {
			throw error(String.format("%s cannot be present when %s has the value %s", field2, field1, value1));
		}
	}

	public enum ResponseEnvKey {
		FullResponseEnvKey("resource_endpoint_response_full"),
		FullConsentResponseEnvKey("consent_endpoint_response_full");

		private final String key;
		ResponseEnvKey(String key) {
			this.key = key;
		}

		public String getKey() {
			return this.key;
		}
	}

}
