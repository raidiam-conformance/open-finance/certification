package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class ExtractConsentIdFromResourceEndpointResponseFull extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	@PostEnvironment(strings = "consent_id")
	public Environment evaluate(Environment env) {
		JsonObject body;
		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		String accountRequestId = Optional.ofNullable(body.getAsJsonObject("data").get("consentId"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Couldn't find data.consentId in the response", args("body", body)));

		env.putString("consent_id", accountRequestId);

		logSuccess("Extracted the consent id", args("consent_id", accountRequestId));

		return env;
	}


}
