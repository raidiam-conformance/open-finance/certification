package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddValidBusinessEntityIdentification;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforceAbsenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsManualObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureBusinessEntityIdentificationIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.EditRecurringPaymentsConsentBodyToSetFourCreditors;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.addCnpjToPayment.AddFirstCnpjToPaymentRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.addCnpjToPayment.AddSecondCnpjToPaymentRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.GenerateNewE2EID;

public abstract class AbstractAutomaticPaymentsApiSweepingAccountsRootCnpjTestModule extends AbstractAutomaticPaymentsFunctionalTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToSetFourCreditors.class);
		callAndStopOnFailure(AddRiskSignalsManualObjectToPixPaymentBody.class);
	}

	@Override
	protected void setupResourceEndpoint() {
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsureBusinessEntityIdentificationIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no 'Payment consent - Business Entity CNPJ' was informed.");
		}
		callAndStopOnFailure(AddValidBusinessEntityIdentification.class);
		super.setupResourceEndpoint();
	}

	@Override
	protected void requestProtectedResource() {
		runInBlock("Call pix/payments endpoint with the first creditor's CNPJ", () -> {
			callAndStopOnFailure(AddFirstCnpjToPaymentRequestBody.class);
			call(getPixPaymentSequence());
		});
		runInBlock("Validate response", this::validateResponse);

		runInBlock("Call pix/payments endpoint with the second creditor's CNPJ", () -> {
			callAndStopOnFailure(AddSecondCnpjToPaymentRequestBody.class);
			userAuthorisationCodeAccessToken();
			callAndStopOnFailure(GenerateNewE2EID.class);
			call(getPixPaymentSequence());
		});
		runInBlock("Validate response", this::validateResponse);
	}

	@Override
	protected void validateResponse() {
		call(postPaymentValidationSequence());
	}
}
