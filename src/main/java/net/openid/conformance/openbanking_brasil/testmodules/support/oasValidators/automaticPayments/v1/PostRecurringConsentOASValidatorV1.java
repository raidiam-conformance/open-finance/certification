package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1;

import com.google.gson.JsonObject;
import org.springframework.http.HttpMethod;

import java.util.List;


public class PostRecurringConsentOASValidatorV1 extends AbstractRecurringConsentOASValidatorV1 {
	@Override
	protected String getEndpointPath() {
		return "/recurring-consents";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected void assertData(JsonObject data) {

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"debtorAccount.issuer",
			"debtorAccount.accountType",
			List.of("CACC", "SVGS")
		);

		super.assertData(data);
	}

}
