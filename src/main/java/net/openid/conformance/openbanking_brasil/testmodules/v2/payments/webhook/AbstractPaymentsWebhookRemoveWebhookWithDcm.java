package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddSoftwareStatementToClientConfigurationRequest;
import net.openid.conformance.condition.client.BuildRequestObjectPostToPAREndpoint;
import net.openid.conformance.condition.client.CallClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckClientConfigurationUriFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckClientIdFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckRedirectUrisFromClientConfigurationEndpoint;
import net.openid.conformance.condition.client.CheckRegistrationClientEndpointContentType;
import net.openid.conformance.condition.client.CheckRegistrationClientEndpointContentTypeHttpStatus200;
import net.openid.conformance.condition.client.CreateClientConfigurationRequestFromDynamicClientRegistrationResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveConsentIdFromClientScopes;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.AddOpenIdScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;

public abstract class AbstractPaymentsWebhookRemoveWebhookWithDcm extends AbstractBrazilDCRPaymentsWebhook {
	private boolean secondAuthCodeFlow = false;

	private boolean firstPaymentWebhookReceived = false;

	@Override
	public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
		Boolean expectingPaymentWebhook = env.getBoolean("payment_webhook_received");
		Boolean expectingPaymentConsentWebhook = env.getBoolean("payment_consent_webhook_received");
		 if (expectingPaymentWebhook != null && path.matches("^(open-banking\\/webhook\\/v\\d+\\/payments\\/v\\d+\\/pix\\/payments\\/)([a-zA-Z0-9][a-zA-Z0-9\\-]{0,99})$") && expectingPaymentWebhook) {
			if (!firstPaymentWebhookReceived){
				firstPaymentWebhookReceived = true;
				return new ResponseEntity<Object>("", HttpStatus.SERVICE_UNAVAILABLE);
			}
			 addWebhookToObjectInEnv(env, false, true, "webhooks_received_payment", requestParts, path);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		 } else if (expectingPaymentConsentWebhook != null && path.matches("^(open-banking\\/webhook\\/v\\d+\\/payments\\/v\\d+\\/consents\\/)(urn:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+)$") && expectingPaymentConsentWebhook){
			 addWebhookToObjectInEnv(env, true, false, "webhooks_received_consent", requestParts, path);
			 return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		 } else {
			 throw new TestFailureException(getId(), "Got a webhook response we weren't expecting");
		}
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		initialiseWebhookObject("consent");
		initialiseWebhookObject("payment");
		callAndStopOnFailure(AddOpenIdScope.class);
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
		setScheduledPaymentDateTime();
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected void requestProtectedResource() {
		super.requestProtectedResource();
		if (!secondAuthCodeFlow){
			callAndContinueOnFailure(RemoveConsentIdFromClientScopes.class, Condition.ConditionResult.FAILURE);
			doDcm();
			callAndStopOnFailure(DoublePaymentAmountSetInConfig.class);
			callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
			callAndStopOnFailure(WaitForSpecifiedTimeFromConfigInSeconds.class);
			ConditionSequence conditionSequence = createOBBPreauthSteps();
			call(conditionSequence);
			performSecondFlow();
		}
		if (secondAuthCodeFlow){
			repeatSequence(this::getRepeatSequence)
				.untilTrue("payment_proxy_check_for_reject")
				.trailingPause(30)
				.times(5)
				.validationSequence(this::getPaymentValidationSequence)
				.onTimeout(sequenceOf(
					condition(TestTimedOut.class),
					condition(ChuckWarning.class)))
				.run();
			validateFinalState();
		}
	}
	@Override
	protected void validatePaymentStatus(){
		if (!secondAuthCodeFlow){
			callAndStopOnFailure(EnsurePaymentStatusWasAcsc.class);
		}
	}

	protected void performSecondFlow() {

		eventLog.startBlock(currentClientString() + "Make request to authorization endpoint");

		createAuthorizationRequest();

		createAuthorizationRequestObject();

		if (isPar.isTrue()) {
			callAndStopOnFailure(BuildRequestObjectPostToPAREndpoint.class);
			addClientAuthenticationToPAREndpointRequest();
			performParAuthorizationRequestFlow();
		} else {
			buildRedirect();
			performRedirect();
		}
	}
	@Override
	protected void onPostAuthorizationFlowComplete() {
		if(!secondAuthCodeFlow) {
			secondAuthCodeFlow = true;
			return;
		}

		super.onPostAuthorizationFlowComplete();
	}
	protected void doDcm() {
		eventLog.startBlock("Updating client with webhook");
		callAndStopOnFailure(CreateClientConfigurationRequestFromDynamicClientRegistrationResponse.class);
		callAndStopOnFailure(AddSoftwareStatementToClientConfigurationRequest.class);
		callAndStopOnFailure(RemoveWebhookFromClientConfigurationRequest.class);
		callClientConfigurationEndpoint();
		eventLog.endBlock();
	}
	@Override
	protected void waitForWebhookResponse() {
		if (!secondAuthCodeFlow){
			waitForSpecifiedWebhookResponse();
		}
	}

	public void waitForSpecifiedWebhookResponse(){
		long delaySeconds = 5;
		eventLog.startBlock(currentClientString() + "The test will now wait for the webhook to be received");
		for (int attempts = 0; attempts < 24; attempts++) {
			eventLog.log(getId(), "Waiting for webhook to be received at with an interval of " + delaySeconds + " seconds");
			setStatus(Status.WAITING);
			try {
				Thread.sleep(delaySeconds * 1000);
			} catch (InterruptedException e) {
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}
			setStatus(Status.RUNNING);
		}
		eventLog.endBlock();
	}
	@Override
	protected void callClientConfigurationEndpoint() {

		callAndStopOnFailure(CallClientConfigurationEndpoint.class);
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentTypeHttpStatus200.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckRegistrationClientEndpointContentType.class, Condition.ConditionResult.FAILURE, "OIDCD-4.3");
		callAndContinueOnFailure(CheckClientIdFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckRedirectUrisFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationUriFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndContinueOnFailure(CheckClientConfigurationAccessTokenFromClientConfigurationEndpoint.class, Condition.ConditionResult.FAILURE, "RFC7592-3");
		callAndStopOnFailure(CheckClientConfigurationDoesNotContainWebhookUri.class);
	}

	@Override
	protected void validatePaymentConsentWebhooks() {
		if (!secondAuthCodeFlow){
			env.putBoolean("payment_consent_webhook_received", false);
			callAndStopOnFailure(ValidatePaymentConsentWebhooks.class);
		}
	}

	@Override
	protected void validatePaymentWebhooks() {
		if (!secondAuthCodeFlow){
			env.putBoolean("payment_webhook_received", false);
			callAndStopOnFailure(ValidatePaymentWebhooks.class);
		}
	}

	@Override
	protected void enablePaymentWebhook() {
		if (!secondAuthCodeFlow){
			env.putString("time_of_payment_request", Instant.now().toString());
			env.putBoolean("payment_webhook_received", true);
		}
	}

	@Override
	protected void enablePaymentConsentWebhook() {
		if (!secondAuthCodeFlow){
			env.putString("time_of_consent_request", Instant.now().toString());
			env.putBoolean("payment_consent_webhook_received", true);
		}
	}
}
