package net.openid.conformance.openbanking_brasil.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToGetProductsNChannelsApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.admin.v2.GetMetricsAdminOASValidatorV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(testName = "admin_api_core_test-module",
	displayName = "Validate structure of admin API resources",
	summary = "Validates the structure of admin API resources",
	profile = OBBProfile.OBB_PROFILE)
public class AdminApiTestModule extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate Admin Metrics response", () -> {
			callAndStopOnFailure(PrepareToGetProductsNChannelsApi.class);
			preCallResource();
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetMetricsAdminOASValidatorV2.class, Condition.ConditionResult.FAILURE);
		});
	}
}
