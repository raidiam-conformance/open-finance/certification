package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class AddWebhookUrisToClientConfigurationRequest extends AbstractCondition {

	@Override
	@PreEnvironment(required = { "registration_client_endpoint_request_body" })
	@PostEnvironment(required = "registration_client_endpoint_request_body")
	public Environment evaluate(Environment env) {
		Optional<JsonObject> optionalRequest = Optional.ofNullable(env.getObject("registration_client_endpoint_request_body"));
		JsonObject request = optionalRequest.orElseThrow(() -> error("Could not find registration_client_endpoint_request_body")).getAsJsonObject();

		Optional<String> optionalWebhookUri = Optional.ofNullable(env.getString("software_api_webhook_uri"));
		String webhookUri = optionalWebhookUri.orElseThrow(() -> error("Unable to find webhook_uri to add to the client configuration request"));

		JsonArray webhookUris = new JsonArray();
		webhookUris.add(webhookUri);
		request.add("webhook_uris", webhookUris);

		env.putObject("registration_client_endpoint_request_body", request);
		log("Added webhook_uris to client configuration request", args("client_configuration_request", request));

		return env;
	}
}
