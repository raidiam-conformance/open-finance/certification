package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.abstractModule.AbstractAutomaticPaymentsApiAutomaticPixWebhookTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringConsentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringPaymentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixListOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PatchRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_automatic-pix-webhook_test-module_v2",
	displayName = "automatic-payments_api_automatic-pix-webhook_test-module_v2",
	summary = "Ensure that the tested institution has correctly implemented the webhook notification endpoint and that this endpoint is correctly called when a payments reaches the SCHD\n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;\n" +
		"• Obtain a SSA from the Directory\n" +
		"• Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;”]\n" +
		"• Expect a 201 - Validate Response\n" +
		"• Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"• Set the recurring consents webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v1/automatic-payments/v2/recurring-consents/{recurringConsentId}, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Call the POST recurring-consents endpoint with automatic pix fields, with startDateTime as D+1, period as SEMANAL, a fixed Amount of 0.50 BRL, and sending firstPayment information with amount as 1 BRL, date as D+0\n" +
		"• Expects 201 - Validate Response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET recurring-consents Endpoint\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"• Call the POST recurring-payments endpoint with the information defined firstPayment payload\n" +
		"• Expects 201 - Validate response\n" +
		"• Set the recurring payments webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v1/automatic-payments/v2/pix/recurring-payments/{recurringPaymentId} where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is ACSC\n" +
		"• Call the POST recurring-payments endpoint with the a payment for D+2, and amount as 0.5 BRL\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is SCHD\n" +
		"• Call the PATCH recurring-payments {recurringPaymentId} endpoint\n" +
		"• Expect 201 - Validate Response\n" +
		"• Call the GET recurring-payments sending the recurringConsentID on the header\n" +
		"Expect 200 - Validate Response and both paymentIDs are retrieved, with the correct status for it, as ACSC and CANC\n" +
		"• Expect three incoming message, all on the same endpoint related to the three different status - ACSC, SCHD and CANC. All must be mtls protected and can be received on any order - Wait 60 seconds for the messages to be returned\n" +
		"• For all webhook calls - Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"• Call the Delete Registration Endpoint\n" +
		"• Expect a 204 - No Content",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.contractDebtorName",
		"resource.contractDebtorIdentification",
		"resource.creditorAccountIspb",
		"resource.creditorAccountIssuer",
		"resource.creditorAccountNumber",
		"resource.creditorAccountAccountType",
		"resource.creditorName",
		"resource.creditorCpfCnpj",
		"resource.webhookWaitTime",
		"directory.client_id",
		"directory.discoveryUrl"
	}
)
public class AutomaticPaymentsApiAutomaticPixWebhookTestModuleV2 extends AbstractAutomaticPaymentsApiAutomaticPixWebhookTestModule {

	@Override
	protected Class<? extends Condition> patchPaymentValidator() {
		return PatchRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentsByConsentIdValidator() {
		return GetRecurringPaymentPixListOASValidatorV2.class;
	}

	@Override
	protected ConditionSequence getUrlsFromAuthServerCondition() {
		return sequenceOf(
			condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class),
			condition(GetAutomaticPaymentPixRecurringV2Endpoint.class)
		);
	}

	@Override
	protected Class<? extends Condition> setPaymentConsentWebhookCreator() {
		return CreateRecurringConsentWebhookV2.class;
	}

	@Override
	protected Class<? extends Condition> setPaymentWebhookCreator() {
		return CreateRecurringPaymentWebhookV2.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setGetPaymentValidator() {
		return GetRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPostPaymentValidator() {
		return PostRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}
}
