package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringConsentIdToQuery;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreOneAcscAndOneCancPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToTwoDaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo50Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.AbstractEnsurePaymentStatusWasX;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.AddMapFromPaymentIdToEndToEndId;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPatchRecurringPaymentEndpointSequence;

public abstract class AbstractAutomaticPaymentsAutomaticPixHappyCorePathTestModule extends AbstractAutomaticPaymentsAutomaticPixTestModule {
	/**
	 * This class should be extended by automatic pix tests that follow the steps bellow (Steps with ? are optional):
	 *     - Call the POST recurring-consents endpoint with automatic pix fields, with referenceStartDate as D+1, period as {value}, a fixed Amount of {value}, and sending firstPayment information {value}
	 *     - Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION
	 *     - Redirect the user to authorize consent
	 *     - Call the GET recurring-consents endpoint
	 *     - Expect 201 - Validate Response and ensure status is AUTHORISED
	 *     - Call the POST recurring-payments endpoint with the amount as {value}, and date as {value}
	 *     - Expect 201 - Validate Response
	 *     - Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD
	 *     - Call the GET recurring-payments {recurringPaymentId}
	 *     - Expect 200 - Validate Response and ensure status is {value}
	 *     - Call the POST recurring-payments endpoint with the a payment for {value}, and amount as 0.5 BRL
	 *     - Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD or ACCP
	 *     - Call the GET recurring-payments {recurringPaymentId}
	 *     - Expect 200 - Validate Response and ensure status is SCHD
	 *     ? Call the PATCH {value} endpoint
	 *     ? Expect {value} - Validate Response
	 *     - Call the GET recurring-payments sending the recurringConsentID on the header
	 *     - Expect 200 - Validate Response and both paymentIDs are retrieved, with the correct status for it, as {value} and {value}, with rejectionReason as {value}
	 * In order to properly create the recurring-consents and recurring-payments request bodies, the user will need to provide the following fields:
	 *     - resource.debtorAccountIspb
	 *     - resource.debtorAccountIssuer
	 *     - resource.debtorAccountNumber
	 *     - resource.debtorAccountType
	 *     - resource.contractDebtorName
	 *     - resource.contractDebtorIdentification
	 *     - resource.creditorAccountIspb
	 *     - resource.creditorAccountIssuer
	 *     - resource.creditorAccountNumber
	 *     - resource.creditorAccountAccountType
	 *     - resource.creditorName
	 *     - resource.creditorCpfCnp
	 */

	protected boolean isFirstPayment = true;

	protected abstract AbstractSetPaymentReference setPaymentReferenceForSecondPayment();

	@Override
	protected void validateResponse() {
		super.validateResponse();
		callAndStopOnFailure(AddMapFromPaymentIdToEndToEndId.class);
		stepsAfterFirstPayment();
	}

	protected void stepsAfterFirstPayment() {
		configureSecondPayment();
		postAndValidateSecondPayment();
		patchSecondPayment();
		getAndValidateRecurringPaymentsEndpoint();
	}

	protected void configureSecondPayment() {
		isFirstPayment = false;
		configureSecondPaymentDate();
		callAndStopOnFailure(setPaymentReferenceForSecondPayment().getClass());
		configureSecondPaymentAmount();
	}

	protected void configureSecondPaymentDate() {
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToTwoDaysInTheFuture.class);
	}

	protected void configureSecondPaymentAmount() {
		callAndStopOnFailure(SetAutomaticPaymentAmountTo50Cents.class);
	}

	protected void postAndValidateSecondPayment() {
		userAuthorisationCodeAccessToken();
		runInBlock("Call pix/payments endpoint", () -> {
			callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
			call(getPixPaymentSequence());
			callAndStopOnFailure(AddMapFromPaymentIdToEndToEndId.class);
		});
		runInBlock("Validate response", () -> {
			call(postPaymentValidationSequence());
			runRepeatSequence();
			validateFinalState();
		});
	}

	@Override
	protected void validateFinalState() {
		if (isFirstPayment) {
			callAndStopOnFailure(getEnsureFirstPaymentStatusCondition().getClass());
		} else {
			callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class, Condition.ConditionResult.FAILURE);
		}
	}

	protected AbstractEnsurePaymentStatusWasX getEnsureFirstPaymentStatusCondition() {
		return new EnsurePaymentStatusWasAcsc();
	}

	protected void patchSecondPayment() {
		useClientCredentialsAccessToken();
		runInBlock("Call PATCH recurring-payment endpoint", () -> {
			call(new CallPatchRecurringPaymentEndpointSequence());
			callAndContinueOnFailure(patchPaymentValidator(), Condition.ConditionResult.FAILURE);
		});
		validateVersionHeader();
	}

	protected void getAndValidateRecurringPaymentsEndpoint() {
		runInBlock("Call GET recurring-payments endpoint to check all payments", () -> {
			callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
			callAndStopOnFailure(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
			callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
			callAndStopOnFailure(AddRecurringConsentIdToQuery.class);
			callAndStopOnFailure(CallProtectedResource.class);
			callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
			callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
			callAndContinueOnFailure(getPaymentsByConsentIdValidator(), Condition.ConditionResult.FAILURE);
			validateVersionHeader();
			callAndContinueOnFailure(getEnsureThereArePaymentsCondition().getClass(), Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		if (isFirstPayment) {
			return super.getPaymentPollStatusCondition();
		}
		return CheckPaymentPollStatusRcvdOrAccpV2.class;
	}

	protected AbstractEnsureThereArePayments getEnsureThereArePaymentsCondition() {
		return new EnsureThereAreOneAcscAndOneCancPayments();
	}

	protected abstract Class<? extends Condition> patchPaymentValidator();
	protected abstract Class<? extends Condition> getPaymentsByConsentIdValidator();
}
