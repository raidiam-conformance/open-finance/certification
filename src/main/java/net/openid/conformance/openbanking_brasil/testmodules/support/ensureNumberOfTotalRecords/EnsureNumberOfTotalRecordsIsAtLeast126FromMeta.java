package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

public class EnsureNumberOfTotalRecordsIsAtLeast126FromMeta extends AbstractEnsureNumberOfTotalRecordsFromMeta {
	@Override
	protected int getTotalRecordsComparisonAmount() {
		return 126;
	}

	@Override
	protected TotalRecordsComparisonOperator getComparisonMethod() {
		return TotalRecordsComparisonOperator.AT_LEAST;
	}
}
