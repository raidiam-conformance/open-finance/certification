package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.abstractModule;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.AbstractAutomaticPaymentsDCRWebhookTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooksOptionalAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.testmodule.OIDFJSON;

public abstract class AbstractAutomaticPaymentsApiWebhookAcscTestModule extends AbstractAutomaticPaymentsDCRWebhookTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		env.putString("recurring_total_amount", "300.00");
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected void validatePaymentConsentWebhooks(){
		JsonArray webhooks = env.getObject("webhooks_received_recurring_consent").get("webhooks").getAsJsonArray();
		int numberOfWebhooks = 0;
		for (JsonElement webhook : webhooks) {
			String type = OIDFJSON.getString(webhook.getAsJsonObject().get("type"));
			if (type.equals("recurring-consent")) {
				numberOfWebhooks++;
			}
		}

		env.putBoolean("recurring_payment_consent_webhook_received", false);
		if (numberOfWebhooks > 1) {
			callAndStopOnFailure(ValidateRecurringConsentWebhooksOptionalAmount.class);
		} else {
			callAndStopOnFailure(ValidateRecurringConsentWebhooks.class);
		}
	}

	@Override
	protected void finalStepsRequestProtectedResource() {
		fetchConsentToCheckStatus("CONSUMED", new EnsurePaymentConsentStatusWasConsumed());
		waitForWebhookResponse();
		validatePaymentConsentWebhooks();
		validatePaymentWebhooks();
	}
}
