package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PostEnrollmentsFidoSignOptionsErrorEnum {
    STATUS_VINCULO_INVALIDO, RP_INVALIDA, STATUS_CONSENTIMENTO_INVALIDO;

    public static Set<String> toSet(){
        return Stream.of(values())
            .map(Enum::name)
            .collect(Collectors.toSet());
    }
}
