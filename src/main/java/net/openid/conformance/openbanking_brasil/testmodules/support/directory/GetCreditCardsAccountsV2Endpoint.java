package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetCreditCardsAccountsV2Endpoint extends AbstractGetXFromAuthServer {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/credit-cards-accounts\\/v\\d+\\/accounts)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "credit-cards-accounts";
	}

	@Override
	protected String getApiVersionRegex() {
		return  "^(2.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
