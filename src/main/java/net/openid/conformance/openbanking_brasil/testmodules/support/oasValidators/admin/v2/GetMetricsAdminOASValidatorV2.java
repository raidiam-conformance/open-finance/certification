package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.admin.v2;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api url: https://raw.githubusercontent.com/OpenBanking-Brasil/draft-openapi/main/swagger-apis/admin/2.0.1.yml
 * Api endpoint: GET /metrics
 * Api version: 2.0.1
 */
@ApiName("Admin Api GET Metrics")
public class GetMetricsAdminOASValidatorV2 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/admin/swagger-admin-2.0.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/metrics";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
