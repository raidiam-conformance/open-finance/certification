package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

public class ValidateConsents422ExpiracaoInvalida extends AbstractValidateConsent422ErrorCode{
	@Override
	protected String getErrorCode() {
		return "DATA_EXPIRACAO_INVALIDA";
	}
}
