package net.openid.conformance.openbanking_brasil.testmodules;

import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.Validate529Or201ReturnedFromConsents;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.SignedPaymentConsentValidationSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilPaymentConsentProdValues;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractFvpDcrPaymentsConsentsTestModule extends AbstractFvpDcrXConsentsTestModule {
	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilPaymentConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		eventLog.startBlock("Calling Payment Consents API");
		ConditionSequence paymentValidationSequence = new SignedPaymentConsentValidationSequence();
		paymentValidationSequence.insertBefore(EnsureContentTypeApplicationJwt.class, condition(EnsureHttpStatusCodeIs201.class).onFail(Condition.ConditionResult.FAILURE));
		ConditionSequenceRepeater sequenceRepeater = new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager, this::getPaymentsConsentSequence)
			.untilTrue("correct_consent_response")
			.times(3)
			.trailingPause(10)
			.onTimeout(sequenceOf(condition(EnsureConsentResponseCodeWas201.class).onFail(Condition.ConditionResult.FAILURE)));
		sequenceRepeater.run();
		call(paymentValidationSequence);
		eventLog.endBlock();
	}

	@Override
	protected ConditionSequence getPaymentsConsentSequence() {
		return super.getPaymentsConsentSequence()
			.replace(EnsureConsentResponseCodeWas201.class, condition(Validate529Or201ReturnedFromConsents.class).onFail(Condition.ConditionResult.FAILURE));
	}
}
