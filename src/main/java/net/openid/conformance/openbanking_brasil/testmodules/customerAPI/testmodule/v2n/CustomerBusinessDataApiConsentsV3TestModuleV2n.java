package net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractCustomerBusinessDataApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetBusinessIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessIdentificationOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessQualificationResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessRelationsResponseOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "customer-business_api_core_consents_v3_test-module_v2-2",
	displayName = "Validate structure of all business customer data API resources V2 with consents V3",
	summary = "Validates the structure of all business customer data API resources V2 with consents V3\n" +
		"• Creates a Consent with the customer business permissions (\"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\",\"CUSTOMERS_BUSINESS_ADITTIONALINFO_READ\",\"RESOURCES_READ\")\n" +
		"• Expects a success 201 - Check all of the fields sent on the consent API is spec compliant \n" +
		"• Calls GET Personal Qualifications resources V2\n" +
		"• Expects a success 200\n" +
		"6",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class CustomerBusinessDataApiConsentsV3TestModuleV2n extends AbstractCustomerBusinessDataApiTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getBusinessQualificationResponseValidator() {
		return BusinessQualificationResponseOASValidatorV2.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getBusinessIdentificationResponseValidator() {
		return BusinessIdentificationOASValidatorV2.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getBusinessRelationsResponseValidator() {
		return BusinessRelationsResponseOASValidatorV2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class), condition(GetBusinessIdentificationsV2Endpoint.class));
	}
}
