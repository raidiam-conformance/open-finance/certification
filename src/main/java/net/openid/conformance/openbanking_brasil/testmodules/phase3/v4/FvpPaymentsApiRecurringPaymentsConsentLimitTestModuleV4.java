package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractFvpPaymentsApiRecurringPaymentsConsentLimitTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments_api_recurring-payments-consent-limit_test-module_v4",
	displayName = "fvp-payments_api_recurring-payments-consent-limit_test-module_v4",
	summary = "Ensure that consent for recurring payments cannot be created if it exceeds the consent limit rules\n" +
		"• Create a client by performing a DCR against the provided server - Expect Success\n" +
		"• Call the POST Consents endpoints with the schedule.daily.startDate field set as D+672 and schedule.daily.quantity as 60\n" +
		"• Expects 422 - DATA_PAGAMENTO_INVALIDA\n" +
		"• Call the POST Consents endpoints with the schedule.weekly.dayOfWeek field set as SEGUNDA_FEIRA, schedule.weekly.startDate field set as D+317 and schedule.weekly.quantity as 60\n" +
		"• Expects 422 - DATA_PAGAMENTO_INVALIDA\n" +
		"• Call the POST Consents endpoints with the schedule.monthly.dayOfMonth field set as 01, schedule.monthly.startDate field set as D+150 and schedule.monthly.quantity as 21\n" +
		"• Expects 422 - DATA_PAGAMENTO_INVALIDA\n" +
		"• Call the POST Consents endpoints with the schedule.custom.dates field set as D+100 and D+732\n" +
		"• Expects 422 - DATA_PAGAMENTO_INVALIDA\n" +
		"• Call the POST Consents endpoints with 2 identical items at schedule.custom.dates field, set as D+1 and D+1\n" +
		"• Expects 422 - PARAMETRO_INVALIDO\n" +
		"• Call the POST Consents endpoints with the schedule.daily.startDate field set as D+10 and schedule.daily.quantity as 61\n" +
		"• Expects 422 - PARAMETRO_INVALIDO\n" +
		"• Call the POST Consents endpoints with the schedule.weekly.dayOfWeek field set as SEGUNDA_FEIRA, schedule.weekly.startDate field set as D+10 and schedule.weekly.quantity as 61\n" +
		"• Expects 422 - PARAMETRO_INVALIDO\n" +
		"• Delete the created client",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpPaymentsApiRecurringPaymentsConsentLimitTestModuleV4 extends AbstractFvpPaymentsApiRecurringPaymentsConsentLimitTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetPaymentConsentsV4Endpoint.class;
	}
}
