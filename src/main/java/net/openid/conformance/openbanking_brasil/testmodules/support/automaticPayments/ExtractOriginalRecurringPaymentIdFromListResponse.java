package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class ExtractOriginalRecurringPaymentIdFromListResponse extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	@PostEnvironment(strings = "payment_id")
	public Environment evaluate(Environment env) {
		JsonObject response = extractResponseFromEnv(env);
		JsonArray data = response.getAsJsonArray("data");
		JsonObject originalPayment = data.get(0).getAsJsonObject();
		String recurringPaymentId = extractRecurringPaymentIdFromPaymentData(originalPayment);
		env.putString("payment_id", recurringPaymentId);
		logSuccess("Successfully extract recurringPaymentId from original recurring-payment",
			args("recurringPaymentId", recurringPaymentId));

		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	private String extractRecurringPaymentIdFromPaymentData(JsonObject paymentData) {
		return Optional.ofNullable(paymentData.get("recurringPaymentId"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find recurringPaymentId in the original payment data",
				args("payment data", paymentData)));
	}
}
