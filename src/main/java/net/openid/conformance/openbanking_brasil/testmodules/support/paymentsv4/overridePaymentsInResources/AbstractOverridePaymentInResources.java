package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.overridePaymentsInResources;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractOverridePaymentInResources extends AbstractCondition {

	protected static final String END_TO_END_ID_REGEX = "E(.{8})(\\d{8})(\\d{4}.{11})";
	protected static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonArray paymentsData = env
			.getElementFromObject("resource", "brazilPixPayment")
			.getAsJsonObject()
			.getAsJsonArray("data");

		JsonObject schedule = Optional.ofNullable(env.getObject("resource"))
			.map(resource -> resource.getAsJsonObject("brazilPaymentConsent"))
			.map(brazilPaymentConsent -> brazilPaymentConsent.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("payment"))
			.map(payment -> payment.getAsJsonObject("schedule"))
			.orElseThrow(() -> error("Could not extract the schedule from the payment in resource"))
			.getAsJsonObject();

		overridePaymentsData(paymentsData, schedule);

		return env;
	}

	protected abstract void overridePaymentsData(JsonArray data, JsonObject schedule);

	protected String generateEndToEndId(String oldEndToEndId, String dateString) {
		Pattern pattern = Pattern.compile(END_TO_END_ID_REGEX);
		Matcher matcher = pattern.matcher(oldEndToEndId);
		if (matcher.matches()) {
			String prefix = matcher.group(1);
			String restOfString = matcher.group(3);

			return "E" + prefix + dateString.replace("-", "") + restOfString;
		}
		throw error("endToEndId is not in a valid format: " + oldEndToEndId);
	}
}
