package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;

import java.time.LocalDate;
import java.time.ZoneOffset;

public class CreateAutomaticRecurringConfigurationObjectSemanalReferenceStartDate5DaysInTheFuture extends AbstractCreateAutomaticRecurringConfigurationObject {

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.SEMANAL;
	}

	@Override
	protected String getFixedAmount() {
		return null;
	}

	@Override
	protected String getMinimumVariableAmount() {
		return "0.50";
	}

	@Override
	protected String getMaximumVariableAmount() {
		return "0.80";
	}

	@Override
	protected String getReferenceStartDate() {
		LocalDate today = LocalDate.now(ZoneOffset.UTC).plusDays(5);
		return today.format(DATE_FORMATTER);
	}
}
