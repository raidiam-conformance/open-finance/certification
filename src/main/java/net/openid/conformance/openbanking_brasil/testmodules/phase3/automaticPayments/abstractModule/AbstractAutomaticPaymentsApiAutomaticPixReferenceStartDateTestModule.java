package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixUnhappyCorePathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalReferenceStartDate5DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixReferenceStartDateTestModule extends AbstractAutomaticPaymentsAutomaticPixUnhappyCorePathTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalReferenceStartDate5DaysInTheFuture();
	}

	@Override
	protected AbstractEnsureRecurringPaymentsRejectionReason rejectionReasonCondition() {
		return new EnsureRecurringPaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento();
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorCodeCondition() {
		return new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento();
	}
}
