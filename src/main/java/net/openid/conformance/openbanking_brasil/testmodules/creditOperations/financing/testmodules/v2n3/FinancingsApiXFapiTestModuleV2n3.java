package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.AbstractFinancingsApiXFapiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsIdentificationV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsListV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsPaymentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsScheduledInstalmentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsWarrantiesV2n3OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "financings_api_x-fapi_test-module_v2-3",
	displayName = "Ensure that the x-fapi-interaction-id is required at the request for all endpoints",
	summary = "Ensure that the x-fapi-interaction-id is required at the request for all endpoints\n" +
		"• Call the POST Consents endpoint with the Credit Operations Permission Group\n" +
		"• Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION\n" +
		"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"• Call the GET Consents endpoint\n" +
		"• Expects 200 - Validate response and ensure status is AUTHORISED\n" +
		"• Call the GET Contracts Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Contracts Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Contracts Endpoint with the x-fapi-interaction-id\n" +
		"• Expects 200 - Validate response and extract contractId\n" +
		"• Call the GET contracts/{contractId} Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET contracts/{contractId} Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Warranties Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Warranties Endpoint with invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Scheduled Installments Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Scheduled Installments Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Payments Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Payments Endpoint with an invalid the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class FinancingsApiXFapiTestModuleV2n3 extends AbstractFinancingsApiXFapiTestModule {

	@Override
	protected Class<? extends Condition> getContractsListValidator() {
		return GetFinancingsListV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> getContractIdentificationValidator() {
		return GetFinancingsIdentificationV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> getContractWarrantiesValidator() {
		return GetFinancingsWarrantiesV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> getContractScheduledInstalmentsValidator() {
		return GetFinancingsScheduledInstalmentsV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> getContractPaymentsValidator() {
		return GetFinancingsPaymentsV2n3OASValidator.class;
	}

}
