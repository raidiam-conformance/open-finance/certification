package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.AbstractGetUnarrangedAccountsOverdraftWarrantiesOASValidator;


public class GetUnarrangedAccountsOverdraftWarrantiesV2n2OASValidator extends AbstractGetUnarrangedAccountsOverdraftWarrantiesOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/unarrangedAccountsOverdraft/unarranged-accounts-overdraft-v2.2.0.yml";
	}
}
