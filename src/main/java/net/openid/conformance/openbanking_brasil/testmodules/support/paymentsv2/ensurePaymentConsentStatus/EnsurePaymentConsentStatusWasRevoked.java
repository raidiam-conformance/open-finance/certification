package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsStatusEnum;

public class EnsurePaymentConsentStatusWasRevoked extends AbstractEnsurePaymentConsentStatusWas {

	@Override
	protected String getExpectedStatus() {
		return RecurringPaymentsConsentsStatusEnum.REVOKED.toString();
	}
}
