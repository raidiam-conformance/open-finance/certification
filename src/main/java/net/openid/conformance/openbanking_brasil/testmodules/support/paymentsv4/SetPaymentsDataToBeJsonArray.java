package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SetPaymentsDataToBeJsonArray extends AbstractCondition {

	@Override
	@PostEnvironment(strings = "payment_is_array")
	public Environment evaluate(Environment env) {
		env.putString("payment_is_array", "ok");
		logSuccess("Payment payload will be a JsonArray");
		return env;
	}
}
