package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Random;

public abstract class AbstractValidateTransactionWithinRange extends AbstractCondition {

	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	@Override
	@PreEnvironment(strings = {"toBookingDate", "fromBookingDate"}, required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {

		String fromBookingDate = env.getString("fromBookingDate");
		String toBookingDate = env.getString("toBookingDate");

		JsonObject body;
		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		JsonArray transactions = body.getAsJsonArray("data");

		if (transactions == null || transactions.isEmpty()) {
			throw error("No transactions returned unable to validate the defined behaviour with booking date query parameters",
				args("response", env.getObject("resource_endpoint_response_full"),
					"body", body,
					"data", transactions));
		}

		int amountOfTransactions = transactions.size();
		Random random = new Random();
		JsonElement randomTransaction = transactions.get(random.nextInt(amountOfTransactions));
		JsonObject randomTransactionObject = randomTransaction.getAsJsonObject();

		processTransactionObject(randomTransactionObject, fromBookingDate, toBookingDate, env);

		return env;
	}

	protected abstract void processTransactionObject(JsonObject randomTransactionObject, String fromBookingDate, String toBookingDate, Environment env);

	protected void validateTransactionDate(String transactionDate, String fromBookingDate, String toBookingDate) {
		LocalDate fmtTransactionDate = parseStringToDate(transactionDate);
		LocalDate fmtFromBookingDate = parseStringToDate(fromBookingDate);
		LocalDate fmtToBookingDate = parseStringToDate(toBookingDate);

		if (fmtTransactionDate.isAfter(fmtToBookingDate) || fmtTransactionDate.isBefore(fmtFromBookingDate)) {
			throw error("Transaction returns is not within the range of the specified query parameters", Map.of("Date: ", transactionDate));
		}
	}

	protected void validateTransactionDateTime(String transactionDateTime, String fromBookingDate, String toBookingDate) {
		LocalDate fmtTransactionDateTime = parseStringToDateTime(transactionDateTime);
		LocalDate fmtFromBookingDate = parseStringToDate(fromBookingDate);
		LocalDate fmtToBookingDate = parseStringToDate(toBookingDate);

		if (fmtTransactionDateTime.isAfter(fmtToBookingDate) || fmtTransactionDateTime.isBefore(fmtFromBookingDate)) {
			throw error("Transaction returns is not within the range of the specified query parameters", Map.of("Date: ", transactionDateTime));
		}
	}

	protected LocalDate parseStringToDate(String dateToParse) {
		return LocalDate.parse(dateToParse, DATE_FORMATTER);
	}

	protected LocalDate parseStringToDateTime(String dateToParse) {
		return LocalDate.parse(dateToParse, DATE_TIME_FORMATTER);
	}

}
