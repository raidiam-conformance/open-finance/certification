package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

public class EnsureNumberOfTotalRecordsIsAtLeast51FromMeta extends AbstractEnsureNumberOfTotalRecordsFromMeta {
	@Override
	protected  TotalRecordsComparisonOperator getComparisonMethod(){
		return TotalRecordsComparisonOperator.AT_LEAST;
	}
	@Override
	protected int getTotalRecordsComparisonAmount(){
		return 51;
	}
}
