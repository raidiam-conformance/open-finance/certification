package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class GetEnrollmentStatus extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    public Environment evaluate(Environment env) {
        try {
            JsonObject body = OIDFJSON.toObject(
                BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                    .orElseThrow(() -> error("Could not extract body from response"))
            );

			String status = Optional.ofNullable(body.getAsJsonObject("data"))
				.map(data -> data.get("status"))
				.map(OIDFJSON::getString)
				.orElseThrow(() -> error("Body does not have data.status field", args("body", body)));
			env.putString("enrollment_status",status);
            logSuccess("Enrollment status was extracted", args("status", status));
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }
        return env;
    }

}
