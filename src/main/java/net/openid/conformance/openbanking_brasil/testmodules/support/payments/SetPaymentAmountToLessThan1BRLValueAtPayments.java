package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public class SetPaymentAmountToLessThan1BRLValueAtPayments extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonElement element = env.getElementFromObject("resource", "brazilPixPayment.data");

		if (element.isJsonArray()) {
			JsonArray paymentsArray = element.getAsJsonArray();
			for (JsonElement paymentElement : paymentsArray) {
				JsonObject paymentObj = paymentElement.getAsJsonObject();
				JsonObject payment = paymentObj.getAsJsonObject("payment");
				payment.addProperty("amount", "0.75");
			}
			logSuccess("Updated all payment amounts to 0.75 in the array of payments");
		} else if (element.isJsonObject()) {
			JsonObject paymentObj = element.getAsJsonObject();
			JsonObject payment = paymentObj.getAsJsonObject("payment");
			payment.addProperty("amount", "0.75");
			logSuccess("Updated payment amount to 0.75");
		} else {
			logFailure("Expected a JSON array or object of payments but got something else");
		}

		return env;
	}
}
