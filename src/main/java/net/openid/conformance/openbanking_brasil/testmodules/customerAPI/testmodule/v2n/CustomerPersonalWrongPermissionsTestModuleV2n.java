package net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractCustomerPersonalWrongPermissionsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "customer-personal_api_wrong-permissions_test-module_v2-2",
	displayName = "Ensures API resource cannot be called with wrong permissions",
	summary = """
		Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test
		• Creates a Consent will the customer personal permissions ( "CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ","CUSTOMERS_PERSONAL_ADITTIONALINFO_READ","RESOURCES_READ")
		• Expects a success 201 - Checks all of the fields sent on the consent API are specification compliant
		• Calls GET Personal Qualifications resources V2
		• Expects a success 200
		• Creates a Consent with all the permissions but the Customer Personal and Business ones - ("ACCOUNTS_READ", "ACCOUNTS_BALANCES_READ", "RESOURCES_READ", "ACCOUNTS_OVERDRAFT_LIMITS_READ",  "ACCOUNTS_TRANSACTIONS_READ", "CREDIT_CARDS_ACCOUNTS_READ", "CREDIT_CARDS_ACCOUNTS_TRANSACTIONS_READ", "CREDIT_CARDS_ACCOUNTS_READ", "CREDIT_CARDS_ACCOUNTS_BILLS_READ", "CREDIT_CARDS_ACCOUNTS_BILLS_TRANSACTIONS_READ", "LOANS_READ", "LOANS_WARRANTIES_READ", "LOANS_SCHEDULED_INSTALMENTS_READ", "LOANS_PAYMENTS_READ", "FINANCINGS_READ", "FINANCINGS_WARRANTIES_READ", "FINANCINGS_SCHEDULED_INSTALMENTS_READ", "FINANCINGS_PAYMENTS_READ", "UNARRANGED_ACCOUNTS_OVERDRAFT_READ", "UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ", "UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ", "UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ", "INVOICE_FINANCINGS_READ", "INVOICE_FINANCINGS_WARRANTIES_READ", "INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ", "INVOICE_FINANCINGS_PAYMENTS_READ", "CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ", "CUSTOMERS_BUSINESS_ADITTIONALINFO_READ")
		• If the Consent Creation leads to a "422 Unprocessable Entity" meaning the server only shares customer data, test will return a WARNING and the test will end
		• Calls the GET Customer Personal Qualification Resource V2
		• Expects a 403
		• Calls the GET Customer Personal Financial Relations Resource V2
		• Expects a 403
	""",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.brazilCnpj", "resource.consentUrl"
})
public class CustomerPersonalWrongPermissionsTestModuleV2n extends AbstractCustomerPersonalWrongPermissionsTestModule {

}
