package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRevocationReasonWas;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public abstract class AbstractEnsureEnrollmentRevocationReasonWas extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			JsonObject responseBody = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response")).getAsJsonObject();

			JsonObject data = Optional.ofNullable(responseBody.getAsJsonObject("data"))
				.orElseThrow(() -> error("Could not extract data from body", args("body", responseBody)));

			JsonObject cancellation = Optional.ofNullable(data.getAsJsonObject("cancellation"))
				.orElseThrow(() -> error("Could not extract cancellation from data", args("data", data)));

			JsonObject reason = Optional.ofNullable(cancellation.getAsJsonObject("reason"))
				.orElseThrow(() -> error("Could not extract reason from cancellation", args("cancellation", cancellation)));

			String revocationReason = OIDFJSON.getString(Optional.ofNullable(reason.get("revocationReason"))
				.orElseThrow(() -> error("Could not extract revocationReason from reason", args("reason", reason))));

			String expectedRevocationReason = getExpectedRevocationReason();

			if (!expectedRevocationReason.equals(revocationReason)) {
				throw error("Enrollments revocation reason is not what was expected",
					args("Expected", expectedRevocationReason, "Actual", revocationReason));
			}

			logSuccess("Received expected revocation reason",
				args("Expected", expectedRevocationReason, "Received", revocationReason));

		} catch (ClassCastException e) {
			throw error("A Json Object was expected");

		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected abstract String getExpectedRevocationReason();

}
