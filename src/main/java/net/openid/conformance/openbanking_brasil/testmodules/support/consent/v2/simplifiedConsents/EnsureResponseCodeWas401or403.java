package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpStatus;

public class EnsureResponseCodeWas401or403 extends AbstractCondition {
	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {
		int status = env.getInteger("resource_endpoint_response_full", "status");
		logSuccess("Found response code", args("status",status));
		if(status == HttpStatus.UNAUTHORIZED.value() || status == HttpStatus.FORBIDDEN.value()) {
			logSuccess("401/403 response status, as expected", args("status",status));
		} else {
			throw error("Was expecting a 401 or 403 response", args("status",status));
		}
		return env;
	}
}
