package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

public class CreateFidoAuthorizeRequestBodyWithMockedFidoAssertionToRequestEntityClaims
	extends CreateFidoAuthorizeRequestBodyToRequestEntityClaims {

	@Override
	protected JsonObject getFidoAssertionObject(Environment env) {
		return new JsonObjectBuilder()
			.addField("id", encodeData("id"))
			.addField("rawId", encodeData("id"))
			.addField("type", "public-key")
			.addField("response", getResponseObject(env, null))
			.build();
	}

	@Override
	protected JsonObject getResponseObject(Environment env, JsonObject fidoKeysJwk) {
		return new JsonObjectBuilder()
			.addField("clientDataJSON", encodeData("clientData"))
			.addField("authenticatorData", encodeData("authenticatorData"))
			.addField("signature", encodeData("signature"))
			.addField("userHandle", "userId")
			.build();
	}
}
