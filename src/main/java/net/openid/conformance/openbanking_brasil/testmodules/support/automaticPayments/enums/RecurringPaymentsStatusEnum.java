package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums;

public enum RecurringPaymentsStatusEnum {

	RCVD, CANC, ACCP, ACPD, RJCT, ACSC, PDNG, SCHD;
}
