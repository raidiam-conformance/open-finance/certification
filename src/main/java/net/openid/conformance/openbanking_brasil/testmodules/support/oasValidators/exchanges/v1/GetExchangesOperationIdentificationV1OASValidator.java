package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetExchangesOperationIdentificationV1OASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/exchanges/exchanges-v1.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/operations/{operationId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		assertData(body.getAsJsonObject("data"));
	}

	private void assertData(JsonObject data) {
		assertField1isRequiredWhenField2IsPresent(
			data,
			"intermediaryInstitutionName",
			"intermediaryInstitutionCnpjNumber"
		);
	}
}
