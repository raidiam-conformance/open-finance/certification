package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.client.ExtractRefreshTokenFromTokenResponse;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractUnhappyAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateEmptySweepingRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToRemoveStartDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetExpirationToNearFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.WaitFor120Seconds;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.EnsureErrorResponseCodeFieldWasConsentimentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.sequence.client.RefreshTokenRequestExpectingErrorSteps;
import org.springframework.http.HttpStatus;

public abstract class AbstractAutomaticPaymentsApiExpirationDateTimeTestModule extends AbstractUnhappyAutomaticPaymentsFunctionalTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateEmptySweepingRecurringConfigurationObject();
	}

	@Override
	protected HttpStatus expectedResponseCode() {
		return HttpStatus.UNAUTHORIZED;
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToSetExpirationToNearFuture.class);
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToRemoveStartDateTime.class);
	}

	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(WaitFor120Seconds.class);
		super.requestProtectedResource();
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();

		runInBlock("Refreshing Access Token expecting 400", () -> call(sequenceOf(
			condition(ExtractRefreshTokenFromTokenResponse.class),
			new RefreshTokenRequestExpectingErrorSteps(false, addTokenEndpointClientAuthentication)
		)));
	}

	@Override
	protected void checkUnauthorizedStatus() {
		fetchConsentToCheckStatus("CONSUMED", new EnsurePaymentConsentStatusWasConsumed());
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas ensureErrorResponseCodeFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasConsentimentoInvalido();
	}
}

