package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddAudAsEnrollmentsUrlToRequestObject extends AbstractCondition {

	@Override
	@PreEnvironment(strings = "enrollments_url", required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {

		JsonObject requestEntityClaims = env.getObject("resource_request_entity_claims");

		String enrollmentsEndpoint = env.getString("enrollments_url");

		requestEntityClaims.addProperty("aud", enrollmentsEndpoint);

		logSuccess("Added aud to request entity claims", args("aud", enrollmentsEndpoint));

		return env;
	}
}
