package net.openid.conformance.openbanking_brasil.testmodules.support.validateField;

public class ValidateConsentsFieldV3 extends AbstractValidateField {

	@Override
	protected int getConsentVersion() {
		return 3;
	}
}
