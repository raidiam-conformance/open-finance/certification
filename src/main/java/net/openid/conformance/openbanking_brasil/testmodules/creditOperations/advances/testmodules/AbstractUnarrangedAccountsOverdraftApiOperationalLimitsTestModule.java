package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractApiOperationalLimitsTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContractGuarantees;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContractInstallments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContractPayments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContracts;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureSpecificCreditOperationsPermissionsWereReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetUnarrangedAccountsOverdraftV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractUnarrangedAccountsOverdraftApiOperationalLimitsTestModule extends AbstractApiOperationalLimitsTestModulePhase2 {

	@Override
	protected abstract Class<? extends Condition> contractsResponseValidator();

	@Override
	protected abstract Class<? extends Condition> contractIdValidator();
	@Override
	protected abstract Class<? extends Condition> contractWarrantiesValidator();

	@Override
	protected abstract Class<? extends Condition> contractScheduledInstalmentsValidator();

	@Override
	protected abstract Class<? extends Condition> contractPaymentsValidator();

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getResourceEndpoint() {
		return GetUnarrangedAccountsOverdraftV2Endpoint.class;
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.UNARRANGED_ACCOUNTS_OVERDRAFT;
	}

	@Override
	protected EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType getPermissionType() {
		return EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType.UNARRANGED_ACCOUNTS_OVERDRAFT;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractId() {
		return PrepareUrlForFetchingCreditAdvanceContracts.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingWarranties() {
		return PrepareUrlForFetchingCreditAdvanceContractGuarantees.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingScheduledInstalments() {
		return PrepareUrlForFetchingCreditAdvanceContractInstallments.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingPayments() {
		return PrepareUrlForFetchingCreditAdvanceContractPayments.class;
	}

}
