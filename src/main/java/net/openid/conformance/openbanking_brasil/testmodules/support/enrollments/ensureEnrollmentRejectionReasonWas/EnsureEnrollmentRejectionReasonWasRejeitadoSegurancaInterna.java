package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentRejectionReasonEnum;

public class EnsureEnrollmentRejectionReasonWasRejeitadoSegurancaInterna extends AbstractEnsureEnrollmentRejectionReasonWas {

    @Override
    protected String getExpectedRejectionReason() {
        return EnrollmentRejectionReasonEnum.REJEITADO_SEGURANCA_INTERNA.toString();
    }
}
