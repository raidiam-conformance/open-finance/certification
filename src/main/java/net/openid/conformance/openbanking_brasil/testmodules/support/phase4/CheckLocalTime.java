package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.Supplier;

public class CheckLocalTime extends AbstractCondition {

	private final Supplier<ZonedDateTime> currentTimeSupplier;

	public CheckLocalTime() {
		this.currentTimeSupplier = () -> ZonedDateTime.now(ZoneId.of("America/Sao_Paulo"));
	}

	public CheckLocalTime(Supplier<ZonedDateTime> currentTimeSupplier) {
		this.currentTimeSupplier = currentTimeSupplier;
	}

	@Override
	public Environment evaluate(Environment env) {
		log("Validating localTime is between 11:30 PM and 11:59 PM in UTC-3");
		ZonedDateTime now = currentTimeSupplier.get();
		LocalTime currentTime = now.toLocalTime();
		LocalTime startTime = LocalTime.of(23, 30,0);
		LocalTime endTime = LocalTime.of(23, 59, 59);

		if (currentTime.equals(startTime) || currentTime.isAfter(startTime) && currentTime.isBefore(endTime)) {
			logSuccess("localTime is between 11:30 PM and 11:59 PM in UTC-3", args("localTime", currentTime));
		} else {
			throw error("localTime is not between 11:30 PM and 11:59 PM in UTC-3", args("localTime", currentTime));
		}
		return env;
	}
}
