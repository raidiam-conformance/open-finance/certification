package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

public class ValidateConsent422EstadoInvalido extends AbstractValidateConsent422ErrorCode {

	@Override
	protected String getErrorCode() {
		return "ESTADO_CONSENTIMENTO_INVALIDO";
	}
}
