package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.abstractModule.AbstractCreditCardApiWrongPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsBillsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsBillsTransactionsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsIdentificationOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsLimitsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsListOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsTransactionsOASValidatorV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "credit-cards_api_wrong-permissions_test-module_v2-1",
	displayName = "Ensures API resource cannot be called with wrong permissions",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Create a Consent with the complete set of the credit cards permission group ([\"CREDIT_CARDS_ACCOUNTS_READ\", \"CREDIT_CARDS_ACCOUNTS_BILLS_READ\", \"CREDIT_CARDS_ACCOUNTS_BILLS_TRANSACTIONS_READ\", \"CREDIT_CARDS_ACCOUNTS_LIMITS_READ\", \"CREDIT_CARDS_ACCOUNTS_TRANSACTIONS_READ\", \"RESOURCES_READ\"])\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET Credit Cards Accounts API V2\n" +
		"\u2022 Expects a 200 response \n" +
		"\u2022 Calls GET Credit Cards Transactions API V2 with AccountID specified\n" +
		"\u2022 Expects a 200 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Limits API V2 with AccountID specified\n" +
		"\u2022 Expects a 200 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Transactions API V2 with AccountID specified\n" +
		"\u2022 Expects a 200 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Bills API V2 with AccountID specified\n" +
		"\u2022 Expects a 200 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Bills Transactions API V2 with AccountID specified\n" +
		"\u2022 Expects a 200 response\n" +
		"\u2022 Creates a Consent with customer business and customer personal API resources (\"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\", \"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\", \"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\", \"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\", \"RESOURCES_READ\")\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET Credit Cards Accounts API \n" +
		"\u2022 Expects a 403 response \n" +
		"\u2022 Calls GET Credit Cards Accounts API with AccountID V2 specified\n" +
		"\u2022 Expects a 403 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Limits API with AccountID V2 specified\n" +
		"\u2022 Expects a 403 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Transactions API with AccountID V2 specified\n" +
		"\u2022 Expects a 403 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Bills API with AccountID V2 specified\n" +
		"\u2022 Expects a 403 response\n" +
		"\u2022 Calls GET Credit Cards Accounts Bills Transactions API with AccountID V2 specified\n" +
		"\u2022 Expects a 403 response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class CreditCardApiWrongPermissionsTestModuleV2n extends AbstractCreditCardApiWrongPermissionsTestModule {
	@Override
	protected Class<? extends Condition> getCardBillsTransactionsValidator() {
		return GetCreditCardAccountsBillsTransactionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardBillsValidator() {
		return GetCreditCardAccountsBillsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardIdentificationValidator() {
		return GetCreditCardAccountsIdentificationOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardAccountsLimitsValidator() {
		return GetCreditCardAccountsLimitsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardAccountsTransactionsValidator() {
		return GetCreditCardAccountsTransactionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardAccountsListValidator() {
		return GetCreditCardAccountsListOASValidatorV2.class;
	}
}

