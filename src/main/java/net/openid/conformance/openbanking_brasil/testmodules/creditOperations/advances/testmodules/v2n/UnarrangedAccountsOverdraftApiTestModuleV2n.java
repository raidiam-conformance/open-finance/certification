package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.AbstractUnarrangedAccountsOverdraftApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftIdentificationV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftListV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftPaymentsV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftScheduledInstalmentsV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftWarrantiesV2n2OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "unarranged-accounts-overdraft_api_core_test-module_V2-2",
	displayName = "Validate structure of all unarranged overdraft API resources V2-2",
	summary = "Validates the structure of all unarranged overdraft API resources V2-2\n" +
		"• Creates a consent with all the permissions needed to access the Credit Operations API  (\"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"• Calls GET Unarranged Overdraft Contracts API V2-2\n" +
		"• Expects 200 - Fetches one of the IDs returned\n" +
		"• Calls GET Unarranged Overdraft Contracts API with ID V2-2\n" +
		"• Expects 200\n" +
		"• Calls GET Unarranged Overdraft Warranties API V2-2\n" +
		"• Expects 200\n" +
		"• Calls GET Unarranged Overdraft Payments API V2-2\n" +
		"• Expects 200\n" +
		"• Calls GET Unarranged Overdraft Contracts Instalments API V2-2\n" +
		"• Expects 200\n" +
		"11",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class UnarrangedAccountsOverdraftApiTestModuleV2n extends AbstractUnarrangedAccountsOverdraftApiTestModule {


	@Override
	protected Class<? extends Condition> apiResourceContractListResponseValidator() {
		return GetUnarrangedAccountsOverdraftListV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractResponseValidator() {
		return GetUnarrangedAccountsOverdraftIdentificationV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractGuaranteesResponseValidator() {
		return GetUnarrangedAccountsOverdraftWarrantiesV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractPaymentsResponseValidator() {
		return GetUnarrangedAccountsOverdraftPaymentsV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractInstallmentsResponseValidator() {
		return GetUnarrangedAccountsOverdraftScheduledInstalmentsV2n2OASValidator.class;
	}

}
