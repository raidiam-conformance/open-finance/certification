package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiStartDateTimeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.addLocalInstrument.AddManuLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setSweepingStartDateTime.EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetUserDefinedCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToRemoveProxy;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_startDateTime_test-module_v2",
	displayName = "automatic-payments_api_startDateTime_test-module_v2",
	summary = "Ensure a payment cannot be executed if its sent prior to startDateTime\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, and startDateTime as D + 10\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 200 - Validate Response and ensure status is AUTHORISED\n" +
		"• Call the POST recurring-payments endpoint\n" +
		"• Expect 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO - Validate Error Message\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 200 - Validate Response and ensure status is AUTHORISED",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorAccountIspb",
		"resource.creditorAccountIssuer",
		"resource.creditorAccountNumber",
		"resource.creditorAccountAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiStartDateTimeTestModuleV2 extends AbstractAutomaticPaymentsApiStartDateTimeTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddManuLocalInstrument.class);
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetUserDefinedCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentsBodyToRemoveProxy.class);
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToTheFuture.class);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class), condition(GetAutomaticPaymentPixRecurringV2Endpoint.class));
	}
}
