package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates;

import java.time.LocalDate;

public class EnsureOnePaymentForToday extends AbstractEnsurePaymentListDates {

	@Override
	protected int amountOfPaymentsExpected() {
		return 1;
	}

	@Override
	protected boolean validateDate(LocalDate date, LocalDate today) {
		return date.equals(today);
	}
}
