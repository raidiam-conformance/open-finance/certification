package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequestWithInvalidPrivateKey;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditEnrollmentsRequestBodyToAddInvalidPermissionsAndAddAccountTypeField;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditEnrollmentsRequestBodyToRemoveDebtorAccountAccountTypeField;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACC;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasContaInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasPermissoesInvalidas;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsSteps;

public abstract class AbstractEnrollmentsApiInvalidParametersTestModule extends AbstractEnrollmentsApiTestModule {

	@Override
	protected void performAuthorizationFlow(){
		performPreAuthorizationSteps();
		executeTestSteps();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected void performPreAuthorizationSteps() {}

	@Override
	protected void executeTestSteps() {
		runInBlock("Call POST enrollments - Don't send an x-fapi-interaction-id - Expects 400",
			this::postAndValidateEnrollmentsWithoutInteractionId);

		runInBlock("Call POST enrollments - Sign the message with an incorrect private key - Expects 400",
			this::postAndValidateEnrollmentsSignedWithInvalidKey);

		runInBlock("Call POST enrollments - Send payload without the issuer field and accountType as CACC - Expects 422",
			this::postAndValidateEnrollmentsWithoutIssuerAndAccountTypeAsCacc);

		runInBlock("Call POST enrollments - Send payload without the debtorAccount.accountType field - Expects 422",
			this::postAndValidateEnrollmentsWithoutAccountType);

		runInBlock("Call POST enrollments - Send payload with an invalid permission value - Expects 422",
			this::postAndValidateEnrollmentsWithInvalidPermission);
	}

	protected void postAndValidateEnrollmentsWithoutInteractionId() {
		call(createPostEnrollmentsSteps()
			.skip(CreateRandomFAPIInteractionId.class, "FAPI interaction ID will not be created")
			.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI interaction ID will not be created")
		);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas400.class, Condition.ConditionResult.FAILURE);
	}

	protected void postAndValidateEnrollmentsSignedWithInvalidKey() {
		call(createPostEnrollmentsSteps()
			.replace(SignEnrollmentsRequest.class, condition(SignEnrollmentsRequestWithInvalidPrivateKey.class))
		);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas400.class, Condition.ConditionResult.FAILURE);
	}

	protected void postAndValidateEnrollmentsWithoutIssuerAndAccountTypeAsCacc() {
		callAndStopOnFailure(CreateEnrollmentsRequestBodyToRequestEntityClaims.class);
		call(createPostEnrollmentsSteps()
			.insertBefore(SignEnrollmentsRequest.class, condition(EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACC.class))
		);
		validate422Response(new EnsureErrorResponseCodeFieldWasContaInvalida());
	}

	protected void postAndValidateEnrollmentsWithoutAccountType() {
		callAndStopOnFailure(CreateEnrollmentsRequestBodyToRequestEntityClaims.class);
		call(createPostEnrollmentsSteps()
			.insertBefore(SignEnrollmentsRequest.class, condition(EditEnrollmentsRequestBodyToRemoveDebtorAccountAccountTypeField.class))
		);
		validate422Response(new EnsureErrorResponseCodeFieldWasContaInvalida());
	}

	protected void postAndValidateEnrollmentsWithInvalidPermission() {
		callAndStopOnFailure(CreateEnrollmentsRequestBodyToRequestEntityClaims.class);
		call(createPostEnrollmentsSteps()
			.insertBefore(SignEnrollmentsRequest.class, condition(EditEnrollmentsRequestBodyToAddInvalidPermissionsAndAddAccountTypeField.class))
		);
		validate422Response(new EnsureErrorResponseCodeFieldWasPermissoesInvalidas());
	}

	@Override
	protected PostEnrollmentsSteps createPostEnrollmentsSteps() {
		return new PostEnrollmentsSteps(addTokenEndpointClientAuthentication, true);
	}

	protected void validate422Response(AbstractEnsureErrorResponseCodeFieldWas errorResponseCodeFieldWas) {
		callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(postEnrollmentsValidator(), Condition.ConditionResult.FAILURE);
		env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		callAndContinueOnFailure(errorResponseCodeFieldWas.getClass(), Condition.ConditionResult.FAILURE);
		env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
	}

}
