package net.openid.conformance.openbanking_brasil.testmodules.support;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class EnsureExpirationWasNotReturned extends AbstractCondition {

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		JsonObject body;
		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "consent_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		JsonObject data = body.getAsJsonObject("data");
		if (data == null) {
			throw error("Data element is missing in the body", args("body", body));
		}

		if (data.has("expirationDateTime")) {
			throw error("expirationDateTime field should not be present in the response",
				args("data", data));
		}
		logSuccess("expirationDateTime is not in the response", args("data", data));
		return env;
	}
}
