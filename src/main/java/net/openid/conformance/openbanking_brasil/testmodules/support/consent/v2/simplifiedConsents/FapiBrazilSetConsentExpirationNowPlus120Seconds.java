package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

public class FapiBrazilSetConsentExpirationNowPlus120Seconds extends AbstractFapiBrazilAddExpirationTimeToConsentRequestInSeconds{
	@Override
	protected int getExpirationTimeInSeconds() {
		return 120;
	}
}
