package net.openid.conformance.openbanking_brasil.testmodules.phase2;

import com.google.gson.JsonObject;
import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.CompareResourceIdWithAPIResourceId;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractAllSpecifiedApiIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlAsNextLinkOrStopPolling;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize1000;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ResourceApiV2PollingSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public abstract class AbstractPhase2ApiResourceTestModule extends AbstractPhase2TestModule {

	private static final String RESOURCE_STATUS = EnumResourcesStatus.AVAILABLE.name();
	protected abstract OPFScopesEnum getScope();
	protected abstract OPFCategoryEnum getProductCategory();
	protected abstract Class<? extends Condition> apiValidator();
	protected abstract String apiName();
	protected abstract String apiResourceId();
	protected abstract String resourceType();

	@Override
	protected void configureClient() {
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(getProductCategory()).addScopes(getScope(), OPFScopesEnum.RESOURCES, OPFScopesEnum.OPEN_ID).build();
	}

	@Override
	protected void validateResponse() {
		runInBlock("Validate " + apiName() + " root response", () -> callAndStopOnFailure(apiValidator()));

		call(exec().startBlock("Setting page-size=1000 and calling the " + apiName() + " endpoint again"));
		callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);
		preCallProtectedResource();

		runInBlock("Validate " + apiName() + " root response", () -> callAndStopOnFailure(apiValidator()));

		env.putString("apiIdName", apiResourceId());
		callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);
		callAndStopOnFailure(GetResourcesV3Endpoint.class);
		callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);

		ResourceApiV2PollingSteps pollingSteps = new ResourceApiV2PollingSteps(env, getId(),
			eventLog,testInfo, getTestExecutionManager());
		runInBlock("Polling Resources API", () -> call(pollingSteps));

		call(exec().startBlock("Poll has ended, setting page-size=1000 and calling the resource again"));
		callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);

		env.putString("resource_type", resourceType());
		env.putString("resource_status", RESOURCE_STATUS);
		new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, getTestExecutionManager(),
			() -> getPreCallProtectedResourceSequence()
				.then(sequenceOf(
					condition(SetProtectedResourceUrlAsNextLinkOrStopPolling.class),
					condition(ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus.class)
				))
		)
			.untilTrue("no_next")
			.times(10)
			.trailingPause(10)
			.onTimeout(sequenceOf(condition(TestTimedOut.class), condition(ChuckWarning.class)))
			.run();

		eventLog.startBlock("Compare active resourceId's with API resources");
		callAndStopOnFailure(CompareResourceIdWithAPIResourceId.class);
		eventLog.endBlock();
	}

	protected ConditionSequence getPreCallProtectedResourceSequence() {
		return sequenceOf(
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(CallProtectedResource.class),
			condition(EnsureResourceResponseCodeWas200.class),
			condition(GetResourcesOASValidatorV3.class)
		);
	}
}
