package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareRejectionOrRevocationReasonForPatchRequest;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentRevocationReasonEnum;
import net.openid.conformance.testmodule.Environment;

public class PrepareRevocationReasonRevogadoManualmenteForPatchRequest extends AbstractCondition {

	@Override
	@PostEnvironment(strings = "revocation_reason")
	public Environment evaluate(Environment env) {
		env.putString("revocation_reason", EnrollmentRevocationReasonEnum.REVOGADO_MANUALMENTE.toString());
		env.removeNativeValue("rejection_reason");
		logSuccess("revocationReason is going to be set as REVOGADO_MANUALMENTE in the PATCH request body");
		return env;
	}
}
