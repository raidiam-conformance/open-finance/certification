package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedFrom;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedFromEnum;

public class EnsureRecurringPaymentsConsentsRevokedFromDetentora extends AbstractEnsureRecurringPaymentsConsentsRevokedFrom {

	@Override
	protected RecurringPaymentsConsentsRevokedFromEnum expectedRevokedFrom() {
		return RecurringPaymentsConsentsRevokedFromEnum.DETENTORA;
	}
}
