package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.editRequestBodyToAddClientExtensionResults;

public class EditFidoRegistrationRequestBodyToAddClientExtensionResults extends AbstractEditRequestBodyToAddClientExtensionResults {

	@Override
	protected String getPathToClientExtensionResults() {
		return "data";
	}
}
