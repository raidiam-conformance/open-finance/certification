package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.IsoFields;
import java.time.temporal.WeekFields;

public class SetPaymentReferenceAccordingToUserDefinedInterval extends AbstractSetPaymentReference {

	private String interval;

	@Override
	public Environment evaluate(Environment env) {
		interval = env.getString("resource", "interval");
		return super.evaluate(env);
	}

	@Override
	protected String getPaymentReference(String dateStr) {
		LocalDate date = getPaymentDate(dateStr);
		int weekNumber = date.get(WeekFields.ISO.weekOfWeekBasedYear());
		int month = date.getMonthValue();
		int quarter = date.get(IsoFields.QUARTER_OF_YEAR);
		int semester = (month - 1) / 6 + 1;
		int year = date.get(ChronoField.YEAR);
		int weekBasedYear = date.get(WeekFields.ISO.weekBasedYear());


		try {
			RecurringPaymentsConsentsAutomaticPixIntervalEnum intervalEnum = RecurringPaymentsConsentsAutomaticPixIntervalEnum.valueOf(interval);
			return switch (intervalEnum) {
				case SEMANAL -> String.format("W%d-%d", weekNumber, weekBasedYear);
				case MENSAL -> String.format("M%02d-%d", month, year);
				case TRIMESTRAL -> String.format("Q%d-%d", quarter, year);
				case SEMESTRAL -> String.format("S%d-%d", semester, year);
				case ANUAL -> String.format("Y%d", year);
			};
		} catch (IllegalArgumentException e) {
			throw error("The value defined for resource.interval is not valid", args("interval", interval));
		}
	}
}
