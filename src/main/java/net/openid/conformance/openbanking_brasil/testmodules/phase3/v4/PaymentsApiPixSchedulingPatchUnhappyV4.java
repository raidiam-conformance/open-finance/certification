package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiPixSchedulingPatchUnhappyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PatchPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_pixscheduling-patch-unhappy_test-module_v4",
	displayName = "Payments API test for different scenarios where a PATCH is called for a scheduled pix that result in failure",
	summary = "1. PATCH Payments unhappy path test that will revoke a scheduled payment with invalid request_body\n" +
		"\u2022 Create consent with request payload with the schedule.single.date field set as D+1\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"\u2022 Call the POST Payments Endpoint\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD or ACCP\n" +
		"\u2022 Expect Payment Scheduled to be reached (SCHD) - Validate Response\n" +
		"\u2022 Call the PATCH Payments Endpoint - cancelledBy must have the same document as the loggedUser however the field rel will be set to \"XXX\"\n" +
		"\u2022 Expect 422 with error set as PAGAMENTO_NAO_PERMITE_CANCELAMENTO\n" +
		"\n" +
		"2. Ensure error when PATCH payments endpoint is called in a state different from SCHD/PDNG/PATC\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"\u2022 Expects 201\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint \n" +
		"\u2022 Expects 201\n" +
		"\u2022 Calls the PATCH Payments Endpoint\n" +
		"\u2022 Expects 422 - PAGAMENTO_NAO_PERMITE_CANCELAMENTO\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expects Definitive state (ACSC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)

public class PaymentsApiPixSchedulingPatchUnhappyV4 extends AbstractPaymentsApiPixSchedulingPatchUnhappyTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() { return GetPaymentsPixOASValidatorV4.class; }

	@Override
	protected Class<? extends Condition> patchPaymentErrorValidator() {
		return PatchPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> patchPaymentConsentValidator() { return PatchPaymentsPixOASValidatorV4.class; }
}
