package net.openid.conformance.openbanking_brasil.testmodules.v3n.consents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractConsentApiBadConsentsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_bad-consents_test-module_v3-2",
	displayName = "Validate that requests with incompatible consents return HTTP 400",
	summary = "Validate that requests with incompatible consents return HTTP 400\n" +
		"• Calls the Token Endpoint using the consents scope v3\n" +
		"• Creates a Consent sending personal and business permissions together\n" +
		"• Expect 422 PERMISSAO_PF_PJ_EM_CONJUNTO - Validate error message\n" +
		"• Creates a Consent without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message and verify that an x-fapi was sent back\n" +
		"• Creates a Consent with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message and verify that a valid x-fapi was sent back",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ConsentApiBadConsentsTestModuleV3n extends AbstractConsentApiBadConsentsTestModule {

	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

}
