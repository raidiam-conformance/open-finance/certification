package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.checkCurrentTime;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public abstract class AbstractCheckCurrentTime extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		ZoneId timeZone = ZoneId.of("America/Sao_Paulo");
		ZonedDateTime now = ZonedDateTime.now(timeZone);
		LocalTime currentTime = now.toLocalTime();

		if (currentTime.isAfter(getStartTime()) && currentTime.isBefore(getEndTime())) {
			logSuccess("Current time is inside the accepted interval", args("current time", currentTime));
		} else {
			throw error("Current time is not inside the accepted interval",
				args("current time", currentTime,
					"start time", getStartTime(), "end time", getEndTime()));
		}
		return env;
	}

	protected abstract LocalTime getStartTime();
	protected abstract LocalTime getEndTime();

}
