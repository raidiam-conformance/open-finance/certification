package net.openid.conformance.openbanking_brasil.testmodules.abstractModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureSpecificCreditOperationsPermissionsWereReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractAllSpecifiedApiIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.FetchSpecifiedNumberOfExtractedApiIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.OperationalLimitsToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.AbstractOperationalLimitsTestModule;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.variant.ClientAuthType;

public abstract class AbstractApiOperationalLimitsTestModulePhase2 extends AbstractOperationalLimitsTestModule {

	private static final String API_RESOURCE_ID = "contractId";
	private static final int NUMBER_OF_IDS_TO_FETCH = 2;


	protected abstract Class<? extends AbstractGetXFromAuthServer> getResourceEndpoint();
	protected abstract OPFScopesEnum getScope();
	protected abstract EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType getPermissionType();
	protected abstract Class<? extends Condition> contractsResponseValidator();
	protected abstract Class<? extends Condition> prepareUrlForFetchingContractId();
	protected abstract Class<? extends Condition> prepareUrlForFetchingWarranties();
	protected abstract Class<? extends Condition> prepareUrlForFetchingScheduledInstalments();
	protected abstract Class<? extends Condition> prepareUrlForFetchingPayments();
	protected abstract Class<? extends Condition> contractIdValidator();
	protected abstract Class<? extends Condition> contractWarrantiesValidator();
	protected abstract Class<? extends Condition> contractScheduledInstalmentsValidator();
	protected abstract Class<? extends Condition> contractPaymentsValidator();




	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(getResourceEndpoint()),
			condition(GetConsentV3Endpoint.class)
		)));
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_OPERATIONS).addScopes(getScope(), OPFScopesEnum.OPEN_ID).build();
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class, Condition.ConditionResult.FAILURE);
		clientAuthType = getVariant(ClientAuthType.class);
	}

	@Override
	protected void validatePermissions() {
		env.putString("permission_type", getPermissionType().name());
		callAndContinueOnFailure(EnsureSpecificCreditOperationsPermissionsWereReturned.class, Condition.ConditionResult.WARNING);
	}

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(contractsResponseValidator(), Condition.ConditionResult.FAILURE);
		eventLog.endBlock();

		runInBlock("Preparing Contracts", () -> {
			env.putString("apiIdName", API_RESOURCE_ID);
			callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);

			env.putInteger("number_of_ids_to_fetch", NUMBER_OF_IDS_TO_FETCH);
			callAndStopOnFailure(FetchSpecifiedNumberOfExtractedApiIds.class);

		});

		for (int i = 0; i < NUMBER_OF_IDS_TO_FETCH; i++) {
			int currentResourceId = i + 1;

			// Call specific contract once with validation

			String contractId = OIDFJSON.getString(env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds").get(i));
			env.putString(API_RESOURCE_ID, contractId);
			callAndStopOnFailure(prepareUrlForFetchingContractId());

			preCallProtectedResource(String.format("Fetching Contract using resource_id_%d", currentResourceId));
			validateResponse("Validate Contract response", contractIdValidator());
			disableLogging();
			// Call specific contract 3 times
			for (int j = 1; j < 4; j++) {
				preCallProtectedResource(String.format("[%d] Fetching Contract using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Contract [%d]", currentResourceId));


			// Call warranties once with validation
			runInLoggingBlock(() -> {
				callAndStopOnFailure(prepareUrlForFetchingWarranties());

				preCallProtectedResource(String.format("Fetch Warranties using resource_id_%d", currentResourceId));
				validateResponse("Validate Warranties", contractWarrantiesValidator());

			});

			// Call warranties 3 times
			for (int j = 1; j < 4; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Warranties using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Warranties[%d]", currentResourceId));


			// Call Scheduled Instalments once with validation

			runInLoggingBlock(() -> {
				callAndStopOnFailure(prepareUrlForFetchingScheduledInstalments());

				preCallProtectedResource(String.format("Fetch Scheduled Instalments using resource_id_%d", currentResourceId));
				validateResponse("Validate Scheduled Instalments Response", contractScheduledInstalmentsValidator());

			});

			// Call Scheduled Instalments 29 times
			for (int j = 1; j < 30; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Scheduled Instalments using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Sc.Inst. [%d]", currentResourceId));

			// Call Payments GET once with validation

			runInLoggingBlock(() -> {
				callAndStopOnFailure(prepareUrlForFetchingPayments());

				preCallProtectedResource(String.format("Fetch Payments using resource_id_%d", currentResourceId));
				validateResponse("Validate Payments Response", contractPaymentsValidator());

			});

			// Call Payments GET 29 times
			for (int j = 1; j < 30; j++) {
				preCallProtectedResource(String.format("[%d] Fetch Payments using resource_id_%d", j + 1, currentResourceId));
			}

			makeOverOlCall(String.format("Payments [%d]", currentResourceId));

			enableLogging();
		}

	}

	protected void validateResponse(String message, Class<? extends Condition> validationClass) {
		runInBlock(message, () -> callAndContinueOnFailure(validationClass, Condition.ConditionResult.FAILURE));
	}


}
