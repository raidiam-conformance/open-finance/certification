package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

public class ExtractTwoLastPaymentIds extends AbstractExtractLastPaymentIds {

	@Override
	protected int amountOfPayments() {
		return 2;
	}
}
