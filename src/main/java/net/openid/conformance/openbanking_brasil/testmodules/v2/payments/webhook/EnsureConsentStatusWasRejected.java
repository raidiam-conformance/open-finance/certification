package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentConsentStatusEnumV2;

public class EnsureConsentStatusWasRejected extends AbstractEnsurePaymentConsentStatusWas {
	@Override
	protected String getExpectedStatus() {
		return PaymentConsentStatusEnumV2.REJECTED.toString();
	}
}
