package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareToFetchPayment extends ResourceBuilder {

	@Override
	@PreEnvironment(strings = "payment_id")
	public Environment evaluate(Environment env) {
		String paymentId = env.getString("payment_id");
		setApi("payments");
		setEndpoint("/pix/payments/" + paymentId);

		return super.evaluate(env);
	}
}
