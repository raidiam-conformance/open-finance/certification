package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForSemanalPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToFourDaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo50Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpV2;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractAutomaticPaymentsAutomaticPixUnhappyCorePathTestModule extends AbstractAutomaticPaymentsAutomaticPixTestModule {
	/**
	 * This class should be extended by automatic pix tests that follow the steps bellow:
	 *    - Call the POST recurring-consents endpoint with automatic pix fields, with referenceStartDate as {value}, interval as SEMANAL, a fixed Amount of {value}, minimumVariableAmount of {value}, maximumVariableAmount as {value}, and sending firstPayment information with amount as 1 BRL, date as D+0
	 *    - Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION
	 *    - Redirect the user to authorize consent
	 *    - Call the GET recurring-consents endpoint
	 *    - Expect 201 - Validate Response and ensure status is AUTHORISED
	 *    - Call the POST recurring-payments endpoint with the information defined firstPayment payload
	 *    - Expect 201 - Validate Response
	 *    - Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD
	 *    - Call the GET recurring-payments {recurringPaymentId}
	 *    - Expect 200 - Validate Response and ensure status is ACSC
	 *    - Call the POST recurring-payments endpoint with {value}, date as {value}, proxy as {value}, and creditorAccount number as {value}
	 *    - Expect 422 {value} or 201 - Validate Response
	 *     If a 201 is returned:
	 *    - Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP
	 *    - Call the GET recurring-payments {recurringPaymentId}
	 *    - Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is {value}
	 * In order to properly create the recurring-consents and recurring-payments request bodies, the user will need to provide the following fields:
	 *     - resource.debtorAccountIspb
	 *     - resource.debtorAccountIssuer
	 *     - resource.debtorAccountNumber
	 *     - resource.debtorAccountType
	 *     - resource.contractDebtorName
	 *     - resource.contractDebtorIdentification
	 *     - resource.creditorAccountIspb
	 *     - resource.creditorAccountIssuer
	 *     - resource.creditorAccountNumber
	 *     - resource.creditorAccountAccountType
	 *     - resource.creditorName
	 *     - resource.creditorCpfCnp
	 */

	protected boolean isFirstPayment = true;

	protected abstract AbstractEnsureRecurringPaymentsRejectionReason rejectionReasonCondition();
	protected abstract AbstractEnsureErrorResponseCodeFieldWas errorCodeCondition();

	@Override
	protected void validateResponse() {
		super.validateResponse();
		stepsAfterFirstPayment();
	}

	protected void stepsAfterFirstPayment() {
		configureSecondPayment();
		postAndValidateSecondPayment();
	}

	protected void configureSecondPayment() {
		isFirstPayment = false;
		configureSecondPaymentAmount();
		configureSecondPaymentDate();
	}

	protected void configureSecondPaymentAmount() {
		callAndStopOnFailure(SetAutomaticPaymentAmountTo50Cents.class);
	}

	protected void configureSecondPaymentDate() {
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToFourDaysInTheFuture.class);
		callAndStopOnFailure(SetPaymentReferenceForSemanalPayment.class);
	}

	protected void postAndValidateSecondPayment() {
		userAuthorisationCodeAccessToken();
		runInBlock("Call pix/payments endpoint", () -> {
			callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
			call(getPixPaymentSequence().replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class)));
		});
		runInBlock("Validate response", () -> {
			int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
			if (status == HttpStatus.CREATED.value()) {
				call(postPaymentValidationSequence());
				runRepeatSequence();
				validateFinalState();
			} else if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				call(sequenceOf(
					condition(postPaymentValidator())
						.dontStopOnFailure()
						.onFail(Condition.ConditionResult.FAILURE)
				));
				env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				callAndStopOnFailure(errorCodeCondition().getClass());
				env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
			}
		});
	}

	@Override
	protected void validateFinalState() {
		if (isFirstPayment) {
			super.validateFinalState();
		} else {
			callAndStopOnFailure(EnsurePaymentStatusWasRjct.class);
			callAndStopOnFailure(rejectionReasonCondition().getClass());
		}
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		if (isFirstPayment) {
			return super.getPaymentPollStatusCondition();
		}
		return CheckPaymentPollStatusRcvdOrAccpV2.class;
	}
}
