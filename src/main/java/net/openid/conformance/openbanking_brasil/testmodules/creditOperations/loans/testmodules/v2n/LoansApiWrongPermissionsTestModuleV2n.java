package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.AbstractLoansApiWrongPermissionsTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3.GetLoansIdentificationV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3.GetLoansListV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3.GetLoansPaymentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3.GetLoansScheduledInstalmentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3.GetLoansWarrantiesV2n3OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "loans_api_wrong-permissions_test-module_v2-3",
	displayName = "Ensures API resource cannot be called with wrong permissions",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Credit Operations API  (\"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Loans Contracts API - V2\n" +
		"\u2022 Expects 201 - Fetches one of the IDs returned\n" +
		"\u2022 Calls GET Loans Contracts API - V2 \n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Loans Payments API - V2 \n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Loans Warranties API - V2 \n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Loans Contracts Instalments API - V2 \n" +
		"\u2022 Expects 200\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Customer Personal or the Customer Business API (\"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\", \"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\",  \"RESOURCES_READ\")or (“CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ”,” \"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ”, \"RESOURCES_READ\")\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Loans Contracts API - V2\n" +
		"\u2022 Expects 403 \n" +
		"\u2022 Calls GET Loans Contracts API - V2 \n" +
		"\u2022 Expects 403 \n" +
		"\u2022 Calls GET Loans Payments API - V2 \n" +
		"\u2022 Expects 403 \n" +
		"\u2022 Calls GET Loans Warranties API - V2 \n" +
		"\u2022 Expects 403 \n" +
		"\u2022 Calls GET Loans Contracts Instalments API - V2 \n" +
		"\u2022 Expects 403 ",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class LoansApiWrongPermissionsTestModuleV2n extends AbstractLoansApiWrongPermissionsTest {

	@Override
	protected Class<? extends Condition> apiResourceContractListResponseValidator() {
		return GetLoansListV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractResponseValidator() {
		return GetLoansIdentificationV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractGuaranteesResponseValidator() {
		return GetLoansWarrantiesV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractPaymentsResponseValidator() {
		return GetLoansPaymentsV2n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractInstallmentsResponseValidator() {
		return GetLoansScheduledInstalmentsV2n3OASValidator.class;
	}
}
