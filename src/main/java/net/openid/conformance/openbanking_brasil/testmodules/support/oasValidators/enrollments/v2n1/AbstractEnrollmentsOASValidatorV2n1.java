package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

public abstract class AbstractEnrollmentsOASValidatorV2n1 extends OpenAPIJsonSchemaValidator {

	protected abstract String getEndpointPathSuffix();

	protected String getEndpointPathApi() {
		return "/enrollments";
	}

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/enrollments/swagger-enrollments-2.1.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return getEndpointPathApi() + getEndpointPathSuffix();
	}

	protected void assertDebtorAccountIssuerConstraint(JsonObject data) {
		JsonObject debtorAccount = data.getAsJsonObject( "debtorAccount");
		if (debtorAccount != null) {
			assertField1IsRequiredWhenField2HasValue2(
				debtorAccount,
				"issuer",
				"accountType",
				List.of("CACC","SVGS")
			);
		}
	}

	protected void assertCreationAndStatusUpdateDateTime(JsonObject data) {
		String creationDateTimeStr = OIDFJSON.getString(data.get("creationDateTime"));
		String statusUpdateDateTimeStr = OIDFJSON.getString(data.get("statusUpdateDateTime"));
		assertGenericDateTime(creationDateTimeStr, "creationDateTime");
		assertGenericDateTime(statusUpdateDateTimeStr, "statusUpdateDateTime");
	}

	private void assertGenericDateTime(String dateTimeStr, String dateTimeName) {
		Instant dateTime = Instant.parse(dateTimeStr);
		Instant currentInstant = Instant.now();
		Duration duration = Duration.between(dateTime, currentInstant);
		long differenceInMinutes = Math.abs(duration.toMinutes());

		if (differenceInMinutes >= 59) {
			throw error(String.format("The %s field value is too far from the current instant in UTC time", dateTimeName),
				args("now", currentInstant, dateTimeName, dateTimeStr));
		}
	}
}
