package net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums;

import java.util.Set;


public enum OPFGroupingEnum implements GroupingEnum {

	PERSONAL_REGISTRATION_DATA(
		Set.of(
			OPFPermissionsEnum.CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.CUSTOMERS,
			OPFScopesEnum.RESOURCES
		)
	),
	PERSONAL_ADDITIONAL_INFO(
		Set.of(
			OPFPermissionsEnum.CUSTOMERS_PERSONAL_ADITTIONALINFO_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.CUSTOMERS,
			OPFScopesEnum.RESOURCES
		)
	),
	BUSINESS_REGISTRATION_DATA(
		Set.of(
			OPFPermissionsEnum.CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.CUSTOMERS,
			OPFScopesEnum.RESOURCES
		)
	),
	BUSINESS_ADDITIONAL_INFO(
		Set.of(
			OPFPermissionsEnum.CUSTOMERS_BUSINESS_ADITTIONALINFO_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.CUSTOMERS,
			OPFScopesEnum.RESOURCES
		)
	),
	ACCOUNTS_BALANCES(
		Set.of(
			OPFPermissionsEnum.ACCOUNTS_READ,
			OPFPermissionsEnum.ACCOUNTS_BALANCES_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.ACCOUNTS,
			OPFScopesEnum.RESOURCES
		)
	),
	ACCOUNTS_LIMITS(
		Set.of(
			OPFPermissionsEnum.ACCOUNTS_READ,
			OPFPermissionsEnum.ACCOUNTS_OVERDRAFT_LIMITS_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.ACCOUNTS,
			OPFScopesEnum.RESOURCES
		)
	),
	ACCOUNTS_TRANSACTIONS(
		Set.of(
			OPFPermissionsEnum.ACCOUNTS_READ,
			OPFPermissionsEnum.ACCOUNTS_TRANSACTIONS_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.ACCOUNTS,
			OPFScopesEnum.RESOURCES
		)
	),
	CREDIT_CARDS_LIMITS(
		Set.of(
			OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_READ,
			OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_LIMITS_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.CREDIT_CARDS_ACCOUNTS,
			OPFScopesEnum.RESOURCES
		)
	),
	CREDIT_CARDS_TRANSACTIONS(
		Set.of(
			OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_READ,
			OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_TRANSACTIONS_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.CREDIT_CARDS_ACCOUNTS,
			OPFScopesEnum.RESOURCES
		)
	),
	CREDIT_CARDS_BILLS(
		Set.of(
			OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_READ,
			OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_BILLS_READ,
			OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_BILLS_TRANSACTIONS_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.CREDIT_CARDS_ACCOUNTS,
			OPFScopesEnum.RESOURCES
		)
	),
	CREDIT_OPERATIONS(
		Set.of(
			OPFPermissionsEnum.LOANS_READ,
			OPFPermissionsEnum.LOANS_WARRANTIES_READ,
			OPFPermissionsEnum.LOANS_SCHEDULED_INSTALMENTS_READ,
			OPFPermissionsEnum.LOANS_PAYMENTS_READ,
			OPFPermissionsEnum.FINANCINGS_READ,
			OPFPermissionsEnum.FINANCINGS_WARRANTIES_READ,
			OPFPermissionsEnum.FINANCINGS_SCHEDULED_INSTALMENTS_READ,
			OPFPermissionsEnum.FINANCINGS_PAYMENTS_READ,
			OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_READ,
			OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ,
			OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ,
			OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ,
			OPFPermissionsEnum.INVOICE_FINANCINGS_READ,
			OPFPermissionsEnum.INVOICE_FINANCINGS_WARRANTIES_READ,
			OPFPermissionsEnum.INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ,
			OPFPermissionsEnum.INVOICE_FINANCINGS_PAYMENTS_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.LOANS,
			OPFScopesEnum.FINANCINGS,
			OPFScopesEnum.UNARRANGED_ACCOUNTS_OVERDRAFT,
			OPFScopesEnum.INVOICE_FINANCINGS,
			OPFScopesEnum.RESOURCES
		)
	),
	INVESTMENTS(
		Set.of(
			OPFPermissionsEnum.BANK_FIXED_INCOMES_READ,
			OPFPermissionsEnum.CREDIT_FIXED_INCOMES_READ,
			OPFPermissionsEnum.FUNDS_READ,
			OPFPermissionsEnum.VARIABLE_INCOMES_READ,
			OPFPermissionsEnum.TREASURE_TITLES_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.BANK_FIXED_INCOMES,
			OPFScopesEnum.CREDIT_FIXED_INCOMES,
			OPFScopesEnum.VARIABLE_INCOMES,
			OPFScopesEnum.TREASURE_TITLES,
			OPFScopesEnum.FUNDS,
			OPFScopesEnum.RESOURCES
		)
	),
	EXCHANGES(
		Set.of(
			OPFPermissionsEnum.EXCHANGES_READ,
			OPFPermissionsEnum.RESOURCES_READ
		),
		Set.of(
			OPFScopesEnum.EXCHANGES
		)
	);

	private final Set<PermissionsEnum> permissions;
	private final Set<ScopesEnum> scopes;


	OPFGroupingEnum(Set<PermissionsEnum> permissions, Set<ScopesEnum> scopes) {
		this.permissions = permissions;
		this.scopes = scopes;
	}

	@Override
	public Set<PermissionsEnum> getPermissions() {
		return permissions;
	}

	@Override
	public Set<ScopesEnum> getScopes() {
		return scopes;
	}

}
