package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedBy;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedByEnum;

public class EnsureRecurringPaymentsConsentsRevokedByUsuario extends AbstractEnsureRecurringPaymentsConsentsRevokedBy {

	@Override
	protected RecurringPaymentsConsentsRevokedByEnum expectedRevokedBy() {
		return RecurringPaymentsConsentsRevokedByEnum.USUARIO;
	}
}
