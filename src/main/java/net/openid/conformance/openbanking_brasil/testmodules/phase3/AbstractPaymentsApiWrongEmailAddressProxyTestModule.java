package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProxyToFakeEmailAddressOnPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProxyToFakeEmailAddressOnPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDPIorPRD;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasDPIorPRD;

public abstract class AbstractPaymentsApiWrongEmailAddressProxyTestModule extends AbstractPaymentUnhappyPathTestModule {

	@Override
	protected void configureDictInfo() {
		callAndContinueOnFailure(SelectDICTCodeLocalInstrument.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(SelectDICTCodePixLocalInstrument.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(SetProxyToFakeEmailAddressOnPaymentConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(SetProxyToFakeEmailAddressOnPayment.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasDPIorPRD.class);
	}

	@Override
	protected void validate422ErrorResponseCode() {
		env.mapKey("endpoint_response","resource_endpoint_response_full");
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasDPIorPRD.class);
		env.unmapKey("endpoint_response");
	}

	@Override
	protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		return EnsureResourceResponseCodeWas201Or422.class;
	}
}
