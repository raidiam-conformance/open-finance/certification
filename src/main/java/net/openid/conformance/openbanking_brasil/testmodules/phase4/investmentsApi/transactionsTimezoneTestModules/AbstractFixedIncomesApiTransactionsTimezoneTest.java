package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionsTimezoneTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckAllTransactionsDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckTransactionDateRange;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo365DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo370DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateToToday;

public abstract class AbstractFixedIncomesApiTransactionsTimezoneTest extends AbstractInvestmentsApiTransactionsTimezoneTestModule {

    @Override
    protected Class<? extends Condition> setFromTransactionTo365DaysAgo() {
        return SetFromTransactionDateTo365DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> setFromTransactionTo370DaysAgo() {
        return SetFromTransactionDateTo370DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> setToTransactionToToday() {
        return SetToTransactionDateToToday.class;
    }

    @Override
    protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
        return AddToAndFromTransactionDateParametersToProtectedResourceUrl.class;
    }

    @Override
    protected Class<? extends Condition> checkTransactionRange() {
        return CheckTransactionDateRange.class;
    }

    @Override
    protected Class<? extends Condition> checkAllDates() {
        return CheckAllTransactionsDates.class;
    }

    @Override
    protected String transactionParameterName() {
        return "TransactionDate";
    }

}
