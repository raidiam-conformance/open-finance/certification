package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentCancelledFromWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentCanceledFromEnum;

public class EnsureEnrollmentCancelledFromWasIniciadora extends AbstractEnsureEnrollmentCancelledFromWas {

    @Override
    protected String getExpectedCancelledFromType() {
        return EnrollmentCanceledFromEnum.INICIADORA.toString();
    }
}
