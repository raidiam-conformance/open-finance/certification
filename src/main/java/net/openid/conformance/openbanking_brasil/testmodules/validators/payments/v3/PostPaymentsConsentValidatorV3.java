package net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v3;

import net.openid.conformance.logging.ApiName;

/**
 * Doc https://raw.githubusercontent.com/OpenBanking-Brasil/openapi/main/swagger-apis/payments/3.0.0-beta.1.yml
 * URL: /pix/payments/{paymentId}
 * URL: /pix/payments
 * Version: 3.0.0-beta.1
 */
@ApiName("Post Payment Consent V3")
public class PostPaymentsConsentValidatorV3 extends GetPaymentsConsentValidatorV3 {

	@Override
	protected boolean isRejectionReasonNeeded() {
		return false;
	}
}
