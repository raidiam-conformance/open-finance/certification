package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived;

import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AbstractValidateWebhooksReceived;

public class ValidateRecurringConsentWebhooksOptionalAmount extends AbstractValidateWebhooksReceived {

	@Override
	protected int expectedWebhooksReceivedAmount() {
		return 2;
	}

	@Override
	protected String webhookType() {
		return "recurring_consent";
	}
}

