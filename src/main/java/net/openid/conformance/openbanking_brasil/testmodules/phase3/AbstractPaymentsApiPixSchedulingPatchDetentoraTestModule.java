package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckForAccessTokenValue;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractExpiresInFromTokenEndpointResponse;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.ValidateExpiresIn;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason.EnsureCancellationReasonWasCanceladoAgendamento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancelledFrom.EnsureCancelledFromWasDetentora;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasCanc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusSchdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;

public abstract class AbstractPaymentsApiPixSchedulingPatchDetentoraTestModule extends AbstractPaymentsPixSchedulingTestModule{

	private boolean isPostPolling = false;

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
	}

	@Override
	protected void executePostPollingSteps() {
		createNewClientCredentialsToken();
		isPostPolling = true;
		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(20)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		callAndStopOnFailure(EnsurePaymentStatusWasCanc.class);
		callAndStopOnFailure(EnsureCancelledFromWasDetentora.class);
		callAndStopOnFailure(EnsureCancellationReasonWasCanceladoAgendamento.class);
	}

	protected void createNewClientCredentialsToken() {
		runInBlock("Create new client_credentials token to continue test", () -> {
			callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
			callAndStopOnFailure(SetPaymentsScopeOnTokenEndpointRequest.class);
			call(sequence(addTokenEndpointClientAuthentication));
			callAndStopOnFailure(CallTokenEndpoint.class);
			callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
			callAndStopOnFailure(CheckForAccessTokenValue.class);
			callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);
			callAndContinueOnFailure(ExtractExpiresInFromTokenEndpointResponse.class, Condition.ConditionResult.FAILURE, "RFC6749-4.4.3", "RFC6749-5.1");
			call(condition(ValidateExpiresIn.class)
				.skipIfObjectMissing("expires_in")
				.onSkip(Condition.ConditionResult.INFO)
				.requirements("RFC6749-5.1")
				.onFail(Condition.ConditionResult.FAILURE)
				.dontStopOnFailure());
			callAndStopOnFailure(SaveAccessToken.class);
		});
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		if (isPostPolling) {
			return CheckPaymentPollStatusSchdV2.class;
		}
		return super.getPaymentPollStatusCondition();
	}
}
