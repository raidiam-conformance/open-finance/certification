package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetAutomaticPaymentRecurringConsentV1Endpoint extends AbstractGetXFromAuthServer {

		@Override
		protected String getEndpointRegex() {
			return "^(https://)(.*?)(/open-banking/automatic-payments/v\\d+/recurring-consents)$";
		}

		@Override
		protected String getApiFamilyType() {
			return "payments-recurring-consents";
		}

		@Override
		protected String getApiVersionRegex() {
			return "^(1.[0-9].[0-9])$";
		}

		@Override
		protected boolean isResource() {
			return false;
		}
	}
