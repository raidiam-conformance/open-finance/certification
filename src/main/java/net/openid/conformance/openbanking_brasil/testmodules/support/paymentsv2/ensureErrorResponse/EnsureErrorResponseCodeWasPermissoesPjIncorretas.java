package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse;

import net.openid.conformance.openbanking_brasil.testmodules.support.enums.consentsV3.CreateConsent422ErrorEnumV3;

import java.util.List;

public class EnsureErrorResponseCodeWasPermissoesPjIncorretas extends AbstractEnsureErrorResponseCodeFieldWas {

	@Override
	protected List<String> getExpectedCodes() {
		return List.of(CreateConsent422ErrorEnumV3.PERMISSOES_PJ_INCORRETAS.toString());
	}
}
