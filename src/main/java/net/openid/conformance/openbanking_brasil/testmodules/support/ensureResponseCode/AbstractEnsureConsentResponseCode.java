package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractEnsureConsentResponseCode extends AbstractEnsureResponseCodeWas {

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		return super.evaluate(env);
	}

	@Override
	protected String getEnvKey(){
		return "consent_endpoint_response_full";
	}

}
