package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentRejectionReasonEnum;

public class EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoEnrollment extends AbstractEnsureEnrollmentRejectionReasonWas {

    @Override
    protected String getExpectedRejectionReason() {
        return EnrollmentRejectionReasonEnum.REJEITADO_TEMPO_EXPIRADO_ENROLLMENT.toString();
    }
}
