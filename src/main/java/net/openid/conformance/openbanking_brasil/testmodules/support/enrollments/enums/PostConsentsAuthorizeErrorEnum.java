package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PostConsentsAuthorizeErrorEnum {
    STATUS_VINCULO_INVALIDO, STATUS_CONSENTIMENTO_INVALIDO, RISCO, FALTAM_SINAIS_OBRIGATORIOS_DA_PLATAFORMA,
	CONTA_DEBITO_DIVERGENTE_CONSENTIMENTO_VINCULO, PARAMETRO_NAO_INFORMADO, PARAMETRO_INVALIDO, ERRO_IDEMPOTENCIA;

    public static Set<String> toSet(){
        return Stream.of(values())
            .map(Enum::name)
            .collect(Collectors.toSet());
    }
}
