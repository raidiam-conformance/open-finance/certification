package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JWTUtil;

import java.text.ParseException;

public class EnsureRefreshTokenIsAJwt extends AbstractCondition {
	@Override
	@PreEnvironment(required = "token_endpoint_response")
	public Environment evaluate(Environment env) {
		String refreshToken = env.getString("token_endpoint_response", "refresh_token");
		if (!Strings.isNullOrEmpty(refreshToken)) {
			try {
				JWTUtil.parseJWT(refreshToken);
			} catch (ParseException e) {
				throw error("Refresh token is not a JWT");
			}
			logSuccess("Refresh token is a JWT");
			return env;
		} else {
			throw error("Couldn't find refresh token");
		}
	}
}
