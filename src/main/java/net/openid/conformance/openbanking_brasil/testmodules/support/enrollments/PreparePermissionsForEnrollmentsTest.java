package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PreparePermissionsForEnrollmentsTest extends AbstractCondition {

    @Override
    @PostEnvironment(strings = "enrollment_permissions")
    public Environment evaluate(Environment env) {
        String[] permissions = {"PAYMENTS_INITIATE"};
        env.putString("enrollment_permissions", String.join(" ", permissions));
        logSuccess("Set permissions for enrollments request", args("permissions", permissions));
        return env;
    }
}
