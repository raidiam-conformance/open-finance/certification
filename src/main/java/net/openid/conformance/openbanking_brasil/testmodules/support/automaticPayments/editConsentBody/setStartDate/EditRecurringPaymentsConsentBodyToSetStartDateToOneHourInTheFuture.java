package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setStartDate;

import java.time.LocalDateTime;

public class EditRecurringPaymentsConsentBodyToSetStartDateToOneHourInTheFuture extends AbstractEditRecurringPaymentsConsentBodyToSetStartDate {

	@Override
	protected LocalDateTime calculateStartDateTime(LocalDateTime currentDateTime) {
		return currentDateTime.plusHours(1);
	}
}
