package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureDailyLimitSetTo1BRL;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureTransactionLimitSetTo1BRL;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsRequestBodyWithoutDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountTo05BRLOnConsent;

public abstract class AbstractFvpEnrollmentsApiPaymentsPreEnrollmentTestModule extends AbstractEnrollmentsApiPaymentsPreEnrollmentTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(CreateEnrollmentsRequestBodyWithoutDebtorAccount.class);
		callAndStopOnFailure(PrepareToPostEnrollments.class);
	}

	protected void configureDictInfo() {
		callAndStopOnFailure(SetPaymentAmountTo05BRLOnConsent.class);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getConsentsValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected void checkStatusAfterAuthorization() {
		super.checkStatusAfterAuthorization();
		callAndStopOnFailure(EnsureDailyLimitSetTo1BRL.class);
		callAndStopOnFailure(EnsureTransactionLimitSetTo1BRL.class);
	}

}
