package net.openid.conformance.openbanking_brasil.testmodules.v3.resources;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.OperationalLimitsToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveConsentIdFromClientScopes;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ResourceApiV2PollingSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.AbstractOperationalLimitsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "resources_api_operational-limits_test-module_v3",
	displayName = "Test will make sure that the server has not implemented any type of operational limits for the Resources API.",
	summary = "This test will generate three different consent requests and call the resources API 450 times for each created consent\n" +
		"\u2022 Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test has been provided\n" +
		"\u2022 Using the client_id for OL and the CPF/CNPJ for OL create a Consent Request sending either business or customer permissions, depending on what has been provided on the test plan configuration - Expect Server to return a 201 - Save ConsentID (1)\n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 With the authorized consent id (1) , call the GET Resources once - Expect a 200 or 202. If the response was 202, the resource will polled again (maximum 4 times) after 30 second pause until receiving 200\n" +
		"\u2022 With the authorized consent id (1) , call the GET Resources API 449 Times - Expect a 200 on all requests\n" +
		"\u2022 Using the client_id for OL and the CPF/CNPJ for OL create a Consent Request sending either business or customer permissions, depending on what has been provided on the test plan configuration - Expect Server to return a 201 - Save ConsentID (2)\n" +
		"\u2022 Redirect User to authorize the Created Consent - Expect a successful authorization\n" +
		"\u2022 With the authorized consent id (2) , call the GET Resources once - Expect a 200 or 202. If the response was 202, the resource will polled again (maximum 4 times) after 30 second pause until receiving 200\n" +
		"\u2022 With the authorized consent id (2) , call the GET Resources API 449 Times - Expect a 200 on all requests\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness"
	}
)
public class ResourcesApiOperationalLimitsTestModuleV3 extends AbstractOperationalLimitsTestModule {

	private int currentBatch = 1;
	private static final int NUMBER_OF_EXECUTIONS = 450;

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(
			sequenceOf(
				condition(GetConsentV3Endpoint.class),
				condition(GetResourcesV3Endpoint.class)
			)
			)
		);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		String productType = env.getString("config", "consent.productType");
		if (productType.equals("business")) {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ALL_BUSINESS_PHASE2).addScopes(OPFScopesEnum.RESOURCES, OPFScopesEnum.OPEN_ID).build();
		} else {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ALL_PERSONAL_PHASE2).addScopes(OPFScopesEnum.RESOURCES, OPFScopesEnum.OPEN_ID).build();
		}
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void requestProtectedResource() {
		for (int i = 0; i < NUMBER_OF_EXECUTIONS; i++) {
			String message = String.format("[%d] Calling Resources Endpoint with consent_id_%d", i + 1, currentBatch);

			if (i == 0) {
				ResourceApiV2PollingSteps pollingSteps = new ResourceApiV2PollingSteps(env, getId(),
					eventLog, testInfo, getTestExecutionManager());
				runInBlock(message, () -> call(pollingSteps));
				validateResponse();
			} else {
				preCallProtectedResource(message);
			}
		}

	}


	@Override
	protected void onPostAuthorizationFlowComplete() {
		expose("consent_id_" + currentBatch, env.getString("consent_id"));
		enableLogging();
		if (currentBatch == 2) {
			fireTestFinished();
		} else {

			callAndContinueOnFailure(RemoveConsentIdFromClientScopes.class, Condition.ConditionResult.FAILURE);
			performAuthorizationFlow();
			currentBatch++;
		}

	}

	@Override
	protected void validateResponse() {
		runInLoggingBlock(() -> {
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetResourcesOASValidatorV3.class, Condition.ConditionResult.FAILURE);
		});
	}

}
