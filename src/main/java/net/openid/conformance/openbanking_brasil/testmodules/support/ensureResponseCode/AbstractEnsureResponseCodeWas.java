package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;

public abstract class AbstractEnsureResponseCodeWas extends AbstractCondition {
	@Override
	public Environment evaluate(Environment env) {
		JsonObject response = env.getObject(getEnvKey());
		int status = OIDFJSON.getInt(Optional.ofNullable(response.get("status"))
			.orElse(response.get("code")));

		if(!getExpectedStatus().contains(HttpStatus.valueOf(status))) {
			throw error("Failed to obtain expected status", args("expected status", getExpectedStatus(), "obtained status", status));
		} else {
			logSuccess("Correct status obtained", args("status", status));
		}
		return env;
	}

	protected abstract List<HttpStatus> getExpectedStatus();
	protected abstract String getEnvKey();
}
