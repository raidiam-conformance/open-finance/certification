package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateCompleteRiskSignalsRequestBodyToRequestEntityClaims;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditConsentsAuthoriseRequestBodyToAddCompleteRiskSignals extends AbstractCondition {

	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";

	@Override
	@PreEnvironment(required = REQUEST_CLAIMS_KEY)
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject(REQUEST_CLAIMS_KEY, "data"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Could not find data inside consents authorise request body"));

		JsonObject riskSignals = CreateCompleteRiskSignalsRequestBodyToRequestEntityClaims.getRiskSignalsObject().getAsJsonObject("data");

		data.add("riskSignals", riskSignals);

		logSuccess("Added complete \"riskSignals\" field to consents authorise request body", args("request body data", data));

		return env;
	}
}
