package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JWTUtil;

import java.text.ParseException;

public class CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida extends AbstractCondition {
	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		try {
			JsonObject response = env.getObject("consent_endpoint_response_full");
			JsonObject jwt = JWTUtil.jwtStringToJsonObjectForEnvironment(OIDFJSON.getString(response.get("body")));
			JsonObject claims = jwt.getAsJsonObject("claims");

			if (!claims.has("errors")){
				logSuccess("No errors in response");
				env.putBoolean("error_status_FPI", false);
				return env;
			}

			JsonArray errors = claims.getAsJsonArray("errors");

			for (JsonElement e : errors) {
				JsonObject error = (JsonObject) e;
				String errorCode = OIDFJSON.getString(error.get("code"));
				if (errorCode.equals("FORMA_PAGAMENTO_INVALIDA")) {
					logSuccess("Error code is FORMA_PAGAMENTO_INVALIDA");
					env.putBoolean("error_status_FPI", true);
					return env;
				}
			}
			logSuccess("Error code is not FORMA_PAGAMENTO_INVALIDA");
			env.putBoolean("error_status_FPI", false);

		} catch (ParseException e) {
			throw error("Could not parse JWT");
		}

		return env;
	}
}
