package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToCreditFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractCreditFixedIncomesApiResourcesTest extends AbstractInvestmentsApiResourcesTestModule {

	@Override
	protected Class<? extends Condition> getResourceValidator() {
		return GetResourcesOASValidatorV3.class;
	}

    @Override
    protected AbstractSetInvestmentApi investmentApi() {
        return new SetInvestmentApiToCreditFixedIncomes();
    }

    @Override
    protected String resourceType() {
        return EnumResourcesType.CREDIT_FIXED_INCOME.name();
    }
	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.CREDIT_FIXED_INCOMES;
	}
	@Override
	protected OPFCategoryEnum getProductCategoryEnum() {
		return OPFCategoryEnum.INVESTMENTS;
	}
}
