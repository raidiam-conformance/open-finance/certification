package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.validateVersionHeader;

public class ValidateVersionHeader2d0d0 extends AbstractValidateVersionHeader {

	@Override
	protected String getExpectedVersion() {
		return "2.0.0";
	}
}
