package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsTransactionsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.AddToAndFromTransactionConversionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckTransactionConversionDateRange;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToFunds;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.CheckAllTransactionsConversionDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetFromAndToTransactionConversionDateToSavedTransactionConversionDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetFromTransactionConversionDateTo180DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetFromTransactionConversionDateTo360DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetToTransactionConversionDateTo180DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetToTransactionConversionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "funds_api_transactiondate_test-module",
    displayName = "funds_api_transactiondate_test-module",
    summary = "This test will confirm that the institution has correctly implemented the queryParameters fromTransactionConversionDate and toTransactionConversionDate for the Funds API\n" +
        "• Call the POST Consents endpoint with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
        "• Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Consents endpoint\n" +
        "• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
        "• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
        "• Call the GET Investments List Endpoint\n" +
        "• Expect a 200 Response - Extract one InvestmentId - Validate the Response_BODY\n" +
        "• Call the GET Investments Transactions Endpoint with the Extracted investmentId - Don't send any query parameters\n" +
        "• Expect a 200 - Make sure if any transactions are returned, that they are from the current date.\n" +
        "• Call the GET Investments Transactions Endpoint - Send query Params fromTransactionConversionDate=D-360 and toTransactionConversionDate=D+0\n" +
        "• Expect a 200 - Make sure at least one transaction is returned, get the transactionConversionDate of one transaction, make sure this transaction is within the range above\n" +
        "• Call GET Investments Transactions API - Send query Params fromTransactionConversionDate=D-180 and toTransactionConversionDate=D+0\n" +
        "• Expect a 200 - Make sure at least one transaction is returned, get the transactionConversionDate of one transaction, make sure this transaction is within the range above\n" +
        "• Call GET Investments Transactions API - Send query Params fromTransactionConversionDate=D-360 and toTransactionConversionDate=D-180\n" +
        "• Expect a 200 - Make sure at least one transaction is returned, get the transactionConversionDate of one transaction, make sure this transaction is within the range above - Extract the date from one of the transactions returned on that API Call \n" +
        "• Call GET Investments Transactions API, send query parameters fromTransactionConversionDate and toTransactionConversionDate to be the transactionConversionDate extracted on the step above\n" +
        "• Expect 200, make sure that the returned transactions is from exactly the date returned above and that no transactions from other dates were returned\n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "directory.discoveryUrl",
        "resource.brazilCpf"
    }
)
public class FundsApiTransactionDateTestModule extends AbstractInvestmentsApiTransactionDateTestModule {

    @Override
    protected AbstractSetInvestmentApi setInvestmentsApi() {
        return new SetInvestmentApiToFunds();
    }

    @Override
    protected Class<? extends Condition> investmentsRootValidator() {
        return GetFundsListV1OASValidator.class;
    }

    @Override
    protected Class<? extends Condition> investmentsTransactionsValidator() {
        return GetFundsTransactionsV1OASValidator.class;
    }

    @Override
    protected Class<? extends Condition> setFromTransactionTo180DaysAgo() {
        return SetFromTransactionConversionDateTo180DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> setToTransactionToToday() {
        return SetToTransactionConversionDateToToday.class;
    }

    @Override
    protected Class<? extends Condition> setFromTransactionTo360DaysAgo() {
        return SetFromTransactionConversionDateTo360DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> setToTransactionTo180DaysAgo() {
        return SetToTransactionConversionDateTo180DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
        return AddToAndFromTransactionConversionDateParametersToProtectedResourceUrl.class;
    }

    @Override
    protected Class<? extends Condition> setFromAndToTransactionToSavedTransaction() {
        return SetFromAndToTransactionConversionDateToSavedTransactionConversionDate.class;
    }

    @Override
    protected Class<? extends Condition> checkTransactionRange() {
        return CheckTransactionConversionDateRange.class;
    }

    @Override
    protected Class<? extends Condition> checkAllDates() {
        return CheckAllTransactionsConversionDates.class;
    }

    @Override
    protected String transactionParameterName() {
        return "TransactionConversionDate";
    }

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.FUNDS;
	}
}
