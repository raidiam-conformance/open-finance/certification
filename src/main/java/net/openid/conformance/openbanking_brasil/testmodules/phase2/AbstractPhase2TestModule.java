package net.openid.conformance.openbanking_brasil.testmodules.phase2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveProtectedResourceAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.AbstractEnsureConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;
import org.apache.http.HttpStatus;
import org.springframework.http.HttpMethod;

import java.util.Optional;


@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"consent.productType"
})

public abstract class AbstractPhase2TestModule extends AbstractOBBrasilFunctionalTestModule {

	protected abstract Class<? extends Condition> getPostConsentValidator();

	protected abstract ConditionSequence getConsentAndResourceV2EndpointSequence();

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps();
	}


	@Override
	protected void configureClient() {
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(getConsentAndResourceV2EndpointSequence()));
		super.configureClient();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndContinueOnFailure(SaveConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
		runInBlock(currentClientString() + "Validate consents response",
			() -> callAndContinueOnFailure(getPostConsentValidator(), Condition.ConditionResult.FAILURE));
	}

	@Override
	public void cleanup() {
		eventLog.startBlock("Deleting consent");
		callAndContinueOnFailure(LoadConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CreateRandomFAPIInteractionId.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(PrepareToFetchConsentRequest.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		super.call(builder);

		String resourceResponse = "resource_endpoint_response_full";
		String consentResponse = "consent_endpoint_response_full";
		String methodString = env.getString("http_method");
		HttpMethod method;
		if (methodString != null) {
			method = HttpMethod.valueOf(methodString);
			if (method.equals(HttpMethod.DELETE)) {
				return;
			}
		}

		if (CallProtectedResource.class.equals(builder.getConditionClass())) {
			if (env.getEffectiveKey(resourceResponse).equals(resourceResponse)) {
				validateLinksAndMeta(resourceResponse);
			} else {
				validateLinksAndMeta(consentResponse);
			}
		}
		if (CallConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
			CallConsentEndpointWithBearerTokenAnyHttpMethod.class.equals(builder.getConditionClass())) {
			validateLinksAndMeta("consent_endpoint_response_full");
		}
	}

	private void validateLinksAndMeta(String responseFull) {

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
			.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));
		if (!isEndpointCallSuccessful(status)) {
			return;
		}

		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");
		if (recursion.equals("false")) {
			callAndStopOnFailure(SaveOldValues.class);
			env.putString("recursion", "true");
			env.mapKey("resource_endpoint_response_full", responseFull);

			if (!responseFull.contains("consent") || env.getElementFromObject(responseFull, "body_json.links") != null) {
				validateLinks(responseFull);
			}

			env.unmapKey("resource_endpoint_response_full");
			callAndStopOnFailure(LoadOldValues.class);
			env.putString("recursion", "false");
		}
	}

	protected void validateLinks(String responseFull) {
		call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
		env.mapKey("resource_endpoint_response_full", responseFull);

		ConditionSequence sequence = new ValidateSelfEndpoint();

		if (responseFull.contains("consent")) {
			sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
		}

		call(sequence);
	}

	private boolean isEndpointCallSuccessful(int status) {
		if (status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK) {
			return true;
		}
		return false;
	}

	protected void fetchConsentToCheckStatus(String status, AbstractEnsureConsentStatusWas statusCondition) {
		runInBlock(String.format("Checking the created consent - Expecting %s status", status), () -> {
			callAndStopOnFailure(SaveProtectedResourceAccessToken.class);
			callAndStopOnFailure(LoadConsentsAccessToken.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			ConditionSequence sequence = new ValidateSelfEndpoint();

			sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
			call(sequence);
			callAndContinueOnFailure(statusCondition.getClass(), Condition.ConditionResult.FAILURE);
		});
	}


}
