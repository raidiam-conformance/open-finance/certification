package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiConsentsRejectionReasonTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_consents_rejection-reason_test-module_v4",
	displayName = "payments_api_consents_rejection-reason_test-module_v4",
	summary = "Ensure the correct rejectionReason is returned whe the consent is rejected in different scenarios\n" +
		"1. Ensure consent is rejected when the same account is sent on creditor and debtor account\n" +
		"• Call POST consent with the same info on creditorAccount and debtorAccount\n" +
		"• Call the POST Consents endpoints using token issued via client_credentials grant\n" +
		"• Expects 422 DETALHE_PAGAMENTO_INVALIDO - Validate Error Message\n" +
		"• If no errors are found:\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user \n" +
		"• Ensure an error is returned and that the authorization code was not sent back \n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"REJECTED\" and rejectionReason is CONTAS_ORIGEM_DESTINO_IGUAIS\n" +
		"2. Ensure consent is rejected when the user rejects it manually\n" +
		"• Call POST consent with valid payload\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user to reject the consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"REJECTED\" and rejectionReason is REJEITADO_USUARIO\n" +
		"3. Ensure consent is rejected when the authorization time expires. This step will take 5 minutes to run.\n" +
		"• Call POST consent with the expirationDateTime as creationDateTime + 5 minutes\n" +
		"• Expects 201 - Validate response\n" +
		"• Set the conformance suite to wait 5 minutes\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"REJECTED\" and rejectionReason is TEMPO_EXPIRADO_AUTORIZACAO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)

public class PaymentsApiConsentsRejectionReasonTestModuleV4 extends AbstractPaymentsApiConsentsRejectionReasonTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}
}
