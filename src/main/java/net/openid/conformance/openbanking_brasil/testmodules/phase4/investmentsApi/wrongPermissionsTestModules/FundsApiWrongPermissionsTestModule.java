package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToFunds;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "funds_api_wrong-permissions_test-module",
    displayName = "funds_api_wrong-permissions_test-module",
    summary = "Test will confirm that a Consent without the investments permissions cannot grant access to the user investments - This test will start with requiring two Consent Sharing requests to be approved.\n" +
        "(1) - Happy path to obtain an InvestmentId\n" +
        "• Call the POST Consents endpoint with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
        "• Expect a 201 - Validate Response and confirm status is on Awaiting Authorisation\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Consents endpoint\n" +
        "• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
        "• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
        "• Call the GET Investments List Endpoint\n" +
        "• Expects a 200 response - Validate the Response_body - Extract one of the shared investment products\n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body\n" +
        "(2) - Unhappy, making sure access is not granted without proper permission group\n" +
        "• Call the POST Consents with either the customer business or the customer personal permissions group, depending on whether the brazilCnpj has or not been provided\n" +
        "• Expects a success 200 - Validate response\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Investments List Endpoint\n" +
        "• Expects a 403 response \n" +
        "• Call the GET Investments Identification endpoint with the extracted investmentID\n" +
        "• Expects a 403 response \n" +
        "• Call the GET Investments Balance endpoint with the extracted investmentID\n" +
        "• Expects a 403 response \n" +
        "• Call the GET Investments Transactions endpoint with the extracted investmentID\n" +
        "• Expects a 403 response \n" +
        "• Call the GET Investments Transactions-Current endpoint with the extracted investmentID\n" +
        "• Expects a 403 response \n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "directory.discoveryUrl",
        "resource.brazilCpf",
        "resource.brazilCnpj"
    }
)
public class FundsApiWrongPermissionsTestModule extends AbstractInvestmentsApiWrongPermissionsTestModule {

    @Override
    protected AbstractSetInvestmentApi investmentApi() {
        return new SetInvestmentApiToFunds();
    }

    @Override
    protected Class<? extends Condition> apiValidator() {
        return GetFundsListV1OASValidator.class;
    }

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.FUNDS;
	}
}
