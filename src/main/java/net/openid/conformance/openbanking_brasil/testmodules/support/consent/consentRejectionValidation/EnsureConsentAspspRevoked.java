package net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class EnsureConsentAspspRevoked extends AbstractConsentRejectionValidation {

	@Override
	protected String getRejectionReasonCode() {
		return "CUSTOMER_MANUALLY_REVOKED";
	}

	@Override
	protected String getRejectedBy() {
		return "USER";
	}

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		try {
			JsonObject data = extractData(env);
			String status = OIDFJSON.getString(data.get("status"));
			if (status.equals("REJECTED")) {
				if (validateResponse(data)) {
					logSuccess("Rejection object contains expected results.",
						args("Expected reason: ", getRejectionReasonCode(), "Expected rejectedBy: ", getRejectedBy()));
					env.putBoolean("code_returned", true);
				} else {
					throw error("Rejection object did not match expected results.",
						args("Expected reason: ", getRejectionReasonCode(), "Expected rejectedBy: ", getRejectedBy()));
				}
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}
}
