package net.openid.conformance.openbanking_brasil.testmodules.abstractModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.RememberOriginalScopes;
import net.openid.conformance.openbanking_brasil.testmodules.ResetScopesToConfigured;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;

public abstract class AbstractApiWrongPermissionsTestModulePhase2 extends AbstractPhase2TestModule {

    protected boolean isFirstConsent = true;
    protected boolean preFetchResources = false;
    protected boolean preFetched = false;


	protected abstract OPFScopesEnum getScope();
	protected abstract Class<? extends Condition> contractSelector();
    protected abstract Class<? extends Condition> prepareUrlForFetchingRootEndpoint();
    protected abstract Class<? extends Condition> prepareUrlForFetchingContractEndpoint();
    protected abstract Class<? extends Condition> prepareUrlForFetchingContractWarrantiesEndpoint();
    protected abstract Class<? extends Condition> prepareUrlForFetchingContractPaymentsEndpoint();
    protected abstract Class<? extends Condition> prepareUrlForFetchingContractScheduledInstalmentsEndpoint();
	protected abstract Class<? extends Condition> apiResourceContractListResponseValidator();
	protected abstract Class<? extends Condition> apiResourceContractResponseValidator();
	protected abstract Class<? extends Condition> apiResourceContractGuaranteesResponseValidator();
	protected abstract Class<? extends Condition> apiResourceContractPaymentsResponseValidator();
	protected abstract Class<? extends Condition> apiResourceContractInstallmentsResponseValidator();

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        super.onConfigure(config, baseUrl);
        callAndStopOnFailure(RememberOriginalScopes.class);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_OPERATIONS).addScopes(getScope(), OPFScopesEnum.OPEN_ID).build();
    }

    @Override
    protected void configureClient() {
        super.configureClient();
    }

    @Override
    protected void requestProtectedResource() {
        if(!preFetchResources) {
            preFetchResources = true;
            super.requestProtectedResource();
            eventLog.startBlock(currentClientString() + "Validate response");
            preFetchResources();
            callAndStopOnFailure(ResetScopesToConfigured.class);
			ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
			String productType = env.getString("config", "consent.productType");
			if (productType.equals("business")) {
				scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.BUSINESS_REGISTRATION_DATA).build();
			} else {
				scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA).build();
			}
            performAuthorizationFlow();
            eventLog.endBlock();
        }
    }

    protected void preFetchResources() {
        callAndStopOnFailure(contractSelector());
        callAndStopOnFailure(prepareUrlForFetchingContractEndpoint());
        preCallProtectedResource("Calling Contract endpoint");
        callAndStopOnFailure(prepareUrlForFetchingContractWarrantiesEndpoint());
        preCallProtectedResource("Calling Contract Warranties endpoint");
        callAndStopOnFailure(prepareUrlForFetchingContractPaymentsEndpoint());
        preCallProtectedResource("Calling Contract Payments endpoint");
        callAndStopOnFailure(prepareUrlForFetchingContractScheduledInstalmentsEndpoint());
        preCallProtectedResource("Calling Contract Scheduled Instalments endpoint");
    }

    @Override
    protected void onPostAuthorizationFlowComplete() {
        if(!preFetched) {
            preFetched = true;
            return;
        }

        requestResourcesWithIncorrectPermissions();

        fireTestFinished();
    }

    protected void requestResourcesWithIncorrectPermissions() {
        runInBlock("Ensure we cannot call the root endpoint", () -> {
            callAndStopOnFailure(prepareUrlForFetchingRootEndpoint());
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(apiResourceContractListResponseValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });

        runInBlock("Ensure we cannot call the Contract endpoint", () -> {
            callAndStopOnFailure(prepareUrlForFetchingContractEndpoint());
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(apiResourceContractResponseValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });

        runInBlock("Ensure we cannot call the Contract Warranties endpoint", () -> {
            callAndStopOnFailure(prepareUrlForFetchingContractWarrantiesEndpoint());
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(apiResourceContractGuaranteesResponseValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });

        runInBlock("Ensure we cannot call the Contract Payments endpoint", () -> {
            callAndStopOnFailure(prepareUrlForFetchingContractPaymentsEndpoint());
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(apiResourceContractPaymentsResponseValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });

        runInBlock("Ensure we cannot call the Contract Scheduled Instalments endpoint", () -> {
            callAndStopOnFailure(prepareUrlForFetchingContractScheduledInstalmentsEndpoint());
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(apiResourceContractInstallmentsResponseValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });
    }

    @Override
    protected void performPreAuthorizationSteps() {
        if (!isFirstConsent) {
            cleanup();
        } else {
            isFirstConsent = false;
        }
        super.performPreAuthorizationSteps();
    }

    @Override
    protected void validateResponse() {}
}
