package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforceAbsenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.EnsurePaymentsTemporizationCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.InsertTemporizationTestValuesIntoConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason.EnsureCancellationReasonWasCanceladoPendencia;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancelledFrom.EnsureCancelledFromWasIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasCanc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasPndg;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRcvdOrPndg;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPatchPixPaymentsEndpointSequenceV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPaymentsApiCancTemporizationTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

    protected abstract Class<? extends Condition> patchPaymentValidator();

    @Override
    protected void setupResourceEndpoint() {
        super.setupResourceEndpoint();
        env.putBoolean("continue_test", true);
        callAndContinueOnFailure(EnsurePaymentsTemporizationCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
        callAndStopOnFailure(InsertTemporizationTestValuesIntoConsentRequest.class);
        if (!env.getBoolean("continue_test")) {
            fireTestSkipped("Test skipped since no Temporization CPF/CNPJ was informed.");
        }
    }

    @Override
    protected  void configureDictInfo(){
        callAndStopOnFailure(EnforceAbsenceOfDebtorAccount.class);
    }

    @Override
    protected void requestProtectedResource() {
        eventLog.startBlock("Calling POST payments, expecting PNDG or RCVD");
        call(getPixPaymentSequence());
        validateResponse();
        eventLog.endBlock();

        eventLog.startBlock("Calling PATCH payments, expecting response code 200");
        callAndStopOnFailure(CreatePatchPaymentsRequestFromConsentRequestV2.class);
        call(new CallPatchPixPaymentsEndpointSequenceV2());
        callAndContinueOnFailure(patchPaymentValidator(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();

        eventLog.startBlock("Calling GET payments, expecting response code 200 with a canceled state");
        call(new CallGetPaymentEndpointSequence()
            .replace(PaymentInitiationPixPaymentsValidatorV2.class, condition(getPaymentValidator())));
        callAndStopOnFailure(EnsurePaymentStatusWasCanc.class);
        callAndStopOnFailure(EnsureCancelledFromWasIniciadora.class);
        callAndStopOnFailure(EnsureCancellationReasonWasCanceladoPendencia.class);
        eventLog.endBlock();
    }

    @Override
    protected void validateFinalState(){
        callAndStopOnFailure(EnsurePaymentStatusWasPndg.class);
    }

    @Override
    protected ConditionSequence getPixPaymentSequence() {
        return new CallPixPaymentsEndpointSequence()
            .insertAfter(EnsureResourceResponseCodeWas201.class, condition(EnsurePaymentStatusWasRcvdOrPndg.class));
    }
}
