package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixUniquePaymentPathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalFutureDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.AbstractEnsurePaymentStatusWasX;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixScheduledFirstPaymentTestModule extends AbstractAutomaticPaymentsAutomaticPixUniquePaymentPathTestModule {

	@Override
	protected boolean isHappyPath() {
		return true;
	}

	@Override
	protected AbstractEnsurePaymentStatusWasX finalPaymentStatusCondition() {
		return new EnsurePaymentStatusWasSchd();
	}

	@Override
	protected AbstractEnsurePaymentConsentStatusWas finalConsentStatusCondition() {
		return new EnsurePaymentConsentStatusWasAuthorised();
	}

	@Override
	protected String finalConsentStatus() {
		return "AUTHORISED";
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorResponseCodeFieldCondition() {
		return null;
	}

	@Override
	protected AbstractEnsureRecurringPaymentsRejectionReason ensureRejectionReasonCondition() {
		return null;
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalFutureDate();
	}
}
