package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.capitalizationBonds.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetCapitalizationBondsOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/capitalizationBonds/swagger-capitalization-bonds-2.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/bonds";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		for(JsonElement dataElement : body.getAsJsonArray("data")) {
			JsonObject data = dataElement.getAsJsonObject();
			this.assertCapitalizationPeriodConstraints(data);
			this.assertContributionPaymentConstraints(data);
			this.assertDrawsConstraint(data);
		}
	}

	protected void assertCapitalizationPeriodConstraints(JsonObject data) {
		JsonArray capitalizationPeriods = JsonPath.read(data, "$.society.products[*].capitalizationPeriod");
		for(JsonElement capitalizationPeriodElement : capitalizationPeriods) {
			JsonObject capitalizationPeriod = capitalizationPeriodElement.getAsJsonObject();
			this.assertField1IsRequiredWhenField2HasValue2(
				capitalizationPeriod,
				"updateIndexAdditionalInfo",
				"updateIndex",
				"OUTROS"
			);

			JsonArray contributionAmounts = capitalizationPeriod.getAsJsonArray("contributionAmount");
			for(JsonElement contributionAmountElement : contributionAmounts) {
				this.assertField1IsRequiredWhenField2HasValue2(
					contributionAmountElement.getAsJsonObject(),
					"periodicityAdditionalInfo",
					"periodicity",
					"OUTROS"
				);
			}
		}
	}

	protected void assertContributionPaymentConstraints(JsonObject data) {
		JsonArray contributionPayments = JsonPath.read(data, "$.society.products[*].contributionPayment");
		for(JsonElement contributionPaymentElement : contributionPayments) {
			JsonObject contributionPayment = contributionPaymentElement.getAsJsonObject();
			this.assertField1IsRequiredWhenField2HasValue2(
				contributionPayment,
				"paymentMethodAdditionalInfo",
				"paymentMethod",
				"OUTROS"
			);
			this.assertField1IsRequiredWhenField2HasValue2(
				contributionPayment,
				"updateIndexAdditionalInfo",
				"updateIndex",
				"OUTROS"
			);
		}
	}

	protected void assertDrawsConstraint(JsonObject data) {
		JsonArray draws = JsonPath.read(data, "$.society.products[*].draws[*]");
		for(JsonElement drawElement : draws) {
			this.assertField1IsRequiredWhenField2HasValue2(
				drawElement.getAsJsonObject(),
				"timeIntervalAdditionalInfo",
				"timeInterval",
				"OUTROS"
			);
		}
	}
}
