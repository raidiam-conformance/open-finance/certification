package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1;

import com.google.gson.JsonObject;
import org.springframework.http.HttpMethod;

import java.util.List;

public class GetEnrollmentsOASValidatorV1 extends AbstractEnrollmentsOASValidatorV1 {

	@Override
	protected String getEndpointPathSuffix() {
		return "/{enrollmentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertDebtorAccountIssuerConstraint(data);
		assertDebtorAccountConstraint(data);
		assertTransactionAndDailyLimitsConstraints(data);
	}

	protected void assertDebtorAccountConstraint(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"debtorAccount",
			"status",
			List.of("AWAITING_ENROLLMENT", "AUTHORISED", "REVOKED")
		);
	}

	protected void assertTransactionAndDailyLimitsConstraints(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionLimit",
			"status",
			List.of("AWAITING_ENROLLMENT", "AUTHORISED")
		);
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"dailyLimit",
			"status",
			List.of("AWAITING_ENROLLMENT", "AUTHORISED")
		);
	}
}
