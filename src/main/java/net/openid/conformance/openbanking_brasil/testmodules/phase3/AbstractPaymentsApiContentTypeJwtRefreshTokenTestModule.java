package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddScopeToTokenEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CompareIdTokenClaims;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureAccessTokenValuesAreDifferent;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractRefreshTokenFromTokenResponse;
import net.openid.conformance.condition.client.FAPIBrazilCallPaymentConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.SetApplicationJwtAcceptHeaderForResourceEndpointRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetScopeInClientConfigurationToOpenId;
import net.openid.conformance.condition.client.ValidateRefreshTokenNotRotated;
import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureTokenEndpointResponseScopeWasPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJWTAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJsonAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddNoAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenNoAcceptField;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentConsentEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentConsentEndpointSequenceErrorAgnostic;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetPaymentEndpointSequenceErrorAgnostic;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;

public abstract class AbstractPaymentsApiContentTypeJwtRefreshTokenTestModule extends AbstractOBBrasilPaymentFunctionalTestModule{

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();


	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(EnsureProxyPresentInConfig.class);
	}

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(SetScopeInClientConfigurationToOpenId.class);
		super.validateClientConfiguration();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.replace(SetPaymentsScopeOnTokenEndpointRequest.class, condition(AddScopeToTokenEndpointRequest.class))
			.insertAfter(CheckIfTokenEndpointResponseError.class, condition(EnsureTokenEndpointResponseScopeWasPayments.class).requirements("RFC6749-3.3").dontStopOnFailure())
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class)))
			.insertAfter(CheckForFAPIInteractionIdInResourceResponse.class,
				condition(EnsureMatchingFAPIInteractionId.class))
			.replace(FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class, condition(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenNoAcceptField.class));
	}

	@Override
	protected void requestProtectedResource() {
		if (!validationStarted) {
			validationStarted = true;
			call(new RefreshTokenRequestSteps(false, addTokenEndpointClientAuthentication)
				.skip(EnsureAccessTokenValuesAreDifferent.class, "No need to make this check")
				.insertAfter(CompareIdTokenClaims.class, condition(ValidateRefreshTokenNotRotated.class)));

			eventLog.startBlock("Calling POST Payments endpoint - */* accept header - expects 201 with JWT response");
			ConditionSequence pixSequence = getPixPaymentSequence()
				.replace(SetApplicationJwtAcceptHeaderForResourceEndpointRequest.class, condition(AddNoAcceptHeaderRequest.class));
			call(pixSequence);
			eventLog.startBlock(currentClientString() + "Validate response");
			call(getPaymentValidationSequence()
				.replace(getPaymentValidator(),condition(postPaymentValidator())));

			eventLog.startBlock("Calling GET Payments endpoint - */* accept header - expects 200 with JWT response");
			ConditionSequence getPaymentNoAcceptSequence = new CallGetPaymentEndpointSequence()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddNoAcceptHeaderRequest.class))
				.replace(PaymentInitiationPixPaymentsValidatorV2.class, condition(getPaymentValidator()));
			call(getPaymentNoAcceptSequence);

			eventLog.startBlock("Calling GET Payments endpoint - application/json accept header - expects 200 with JWT response or 406");
			ConditionSequence getPaymentJsonAcceptSequence = new CallGetPaymentEndpointSequenceErrorAgnostic()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddJsonAcceptHeaderRequest.class))
				.insertAfter(EnsureContentTypeJson.class, condition(getPaymentValidator())
					.onFail(Condition.ConditionResult.FAILURE)
					.dontStopOnFailure()
					.skipIfStringMissing("status_not_acceptable"))
				.replace(PaymentInitiationPixPaymentsValidatorV2.class, condition(getPaymentValidator())
					.onFail(Condition.ConditionResult.FAILURE)
					.dontStopOnFailure()
					.skipIfStringMissing("status_ok"));
			call(getPaymentJsonAcceptSequence);

			eventLog.startBlock("Calling GET Consents endpoint - */* accept header - expects 200 with JWT response");
			ConditionSequence getConsentNoAcceptSequence = new CallGetPaymentConsentEndpointSequence()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddNoAcceptHeaderRequest.class))
				.replace(PaymentConsentValidatorV2.class, condition(getPaymentConsentValidator()));
			call(getConsentNoAcceptSequence);

			eventLog.startBlock("Calling GET Consents endpoint - application/json accept header - expects 200 with JWT response or 406");
			ConditionSequence getConsentJsonAcceptSequence = new CallGetPaymentConsentEndpointSequenceErrorAgnostic()
				.replace(AddJWTAcceptHeaderRequest.class, condition(AddJsonAcceptHeaderRequest.class))
				.insertAfter(FetchServerKeys.class, condition(paymentConsentErrorValidator())
					.onFail(Condition.ConditionResult.FAILURE)
					.dontStopOnFailure()
					.skipIfStringMissing("status_not_acceptable"))
				.replace(PaymentConsentValidatorV2.class, condition(getPaymentConsentValidator())
					.onFail(Condition.ConditionResult.FAILURE)
					.dontStopOnFailure()
					.skipIfStringMissing("status_ok"));
			call(getConsentJsonAcceptSequence);

			eventLog.endBlock();
		}
	}

	@Override
	protected void requestAuthorizationCode() {
		super.requestAuthorizationCode();
		callAndStopOnFailure(ExtractRefreshTokenFromTokenResponse.class);
	}
}
