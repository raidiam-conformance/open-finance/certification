package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPreFlightCertCheckPaymentsModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.checkConsentsVersion.AbstractCheckConsentsVersion;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.checkConsentsVersion.CheckConsentsVersionV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_preflight_test-module_v4",
	displayName = "Pre-flight checks will validate the consentUrl along with the loggedUser and debtorAccount, the mTLS certificate before requesting an access token using the Directory client_id provided in the test configuration. Then, an SSA will be generated using the Open Banking Brasil Directory. Finally, it will verify if the consent version on configuration is set to v4.",
	summary = "Pre-flight checks will validate the consentUrl along with the loggedUser and debtorAccount, the mTLS certificate before requesting an access token using the Directory client_id provided in the test configuration. Then, an SSA will be generated using the Open Banking Brasil Directory. Finally, it will verify if the consent version on configuration is set to v4.",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"directory.client_id"
	}
)
public class PreFlightCertCheckPaymentsModuleV4 extends AbstractPreFlightCertCheckPaymentsModule {

	@Override
	protected AbstractCheckConsentsVersion versionCheckingCondition() {
		return new CheckConsentsVersionV4();
	}
}
