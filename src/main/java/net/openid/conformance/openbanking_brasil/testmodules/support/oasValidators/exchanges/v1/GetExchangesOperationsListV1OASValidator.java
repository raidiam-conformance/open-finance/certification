package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetExchangesOperationsListV1OASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/exchanges/exchanges-v1.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/operations";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
