package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.AddMapFromPaymentIdToEndToEndId;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ExtractFiveLastPaymentIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ExtractPaymentIdFromLastPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.PrepareToFetchNextPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.PrepareToFetchPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPaymentApiSchedulingTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	protected abstract AbstractSchedulePayment schedulingCondition();

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		callAndStopOnFailure(RemovePaymentConsentDate.class);
		callAndStopOnFailure(schedulingCondition().getClass());
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrAccpV2.class;
	}

	@Override
	protected void validateFinalState() {
		callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void validateResponse() {
		callAndStopOnFailure(ExtractPaymentIdFromLastPayment.class);
		extractNPaymentsId();
		callAndStopOnFailure(AddMapFromPaymentIdToEndToEndId.class);
		super.validateResponse();
		runInBlock("Validate that all the remaining payments are SCHD", this::validateAllPayments);
		fetchConsentToCheckStatus("CONSUMED", new EnsurePaymentConsentStatusWasConsumed());
	}

	@Override
	protected ConditionSequence getRepeatSequence() {
		return sequenceOf(
			condition(ClearRequestObjectFromEnvironment.class),
			condition(PrepareToFetchPayment.class),
			condition(SetResourceMethodToGet.class),
			condition(ClearContentTypeHeaderForResourceEndpointRequest.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(AddJWTAcceptHeader.class),
			condition(CallProtectedResource.class),
			condition(EnsureResourceResponseCodeWas200.class),
			condition(EnsureMatchingFAPIInteractionId.class),
			condition(getPaymentPollStatusCondition())
		);
	}

	protected ConditionSequence getPaymentsValidationRepeatSequence() {
		return sequenceOf(
			condition(ClearRequestObjectFromEnvironment.class),
			condition(PrepareToFetchNextPayment.class),
			condition(SetResourceMethodToGet.class),
			condition(ClearContentTypeHeaderForResourceEndpointRequest.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(AddJWTAcceptHeader.class),
			condition(CallProtectedResource.class),
			condition(EnsureResourceResponseCodeWas200.class),
			condition(EnsureMatchingFAPIInteractionId.class),
			condition(EnsurePaymentStatusWasSchd.class),
			condition(getPaymentValidator())
		);
	}

	protected void validateAllPayments() {
		repeatSequence(this::getPaymentsValidationRepeatSequence)
			.untilTrue("no_more_payments")
			.trailingPause(5)
			.times(50)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();
	}

	@Override
	protected void runRepeatSequence() {
		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(20)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();
	}

	@Override
	protected void updatePaymentConsent() {}

	protected void extractNPaymentsId() {
		callAndStopOnFailure(ExtractFiveLastPaymentIds.class);
	}
}
