package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4;

public class GetAccountTransactionsCurrentOASValidatorV2n4 extends AbstractGetTransactionsOASValidatorV2n4 {
	@Override
	protected String getEndpointPath() {
		return "/accounts/{accountId}/transactions-current";
	}
}
