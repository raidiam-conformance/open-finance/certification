package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;
import java.util.UUID;

public class EditRecurringPaymentRequestClaimsToSetDifferentProxy extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource_request_entity_claims", "data"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Unable to find data in recurring-payments request payload"));

		data.addProperty("proxy", UUID.randomUUID().toString());

		logSuccess("Updated data.proxy to contain a randomly generated proxy", args("data", data));
		return env;
	}
}
