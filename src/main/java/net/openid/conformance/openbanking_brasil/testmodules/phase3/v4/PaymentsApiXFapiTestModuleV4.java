package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsXFapiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PatchPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "payments_api_x-fapi_test-module_v4",
	displayName = "Ensure x-fapi-interaction-id is being required at payments endpoints",
	summary = "Ensure x-fapi-interaction-id is being required at payments endpoints\n" +
		"1 - Validate the Behaviour on the POST/GET Payments-Consents Endpoints\n" +
		"· Call the POST Consents endpoint without the x-fapi-interaction-id\n" +
		"· Expects 400 - Validate Error message, ensure a x-fapi-interaction-id is sent on the response\n" +
		"· Call the POST Consents endpoint with the x-fapi-interaction-id, set date to be of a scheduled payment (D+1)\n" +
		"· Expects 201 - Validate response, ensure x-fapi-interaction-id is the same one sent back\n" +
		"· Call GET Consent without the x-fapi-interaction-id\n" +
		"· Expects 400 - Validate Error message, ensure a x-fapi-interaction-id is sent on the response\n" +
		"· Call GET Consent with the x-fapi-interaction-id\n" +
		"· Expects 200 - Validate if status is \"AWAITING_AUTHORISATION\"\n" +
		"· Redirect the use to authorize the Consent'\n" +
		"2 - Validate the Behaviour on the POST/GET Pix-Payments Endpoints \n" +
		"· Call the POST Payments without the x-fapi-interaction-id\n" +
		"· Expects 400 - Validate Error message, ensure a x-fapi-interaction-id is sent on the response\n" +
		"· Call the POST Payments with the x-fapi-interaction-id\n" +
		"· Expects 201 - Validate response, ensure x-fapi-interaction-id is the same one sent back\n" +
		"· Call GET Payments without the x-fapi-interaction-id\n" +
		"· Expects 400 - Validate Error message, ensure a x-fapi-interaction-id is sent on the response\n" +
		"· Call GET Payments with the x-fapi-interaction-id\n" +
		"· Expects 200 - Validate response, ensure x-fapi-interaction-id is the same one sent back\n" +
		"3 - Validate the Behaviour on the PATCH Pix-Payments Endpoints\n" +
		"· Call the PATCH Payments without the x-fapi-interaction-id\n" +
		"· Expects 400 - Validate Error message, ensure a x-fapi-interaction-id is sent on the response\n" +
		"· Call PATCH Payments with the x-fapi-interaction-id\n" +
		"· Expects 200 - Validate response, ensure x-fapi-interaction-id is the same one sent back" ,
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiXFapiTestModuleV4 extends AbstractPaymentsXFapiTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> patchValidator() {
		return PatchPaymentsPixOASValidatorV4.class;
	}
}
