package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2ApiResourceTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetUnarrangedAccountsOverdraftV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractUnarrangedAccountsOverdraftApiResourcesTestModule extends AbstractPhase2ApiResourceTestModule {

	@Override
	protected abstract Class<? extends Condition> apiValidator();

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetUnarrangedAccountsOverdraftV2Endpoint.class),
			condition(GetConsentV3Endpoint.class)
		);
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.UNARRANGED_ACCOUNTS_OVERDRAFT;
	}

	@Override
	protected OPFCategoryEnum getProductCategory() {
		return OPFCategoryEnum.CREDIT_OPERATIONS;
	}

	@Override
	protected String apiName() {
		return EnumResourcesType.UNARRANGED_ACCOUNT_OVERDRAFT.name();
	}

	@Override
	protected String apiResourceId() {
		return "contractId";
	}

	@Override
	protected String resourceType() {
		return EnumResourcesType.UNARRANGED_ACCOUNT_OVERDRAFT.name();
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
