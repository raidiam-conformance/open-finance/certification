package net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetExchangesV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractOperationID;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractExchangesApiTestModule extends AbstractPhase2TestModule {


	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.EXCHANGES).addScopes(OPFScopesEnum.EXCHANGES, OPFScopesEnum.OPEN_ID).build();
	}

	@Override
	protected void requestProtectedResource() {
		preCallProtectedResource("GET Operations List to extract an operation ID");
		eventLog.startBlock("Validating GET Operations List response");
		call(validateGetOperationsResponse());
		eventLog.endBlock();
		executeParticularTestSteps();
	}

	protected ConditionSequence validateGetOperationsResponse() {
		return sequenceOf(
			condition(EnsureResourceResponseCodeWas200.class),
			condition(GetExchangesOperationsListV1OASValidator.class),
			condition(ExtractOperationID.class)
		);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps()
			.insertAfter(FAPIBrazilConsentEndpointResponseValidatePermissions.class,
				condition(SaveConsentsAccessToken.class));
	}

	protected abstract void executeParticularTestSteps();

	@Override
	protected void validateResponse() {}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetExchangesV1Endpoint.class)
		);
	}
}
