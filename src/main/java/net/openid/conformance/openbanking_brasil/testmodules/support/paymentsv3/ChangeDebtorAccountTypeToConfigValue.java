package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;


import net.openid.conformance.testmodule.Environment;

public class ChangeDebtorAccountTypeToConfigValue extends AbstractChangeDebtorAccountType {

	@Override
	protected String getAccountType(Environment env) {
		if (env.getString("config", "resource.debtorAccountType") != null) {
			return env.getString("config", "resource.debtorAccountType");
		} else {
			throw error("debtorAccountType missing from config");
		}
	}
}
