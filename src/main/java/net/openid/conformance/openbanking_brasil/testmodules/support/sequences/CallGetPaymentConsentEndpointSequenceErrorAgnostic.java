package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.condition.client.CheckForDateHeaderInResourceResponse;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateIdempotencyKey;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow200or406;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJWTAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class CallGetPaymentConsentEndpointSequenceErrorAgnostic extends AbstractConditionSequence {
    @Override
    public void evaluate() {
        call(exec().mapKey("request_object_claims", "consent_endpoint_request"));
        callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
        call(exec().unmapKey("request_object_claims"));

        callAndStopOnFailure(SetResourceMethodToGet.class);
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
        callAndStopOnFailure(AddJWTAcceptHeaderRequest.class);

        callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders.class, Condition.ConditionResult.FAILURE);

        call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
        callAndContinueOnFailure(OptionallyAllow200or406.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
        call(exec().unmapKey("resource_endpoint_response_full"));

        callAndContinueOnFailure(CheckForDateHeaderInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
        callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
        callAndStopOnFailure(EnsureMatchingFAPIInteractionId.class);

        call(exec().mapKey("endpoint_response", "consent_endpoint_response_full"));
        call(exec().mapKey("endpoint_response_jwt", "consent_endpoint_response_jwt"));
        call(condition(EnsureContentTypeApplicationJwt.class)
            .onFail(Condition.ConditionResult.FAILURE)
            .dontStopOnFailure()
            .skipIfStringMissing("status_ok"));

        call(condition(EnsureContentTypeJson.class)
            .onFail(Condition.ConditionResult.FAILURE)
            .dontStopOnFailure()
            .skipIfStringMissing("status_not_acceptable"));

        call(condition(ExtractSignedJwtFromResourceResponse.class)
            .onFail(Condition.ConditionResult.FAILURE)
            .dontStopOnFailure()
            .skipIfStringMissing("status_ok"));

        call(condition(FAPIBrazilValidateResourceResponseSigningAlg.class)
            .onFail(Condition.ConditionResult.FAILURE)
            .skipIfStringMissing("status_ok"));

        call(condition(FAPIBrazilValidateResourceResponseTyp.class)
            .onFail(Condition.ConditionResult.FAILURE)
            .skipIfStringMissing("status_ok"));

        call(exec().mapKey("server", "org_server"));
        call(exec().mapKey("server_jwks", "org_server_jwks"));
        callAndStopOnFailure(FetchServerKeys.class);
        call(exec().unmapKey("server"));
        call(exec().unmapKey("server_jwks"));
        call(condition(ValidateResourceResponseSignature.class)
            .onFail(Condition.ConditionResult.FAILURE)
            .dontStopOnFailure()
            .skipIfStringMissing("status_ok"));

        call(condition(ValidateResourceResponseJwtClaims.class)
            .onFail(Condition.ConditionResult.FAILURE)
            .dontStopOnFailure()
            .skipIfStringMissing("status_ok"));

        call(exec().startBlock("Validate GET response"));

        call(condition(PaymentConsentValidatorV2.class)
            .onFail(Condition.ConditionResult.FAILURE)
            .dontStopOnFailure()
            .skipIfStringMissing("status_ok"));


        call(exec().endBlock());
        call(exec().unmapKey("endpoint_response"));
        call(exec().unmapKey("endpoint_response_jwt"));
    }
}
