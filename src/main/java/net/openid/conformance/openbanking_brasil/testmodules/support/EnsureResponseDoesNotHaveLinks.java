package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class EnsureResponseDoesNotHaveLinks extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			JsonObject body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.orElseThrow(() -> error("Could not extract body from response"))
			);

			if (body.has("links")) {
				throw error("Response body has \"links\" field");
			} else {
				logSuccess("Response body does not have \"links\" field");
			}
		} catch (ParseException e) {
			throw error("Could not parse body");
		}
		return env;
	}
}
