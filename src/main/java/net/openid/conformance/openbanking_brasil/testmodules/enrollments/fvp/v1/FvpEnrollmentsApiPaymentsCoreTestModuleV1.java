package net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractFvpEnrollmentsApiPaymentsCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.GetEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostFidoRegistrationOptionsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostFidoSignOptionsOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "fvp-enrollments_api_payments-core_test-module_v1",
	displayName = "fvp-enrollments_api_payments-core_test-module_v1",
	summary = "Ensure payment reaches an accepted state when a payment without redirect is executed.\n" +
		"• GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array\n" +
		"• Execute a full enrollment journey sending the origin extracted above, extract the enrollment ID, and store the refresh_token generated ensuring it has the nrp-consents scope\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AUTHORISED\", and that dailyLimit and transactionLimit are 1 BRL\n" +
		"• Call POST consent with valid payload, and amount of 0.5 BRL\n" +
		"• Expects 201 - Validate response\n" +
		"• Call the POST sign-options endpoint with the consentID created\n" +
		"• Expect 201 - Validate response and extract challenge\n" +
		"• Calls the POST Token endpoint with the refresh_token grant, ensuring a token with the nrp-consent scope was issued\n" +
		"• Call POST consent authorize with valid payload, signing the challenge with the compliant private key\n" +
		"• Expects 204\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"• Calls the POST Payments Endpoint, using the access token with the nrp-consents, authorisationFlow as FIDO_FLOW and consentId with the corresponding ConsentID, and amount of 0.5 BRL\n" +
		"• Expects 201 - Validate Response\n" +
		"• Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"• Expects Accepted state (ACSC) - Validate Response\n" +
		"• Call the PATCH Enrollments {EnrollmentID}\n" +
		"• Expects 204\n"
	,
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
	}
)

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.enrollmentsUrl",
	"resource.consentUrl",
	"resource.resourceUrl",
})
public class FvpEnrollmentsApiPaymentsCoreTestModuleV1 extends AbstractFvpEnrollmentsApiPaymentsCoreTestModule {
	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV1.class;
	}
	@Override
	protected Class<? extends Condition> postFidoSignOptionsValidator() {
		return PostFidoSignOptionsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetPaymentConsentsV4Endpoint.class), condition(GetPaymentV4Endpoint.class), condition(GetEnrollmentsV1Endpoint.class));
	}
}
