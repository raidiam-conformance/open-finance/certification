package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractFvpAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-automatic-payments_api_sweeping-accounts-invalid-creditor_test-module_v1",
	displayName = "fvp-automatic-payments_api_sweeping-accounts-invalid-creditor_test-module_v1",
	summary = "Ensure a consent for sweeping accounts cannot be created with invalid creditor account fields\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, sending the logged user as provided on the config, and the creditor as a different one\n" +
		"• Expect 422 PARAMETRO_INVALIDO - Validate Error Message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV1 extends AbstractFvpAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetAutomaticPaymentRecurringConsentV1Endpoint.class;
	}
}
