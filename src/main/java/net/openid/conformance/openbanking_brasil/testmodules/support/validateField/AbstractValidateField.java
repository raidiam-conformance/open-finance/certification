package net.openid.conformance.openbanking_brasil.testmodules.support.validateField;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.JsonHelper;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public abstract class AbstractValidateField extends AbstractJsonAssertingCondition {

	protected static final int CPF_LENGTH = 11;
	protected static final int CNPJ_LENGTH = 14;

    @Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonObject config = env.getObject("config");
		validateMainFields(config);
		return env;
	}

	protected void validateMainFields(JsonObject config) {
		validateConsentUrl(config);
		validateBrazilCpf(config);
		validateProductType(config);
	}

	protected void validateConsentUrl(JsonObject config) {
		JsonElement consentElement = findElementOrThrowError(config, "$.resource.consentUrl");
		String consentUrl = OIDFJSON.getString(consentElement);
		String regexValidator = getConsentRegex();
		if (!consentUrl.matches(regexValidator)) {
			throw error(String.format("consentUrl does not match the regex %s", regexValidator));
		}
	}

	protected void validateBrazilCpf(JsonObject config) {
		JsonElement brazilCpfElement = findElementOrThrowError(config, "$.resource.brazilCpf");
		String brazilCpf = OIDFJSON.getString(brazilCpfElement);
		if (Strings.isNullOrEmpty(brazilCpf) || brazilCpf.length() != CPF_LENGTH) {
			throw error("brazilCpf is not valid", args("brazilCpf", brazilCpf));
		}
	}

	protected void validateProductType(JsonObject config) {
		JsonElement productTypeElement = findElementOrThrowError(config, "$.consent.productType");
		String productType = OIDFJSON.getString(productTypeElement);
		if (Strings.isNullOrEmpty(productType) || (!productType.equals("business") && !productType.equals("personal"))) {
			throw error("Product type (Business or Personal) must be specified in the test configuration");
		}

		if (productType.equals("business")) {
			JsonElement brazilCnpjElement = findElementOrThrowError(config, "$.resource.brazilCnpj");
			String brazilCnpj = OIDFJSON.getString(brazilCnpjElement);
			if(Strings.isNullOrEmpty(brazilCnpj) || brazilCnpj.length() != CNPJ_LENGTH) {
				throw error("brazilCnpj is not valid", args("brazilCnpj", brazilCnpj));
			}
		}
	}

	protected abstract int getConsentVersion();

	protected String getConsentRegex() {
		return String.format("^(https://)(.*?)(consents/v%d/consents)", getConsentVersion());
	}

	protected JsonElement findElementOrThrowError(JsonElement rootElement, String path) {
		if (!JsonHelper.ifExists(rootElement, path)) {
			throw error(String.format("Element with path %s was not found.", path));
		}
		return findByPath(rootElement, path);
	}
}
