package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1;

import org.springframework.http.HttpMethod;

public class PostFidoRegistrationOASValidatorV1 extends AbstractEnrollmentsOASValidatorV1 {

	@Override
	protected String getEndpointPathSuffix() {
		return "/{enrollmentId}/fido-registration";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}
}
