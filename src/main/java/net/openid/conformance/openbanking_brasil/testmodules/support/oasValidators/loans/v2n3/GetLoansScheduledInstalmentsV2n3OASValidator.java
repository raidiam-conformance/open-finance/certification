package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.AbstractGetLoansScheduledInstalmentsOASValidator;


public class GetLoansScheduledInstalmentsV2n3OASValidator extends AbstractGetLoansScheduledInstalmentsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/loans/loans-v2.3.0.yml";
	}
}
