package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetUnarrangedAccountsOverdraftV2Endpoint extends AbstractGetXFromAuthServer {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/unarranged-accounts-overdraft\\/v\\d+\\/contracts)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "unarranged-accounts-overdraft";
	}

	@Override
	protected String getApiVersionRegex() {
		return "^(2.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
