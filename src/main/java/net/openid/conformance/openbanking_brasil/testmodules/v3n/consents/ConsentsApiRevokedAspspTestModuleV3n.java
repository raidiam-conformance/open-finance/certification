package net.openid.conformance.openbanking_brasil.testmodules.v3n.consents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractConsentsApiRevokedAspspTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_revoked-aspsp_test-module_v3-2",
	displayName = "Validate that when a consent is manually revoked the rejected by is 'CUSTOMER_MANUALLY_REVOKED'",
	summary = "Makes sure that the user can revoke its created consent inside the ASPSP.\n" +
		"\u2022 For this test, the Tester will have to manually revoke the created consent on the ASPSP platform while the server is set to poll the consent API. \n" +
		"\u2022 Creates a Consent with all of the existing permissions\n" +
		"\u2022 Expects a success - 201\n" +
		"\u2022 Redirect the user to authorize the consent\n" +
		"\u2022 Call the GET Consents API\n" +
		"\u2022 POLL the GET Consents API for 10 minutes, one call every 60 seconds.\n" +
		"\u2022 Continue Polling until the Consent Status reaches status REJECTED. Make Sure RejectedBy is set to USER. Make sure Reason is set to \"CUSTOMER_MANUALLY_REVOKED”\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ConsentsApiRevokedAspspTestModuleV3n extends AbstractConsentsApiRevokedAspspTestModule {


	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return GetConsentOASValidatorV3n2.class;
	}
}
