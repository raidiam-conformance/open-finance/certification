package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiMultipleConsentsTimezoneConditionalTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringPixOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_multiple-consents-timezone-conditional_test-module_v4",
	displayName = PlanNames.PAYMENTS_API_TIMEZONE_PHASE_3_V4_TEST_PLAN,
	summary = "This test module should be executed only by institutions that currently support consents with multiple accounts, in case the institution does not support this feature the ‘Payment consent - Logged User CPF - Multiple Consents Test' and 'Payment consent - Business Entity CNPJ - Multiple Consents Test’  should be left empty, which in turn will make the test return a SKIPPED\n" +
		"\n" +
		"This test module will evaluate if the consent for multiple consents is rejected when not approved by the end of the day.\n" +
		"\u2022 Validates if the user has provided the field \"Payment consent - Logged User CPF - Multiple Consents Test\". If field is not provided the whole test scenario must be SKIPPED\n" +
		"\u2022 Set the payload to be customer business if Payment consent - Business Entity CNPJ - Multiple Consents Test was provided. If left blank, it should be customer personal\n" +
		"\u2022 Creates consent request_body with valid email proxy (cliente-a00001@pix.bcb.gov.br) and its standardized payload, set the 'Payment consent - Logged User CPF - Multiple Consents Test' on the loggedUser identification field, Debtor account should not be sent\n" +
		"\u2022 Call the POST Consents API, sending the schedule.single.date as D+1\n" +
		"\u2022 Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"\u2022 Redirects the user to authorize the created consent - Expect Successful Authorization\n" +
		"\u2022 Calls the GET Consents Endpoint\n" +
		"\u2022 Expects 200 - Check the status is “PARTIALLY_ACCEPTED” and validate the response\n" +
		"\u2022 Check if localTime is between 23h30 and 23h59, if not the test should fail\n" +
		"\u2022 Set the conformance suite to sleep until 00:00\n" +
		"\u2022 Calls the GET Consents Endpoint\n" +
		"\u2022 Expects 200 - Validate response - Check if Status is REJECTED and rejectionReason is TEMPO_EXPIRADO_AUTORIZACAO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"conditionalResources.brazilCpfJointAccount",
		"conditionalResources.brazilCnpjJointAccount",
	}
)
public class PaymentsApiMultipleConsentsTimezoneConditionalTestModuleV4 extends AbstractPaymentsApiMultipleConsentsTimezoneConditionalTestModuleV4 {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

		@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

}

