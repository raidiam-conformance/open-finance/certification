package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Map;
import java.util.Random;

public class InsertBrazilPaymentConsentProdValues extends AbstractCondition {
	@Override
	public Environment evaluate(Environment env) {

		String businessEntityIdentification = env.getString("config", "resource.brazilCnpj");


		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		Random random = new Random();
		float leftLimit = 0.01F;
		float rightLimit = 1F;
		double generatedFloat = leftLimit + random.nextFloat() * (rightLimit - leftLimit);

		JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
			.addFields("data.loggedUser.document", Map.of(
				"identification", DictHomologKeys.PROXY_PRODUCTION_CPF,
				"rel", "CPF")
			)
			.addFields("data.creditor",
				Map.of(
					"personType", DictHomologKeys.PROXY_PRODUCTION_PERSON_TYPE,
					"cpfCnpj", DictHomologKeys.PROXY_PRODUCTION_CPF,
					"name", DictHomologKeys.PROXY_PRODUCTION_NAME)
			)
			.addFields("data.payment", Map.of(
				"type", "PIX",
				"currency", DictHomologKeys.PROXY_EMAIL_STANDARD_CURRENCY,
				"amount", String.format("%.2f", generatedFloat),
				"date", currentDate.toString())
			)
			.addFields("data.payment.details", Map.of(
				"localInstrument", DictHomologKeys.PROXY_EMAIL_STANDARD_LOCALINSTRUMENT,
				"proxy", DictHomologKeys.PROXY_PRODUCTION_PROXY)
			)
			.addFields("data.payment.details.creditorAccount", Map.of(
				"ispb", DictHomologKeys.PROXY_PRODUCTION_ISPB,
				"issuer", DictHomologKeys.PROXY_PRODUCTION_BRANCH_NUMBER,
				"number", DictHomologKeys.PROXY_PRODUCTION_ACCOUNT_NUMBER,
				"accountType", DictHomologKeys.PROXY_PRODUCTION_ACCOUNT_TYPE)
			);

		if (businessEntityIdentification != null){
			jsonObjectBuilder
				.addFields("data.businessEntity.document", Map.of(
					"identification", businessEntityIdentification,
					"rel", "CNPJ"
				));
		}



		JsonObject consentRequest = jsonObjectBuilder.build();

		env.putObject("config", "resource.brazilPaymentConsent", consentRequest);

		logSuccess("Consent request body was added to the config", args("consentRequest", consentRequest));

		return env;
	}
}
