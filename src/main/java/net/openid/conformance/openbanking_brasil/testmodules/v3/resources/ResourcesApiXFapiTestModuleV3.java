package net.openid.conformance.openbanking_brasil.testmodules.v3.resources;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.v3.AbstractCustomerDataXFapiTestModuleV3;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "resources_api_x-fapi_test-module_v3",
	displayName = "Validate the structure of all consent API resources V3",
	summary = "Test will ensure that the x-fapi-interaction-id is required at the request\n" +
		"\u2022 Call the POST Consents endpoint with the Customers API Permission Group\n" +
		"\u2022 Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION\n" +
		"\u2022 Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"\u2022 Call the GET Consents endpoint\n" +
		"\u2022 Expects 200 - Validate response and ensure status is AUTHORISED\n" +
		"\u2022 Call the GET Resources Endpoint without the x-fapi-interaction-id\n" +
		"\u2022 Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"\u2022 Call the GET Resources Endpoint with an invalid x-fapi-interaction-id\n" +
		"\u2022 Expects 400 - Validate error message\n" +
		"\u2022 Call the GET Resources Endpoint with the x-fapi-interaction-id\n" +
		"\u2022 Expects 200 - Validate response\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl", "consent.productType"
})
public class ResourcesApiXFapiTestModuleV3 extends AbstractCustomerDataXFapiTestModuleV3 {


	@Override
	protected boolean isConsents() {
		return false;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class),condition(GetResourcesV3Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> getResourceValidator() {return GetResourcesOASValidatorV3.class;}

	@Override
	protected OPFCategoryEnum getCategory() {
		String productType = env.getString("config", "consent.productType");

		if (productType.equals("business")) {
			return OPFCategoryEnum.BUSINESS_REGISTRATION_DATA;
		} else {
			return OPFCategoryEnum.PERSONAL_REGISTRATION_DATA;
		}
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.RESOURCES;
	}

}
