package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.validateVersionHeader;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public abstract class AbstractValidateVersionHeader extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_headers")
	public Environment evaluate(Environment env) {
		JsonObject headers = env.getObject("resource_endpoint_response_headers");
		String versionHeader = Optional.ofNullable(headers.get("x-v"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find \"x-v\" header in the response",
				args("headers", headers)));

		if (!versionHeader.equals(getExpectedVersion())) {
			throw error("The value of the \"x-v\" header does not match the expected version",
				args("x-v", versionHeader, "expected", getExpectedVersion()));
		}
		logSuccess("Successfully validated the \"x-v\" header");

		return env;
	}

	protected abstract String getExpectedVersion();
}
