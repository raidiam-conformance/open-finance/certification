package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class EditRecurringPaymentsBodyToSetDateToTodayUtcMinus3 extends AbstractEditRecurringPaymentsBodyToSetDate {

	@Override
	protected String getDate() {
		LocalDate futureDate = LocalDate.now(ZoneOffset.ofHours(-3));
		return futureDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
}
