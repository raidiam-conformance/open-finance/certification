package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1;

import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetTreasureTitleIdentificationV1OASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/treasureTitles/treasure-titles-v1.0.0.yml";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}";
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertData(data);
		assertRenumeration(data);
	}

	private void assertData(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"voucherPaymentPeriodicity",
			"voucherPaymentIndicator",
			"SIM"
		);

		if (isPresent(data, "voucherPaymentPeriodicity")) {
			assertField1IsRequiredWhenField2HasValue2(
				data,
				"voucherPaymentPeriodicityAdditionalInfo",
				"voucherPaymentPeriodicity",
				"OUTROS"
			);
		}


	}

	private void assertRenumeration(JsonObject data) {
		JsonObject remuneration = JsonPath.read(data, "$.remuneration");

		assertField1IsRequiredWhenField2HasValue2(
			remuneration,
			"preFixedRate",
			"indexer",
			"PRE_FIXADO"
		);

		assertField1IsRequiredWhenField2HasValue2(
			remuneration,
			"postFixedIndexerPercentage",
			"indexer",
			"PRE_FIXADO"
		);

		assertField1IsRequiredWhenField2HasValue2(
			remuneration,
			"indexerAdditionalInfo",
			"indexer",
			"OUTROS"
		);

	}


}
