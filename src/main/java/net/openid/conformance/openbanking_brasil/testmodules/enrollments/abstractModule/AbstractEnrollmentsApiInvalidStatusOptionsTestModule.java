package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoRegistrationOptionsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasAwaitingAccountHolderValidation;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoRegistrationOptions;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;

public abstract class AbstractEnrollmentsApiInvalidStatusOptionsTestModule extends AbstractEnrollmentsApiTestModule {

	@Override
	protected void performAuthorizationFlow() {
		performPreAuthorizationSteps();
		executeTestSteps();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected void executeTestSteps() {
		runInBlock("Call POST fido-registration-options with client_credentials token - Expects 401", () -> {
			PostEnrollmentsResourceSteps fidoRegistrationOptionsSteps = new PostEnrollmentsResourceSteps(
				new PrepareToPostFidoRegistrationOptions(),
				CreateEnrollmentsFidoRegistrationOptionsRequestBody.class,
				true
			);
			call(fidoRegistrationOptionsSteps);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas401.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postFidoRegistrationOptionsValidator(), Condition.ConditionResult.FAILURE);
		});

		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasAwaitingAccountHolderValidation(), "AWAITING_ACCOUNT_HOLDER_VALIDATION");
	}

	protected abstract Class<? extends Condition> postFidoRegistrationOptionsValidator();

}
