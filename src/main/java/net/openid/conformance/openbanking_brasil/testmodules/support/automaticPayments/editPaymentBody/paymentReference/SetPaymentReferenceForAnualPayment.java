package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

public class SetPaymentReferenceForAnualPayment extends AbstractSetPaymentReference {

	@Override
	protected String getPaymentReference(String dateStr) {
		LocalDate date = getPaymentDate(dateStr);
		int year = date.get(ChronoField.YEAR);

		return String.format("Y%d", year);
	}
}
