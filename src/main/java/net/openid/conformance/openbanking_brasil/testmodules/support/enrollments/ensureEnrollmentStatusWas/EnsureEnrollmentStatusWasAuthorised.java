package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentStatusEnum;

public class EnsureEnrollmentStatusWasAuthorised extends AbstractEnsureEnrollmentStatusWas {

    @Override
    protected String getExpectedStatus() {
        return EnrollmentStatusEnum.AUTHORISED.toString();
    }
}
