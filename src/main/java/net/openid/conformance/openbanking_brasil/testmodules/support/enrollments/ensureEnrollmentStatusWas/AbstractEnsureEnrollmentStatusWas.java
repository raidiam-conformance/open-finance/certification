package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public abstract class AbstractEnsureEnrollmentStatusWas extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    public Environment evaluate(Environment env) {
        String expectedStatus = getExpectedStatus();
        try {
            JsonObject body = OIDFJSON.toObject(
                BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
                    .orElseThrow(() -> error("Could not extract body from response"))
            );
            String status = OIDFJSON.getString((
                Optional.ofNullable(body.getAsJsonObject("data").get("status"))
                    .orElseThrow(() -> error("Body does not have data.status field", args("body", body)))
            ));
            if (!status.equals(expectedStatus)) {
                throw error("Enrollment not in the expected state", args("expected", expectedStatus, "status", status));
            }
            logSuccess("Enrollment was in the expected state after redirect",
                args("expected", expectedStatus, "status", status));
        } catch (ParseException e) {
            throw error("Could not parse the body");
        }
        return env;
    }

    protected abstract String getExpectedStatus();
}
