package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiQRDNWithQRESCodeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_qrdn-with-qres-code_test-module_v4",
	displayName = "Payments Consents API test module for QRDN local instrument with a QRES code",
	summary = "Ensure that payment fails when the localInstrument is QRDN but the qrcode is a QRES  (Reference Error 2.2.2.8)\n" +
		"• Call POST Consent with localInstrument as QRDN but the “qrcode” field with a valid static QRcode\n" +
		"• Expects 422 - DETALHE_PAGAMENTO_INVALIDO\n" +
		"• Validate Error message\n" +
		"If no errors are identified:\n" +
		"• Expects 201 - Validate Response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Ensure an error is returned and that the authorization code was not sent back\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if Status is REJECTED and rejectionReason is QRCODE_INVALIDO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiQRDNWithQRESCodeTestModuleV4 extends AbstractPaymentsApiQRDNWithQRESCodeTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}
}
