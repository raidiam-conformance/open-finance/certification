package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3.GetCreditFixedIncomesBalancesV1n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3.GetCreditFixedIncomesIdentificationV1n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3.GetCreditFixedIncomesListV1n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3.GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3.GetCreditFixedIncomesTransactionsV1n3OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "credit-fixed-incomes_api_x-fapi_test-module_v1",
	displayName = "credit-fixed-incomes_api_x-fapi_test-module_v1",
	summary = "Ensure that the x-fapi-interaction-id is required at the request for all endpoints\n" +
		"• Call the POST Consents endpoint with the Investments Permission Group\n" +
		"• Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION\n" +
		"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"• Call the GET Consents endpoint\n" +
		"• Expects 200 - Validate response and ensure status is AUTHORISED\n" +
		"• Call the GET Investments Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Investments Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Investments Endpoint with the x-fapi-interaction-id\n" +
		"• Expects 200 - Validate response and extract InvestmentId\n" +
		"• Call the GET Investments/{InvestmentId} Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Investments/{InvestmentId} Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET balances Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET balances Endpoint with invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction current Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction current Endpoint with invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class CreditFixedIncomesApiXFapiTestModuleV1n extends AbstractCreditFixedIncomesApiXFapiTest {

	@Override
	protected Class<? extends Condition> investmentsRootValidator() {
		return GetCreditFixedIncomesListV1n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsIdentificationValidator() {
		return GetCreditFixedIncomesIdentificationV1n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsBalancesValidator() {
		return GetCreditFixedIncomesBalancesV1n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsTransactionsValidator() {
		return GetCreditFixedIncomesTransactionsV1n3OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsTransactionsCurrentValidator() {
		return GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator.class;
	}

}
