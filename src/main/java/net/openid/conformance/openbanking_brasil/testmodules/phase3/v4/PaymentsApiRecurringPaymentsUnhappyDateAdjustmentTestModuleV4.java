package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiRecurringPaymentsUnhappyDateAdjustmentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_recurring-payments-unhappy-date-adjustment_test-module_v4",
	displayName = "payments_api_recurring-payments-unhappy-date-adjustment_test-module_v4",
	summary = "Ensure that consent for monthly recurring payments can be carried out and that the payment is not successfully scheduled when its due date is the 31st, and the dates are not adjusted accordingly\n" +
		"• Call the POST Consents endpoints with the schedule.monthly.dayOfMonth field set as 31, schedule.monthly.startDate field set as D+1, and schedule.monthly.quantity as 5\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"• Calls the POST Payments Endpoint with the 5 Payments, using the appropriate endToEndId for each of them, ensuring the day for the first payment is the next day 31 and not the startDate. When dealing with months that do not have a 31st, send the date as the previous day.\n" +
		"• Expects 422 - PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"CONSUMED\" and validate response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiRecurringPaymentsUnhappyDateAdjustmentTestModuleV4 extends AbstractPaymentsApiRecurringPaymentsUnhappyDateAdjustmentTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}
}
