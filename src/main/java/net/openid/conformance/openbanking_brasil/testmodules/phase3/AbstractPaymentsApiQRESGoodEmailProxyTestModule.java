package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRESCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectQRCodeWithRealEmailIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRESCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;
import net.openid.conformance.sequence.ConditionSequence;

import java.util.Optional;

public abstract class AbstractPaymentsApiQRESGoodEmailProxyTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

    protected abstract Class<? extends Condition> paymentConsentErrorValidator();

    @Override
    protected void configureDictInfo() {
        callAndStopOnFailure(SelectQRESCodeLocalInstrument.class);
        callAndStopOnFailure(SelectQRESCodePixLocalInstrument.class);
        callAndStopOnFailure(InsertRandom1333XXAmountIntoPaymentsAndPaymentsConsentsResources.class);
        callAndStopOnFailure(InjectQRCodeWithRealEmailIntoConfig.class);
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
			.insertAfter(OptionallyAllow201Or422.class,
                condition(paymentConsentErrorValidator())
                    .dontStopOnFailure()
                    .onFail(Condition.ConditionResult.FAILURE)
                    .skipIfStringMissing("validate_errors")
            )
            .insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
                sequenceOf(condition(CreateRandomFAPIInteractionId.class),
                    condition(AddFAPIInteractionIdToResourceEndpointRequest.class)))
            .insertAfter(CheckForFAPIInteractionIdInResourceResponse.class,
                condition(EnsureMatchingFAPIInteractionId.class)
            );
    }

    @Override
    protected void performPreAuthorizationSteps() {
        call(createOBBPreauthSteps());
        callAndStopOnFailure(SaveAccessToken.class);
        callAndStopOnFailure(CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida.class, Condition.ConditionResult.FAILURE);
        if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
            fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
        }
        eventLog.startBlock(currentClientString() + "Validate consents response");
        callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }
}
