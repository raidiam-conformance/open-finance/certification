package net.openid.conformance.openbanking_brasil.testmodules.support;

public class EnsureTransactionLimitSetTo1BRL extends AbstractEnsureEnrollmentVariableSetToValue {

	@Override
	protected String fieldName() {
		return "transactionLimit";
	}

	@Override
	protected String envVarName() {
		return "transaction_limit";
	}

	@Override
	protected String value() {
		return "1.00";
	}
}
