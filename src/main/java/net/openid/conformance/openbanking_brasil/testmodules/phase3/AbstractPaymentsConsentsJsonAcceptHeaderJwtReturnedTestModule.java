package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseWasJwt;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200or406;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.SignedPaymentConsentSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.sequence.ConditionSequence;
import org.springframework.http.HttpStatus;

public abstract class AbstractPaymentsConsentsJsonAcceptHeaderJwtReturnedTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	protected abstract Class<? extends Condition> postPaymentConsentValidator();
	protected abstract Class<? extends Condition> getPaymentConsentValidator();
	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
	}

	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void postConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndContinueOnFailure(SanitiseQrCodeConfig.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void runTests() {
		runInBlock("Create a payment consent", () -> {
			eventLog.startBlock("Setting date to today");
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

			call(sequence(SignedPaymentConsentSequence.class));

			callAndStopOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);

			ConditionSequence validationSequence = new ValidateSelfEndpoint()
				.replace(EnsureResourceResponseCodeWas200.class, condition(EnsureResourceResponseCodeWas200or406.class))
				.skip(SaveOldValues.class, "Skipped in the sequence for further check")
				.skip(LoadOldValues.class, "Skipped in the sequence for further check");

			call(validationSequence);
			if(env.getInteger("resource_endpoint_response_full", "status") == HttpStatus.OK.value()) {

				callAndStopOnFailure(EnsureResponseWasJwt.class);
				env.mapKey("consent_endpoint_response_full","resource_endpoint_response_full");
				callAndStopOnFailure(getPaymentConsentValidator());
			} else {
				callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
			}

		});
	}
}
