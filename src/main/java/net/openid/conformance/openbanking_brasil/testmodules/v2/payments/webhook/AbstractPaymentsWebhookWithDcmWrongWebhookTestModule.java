package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.CheckErrorFromDynamicRegistrationEndpointIsInvalidWebhook;
import net.openid.conformance.condition.client.CheckNoErrorFromDynamicRegistrationEndpoint;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs400;
import net.openid.conformance.condition.client.ExtractDynamicRegistrationResponse;
import net.openid.conformance.condition.client.VerifyClientManagementCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddScopeToClientConfigurationFromConsentUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.CallDynamicRegistrationEndpointAndVerifySuccessfulResponse;

import java.time.Instant;

public abstract class AbstractPaymentsWebhookWithDcmWrongWebhookTestModule extends AbstractBrazilDCRPaymentsWebhook {
	@Override
	protected void configureClient() {
		env.putString("test_start_timestamp", Instant.now().toString());
		call(new ValidateWellKnownUriSteps());
		getPaymentAndPaymentConsentEndpoints();
		callAndStopOnFailure(AddScopeToClientConfigurationFromConsentUrl.class);
		doDcr();
		eventLog.endBlock();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {}

	@Override
	protected void addWebhookUrisToDynamicRegistrationRequest() {
		callAndStopOnFailure(AddWrongWebhookUriToclientConfigRequest.class);
	}

	@Override
	protected void setupResourceEndpoint() {
	}

	@Override
	protected void performAuthorizationFlow() {
		fireTestFinished();
	}

	@Override
	protected void callRegistrationEndpoint() {
		ConditionSequence conditionSequence = new CallDynamicRegistrationEndpointAndVerifySuccessfulResponse();
		conditionSequence.replace(EnsureHttpStatusCodeIs201.class, condition(EnsureHttpStatusCodeIs400.class));
		conditionSequence.insertAfter(EnsureHttpStatusCodeIs201.class, condition(CheckErrorFromDynamicRegistrationEndpointIsInvalidWebhook.class));
		conditionSequence.skip(CheckNoErrorFromDynamicRegistrationEndpoint.class,"Expecting Error in DCR response");
		conditionSequence.skip(ExtractDynamicRegistrationResponse.class,"Expecting Error in DCR response");
		conditionSequence.skip(VerifyClientManagementCredentials.class,"Expecting Error in DCR response");
		call(conditionSequence);

		eventLog.endBlock();
	}
}
