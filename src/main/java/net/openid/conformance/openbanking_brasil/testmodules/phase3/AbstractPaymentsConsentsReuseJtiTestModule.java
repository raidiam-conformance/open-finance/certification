package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddJtiAsUuidToRequestObject;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.EnsureResourceResponseReturnedJsonContentType;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.SetApplicationJwtAcceptHeaderForResourceEndpointRequest;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.SignedPaymentConsentSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPaymentsConsentsReuseJtiTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	protected abstract Class<? extends Condition> postPaymentConsentValidator();
	protected abstract Class<? extends Condition> getPaymentConsentValidator();
	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
	}

	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void postConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndContinueOnFailure(SanitiseQrCodeConfig.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void runTests() {
		runInBlock("Create a payment consent", () -> {
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

			call(sequence(SignedPaymentConsentSequence.class));

			callAndStopOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);

			callAndContinueOnFailure(SetApplicationJwtAcceptHeaderForResourceEndpointRequest.class, Condition.ConditionResult.FAILURE);

			call(new ValidateSelfEndpoint()
				.skip(SaveOldValues.class, "Not saving old values")
				.skip(LoadOldValues.class, "Not loading old values")
			);

			env.mapKey("consent_endpoint_response_full", "resource_endpoint_response_full");

			callAndContinueOnFailure(getPaymentConsentValidator(), Condition.ConditionResult.FAILURE);

			env.unmapKey("consent_endpoint_response_full");

		});

		runInBlock("Create a payment consent re-using jti", () -> {
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

			call(new SignedPaymentConsentSequence()
				.skip(AddJtiAsUuidToRequestObject.class, "Re-use previous jti")
				.replace(EnsureContentTypeApplicationJwt.class, condition(EnsureResourceResponseReturnedJsonContentType.class))
				.replace(EnsureHttpStatusCodeIs201.class, condition(EnsureConsentResponseCodeWas403.class))
				.insertAfter(EnsureHttpStatusCodeIs201.class,
					condition(paymentConsentErrorValidator())
				)
				.skip(ExtractSignedJwtFromResourceResponse.class, "403 Response JSON")
				.skip(FAPIBrazilValidateResourceResponseSigningAlg.class, "403 Response JSON")
				.skip(FAPIBrazilValidateResourceResponseTyp.class, "403 Response JSON")
				.skip(ValidateResourceResponseSignature.class, "403 Response JSON")
				.skip(ValidateResourceResponseJwtClaims.class, "403 Response JSON")
			);


		});


	}

}
