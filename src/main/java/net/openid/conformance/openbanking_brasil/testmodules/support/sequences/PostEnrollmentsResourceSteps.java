package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIatToRequestObject;
import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.condition.client.AddIssAsCertificateOuToRequestObject;
import net.openid.conformance.condition.client.AddJtiAsUuidToRequestObject;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateIdempotencyKey;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilExtractClientMTLSCertificateSubject;
import net.openid.conformance.condition.client.FAPIBrazilGetKeystoreJwksUri;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateOrganizationJWKsPrivatePart;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.AddAudAsEnrollmentsUrlToRequestObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.CallEnrollmentsEndpointWithBearerToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.AbstractPrepareToMakeACallToEnrollmentsEndpoint;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class PostEnrollmentsResourceSteps extends AbstractConditionSequence {

	private final AbstractPrepareToMakeACallToEnrollmentsEndpoint urlPreparingCondition;
	private final Class<? extends Condition> requestBodyCreatingCondition;
	private final boolean stopAfterResourceEndpointCall;


	public PostEnrollmentsResourceSteps(AbstractPrepareToMakeACallToEnrollmentsEndpoint urlPreparingCondition,
										Class<? extends Condition> requestBodyCreatingCondition,
										boolean stopAfterResourceEndpointCall) {
		this.urlPreparingCondition = urlPreparingCondition;
		this.requestBodyCreatingCondition = requestBodyCreatingCondition;
		this.stopAfterResourceEndpointCall = stopAfterResourceEndpointCall;
	}

	public PostEnrollmentsResourceSteps(AbstractPrepareToMakeACallToEnrollmentsEndpoint urlPreparingCondition,
										Class<? extends Condition> requestBodyCreatingCondition) {
		this.urlPreparingCondition = urlPreparingCondition;
		this.requestBodyCreatingCondition = requestBodyCreatingCondition;
		this.stopAfterResourceEndpointCall = false;
	}

	@Override
	public void evaluate() {
		// Create request payload
		callAndStopOnFailure(urlPreparingCondition.getClass());
		callAndStopOnFailure(requestBodyCreatingCondition);
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
		callAndStopOnFailure(CreateIdempotencyKey.class);
		callAndStopOnFailure(AddIdempotencyKeyHeader.class);
		callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
		callAndStopOnFailure(AddAudAsEnrollmentsUrlToRequestObject.class);
		call(exec().mapKey("request_object_claims", "resource_request_entity_claims"));
		callAndStopOnFailure(AddIssAsCertificateOuToRequestObject.class, "BrazilOB-6.1");
		callAndStopOnFailure(AddJtiAsUuidToRequestObject.class, "BrazilOB-6.1");
		callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");
		call(exec().unmapKey("request_object_claims"));
		callAndStopOnFailure(ValidateOrganizationJWKsPrivatePart.class);

		// Sign request payload and call endpoint
		callAndStopOnFailure(SignEnrollmentsRequest.class);
		callAndStopOnFailure(CallEnrollmentsEndpointWithBearerToken.class);

		// Optional stopping point if we don't want to validate the response
		if (stopAfterResourceEndpointCall) {
			return;
		}

		// Validate response
		call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));
		call(exec().mapKey("endpoint_response_jwt", "consent_endpoint_response_jwt"));
		callAndContinueOnFailure(EnsureContentTypeApplicationJwt.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
		callAndContinueOnFailure(EnsureHttpStatusCodeIs201.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(ExtractSignedJwtFromResourceResponse.class, "BrazilOB-6.1");
		callAndContinueOnFailure(FAPIBrazilValidateResourceResponseSigningAlg.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
		callAndContinueOnFailure(FAPIBrazilValidateResourceResponseTyp.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
		callAndStopOnFailure(FAPIBrazilGetKeystoreJwksUri.class, Condition.ConditionResult.FAILURE);
		call(exec().mapKey("server", "org_server"));
		call(exec().mapKey("server_jwks", "org_server_jwks"));
		callAndStopOnFailure(FetchServerKeys.class);
		call(exec().unmapKey("server"));
		call(exec().unmapKey("server_jwks"));
		callAndContinueOnFailure(ValidateResourceResponseSignature.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
		callAndContinueOnFailure(ValidateResourceResponseJwtClaims.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
		call(exec().unmapKey("endpoint_response"));
		call(exec().unmapKey("endpoint_response_jwt"));
		callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI-R-6.2.1-11", "FAPI1-BASE-6.2.1-11");
		call(exec().endBlock());
	}
}
