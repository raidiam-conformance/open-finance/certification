package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import net.openid.conformance.condition.Condition;

public abstract class AbstractAutomaticPaymentsConsentsFunctionalTestModule extends AbstractAutomaticPaymentsFunctionalTestModule {

	@Override
	protected void requestProtectedResource() {}

	@Override
	protected void validateResponse() {}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return null;
	}
}
