package net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractFvpEnrollmentsApiPaymentsUnmatchingFieldsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.GetEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostFidoRegistrationOptionsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostFidoSignOptionsOASValidatorV2n1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-enrollments_api_payments-unmatching-fields_test-module_v2-1",
	displayName = "fvp-enrollments_api_payments-unmatching-fields_test-module_v2-1",
	summary = "Ensure Error when fields for the Fido Flow are not sent at the POST PIX Payments Endpoints\n" +
		"• GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array\n" +
		"• Execute a full enrollment journey sending the origin extracted above, extract the enrollment ID, and store the refresh_token generated ensuring it has the nrp-consents scope\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AUTHORISED\", and that dailyLimit and transactionLimit are 1 BRL\n\n" +
		"1. Ensure payment is failed when an unmatching authorisationFlow field is sent\n" +
		"• Call POST consent with valid payload, and amount of 0.5 BRL\n" +
		"• Expects 201 - Validate response\n" +
		"• Call the POST sign-options endpoint with the consentID created\n" +
		"• Expect 201 - Validate response and extract challenge\n" +
		"• Call POST consent authorize with valid payload, signing the challenge with the compliant private key\n" +
		"• Expects 201 - Validate response\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"• Calls the POST Token endpoint with the refresh_token grant, ensuring a token with the nrp-consent scope was issued\n" +
		"• Calls the POST Payments Endpoint, using the access token with the nrp-consents, authorisationFlow as HYBRID_FLOW and consentId with the corresponding ConsentID, and amount of 0.5 BRL\n" +
		"• Expects 201 or 422 PARAMETRO_INVALIDO - Validate Error Response\n" +
		"If response is 201:\n" +
		"• Call the GET Payments Endpoint\n" +
		"• Expects 200 with a RJCT status, and rejectionReason as DETALHE_PAGAMENTO_INVALIDO\n\n" +
		"2. Ensure payment is failed when a consentID is not sent at the POST Pix Payments Endpoint\n" +
		"• Call POST consent with valid payload, and amount of 0.5 BRL\n" +
		"• Expects 201 - Validate response\n" +
		"• Call the POST sign-options endpoint with the consentID created\n" +
		"• Expect 201 - Validate response and extract challenge\n" +
		"• Call POST consent authorize with valid payload, signing the challenge with the compliant private key\n" +
		"• Expects 201 - Validate response\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"• Calls the POST Token endpoint with the refresh_token grant, ensuring a token with the nrp-consent scope was issued\n" +
		"• Calls the POST Payments Endpoint, using the access token with the nrp-consents, authorisationFlow as FIDO_FLOW and without sending the consentId field, and amount of 0.5 BRL\n" +
		"• Expects 201 or 422 PARAMETRO_NAO_INFORMADO - Validate Error Response\n" +
		"If response is 201:\n" +
		"• Call the GET Payments Endpoint\n" +
		"• Expects 200 with a RJCT status, and rejectionReason as DETALHE_PAGAMENTO_INVALIDO\n\n" +
		"• Call the PATCH Enrollments {EnrollmentID}\n" +
		"• Expects 204\n"
	,
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class FvpEnrollmentsApiPaymentsUnmatchingFieldsTestModuleV2 extends AbstractFvpEnrollmentsApiPaymentsUnmatchingFieldsTestModule {

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> postFidoSignOptionsValidator() {
		return PostFidoSignOptionsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetPaymentConsentsV4Endpoint.class), condition(GetPaymentV4Endpoint.class), condition(GetEnrollmentsV2Endpoint.class));
	}
}
