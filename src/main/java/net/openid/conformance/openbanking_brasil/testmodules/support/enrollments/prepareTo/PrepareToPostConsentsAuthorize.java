package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo;

import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpMethod;

import java.util.Optional;

public class PrepareToPostConsentsAuthorize extends AbstractPrepareToMakeACallToEnrollmentsEndpoint {

	@Override
	protected HttpMethod httpMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected String endpoint() {
		return "authorise";
	}

	@Override
	protected boolean expectsSelfLink() {
		return false;
	}

	@Override
	protected String getEnrollmentsUrl(Environment env) {
		return super.getEnrollmentsUrl(env).replaceFirst("enrollments$", "consents");
	}

	@Override
	protected String getEnrollmentId(Environment env) {
		return Optional.ofNullable(env.getString("consent_id"))
			.orElseThrow(() -> error("consentId is missing from environment"));
	}
}
