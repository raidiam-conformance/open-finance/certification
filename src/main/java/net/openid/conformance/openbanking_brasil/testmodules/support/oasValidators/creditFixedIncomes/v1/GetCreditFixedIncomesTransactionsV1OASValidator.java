package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1;

public class GetCreditFixedIncomesTransactionsV1OASValidator extends AbstractCreditFixedIncomesTransactionsOASValidatorV1 {

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}/transactions";
	}

}
