package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractFvpPaymentsApiRecurringPaymentsWrongCustomQuantityTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments_api_recurring-payments-wrong-custom-quantity_test-module_v4",
	displayName = "fvp-payments_api_recurring-payments-wrong-custom-quantity_test-module_v4",
	summary = "Ensure that consent for custom recurring payments can't be carried out if the number of payments is lower than permitted\n" +
		"• Create a client by performing a DCR against the provided server - Expect Success\n" +
		"• Call the POST Consents endpoints with 1 item at schedule.custom.dates field set as D+1\n" +
		"• Expects 422 - PARAMETRO_INVALIDO\n" +
		"• Delete the created client",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpPaymentsApiRecurringPaymentsWrongCustomQuantityTestModuleV4 extends AbstractFvpPaymentsApiRecurringPaymentsWrongCustomQuantityTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetPaymentConsentsV4Endpoint.class;
	}
}
