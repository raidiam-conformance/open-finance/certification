package net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v3.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PaymentConsentRejectionReasonEnumV3 {
	VALOR_INVALIDO, NAO_INFORMADO,
	FALHA_INFRAESTRUTURA, TEMPO_EXPIRADO_AUTORIZACAO,
	TEMPO_EXPIRADO_CONSUMO, REJEITADO_USUARIO,
	CONTAS_ORIGEM_DESTINO_IGUAIS, CONTA_NAO_PERMITE_PAGAMENTO,
	SALDO_INSUFICIENTE, VALOR_ACIMA_LIMITE, QRCODE_INVALIDO;


	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
