package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingInvestmentsRoot extends ResourceBuilder {

    @Override
    @PreEnvironment(strings = "investment-api")
    public Environment evaluate(Environment env) {
        String api = env.getString("investment-api");

        setApi(api);
        setEndpoint("/investments");

        return super.evaluate(env);
    }
}
