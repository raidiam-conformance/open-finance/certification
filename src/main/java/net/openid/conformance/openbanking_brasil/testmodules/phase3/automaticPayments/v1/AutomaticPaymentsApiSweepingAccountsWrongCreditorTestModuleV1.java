package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiSweepingAccountsWrongCreditorTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringPixOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_sweeping-accounts-wrong-creditor_test-module_v1",
	displayName = "automatic-payments_api_sweeping-accounts-wrong-creditor_test-module_v1",
	summary = "Ensure a payment cannot be executed for a creditor different from the one registered at the consent\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, sending the creditor account as the CPF informed at the config, the amount value as 600.00\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED\n" +
		"• Call the POST recurring-payments endpoint with the amount as 300.00, but with a different creditor\n" +
		"• Expect 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO - Validate Error Message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiSweepingAccountsWrongCreditorTestModuleV1 extends AbstractAutomaticPaymentsApiSweepingAccountsWrongCreditorTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return false;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV1Endpoint.class), condition(GetAutomaticPaymentPixRecurringV1Endpoint.class));
	}

}
