package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.investments.funds.v1;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api url: https://openbanking-brasil.github.io/draft-openapi/swagger-apis/investments/1.0.1.yml
 * Api endpoint: /funds
 * Api version: 1.0.1
 */

@ApiName("Investments Funds V1")
public class GetInvestmentsFundsOASValidatorV1  extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-investments-1.0.1.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/funds";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
