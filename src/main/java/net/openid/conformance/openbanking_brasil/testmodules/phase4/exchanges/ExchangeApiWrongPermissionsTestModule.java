package net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractPermissionsCheckingFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetExchangesV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationIdentificationV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckScopeInConfigIsValid;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractOperationID;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.PrepareUrlForFetchingOperation;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "exchange_api_wrong-permissions_test-module",
	displayName = "exchange_api_wrong-permissions_test-module",
	summary = "Test will confirm that a Consent without the Exchange permissions cannot grant access to the user information - This test will start with requiring two Consent Sharing requests to be approved.\n" +
		"(1) - Happy path to obtain an operationId\n" +
		"• Call the POST Consents endpoint with the Exchange API Permission Group\n" +
		"• Expect a 201 - Validate Response and confirm status is on Awaiting Authorisation\n" +
		"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"• Call the GET Consents endpoint\n" +
		"• Expects 200 - Validate response and confirm that the Consent is set to \"\"Authorised\"\"\n" +
		"• Call the GET Operations List Endpoint\n" +
		"• Expects a 200 response - Validate the Response_body - Extract one of the exchange operations\n" +
		"• Call the Delete Consents Endpoints\n" +
		"• Expect a 204 without a body\n" +
		"(2) - Unhappy, making sure access is not granted without proper permission group\n" +
		"• Call the POST Consents with either the customer business or the customer personal permissions group, depending on whether the brazilCnpj has or not been provided\n" +
		"• Expects a success 200 - Validate response\n" +
		"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"• Call the GET Operations List Endpoint\n" +
		"• Expects a 403 response\n" +
		"• Call the GET Operations Identification endpoint with the extracted operationID\n" +
		"• Expects a 403 response\n" +
		"• Call the Delete Consents Endpoints\n" +
		"• Expect a 204 without a body",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class ExchangeApiWrongPermissionsTestModule extends AbstractPermissionsCheckingFunctionalTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
	}

	@Override
	protected void configureClient() {
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetExchangesV1Endpoint.class)
		)));
		callAndStopOnFailure(CheckScopeInConfigIsValid.class);
		super.configureClient();
	}

	@Override
	protected void preFetchResources() {
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(GetExchangesOperationsListV1OASValidator.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(ExtractOperationID.class);
	}

	@Override
	protected void prepareCorrectConsents() {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder
			.addPermissionsBasedOnCategory(OPFCategoryEnum.EXCHANGES)
			.addScopes(OPFScopesEnum.EXCHANGES, OPFScopesEnum.OPEN_ID)
			.build();
	}

	@Override
	protected void prepareIncorrectPermissions() {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		String productType = env.getString("config", "consent.productType");
		if (productType.equals("business")) {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.BUSINESS_REGISTRATION_DATA).build();
		} else {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA).build();
		}
	}

	@Override
	protected void requestResourcesWithIncorrectPermissions() {
		env.putString("metaOnlyRequestDateTime", "true");

		runInBlock("Ensure we cannot call GET operations list endpoint",()->{
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(EnsureResourceResponseCodeWas403.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetExchangesOperationsListV1OASValidator.class, Condition.ConditionResult.FAILURE);
		});
		runInBlock("Ensure we cannot call the GET operations identification endpoint",()->{
			callAndStopOnFailure(PrepareUrlForFetchingOperation.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
			callAndContinueOnFailure(GetExchangesOperationIdentificationV1OASValidator.class, Condition.ConditionResult.FAILURE);

		});
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps()
			.insertAfter(FAPIBrazilConsentEndpointResponseValidatePermissions.class, condition(SaveConsentsAccessToken.class));
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndStopOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class);
		validateConsentResponse();
	}

	protected void validateConsentResponse() {
		callAndContinueOnFailure(PostConsentOASValidatorV3n2.class, Condition.ConditionResult.FAILURE);
	}
}
