package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

import java.util.List;

public abstract class AbstractCreditFixedIncomesTransactionsOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonArray data = body.getAsJsonArray("data");
		data.forEach(el -> assertData(el.getAsJsonObject()));
	}

	private void assertData(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionTypeAdditionalInfo",
			"transactionType",
			"OUTROS"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionUnitPrice",
			"transactionType",
			List.of("COMPRA", "VENDA", "VENCIMENTO")
		);


		assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionQuantity",
			"transactionType",
			List.of("COMPRA", "VENDA", "VENCIMENTO", "TRANSFERENCIA_TITULARIDADE", "TRANSFERENCIA_CUSTODIA")
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"incomeTax",
			"type",
			"SAIDA"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"financialTransactionTax",
			"type",
			"SAIDA"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"remunerationTransactionRate",
			"type",
			"ENTRADA"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"indexerPercentage",
			"type",
			"ENTRADA"
		);
	}
}
