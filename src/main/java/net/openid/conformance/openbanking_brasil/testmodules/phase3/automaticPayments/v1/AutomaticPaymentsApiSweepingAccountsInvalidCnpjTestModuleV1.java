package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_sweeping-accounts-invalid-cnpj_test-module_v1",
	displayName = "automatic-payments_api_sweeping-accounts-invalid-cnpj_test-module_v1",
	summary = "Ensure a consent for cannot be created if there are two creditors that are not from the same company. " +
		"The test will be skipped if no Business Entity CNPJ is provided.\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, sending 2 creditors at the creditors field, changing the last five numbers from a valid CNPJ\n" +
		"• Expect 422 PARAMETRO_INVALIDO - Validate Error Message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModuleV1 extends AbstractAutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return false;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV1Endpoint.class));
	}
}
