package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.condition.common.AbstractWaitForSpecifiedSeconds;
import net.openid.conformance.testmodule.Environment;

public class WaitFor120Seconds extends AbstractWaitForSpecifiedSeconds {
	@Override
	protected long getExpectedWaitSeconds(Environment env) {
		return 120;
	}
}
