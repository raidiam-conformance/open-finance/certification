package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetIncorrectEndToEndIdTimeInThePaymentRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPaymentsApiPixSchedulingEndToEndUnhappyTestModule extends AbstractPaymentsPixSchedulingTestModule{
	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(SetIncorrectEndToEndIdTimeInThePaymentRequestBody.class);
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return super.getPixPaymentSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas422.class));
	}

	@Override
	protected void validateResponse() {
		callAndStopOnFailure(postPaymentValidator());
		env.mapKey(EnsureErrorResponseCodeFieldWasParametroInvalido.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class);
	}
}
