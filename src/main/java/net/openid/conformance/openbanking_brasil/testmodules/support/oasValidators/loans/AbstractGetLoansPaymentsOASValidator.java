package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public abstract class AbstractGetLoansPaymentsOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/contracts/{contractId}/payments";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertCharges(data);
		assertRelease(data);
	}

	private void assertRelease(JsonObject data){
		JsonArray releases = data.getAsJsonArray("releases");

		releases.forEach(release -> assertField1IsRequiredWhenField2HasValue2(
			release.getAsJsonObject(),
			"instalmentId",
			"isOverParcelPayment",
			false
		));
	}

	private void assertCharges(JsonObject data){


		JsonArray charges = JsonPath.read(data, "releases[*].overParcel.charges[*]");
		charges.forEach(charge -> assertField1IsRequiredWhenField2HasValue2(
			charge.getAsJsonObject(),
			"chargeAdditionalInfo",
			"chargeType",
			"OUTROS"
		));
	}
}
