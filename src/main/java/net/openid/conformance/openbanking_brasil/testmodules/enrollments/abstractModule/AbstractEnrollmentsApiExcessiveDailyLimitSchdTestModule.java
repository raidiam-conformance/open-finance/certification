package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.ExtractDailyLimitFromEnrollmentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.ExtractTransactionLimitFromEnrollmentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToTransactionLimitOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.AddMapFromPaymentIdToEndToEndId;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ExtractFiveLastPaymentIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ExtractPaymentIdFromLastPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.PrepareToFetchNextPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule5DailyPaymentsStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractEnrollmentsApiExcessiveDailyLimitSchdTestModule extends AbstractEnrollmentsApiPaymentsCoreTestModule {

	protected int amountOfPayments;
	protected int currentPayment = 0;

	@Override
	protected void getEnrollmentsAfterFidoRegistration() {
		super.getEnrollmentsAfterFidoRegistration();
		callAndStopOnFailure(ExtractTransactionLimitFromEnrollmentResponse.class);
		callAndStopOnFailure(ExtractDailyLimitFromEnrollmentResponse.class);
	}

	@Override
	protected void configPaymentsFlow() {
		setAmountOfPayments();
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		callAndStopOnFailure(SetPaymentAmountToTransactionLimitOnConsent.class);
		if (currentPayment == amountOfPayments - 1) {
			callAndStopOnFailure(RemovePaymentConsentDate.class);
			callAndStopOnFailure(Schedule5DailyPaymentsStarting1DayInTheFuture.class);
		}
		super.configPaymentsFlow();
	}

	@Override
	protected void updatePaymentConsent() {
		if (currentPayment < amountOfPayments) {
			super.updatePaymentConsent();
		}
	}

	@Override
	protected void configureDictInfo() {}

	@Override
	protected void validatePaymentsResponse() {
		if (currentPayment < amountOfPayments) {
			super.validatePaymentsResponse();
		} else {
			callAndStopOnFailure(ExtractPaymentIdFromLastPayment.class);
			callAndStopOnFailure(ExtractFiveLastPaymentIds.class);
			callAndStopOnFailure(AddMapFromPaymentIdToEndToEndId.class);
			super.validatePaymentsResponse();
			runInBlock("Validate that all the remaining payments are SCHD", this::validateAllPayments);
			fetchConsentToCheckStatus("CONSUMED", new EnsurePaymentConsentStatusWasConsumed());
		}
	}

	@Override
	protected void validateFinalState() {
		if (currentPayment < amountOfPayments) {
			super.validateFinalState();
		} else {
			callAndStopOnFailure(EnsurePaymentStatusWasSchd.class);
		}
	}

	protected void validateAllPayments() {
		repeatSequence(this::getPaymentsValidationRepeatSequence)
			.untilTrue("no_more_payments")
			.trailingPause(5)
			.times(50)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();
	}

	protected ConditionSequence getPaymentsValidationRepeatSequence() {
		return sequenceOf(
			condition(ClearRequestObjectFromEnvironment.class),
			condition(PrepareToFetchNextPayment.class),
			condition(SetResourceMethodToGet.class),
			condition(ClearContentTypeHeaderForResourceEndpointRequest.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(AddJWTAcceptHeader.class),
			condition(CallProtectedResource.class),
			condition(EnsureResourceResponseCodeWas200.class),
			condition(EnsureMatchingFAPIInteractionId.class),
			condition(EnsurePaymentStatusWasSchd.class),
			condition(getPaymentValidator())
		);
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		configPaymentsFlow();
		if (currentPayment < amountOfPayments) {
			isEnrollmentsFlowComplete = true;
			currentPayment++;
			eventLog.log(getName(), String.format("Starting execution of payment flow #%d", currentPayment));
			performPreAuthorizationSteps();
			makeRefreshTokenCall();
			executeTestSteps();
			onPostAuthorizationFlowComplete();
		} else {
			fireTestFinished();
		}
	}

	protected void setAmountOfPayments() {
		double transactionLimit = Double.parseDouble(env.getString("transaction_limit"));
		double dailyLimit = Double.parseDouble(env.getString("daily_limit"));
		amountOfPayments = (int) (transactionLimit / dailyLimit) + 1;
		eventLog.log(getName(), String.format("We are going to execute the entire payments flow %d times", amountOfPayments));
	}

	@Override
	protected void executeFurtherSteps() {}

	protected abstract Class<? extends Condition> getPaymentConsentValidator();
}
