package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.FAPIBrazilSignPaymentInitiationRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractPaymentId;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj.editRequestBody.EditPaymentsRequestBodyToRemoveConsentId;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.SetPaymentAuthorisationFlowToFidoFlow;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.SetPaymentAuthorisationFlowToHybridFlow;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.PrepareToFetchPayment;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpStatus;

public abstract class AbstractEnrollmentsApiPaymentsUnmatchingFieldsTestModule extends AbstractEnrollmentsApiPaymentsCoreTestModule {

	protected abstract Class<? extends Condition> postPaymentErrorValidator();

	protected int amountOfPayments = 2;
	protected int currentPayment = 0;

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		ConditionSequence pixPaymentSequence = super.getPixPaymentSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class)
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE));
		if(currentPayment == 2) {
			pixPaymentSequence = pixPaymentSequence.insertBefore(FAPIBrazilSignPaymentInitiationRequest.class, condition(EditPaymentsRequestBodyToRemoveConsentId.class));
		}
		return pixPaymentSequence;
	}

	@Override
	protected void validatePaymentsResponse() {
		if (OIDFJSON.getInt(env.getObject("resource_endpoint_response_full").get("status")) == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			validate422Response();
		} else {
			validate201Response();
		}
	}

	protected void validate422Response() {
		runInBlock("Validate 422 response", () -> {
			callAndContinueOnFailure(postPaymentErrorValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			if (currentPayment == 1) {
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class, Condition.ConditionResult.FAILURE);
			} else if (currentPayment == 2) {
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroNaoInformado.class, Condition.ConditionResult.FAILURE);
			}
			env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		});
	}

	protected void validate201Response() {
		runInBlock("Get pix/payments endpoint - Expects 200 with RJCT status for DETALHE_PAGAMENTO_INVALIDO", () -> {
			callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
			callAndStopOnFailure(ExtractPaymentId.class);
			callAndStopOnFailure(PrepareToFetchPayment.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
			callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
			callAndStopOnFailure(AddJWTAcceptHeader.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getPaymentValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsurePaymentStatusWasRjct.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsurePaymentRejectionReasonCodeWasDetalhePagamentoInvalido.class, Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected void executeFurtherSteps() {}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		if (!isEnrollmentsFlowComplete) {
			isEnrollmentsFlowComplete = true;
		}
		if (currentPayment < amountOfPayments) {
			currentPayment++;
			configPaymentsFlow();
			eventLog.log(getName(), String.format("Starting execution of payment flow #%d", currentPayment));
			performPreAuthorizationSteps();
			makeRefreshTokenCall();
			executeTestSteps();
			onPostAuthorizationFlowComplete();
		} else {
			fireTestFinished();
		}
	}

	@Override
	protected void configureDictInfo() {
		if (currentPayment == 1) {
			callAndStopOnFailure(SetPaymentAuthorisationFlowToHybridFlow.class);
		} else {
			callAndStopOnFailure(SetPaymentAuthorisationFlowToFidoFlow.class);
		}
	}

}
