package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

public class EnsureNumberOfTotalRecordsIsAtLeast25FromData extends AbstractEnsureNumberOfTotalRecordsFromData{

	@Override
	protected  TotalRecordsComparisonOperator getComparisonMethod(){
		return TotalRecordsComparisonOperator.AT_LEAST;
	}

	@Override
	protected int getTotalRecordsComparisonAmount(){
		return 25;
	}
}
