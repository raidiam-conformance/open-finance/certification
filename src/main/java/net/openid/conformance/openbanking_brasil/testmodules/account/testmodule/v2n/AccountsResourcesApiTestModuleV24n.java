package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsListOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractApiResourceTestModuleV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "accounts_api_resources_test-module_v2-4",
	displayName = "Validate structure of all accounts API resources V2",
	summary = "Makes sure that the Resource API V2 and the API that is the scope of this test plan are returning the same available IDs\n" +
		"\u2022Create a consent with all the permissions needed to access the tested API\n" +
		"\u2022 Expects server to create the consent with 201\n" +
		"\u2022 Redirect the user to authorize at the financial institution\n" +
		"\u2022 Call the tested resource API V2\n" +
		"\u2022 Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the id provided by this API\n" +
		"\u2022 Call the resources API V2\n" +
		"\u2022 Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class AccountsResourcesApiTestModuleV24n extends AbstractApiResourceTestModuleV2 {

	@Override
	protected Class<? extends Condition> getResourceValidator() {
		return GetResourcesOASValidatorV3.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetConsentV3Endpoint.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getApiEndpoint() {
		return GetAccountsV2Endpoint.class;
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.ACCOUNTS;
	}

	@Override
	protected OPFCategoryEnum getProductCategoryEnum() {
		return OPFCategoryEnum.ACCOUNTS;
	}

	@Override
	protected Class<? extends Condition> apiValidator() {
		return GetAccountsListOASValidatorV2n4.class;
	}

	@Override
	protected String apiName() {
		return "accounts";
	}

	@Override
	protected String apiResourceId() {
		return "accountId";
	}

	@Override
	protected String resourceType() {
		return EnumResourcesType.ACCOUNT.name();
	}

}
