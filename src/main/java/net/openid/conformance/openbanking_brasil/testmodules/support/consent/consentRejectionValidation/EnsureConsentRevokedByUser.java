package net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation;

public class EnsureConsentRevokedByUser extends AbstractConsentRejectionValidation {

	@Override
	protected String getRejectionReasonCode() {
		return "CUSTOMER_MANUALLY_REVOKED";
	}

	@Override
	protected String getRejectedBy() {
		return "USER";
	}
}
