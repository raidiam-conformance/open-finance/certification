package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AbstractEditObjectBodyToManipulateCnpjOrCpf;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ extends AbstractEditObjectBodyToManipulateCnpjOrCpf {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonArray creditors = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.map(consentElement -> consentElement.getAsJsonObject().getAsJsonObject("data"))
			.map(data -> data.getAsJsonArray("creditors"))
			.orElseThrow(() -> error("Unable to find data.creditors in consents payload"));

		JsonObject originalCreditor = creditors.get(0).getAsJsonObject();
		JsonObject newCreditor = originalCreditor.deepCopy();

		String newCnpj = generateNewRandomCnpj();

		newCreditor.addProperty("cpfCnpj", newCnpj);
		creditors.add(newCreditor);

		logSuccess("A new creditor with a different CNPJ has been added to the creditors array",
			args("creditors", creditors));

		return env;
	}
}
