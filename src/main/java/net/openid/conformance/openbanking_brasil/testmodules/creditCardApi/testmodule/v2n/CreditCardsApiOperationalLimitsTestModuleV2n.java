package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.abstractModule.AbstractCreditCardsApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsBillsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsIdentificationOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsLimitsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsListOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsTransactionsCurrentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsTransactionsOASValidatorV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "credit-cards_api_operational-limits_test-module_v2-1",
	displayName = "Credit Cards API Operational Limits test module V2 ",
	summary = "This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Credit Cards API are considered correctly and, if present, that the pagination-key parameter is correctly serving it’s function\n" +
		"This test will require the user to have set at least one ACTIVE resource each with at least 20 Transactions to be returned on the transactions endpoint. The tested resource must be sent on the field FirstCreditCardAccountId (R_1) on the test configuration and on SecondCreditCardAccountId (R_2), in case the company supports multiple accounts for the same user. If the server does not provide the SecondCreditCardAccountId (R_2) the test will skip this part of the test" +
		"This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Credit Cards API are considered correctly and, if present, that the pagination-key parameter is correctly serving it’s function\n" +
		"\u2022 Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL), the CPF for Operational Limits (CPF for OL) and at least the FirstCreditCardAccountId (R_1) have been provided\n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Credit Cards permission group - Expect Server to return a 201 - Save ConsentID (1)\n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards API with the saved Resource ID (R_1) 4 Times  - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Transactions API with the saved Resource ID (R_1) 4 Times, send query parameters fromBookingDate as D-6 and toBookingDate as Today - Expect a 200 response - On the first API Call Make Sure That at least 20 Transactions have been returned \n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Bills API with the saved Resource ID (R_1) 30 Times  - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Limits API with the saved Resource ID (R_1) 240 Times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Transactions-Current API with the saved Resource ID (R_1) 240 Times  - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards Transactions-Current API with the saved Resource ID (R_1) Once, send query parameters fromBookingDate as D-6 and toBookingDate as Today, with page-size=1 - Expect a 200 response - Fetch the links.next URI\n" +
		"\u2022 Call the GET Credit Cards Transactions-Current API 19 more times, always using the links.next returned and always forcing the page-size to be equal to 1 - Expect a 200 on every single call\n" +
		"\u2022 With the authorized consent id (1), call the GET Credit Cards API with the saved Resource ID (R_2) once - Expect a 200 response\n" +
		"\u2022 If the field SecondCreditCardAccountId (R_2) has been returned, repeat  the exact same process done with the first tested resources (R_1) but now, execute it against the second returned Resource (R_2) \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness",
		"resource.first_credit_card_account_id",
		"resource.second_credit_card_account_id"
	}
)
public class CreditCardsApiOperationalLimitsTestModuleV2n extends AbstractCreditCardsApiOperationalLimitsTestModule {


	@Override
	protected Class<? extends Condition> cardIdentificationResponseValidator() {
		return GetCreditCardAccountsIdentificationOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> creditCardAccountsTransactionResponseValidator() {
		return GetCreditCardAccountsTransactionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> creditCardBillValidator() {
		return GetCreditCardAccountsBillsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> creditCardAccountsLimitsResponseValidator() {
		return GetCreditCardAccountsLimitsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> creditCardAccountsTransactionCurrentResponseValidator() {
		return GetCreditCardAccountsTransactionsCurrentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> cardAccountsDataResponseResponseValidator() {
		return GetCreditCardAccountsListOASValidatorV2.class;
	}
}

