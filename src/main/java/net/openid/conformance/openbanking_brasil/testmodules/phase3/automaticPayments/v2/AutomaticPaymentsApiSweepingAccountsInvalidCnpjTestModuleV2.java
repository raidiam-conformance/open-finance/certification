package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_sweeping-accounts-invalid-cnpj_test-module_v2",
	displayName = "automatic-payments_api_sweeping-accounts-invalid-cnpj_test-module_v2",
	summary = "Ensure a consent cannot be created if there are two creditors that are not from the same company. " +
		"The test will be skipped if no Business Entity CNPJ is provided.\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, sending 2 creditors at the creditors field, changing the numbers from a valid CNPJ\n" +
		"• Expect 422 DETALHE_PAGAMENTO_INVALIDO - Validate Error Message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModuleV2 extends AbstractAutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentErrorValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected void performTestSteps() {
		performTestStep(
			"POST consents endpoint with sweeping accounts, sending 2 creditors with different CNPJ - Expects 422 DETALHE_PAGAMENTO_INVALIDO",
			EditRecurringPaymentsConsentBodyToSetSecondCreditorWithInvalidCNPJ.class,
			EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class
		);
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class));
	}
}
