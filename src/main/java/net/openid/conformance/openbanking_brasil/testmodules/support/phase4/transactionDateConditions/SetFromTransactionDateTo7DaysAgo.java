package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions;

public class SetFromTransactionDateTo7DaysAgo extends AbstractSetFromTransactionDate {

    @Override
    protected int amountOfDaysToThePast() {
        return 7;
    }
}
