package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1.GetBankFixedIncomesListOASValidatorV1;

public class GetBankFixedIncomesListOASValidatorV1n4 extends GetBankFixedIncomesListOASValidatorV1 {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/bank-fixed-incomes/bank-fixed-incomes-v1.0.4.yml";
	}

}
