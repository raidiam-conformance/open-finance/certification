package net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas;

public class EnsureConsentWasAuthorised extends AbstractEnsureConsentStatusWas {

	@Override
	protected String getExpectedStatus() {
		return "AUTHORISED";
	}
}
