package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule5DailyPaymentsStarting1DayInTheFuture;

public abstract class AbstractPaymentsApiRecurringPaymentsDailyCoreTestModule extends AbstractPaymentApiSchedulingTestModule {

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new Schedule5DailyPaymentsStarting1DayInTheFuture();
	}

	@Override
	protected void configureDictInfo() {
		// This space intentionally left blank
	}
}
