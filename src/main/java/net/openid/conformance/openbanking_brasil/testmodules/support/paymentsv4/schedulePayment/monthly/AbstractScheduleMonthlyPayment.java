package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.monthly;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public abstract class AbstractScheduleMonthlyPayment extends AbstractSchedulePayment {

	public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	protected JsonObject createScheduleObject() {
		return new JsonObjectBuilder()
			.addFields("monthly", Map.of(
				"dayOfMonth", getDayOfMonth(),
				"startDate", getStartDateStr(),
				"quantity", getQuantity()
			))
			.build();
	}

	protected String getStartDateStr() {
		return LocalDate.now(ZoneId.of("America/Sao_Paulo")).plusDays(getNumberOfDaysToAddToCurrentDate()).format(FORMATTER);
	}

	protected abstract int getDayOfMonth();
	protected abstract int getNumberOfDaysToAddToCurrentDate();
	protected abstract int getQuantity();
}
