package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public abstract class AbstractGetEnrollmentsFromAuthServer extends AbstractGetXFromAuthServer {

	@Override
	protected String getResourceUrlProperty() {
		return "enrollmentsUrl";
	}

}
