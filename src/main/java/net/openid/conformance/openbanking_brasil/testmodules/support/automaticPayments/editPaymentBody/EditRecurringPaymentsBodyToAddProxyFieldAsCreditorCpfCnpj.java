package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EditRecurringPaymentsBodyToAddProxyFieldAsCreditorCpfCnpj extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));

		String creditorCpfCnpj = Optional.ofNullable(env.getElementFromObject("resource", "creditorCpfCnpj"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find creditor CPF/CNPJ in the resource"));

		data.addProperty("proxy", creditorCpfCnpj);

		logSuccess("The proxy field has been added to the payload with value matching creditor CPF/CNPJ",
			args("data", data));

		return env;
	}

}
