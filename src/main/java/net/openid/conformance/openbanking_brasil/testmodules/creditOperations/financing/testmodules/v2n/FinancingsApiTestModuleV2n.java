package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.AbstractFinancingsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2.GetFinancingsIdentificationV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2.GetFinancingsListV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2.GetFinancingsPaymentsV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2.GetFinancingsScheduledInstalmentsV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2.GetFinancingsWarrantiesV2n2OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "financings_api_core_test-module_v2-2",
	displayName = "Validate structure of all financing API resources V2",
	summary = "Validates the structure of all financing API resources V2\n" +
		"• Creates a consent with all the permissions needed to access the Credit Operations API  (\"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"• Calls GET Financings Contracts API V2\n" +
		"• Expects 200 - Fetches one of the IDs returned\n" +
		"• Calls GET Financings Contracts API V2 with ID \n" +
		"• Expects 200\n" +
		"• Calls GET Financings Warranties API V2\n" +
		"• Expects 200\n" +
		"• Calls GET Financings Payments API V2\n" +
		"• Expects 200\n" +
		"• Calls GET Financings Contracts Instalments API V2\n" +
		"• Expects 200\n" +
		"8",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class FinancingsApiTestModuleV2n extends AbstractFinancingsApiTestModule {

	@Override
	protected Class<? extends Condition> apiResourceContractListResponseValidator() {
		return GetFinancingsListV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractResponseValidator() {
		return GetFinancingsIdentificationV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractGuaranteesResponseValidator() {
		return GetFinancingsWarrantiesV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractPaymentsResponseValidator() {
		return GetFinancingsPaymentsV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractInstallmentsResponseValidator() {
		return GetFinancingsScheduledInstalmentsV2n2OASValidator.class;
	}

}
