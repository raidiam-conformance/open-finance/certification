package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CreateIdempotencyKey;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditConsentsAuthoriseRequestBodyToAddRiskSignalsIsCharging;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasErroIdempotencia;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasNaoInformadoV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractEnrollmentsApiIdempotencyTestModule extends AbstractEnrollmentsApiPaymentsCoreTestModule {

	@Override
	protected PostEnrollmentsResourceSteps createPostSignOptionsSteps() {
		return (PostEnrollmentsResourceSteps) super.createPostSignOptionsSteps()
			.skip(CreateIdempotencyKey.class, "We will be using the same idempotency key as POST consents");
	}

	@Override
	protected ConditionSequence createConsentsAuthoriseSteps() {
		return super.createConsentsAuthoriseSteps()
			.skip(CreateIdempotencyKey.class, "We will be using the same idempotency key as POST consents");
	}

	@Override
	protected void postAndValidateConsentAuthorise() {
		super.postAndValidateConsentAuthorise();

		runInBlock("Post consents authorise with the same idempotency key and payload - Expects 204", () -> {
			call(createConsentsAuthoriseSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Post consents authorise with the same idempotency key but different payload - 422 ERRO_IDEMPOTENCIA", () -> {
			call(createConsentsAuthoriseSteps().insertBefore(SignEnrollmentsRequest.class, condition(EditConsentsAuthoriseRequestBodyToAddRiskSignalsIsCharging.class)));
			callAndContinueOnFailure(postConsentsAuthoriseValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasErroIdempotencia.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasErroIdempotencia.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasErroIdempotencia.RESPONSE_ENV_KEY);
		});
	}

	@Override
	protected void getConsentAfterConsentAuthorise() {
		fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
		callAndContinueOnFailure(EnsureConsentsRejectionReasonCodeWasNaoInformadoV3.class, Condition.ConditionResult.FAILURE);
	}


	@Override
	protected void postAndValidatePayment() {}

	@Override
	protected void executeFurtherSteps() {}

	@Override
	protected void configureDictInfo() {}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return null;
	}

	protected abstract Class<? extends Condition> postConsentsAuthoriseValidator();
}
