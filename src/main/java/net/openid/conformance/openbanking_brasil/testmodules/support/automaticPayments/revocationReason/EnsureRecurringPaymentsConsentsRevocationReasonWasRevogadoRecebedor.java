package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revocationReason;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevocationReasonEnum;

public class EnsureRecurringPaymentsConsentsRevocationReasonWasRevogadoRecebedor extends AbstractEnsureRecurringPaymentsConsentsRevocationReason {

	@Override
	protected RecurringPaymentsConsentsRevocationReasonEnum expectedRevocationReason() {
		return RecurringPaymentsConsentsRevocationReasonEnum.REVOGADO_RECEBEDOR;
	}
}
