package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class RecordCurrentTime extends AbstractCondition {


	@Override
	public Environment evaluate(Environment env) {
		String time = LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME);
		log("Recorded time", args("Time", time));
		env.putString("timestamp", time);
		return env;
	}
}
