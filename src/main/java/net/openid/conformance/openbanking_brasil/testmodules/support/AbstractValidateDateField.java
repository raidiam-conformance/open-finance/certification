package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;

public abstract class AbstractValidateDateField extends AbstractCondition {

	private final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(getDateFormatter());

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {

		JsonObject body = JsonParser.parseString(env.getString("resource_endpoint_response_full", "body"))
			.getAsJsonObject();

		JsonArray dataArray = body.getAsJsonArray("data");

		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

		dataArray.forEach(jsonElement -> {
			JsonObject transactionObject = jsonElement.getAsJsonObject();
			String transactionDateString = OIDFJSON.getString(transactionObject.get(getDateField()));
			try {
				LocalDate transactionDate = LocalDate.parse(transactionDateString, FORMATTER);

				if (isDateInvalid(currentDate, transactionDate)) {
					throw error(getErrorMessage(),
						Map.of("Transaction", transactionObject));
				}

				logSuccess("All transactions dates are valid");
			} catch (DateTimeParseException e) {
				throw error("Could not parse the value of the transaction date field", e);
			}
		});
		return env;
	}

	protected abstract boolean isDateInvalid(LocalDate currentDate, LocalDate transactionDate);

	protected abstract String getErrorMessage();

	protected abstract String getDateField();
	protected abstract String getDateFormatter();

}
