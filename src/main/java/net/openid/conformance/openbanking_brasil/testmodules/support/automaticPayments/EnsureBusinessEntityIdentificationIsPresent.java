package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class EnsureBusinessEntityIdentificationIsPresent extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonElement businessEntityIdElement = env.getElementFromObject("config", "resource.businessEntityIdentification");

		if (businessEntityIdElement == null) {
			env.putBoolean("continue_test", false);
			throw error("Business Entity CNPJ field is empty. Institution is assumed to not have this functionality and the test will be skipped.");
		}

		logSuccess("Business Entity CNPJ field is filled. The test will continue its execution",
			args("Business Entity CNPJ", OIDFJSON.getString(businessEntityIdElement)));
		return env;
	}
}
