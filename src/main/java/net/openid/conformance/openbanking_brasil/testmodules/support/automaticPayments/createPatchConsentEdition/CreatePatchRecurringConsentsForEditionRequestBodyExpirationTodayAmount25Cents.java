package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class CreatePatchRecurringConsentsForEditionRequestBodyExpirationTodayAmount25Cents extends AbstractCreatePatchRecurringConsentsForEditionRequestBody {

	@Override
	protected String getExpirationDateTime() {
		LocalDate currentDate = LocalDate.now(ZoneOffset.UTC);
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String currentDateStr = currentDate.format(dateFormatter);
		return String.format("%sT23:59:59Z", currentDateStr);
	}

	@Override
	protected String getMaxVariableAmount() {
		return "0.25";
	}
}
