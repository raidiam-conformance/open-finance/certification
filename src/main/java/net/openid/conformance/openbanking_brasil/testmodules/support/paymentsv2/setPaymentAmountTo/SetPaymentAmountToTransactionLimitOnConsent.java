package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractSetAmount;
import net.openid.conformance.testmodule.Environment;

public class SetPaymentAmountToTransactionLimitOnConsent extends AbstractSetAmount {

	private String transactionLimit;

	@Override
	@PreEnvironment(required = "resource", strings = "transaction_limit")
	public Environment evaluate(Environment env) {
		transactionLimit = env.getString("transaction_limit");
		return super.evaluate(env);
	}

	@Override
	protected String getAmount() {
		return transactionLimit;
	}

	@Override
	protected String getEnvKey() {
		return "brazilPaymentConsent";
	}
}
