package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;

import java.time.LocalDate;
import java.time.ZoneOffset;

public class CreateAutomaticRecurringConfigurationObjectSemanalWithoutFirstPayment extends AbstractCreateAutomaticRecurringConfigurationObject {

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.SEMANAL;
	}

	@Override
	protected String getFixedAmount() {
		return null;
	}

	@Override
	protected boolean hasFirstPayment() {
		return false;
	}

	@Override
	protected String getReferenceStartDate() {
		LocalDate today = LocalDate.now(ZoneOffset.UTC).plusDays(2);
		return today.format(DATE_FORMATTER);
	}
}
