package net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractCustomerBusinessApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessIdentificationOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessQualificationResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessRelationsResponseOASValidatorV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "customer-business_api_operational-limits_test-module_v2-2",
	displayName = "Make sure that the server is not blocking access to the APIs as long as the operational limits for the Customer Business API are considered correctly",
	summary ="This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Customer Business API are considered correctly.\n" +
		"\u2022 Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test have been provided\n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Customer Business permission group - Expect Server to return a 201 - Save ConsentID (1)\n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 With the authorized consent id (1), call the GET Customer Business Identifications API 4 Times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Customer Business Qualifications 4 Times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Customer Business Financial Relations 4 Times - Expect a 200 response\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness"
	}
)
public class CustomerBusinessApiOperationalLimitsTestModuleV2n extends AbstractCustomerBusinessApiOperationalLimitsTestModule {

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getBusinessQualificationResponseValidator() {
		return BusinessQualificationResponseOASValidatorV2.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getBusinessIdentificationResponseValidator() {
		return BusinessIdentificationOASValidatorV2.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getBusinessRelationsResponseValidator() {
		return BusinessRelationsResponseOASValidatorV2.class;
	}

}
