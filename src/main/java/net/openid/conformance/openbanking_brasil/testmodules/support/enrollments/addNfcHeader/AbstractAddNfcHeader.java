package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.addNfcHeader;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractAddNfcHeader extends AbstractCondition {

	protected abstract boolean nfcHeaderValue();

	@Override
	@PostEnvironment(required = "resource_endpoint_request_headers")
	public Environment evaluate(Environment env) {
		JsonObject headers = env.getObject("resource_endpoint_request_headers");

		if (headers == null) {
			headers = new JsonObject();
			env.putObject("resource_endpoint_request_headers", headers);
		}

		headers.addProperty("x-bcb-nfc", String.valueOf(nfcHeaderValue()));

		logSuccess("Added x-bcb-nfc to resource endpoint request headers",
			args("resource_endpoint_request_headers", headers));

		return env;
	}
}
