package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.checkCurrentTime;

import java.time.LocalTime;

public class CheckCurrentTimeAfter12PM extends AbstractCheckCurrentTime {

	@Override
	protected LocalTime getStartTime() {
		return LocalTime.of(12, 0);
	}

	@Override
	protected LocalTime getEndTime() {
		return LocalTime.of(23, 59, 59);
	}
}
