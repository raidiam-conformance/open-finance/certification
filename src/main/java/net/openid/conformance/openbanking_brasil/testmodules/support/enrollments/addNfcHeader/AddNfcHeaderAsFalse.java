package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.addNfcHeader;

public class AddNfcHeaderAsFalse extends AbstractAddNfcHeader {

	@Override
	protected boolean nfcHeaderValue() {
		return false;
	}
}
