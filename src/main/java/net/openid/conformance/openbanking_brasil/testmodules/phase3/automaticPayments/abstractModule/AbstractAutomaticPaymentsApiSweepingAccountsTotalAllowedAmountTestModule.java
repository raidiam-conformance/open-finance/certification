package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsManualObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.addLocalInstrument.AddManuLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToRemoveStartDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetUserDefinedCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToRemoveProxy;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasLimiteValorTotalConsentimentoExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentRejectionReasonCodeWasLimiteValorTotalConsentimentoExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo60Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.GenerateNewE2EID;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractAutomaticPaymentsApiSweepingAccountsTotalAllowedAmountTestModule extends AbstractAutomaticPaymentsFunctionalTestModule {

	protected boolean isFirstPayment = true;

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddManuLocalInstrument.class);
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToRemoveStartDateTime.class);
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetUserDefinedCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentsBodyToRemoveProxy.class);
		callAndStopOnFailure(AddRiskSignalsManualObjectToPixPaymentBody.class);
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		env.putString("recurring_total_amount", "1.00");
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return new SetAutomaticPaymentAmountTo60Cents();
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();

		isFirstPayment = false;
		userAuthorisationCodeAccessToken();

		runInBlock("POST second recurring-payment with amount 0.60 BRL, exceeding the limit set at the consent", () -> {
			callAndStopOnFailure(GenerateNewE2EID.class);
			call(getPixPaymentSequence().replace(EnsureResourceResponseCodeWas201.class,
				condition(EnsureResourceResponseCodeWas201Or422.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)));

			int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));

			if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				callAndContinueOnFailure(postPaymentValidator(), Condition.ConditionResult.FAILURE);
				env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasLimiteValorTotalConsentimentoExcedido.class, Condition.ConditionResult.FAILURE);
				env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
			} else if (status == HttpStatus.CREATED.value()) {
				super.validateResponse();
			}
		});

		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
	}

	@Override
	protected void validateFinalState() {
		if (isFirstPayment) {
			super.validateFinalState();
		} else {
			callAndContinueOnFailure(EnsurePaymentStatusWasRjct.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureRecurringPaymentRejectionReasonCodeWasLimiteValorTotalConsentimentoExcedido.class, Condition.ConditionResult.FAILURE);
		}
	}
}
