package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.abstractModule;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.AbstractAutomaticPaymentsDCRWebhookTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringConsentIdToQuery;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureCreditorIdentificationIsCnpj;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.addLocalInstrument.AddManuLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanal;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToAddDefinedCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetFirstPaymentCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToRemoveProxy;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForFirstPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForSemanalPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToFirstPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToTwoDaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreOneAcscAndOneCancPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo50Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountToFirstPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringPaymentWebhooksReceivedThree;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.AddMapFromPaymentIdToEndToEndId;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPatchRecurringPaymentEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.testmodule.OIDFJSON;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixWebhookTestModule extends AbstractAutomaticPaymentsDCRWebhookTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanal();
	}

	protected boolean isFirstPayment = true;

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(EnsureCreditorIdentificationIsCnpj.class);
		callAndStopOnFailure(AddManuLocalInstrument.class);
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(EditRecurringPaymentsBodyToRemoveProxy.class);
		configurePaymentDate();
		configureCreditorFields();
	}

	// TODO: delete this method when V1 is no longer supported
	@Override
	protected void keepOldStartDateTime() {}

	protected void configurePaymentDate() {
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToFirstPaymentDate.class);
		callAndStopOnFailure(SetPaymentReferenceForFirstPayment.class);
		callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
	}

	protected void configureCreditorFields() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToAddDefinedCreditor.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetFirstPaymentCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj.class);
	}

	@Override
	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return new SetAutomaticPaymentAmountToFirstPaymentAmount();
	}

	@Override
	protected void requestProtectedResource() {
		env.mapKey("old_access_token", "authorization_code_access_token");
		callAndStopOnFailure(SaveAccessToken.class);
		eventLog.log("Saved the authorization_code access token", env.getObject("authorization_code_access_token"));
		env.unmapKey("old_access_token");
		super.requestProtectedResource();
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();
		callAndStopOnFailure(AddMapFromPaymentIdToEndToEndId.class);
		stepsAfterFirstPayment();
	}

	protected void stepsAfterFirstPayment() {
		configureSecondPayment();
		postAndValidateSecondPayment();
		patchSecondPayment();
		getAndValidateRecurringPaymentsEndpoint();
	}

	protected void configureSecondPayment() {
		isFirstPayment = false;
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToTwoDaysInTheFuture.class);
		callAndStopOnFailure(SetPaymentReferenceForSemanalPayment.class);
		callAndStopOnFailure(SetAutomaticPaymentAmountTo50Cents.class);
		callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
	}

	protected void postAndValidateSecondPayment() {
		loadAuthorizationCode();
		runInBlock("Call pix/payments endpoint", () -> {
			call(getPixPaymentSequence());
			callAndStopOnFailure(AddMapFromPaymentIdToEndToEndId.class);
		});
		runInBlock("Validate response", () -> {
			call(postPaymentValidationSequence());
			repeatSequence(this::getRepeatSequence)
				.untilTrue("payment_proxy_check_for_reject")
				.trailingPause(30)
				.times(5)
				.validationSequence(this::getPaymentValidationSequence)
				.onTimeout(sequenceOf(
					condition(TestTimedOut.class),
					condition(ChuckWarning.class)))
				.run();
			validateFinalState();
		});
	}

	@Override
	protected void validateFinalState() {
		if (isFirstPayment) {
			super.validateFinalState();
		} else {
			callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class, Condition.ConditionResult.FAILURE);
		}
	}

	protected void patchSecondPayment() {
		useClientCredentialsAccessToken();
		runInBlock("Call PATCH recurring-payment endpoint", () -> {
			call(new CallPatchRecurringPaymentEndpointSequence());
			callAndContinueOnFailure(patchPaymentValidator(), Condition.ConditionResult.FAILURE);
		});
	}

	protected void getAndValidateRecurringPaymentsEndpoint() {
		runInBlock("Call GET recurring-payments endpoint to check all payments", () -> {
			callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
			callAndStopOnFailure(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
			callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
			callAndStopOnFailure(AddRecurringConsentIdToQuery.class);
			callAndStopOnFailure(CallProtectedResource.class);
			callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
			callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
			callAndContinueOnFailure(getPaymentsByConsentIdValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getEnsureThereArePaymentsCondition().getClass(), Condition.ConditionResult.FAILURE);
		});
	}

	protected AbstractEnsureThereArePayments getEnsureThereArePaymentsCondition() {
		return new EnsureThereAreOneAcscAndOneCancPayments();
	}

	@Override
	protected void validatePaymentConsentWebhooks(){
		JsonArray webhooks = env.getObject("webhooks_received_recurring_consent").get("webhooks").getAsJsonArray();
		int numberOfWebhooks = 0;
		for (JsonElement webhook : webhooks) {
			String type = OIDFJSON.getString(webhook.getAsJsonObject().get("type"));
			if (type.equals("recurring-consent")) {
				numberOfWebhooks++;
			}
		}

		env.putBoolean("recurring_payment_consent_webhook_received", false);
		if (numberOfWebhooks > 0) {
			callAndStopOnFailure(ValidateRecurringConsentWebhooks.class);
		}
	}

	@Override
	protected void checkFinalConsentStatus() {}

	protected void loadAuthorizationCode() {
		env.mapKey("old_access_token", "authorization_code_access_token");
		callAndStopOnFailure(LoadOldAccessToken.class);
		callAndStopOnFailure(SaveAccessToken.class);
		eventLog.log("Loaded the authorization_code access token", env.getObject("access_token"));
		env.unmapKey("old_access_token");
	}

	@Override
	protected void validatePaymentWebhooks() {
		env.putBoolean("recurring_payment_webhook_received", false);
		callAndStopOnFailure(ValidateRecurringPaymentWebhooksReceivedThree.class);
	}

	protected abstract Class<? extends Condition> patchPaymentValidator();
	protected abstract Class<? extends Condition> getPaymentsByConsentIdValidator();
}
