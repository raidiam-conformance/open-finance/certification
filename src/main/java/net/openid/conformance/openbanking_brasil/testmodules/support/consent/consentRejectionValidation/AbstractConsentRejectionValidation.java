package net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Map;

public abstract class AbstractConsentRejectionValidation extends AbstractCondition {


	protected abstract String getRejectionReasonCode();
	protected abstract String getRejectedBy();

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		try {
			JsonObject data = extractData(env);
			if (validateResponse(data)) {
				logSuccess("Rejection object contains expected results.",
					args("Expected reason: ", getRejectionReasonCode(), "Expected rejectedBy: ", getRejectedBy()));
			} else {
				throw error("Rejection object did not match expected results.",
					args("Expected reason: ", getRejectionReasonCode(), "Expected rejectedBy: ", getRejectedBy()));
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected JsonObject extractData(Environment env) throws ParseException {
		return BodyExtractor.bodyFrom(env, "consent_endpoint_response_full")
			.map(OIDFJSON::toObject)
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Could not extract data from response body"));
	}

	protected boolean validateResponse(JsonObject data) {
		JsonObject rejection = data.getAsJsonObject("rejection");
		if (rejection == null) {
			JsonElement statusElem = data.get("status");
			if (!statusElem.isJsonNull()) {
				if(OIDFJSON.getString(statusElem).equals("REJECTED")) {
					throw error("consent status is REJECTED but no rejection object found", args("data", data));
				}
			}
			log("Status is not REJECTED");
			return false;
		}
		JsonElement rejectionReasonElem = rejection.get("reason");

		if (rejectionReasonElem == null) {
			throw error("Rejection object did not contain reason object");
		}

		if(!rejectionReasonElem.isJsonObject()) {
			throw error("Reason object is not a json object, it should have the mandatory field code");
		}

		JsonObject rejectionReason = rejectionReasonElem.getAsJsonObject();
		String code = null;
		if(rejectionReason.has("code")) {
			code = OIDFJSON.getString(rejectionReason.get("code"));
		}

		if (Strings.isNullOrEmpty(code)){
			throw error("Unable to find rejection code in rejection object");
		}
		logSuccess(code);

		if (!rejection.has("rejectedBy")) {
			throw error("Unable to find rejectedBy inside rejection object");
		}
		String rejectedBy = OIDFJSON.getString(rejection.get("rejectedBy"));
		if (Strings.isNullOrEmpty(rejectedBy)){
			throw error("RejectedBy is not a string inside rejection object");
		}
		logSuccess(rejectedBy);

		if (getRejectionReasonCode().equals(code) && getRejectedBy().equals(rejectedBy)){
			logSuccess("Successfully found code and rejectedBy", Map.of("code",code,"rejectedBy",rejectedBy));
			return true;
		}
		return false;
	}
}
