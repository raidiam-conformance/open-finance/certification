package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.personalInsurance.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

import java.util.Optional;

@ApiName("Pension Insurance V2")
public class GetPersonalInsuranceOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/personalInsurance/personal-insurance-2.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/personals";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		for(JsonElement dataElement : body.getAsJsonArray("data")) {
			JsonObject data = dataElement.getAsJsonObject();
			assertCoveragesTypeAdditionalInfosConstraints(data);
			assertPaymentMethodAdittionalInfoConstraint(data);
		}
	}

	protected void assertCoveragesTypeAdditionalInfosConstraints(JsonObject data) {
		JsonArray coverages = JsonPath.read(data, "$.society.products[*].coverages[*]");
		for(JsonElement coverageElement : coverages) {
			this.assertField1IsRequiredWhenField2HasValue2(
				coverageElement.getAsJsonObject(),
				"typeAdditionalInfos",
				"type",
				"OUTROS"
			);
		}
	}

	protected void assertPaymentMethodAdittionalInfoConstraint(JsonObject data) {
		JsonArray premiumPayments = JsonPath.read(data, "$.society.products[*].premiumPayment");
		for(JsonElement premiumPaymentElement : premiumPayments) {
			JsonObject premiumPayment = premiumPaymentElement.getAsJsonObject();
			JsonArray premiumPaymentMethods = Optional.ofNullable(premiumPayment.getAsJsonArray("paymentMethods")).orElse(new JsonArray());
			if(premiumPaymentMethods.contains(new JsonPrimitive("OUTROS")) && premiumPayment.get("paymentMethodAdittionalInfo") == null) {
				throw error("data.society.products.premiumPayment.paymentMethodAdittionalInfo is required when data.society.products.premiumPayment.paymentMethods contains OUTROS");
			}
		}
	}
}
