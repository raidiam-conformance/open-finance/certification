package net.openid.conformance.extensions;

import org.springframework.http.ContentDisposition;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class BodyRepeatableHttpServletResponse extends HttpServletResponseWrapper {

	private final HttpServletResponse delegateResponse;
	private TeeOutputStream tee;
	private final BodyUpdate bodyUpdate;

	public BodyRepeatableHttpServletResponse(HttpServletResponse response, BodyUpdate bodyUpdate) {
		super(response);
		this.delegateResponse = response;
		this.bodyUpdate = bodyUpdate;
	}

	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		OutputStream outputStream = super.getOutputStream();
		tee = new TeeOutputStream((ServletOutputStream) outputStream);
		return tee;
	}

	public byte[] getCachedBody() {
		return tee.getTee().toByteArray();
	}

	public interface BodyUpdate {
		void updateBody(String filename, byte[] body);
	}

	private class TeeOutputStream extends ServletOutputStream {

		private final ServletOutputStream delegate;
		private final ByteArrayOutputStream tee = new ByteArrayOutputStream();

		TeeOutputStream(ServletOutputStream delegate) {
			this.delegate = delegate;
		}

		@Override
		public void write(int b) throws IOException {
			delegate.write(b);
			tee.write(b);
		}

		@Override
		public void close() throws IOException {
			delegate.close();
			tee.close();
			String header = BodyRepeatableHttpServletResponse.this.delegateResponse.getHeader("Content-Disposition");
			String filename = ContentDisposition.parse(header).getFilename();
			BodyRepeatableHttpServletResponse.this.bodyUpdate.updateBody(filename, tee.toByteArray());
		}

		public ByteArrayOutputStream getTee() {
			return tee;
		}

		@Override
		public boolean isReady() {
			return delegate.isReady();
		}

		@Override
		public void setWriteListener(WriteListener writeListener) {
			delegate.setWriteListener(writeListener);
		}
	}

}
