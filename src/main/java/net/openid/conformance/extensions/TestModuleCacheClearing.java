package net.openid.conformance.extensions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.TemporalAmount;

public class TestModuleCacheClearing {

	private static final TemporalAmount FIFTEEN_MINUTES = Duration.ofMinutes(15);
	private static final Logger LOG = LoggerFactory.getLogger(TestModuleCacheClearing.class);


	private TestModuleRepository testModuleRepository;

	public TestModuleCacheClearing(TestModuleRepository testModuleRepository) {
		this.testModuleRepository = testModuleRepository;
	}

	@Scheduled(fixedRateString = "300000")
	public void clearTestModuleCache(){
		LOG.info("Clearing test module cache");
		testModuleRepository.removeOlderThan(Instant.now().minus(FIFTEEN_MINUTES));
		LOG.info("Cleared test module cache");
	}

}
