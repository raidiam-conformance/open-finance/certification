package net.openid.conformance.extensions.yacs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class EndpointDecorationService {
	private static final Logger LOG = LoggerFactory.getLogger(EndpointDecorationService.class);

	private final Set<String> decoratedEndpoints;


	public EndpointDecorationService() {
		this.decoratedEndpoints = loadDecoratedEndpoints();
	}

	/**
	 * Loads and returns a set of decorated endpoints from the resources in the /swagger/ resource directory.
	 * Decorated endpoints are identified by the presence of curly braces in the path (e.g., /example/{id}).
	 * This method reads all files in the specified directory, parses them as YAML, and collects the paths
	 * that contain curly braces into a set.
	 *
	 * @return a set of decorated endpoint paths
	 */
	@SuppressWarnings("unchecked")
	private Set<String> loadDecoratedEndpoints() {
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		Set<String> decoratedEndpoints = new HashSet<>();
		try {
			Resource[] yamlResources = resolver.getResources("/swagger/**/*.*");
			for (Resource resource : yamlResources) {
				Map<String, Object> spec = new Yaml().load(resource.getInputStream());
				Map<String, String> paths = (Map<String, String>) spec.get("paths");
				paths.keySet().stream().filter(p -> p.contains("{") || p.contains("}")).forEach(decoratedEndpoints::add);
			}

			LOG.info("Loaded decorated endpoints: {}", decoratedEndpoints);
		} catch (IOException e) {
			LOG.error("Could not load decorated endpoints", e);
		}

		return decoratedEndpoints;
	}

	public String decorateEndpoint(String endpoint) {
		LOG.info("Decorating endpoint - {}", endpoint);

		// Remove any prefix before "/open-banking"
		int idx = endpoint.indexOf("/open-banking");
		if (idx > 0) {
			endpoint = endpoint.substring(idx);
			LOG.info("Stripped prefix; new endpoint is {}", endpoint);
		}

		for (String decoratedEndpoint : decoratedEndpoints) {
			String regex = decoratedEndpoint.replaceAll("\\{[a-zA-Z0-9\\-]+}", "[^/]+") + "$";

			if (endpoint.matches("^.*" + regex)) {
				String fullEndpoint = endpoint.replaceAll(regex, decoratedEndpoint);
				LOG.info("Endpoint decorated - {}", fullEndpoint);
				return fullEndpoint;
			}
		}

		LOG.info("Endpoint not decorated");
		return endpoint;
	}

}
