package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.Gson;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class DcrException extends RuntimeException {

	static final long serialVersionUID = 646784461L;

	private static DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;
	private UserReport report;

	public DcrException(Exception cause) {
		super(String.format("For Debugging Reasons, the internal failure message when registering the client was the following: %s", cause.getMessage()), cause);
	}

	public DcrException(String body) {
		super(String.format("For Debugging Reasons, the internal failure message when registering the client was the following: %s", body));
	}

	public DcrException(UserReport report) {
		this(new Gson().toJson(report));
		this.report = report;
	}

	@Override
	public String getMessage() {
		if(report == null) {
			return super.getMessage();
		}
		List<String> responseHeaders = new ArrayList<>();
		List<String> requestHeaders = new ArrayList<>();
		report.responseHeaders.forEach((key, value) -> responseHeaders.add(String.format("%s:  %s", key, value)));
		report.requestHeaders.forEach((key, value) -> requestHeaders.add(String.format("%s:  %s", key, value)));


		Map<String, Object> fields = Map.of(
			"title", "The following information was reported",
			"timestamp", report.timestamp,
			"statusCode", report.status.value(),
			"status", report.status.getReasonPhrase(),
			"endpoint", report.endpoint,
			"responseHeaders", responseHeaders,
			"responseBody", report.responseBody,
			"requestHeaders", requestHeaders,
			"requestBody", report.requestBody
		);
		String json = new Gson().toJson(fields);
		return Base64.getEncoder().encodeToString(json.getBytes(StandardCharsets.UTF_8));
	}


	public static class UserReport implements Serializable {

		static final long serialVersionUID = 589594977L;

		private String timestamp;
		private String endpoint;
		private HttpStatus status;
		private HttpHeaders responseHeaders;
		private Map<String, Object> responseBody;
		private HttpHeaders requestHeaders;
		private Map<String, Object> requestBody;

		public HttpHeaders getRequestHeaders() {
			return requestHeaders;
		}

		public void setRequestHeaders(HttpHeaders requestHeaders) {
			this.requestHeaders = requestHeaders;
		}

		public Map<String, Object> getRequestBody() {
			return requestBody;
		}

		public void setRequestBody(Map<String, Object> requestBody) {
			this.requestBody = requestBody;
		}

		public UserReport() {
			timestamp = formatter.format(Instant.now());
		}

		public String getTimestamp() {
			return timestamp;
		}

		public String getEndpoint() {
			return endpoint;
		}

		public void setEndpoint(String endpoint) {
			this.endpoint = endpoint;
		}

		public HttpStatus getStatus() {
			return status;
		}

		public void setStatus(HttpStatus status) {
			this.status = status;
		}

		public HttpHeaders getResponseHeaders() {
			return responseHeaders;
		}

		public void setResponseHeaders(HttpHeaders headers) {
			this.responseHeaders = headers;
		}

		public Map<String, Object> getResponseBody() {
			return responseBody;
		}

		public void setResponseBody(Map<String, Object> body) {
			this.responseBody = body;
		}
	}

}
