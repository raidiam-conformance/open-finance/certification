package net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers;

import com.google.gson.JsonObject;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.http.client.methods.HttpRequestWrapper;
import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.http.HttpMethod;

import java.net.URI;
import java.util.List;
import java.util.Optional;


public class PostRiskSignalsHandler extends BaseHandler {


	public PostRiskSignalsHandler(ServerInfoTemplate serverInfoTemplate) {
		super(serverInfoTemplate);
	}

	@Override
	public List<String> getDecoratedEndpoints() {
		return List.of(
			"/open-banking/enrollments/v1/enrollments/{enrollmentId}/risk-signals",
			"/open-banking/enrollments/v2/enrollments/{enrollmentId}/risk-signals"
		);
	}



	@Override
	public HttpMethod getMethod() {
		return HttpMethod.POST;
	}


	@Override
	public void handle(JsonObject additionalInfo, HttpRequestWrapper request) {
		Optional<JsonObject> requestBody = extractRequestBody(request);

		HttpUriRequest actualRequest = (HttpUriRequest) request.getOriginal();
		URI uri = actualRequest.getURI();
		String url = uri.toString();

		addRiskSignalsEnumList(additionalInfo, requestBody);
		addEnrollmentIdFromUri(additionalInfo,url);
		addErrorCodes(additionalInfo);
		addClientIp(additionalInfo);
	}
}
