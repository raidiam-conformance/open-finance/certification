package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import net.openid.conformance.security.OIDCAuthenticationFacade;
import net.openid.conformance.testmodule.OIDFJSON;
import org.mitre.openid.connect.client.OIDCAuthoritiesMapper;
import org.mitre.openid.connect.client.SubjectIssuerGrantedAuthority;
import org.mitre.openid.connect.model.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.text.ParseException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class AuthDomainRoleAwareOBBAuthoritiesMapper implements OIDCAuthoritiesMapper {

	public static final SimpleGrantedAuthority CERTIFICATION_MANAGER = new SimpleGrantedAuthority("ROLE_CERTIFICATION_MANAGER");
	private static final String USER_DOMAIN_ROLE = "PFVPC";
	private static final String CERTIFICATION_MANAGER_CLAIM = "certification_manager";

	private static final Logger logger = LoggerFactory.getLogger(AuthDomainRoleAwareOBBAuthoritiesMapper.class);

	private String ADMIN_GROUP;

	private String ADMIN_ISSUER;

	private boolean devmode;

	public AuthDomainRoleAwareOBBAuthoritiesMapper(String adminGroup, String adminIss, boolean devmode) {

		this.ADMIN_GROUP = adminGroup;
		this.ADMIN_ISSUER = adminIss;
		this.devmode = devmode;
	}



	@Override
	public Collection<? extends GrantedAuthority> mapAuthorities(JWT idToken, UserInfo userInfo) {

		Set<GrantedAuthority> out = new HashSet<>();
		if(devmode) {
			out.add(OIDCAuthenticationFacade.ROLE_ADMIN);
			out.add(OIDCAuthenticationFacade.ROLE_USER);
			return out;
		}

		try {
			JWTClaimsSet claims = idToken.getJWTClaimsSet();
			SubjectIssuerGrantedAuthority authority = new SubjectIssuerGrantedAuthority(claims.getSubject(), claims.getIssuer());
			out.add(authority);
			JsonObject idTokenClaims = JsonParser.parseString(claims.toString()).getAsJsonObject();
			if (claims.getIssuer().equalsIgnoreCase(ADMIN_ISSUER))
			{
				JsonElement groupsEl = null;
				if (userInfo != null) {
					groupsEl = userInfo.getSource().get("groups");
				}
				if (groupsEl == null) {
					// if not in userinfo, check in id_token instead (azure can only put it here)

					groupsEl = idTokenClaims.get("groups");
				}
				if (groupsEl != null && groupsEl.isJsonArray()) {
					JsonArray groups = (JsonArray) groupsEl;
					JsonElement adminGroup = new JsonPrimitive(ADMIN_GROUP);
					if (groups.contains(adminGroup)) {
						out.add(OIDCAuthenticationFacade.ROLE_ADMIN);
					}
				}
			}
			try {
				validateIfPermitted(out, idTokenClaims);
			} catch (ObjectNotFoundException e) {
				logger.info("Unable to parse trust framework claims");
			}
		} catch (ParseException e) {
			logger.error("Unable to parse ID Token inside of authorities mapper", e);
		}
		return out;
	}

	private void validateIfPermitted(Set<GrantedAuthority> out, JsonObject claims) throws ParseException, ObjectNotFoundException {
		JsonObject trustFrameworkProfile = claims.getAsJsonObject("trust_framework_profile");
		if(trustFrameworkProfile == null) {
			return;
		}
		JsonElement conformanceManagerClaim = trustFrameworkProfile.get(CERTIFICATION_MANAGER_CLAIM);
		if(conformanceManagerClaim != null) {
			boolean conformanceManager = OIDFJSON.getBoolean(conformanceManagerClaim);
			if(conformanceManager) {
				out.add(OIDCAuthenticationFacade.ROLE_USER);
				out.add(CERTIFICATION_MANAGER);
				out.add(new TOCGrantedAuthority());
				return;
			}
		}

		JsonObject object = trustFrameworkProfile;
		object = getObject("org_access_details", object);
		for(String orgId: object.keySet()) {
			JsonObject org = getObject(orgId, object);
			JsonObject domainRoleDetails = org.getAsJsonObject("domain_role_details");
			if(domainRoleDetails != null) {
				for(String roleNumber: domainRoleDetails.keySet()) {
					JsonObject detail = domainRoleDetails.getAsJsonObject(roleNumber);
					if(detail == null) {
						continue;
					}
					JsonElement role = detail.get("contact_role");
					JsonElement status = detail.get("status");
					if(role == null || status == null) {
						continue;
					}
					if(!OIDFJSON.getString(status).equals("Active")) {
						continue;
					}
					if(!OIDFJSON.getString(role).equals(USER_DOMAIN_ROLE)) {
						continue;
					}
					out.add(OIDCAuthenticationFacade.ROLE_USER);
					out.add(new TOCGrantedAuthority());
				}
			}
		}
	}

	private JsonObject getObject(String key, JsonObject object) throws ObjectNotFoundException {
		JsonObject found = object.getAsJsonObject(key);
		if(found == null) {
			throw new ObjectNotFoundException();
		}
		return found;
	}

	private static class ObjectNotFoundException extends Exception {
		static final long serialVersionUID = 2830849484L;
	}

}
