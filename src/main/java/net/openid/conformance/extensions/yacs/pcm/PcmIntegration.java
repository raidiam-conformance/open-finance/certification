package net.openid.conformance.extensions.yacs.pcm;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor;

import java.util.List;

public class PcmIntegration {

	private static final Logger LOG = LoggerFactory.getLogger(PcmIntegration.class);

	@Autowired
	private ScheduledAnnotationBeanPostProcessor scheduledTaskPostProcessor;

	private final PcmRepository repository;
	private final PcmClient client;
	private int failedCommsThreshold = 25;

	public PcmIntegration(PcmRepository repository, PcmClient client) {
		this.repository = repository;
		this.client = client;
	}

	@Scheduled(cron = "#{@environment.getProperty('pcm.cron', '0 0 * * * *')}", zone="America/Sao_Paulo")
	public void purge() {
		LOG.info("Forwarding pcm events");
		Iterable<PcmEvent> events = repository.findAll();
		List<PcmEvent> pcmEvents = Lists.newArrayList(events);
		for(PcmEvent event: pcmEvents) {
			try {
				if(client.sendEvent(event)) {
					repository.delete(event);
				}
			} catch (PcmClient.PcmCommsFailedException e) {
				switch(failedCommsThreshold) {
					case 0:
						LOG.error("PCM comms failed too many times - disabling scheduled task");
						scheduledTaskPostProcessor.postProcessBeforeDestruction(this, "pcmIntegration");
						break;
					case 1:
						LOG.error("PCM comms failed - will try once more");
						failedCommsThreshold--;
						break;
					default:
						LOG.error("PCM comms failed - will try {} more times", failedCommsThreshold--);
						break;
				}
				break;
			}
		}
	}

}
