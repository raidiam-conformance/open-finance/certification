package net.openid.conformance.extensions.yacs;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractYACSFilter implements Filter {

	private final RequestMatch match;

	protected AbstractYACSFilter() {
		this(RequestMatch.MATCH_ANY);
	}

	protected AbstractYACSFilter(RequestMatch match) {
		this.match = match;
	}

	@Override
	public final void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		if(!match.matches((HttpServletRequest) servletRequest)) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}
		BodyRepeatableHttpServletRequest repeatableBodyRequest = null;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		if(servletRequest instanceof BodyRepeatableHttpServletRequest) {
			repeatableBodyRequest = (BodyRepeatableHttpServletRequest) servletRequest;
		} else {
			repeatableBodyRequest = new BodyRepeatableHttpServletRequest(servletRequest);
		}
		boolean proceed = processRequest(repeatableBodyRequest, response);
		if(proceed) {
			filterChain.doFilter(repeatableBodyRequest, servletResponse);
		}
	}

	public abstract boolean processRequest(BodyRepeatableHttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;

}
