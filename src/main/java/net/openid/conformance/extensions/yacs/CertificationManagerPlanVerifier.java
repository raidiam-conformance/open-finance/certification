package net.openid.conformance.extensions.yacs;

import net.openid.conformance.security.AuthenticationFacade;

import java.util.Collections;
import java.util.List;

public class CertificationManagerPlanVerifier implements EnhancedVerification {

	private final AuthenticationFacade authenticationFacade;
	private final boolean devmode;

	private List<String> certManagerOnlyPlans;

	public CertificationManagerPlanVerifier(AuthenticationFacade authenticationFacade, CertificationManagerPlans certificationManagerPlans) {
		this(authenticationFacade, certificationManagerPlans, false);
	}

	public CertificationManagerPlanVerifier(AuthenticationFacade authenticationFacade, CertificationManagerPlans certificationManagerPlans,  boolean devmode) {
		this.authenticationFacade = authenticationFacade;
		this.certManagerOnlyPlans = certificationManagerPlans.getPlans();
		if(certManagerOnlyPlans==null) {
			certManagerOnlyPlans = Collections.emptyList();
		}
		this.devmode = devmode;
	}

	@Override
	public VerificationResult allowed(BodyRepeatableHttpServletRequest request) {
		if(devmode) {
			return VerificationResult.passed();
		}
		if(authenticationFacade.getContextAuthentication().getAuthorities().contains(AuthDomainRoleAwareOBBAuthoritiesMapper.CERTIFICATION_MANAGER)) {
			return VerificationResult.passed();
		}
		String planName = request.getParameter("planName");
		boolean allowed = !certManagerOnlyPlans.contains(planName);
		return allowed ? VerificationResult.passed() : VerificationResult.certManagerOnly();
	}
}
