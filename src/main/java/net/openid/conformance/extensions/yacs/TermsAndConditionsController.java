package net.openid.conformance.extensions.yacs;

import net.openid.conformance.ui.ServerInfoTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Optional;

@Controller
public class TermsAndConditionsController {

	private static Map<String, String> BRANDED_TERMS = Map.of(
		"openfinancebrazil", "yacs/yacsTermsAndConditions.html",
		"openfinancebrasil-fvp", "yacs/yacsTermsAndConditions.html",
		"openinsurancebrazil", "yacs/yaoicsTermsAndConditions.html",
		"openinsurancebrazil-fvp", "yacs/yaoicsTermsAndConditions.html"
	);

	@Autowired
	private ServerInfoTemplate serverInfoTemplate;

	@RequestMapping(value = "/toc.html")
	public String agreeToc(Model model) {
		String brand = serverInfoTemplate.getServerInfo().get("brand")
			.toLowerCase()
			.replaceAll("\\W", "");

		model.addAttribute("brand", brand);

		String brandedTermsAndConditions = BRANDED_TERMS.getOrDefault(brand, "yacs/yacsTermsAndConditions.html");

		return brandedTermsAndConditions;
	}

	@GetMapping("/toc/agree")
	public ResponseEntity<?> agreed(HttpServletRequest request, @NotNull Authentication authentication) {
		Optional<? extends TOCGrantedAuthority> tocGrant = authentication.getAuthorities()
			.stream()
			.filter(g -> g instanceof TOCGrantedAuthority)
			.findAny()
			.map(a ->  (TOCGrantedAuthority) a);
		tocGrant.ifPresent(g -> g.setAccepted(true));
		HttpSession session = request.getSession();
		String targetUrl = String.valueOf(session.getAttribute(TermsAndConditionsAuthenticationFilter.PRE_TOC_URI_KEY));
		return ResponseEntity.status(HttpStatus.TEMPORARY_REDIRECT).header("location", targetUrl).build();
	}

}
