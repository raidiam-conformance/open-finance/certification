package net.openid.conformance.extensions.yacs;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nimbusds.jwt.JWT;
import net.openid.conformance.CollapsingGsonHttpMessageConverter;
import net.openid.conformance.extensions.AuthenticatedUserRepository;
import net.openid.conformance.extensions.CachedAuthenticatedUser;
import net.openid.conformance.extensions.EndpointLimits;
import net.openid.conformance.info.Plan;
import net.openid.conformance.info.PlanRepository;
import net.openid.conformance.info.PublicPlan;
import net.openid.conformance.info.TestPlanService;
import net.openid.conformance.pagination.PaginationRequest;
import net.openid.conformance.pagination.PaginationResponse;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.variant.VariantSelection;
import org.bson.Document;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import software.amazon.awssdk.services.kms.model.InvalidCiphertextException;

import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class YACSDBTestPlanService implements TestPlanService {
	@Autowired
	private PlanRepository plans;

	@Autowired
	private AuthenticationFacade authenticationFacade;

	@Autowired
	private YACSEncryptAndDecryptOperations yacsEncryptAndDecryptOperations;

	private final AuthenticatedUserRepository authenticatedUserRepository;

	private static final Logger LOG = LoggerFactory.getLogger(YACSDBTestPlanService.class);

	private final TestPlanService delegate;

	private Gson gson = CollapsingGsonHttpMessageConverter.getDbObjectCollapsingGson();

	public YACSDBTestPlanService(AuthenticatedUserRepository authenticatedUserRepository, TestPlanService delegate) {
		this.authenticatedUserRepository = authenticatedUserRepository;
		this.delegate = delegate;
	}

	/**
	 * @param planId
	 * @param testName
	 * @param variant
	 * @param id
	 */
	@Override
	public void updateTestPlanWithModule(String planId, String testName, VariantSelection variant, String id) {
		delegate.updateTestPlanWithModule(planId,testName,variant,id);
	}

	/* (non-Javadoc)
	 * @see TestPlanService#createTestPlan(java.lang.String, java.lang.String, com.google.gson.JsonObject, java.util.Map, TestPlan)
	 */
	@Override
	public void createTestPlan(String id, String planName, VariantSelection variant, JsonObject config, String description, String certificationProfileName, List<Plan.Module> testModules, String summary, String publish) {
		Authentication contextAuthentication = authenticationFacade.getContextAuthentication();
		if(!contextAuthentication.getClass().isAssignableFrom(OIDCAuthenticationToken.class)) {
			LOG.error("Logged in authentication is not OIDC token");
			throw new AccessDeniedException("Logged in authentication is not OIDC token");
		}
		OIDCAuthenticationToken oidcAuth = (OIDCAuthenticationToken) contextAuthentication;
		JWT idToken = oidcAuth.getIdToken();
		try {
			Object user = idToken.getJWTClaimsSet().getClaim("sub");
			Object issuer = idToken.getJWTClaimsSet().getClaim("iss");
			CachedAuthenticatedUser cachedAuthenticatedUser = new CachedAuthenticatedUser(user.toString(), issuer.toString());
			authenticatedUserRepository.incrementEndpointCalled(cachedAuthenticatedUser, EndpointLimits.CREATE_TEST_PLAN);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		delegate.createTestPlan(id,planName,variant,config,description,certificationProfileName,testModules,summary,publish);
	}
	/* (non-Javadoc)
	 * @see TestPlanService#getTestPlan(java.lang.String)
	 */
	@Override
	public Plan getTestPlan(String id) {

		if (!authenticationFacade.isAdmin()) {
			var plan = plans.findByIdAndOwner(id, authenticationFacade.getPrincipal()).orElse(null);
			if (plan != null){
				plan = getAndDecryptConfig(plan);
			}
			return plan;
		} else {
			var plan = plans.findById(id).orElse(null);
			if (plan != null){
				plan = getAndDecryptConfig(plan);
			}
			return plan;
		}
	}

	private Plan getAndDecryptConfig(Plan plan) {
		Gson gson = CollapsingGsonHttpMessageConverter.getDbObjectCollapsingGson();
		JsonObject testPlanObj = JsonParser.parseString(gson.toJson(plan)).getAsJsonObject();
		JsonObject newConfig;
		try {
			newConfig = yacsEncryptAndDecryptOperations.decryptConfig(testPlanObj.getAsJsonObject("config"));
		} catch (InvalidCiphertextException | IllegalArgumentException e){
			newConfig = testPlanObj.getAsJsonObject("config");
		}

		return new Plan(plan.getId(),plan.getPlanName(),plan.getVariant(),newConfig,Instant.parse(plan.getStarted()),plan.getOwner(),plan.getDescription(),plan.getCertificationProfileName(),plan.getModules(),plan.getVersion(),plan.getSummary(),plan.getPublish());
	}

	/* (non-Javadoc)
	 * @see TestPlanService#getPublicPlan(java.lang.String)
	 */
	@Override
	public PublicPlan getPublicPlan(String id) {
		return delegate.getPublicPlan(id);
	}

	@Override
	public JsonObject getModuleConfig(String planId, String moduleName) {
		Plan testPlan = getTestPlan(planId);

		List<Plan.Module> modules = testPlan.getModules();

		boolean found = false;

		for (Plan.Module module : modules)
		{
			if (module.getTestModule().equals(moduleName)) {
				found = true;
			}
		}

		if (!found) {
			// the user has asked to create a module that isn't part of the plan
			return null;
		}

		Document dbConfig = testPlan.getConfig();

		String json = gson.toJson(dbConfig);

		JsonObject config = JsonParser.parseString(json).getAsJsonObject();

		if (config.has("override")) {
			JsonObject override = config.getAsJsonObject("override");
			config.remove("override");
			if (override.has(moduleName)) {
				// Move all the overridden elements up into the configuration
				JsonObject overrides = override.getAsJsonObject(moduleName);
				for (Map.Entry<String, JsonElement> entry : overrides.entrySet()) {
					config.add(entry.getKey(), entry.getValue());
				}
			}
		}

		return config;
	}

	/* (non-Javadoc)
	 * @see TestPlanService#getPaginatedPlansForCurrentUser()
	 */
	@Override
	public PaginationResponse<Plan> getPaginatedPlansForCurrentUser(PaginationRequest page) {
		PaginationResponse<Plan> planList;
		if (!authenticationFacade.isAdmin()) {
			Map<String, String> owner = authenticationFacade.getPrincipal();
			planList = page.getResponse(
				p -> plans.findAllByOwner(owner, p),
				(s, p) -> plans.findAllByOwnerSearch(owner, s, p));
		} else {
			planList = page.getResponse(
				p -> plans.findAll(p),
				(s, p) -> plans.findAllSearch(s, p));
		}
		List<Plan> newPlanList = new ArrayList<>();

			planList.data.forEach(plan -> {
				newPlanList.add(getAndDecryptConfig(plan));
			});
			List<Plan> itemsToRemove = planList.data;
			planList.data.removeAll(itemsToRemove);
			planList.data.addAll(newPlanList);


		return planList;
	}

	/* (non-Javadoc)
	 * @see TestPlanService#getPaginatedPublicPlans()
	 */
	@Override
	public PaginationResponse<PublicPlan> getPaginatedPublicPlans(PaginationRequest page) {
		return delegate.getPaginatedPublicPlans(page);
	}

	/*
	 * (non-Javadoc)
	 * @see TestPlanService#publishTestPlan(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean publishTestPlan(String id, String publish) {
		return delegate.publishTestPlan(id,publish);
	}

	@Override
	public boolean changeTestPlanImmutableStatus(String id, Boolean immutable) {
		return delegate.changeTestPlanImmutableStatus(id,immutable);
	}

	@Override
	public VariantSelection getTestPlanVariant(String planId) {
		return delegate.getTestPlanVariant(planId);
	}

	@Override
	public void createIndexes(){
		delegate.createIndexes();
	}

	@Override
	public void deleteMutableTestPlan(String id) {
			delegate.deleteMutableTestPlan(id);
	}
}
