package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

public class WebhookUrisAdder implements DcrRequestProcessor{
	private final List<String> webhookUris;

	public WebhookUrisAdder(List<String> webhookUris) {
		this.webhookUris = webhookUris;
	}

	public WebhookUrisAdder(String webhookUris) {
		this(List.of(webhookUris));
	}

	@Override
	public void process(JsonObject jsonObject) {
		JsonArray webhookUriArray = new JsonArray();
		webhookUris.stream()
			.forEach(it -> webhookUriArray.add(it));
		jsonObject.add("webhook_uris" ,webhookUriArray);
	}
}
