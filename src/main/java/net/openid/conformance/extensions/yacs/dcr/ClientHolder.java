package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.Instant;

public class ClientHolder {

    private Instant createdAt = Instant.now();
    private JsonObject clientConfig;

    private boolean irremovable = false;

    private int deleteRetryCount = 0;

    public ClientHolder(JsonObject clientConfig) {
        this.clientConfig = clientConfig;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public JsonObject getClientConfig() {
        return clientConfig;
    }

    public JsonObject getDynamicClient() {
        return clientConfig.getAsJsonObject("client");
    }

    public String getOrgId() {
        JsonObject additionalInfo = clientConfig.getAsJsonObject("additionalInfo");
        return OIDFJSON.getString(additionalInfo.get("orgId"));
    }

    public boolean isIrremovable() {
        return irremovable;
    }

    public void incrementRetryCount() {
        this.deleteRetryCount++;
    }

    public int getDeleteRetryCount() {
        return deleteRetryCount;
    }

    public void setIrremovable(boolean irremovable) {
        this.irremovable = irremovable;
    }
}
