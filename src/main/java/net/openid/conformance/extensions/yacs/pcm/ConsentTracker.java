package net.openid.conformance.extensions.yacs.pcm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConsentTracker {

	private Map<String, List<TrackedConsent>> consents = new HashMap<>();

	public List<TrackedConsent> getConsentsForTest(String testId) {
		List<TrackedConsent> trackedConsents = consents.get(testId);
		if(trackedConsents == null) {
			trackedConsents = new ArrayList<>();
			consents.put(testId, trackedConsents);
		}
		return trackedConsents;
	}
}
