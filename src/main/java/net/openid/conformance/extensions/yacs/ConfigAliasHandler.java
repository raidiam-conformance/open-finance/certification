package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConfigAliasHandler {

	public void handle(JsonObject config, Set<Map> orgs, String wellKnown) {
		JsonElement configuredAlias = config.get("alias");
		if(configuredAlias == null) {
			inferAlias(config, orgs, wellKnown);
			return;
		}
		String aliasRequested = OIDFJSON.getString(configuredAlias);
		boolean allowed = verifyAliasAllowed(aliasRequested, config, orgs, wellKnown);
		if(!allowed) {
			throw new RuntimeException("nppe");
		}
	}

	private boolean verifyAliasAllowed(String aliasRequested, JsonObject config, Set<Map> orgs, String wellKnown) {
		for(Map org: orgs) {
			String orgId = String.valueOf(org.get("OrganisationId"));
			if(!orgId.equals(aliasRequested)) {
				continue;
			}
			List<Map> authServersForOrg = (List<Map>) org.get("AuthorisationServers");
			for(Map authServer: authServersForOrg) {
				String oidcDiscovery = String.valueOf(authServer.get("OpenIDDiscoveryDocument"));
				if(oidcDiscovery.equals(wellKnown)) {
					insertOrgId(config, orgId);
					return true;
				}
			}
		}
		return false;
	}

	private void inferAlias(JsonObject config, Set<Map> orgs, String wellKnown) {
		for(Map org: orgs) {
			String orgId = String.valueOf(org.get("OrganisationId"));
			List<Map> authServersForOrg = (List<Map>) org.get("AuthorisationServers");
			for(Map authServer: authServersForOrg) {
				String oidcDiscovery = String.valueOf(authServer.get("OpenIDDiscoveryDocument"));
				if(oidcDiscovery.equals(wellKnown)) {
					config.addProperty("alias", orgId);
					insertOrgId(config, orgId);
					return;
				}
			}
		}
	}

	private void insertOrgId(JsonObject config, String organisationId){
		if (config.keySet().contains("resource")){
			JsonObject resource = config.getAsJsonObject("resource");
			resource.addProperty("brazilOrganizationId",organisationId);
		} else {
			JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder();
			jsonObjectBuilder.addField("brazilOrganizationId",organisationId);

			JsonObject resource = jsonObjectBuilder.build();
			config.add("resource",resource);
		}
	}

}
