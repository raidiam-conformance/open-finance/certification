package net.openid.conformance.extensions.yacs.pcm;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.Instant;

public class PcmToken {

	private JsonObject token;
	private Instant expiresAt;

	public PcmToken(JsonObject tokenObj) {
		this.token = tokenObj;
		int exp = OIDFJSON.getInt(token.get("expires_in"));
		Instant now = Instant.now();
		expiresAt = now.plusSeconds(exp);
	}

	public boolean isExpired() {
		return Instant.now().isAfter(expiresAt);
	}

	public JsonObject getRawToken() {
		return token;
	}

	public String getAccessToken() {
		return OIDFJSON.getString(token.get("access_token"));
	}

	public String toBearerToken() {
		return String.format("Bearer %s", getAccessToken());
	}

}
