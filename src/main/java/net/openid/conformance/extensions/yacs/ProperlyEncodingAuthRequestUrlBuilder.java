package net.openid.conformance.extensions.yacs;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import org.mitre.oauth2.model.RegisteredClient;
import org.mitre.openid.connect.client.service.AuthRequestUrlBuilder;
import org.mitre.openid.connect.config.ServerConfiguration;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

public class ProperlyEncodingAuthRequestUrlBuilder implements AuthRequestUrlBuilder {
	@Override
	public String buildAuthRequestUrl(ServerConfiguration serverConfig, RegisteredClient clientConfig, String redirectUri, String nonce, String state, Map<String, String> options, String loginHint) {

		UriComponentsBuilder ucb = UriComponentsBuilder.fromUriString(serverConfig.getAuthorizationEndpointUri())

		.queryParam("response_type", "code")
		.queryParam("client_id", clientConfig.getClientId())
		.queryParam("scope", Joiner.on(" ").join(clientConfig.getScope()))
		.queryParam("redirect_uri", redirectUri)
		.queryParam("nonce", nonce)
		.queryParam("state", state);

		for (Map.Entry<String, String> option : options.entrySet()) {
			ucb.queryParam(option.getKey(), option.getValue());
		}
		if (!Strings.isNullOrEmpty(loginHint)) {
			if(loginHint.startsWith("acct:")) {
				ucb.queryParam("login_hint", loginHint.substring(5));
			} else {
				ucb.queryParam("login_hint", loginHint);
			}
		}

		return ucb.toUriString();

	}
}
