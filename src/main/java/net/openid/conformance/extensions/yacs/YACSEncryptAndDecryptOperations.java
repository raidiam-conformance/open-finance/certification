package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.extensions.YACSKmsOperations;
import net.openid.conformance.testmodule.OIDFJSON;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.DecryptRequest;
import software.amazon.awssdk.services.kms.model.DecryptResponse;
import software.amazon.awssdk.services.kms.model.GetPublicKeyRequest;
import software.amazon.awssdk.services.kms.model.GetPublicKeyResponse;

import javax.crypto.Cipher;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Map;

public class YACSEncryptAndDecryptOperations {

	private interface Operation {
		JsonElement perform(JsonElement value);
	}

	private final YACSKmsOperations kmsOperations;

	private final KmsClient kmsClient;

	private final String kmsKeyId;

	public YACSEncryptAndDecryptOperations(YACSKmsOperations kmsOperations, KmsClient kmsClient, String kmsKeyId) {
		this.kmsOperations = kmsOperations;
		this.kmsClient = kmsClient;
		this.kmsKeyId = kmsKeyId;
	}


	public JsonObject decryptConfig(JsonObject config) {
		return processJsonElement(config, 0, null, null, this::decryptJsonValue).getAsJsonObject();
	}


	public JsonObject encryptConfig(JsonObject config) {
		return processJsonElement(config, 0, null, null, this::encryptJsonValue).getAsJsonObject();
	}


	private JsonElement processJsonElement(JsonElement element, int breakPoint, JsonElement parent, String key, Operation operation) {
		if (element == null) {
			return null;
		}

		if (element.isJsonObject()) {
			for (Map.Entry<String, JsonElement> entry : element.getAsJsonObject().entrySet()) {
				processJsonElement(entry.getValue(), breakPoint + 1, element, entry.getKey(), operation);
			}

		} else if (element.isJsonArray()) {
			JsonArray array = new JsonArray();
			for (JsonElement e : element.getAsJsonArray()) {
				processJsonElement(e, breakPoint + 1, array, null, operation);
			}
			element = array;

		} else {
			element = operation.perform(element);
		}

		if (parent == null) {
			return element;
		}

		if (parent.isJsonObject()) {
			parent.getAsJsonObject().add(key, element);
		} else if (parent.isJsonArray()) {
			parent.getAsJsonArray().add(element);
		}

		return element;
	}


	private JsonElement encryptJsonValue(JsonElement value) {
		try {
			String cipherText = encryptValue(OIDFJSON.getString(value), kmsKeyId);
			return new JsonPrimitive(cipherText);
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("Unable to encrypt JSON value", e);
		}
	}

	private String encryptValue(String value, String kmsKeyId) throws GeneralSecurityException {
		GetPublicKeyRequest getPublicKeyRequest = GetPublicKeyRequest.builder().keyId(kmsKeyId).build();
		GetPublicKeyResponse getPublicKeyResponse = kmsOperations.getPublicKey(kmsClient, getPublicKeyRequest);

		PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(getPublicKeyResponse.publicKey().asByteArray()));
		AlgorithmParameters parameters = AlgorithmParameters.getInstance("OAEP", Security.getProvider("BC"));
		AlgorithmParameterSpec specification = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT);
		parameters.init(specification);
		Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding", Security.getProvider("BC"));
		cipher.init(Cipher.ENCRYPT_MODE, publicKey, parameters);

		return Base64.getEncoder().encodeToString(cipher.doFinal(value.getBytes()));
	}

	private JsonElement decryptJsonValue(JsonElement value) {
		DecryptResponse response = decryptValue(OIDFJSON.getString(value), kmsKeyId);
		return new JsonPrimitive(response.plaintext().asString(StandardCharsets.UTF_8));
	}

	private DecryptResponse decryptValue(String value, String kmsKeyId) {
		DecryptRequest decryptRequest = DecryptRequest.builder().encryptionAlgorithm("RSAES_OAEP_SHA_256").keyId(kmsKeyId).ciphertextBlob(SdkBytes.fromByteArray(Base64.getDecoder().decode(value.getBytes()))).build();

		return kmsOperations.decrypt(kmsClient, decryptRequest);
	}
}
