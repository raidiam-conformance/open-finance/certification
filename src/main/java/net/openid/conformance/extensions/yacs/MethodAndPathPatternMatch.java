package net.openid.conformance.extensions.yacs;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

public class MethodAndPathPatternMatch implements RequestMatch {

	private final String methodToMatch;
	private final Pattern pattern;

	public MethodAndPathPatternMatch(String method, String pathPattern) {
		this.methodToMatch = method;
		this.pattern = Pattern.compile(pathPattern);

	}

	@Override
	public boolean matches(HttpServletRequest request) {
		String method = request.getMethod();
		String path = request.getServletPath();
		return methodToMatch.equals(method) && pattern.matcher(path).matches();
	}
}
