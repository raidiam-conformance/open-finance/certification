package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import net.openid.conformance.extensions.yacs.CachedDiscoveryDocumentService;
import net.openid.conformance.extensions.yacs.pcm.PcmEvent;
import net.openid.conformance.extensions.yacs.pcm.PcmRepository;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.variant.ClientAuthType;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;

public class DcrHelper {
	private final Logger logger = LoggerFactory.getLogger(DcrHelper.class);
	private final DirectoryCredentials directoryCredentials;
	private final DirectoryDetails directoryDetails;
	private final PcmRepository pcmRepository;
	private final CachedDiscoveryDocumentService cachedDiscoveryDocumentService;
	private RestTemplate restTemplate;

	private RestTemplate restTemplateNoErrorHandler;

	private DcrClientRepository clientRepository;

	public DcrHelper(DirectoryCredentials directoryCredentials,
					 DirectoryDetails directoryDetails, DcrClientRepository dcrClientRepository, PcmRepository pcmRepository, CachedDiscoveryDocumentService cachedDiscoveryDocumentService) {
		this.directoryCredentials = directoryCredentials;
		this.directoryDetails = directoryDetails;
		this.clientRepository = dcrClientRepository;
		this.pcmRepository = pcmRepository;
		this.cachedDiscoveryDocumentService = cachedDiscoveryDocumentService;
		HttpClient httpClient = null;
		try {
			httpClient = createHttpClient(directoryCredentials);
		} catch (Exception e) {
			logger.error("Failed to create http client");
			throw new RuntimeException(e);
		}
		restTemplate = createRestTemplate(false, httpClient);
		restTemplateNoErrorHandler = createRestTemplate(true, httpClient);

	}

	private RestTemplate createRestTemplate(boolean disableErrorHandler, HttpClient httpClient){
		RestTemplate newRestTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient));

		if (disableErrorHandler){
			newRestTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public boolean hasError(ClientHttpResponse response) throws IOException {
					return false;
				}
			});
		}

		return newRestTemplate;
	}

	public JsonObject registerClient(String authServerDisvoveryUri, ClientAuthType clientAuthType, String redirect, String webhookUri, String orgId) {
		JsonObject response = new JsonObject();
		JsonObject jwks = directoryCredentials.signingJwks();
		JsonObject credentials = new JsonObjectBuilder()
			.addField("jwks", jwks)
			.addField("clientKey", directoryCredentials.clientKeyData())
			.addField("clientCert", directoryCredentials.clientCertData())
			.addField("clientCa", directoryCredentials.clientCaData())
			.addField("directoryClientId", directoryCredentials.directoryClientId())
			.build();
		response.add("credentials", credentials);

		JsonObject directoryDiscoveryDocument = getDiscoveryDocument(directoryDetails.getDiscoveryUri());
		JsonObject authServerDiscoveryDocument = getDiscoveryDocument(authServerDisvoveryUri, false);
		String tokenEndpoint = OIDFJSON.getString(directoryDiscoveryDocument.get("token_endpoint"));
		String dcrEndpoint = extractKeyIfPresentOnMtlsObject("registration_endpoint", authServerDiscoveryDocument);

		JsonObject token = getTokenForDcr(tokenEndpoint, directoryCredentials.directoryClientId(), "directory:software");
		String ss = getSoftwareStatement(token);
		JsonObject client = doDcr(ss, dcrEndpoint, clientAuthType, redirect, webhookUri, orgId);
		JsonObject additionalInfo = new JsonObject();
		additionalInfo.addProperty("orgId", orgId);
		response.add("client", client);
		response.add("additionalInfo", additionalInfo);
		return response;
	}

	public String extractKeyIfPresentOnMtlsObject(String key, JsonObject discoveryDocumentObject) {
		JsonObject mtlsObject = discoveryDocumentObject.getAsJsonObject("mtls_endpoint_aliases");
		if(mtlsObject != null && mtlsObject.has(key)) {
			logger.info("Found key {} on mtls object", key);
			return OIDFJSON.getString(mtlsObject.get(key));
		} else {
			logger.info("Found key {} on root object", key);
			return OIDFJSON.getString(discoveryDocumentObject.get(key));
		}
	}
	public void unregisterClient(ClientHolder savedClient, String testId) throws ClientDeletionException {
		logger.info("Performing DCR client deletion");
		String registrationClientUri = OIDFJSON.getString(savedClient.getDynamicClient().get("registration_client_uri"));
		String accessToken = OIDFJSON.getString(savedClient.getDynamicClient().get("registration_access_token"));
		HttpHeaders headers = new HttpHeaders();

		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setBearerAuth(accessToken);
		headers.add("x-fapi-interaction-id", UUID.randomUUID().toString());

		HttpEntity<String> request = new HttpEntity<>(null, headers);
		Instant start = Instant.now();
		ResponseEntity<String> res = restTemplateNoErrorHandler.exchange(registrationClientUri, HttpMethod.DELETE, request, String.class);

		JsonObject additionalInfo = new JsonObject();
		additionalInfo.addProperty("personType", "PF");
		Instant end = Instant.now();
		long duration = end.toEpochMilli() - start.toEpochMilli();
		String fapiInteractionId = res.getHeaders().getFirst("x-fapi-interaction-id");
		String orgId = savedClient.getOrgId();
		URI uri = URI.create(registrationClientUri);
		PcmEvent event = new PcmEvent(res.getStatusCode().value(), "POST",
			uri.getHost(), fapiInteractionId, uri.getPath(), directoryCredentials.directoryClientId(), directoryCredentials.orgId(),
			orgId, duration, "client", additionalInfo);

		emitPcmEvent(event);

		if(!res.getStatusCode().is2xxSuccessful() && !res.getStatusCode().is4xxClientError()) {
			logger.error("DCR client deletion failed for client at {}", registrationClientUri);
			clientRepository.incrementDeleteRetryCount(testId);
			throw new ClientDeletionException(registrationClientUri);
		} else if (res.getStatusCode().is4xxClientError()) {
			clientRepository.setClientIrremovable(testId, true);
			throw new ClientDeletionException(registrationClientUri);
		}
		logger.info("DCR client deletion successful");
	}

	private JsonObject getDiscoveryDocument(String directoryDiscoveryUri) {
		return getDiscoveryDocument(directoryDiscoveryUri, true);
	}

	private JsonObject getTokenForDcr(String tokenEndpoint, String clientId, String... scopes) {
		logger.info("Preparing to retrieve token for DCR");
		HttpHeaders headers = new HttpHeaders();

		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("x-fapi-interaction-id", UUID.randomUUID().toString());

		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
		requestBody.add("grant_type", "client_credentials");
		requestBody.add("scope", String.join(" ", scopes));
		requestBody.add("client_id", clientId);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(requestBody, headers);

		ResponseEntity<String> res = restTemplateNoErrorHandler.exchange(tokenEndpoint, HttpMethod.POST, request, String.class);

		if(!res.getStatusCode().is2xxSuccessful()) {
			logger.error("DCR token fetch failed for client at {}", tokenEndpoint);
			throw new RuntimeException("DCR token fetch failed for client");
		}

		String jsonString = res.getBody();

		logger.info("Token for DCR retrieved");

		return JsonParser.parseString(jsonString).getAsJsonObject();
	}

	private String getSoftwareStatement(JsonObject token) {
		logger.info("Preparing to retrieve software statement");
		HttpHeaders headers = new HttpHeaders();

		headers.set("accept", "application/jwt;charset=UTF-8");
		headers.setBearerAuth(OIDFJSON.getString(token.get("access_token")));

		HttpEntity<String> request = new HttpEntity<>(null, headers);

		String ssUri = directoryDetails.softwareStatementUri();

		ResponseEntity<String> response = restTemplate.exchange(ssUri, HttpMethod.GET, request, String.class);
		logger.info("Software statement retrieved");
		String ss = response.getBody();

		return ss;

	}

	private JsonObject doDcr(String softwareStatement, String registrationEndpoint, ClientAuthType authType, String redirectUri, String webhookUri, String orgId) {
		logger.info("Preparing to register client");
		JsonObject dcrRequest = new JsonObject();
		GrantTypeAdder.INSTANCE.process(dcrRequest);
		ResponseTypeAdder.INSTANCE.process(dcrRequest);
		new JwksUriAdder(directoryDetails).process(dcrRequest);
		new RedirectUrisAdder(redirectUri)
			.process(dcrRequest);
		new SoftwareStatementAdder(softwareStatement).process(dcrRequest);
		if (webhookUri != null){
			new WebhookUrisAdder(webhookUri).process(dcrRequest);
		}
		switch (authType) {
			case MTLS:
				new TlsClientAuthProcessor(directoryCredentials).process(dcrRequest);
				break;
			case PRIVATE_KEY_JWT:
				new PrivateKeyJWTAuthProcessor().process(dcrRequest);
				break;
			default:
				throw new RuntimeException("Unknown client auth type");
		}

		HttpHeaders requestHeaders = new HttpHeaders();

		requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.add("x-fapi-interaction-id", UUID.randomUUID().toString());

		Instant start = Instant.now();
		HttpEntity<String> request = new HttpEntity<>(dcrRequest.toString(), requestHeaders);
		ResponseEntity<String> res = restTemplateNoErrorHandler.exchange(registrationEndpoint, HttpMethod.POST, request, String.class);
		JsonObject additionalInfo = new JsonObject();
		additionalInfo.addProperty("personType", "PF");
		Instant end = Instant.now();
		URI uri = URI.create(registrationEndpoint);
		String path = uri.getPath();
		String host = uri.getHost();
		long duration = end.toEpochMilli() - start.toEpochMilli();
		String fapiInteractionId = res.getHeaders().getFirst("x-fapi-interaction-id");
		PcmEvent event = new PcmEvent(res.getStatusCode().value(), "POST",
			host, fapiInteractionId, path, directoryCredentials.directoryClientId(), directoryCredentials.orgId(),
			orgId, duration, "client", additionalInfo);
		emitPcmEvent(event);
		if(!res.getStatusCode().is2xxSuccessful()) {
			logger.error("DCR client registration failed for client at {} with status {} and message {}", registrationEndpoint, res.getStatusCode(), res.getBody());
			DcrException.UserReport report = new DcrException.UserReport();
			report.setEndpoint(registrationEndpoint);
			report.setStatus(res.getStatusCode());

			Gson gson = new Gson();
			Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
			report.setResponseBody(gson.fromJson(res.getBody(), mapType));
			report.setRequestBody(gson.fromJson(dcrRequest, mapType));

			report.setResponseHeaders(res.getHeaders());
			report.setRequestHeaders(requestHeaders);
			throw new DcrException(report);
		}
		logger.info("DCR client registration successful");
		JsonObject client = JsonParser.parseString(res.getBody()).getAsJsonObject();

		return client;

	}


	private JsonObject getDiscoveryDocument(String directoryDiscoveryUri, boolean mtlsAliases) {
		logger.info("Fetching discovery document (cached) from {}", directoryDiscoveryUri);
		return cachedDiscoveryDocumentService.getDiscoveryDocument(directoryDiscoveryUri, mtlsAliases);
	}

	private HttpClient createHttpClient(DirectoryCredentials credentials)
		throws NoSuchAlgorithmException,
		KeyManagementException {

		HttpClientBuilder builder = HttpClientBuilder.create()
			.useSystemProperties();

		int timeout = 60;
		RequestConfig config = RequestConfig.custom()
			.setConnectTimeout(timeout * 1000)
			.setConnectionRequestTimeout(timeout * 1000)
			.setSocketTimeout(timeout * 1000).build();
		builder.setDefaultRequestConfig(config);

		KeyManager[] km = credentials.mtlsKeymanagers();

		String[] suites = {"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"};

		TrustManager[] trustAllCerts = {
			new X509TrustManager() {

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}
			}
		};

		SSLContext sc = SSLContext.getInstance("TLS");
		sc.init(km, trustAllCerts, new SecureRandom());

		builder.setSSLContext(sc);

		SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sc,
			new String[] { "TLSv1.2" },
			suites,
			NoopHostnameVerifier.INSTANCE);

		builder.setSSLSocketFactory(sslConnectionSocketFactory);

		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
			.register("https", sslConnectionSocketFactory)
			.register("http", new PlainConnectionSocketFactory())
			.build();

		PoolingHttpClientConnectionManager ccm = new PoolingHttpClientConnectionManager(registry);
		ccm.setMaxTotal(100);
		ccm.setDefaultMaxPerRoute(10);

		builder.setConnectionManager(ccm);

		builder.disableRedirectHandling();

		builder.disableAutomaticRetries();

		HttpClient httpClient = builder.build();
		return httpClient;
	}

	private void emitPcmEvent(PcmEvent event) {
		try {
			pcmRepository.save(event);
		} catch(Exception e) {
			logger.error("Failed to emit PCM event", e);
		}
	}


}
