package net.openid.conformance.extensions.yacs;

import javax.servlet.http.HttpServletRequest;

public interface RequestMatch {

	static RequestMatch MATCH_ANY = (r) -> true;

	boolean matches(HttpServletRequest request);

}
