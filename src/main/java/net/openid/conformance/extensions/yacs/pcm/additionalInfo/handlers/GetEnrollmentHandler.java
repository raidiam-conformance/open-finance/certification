package net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers;

import com.google.gson.JsonObject;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.http.client.methods.HttpRequestWrapper;
import org.springframework.http.HttpMethod;

import java.util.List;


public class GetEnrollmentHandler extends BaseHandler {

	public GetEnrollmentHandler(ServerInfoTemplate serverInfoTemplate) {
		super(serverInfoTemplate);
	}

	@Override
	public void handle(JsonObject additionalInfo, HttpRequestWrapper request) {
		addEnrollmentId(additionalInfo);
		addStatus(additionalInfo);
		addErrorCodes(additionalInfo);
		addPersonType(additionalInfo);
		addCancellationReasonFromResponseBody(additionalInfo);
		addCancelledFrom(additionalInfo);
		addClientIp(additionalInfo);
	}

	@Override
	public List<String> getDecoratedEndpoints() {
		return List.of(
			"/open-banking/enrollments/v1/enrollments/{enrollmentId}",
			"/open-banking/enrollments/v2/enrollments/{enrollmentId}"
		);
	}

	@Override
	public HttpMethod getMethod() {
		return HttpMethod.GET;
	}
}
