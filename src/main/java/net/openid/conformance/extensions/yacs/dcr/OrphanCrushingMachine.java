package net.openid.conformance.extensions.yacs.dcr;

import net.openid.conformance.testmodule.OIDFJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.TemporalAmount;
import java.util.Map;

public class OrphanCrushingMachine {

	private static final TemporalAmount FIFTEEN_MINUTES = Duration.ofMinutes(15);
	private static final Logger LOG = LoggerFactory.getLogger(OrphanCrushingMachine.class);

	private DcrClientRepository clientRepository;
	private DcrHelper dcrHelper;

	public OrphanCrushingMachine(DcrClientRepository clientRepository, DcrHelper dcrHelper) {
		this.clientRepository = clientRepository;
		this.dcrHelper = dcrHelper;
	}

	@Scheduled(fixedRateString = "300000")
	public void emptyParticipantsCache() {
		LOG.info("Removing orphaned DCR clients");

		Map<String, ClientHolder> clients = clientRepository.clientsOlderThan(Instant.now().minus(FIFTEEN_MINUTES));
		crushOrphans(clients);
		LOG.info("Orphaned DCR clients removed");
	}

	private void crushOrphans(Map<String, ClientHolder> clients) {
		for(Map.Entry<String, ClientHolder> entry: clients.entrySet()) {
			ClientHolder client = entry.getValue();
			String clientId = OIDFJSON.getString(client.getDynamicClient().get("client_id"));
			String uri = OIDFJSON.getString(client.getDynamicClient().get("registration_client_uri"));
			String testId = entry.getKey();
			try {
				if (!clientRepository.isClientIrremovable(testId)){
					dcrHelper.unregisterClient(client, testId);
					LOG.info("Unregistered orphaned client {} from test {}", clientId, testId);
				} else {
					LOG.info("Client {} is marked as irremovable, no further attempts to delete client registered at {}. Removing from cache.", clientId, uri);
				}
				clientRepository.deleteSavedClient(testId);
			} catch (ClientDeletionException ex) {
				LOG.error("Problem deleting pre-roll DCR client {} for test {}", clientId, testId);
				clientRepository.incrementDeleteRetryCount(entry.getKey());
			}
		}
	}

	public static class ShutdownHook implements Runnable {

		private final OrphanCrushingMachine orphanCrushingMachine;
		private final DcrClientRepository repository;

		public ShutdownHook(DcrClientRepository clientRepository, OrphanCrushingMachine orphanCrushingMachine) {
			this.repository = clientRepository;
			this.orphanCrushingMachine = orphanCrushingMachine;
		}

		@Override
		public void run() {
			LOG.info("Application shutting down - remove as many clients as we can");
			Map<String, ClientHolder> clients = repository.clientsOlderThan(Instant.now());
			orphanCrushingMachine.crushOrphans(clients);
		}
	}

}
