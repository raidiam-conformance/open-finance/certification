package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;

import java.time.Instant;
import java.util.Map;

public interface DcrClientRepository {

	void saveClient(String testId, JsonObject clientConfig);
	ClientHolder getSavedClient(String testId);

	Map<String, ClientHolder> clientsOlderThan(Instant instant);

	void deleteSavedClient(String id) throws ClientDeletionException;

	void setClientIrremovable(String id, boolean irremovable);

	boolean isClientIrremovable(String id);

	void incrementDeleteRetryCount(String testId);
}
