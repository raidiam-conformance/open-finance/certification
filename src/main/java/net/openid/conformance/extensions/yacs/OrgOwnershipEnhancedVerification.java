package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jose.util.JSONObjectUtils;
import com.nimbusds.jwt.JWT;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.testmodule.OIDFJSON;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OrgOwnershipEnhancedVerification implements EnhancedVerification {

	private static final Logger LOG = LoggerFactory.getLogger(OrgOwnershipEnhancedVerification.class);

	private AuthenticationFacade authenticationFacade;

	private final ParticipantsService participantsService;
	private boolean devmode;

	public OrgOwnershipEnhancedVerification(AuthenticationFacade authenticationFacade,
											ParticipantsService participantsService, boolean devmode) {
		this.authenticationFacade = authenticationFacade;
		this.participantsService = participantsService;
		this.devmode = devmode;
	}

	@Override
	public VerificationResult allowed(BodyRepeatableHttpServletRequest request) {
		if(devmode) {
			return VerificationResult.passed();
		}
		JsonObject config = request.body();
		JsonObject serverConfig = config.getAsJsonObject("server");
		if(serverConfig == null) {
			LOG.error("No server config - failing");
			return VerificationResult.generalRulesViolation();
		}
		JsonElement authorisationServerIdElement = serverConfig.get("authorisationServerId");
		if(authorisationServerIdElement == null || OIDFJSON.getString(authorisationServerIdElement).isEmpty()){
			return VerificationResult.generalRulesViolation();
		}
		OIDCAuthenticationToken auth = (OIDCAuthenticationToken) authenticationFacade.getContextAuthentication();
		JWT idToken = auth.getIdToken();
			try {
				var trustFrameworkProfile =  idToken.getJWTClaimsSet().getJSONObjectClaim("trust_framework_profile");
				if(trustFrameworkProfile == null) {
					LOG.info("No trust framework profile on id token");
					return VerificationResult.generalRulesViolation();
				}
				var orgAccessDetails = JSONObjectUtils.getJSONObject(trustFrameworkProfile, "org_access_details");
				if(orgAccessDetails == null) {
					LOG.info("No org access details in trust framework profile");
					return VerificationResult.generalRulesViolation();
				}
				Set<String> orgIds = orgAccessDetails.keySet();
				Set<Map> orgs = participantsService.orgsFor(orgIds);
				List<Map> allAuthServersForOrg = new ArrayList<>();

				for(Map org: orgs) {
					List<Map> authServersForOrg = (List<Map>) org.get("AuthorisationServers");
					allAuthServersForOrg.addAll(authServersForOrg);
				}
				List<String> serverIdsForAuthServers = new ArrayList<>();
				for(Map authServer : allAuthServersForOrg){
					serverIdsForAuthServers.add(authServer.get("AuthorisationServerId").toString());
				}
				String authorisationServerId = OIDFJSON.getString(authorisationServerIdElement);
				boolean allowed = serverIdsForAuthServers.contains(authorisationServerId);
				LOG.info(allowed ? "AuthorisationServerId in config belongs to org of which user is a member" : "AuthorisationServerId is not from any org linked to user");
				return allowed ? VerificationResult.passed() : VerificationResult.generalRulesViolation();

			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
	}

}
