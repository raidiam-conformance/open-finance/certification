package net.openid.conformance.extensions;

import com.google.gson.JsonObject;

import java.time.Instant;
import java.util.Map;

public interface TestModuleRepository {
	void saveTestModuleDetails(String testId, JsonObject testModuleInfo);

	JsonObject getCachedObjectForTest(String testId);

	Map<String, JsonObject> getTestModulesOlderThan(Instant instant);

	void removeOlderThan(Instant instant);

	String getTestIdFromJsonPrimitiveValue(String value, String jsonKey);
}
