package net.openid.conformance.extensions;

public enum EndpointLimits {
	CREATE_TEST_PLAN(10, 60);

	private final int executionLimit;
	private final int executionIntervalLimitSeconds;

	EndpointLimits(int executionLimit, int executionIntervalLimitSeconds) {
		this.executionLimit = executionLimit;
		this.executionIntervalLimitSeconds = executionIntervalLimitSeconds;
	}

	public int getExecutionLimit() {
		return executionLimit;
	}

	public int getExecutionIntervalLimitSeconds() {
		return executionIntervalLimitSeconds;
	}
}
