package net.openid.conformance.extensions.oneshot;

import net.openid.conformance.extensions.CertificationOffloadHelper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Set;

@Configuration
public class OneshotConfig {

	@Bean
	public OneShotRunner oneShotRunner(OneShotService oneShotService, Set<OneShotJob> jobs, TaskExecutor taskExecutor) {
		return new OneShotRunner(taskExecutor, oneShotService, jobs);
	}

	@Bean
	@AssureJobRun(jobName = "MigrateLegacyCertificationsJob")
	@ConditionalOnBean(CertificationOffloadHelper.class)
	public MigrateLegacyCertificationsJob migrateLegacyCertificationsJob(MongoTemplate mongoTemplate,
																		 CertificationOffloadHelper helper) {
		return new MigrateLegacyCertificationsJob(mongoTemplate, helper);
	}
}
