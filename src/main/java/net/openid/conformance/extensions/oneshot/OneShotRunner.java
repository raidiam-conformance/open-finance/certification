package net.openid.conformance.extensions.oneshot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;

import java.util.Optional;
import java.util.Set;

public class OneShotRunner {

	private static final Logger LOG = LoggerFactory.getLogger(OneShotRunner.class);

	private final OneShotService oneShotService;
	private final Iterable<OneShotJob> jobs;
	private final TaskExecutor taskExecutor;

	public OneShotRunner(TaskExecutor taskExecutor, OneShotService oneShotService, Set<OneShotJob> jobs) {
		this.taskExecutor = taskExecutor;
		this.oneShotService = oneShotService;
		this.jobs = jobs;
	}

	@EventListener(ApplicationReadyEvent.class)
	public void runOutstandingJobs() {
		for (OneShotJob job : jobs) {
			OneShot oneShot = job.getClass().getAnnotation(OneShot.class);
			String id = Optional.ofNullable(oneShot).map(OneShot::id).orElse(job.getClass().getName());
			LOG.info("Checking job {} for possible execution", id);
			if(!oneShotService.hasExecuted(id)) {
				taskExecutor.execute(new JobExecution(id, job));
			} else {
				LOG.info("Skipping job {} as it has already run - consider removing it from the codebase", id);
			}
		}
	}

	class JobExecution implements Runnable {

		private final String id;
		private final OneShotJob job;

		JobExecution(String id, OneShotJob job) {
			this.id = id;
			this.job = job;
		}

		@Override
		public void run() {
			LOG.info("Executing job {}", id);
			job.execute();
			oneShotService.saveExecution(id);
			LOG.info("Job {} executed", id);
		}
	}

}
