package net.openid.conformance.extensions.oneshot;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = DBOneShotService.COLLECTION)
public class OneShotInfo {

	@Id
	private String _id;
	private boolean executed;
	private Date executedAt;

	public OneShotInfo() {
		// Load constructor
	}

	public OneShotInfo(String id) {
		this._id = id;
		this.executed = false;
		this.executedAt = null;
	}

	public String getId() {
		return _id;
	}

	public boolean isExecuted() {
		return executed;
	}

	public void setExecuted(boolean executed) {
		this.executed = executed;
		this.executedAt = new Date();
	}

	public Date getExecutedAt() {
		return executedAt;
	}
}
