package net.openid.conformance.extensions.oneshot;

public interface OneShotService {


	void saveExecution(String id);

	boolean hasExecuted(String id);
}
