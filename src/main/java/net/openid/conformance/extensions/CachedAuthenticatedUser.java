package net.openid.conformance.extensions;

import java.util.Objects;

public class CachedAuthenticatedUser {

	private final String sub;
	private final String iss;

	public CachedAuthenticatedUser(String sub, String iss) {
		this.sub = sub;
		this.iss = iss;
	}

	public String getSub() {
		return sub;
	}

	public String getIss() {
		return iss;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || !getClass().equals(o.getClass())){
			return false;
		}
		CachedAuthenticatedUser that = (CachedAuthenticatedUser) o;
		return Objects.equals(sub, that.sub) && Objects.equals(iss, that.iss);
	}

	@Override
	public int hashCode() {
		return Objects.hash(sub, iss);
	}
}
