package net.openid.conformance.extensions;


import java.time.Instant;
import java.util.Map;

public interface AuthenticatedUserRepository {

	void incrementEndpointCalled(CachedAuthenticatedUser cachedAuthenticatedUser, EndpointLimits endpointLimits);

	Map<String, Integer> getUserUsage(CachedAuthenticatedUser cachedAuthenticatedUser);

	CachedAuthenticatedUser getAuthenticatedUser(CachedAuthenticatedUser cachedAuthenticatedUser);

	boolean getLimitReached(CachedAuthenticatedUser cachedAuthenticatedUser, EndpointLimits endpointLimits);

	void removeAuthenticatedUserOlderThan(Instant instant);
}
