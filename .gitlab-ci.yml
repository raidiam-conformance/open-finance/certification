image: alpine:latest

workflow:
  auto_cancel:
    on_new_commit: interruptible

variables:
  SANDBOX_DOCKER_REGISTRY_BASE: 574283098123.dkr.ecr.us-west-2.amazonaws.com
  DEV_DOCKER_REGISTRY_BASE: 241533149311.dkr.ecr.us-west-2.amazonaws.com
  SANDBOX_REGISTRY_NAME: obb-conformance-suite
  DEV_REGISTRY_NAME: obb-conformance-suite
  DOCKER_REGISTRY: "${SANDBOX_DOCKER_REGISTRY_BASE}/${SANDBOX_REGISTRY_NAME}"
  DEV_DOCKER_REGISTRY: "${DEV_DOCKER_REGISTRY_BASE}/${DEV_REGISTRY_NAME}"
  LM_JAVA_VERSION: 17

stages:
  - maintenance_pages
  - build
  - deploy_versioned_jar
  - dev_build
  - test
  - upload_dev
  - upload

#  - deploy
#  - compare
#  - cleanup

.maven: &maven
  image: maven:3-openjdk-17
  interruptible: true
  variables:
    MAVEN_CONFIG: $CI_PROJECT_DIR/.m2
    MAVEN_OPTS: "-Duser.home=$CI_PROJECT_DIR"
  cache:
    key: maven
    paths:
      - .m2/

build:
  <<: *maven
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  stage: build
  coverage: /\d+.{0,1}\d* \% covered/
  script:
    - mvn versions:set -DnewVersion=0.0.2-${CI_COMMIT_SHORT_SHA}
    - mvn -B -Dpmd.skip clean deploy -Dspring-boot.repackage.skip -s ci_settings.xml
    - set_app_version
    - mvn -B -Dpmd.skip clean package
    - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print covered, "/", instructions, " instructions covered"; print 100*covered/instructions, "% covered" }'  target/site/jacoco/jacoco.csv
  artifacts:
    paths:
      - target/

dev_build:
  <<: *maven
  except:
    - master
  stage: build
  script:
    - set_app_version
    - mvn -B clean package
  artifacts:
    paths:
      - target/
    reports:
      junit:
        - "**/target/surefire-reports/TEST-*.xml"

dev_test_coverage:
  <<: *maven
  except:
    - master
  stage: test
  needs: ["dev_build"]
  coverage: /\d+.{0,1}\d* \% covered/
  script:
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - test_coverage
  after_script:
    - microdnf -y install python3
    - python3 scripts/cover2cover.py target/site/jacoco/jacoco.xml $CI_PROJECT_DIR/src/main/java/ > target/site/cobertura.xml
    - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print covered, "/", instructions, " instructions covered"; print 100*covered/instructions, "% covered" }'  target/site/diffCoverage/diff-coverage.csv
    - echo "Report is available at https://raidiam-conformance.gitlab.io/-/open-finance/certification/-/jobs/${CI_JOB_ID}/artifacts/target/site/diffCoverage/html/index.html"
  allow_failure:
    exit_codes:
      - 137
  artifacts:
    when: always
    expose_as: test_coverage_report
    paths:
      - target/site/diffCoverage/html/
    reports:
      coverage_report:
        coverage_format: cobertura
        path: target/site/cobertura.xml

dev_mutation_coverage:
  <<: *maven
  except:
    - master
  stage: test
  needs: ["dev_build"]
  script:
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - mutation_coverage
  allow_failure:
    exit_codes:
      - 137
  artifacts:
    when: always
    paths:
      - target/pit-reports/
    expose_as: mutation_coverage_report


upload_dev:
  resource_group: uses_dev_environment
  except:
    - master
  stage: upload_dev
  needs: ["dev_build", "dev_test_coverage", "dev_mutation_coverage"]
  interruptible: true
  image: docker:24.0.7-git
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: sts.amazonaws.com
  script:
    - setup_docker
    - install_build_dependencies
    - assume_aws_role "$DEV_ROLE_ARN"
    - upload_dev
    - sleep 180 # wait for deployment to happen
    - verify_deployed_version
    - run_dev_tests # runs as part of upload jobs - combined with resource_group, this makes sure no other pipeline deploys before/whilst the tests run
  allow_failure:
    exit_codes:
      - 137

upload:
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  stage: upload
  needs: ["build"]
  interruptible: true
  image: docker:git
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: sts.amazonaws.com
  script:
    - setup_docker
    - install_build_dependencies
    - assume_aws_role "$SB_ROLE_ARN"
    - upload
  allow_failure:
    exit_codes:
      - 137

maintenance_pages:
    stage: maintenance_pages
    rules:
      - if: $CI_COMMIT_BRANCH == "master"
        changes:
          - "maintenance/*"

    image: docker:git
    services:
      - docker:dind
    variables:
      DOCKER_DRIVER: overlay2
    id_tokens:
      GITLAB_OIDC_TOKEN:
        aud: sts.amazonaws.com
    script:
        - setup_docker
        - install_build_dependencies
        - assume_aws_role "$SB_ROLE_ARN"
        - upload_maintenance
#
#test:
#  <<: *maven
#  stage: test
#  needs: ["build"]
#  script:
#    # Don't recompile the main app
#    - mvn -B -Dmaven.main.skip test
#    - |
#      perl -ne '/Total.*?(\d+%)/ && print "Unit test coverage $1\n"' target/site/jacoco/index.html.j2
#  only:
#    - branches
#
#.compare: &compare
#  stage: compare
#  interruptible: true
#  # we only want to show a 'warning' if there's differences, but gitlab is inflexible so any failure will show as a
#  # warning. When implemented, https://gitlab.com/gitlab-org/gitlab/-/issues/16733 would allow us to still fail if
#  # something went wrong.
#  allow_failure: true
#  image: python:alpine
#  only:
#    refs:
#      - branches
#    kubernetes: active
#
#compare_cloud:
#  <<: *compare
#  needs: ["server_test", "ciba_test", "client_test"]
#  script:
#    - run_compare server_test ciba_test client_test
#
#compare_local:
#  <<: *compare
#  needs: ["local_test"]
#  script:
#    - run_compare local_test
#
#check-trailing-whitespace:
#  interruptible: true
#  stage: build
#  image: python:2
#  script:
#    - ./scripts/checkwhitespace.py
#  only:
#    - branches

#test:
#  <<: *maven
#  stage: test
#  needs: ["build"]
#  coverage: '/Unit test coverage (\d+\%)/'
#  script:
#    # Don't recompile the main app
#    - mvn -B -Dmaven.main.skip test
#    - |
#      perl -ne '/Total.*?(\d+%)/ && print "Unit test coverage $1\n"' target/site/jacoco/index.html.j2
#  only:
#    - branches
#
#.compare: &compare
#  stage: compare
#  interruptible: true
#  # show a 'warning' (not a failure) if we successfully find differences
#  allow_failure:
#    exit_codes:
#      - 16
#  image: python:3.10-alpine3.15
#  only:
#    refs:
#      - branches
#
#compare_cloud:
#  <<: *compare
#  needs: ["server_test", "ciba_test", "ekyc_test", "brazil_test"]
#  script:
#    - run_compare server_test ciba_test ekyc_test brazil_test
#
#compare_local:
#  <<: *compare
#  needs: ["local_test"]
#  script:
#    - run_compare local_test
#
#compare_client:
#  <<: *compare
#  needs: ["client_test"]
#  script:
#    - run_compare client_test
#
#check-trailing-whitespace:
#  interruptible: true
#  stage: build
#  image: python:3.10-alpine3.15
#  script:
#    - apk add -U git
#    - ./scripts/checkwhitespace.py
#  only:
#    - branches

#codequality:
#  image: docker:latest
#  variables:
#    DOCKER_DRIVER: overlay2
#  services:
#    - docker:dind
#  script:
#    - setup_docker
#    - codeclimate
#  artifacts:
#    paths: [codeclimate.json]
#
#.auto-deploy:
#  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v1.1.0"
#
#.deploy: &deploy
#  extends: .auto-deploy
#  stage: deploy
#  # there's a race condition between 'find_deploy_url' and the actual deploy where we could end up with multiple
#  # deployments to the same url - use a resource_group to avoid multiple deploy jobs running at the same time
#  resource_group: deploy
#  script:
#    - find_deploy_url
#    - echo "DYNAMIC_ENVIRONMENT_URL=$CI_ENVIRONMENT_URL" >> deploy.env
#    - auto-deploy check_kube_domain
#    - auto-deploy download_chart
#    - auto-deploy ensure_namespace
#    - auto-deploy initialize_tiller
#    - deploy
#  artifacts:
#    reports:
#      dotenv: deploy.env
#  dependencies: []
#  only:
#    refs:
#      - branches
#    kubernetes: active
#
#deploy-review:
#  <<: *deploy
#  environment:
#    name: review/$CI_COMMIT_REF_NAME
#    on_stop: stop_review
#    url: $DYNAMIC_ENVIRONMENT_URL
#  except:
#    - master
#    - production
#    - demo
#
#deploy-normal:
#  <<: *deploy
#  environment:
#    name: $CI_COMMIT_BRANCH
#    on_stop: stop_normal
#    url: $DYNAMIC_ENVIRONMENT_URL
#  only:
#    - production
#    - demo
#
#deploy-staging:
#  <<: *deploy
#  environment:
#    name: staging
#    url: $DYNAMIC_ENVIRONMENT_URL
#  only:
#    - master
#
#stop_review:
#  extends: .auto-deploy
#  stage: cleanup
#  variables:
#    GIT_STRATEGY: none
#  script:
#    - auto-deploy initialize_tiller
#    - auto-deploy delete
#  environment:
#    name: review/$CI_COMMIT_REF_NAME
#    action: stop
#  needs: []
#  when: manual
#  allow_failure: true
#  only:
#    refs:
#      - branches
#    kubernetes: active
#  except:
#    - master
#    - production
#    - demo
#
#stop_normal:
#  extends: .auto-deploy
#  stage: cleanup
#  variables:
#    GIT_STRATEGY: none
#  script:
#    - auto-deploy initialize_tiller
#    - auto-deploy delete
#  environment:
#    name: $CI_COMMIT_BRANCH
#    action: stop
#  needs: []
#  when: manual
#  allow_failure: true
#  only:
#    - demo
#
#.deployment_test: &deployment_test
#  stage: test
#  interruptible: true
#  image: python:3.9-alpine3.14
#  only:
#    refs:
#      - branches
#    kubernetes: active
#  artifacts:
#    when: always
#    paths:
#      - "*.zip"
#
#client_test:
#  <<: *deployment_test
#  script:
#    - set_up_for_running_test_plan
#    - run_client_test_plan
#
#server_test:
#  <<: *deployment_test
#  script:
#    - set_up_for_running_test_plan
#    - run_server_test_plan
#
#ciba_test:
#  <<: *deployment_test
#  script:
#    - set_up_for_running_test_plan
#    - run_ciba_test_plan
#
#brazil_test:
#  <<: *deployment_test
#  script:
#    - set_up_for_running_test_plan
#    - run_brazil_test_plan
#
#local_test:
#  stage: test
#  interruptible: true
#  image: docker:19.03.10
#  services:
#    - docker:dind
#  needs: ["build"]
#  script:
#    - apk add docker-compose
#    - docker-compose -f $CI_PROJECT_DIR/docker-compose-localtest.yml pull
#    - docker-compose -f $CI_PROJECT_DIR/docker-compose-localtest.yml build --pull
#    - docker-compose -f $CI_PROJECT_DIR/docker-compose-localtest.yml up --detach httpd
#    - sleep 5
#    - docker-compose -f $CI_PROJECT_DIR/docker-compose-localtest.yml logs --tail="all" oidcc-provider
#    - docker-compose -f $CI_PROJECT_DIR/docker-compose-localtest.yml run test
#  after_script:
#    - docker-compose -f $CI_PROJECT_DIR/docker-compose-localtest.yml logs --tail="all" oidcc-provider > $CI_PROJECT_DIR/oidcc-provider-log.txt
#    - docker-compose -f $CI_PROJECT_DIR/docker-compose-localtest.yml logs --tail="all" server > $CI_PROJECT_DIR/server-log.txt
#    - docker-compose -f $CI_PROJECT_DIR/docker-compose-localtest.yml exec -T mongodb mongodump --db=test_suite --archive=/data/db/mongodb-dump.gz --gzip
#    - docker-compose -f $CI_PROJECT_DIR/docker-compose-localtest.yml down
#  artifacts:
#    when: always
#    paths:
#      - oidcc-provider-log.txt
#      - server-log.txt
#      - mongo/data/mongodb-dump.gz
#      - "*.zip"
# ---------------------------------------------------------------------------

.auto_devops: &auto_devops |
  # Auto DevOps variables and functions
  [[ "$TRACE" ]] && set -x
  export CI_APPLICATION_REPOSITORY=gcr.io/$GCP_PROJECT_ID/$CI_COMMIT_REF_SLUG
  export CI_APPLICATION_TAG=$CI_COMMIT_SHA
  export CI_CONTAINER_NAME=ci_job_build_${CI_JOB_ID}
  export TILLER_NAMESPACE=$KUBE_NAMESPACE
  export HELM_HOST="localhost:44134"


  function test_coverage() {
    echo $CI_COMMIT_BRANCH
    if [[ $CI_COMMIT_BRANCH == "untested/"* ]]; then
      if ! mvn diff-coverage:diffCoverage; then
        exit 137
      fi
    else
      mvn diff-coverage:diffCoverage
    fi
  }

  function mutation_coverage() {
    if [[ $CI_COMMIT_BRANCH == "untested/"* ]]; then
      if ! ./scripts/run-mutation-tests.sh; then
        exit 137
      fi
    else
      ./scripts/run-mutation-tests.sh
    fi

  }

  function codeclimate() {
    cc_opts="--env CODECLIMATE_CODE="$PWD" \
             --volume "$PWD":/code \
             --volume /var/run/docker.sock:/var/run/docker.sock \
             --volume /tmp/cc:/tmp/cc"

    docker run ${cc_opts} codeclimate/codeclimate analyze -f json src > codeclimate.json
  }

  function find_deploy_url() {
    if [ "$CI_COMMIT_BRANCH" = "master" ]; then
      CI_ENVIRONMENT_URL=https://staging.$KUBE_INGRESS_BASE_DOMAIN
      return
    elif [ "$CI_COMMIT_BRANCH" = "production" ]; then
      CI_ENVIRONMENT_URL=https://www.$KUBE_INGRESS_BASE_DOMAIN
      return
    elif [ "$CI_COMMIT_BRANCH" = "demo" ]; then
      CI_ENVIRONMENT_URL=https://demo.$KUBE_INGRESS_BASE_DOMAIN
      return
    fi
    # not one of the fixed branches - find a review app to use
    apk add jq
    url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/environments?states=available&per_page=100&search=review/"
    curl -o env-list.js --silent --show-error --header "PRIVATE-TOKEN: ${GITLAB_API_KEY_OIDF_BOT}" "$url"
    res=$?
    if [ $res -ne 0 ]; then
      echo "curl of $url failed: $res"
      exit 1
    fi
    echo "Active review apps:"
    jq ".[] | del(.project,.state)" < env-list.js # the project element is big/always the same/unimportant
    echo "Checking for environment for CI_ENVIRONMENT_SLUG '${CI_ENVIRONMENT_SLUG}'"
    # first check if there's already an environment for this branch
    CI_ENVIRONMENT_URL=`jq -r ".[] | select(.slug==\"${CI_ENVIRONMENT_SLUG}\") | .external_url" < env-list.js`
    if [ -z "$CI_ENVIRONMENT_URL" ] || [ "$CI_ENVIRONMENT_URL" == "null" ]; then
      echo "None found; trying to find a free environment"
      x=1
      while :; do
        FAKE_BRANCH_NAME=dev-branch-$x
        CI_ENVIRONMENT_URL=https://review-app-$FAKE_BRANCH_NAME.$KUBE_INGRESS_BASE_DOMAIN
        echo "Checking: $CI_ENVIRONMENT_URL"
        res=0
        jq --exit-status  ".[] | select(.external_url==\"$CI_ENVIRONMENT_URL\") | halt_error(9)" < env-list.js || res=$?
        echo "jq exit status: $res"
        if [ $res -eq 4 ]; then
          # no match - we found a free one
          break
        fi
        x=$(( $x + 1 ))
        if [ $x -gt 9 ]; then
          echo "All review app environments seem to already be in use. Instructions on how to proceed can be found on the wiki:"
          echo "https://gitlab.com/openid/conformance-suite/-/wikis/Developers/Contributing#review-app-failures"
          exit 1
        fi
      done
      echo "Found a free environment: $CI_ENVIRONMENT_URL"
    else
      echo "This branch is already using environment: $CI_ENVIRONMENT_URL"
    fi
  }

  function deploy() {
    track="${1-stable}"
    name="$CI_ENVIRONMENT_SLUG"

    if [[ "$track" != "stable" ]]; then
      name="$name-$track"
    fi

    replicas="1"
    service_enabled="false"

    env_track=$( echo $track | tr -s  '[:lower:]'  '[:upper:]' )
    env_slug=$( echo ${CI_ENVIRONMENT_SLUG//-/_} | tr -s  '[:lower:]'  '[:upper:]' )

    if [[ "$track" == "stable" ]]; then
      # for stable track get number of replicas from `PRODUCTION_REPLICAS`
      eval new_replicas=\$${env_slug}_REPLICAS
      service_enabled="true"
    else
      # for all tracks get number of replicas from `CANARY_PRODUCTION_REPLICAS`
      eval new_replicas=\$${env_track}_${env_slug}_REPLICAS
    fi
    if [[ -n "$new_replicas" ]]; then
      replicas="$new_replicas"
    fi

    if [[ "$CI_COMMIT_BRANCH" == "master" ]]; then
      # we expect much bigger databases on staging as it persists; use more RAM and disk for mongo
      mongo_settings="--set mongodb.resources.limits.memory=4Gi \
        --set mongodb.persistence.size=250Gi"
    fi
    if [[ "$CI_COMMIT_BRANCH" == "production" ]]; then
      # we expect much bigger databases on production and we have a dedicated node; use much more RAM and disk for mongo
      mongo_settings="--set mongodb.resources.limits.memory=9Gi \
        --set mongodb.persistence.size=450Gi"
    fi

    # Create OIDC credentials secrets
    kubectl delete secret oidc-gitlab-credentials \
      --namespace="$KUBE_NAMESPACE" \
      --ignore-not-found=true
    kubectl delete secret oidc-google-credentials \
      --namespace="$KUBE_NAMESPACE" \
      --ignore-not-found=true
    kubectl create secret generic oidc-gitlab-credentials \
      --namespace="$KUBE_NAMESPACE" \
      --from-literal=clientid="$OIDC_GITLAB_CLIENTID" \
      --from-literal=secret="$OIDC_GITLAB_SECRET"
    kubectl create secret generic oidc-google-credentials \
      --namespace="$KUBE_NAMESPACE" \
      --from-literal=clientid="$OIDC_GOOGLE_CLIENTID" \
      --from-literal=secret="$OIDC_GOOGLE_SECRET"

    helm upgrade --install \
      --wait \
      --set application.env_slug="$CI_ENVIRONMENT_SLUG" \
      --set application.path_slug="$CI_PROJECT_PATH_SLUG" \
      --set service.enabled="$service_enabled" \
      --set releaseOverride="$CI_ENVIRONMENT_SLUG" \
      --set image.repository="$CI_APPLICATION_REPOSITORY" \
      --set image.tag="$CI_APPLICATION_TAG" \
      --set image.pullPolicy=IfNotPresent \
      --set application.track="$track" \
      --set service.url="$CI_ENVIRONMENT_URL" \
      --set replicaCount="$replicas" \
      $mongo_settings \
      --namespace="$KUBE_NAMESPACE" \
      --version="$CI_PIPELINE_ID-$CI_JOB_ID" \
      "$name" \
      chart/

    echo -n "Waiting for mongodb pod to start"
    export MONGO_POD=""
    while [ -z "$MONGO_POD" ]; do echo -n .; sleep 5; get_mongo_pod; done; echo
    echo "Using pod: $MONGO_POD"
    echo -n "Connecting to mongodb"
    while ! kubectl exec $MONGO_POD -- mongo mongodb://localhost/test_suite --eval "db.version()" > /dev/null 2>&1; do echo -n .; sleep 1; done; echo
    # despite the db.version check we've seen instances of the insert failing - give it an extra few seconds to settle
    sleep 2
    echo "Inserting CI token"
    CI_TOKEN=$( dd bs=64 count=1 < /dev/urandom | base64 - | tr -d '\n=' )
    kubectl exec $MONGO_POD -- \
      mongo mongodb://localhost/test_suite --eval \
        "db.API_TOKEN.update({ _id: \"ci_token\" }, { _id: \"ci_token\", owner: { sub: \"ci\", iss: \"${CI_ENVIRONMENT_URL}\" }, info: {}, token: \"${CI_TOKEN}\", expires: null }, { upsert: true })"
    echo "Inserting preset tokens"
    OLDIFS=$IFS
    IFS='|'
    for token in ${PRESET_CI_TOKENS}; do
      kubectl exec $MONGO_POD -- \
        mongo mongodb://localhost/test_suite --eval \
          "db.API_TOKEN.update($token, { upsert: true })"
    done
    IFS=$OLDIFS

    # This passes these two variables to the test jobs
    echo "CONFORMANCE_SERVER=$CI_ENVIRONMENT_URL" >> deploy.env
    # Note that this means the token is available to download from the pipeline artifacts - this isn't great, but equally the token doesn't give access to anything really important
    echo "CONFORMANCE_TOKEN=$CI_TOKEN" >> deploy.env
  }

  function get_mongo_pod() {
    MONGO_POD=`kubectl -n ${KUBE_NAMESPACE} get pod \
      -l app.kubernetes.io/component=mongodb -l app.kubernetes.io/instance=${CI_ENVIRONMENT_SLUG} \
      --field-selector=status.phase=Running \
      -o template --template="{{(index .items 0).metadata.name}}" 2>&1` \
      || MONGO_POD=''
    export MONGO_POD
  }

  function setup_docker() {
    if ! docker info &>/dev/null; then
      if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      fi
    fi
  }

  function ensure_namespace() {
    kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
  }

  function check_kube_domain() {
    if [ -z ${KUBE_INGRESS_BASE_DOMAIN+x} ]; then
      echo "In order to deploy, KUBE_INGRESS_BASE_DOMAIN must be set as a variable at the group or project level, or manually added in .gitlab-ci.yml"
      false
    else
      true
    fi
  }

  function install_build_dependencies() {
    apk -q add --no-cache --update --upgrade curl tar gzip make ca-certificates openssl python3 py-pip aws-cli
    apk -q add --no-cache py3-cffi py3-wheel cython py3-yaml py3-httpx
    update-ca-certificates
    OS=`uname -s | tr '[:upper:]' '[:lower:]'`
    ARCH=`uname -m | tr '[:upper:]' '[:lower:]'`
    curl -L https://github.com/docker/compose/releases/download/v2.23.3/docker-compose-$OS-$ARCH -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
    docker-compose --version
    aws --version
  }

  function run_compare() {
    apk add curl colordiff
    mkdir reference
    cd reference
    for i in $*; do
      echo "Fetching master branch latest passed pipeline results for '$*'"
      res=0
      curl --silent --show-error --fail -L -o $i.zip https://gitlab.com/openid/conformance-suite/-/jobs/artifacts/master/download?job=$i || res=$?
      if [ $res -eq 0 ]; then
        unzip $i.zip
        rm $i.zip
      fi
    done
    cd ..
    ./scripts/compare-results.py reference .
  }

  function assume_aws_role() {
    echo "Assuming AWS Role"
    export $(printf "AWS_ACCESS_KEY_ID=%s AWS_SECRET_ACCESS_KEY=%s AWS_SESSION_TOKEN=%s" \
      $(aws sts assume-role-with-web-identity \
      --role-arn "$1" \
      --role-session-name "GitLabRunner-${CI_PROJECT_ID}-${CI_PIPELINE_ID}" \
      --web-identity-token ${GITLAB_OIDC_TOKEN} \
      --duration-seconds 3600 \
      --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' \
      --output text))
  }

  function upload_dev() {

    echo "Logging in to container registry..."
    aws ecr --region us-west-2 get-login-password | docker login --username AWS --password-stdin $DEV_DOCKER_REGISTRY
    aws ecr batch-delete-image --repository-name $DEV_REGISTRY_NAME --image-ids imageTag=dev --region us-west-2

    echo "Pushing image to ECR"
    docker buildx create --driver docker-container --name multi-arch --use
    if aws ecr describe-images --repository-name=$DEV_REGISTRY_NAME --image-ids=imageTag=$CI_APPLICATION_TAG --region us-west-2 > /dev/null ; then
      echo "Image already pushed - not pushing again"
    else
      docker buildx build --platform linux/amd64,linux/arm64 --builder=multi-arch --push -t $DEV_DOCKER_REGISTRY:$CI_APPLICATION_TAG .
    fi
    docker buildx build --platform linux/amd64,linux/arm64 --builder=multi-arch --push -t $DEV_DOCKER_REGISTRY:dev .
    echo "Uploaded tag - $CI_APPLICATION_TAG"
  }

  function verify_deployed_version() {
    export CONFORMANCE_SERVER="https://dev.opf.conformance.raidiam.io/"
    apk add jq
    DEPLOYED_SHA=$(wget -qO- $CONFORMANCE_SERVER/api/server | jq -r ".version" | sed  's/[^,:]*: //g' | xargs)
    echo "Git commit deployed to dev: '${DEPLOYED_SHA}'"
    echo "Git commit of this pipeline: '${CI_COMMIT_SHA}'"
    if [ "${DEPLOYED_SHA}" != "${CI_COMMIT_SHA}" ] ; then
      echo "Image deployed to dev env is not the image built from this commit - failing"
      exit -1
    fi

     echo "Deployed image is the one built from this commit - testing"

  }

  function run_dev_tests() {
    apk add bash
    export CONFORMANCE_SERVER="https://dev.opf.conformance.raidiam.io/"
    # CONFORMANCE_TOKEN is set in variables under https://gitlab.com/obb1/certification/-/settings/ci_cd
    export CONFORMANCE_TOKEN="$DEV_CONFORMANCE_TOKEN"
    ./scripts/run-functional-tests.sh
  }

  function upload() {
    echo "Logging in to container registry..."

    aws ecr --region us-west-2 get-login-password | docker login --username AWS --password-stdin $DOCKER_REGISTRY
    aws ecr batch-delete-image --repository-name $SANDBOX_REGISTRY_NAME --image-ids imageTag=sandbox --region us-west-2

    echo "Pusing image to ECR"
    docker buildx create --driver docker-container --name multi-arch --use

     if aws ecr describe-images --repository-name=$SANDBOX_REGISTRY_NAME --image-ids=imageTag=$CI_APPLICATION_TAG --region us-west-2 > /dev/null ; then
      echo "Image already pushed - not pushing again"
    else
      docker buildx build --platform linux/amd64,linux/arm64 --builder=multi-arch --push -t $DOCKER_REGISTRY:$CI_APPLICATION_TAG .
    fi
    docker buildx build --platform linux/amd64,linux/arm64 --builder=multi-arch --push -t $DOCKER_REGISTRY:sandbox .
    echo "Uploaded tag - $CI_APPLICATION_TAG"
  }


  function do_upload_maintenance() {

    echo "Logging in to container registry..."

    aws ecr --region us-west-2 get-login-password | docker login --username AWS --password-stdin $DOCKER_REGISTRY
    aws ecr batch-delete-image --repository-name $SANDBOX_REGISTRY_NAME --image-ids imageTag=$3 --region us-west-2

    echo "Pusing image to ECR"

    docker buildx build --platform linux/amd64,linux/arm64 --build-arg CS_ENV="$1" --build-arg CS_IMG="$2" --builder=multi-arch --push -t $DOCKER_REGISTRY:$3 maintenance/

  }

  function upload_maintenance() {
      docker buildx create --driver docker-container --name multi-arch --use
      do_upload_maintenance "Open Finance" "ob_brasil.svg" "opfmaintenance"
      do_upload_maintenance "Open Insurance" "opin_brasil.png" "opinmaintenance"
      do_upload_maintenance "Open Finance FVP" "fvcp_obb.svg" "ocitppmaintenance"
      do_upload_maintenance "Open Finance FVP 2" "fvcp_obb.svg" "yacsmaintenance"
      do_upload_maintenance "Open Insurance FVP 2" "opin_fvp_logo.svg" "opinfvpmaintenance"
  }


  function install_test_dependencies() {
    echo "Installing extra dependencies"
    apk add -U openssh-client git
    apk add --update nodejs nodejs-npm
    eval $(ssh-agent -s)
    echo "$SSH_PRIVATE_KEY" | ssh-add -
    cd ..
  }

  function set_up_for_running_test_plan() {
    install_test_dependencies
    pip install httpx
  }

  function run_client_test_plan() {
    apk add nodejs npm
    echo "Running automated tests against $CONFORMANCE_SERVER"
    ../conformance-suite/.gitlab-ci/run-tests.sh --client-tests-only
  }

  function run_server_test_plan() {
    echo "Running automated tests against $CONFORMANCE_SERVER"
    ../conformance-suite/.gitlab-ci/run-tests.sh --server-tests-only
  }

  function run_ciba_test_plan() {
    echo "Running automated tests against $CONFORMANCE_SERVER"
    ../conformance-suite/.gitlab-ci/run-tests.sh --ciba-tests-only
  }

  function run_brazil_test_plan() {
    echo "Running automated tests against $CONFORMANCE_SERVER"
    ../conformance-suite/scripts/test-configs-brazil/run-raidiam.sh
  }

  function set_app_version() {
    echo "Set application version to GitLab $CI_COMMIT_SHA"

    pwd

    ls -la

    echo "Before"
    cat src/main/resources/application.properties

    sed -i "/fintechlabs.version=.*/ s/$/ build: $CI_COMMIT_SHA	/" src/main/resources/application.properties

    echo "After"
    cat src/main/resources/application.properties

  }

  function run_panva_test_plan() {
    echo "Running automated tests against $CONFORMANCE_SERVER"
    ../conformance-suite/.gitlab-ci/run-tests.sh --panva-tests-only
  }


before_script:
  - *auto_devops
